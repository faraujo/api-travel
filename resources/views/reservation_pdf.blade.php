<!DOCTYPE html>

<html>

<head>

    <title>{{ $title }}</title>
    <style>
        * {
            font-size: 12px;
            border-collapse: collapse;
        }

        .cabecera, .visitante, .titular {
            font-family: Arial, Helvetica, sans-serif;
        }

        .fs-16 {
            font-size: 16px;
        }

        .text-left {
            text-align: left;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .uppercase {
            text-transform: uppercase;
            font-weight: bold;

        }

        .bold {
            font-weight: bold;
        }

        .cabecera, .visitante, .titular, .legal-general-productos {
            width: 100%;
        }

        .grey-bg {
            background-color: #CCCCCC;
        }

        .v-align-bot {
            vertical-align: bottom;
        }

        .v-align-middle {
            vertical-align: middle;
        }

        .v-align-top {
            vertical-align: top;
        }

        .to-left {
            float: left;
        }

        .to-right {
            float: right;
        }

        .folio-header{
            margin-bottom:10px;
            font-family: Arial Narrow, Helvetica, sans-serif;
        }

        #img-logo {
            /*height: 22px !important;*/
            width: 140px !important;
        }

        #img-codebar {
            /*margin: auto auto auto 0;*/
            height: 40px;
            width: 140px;
            margin-top:20px;
        }

        #img-qr {
            height: 90px;
            width: 90px;
        }

        .legal-coupon{
            font-family: Arial Narrow, Helvetica, sans-serif;
        }

        .coupon{
            text-align: center;
            padding:10px;
        }
        .img-coupon{
            width:300px;
        }

        .container-coupon{
            border:1px dashed #444;
        }
        /*.coupons img:nth-child(3n) {*/
            /*margin-right: 0;*/
            /*margin-left: 3%;*/
        /*}*/

        /*.coupons img:nth-child(3n + 1) {*/
            /*margin-left: 0;*/
            /*margin-right: 3%;*/
        /*}*/

        #codebar-number{
            margin-top:10px;
            text-justify: distribute;
            letter-spacing: 11px;
            font-family: Arial;
        }

        .visitante, .titular {
            border-collapse: collapse;
        }

        .visitante td, .visitante th, .titular td, .titular th {
            border: 2px solid black;
        }

        .visitante tr:first-child th, .titular tr:first-child th {
            border-top: 1px solid black;
        }

        .visitante tr:last-child td, .titular tr:last-child td {
            border-bottom: 1px solid black;
        }

        .visitante tr td:first-child,
        .visitante tr th:first-child,
        .titular tr td:first-child,
        .titular tr th:first-child {
            border-left: 1px solid black;
        }

        .visitante tr td:last-child,
        .visitante tr th:last-child,
        .titular tr td:last-child,
        .titular tr th:last-child {
            border-right: 1px solid black;
        }

        .firma, .coupons {
            width: 100%;
        }

        .legal-1, .legal-1 tr, .legal-1 tr td, .legal-1 tr th {
            border: 0px solid grey;
            border-collapse: collapse;
        }


        .anti-fraud-random {
            font-size: 12px;
        }

        .anti-fraud-code {
            font-size: 16px;
        }

    </style>

</head>

<body>

<table class="cabecera">
    <tr>
        <td width="30%" class="logo text-left v-align-top">
            <div>
                <img width="140" id="img-logo" src="{{$client_logo}}" alt="experiencias-cliente">
            </div>
            <div>
                <img id="img-codebar" src="{{$barcode}}" alt="barcode"   />
                <div id="codebar-number">{{$code}}</div>
            </div>
        </td>
        <td width="40%" class="text-center" style="vertical-align: top">
            <b>{!! nl2br(e($direccion_facturacion)) !!}</b>
        </td>
        <td width="30%" class="text-right v-align-top">
            <div class="text-right folio-header fs-16" >{{__('pdf.folio')}} {{$code}}</div>
            <img id="img-qr" src="{{$qrcode}}" alt="barcode"   />


        </td>
    </tr>
    <tr>
        <td colspan="3" class="text-right">
            <span class="anti-fraud-random bold">{{$anti_fraud_random}}</span>
            <span class="anti-fraud-code bold">{{$anti_fraud_code}}</span>
        </td>
    </tr>
</table>

<table class="visitante">
    <tr>
        <th>{{__('pdf.nombre_visitante')}}: {{$client_name}}</th>
        <th>{{__('pdf.fecha_compra')}}: {{$date_time_processed}}</th>
        <th>{{__('pdf.tipo_cambio')}}: {!!  html_entity_decode($conversion) !!}</th>
    </tr>
</table>


{!!  html_entity_decode($payment_data) !!}



<p>{!!  html_entity_decode($body) !!}</p>

<!--<p>{!!  html_entity_decode($legal_1) !!}</p>-->

<p>{!!  html_entity_decode($signatures_reservation) !!}</p>

<!--<p>{!!  html_entity_decode($legal_2) !!}</p>-->

<p>{!!  html_entity_decode($products_legal) !!}</p>

<!--<p>{!!  html_entity_decode($legal_3) !!}</p>-->

{!!  html_entity_decode($img_coupons) !!}

</body>

</html>