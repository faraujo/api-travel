<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>@yield('title')</title>
    <style>
      /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */
      
      /*All the styling goes here*/
      
      img {
        border: none;
        -ms-interpolation-mode: bicubic;
      }
      .social {
        width: 100%;
      }
      .social a {
        font-size: 20px!important;
          padding: 3px;
          margin: 4px!important;
          display: inline-block;
      }
      body {
        background-color: #f6f6f6!important;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }

      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
       }
        table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top; 
      }

      /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

      .body {
        background-color: #f6f6f6!important;
      }

      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      .container {
        display: block;
        margin: 0 auto !important;
        /* makes it centered */
        max-width: 580px;
        width: 580px;
        height: 100%;
      }

      /* This should also be a block element, so that it will fill 100% of the .container */
      .content {
        box-sizing: border-box;
        display: block;
        margin: 0 auto;
        max-width: 580px;
        padding: 10px;
        background-color:white;
        height: 100%;
        height: 100%;
        border-top: 5px solid #00877b;
        border-bottom: 5px solid #00877b;
      }

      /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
      .main {
        background: #ffffff;
        border-radius: 3px;
        width: 100%; 
      }

      .wrapper {
        box-sizing: border-box;
        padding: 20px; 
      }

      .content-block {
        padding-bottom: 10px;
        padding-top: 10px;
      }

      .footer {
        clear: both;
        margin-top: 10px;
        text-align: center;
        width: 100%!important;
      }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
          color: #999999;
          font-size: 12px;
          text-align: center; 
      }

      /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        margin-bottom: 30px; 
      }

      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize; 
      }

      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px; 
      }
        p li,
        ul li,
        ol li {
          list-style-position: inside;
          margin-left: 5px; 
      }

      a {
        color: #3498db;
        text-decoration: underline; 
      }

      /* -------------------------------------
          BUTTONS
      ------------------------------------- */
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto; 
      }
        .btn table td {
          background-color: #ffffff;
          border-radius: 5px;
          text-align: center; 
      }
        .btn a {
          background-color: #ffffff;
          border: solid 1px #00877b;
          border-radius: 5px;
          box-sizing: border-box;
          color: #00877b;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0;
          padding: 12px 25px;
          text-decoration: none;
          text-transform: capitalize; 
      }

      .btn-primary table td {
        background-color: #00877b; 
      }

      .btn-primary a {
        background-color: #00877b;
        border-color: #00877b;
        color: #ffffff; 
      }

      /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
      .last {
        margin-bottom: 0; 
      }

      .first {
        margin-top: 0; 
      }

      .align-center {
        text-align: center; 
      }

      .align-right {
        text-align: right; 
      }

      .align-left {
        text-align: left; 
      }

      .clear {
        clear: both; 
      }

      .mt0 {
        margin-top: 0; 
      }

      .mb0 {
        margin-bottom: 0; 
      }

      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0; 
      }

      .powered-by a {
        text-decoration: none; 
      }

      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        margin: 20px 0; 
      }

      /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
      @media only screen and (max-width: 620px) {
        table[class=body] h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important; 
        }
        table[class=body] p,
        table[class=body] ul,
        table[class=body] ol,
        table[class=body] td,
        table[class=body] span,
        table[class=body] a {
          font-size: 16px !important; 
        }
        table[class=body] .wrapper,
        table[class=body] .article {
          padding: 10px !important; 
        }
        table[class=body] .content {
          padding: 0 !important; 
        }
        table[class=body] .container {
          padding: 0 !important;
          width: 100% !important; 
        }
        table[class=body] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important; 
        }
        table[class=body] .btn table {
          width: 100% !important; 
        }
        table[class=body] .btn a {
          width: 100% !important; 
        }
        table[class=body] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important; 
        }
        table[class=body] .social a {
          font-size: 25px!important;
        }
      }

      /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
      @media all {
        .ExternalClass {
          width: 100%; 
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%; 
        }
        .apple-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important; 
        }
        #MessageViewBody a {
          color: inherit;
          text-decoration: none;
          font-size: inherit;
          font-family: inherit;
          font-weight: inherit;
          line-height: inherit;
        }
        .btn-primary table td:hover {
          background-color: #00877b !important; 
        }
        .btn-primary a:hover {
          background-color: #00877b !important;
          border-color: #00877b !important; 
        } 
      }

    </style>
  </head>
  <body>
    <!--<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>-->
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="background-color:rgb(246, 246, 246);">
      <tr>
        <td>&nbsp;</td>
        <td class="container" width="580" style="width:580px;background-color: white;">
            <div class="content">
              
            <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main">
            
              <!-- START MAIN CONTENT AREA -->
              @yield('content')

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->

            <!-- START FOOTER -->
           
              <table class="footer" width="100%" role="presentation" border="0" cellpadding="0" cellspacing="0" style="width: 100%!important;">
                <tr>
                  <td class="social">
                    <hr>
                    <a style="margin: 4px!important;display: inline-block!important;" href="https://www.instagram.com/viavoxinteractive" target="_blank"><img style="margin: 2px!important;" width="30" src="https://www.viavoxexperience.com/images/socialmail/instagram.png" title="Instagram" alt="Instagram"/></a>
                    &nbsp;&nbsp;
                    <a style="margin: 4px!important;display: inline-block!important;" href="http://www.linkedin.com/companies/viavox-interactive" target="_blank"><img style="margin: 2px!important;" width="30" src="https://www.viavoxexperience.com/images/socialmail/linkedin.png" title="Linkedin" alt="Linkedin"/></a>
                    &nbsp;&nbsp;
                    <a style="margin: 4px!important;display: inline-block!important;" href="https://www.facebook.com/viavox" target="_blank"><img width="30" style="margin: 2px!important;" src="https://www.viavoxexperience.com/images/socialmail/facebook.png" title="Facebook" alt="Facebook"/></a>
                    &nbsp;&nbsp;
                    <a style="margin: 4px!important;display: inline-block!important;" href="https://twitter.com/viavox" target="_blank"><img width="30" style="margin: 2px!important;" src="https://www.viavoxexperience.com/images/socialmail/twitter.png" title="Twitter" alt="Twitter"/></a>
                  </td>
                </tr>
                <tr>
                  <td class="content-block">
                    <a href="https://www.viavoxexperience.com/es/cookies" rel="nofollow">Política de cookies</a> | <a href="https://www.viavoxexperience.com/es/privacy" rel="nofollow">Condiciones de privacidad</a> | <a href="https://www.viavoxexperience.com/es/policy" rel="nofollow">Política de confidencialidad</a> | <a href="https://www.viavoxexperience.com/es/legal" rel="nofollow">Aviso legal</a>
                  </td>
                </tr>
                <tr>
                  <td class="content-block powered-by">
                    © Viavox Experience 2020
                  </td>
                </tr>
              </table>
            
            <!-- END FOOTER -->

          </div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>