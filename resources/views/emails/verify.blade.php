@extends('emails.layouts.index')

@section('title', $title)

@section('content')
<tr>  
    <td class="wrapper" style="padding: 30px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <a href="https://www.viavoxexperience.com">
                    <img width="150" src="https://www.viavoxexperience.com/images/logo_viavox_experience.png"/>
                </a>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td>
            <h2 style="color:#00877b;margin-bottom:15px">Verificación de email</h2>
            <p>Pulsa el siguiente link verificar tu email</p>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                <tbody>
                <tr>
                    <td align="left">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td> 
                                <a href="{{$body}}" target="_blank">Confirmar email</a> 
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <p>Si el link de verificación no funciona, prueba a copiar el link en la barra de direcciones de tu navegador.</p>
            </td>
        </tr>
        </table>
    </td>
</tr>
@endsection