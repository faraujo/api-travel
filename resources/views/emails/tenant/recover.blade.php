@extends('emails.tenant.layouts.index')

@section('title', $title)

@section('content')
<tr>  
    <td class="wrapper" style="padding: 30px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <a>
                    <img width="150"src="{{$client_logo}}"/>
                </a>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td>
            <h2 style="color:#00877b;margin-bottom:15px">Restablecer contraseña</h2>
            <p>Hemos recibido una petición para restablecer la contraseña asociada a esta dirección de correo. Si has realizado esta petición, sigue las instrucciones.</p>
            <p>Pulsa el siguiente link para recuperar tu contraseña. El link estará activo durante 60 minutos:</p>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                <tbody>
                <tr>
                    <td align="left">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td> 
                                <a href="{{$body}}" target="_blank">Restablecer contraseña</a> 
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <p>Si no has solicitado la recuperación de contraseña, ignora este email. Tu cuenta está segura y no es necesario que realices ningun cambio.</p>
            <p>Si el link de recuperación no funciona, prueba a copiar el link en la barra de direcciones de tu navegador.</p>
            </td>
        </tr>
        </table>
    </td>
</tr>
@endsection

