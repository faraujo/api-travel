@extends('emails.tenant.layouts.index')

@section('title', $title)

@section('content')
<tr>  
    <td class="wrapper" style="padding: 30px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <a>
                    <img width="150" src="{{$client_logo}}"/>
                </a>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td>
                <h2>Email de información</h2>
                {!! $body !!}
            </td>
        </tr>
        </table>
    </td>
</tr>
@endsection