@extends('emails.layouts.index')

@section('title', $title)

@section('content')
<tr>  
    <td class="wrapper" style="padding: 30px;">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <a href="https://www.viavoxexperience.com">
                    <img width="150" src="https://www.viavoxexperience.com/images/logo_viavox_experience.png"/>
                </a>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td>
            <h2 style="color:#00877b;margin-bottom:15px">Bienvenido</h2>
            {!! $body !!}
            </td>
        </tr>
        </table>
    </td>
</tr>
@endsection