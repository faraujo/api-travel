<?php

return [
    'cotizacion_cliente' => 'Cotización',
    'confirmacion_compra_cliente' => 'Confirmación de compra',
    'estimado_a' => 'Estimado/a',
    'amable_solicitud_cotizacion' => 'De acuerdo a su amable solicitud le enviamos el detalle de su cotización',
    'amable_solicitud_compra' => 'De acuerdo a su amable solicitud le enviamos el detalle de confirmación de su compra',
    'gracias_por_contactarnos' => '¡Gracias por contactarnos!',
    'gracias_por_su_compra' => '¡Muchas gracias por su compra!',
    'comprar_ahora' => '¡COMPRAR AHORA!',
    'nombre_del_visitante' => 'Nombre del visitante: ',
    'cotizacion' => 'Cupón',
    'servicio_sin_transporte' => 'Servicio sin transporte',
    'visitantes' => 'Visitantes',
    'fecha_visita' => 'Fecha Visita',
    'importe' => 'Importe',
    'subtotal' => 'Subtotal',
    'total' => 'Total',
    'total_not_paid' => 'Pendiente de pago',
    'tarifa_aviso' => '* Tarifas y disponibilidad sujetas a cambios sin previo aviso.',
    'sustitucion' => 'Sustitución',
    'imprimir_boleto' => '¡Imprime aquí tu PDF! '

];