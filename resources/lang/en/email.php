<?php

return [
    'cotizacion_cliente' => 'EN Cotización Cliente',
    'confirmacion_compra_cliente' => 'EN Confirmación de compra Cliente',
    'estimado_a' => 'EN Estimado/a',
    'amable_solicitud_cotizacion' => 'EN De acuerdo a su amable solicitud le enviamos el detalle de su cotización',
    'amable_solicitud_compra' => 'EN De acuerdo a su amable solicitud le enviamos el detalle de confirmación de su compra',
    'gracias_por_contactarnos' => 'EN ¡Gracias por contactarnos!',
    'gracias_por_su_compra' => 'EN ¡Muchas gracias por su compra!',
    'comprar_ahora' => 'EN ¡COMPRAR AHORA!',
    'nombre_del_visitante' => 'EN Nombre del visitante: ',
    'cotizacion' => 'EN Cupón',
    'servicio_sin_transporte' => 'EN Servicio sin transporte',
    'visitantes' => 'EN Visitantes',
    'fecha_visita' => 'EN Fecha Visita',
    'importe' => 'EN Importe',
    'subtotal' => 'EN Subtotal',
    'total' => 'EN Total',
    'total_not_paid' => 'EN Pendiente de pago',
    'tarifa_aviso' => 'EN * Tarifas y disponibilidad sujetas a cambios sin previo aviso.',
    'sustitucion' => 'Sustitution',
    'imprimir_boleto' => 'Print your ticket! '


];