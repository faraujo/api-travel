<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class WebLegalUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations en formato incorrecto
     */
    public function testBad1()
    {

        $legalFactory = factory(App\Models\tenant\WebLegal::class)->create([
            'type' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/legal', [
            'id' => $legalFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "translation"=>["you need at least one translation"]
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations inexistentes
     */
    public function testOk1()
    {
        $legalFactory = factory(App\Models\tenant\WebLegal::class)->create([
            'type' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/legal', [
            'id' => $legalFactory->id,
            'legal' => ['ES' => 'cualquiera español']
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_web_legal_translation', [
            'legal_id' => $legalFactory->id,
            'language_id' => 1,
            'legal' => 'cualquiera español'
        ]);
    }
}