<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class WebLegalShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta legal con parametro lang mal
     */
    public function testBad1()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/legal', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta productos con parametro lang
     */
    public function testOk1()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/legal', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta legales con parametro lang en mayusculas
     */
    public function testOk2()
    {
        //Se especifica parametro lang correctamente en mayusculas
        $this->json('GET', '/api/v1/legal', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    public function testOk3()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/legal', [
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }
}