<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class WebLegalShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta legal inexistente sin parametros
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/legal/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta producto existente sin parametros
     */
    public function testOk1()
    {

        $LegalFactory = factory(App\Models\tenant\WebLegal::class)->create([
            'type' => 'test',
        ]);
        $translation1 = factory(App\Models\tenant\WebLegalTranslation::class)->create([
            'language_id' => 1,
            'legal_id' => $LegalFactory->id,
        ]);
        factory(App\Models\tenant\WebLegalTranslation::class)->create([
            'language_id' => 2,
            'legal_id' => $LegalFactory->id,
        ]);
        factory(App\Models\tenant\WebLegalTranslation::class)->create([
            'language_id' => 3,
            'legal_id' => $LegalFactory->id,
        ]);

        $this->json('GET', '/api/v1/legal/' . $LegalFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $LegalFactory->id,
                'type' => $LegalFactory->type
            ]);
    }
}