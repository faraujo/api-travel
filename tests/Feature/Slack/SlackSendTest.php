<?php

namespace Tests\Feature;

use App\Notifications\SlackNotification;
use App\Notifications\SlackNotificationInfo;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SlackSendTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba funcionamiento de las validaciones
     * No se especifican los parámetros requeridos from y content.
     */

    public function testBad1()
    {

        $this->json('POST', '/api/v1/slack', [
        ])
            ->assertExactJson(['error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "content" => ["The content field is required."],
                "from" => ["The from field is required."],
            ]]
            ]);

    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     * Se especifican los parámetros requeridos from y content. Y se incluyen parámetros space y attachment no válidos.
     */

    public function testBad2()
    {

        $this->json('POST', '/api/v1/slack', [
            'from' => 'from microservicio para pruebas',
            'content' => 'contenido del mensaje para pruebas',
            'space' => 'espacio de Slack incorrecto',
            'attachment' => 'adjunto sin array'
        ])
            ->assertExactJson(['error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "space" => ["Space not available"],
                "attachment" => ["The attachment must be an array."],
            ]]
            ]);

    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     * Se especifican los parámetros requeridos from y content. Resto de parámetros válidos, pero con type incorrecto.
     */

    public function testBad3()
    {

        $this->json('POST', '/api/v1/slack', [
            'from' => 'from microservicio para pruebas',
            'content' => 'contenido del mensaje para pruebas',
            'space' => 'default',
            'attachment[title]' => 'titulo del adjunto',
            'type' => 'tipo incorrecto'
        ])
            ->assertExactJson(['error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "type" => ["The selected type is invalid."],
            ]]
            ]);

    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     * Validación sin errores.
     */

    public function testOK1()
    {
        Notification::fake();

        $this->json('POST', '/api/v1/slack', [
            'from' => 'from microservicio para pruebas',
            'content' => 'contenido del mensaje para pruebas',
            'space' => 'default',
            'attachment[title]' => 'titulo del adjunto',
            'attachment[content]' => 'contenido del adjunto',
            'type' => 'info'
        ])
            ->assertExactJson(['error' => 200
            ]);

    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     * Validación sin errores.
     */

    public function testOK2()
    {
        Notification::fake();

        $this->json('POST', '/api/v1/slack', [
            'from' => 'from microservicio para pruebas',
            'content' => 'contenido del mensaje para pruebas',
            'space' => 'default',
            'channel' => ['#viavox_travel_mtr_noti'],
            'attachment[title]' => 'titulo del adjunto',
            'attachment[content]' => 'contenido del adjunto',
            'type' => 'info'
        ])
            ->assertExactJson(['error' => 200
            ]);

    }

}

