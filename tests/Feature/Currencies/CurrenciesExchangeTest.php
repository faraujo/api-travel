<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CurrenciesExchangeTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test no manda ninguno de los parámetros requeridos
     */
    public function testBad1()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'password' => bcrypt('test'),
        ]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);

        $this->json('POST', '/api/v1/currency/exchange', [
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["currency_id"=>["The currency id field is required."],
                "exchange"=>["The exchange field is required."]]]]);
    }

    /**
     * Test manda todos los parámetros posibles en formato incorrecto
     */
    public function testBad2()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'password' => bcrypt('test'),
        ]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);

        $this->json('POST', '/api/v1/currency/exchange', [
            'currency_id' => 'caracteres',
            'exchange' => 'caracteres',
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[
                ["currency_id"=>["The currency id must be an integer."],
                "exchange"=>["The exchange must be a number."],
                ]]]);
    }

    /**
     * Test manda currency_id inexistente
     */
    public function testBad3()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'password' => bcrypt('test'),
        ]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);

        $this->json('POST', '/api/v1/currency/exchange', [
            'currency_id' => 999999999999,
            'exchange' => 0.5,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["currency_id"=>["The selected currency id is invalid."]]]]);
    }


    /**
     * Test envío sin token
     */
    public function testBad4()
    {

        $currencyFactory = factory(App\Models\tenant\Currency::class)->create();

        $this->json('POST', '/api/v1/currency/exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.5,

        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["Authorization"=>["The authorization token is required"]]]]);
    }

    /**
     * Test crea exchange y comprueba su creación en tabla mo_currency_exchange
     */
    public function testOk1()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'password' => bcrypt('test'),
        ]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);

        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);
        $this->assertDatabaseHas('mo_currency', [
            'id' => $currencyFactory->id,
        ]);

        $this->json('POST', '/api/v1/currency/exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.5,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.5,
            'user_id' => $userFactory->id,
            'date_time_start' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Test crea exchange, comprueba su creación en tabla mo_currency_exchange y manda creación de exchange con mismo currency y misma fecha, borra registro existente y crea otro con el nuevo cambio
     */
    public function testOk2()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'password' => bcrypt('test'),
        ]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);

        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);
        
        $fecha1 = Carbon::now()->format('Y-m-d H:i:s');

        $this->json('POST', '/api/v1/currency/exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.5,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.5,
            'user_id' => $userFactory->id,
            'date_time_start' => $fecha1,
        ]);

        //manda diferente exchange para la misma fecha y hora que ya había un valor de moneda
        $fecha2 = Carbon::now()->format('Y-m-d H:i:s');

        $this->json('POST', '/api/v1/currency/exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.7,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson(["error" => 200]);

        //comprueba que el registro que había primero se ha borrado
        $this->assertDatabaseMissing('mo_currency_exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.5,
            'user_id' => $userFactory->id,
            'date_time_start' => $fecha1,
            'deleted_at' => null
        ]);

        //comprueba registro con datos nuevos
        $this->assertDatabaseHas('mo_currency_exchange', [
            'currency_id' => $currencyFactory->id,
            'exchange' => 0.7,
            'user_id' => $userFactory->id,
            'date_time_start' => $fecha2,
        ]);
    }



}