<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CurrenciesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta monedas con parámetros de paginación en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/currency/', [
            'page' => 'caracteres',
            'limit' => 'caracteres',
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [["limit" => ["The limit must be an integer."],
                "page" => ["The page must be an integer."]]]]);
    }

    /**
     * Test consulta monedas y comprueba estructura de salida de datos
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/currency', [
        ])
            ->assertJsonStructure(['error', 'data' => ["0" => ['currency','main_currency']],'total_results']);
    }

    /**
     * Test crea factory y comprueba que se muestra la moneda
     */
    public function testOk2()
    {
        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $this->json('GET', '/api/v1/currency', [
        ])
            ->assertJsonFragment([
                'name' => $currencyFactory->name,
                'simbol' => $currencyFactory->simbol,
                'iso_number' => $currencyFactory->iso_number,
                'iso_code' => $currencyFactory->iso_code,
                'presentation_simbol' => $currencyFactory->presentation_simbol,
                'position' => $currencyFactory->position,
                'decimal_separator' => $currencyFactory->decimal_separator,
                'thousand_separator' => $currencyFactory->thousand_separator,
            ]);
    }

}