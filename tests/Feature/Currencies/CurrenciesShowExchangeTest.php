<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CurrenciesShowExchangeTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de registros de conversión con parámetros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/currency/exchange', [
            'currency_id' => 'string',
            'user_id' => 'string',
            'date_time' => 'string',
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["currency_id"=>["The currency id must be an integer."],
                "date_time"=>["The date time does not match the format Y-m-d H:i:s.","The date time is not a valid date."],
                "limit"=>["The limit must be an integer."],"page"=>["The page must be an integer."],"user_id"=>["The user id must be an integer."]]]]);
    }

    /**
     * test consulta de registros de conversión con parametro date_time y sin enviar currency_id que se necesita con este filtro
     */
    public function testBad2()
    {

        $this->json('GET', '/api/v1/currency/exchange', [
            'date_time' => '2018-10-08 09:00:00',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["currency_id"=>["The currency id field is required when date time is present."]]]]);
    }

    /**
     * test consulta de registros de conversión con parametro date_time en formato date pero no date_time
     */
    public function testBad3()
    {
        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $this->json('GET', '/api/v1/currency/exchange', [
            'date_time' => '2018-10-08',
            'currency_id' => $currencyFactory->id,
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
        "error_inputs"=>[["date_time"=>["The date time does not match the format Y-m-d H:i:s."]]]]);
    }


    /**
     * test crea factory de moneda, crea factory de conversión y comprueba que se muestra en la salida
     */
    public function testOk1()
    {
        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $exchangeFactory = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
            'date_time_start' => '2099-12-10 09:00:00',
        ]);

        $this->assertDatabaseHas('mo_currency', [
            'id' => $currencyFactory->id,
        ]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'id' => $exchangeFactory->id,
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
        ]);


        $this->json('GET', '/api/v1/currency/exchange', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $currencyFactory->id,
                'user_id' => $userFactory->id,
            ]);
    }

    /**
     * test prueba filtro user_id
     */
    public function testOk2()
    {
        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        //crea 2 factories y filtra por uno de ellos, se comprueba que viene el usuario en la salida y que sólo viene un resultado
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $userFactory2 = factory(App\Models\tenant\User::class)->create([]);

        $exchangeFactory = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
            'date_time_start' => '2099-12-10 09:00:00',
        ]);

        $this->assertDatabaseHas('mo_currency', [
            'id' => $currencyFactory->id,
        ]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'id' => $exchangeFactory->id,
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
        ]);


        $this->json('GET', '/api/v1/currency/exchange', ['user_id' => $userFactory->id])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $currencyFactory->id,
                'user_id' => $userFactory->id,
                'total_results' => 1
            ]);

        $this->json('GET', '/api/v1/currency/exchange', ['user_id' => $userFactory2->id])
            ->assertExactJson(["data"=>[],"error"=>200,"total_results"=>0]);
    }

    /**
     * test prueba filtro currency_id
     */
    public function testOk3()
    {
        //crea 2 factories de moneda y filtra por una de ellos, se comprueba que viene la moneda en la salida y que sólo viene un resultado
        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $currencyFactory2 = factory(App\Models\tenant\Currency::class)->create([]);

        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $exchangeFactory = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
            'date_time_start' => '2099-12-10 09:00:00',
        ]);

        $this->assertDatabaseHas('mo_currency', [
            'id' => $currencyFactory->id,
        ]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'id' => $exchangeFactory->id,
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
        ]);

        $this->json('GET', '/api/v1/currency/exchange', ['currency_id' => $currencyFactory->id])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $currencyFactory->id,
                'user_id' => $userFactory->id,
                'total_results' => 1
            ]);

        $this->json('GET', '/api/v1/currency/exchange', ['currency_id' => $currencyFactory2->id])
            ->assertExactJson(["data"=>[],"error"=>200,"total_results"=>0]);
    }

    /**
     * test prueba filtro date_time
     */
    public function testOk4()
    {
        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        //crea 3 factories de conversión de moneda y filtra por una fecha mayor que las 3, debe mostrar el registro inmediatamente anterior a la fecha enviada
        $exchangeFactory1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
            'date_time_start' => '2099-12-10 09:00:00',
            'exchange' => 0.7
        ]);

        $exchangeFactory2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
            'date_time_start' => '2099-12-15 09:00:00',
            'exchange' => 0.8
        ]);

        $exchangeFactory3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
            'date_time_start' => '2099-12-20 09:00:00',
            'exchange' => 0.9
        ]);

        $this->assertDatabaseHas('mo_currency', [
            'id' => $currencyFactory->id,
        ]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'id' => $exchangeFactory1->id,
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
        ]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'id' => $exchangeFactory2->id,
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
        ]);

        $this->assertDatabaseHas('mo_currency_exchange', [
            'id' => $exchangeFactory3->id,
            'currency_id' => $currencyFactory->id,
            'user_id' => $userFactory->id,
        ]);

        //devuelve reegistro con fecha igual o inmediatamente inferior a la enviada
        $this->json('GET', '/api/v1/currency/exchange', [
            'date_time' => '2099-12-22 09:00:00','currency_id' => $currencyFactory->id])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $exchangeFactory3->id,
                'total_results' => 1
            ]);

        //devuelve reegistro con fecha igual o inmediatamente inferior a la enviada
        $this->json('GET', '/api/v1/currency/exchange', [
            'date_time' => '2099-12-17 09:00:00','currency_id' => $currencyFactory->id])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $exchangeFactory2->id,
                'total_results' => 1
            ]);

        //devuelve reegistro con fecha igual o inmediatamente inferior a la enviada
        $this->json('GET', '/api/v1/currency/exchange', [
            'date_time' => '2099-12-12 09:00:00','currency_id' => $currencyFactory->id])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $exchangeFactory1->id,
                'total_results' => 1
            ]);

    }
}