<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ReadLanguageTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta idiomas con parámetro front incorrecto
     */
    public function testBad1()
    {
        // Se especifica incorrectamente parámetro front

        $this->json('GET', '/api/v1/language', [
            'front' => 'abc'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta idiomas con parámetro back incorrecto
     */
    public function testBad2()
    {
        // Se especifica incorrectamente parámetro back

        $this->json('GET', '/api/v1/language', [
            'back' => '9'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta idiomas sin parametros
     */
    public function testOk1()
    {
        //Petición sin parámetros
        $this->json('get', '/api/v1/language')->assertJsonStructure([
            'error',
            'data' => ["0" => ['language']],
        ]);
    }

    /**
     * Test consulta idiomas con parámetro front
     */
    public function testOk2()
    {
        //Petición con parámetro front
        $this->json('GET', '/api/v1/language', [
            'front' => '1'
        ])->assertJsonStructure(['error', 'data' => ["0" => ['language']]]);
    }

    /**
     * Test consulta idiomas con parámetro back
     */
    public function testOk3()
    {
        //Petición con parámetro back
        $this->json('GET', '/api/v1/language', [
            'back' => '0'
        ])->assertJsonStructure(['error', 'data' => ["0" => ['language']]]);
    }

    /**
     * Test consulta idiomas con todos los parámetros
     */
    public function testOk4()
    {
        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST', 'back' => 0, 'front' => 1]);

        //Petición con todos los parámetros
        $this->json('GET', '/api/v1/language', [
            'back' => $languageFactory->back,
            'front' => $languageFactory->front,
        ])->assertJsonStructure(['error', 'data' => ["0" => ['language']]]);
    }

}