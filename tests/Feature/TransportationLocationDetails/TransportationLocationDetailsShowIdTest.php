<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationDetailsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
   

    /**
     * Consulta parada inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/transportationlocationstop/999999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar parada existente
     */
    public function testOk2()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        $transportationlocationdetailFactory = factory(\App\Models\tenant\TransportationLocationDetail::class)->create([
            'transportation_location_id' => $pickupFactory->id,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $this->json('GET', '/api/v1/transportationlocationstop/' . $transportationlocationdetailFactory->id, [])
        ->assertExactJson(['error' => 200, 'data' => [[
            'transportationlocationstop' => [[
                'date' => $transportationlocationdetailFactory->date,
                'id' => $transportationlocationdetailFactory->id,
                'name' => $transportationlocationdetailFactory->name,
                'code' => $transportationlocationdetailFactory->code,
                'transportation_location_id' => $transportationlocationdetailFactory->transportation_location_id,
                'product_id' => $transportationlocationdetailFactory->product_id,
                'session' => $transportationlocationdetailFactory->session,
                'subchannel_id' => $transportationlocationdetailFactory->subchannel_id,
                ]]]
        ]]);
    }
}