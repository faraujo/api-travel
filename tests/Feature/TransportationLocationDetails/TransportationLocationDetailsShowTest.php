<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationDetailsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta paradas con parámetro transportation_location_id incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/transportationlocationstop', [
            'transportation_location_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"])
        ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "transportation_location_id" => ["The transportation location id must be an integer."]
            ]
        ]]);
    }

    /**
     * Test consulta paradas con parámetro product_id incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/transportationlocationstop', [
            'product_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs'])
        ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "product_id" => ["The product id must be an integer."]
            ]
        ]]);
    }

    /**
     * Test consulta tags con parámetro subchannel_id incorrecto
     */
    public function testBad3()
    {
        $this->json('GET', '/api/v1/transportationlocationstop', [
            'subchannel_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs'])
        ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "subchannel_id" => ["The subchannel id must be an integer."]
            ]
        ]]);
    }

    /**
     * Test consulta paradas con parámetros incorrectos
     */
    public function testBad4()
    {
        $this->json('GET', '/api/v1/transportationlocationstop', [
            'transportation_location_id' => 'es',
            'product_id' => 'es',
            'subchannel_id' => 'es',
            'limit' => 'afaf',
            'page' => 'asdf'
        ])->assertJsonStructure(['error', 'error_description'])
        ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "transportation_location_id" => ["The transportation location id must be an integer."],
                "product_id" => ["The product id must be an integer."],
                "subchannel_id" => ["The subchannel id must be an integer."],
                "limit" => ["The limit must be a number."],
                "page" => ["The page must be a number."],
            ]
        ]]);
    }

    /**
     * Test consulta paradas con parámetro transportation_location_id inexistente
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/transportationlocationstop', [
            'transportation_location_id' => '999999999999999',
        ])->assertExactJson(['data' => [], 'error' => 200, "total_results" => 0,]);
    }

    /**
     * Test consulta paradas con parámetro product_id inexistente
     */
    public function testOk2()
    {
        $this->json('GET', '/api/v1/transportationlocationstop', [
            'product_id' => '999999999999999',
        ])->assertExactJson(['data' => [], 'error' => 200, "total_results" => 0,]);
    }

    /**
     * Test consulta parada existente en base de datos
     */
    public function testOk3()
    {
        //crea parada
        $transportationlocationdetailFactory = factory(\App\Models\tenant\TransportationLocationDetail::class)->create([]);

        $this->json('GET', '/api/v1/transportationlocationstop', [
        ])->assertJsonStructure(['error', 'data' => [], 'total_results'])
        ->assertJsonFragment(['id' => $transportationlocationdetailFactory->id]);
    }

    /**
     * Test consulta parada existente en base de datos con filtros
     */
    public function testOk4()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        //crea parada
        $transportationlocationdetailFactory = factory(\App\Models\tenant\TransportationLocationDetail::class)->create([
            'transportation_location_id' => $pickupFactory->id,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('GET', '/api/v1/transportationlocationstop', [
            'transportation_location_id' => $pickupFactory->id,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ])->assertJsonStructure(['error', 'data' => [], 'total_results'])
        ->assertJsonFragment(['total_results' => 1])
        ->assertJsonFragment(['id' => $transportationlocationdetailFactory->id]);

        $transportationlocationdetailFactory->delete();

        $this->json('GET', '/api/v1/transportationlocationstop',[
            'transportation_location_id' => $pickupFactory->id,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ])->assertJsonStructure(['error', 'data' => [], 'total_results'])->assertJsonFragment(['total_results' => 0]);

    }
   
}