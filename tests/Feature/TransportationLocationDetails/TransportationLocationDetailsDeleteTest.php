<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationDetailsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/transportationlocationstop', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/transportationlocationstop', [
            'ids' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/transportationlocationstop', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' => [["ids.0" => ["The selected ids.0 is invalid."]]]
            ]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {
        $transportationlocationdetailFactory = factory(\App\Models\tenant\TransportationLocationDetail::class)->create([]);

        $this->assertDatabaseHas('mo_transportation_location_detail', [
            'id' => $transportationlocationdetailFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/transportationlocationstop', [
            'ids' => $transportationlocationdetailFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }


    /**
     * Test para eliminar una parada que crea primero un factory
     */
    public function testOk1()
    {

        $transportationlocationdetailFactory = factory(\App\Models\tenant\TransportationLocationDetail::class)->create([]);

        $this->assertDatabaseHas('mo_transportation_location_detail', [
            'id' => $transportationlocationdetailFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/transportationlocationstop', [
            'ids' => [$transportationlocationdetailFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_transportation_location_detail el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_transportation_location_detail', ['deleted_at' => null, 'id' => $transportationlocationdetailFactory->id]);
        
    }
  

}