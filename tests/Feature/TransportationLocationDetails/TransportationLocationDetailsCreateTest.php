<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationDetailsCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía ningún campo
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/transportationlocationstop', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "name" => ["The name field is required."],
                "code" => ["The code field is required."],
                "transportation_location_id" => ["The transportation location id field is required."],
                "session" => ["The session field is required."]
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos en formato incorrecto (envío de strings en campos de id y formato fecha)
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/transportationlocationstop', [
            'name' => 'nombre prueba',
            'code' => 'código prueba',
            'transportation_location_id' => 'localización transporte prueba',
            'product_id' => 'producto prueba',
            'subchannel_id' => 'subcanal prueba',
            'date' => 'fecha prueba',
            'session' => 'sesión prueba'])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "transportation_location_id" => ["The transportation location id must be an integer."],
                "product_id" => ["The product id must be an integer."],
                "subchannel_id" => ["The subchannel id must be an integer."],
                "date" => ["The date does not match the format Y-m-d.","The date is not a valid date."],
                "session" => ["The session does not match the format H:i."],
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Se mandan campos en formato incorrecto (envío de ids negativos y formatos de fecha incorrectos)
     */
    public function testBad3()
    {

        $this->json('POST', '/api/v1/transportationlocationstop', [
            'name' => 'nombre prueba',
            'code' => 'código prueba',
            'transportation_location_id' => -1,
            'product_id' => -1,
            'subchannel_id' => -1,
            'date' => '2019/10/01',
            'session' => '10'])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "transportation_location_id" => ["The transportation location id must be at least 0."],
                "product_id" => ["The product id must be at least 0."],
                "subchannel_id" => ["The subchannel id must be at least 0."],
                "date" => ["The date does not match the format Y-m-d."],
                "session" => ["The session does not match the format H:i."],
            ]]
            ]);
    }

    /**
     * Realiza el test de creación de parada de la locación (con todos los campos)
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK1()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        $this->json('POST', '/api/v1/transportationlocationstop', [
            'name' => 'nombre prueba',
            'code' => 'código prueba',
            'transportation_location_id' => $pickupFactory->id,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'date' => '2019-10-01',
            'session' => '10:00'])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_transportation_location_detail', [
            'name' => 'nombre prueba',
            'code' => 'código prueba',
            'transportation_location_id' => $pickupFactory->id,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'date' => '2019-10-01',
            'session' => '10:00'
        ]);

    }

    /**
     * Realiza el test de creación de parada de la locación (solo con campos requeridos)
     * Comprueba que se ha insertado correctamente en base de datos
     *
     */
    public function testOK2()
    {
        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        $this->json('POST', '/api/v1/transportationlocationstop', [
            'name' => 'nombre prueba',
            'code' => 'código prueba',
            'transportation_location_id' => $pickupFactory->id,
            'date' => '2019-10-01',
            'session' => '10:00'])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_transportation_location_detail', [
            'name' => 'nombre prueba',
            'code' => 'código prueba',
            'transportation_location_id' => $pickupFactory->id,
            'date' => '2019-10-01',
            'session' => '10:00'
        ]);
    }
    
}