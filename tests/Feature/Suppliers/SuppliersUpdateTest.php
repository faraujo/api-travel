<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SuppliersUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía ningún campo
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/supplier', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "id" => ["The id field is required."],
                "name" => ["The name field is required."]
            ]]
            ]);
    }

    /**
     * Realiza el test de creación de proveedor
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK1()
    {

        $supplierFactory = factory(\App\Models\tenant\Supplier::class)->create([]);

        $this->json('PUT', '/api/v1/supplier', [
            'id' => $supplierFactory->id,
            'name' => 'nombre prueba'])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_supplier', [
            'id' => $supplierFactory->id,
            'name' => 'nombre prueba'
        ]);

    }
}