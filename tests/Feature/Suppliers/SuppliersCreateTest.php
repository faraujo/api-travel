<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SuppliersCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía ningún campo
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/supplier', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "name" => ["The name field is required."]
            ]]
            ]);
    }

    /**
     * Realiza el test de creación de proveedor
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK1()
    {
        $this->json('POST', '/api/v1/supplier', [
            'name' => 'nombre prueba'])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_supplier', [
            'name' => 'nombre prueba'
        ]);

    }
}