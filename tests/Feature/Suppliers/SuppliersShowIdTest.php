<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SuppliersShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
   

    /**
     * Consulta proveedor inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/supplier/999999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar proveedor existente
     */
    public function testOk2()
    {
        $supplierFactory = factory(\App\Models\tenant\Supplier::class)->create([]);
        $this->json('GET', '/api/v1/supplier/' . $supplierFactory->id, [])
        ->assertExactJson(['error' => 200, 'data' => [[
            'supplier' => [[
                'id' => $supplierFactory->id,
                'name' => $supplierFactory->name
            ]]]
        ]]);
    }
}