<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SuppliersShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de proveedores con parámetros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/supplier', [
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."]]]]);
    }


    /**
     * test crea factory de suppplier y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        $supplierFactory = factory(App\Models\tenant\Supplier::class)->create([
            'name' => 'Supplier test',
        ]);

        $this->assertDatabaseHas('mo_supplier', [
            'id' => $supplierFactory->id,
        ]);

        $this->json('GET', '/api/v1/supplier', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $supplierFactory->id,
                'name' => $supplierFactory->name,

            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/supplier', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['supplier' => ["0" => ['id', 'name']]]]
            ]);

    }


}