<?php

namespace Tests\Feature;

use App\Models\tenant\PasswordHistory;
use App\Models\tenant\PasswordToken;
use App\Models\tenant\Settings;
use App\Models\tenant\User;
use App\Models\tenant\UserToken;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class LoginRecoverTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin envío de parámetros
     */
    public function testBad1()
    {

        $userFactory = factory(User::class)->create([]);

        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addHours(1),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["c_password"=>["The c password field is required."],"password"=>["The password field is required."]]]]);
    }

    /**
     *  Prueba automatizada enviando password en formato incorrecto
     */
    public function testBad2()
    {

        $userFactory = factory(User::class)->create([]);

        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addHours(1),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passwordincorrecto',
            'c_password' => 'passwordincorrecto',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["The password format is invalid."]]]]);
    }

    /**
     * Prueba automatizada enviando password sin parámetro repetición del password
     */
    public function testBad3()
    {

        $userFactory = factory(User::class)->create([]);

        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addHours(1),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passDA1!',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["c_password"=>["The c password field is required."]]]]);
    }

    /**
     * Intenta restablecer contraseña con el token ya espirado
     */
    public function testBad4()
    {
        $userFactory = factory(User::class)->create([]);

        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->subHour(1),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passDA1!',
            'c_password' => 'passDA1!',
        ])
            ->assertExactJson(['error' => 401, 'error_description' => 'Unauthorized']);
    }

    /**
     * Crea 5 password y los almacena en el histórico, comprueba que las 4 últimas introducidas no se las permite volver a poner al usuario.
     */
    public function testBad5()
    {
        $userFactory = factory(User::class)->create([
            'password' => bcrypt('passSegura1!'),
            'email' => 'prueba5@viavox.com',
        ]);

        factory(PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura2!'),
        ]);

        factory(PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura3!'),
        ]);

        factory(PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura4!'),
        ]);

        factory(PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura5!'),
        ]);

        $time_token = Settings::where('name','recover_token_validate')->first()->value;


        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addMinutes($time_token),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passSegura5!',
            'c_password' => 'passSegura5!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passSegura4!',
            'c_password' => 'passSegura4!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passSegura3!',
            'c_password' => 'passSegura3!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passSegura2!',
            'c_password' => 'passSegura2!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passSegura1!',
            'c_password' => 'passSegura1!',
        ])
            ->assertExactJson(['error' => 200]);
    }

    /**
     * Intenta introducir password con espacios
     */
    public function testBad6()
    {

        $userFactory = factory(User::class)->create([
            'password' => bcrypt('passSegura1!'),
            'email' => 'prueba5@viavox.com',
        ]);

        $time_token = Settings::where('name','recover_token_validate')->first()->value;

        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addMinutes($time_token),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'daMar 11!',
            'c_password' => 'daMar 11!',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);
    }

    public function testBad7(){

        $userFactory = factory(User::class)->create([
            'password' => bcrypt('passSegura1!'),
            'email' => 'prueba5@viavox.com',
        ]);


        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->subMinute(),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
        ])
            ->assertExactJson(["error"=>401,"error_description"=>"Unauthorized"]);

    }

    /**
     * Intenta introducir password con acentos
     */
    public function testBad8()
    {

        $userFactory = factory(User::class)->create([]);

        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addHour(1),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'daMar11!á',
            'c_password' => 'daMar11!á',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'daMar11!Á',
            'c_password' => 'daMar11!Á',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);

    }

    /**
     * Restablece contraseña correctamente y comprueba que el usuario se ha actualizado en ese instante y que se ha borrado el token que tenía
     * para restablecer password
     */
    public function testOk1()
    {
        $userFactory = factory(User::class)->create([]);

        $userRecoverFactory = factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addHour(1),
        ]);

        $this->assertDatabaseHas('mo_user_recover_token', ['token' => $userRecoverFactory->token,'user_id' => $userFactory->id]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passDA1!',
            'c_password' => 'passDA1!',
        ])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_user_recover_token', ['user_id' => $userFactory->id, 'deleted_at' => Carbon::now()]);

    }

    /**
     * Hace login y después pide restablecer contraseña, comprueba que al cambiar password se borra tanto el token de restablecer password como el token
     * de usuario que tenía al hacer login. Simula así múltiples entornos, podría el usuario ir a hacer login desde su teléfono y tener ya una sesión abierta en
     * su ordenador
     */
    public function testOk2()
    {
        $userFactory = factory(User::class)->create([]);

        //crea token de usuario para simular que ha hecho login
        factory(UserToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addHour(1),
        ]);

        //crea token de password para simular que ha pedido un cambio de password
        factory(PasswordToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'token',
            'expired_at' => Carbon::now()->addHour(1),
        ]);

        $this->json('POST', '/api/v1/login/recover?token=token', [
            'password' => 'passDA1!',
            'c_password' => 'passDA1!',
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_user', ['updated_at' => Carbon::now(),'id' => $userFactory->id]);

        $this->assertDatabaseHas('mo_user_token', ['user_id' => $userFactory->id, 'deleted_at' => Carbon::now()]);

        $this->assertDatabaseHas('mo_user_recover_token', ['user_id' => $userFactory->id, 'deleted_at' => Carbon::now()]);

    }




}