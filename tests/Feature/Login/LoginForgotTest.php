<?php

namespace Tests\Feature;

use App\Models\tenant\PasswordToken;
use App\Models\tenant\Settings;
use App\Models\tenant\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class LoginForgotTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin envio de parámetros
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/login/forgot', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["email" => ["The email field is required."], "environment" => ["The environment field is required."]]]]);
    }

    /**
     *  Prueba automatizada enviando email en formato incorrecto
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/login/forgot', [
            'email' => 'emailincorrecto',
            'environment' => 'front',
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    'email' => ['The email must be a valid email address.'],
                ]]
            ]);
    }

    /**
     * Prueba automatizada enviando email inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('POST', '/api/v1/login/forgot', [
            'email' => 'emailinexistenteEnBBDD@falso.com',
            'environment' => 'front',
        ])
            ->assertExactJson([
                "error" => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "email" => ["The selected email is invalid."]
                ]]
            ]);
    }

    /**
     * Prueba automatizada enviando entorno inválido
     */
    public function testBad4()
    {
        $userFactory = factory(User::class)->create([]);

        $this->json('POST', '/api/v1/login/forgot', [
            'email' => $userFactory->email,
            'environment' => 'incorrecto'
        ])
            ->assertExactJson([
                "error" => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "environment" => ["The selected environment is invalid."]
                ]]
            ]);
    }

    /**
     * Intenta cambiar password con campo bloqued_login a 1
     */
    public function testBad5()
    {
        $userFactory = factory(User::class)->create([
            'bloqued_login' => 1,
        ]);

        $this->json('POST', '/api/v1/login/forgot', [
            'email' => $userFactory->email,
            'environment' => 'front'
        ])
            ->assertExactJson([
                "error" => 403,
                "error_description" => "Forbidden",
                "error_inputs" => [[
                    "email" => ["User bloqued"]
                ]]
            ]);
    }

    /**
     * Intenta cambiar password con campo bloqued_to hasta una hora más tarde de la actual
     */
    public function testBad6()
    {
        $time_bloqued = Settings::where('name','time_bloqued_password')->first()->value;
        $userFactory = factory(User::class)->create([
            'bloqued_to' => Carbon::now()->addMinutes($time_bloqued),
        ]);

        $this->json('POST', '/api/v1/login/forgot', [
            'email' => $userFactory->email,
            'environment' => 'front'
        ])
            ->assertExactJson([
                "error" => 403,
                "error_description" => "Forbidden",
                "error_inputs" => [[
                    "email" => ["User bloqued"]
                ]]
            ]);
    }

    /**
     * Intenta cambiar password con campo bloqued_to hasta una hora más tarde de la actual y campo bloqued_login a 1
     */
    public function testBad7()
    {
        $time_bloqued = Settings::where('name','time_bloqued_password')->first()->value;
        $userFactory = factory(User::class)->create([
            'bloqued_to' => Carbon::now()->addMinutes($time_bloqued),
            'bloqued_login' => 1,
        ]);

        $this->json('POST', '/api/v1/login/forgot', [
            'email' => $userFactory->email,
            'environment' => 'front'
        ])
            ->assertExactJson([
                "error" => 403,
                "error_description" => "Forbidden",
                "error_inputs" => [[
                    "email" => ["User bloqued"]
                ]]
            ]);
    }

    /**
     * Solicita cambio de contraseña, devuelve 200 y seguido se vuelve a pedir un cambio, con lo que hay un token en vigor y sin expirar
     */
    public function testOk1()
    {
        $userFactory = factory(User::class)->create([
        ]);

        $this->json('POST', '/api/v1/login/forgot', [
            'email' => $userFactory->email,
            'environment' => 'front',
        ])
            ->assertExactJson([
                "error" => 200]);

        $this->assertDatabaseHas('mo_user_recover_token', ['updated_at' => Carbon::now(), 'user_id' => $userFactory->id]);


        $this->json('POST', '/api/v1/login/forgot', [
            'email' => $userFactory->email,
            'environment' => 'front',
        ])
            ->assertExactJson([
                "error" => 409,
                "error_description" => "Conflict",
                "error_inputs" => [[
                    "email" => ["You have already been sent an email to reset your password"]
                ]]
            ]);
    }


}