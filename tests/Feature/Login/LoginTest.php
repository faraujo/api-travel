<?php

namespace Tests\Feature;

use App\Models\tenant\Settings;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
//use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use App\Models\tenant\User;

class LoginTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Prueba automatizada sin envio de parametros
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/login', [])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.']
                ]]
            ]);
    }

    /**
     *  Prueba automatizada enviando email en formato incorrecto
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/login', [
            'email' => 'emailincorrecto',
            'password' => 'daMar1!',
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    'email' => ['The email must be a valid email address.'],
                ]]
            ]);
    }

    /**
     * Prueba automatizada enviando email inexistente en base de datos
     */
    public function testBad3()
    {

        $this->assertDatabaseMissing('mo_user', ['email' => 'emailFalso@gmail.com']);

        $this->json('POST', '/api/v1/login', [
            'email' => 'emailFalso@gmail.com',
            'password' => 'daMar1!',
        ])
            ->assertExactJson([
                'error' => 401,
                'error_description' => 'Unauthorized',
            ]);
    }

    /**
     *  Test intenta hacer login con un usuario bloqueado
     */
    public function testBad4()
    {

        factory(User::class)->create([
            'email' => 'email@gmail.com',
            'password' => bcrypt('daMar1!'),
            'faults_login' => 0,
            'bloqued_login' => 1,
        ]);

        $this->json('POST', '/api/v1/login', [
            'email' => 'email@gmail.com',
            'password' => 'daMar1!',
        ])
            ->assertJson([
                "error" => 403,
                "error_description" => "Forbidden",
                "error_inputs" => [[
                    "email" => ["User bloqued"]
                ]]
            ]);

    }

    /**
     *  Test intenta hacer login con password incorrecto
     */
    public function testBad5()
    {

        factory(User::class)->create([
            'email' => 'email@gmail.com',
            'password' => bcrypt('daMar1!'),
            'faults_login' => 0,
            'bloqued_login' => 0,
        ]);

        //$this->assertDatabaseMissing('mo_user', ['email' => 'emailFalso@gmail.com']);

        $this->json('POST', '/api/v1/login', [
            'email' => 'email@gmail.com',
            'password' => 'daMar1!111',
        ])
            ->assertJson(['error' => 401, 'error_description' => 'Unauthorized']);
    }

    /**
     *  Test intenta hacer login hasta alcanzar máximo número de fallos
     */
    public function testBad7()
    {

        $user = factory(User::class)->create([
            'email' => 'email@gmail.com',
            'password' => bcrypt('daMar1!'),
            'faults_login' => 0,
            'bloqued_login' => 0,
        ]);

        $contador = 0;
        $errores_maximos = Settings::where('name', '=', 'maximo_accesos_failed')->first()->value;

        while ($contador < $errores_maximos) {

            $this->json('POST', '/api/v1/login', [
                'email' => 'email@gmail.com',
                'password' => 'daMar1!111',
            ])
                ->assertJson(['error' => 401, 'error_description' => 'Unauthorized']);

            $contador++;
        }

        //Se comprueba actulización de parámetros de bloqueo del usuario
        $tiempo_bloqueo = Settings::where('name', 'time_bloqued_password')->first()->value;
        $this->assertDatabaseHas('mo_user', ['bloqued_to' => Carbon::now()->addMinutes($tiempo_bloqueo), 'faults_login' => '0', 'email' => 'email@gmail.com']);

        //cuando usuario bloqueado devuelve otro error y otra descripción
        $this->json('POST', '/api/v1/login', [
            'email' => 'email@gmail.com',
            'password' => 'daMar1!111',
        ])
            ->assertJson([
                "error" => 403,
                "error_description" => "Forbidden",
                "error_inputs" => [[
                    "email" => ["User bloqued temporarily"]
                ]]
            ]);

    }


    /**
     *  Test de login
     */
    public function testOk1()
    {
        $user = factory(User::class)->create([
            'email' => 'email@gmail.com',
            'password' => bcrypt('daMar1!'),
            'faults_login' => 0,
            'bloqued_login' => 0,
        ]);

        $this->json('POST', '/api/v1/login', [
            'email' => 'email@gmail.com',
            'password' => 'daMar1!',
        ])
            ->assertJsonFragment(['error' => 200])
            ->assertJsonStructure(['error', 'data' => [["token"]]]);

        $this->assertDatabaseHas('mo_user_token', ['user_id' => $user->id, 'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);


    }


}
