<?php

namespace Tests\Feature;

use App\Models\tenant\User;
use App\Models\tenant\UserToken;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
class LoginLogoutTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parametro obligatorio token
     */
    public function testBad1()
    {
        $user = factory(User::class)->create([]);
        $userToken = factory(UserToken::class)->create(['user_id' => $user->id]);
        $this->json('POST', '/api/v1/login/logout', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'token' => [
                    'Authorization header is required.'
                ]
            ]]]);

    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOK1()
    {
        $user = factory(User::class)->create([]);
        $userToken = factory(UserToken::class)->create(['user_id' => $user->id]);
        $this->assertDatabaseHas('mo_user_token',  ['user_id' => $user->id, 'token' => $userToken->token, 'deleted_at' => null]);
        $this->json('POST', '/api/v1/login/logout', [],['Authorization' => 'Bearer '.$userToken->token])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_user_token',  ['user_id' => $user->id, 'token' => $userToken->token, 'deleted_at' => null]);

    }
}
