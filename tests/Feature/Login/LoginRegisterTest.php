<?php

namespace Tests\Feature;

use App\Models\tenant\DocumentType;
use App\Models\tenant\User;
use App\Models\tenant\WorkerType;
use App\Models\tenant\Country;
use App\Models\tenant\State;
use Psy\Util\Json;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginRegisterTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Prueba automatizada sin envio de parametros
     *
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/login/register', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'email' => ['The email field is required.'],
                'password' => ['The password field is required.'],
                'c_password' => ['The c password field is required.'],
            ]]]);
    }

    /**
     * Prueba automatizada con email en formato incorrecto
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/login/register', [
            'email' => 'emailformatoincorrecto',
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'email' => [
                    'The email must be a valid email address.'
                ]
            ]]]);
    }

    /**
     * Prueba automatizada con password y c_password NO coinciden
     */
    public function testBad3()
    {
        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@correcto.com',
            'password' => 'daMar11!',
            'c_password' => 'daMar1!1111111',
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    'c_password' => ['The c password and password must match.'],
                ]
                ]]);
    }


    /**
     * Prueba automatizada con email ya existente en base de datos
     */
    public function testBad4()
    {
        $user = factory(User::class)->create([]);
        $this->json('POST', '/api/v1/login/register', [
            'email' => $user->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    'email' => ['The email has already been taken.'],
                ]
                ]]);
    }

    /**
     * Prueba automatizada campos no requeridos incorrectos
     */
    public function testBad5()
    {
        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@pruebasautomatizadaserror.com',
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'name' => 'Pruebas automatizadas',
            'document_type_id' => 'rrrrrrrrr',
            'number_document' => 'rrrrr',
            'sex_id' => 'rrrr',
            'birthdate' => 'rrrrrrr',
            'language_id' => 'rrrrrrr',
            'country_id' => 'rrrrrrrrr',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["birthdate"=>["The birthdate does not match the format Y-m-d.","The birthdate is not a valid date.",
                    "The birthdate must be a date before or equal to today."],"country_id"=>["The country id must be an integer."],"document_type_id"=>["The document type id must be an integer.","The selected document type id is invalid."],
                    "language_id"=>["The language id must be an integer.","The selected language id is invalid."],"sex_id"=>["The selected sex id is invalid.","The sex id must be an integer."]]]]);
    }

    /**
     * Prueba automatizada con password incorrecto
     */
    public function testBad6()
    {

        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@gmail.com',
            'password' => 'daMarrrr',
            'c_password' => 'daMarrrr',
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    'password' => ['The password format is invalid.'],
                ]
                ]]);
    }

    /**
     * Intenta introducir password con espacios
     */
    public function testBad7()
    {

        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@gmail.com',
            'password' => 'daMar 11!',
            'c_password' => 'daMar 11!',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@gmail.com',
            'password' => ' daMar11!',
            'c_password' => ' daMar11!',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["c_password"=>["The c password and password must match."],"password"=>["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11! ',
            'c_password' => 'daMar11! ',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["c_password"=>["The c password and password must match."],"password"=>["You can not enter spaces or accented characters in the password."]]]]);
    }

    /**
     * Intenta introducir password con acentos
     */
    public function testBad8()
    {
        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11!á',
            'c_password' => 'daMar11!á',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('POST', '/api/v1/login/register', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11!Á',
            'c_password' => 'daMar11!Á',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    public function testOk1()
    {
        $documentFactory = factory(DocumentType::class)->create([]);

        $workerType = factory(WorkerType::class)->create([]);
        $countryFactory=factory(\App\Models\tenant\Country::cLass)->create([]);
        $stateFactory=factory(\App\Models\tenant\State::cLass)->create([
            'country_id'=>$countryFactory->id,
        ]);

        $this->json('POST', '/api/v1/login/register', [
            'name' => 'nameok1',
            'surname' => 'surname ok1',
            'document_type_id' => $documentFactory->id,
            'number_document' => '444444',
            'sex_id' => 1,
            'birthdate' => '2000-12-12',
            'email' => 'email@gmail.com',
            'password' => 'passW11!',
            'c_password' => 'passW11!',
            'language_id' => 1,
            'telephone1' => 'telephone ok1',
            'telephone2' => 'tele2 ok1',
            'telephone3' => 'tele3 ok1',
            'address' => 'addres ok1',
            'postal_code' => 'postal_code ok1',
            'city' => 'city ok1',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business ok1',
            'telephone_business' => 'tele business ok1',
            'observations' => 'observations ok1',
            'worker' => 1,
            'worker_type_id' => $workerType->id,
        ])
            ->assertJson([
                'error' => 200,
            ]);

        //comprueba todos los campos excepto password que se guarda encriptado
        $this->assertDatabaseHas('mo_user', [
            'name' => 'nameok1',
            'surname' => 'surname ok1',
            'document_type_id' => $documentFactory->id,
            'number_document' => '444444',
            'sex_id' => 1,
            'birthdate' => '2000-12-12',
            'email' => 'email@gmail.com',
            'language_id' => 1,
            'telephone1' => 'telephone ok1',
            'telephone2' => 'tele2 ok1',
            'telephone3' => 'tele3 ok1',
            'address' => 'addres ok1',
            'postal_code' => 'postal_code ok1',
            'city' => 'city ok1',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business ok1',
            'telephone_business' => 'tele business ok1',
            'observations' => 'observations ok1',
            'worker' => 1,
            'worker_type_id' => $workerType->id,
            'bloqued_login' => 0,
            'faults_login' => 0,
        ]);
    }

}
