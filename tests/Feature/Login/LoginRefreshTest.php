<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 26/07/2018
 * Time: 13:29
 */

namespace Tests\Feature;


use App\Models\tenant\User;
use App\Models\tenant\UserRememberToken;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LoginRefreshTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin envio de parametros requeridos
     */
    public function testBad1(){
        $this->json('POST','/api/v1/login/refresh', [])
            ->assertExactJson(['error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["remember_token" => ["The remember token field is required."]]]]);

    }

    /**
     * Prueba automatizada enviando datos erroneos
     */
    public function testBad2(){
        $this->json('POST','/api/v1/login/refresh', [
            'remember_token' => 'test'
        ])
            ->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Prueba automatizada enviando parametros requeridos correctos
     */
    public function testOk1(){
        $user = factory(User::class)->create([]);

        $user_remember = factory(UserRememberToken::class)->create([
            'user_id' => $user->id
        ]);

        $this->json('POST','/api/v1/login/refresh', [
            'remember_token' => $user_remember->token
        ])
            ->assertJsonFragment(['error' => 200]);
    }

}