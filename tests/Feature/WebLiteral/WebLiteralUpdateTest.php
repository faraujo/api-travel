<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class WebLiteralUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations en formato incorrecto
     */
    public function testBad1()
    {

        $literalFactory = factory(App\Models\tenant\WebLiteral::class)->create([
            'name' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/literal', [
            'id' => $literalFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "translation"=>["you need at least one translation"]
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations inexistentes
     */
    public function testOk1()
    {
        $literalFactory = factory(App\Models\tenant\WebLiteral::class)->create([
            'name' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/literal', [
            'id' => $literalFactory->id,
            'translation' => ['ES' => 'cualquiera español']
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_web_literal_translation', [
            'literal_id' => $literalFactory->id,
            'language_id' => 1,
            'translation' => 'cualquiera español'
        ]);
    }
}