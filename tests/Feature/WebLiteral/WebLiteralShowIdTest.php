<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class WebLiteralShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta legal inexistente sin parametros
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/literal/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta producto existente sin parametros
     */
    public function testOk1()
    {

        $LiteralFactory = factory(App\Models\tenant\WebLiteral::class)->create([
            'name' => 'test',
        ]);
        $translation1 = factory(App\Models\tenant\WebLiteralTranslation::class)->create([
            'language_id' => 1,
            'literal_id' => $LiteralFactory->id,
        ]);
        factory(App\Models\tenant\WebLiteralTranslation::class)->create([
            'language_id' => 2,
            'literal_id' => $LiteralFactory->id,
        ]);
        factory(App\Models\tenant\WebLiteralTranslation::class)->create([
            'language_id' => 3,
            'literal_id' => $LiteralFactory->id,
        ]);

        $this->json('GET', '/api/v1/literal/' . $LiteralFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $LiteralFactory->id,
                'name' => $LiteralFactory->name
            ]);
    }
}