<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class WebLiteralShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta legal con parametro lang mal
     */
    public function testBad1()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/literal', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta productos con parametro lang
     */
    public function testOk1()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/literal', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta legales con parametro lang en mayusculas
     */
    public function testOk2()
    {
        //Se especifica parametro lang correctamente en mayusculas
        $this->json('GET', '/api/v1/literal', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    public function testOk3()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/literal', [
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    public function testOk4()
    {
        $literalFactory = factory(App\Models\tenant\WebLiteral::class)->create([
            'name' => 'test1',
        ]);
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/literal', [
            'name' => $literalFactory->name
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }
}