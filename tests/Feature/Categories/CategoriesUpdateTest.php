<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class CategoriesUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se envía un campo string donde debe ser numérico, no se envía un campo requerido y se envia file_id para asociar a categoria en formato incorrecto
     */
    public function testValidator()
    {
        $this->json('put', '/api/v1/category', [
            'id' => 4,
            'order' => 'r',
            'name' => ['EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'files' => [0 => 'g']
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["files.0"=>["The files.0 must be an integer."],"order" => ["The order must be an integer."],
                "name.ES" => ["The name. e s field is required."]
            ]]
            ]);
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se manda campo order negativo y file_id inexistente en base de datos
     */
    public function testOrderNeg()
    {
        $this->json('put', '/api/v1/category', [
            'id' => 4,
            'order' => -20,
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'files' => [0 => 99999999999]
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["files.0" => ["The selected files.0 is invalid."],"order" => ["The order must be at least 0."]
            ]]
            ]);
    }

    /**
     * Test para modificar una categoría
     *
     * A partir de los datos de la categoría que quiere modificar, modifica el usuario los campos que desee
     * Comprueba que los datos se actualizan correctamente en base de datos.
     * Repite el update, incluyendo algún campo vacío
     */
    public function testModificar()
    {

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([

        ]);

        factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
        ]);
        factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 2,
            'category_id' => $categoryFactory->id,
        ]);
        factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 3,
            'category_id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_category_translation', [
            'category_id' => $categoryFactory->id,
        ]);

        $this->json('put', '/api/v1/category', [
            'id' => $categoryFactory->id,
            'order' => 350,
            'name' => ['ES' => 'nombre CON ÑOO ñoo asáaá modificado', 'EN' => 'name update Inglés', 'PT' => 'name update portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 UUmodificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
            'order' => 350
        ]);

        $this->assertDatabaseHas('mo_category_translation', [
            'category_id' => $categoryFactory->id,
            'language_id' => 1,
            'name' => 'nombre CON ÑOO ñoo asáaá modificado',
            'description' => 'Descripcion1 modificada español',
            'short_description' => 'Short 1descripcion modificada español',
            'friendly_url' => '1friendly-url3-UUmodificada-español',
            'title_seo' => '1title_seo 3modificada español',
            'description_seo' => '1description_seo3 modificada español',
            'keywords_seo' => '1keywords_seo 3modificada español'
        ]);
        $this->assertDatabaseHas('mo_category_translation', [
            'category_id' => $categoryFactory->id,
            'language_id' => 2,
            'name' => 'name update Inglés',
            'description' => 'Descripcion3 modificada inglés',
            'short_description' => 'Short3 descripcion modificada inglés',
            'friendly_url' => 'friendly-url-3modificada-ingles',
            'title_seo' => 'title_seo3  modificada inglés',
            'description_seo' => 'description_seo3 modificada inglés',
            'keywords_seo' => 'keywords_seo3 modificada inglés'
        ]);
        $this->assertDatabaseHas('mo_category_translation', [
            'category_id' => $categoryFactory->id,
            'language_id' => 3,
            'name' => 'name update portugués',
            'description' => 'Descripcion3 modificada portugués',
            'short_description' => 'Short 3descripción modificada portugués',
            'friendly_url' => 'friendly-url3-modificada-portugues',
            'title_seo' => 'title_seo3 modificada portugués',
            'description_seo' => 'description_seo3 modificada portugués',
            'keywords_seo' => 'keywords_seo3 modificada portugués'
        ]);

        $this->json('put', '/api/v1/category', [
            'id' => $categoryFactory->id,
            'order' => '',
            'name' => ['ES' => 'nombre CON ÑOO ñoo asáaá modificado', 'EN' => 'name update Inglés', 'PT' => 'name update portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
            'order' => 0
        ]);

        $this->assertDatabaseHas('mo_category_translation', [
            'category_id' => $categoryFactory->id,
            'language_id' => 1,
            'name' => 'nombre CON ÑOO ñoo asáaá modificado',
            'description' => 'Descripcion1 modificada español',
            'short_description' => 'Short 1descripcion modificada español',
            'friendly_url' => 'nombre-con-noo-noo-asaaa-modificado',
            'title_seo' => '1title_seo 3modificada español',
            'description_seo' => '1description_seo3 modificada español',
            'keywords_seo' => null
        ]);

    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto con una sola traduccion
     */
    public function testCrear4()
    {
        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->json('PUT', '/api/v1/category', [
            'id' => $categoryFactory->id,
            'name' => [ 'ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español']])
            ->assertExactJson([
                'error' => 200
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto con una parametro no requerido
     */
    public function testCrear5()
    {
        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->json('PUT', '/api/v1/category', [
            'id' => $categoryFactory->id,
            'keywords_seo' => ['ES' => 'keywords_seo español']])
            ->assertExactJson([
                'error' => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [["description.ES" => ["The description. e s field is required."],
                    "description_seo.ES"=>["The description seo. e s field is required."],
                    "name.ES"=>["The name. e s field is required."],
                    "short_description.ES"=>["The short description. e s field is required."],
                    "title_seo.ES"=>["The title seo. e s field is required."]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto sin traduccion
     */
    public function testSinTraducciones()
    {
        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->json('PUT', '/api/v1/category', [
            'id' => $categoryFactory->id])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format","error_inputs"=> [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación categorias a files
     *
     * Se crea factory de files, se hace factory de category_file, al mandar relación con diferente file, borra las relaciones anteriores y crea las nuevas
     */
    public function testOk1()
    {

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'name file'
        ]);
        $this->assertDatabaseHas('mo_file', [
            'url_file' => $fileFactory->url_file,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => $fileFactoryTranslation->name,
        ]);

        //se crea factory con relación de product y category
        $categoryFileFactory = factory(App\Models\tenant\CategoryFile::class)->create([
            'category_id' => $categoryFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_category_file', [
            'category_id' => $categoryFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        //se crea nueva file para hacer nueva asignación y comprobar que borra la anterior
        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('nueva.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory2->id,
        ]);



        $this->json('PUT', '/api/v1/category', [
            'id' => $categoryFactory->id,
            'name' => [ 'ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => $fileFactory2->id]])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_category_file', [
            'file_id' => $fileFactory2->id,
        ]);

        $this->assertDatabaseMissing('mo_category_file', [
            'file_id' => $fileFactory->id,
            'deleted_at' => null,
        ]);

    }


}