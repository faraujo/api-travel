<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class CategoriesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta category inexistente sin parametros
     */
    public function testCategoryBad()
    {
        $this->json('GET', '/api/v1/category/9999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta category existente sin parámetro lang, comprueba que devuelve id de file asociado a category
     */
    public function testCategoryRead()
    {
        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
            'order' => 4,
        ]);
        $translation1 = factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 2,
            'category_id' => $categoryFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 3,
            'category_id' => $categoryFactory->id,
        ]);

        //se añade factories de file y traaduccion para comprobar que se muestra en salida
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $categoryFileFactory = factory(App\Models\tenant\CategoryFile::class)->create([
            'file_id' => $fileFactory->id,
            'category_id' => $categoryFactory->id,
        ]);

        $this->json('GET', '/api/v1/category/' . $categoryFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $categoryFactory->id,
                'name' => $translation1->name,
                'name' => $translation2->name,
                'name' => $translation3->name,
                'description' => $translation2->description,
                'short_description' => $translation3->short_description,
                'id' => $fileFactory->id,
            ]);
    }

    /**
     * Consulta category creada y comprueba estructura
     */
    public function testOk1(){

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
            'order' => 4,
        ]);
        $translation1 = factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 2,
            'category_id' => $categoryFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\CategoryTranslation::class)->create([
            'language_id' => 3,
            'category_id' => $categoryFactory->id,
        ]);

        $this->json('GET', '/api/v1/category/' . $categoryFactory->id)
            ->assertJsonStructure([
                'data' => [0 => ['category' => [0 => ['files','lang']]]]
            ]);
    }

}