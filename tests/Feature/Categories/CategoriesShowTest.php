<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CategoriesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta categorias con parámetro lang
     */
    public function testLang()
    {
        //Se especifica parametro lang correctamente

        $this->json('GET', '/api/v1/category', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data', "total_results"]);
    }

    /**
     * Test consulta categorias con parámetro lang en mayúsculas
     */
    public function testLangUpper()
    {
        //Se especifica parametro lang correctamente en mayusculas

        $this->json('GET', '/api/v1/category', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

    /**
     * Test consulta categorías con parametro lang mal
     */
    public function testLangBad()
    {
        // Se especifica parametro lang incorrectamente

        $this->json('GET', '/api/v1/category', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta categorias con parámetro product incorrecto
     */
    public function testProductBad()
    {
        // Se especifica incorrectamente parametro type

        $this->json('GET', '/api/v1/category', [
            'product_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta categorías con parámetro product inexistente
     */
    public function testProductNotExists()
    {
        //Se especifica un product inexistente

        $this->json('GET', '/api/v1/category', [
            'lang' => 'es',
            'product_id' => '999999999999999',
        ])->assertExactJson(['data' => [], 'error' => 200, "total_results" => 0,]);
    }

    /**
     * Test consulta categorías con parametro product existente en base de datos, aunque no relacionado a ninguna categoría no devuelve resultados pero tampoco error
     */
    public function testWithProductGood()
    {
        //crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('GET', '/api/v1/category', [
            'product_id' => $productFactory->id,
        ])->assertJsonStructure(['error', 'data']);
    }

    /**
     * Test consulta categorías con algunos de los parámetros mal
     */
    public function testWithSomeParametersBad()
    {
        // Se especifican incorrectamente parámetros

        $this->json('GET', '/api/v1/category', [
            'lang' => 'es',
            'product_id' => 'esas',
            'name' => 'cualquiera',
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta categorías con parámetros incorrectos
     */
    public function testWithBadParameters()
    {
        //Se especifican incorretamente alguno de los parametros

        $this->json('GET', '/api/v1/category', [
            'lang' => 'es',
            'name' => 'ven',
            'limit' => 'afaf',
        ])->assertJsonStructure(['error', 'error_description']);
    }

    /**
     * Test consulta categorías con algunos parámetros mal
     */
    public function testWithAllParametersSomeBad()
    {
        // Se especifican incorrectamente algunos parametros

        $this->json('GET', '/api/v1/category', [
            'lang' => 'es',
            'product_id' => 'h',
            'name' => 'cualquiera',
            'start' => 'm',
            'limit' => 5
        ])->assertJsonStructure(['error', 'error_description']);
    }

    /**
     *  Test consulta de categorías sin parámetros
     */
    public function testReadCategories()
    {

        //crea factory de categoria
        $factory = factory(App\Models\tenant\Category::class)->create([
            'order' => 4,
        ]);
        //crea translation o translations de category
        factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $factory->id,
            'language_id' => 1,
        ]);

        // No se especifica parametros
        $this->json('GET', '/api/v1/category')->assertJsonFragment(['id' => $factory->id]);
    }

    /**
     *  Test consulta de categoría filtrando por product borrado, no debe devolver resultados
     */
    public function testReadProductsCategory()
    {
        //crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        //crea factory de categoria
        $factory = factory(App\Models\tenant\Category::class)->create([
            'order' => 4,
        ]);
        //crea translation o translations de category
        factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $factory->id,
            'language_id' => 1,
        ]);
        //crea una relación product category
        $proCat = factory(App\Models\tenant\ProductCategory::class)->create([
            'category_id' => $factory->id,
            'product_id' => $productFactory->id,
        ]);
        //consulta categorías filtrando por el product que acaba de relacionar
        $this->json('GET', '/api/v1/category', ['product_id' => $productFactory->id])->assertJsonFragment(['total_results' => 1]);
        //borra categoría
        $proCat->delete();
        //vuelve a consultar categoría filtrando por el product, que estaba relacionado con una categoría, en el product_category que acaba de borrar
        $this->json('GET', '/api/v1/category', ['product_id' => $productFactory->id])->assertJsonFragment(['total_results' => 0]);
    }

    /**
     * Test consulta categorías con casi todos los parámetros
     */
    public function testWithParameters()
    {
        //crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        //crea factory de categoria
        $factory = factory(App\Models\tenant\Category::class)->create([
            'order' => 4,
        ]);
        //crea translation o translations de category
        factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $factory->id,
            'language_id' => 2,
        ]);
        //crea una relación product category
        $proCat = factory(App\Models\tenant\ProductCategory::class)->create([
            'category_id' => $factory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/category', [
            'lang' => 'en',
            'product_id' => $productFactory->id,
            'name' => 'EN',
        ])->assertJsonFragment(['id' => $factory->id]);
    }

    /**
     * Test consulta categorías con todos los parámetros
     */
    public function testWithAllParameters()
    {

        //crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        //crea factory de categoria
        $factory = factory(App\Models\tenant\Category::class)->create([
            'order' => 4,
        ]);
        //crea translation o translations de category
        factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $factory->id,
            'language_id' => 1,
        ]);
        //crea una relación product category
        $proCat = factory(App\Models\tenant\ProductCategory::class)->create([
            'category_id' => $factory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/category', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'name' => 'ES',
            'start' => 1,
            'limit' => 5
        ])->assertJsonFragment(['id' => $factory->id]);
    }
}