<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CategoriesDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda id requerido
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/category', [

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["ids"=>["The ids field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en campo id requerido
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/category', [
            'ids' => ['caracteres'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["ids.0"=>["The ids.0 must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda id de usuario inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/category', [
            'ids' => [99999999],
        ])
            ->assertExactJson(["error"=>404,"error_description"=>"Data not found", "error_inputs" => [
                [
                    "ids.0" => ["The selected ids.0 is invalid."],
                ]
            ]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/category', [
            'ids' => $categoryFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Borrar sin traducciones
     */
    public function testOk1()
    {
        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
            //'code' => 'ABC',
        ]);

        $this->json('DELETE', '/api/v1/category', [
            'ids' => [$categoryFactory->id]
        ])->assertExactJson(['error' => 200]);

        //Comprueba que en la tabla mo_category el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_category', ['deleted_at' => null, 'id' => $categoryFactory->id]);

    }

    /**
     * Test para eliminar una categoría que crea primero un category factory y sus traducciones
     */
    public function testOk2()
{

    $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        //'order' => 'pruebaFactory2',
    ]);
    $translation1 = factory(App\Models\tenant\CategoryTranslation::class)->create([
        'language_id' => 1,
        'category_id' => $categoryFactory->id,
    ]);
    $translation2 = factory(App\Models\tenant\CategoryTranslation::class)->create([
        'language_id' => 2,
        'category_id' => $categoryFactory->id,
    ]);
    $translation3 = factory(App\Models\tenant\CategoryTranslation::class)->create([
        'language_id' => 3,
        'category_id' => $categoryFactory->id,
    ]);

    $this->assertDatabaseHas('mo_category', [
        'id' => $categoryFactory->id,
    ]);

    $this->assertDatabaseHas('mo_category_translation', [
        'category_id' => $categoryFactory->id,
    ]);


    $this->json('DELETE', '/api/v1/category', [
        'ids' => [$categoryFactory->id],
    ])
        ->assertExactJson([
            'error' => 200,
        ]);

    //Comprueba que en la tabla mo_category el registro con id que ha sido creado no tiene el campo deleted_at como nulo
    $this->assertDatabaseMissing('mo_category', ['deleted_at' => null, 'id' => $categoryFactory->id]);
    //Comprueba que las traducciones de la categoría que han sido creadas no tienen el campo deleted_at como nulo
    $this->assertDatabaseMissing('mo_category_translation', ['deleted_at' => null, 'category_id' => $translation1->category_id]);
    $this->assertDatabaseMissing('mo_category_translation', ['deleted_at' => null, 'category_id' => $translation2->category_id]);
    $this->assertDatabaseMissing('mo_category_translation', ['deleted_at' => null, 'category_id' => $translation3->category_id]);
}

    public function testOk3()
    {

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
            //'order' => 'pruebaFactory2',
        ]);
        $productCategory1 = factory(App\Models\tenant\ProductCategory::class)->create([
            'category_id' => $categoryFactory->id,
        ]);
        $productCategory2 = factory(App\Models\tenant\ProductCategory::class)->create([
            'category_id' => $categoryFactory->id,
        ]);


        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_category', [
            'category_id' => $categoryFactory->id,
        ]);


        $this->json('DELETE', '/api/v1/category', [
            'ids' => [$categoryFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_product_category los registros con id que han sido creados no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_category', ['deleted_at' => null, 'id' => $categoryFactory->id]);
        //Comprueba que las categorías de producto que han sido creadas no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product_category', ['deleted_at' => null, 'category_id' => $productCategory1->category_id]);
        $this->assertDatabaseMissing('mo_product_category', ['deleted_at' => null, 'category_id' => $productCategory2->category_id]);

    }

    /**
     * Crea dos registros, comprueba que existen en base de datos, borra y comprueba que ya no existen
     *
     */
    public function testOk4()
    {

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);

        $categoryFactory2 = factory(App\Models\tenant\Category::class)->create([]);

        //comprueba que las categorías se han creado
        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory2->id,
        ]);

        $this->json('DELETE', '/api/v1/category', [
            'ids' => [$categoryFactory->id, $categoryFactory2->id],
        ])->assertExactJson([
            'error' => 200,
        ]);

        //comprueba que el campo deleted_at de las categorias que hemos borrado sea distinto de null
        $this->assertDatabaseMissing('mo_category', ['deleted_at' => null, 'id' => $categoryFactory->id]);

        $this->assertDatabaseMissing('mo_category', ['deleted_at' => null, 'id' => $categoryFactory2->id]);
    }


}