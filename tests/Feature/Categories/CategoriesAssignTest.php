<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CategoriesAssignTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba las validaciones del metodo
     *
     * Sin enviar ningun parametro
     */

    public function testAssignWithOutParameters()
    {
        $this->json('POST', '/api/v1/category/assign', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'product_id' => ['The product id field is required.'],
            ]]]);
    }

    /**
     * Test que comprueba validaciones del metodo
     *
     * Enviando formato incorrecto de product_id
     */
    public function testAssignWithBadProductId()
    {
        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => 'text',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_id' => ['The product id must be a number.'],
        ]]]);
    }

    /**
     * Test que comprueba validaciones del metodo
     *
     * Envio de product_id inexistente en base de datos
     */
    public function testAssignProductNotExists()
    {
        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => 999999999999999999999999,
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Test que comprueba validaciones del metodo
     *
     * Envio formato incorrecto de categories
     */
    public function testAssignWithBadCategoryIdFormat()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => $productFactory->id,
            'categories' => 'texto',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'categories' => ['The categories must be an array.'],
        ]]]);
    }

    /**
     * Test que comprueba validaciones del metodo
     *
     * Envio de category inexistente
     */
    public function testAssignCategoryNotExists()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => $productFactory->id,
            'categories' => [99999999999999999999999],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'categories.0' => ['The selected categories.0 is invalid.'],
        ]]]);
    }

    /**
     * Test que comprueba funcionamiento del metodo
     *
     * Envio de product sin categories
     */
    public function testAssignWithOutCategory()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => $productFactory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_product_category', ['product_id' => $productFactory->id, 'deleted_at' => null]);
    }

    /**
     * Test que comprueba funcionamiento del metodo
     *
     * Envio de product y categorie
     */
    public function testAssignWithAllParameters()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        ]);

        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => $productFactory->id,
            'categories' => [$categoryFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_product_category', ['product_id' => $productFactory->id, 'category_id' => $categoryFactory->id, 'deleted_at' => null]);

    }

    /**
     * Test que comprueba funcionamiento del metodo
     *
     * Envio de product y categorie sobreescribiendo relaciones ya existentes
     */
    public function testAssignWithAllParametersDeletedOthers()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        ]);

        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => $productFactory->id,
            'categories' => [$categoryFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_product_category', ['product_id' => $productFactory->id, 'category_id' => $categoryFactory->id, 'deleted_at' => null]);

        $categoryFactory2 = factory(App\Models\tenant\Category::class)->create([
        ]);

        $this->json('POST', '/api/v1/category/assign', [
            'product_id' => $productFactory->id,
            'categories' => [$categoryFactory2->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_product_category', ['product_id' => $productFactory->id, 'category_id' => $categoryFactory->id, 'deleted_at' => null]);

        $this->assertDatabaseHas('mo_product_category', ['product_id' => $productFactory->id, 'category_id' => $categoryFactory2->id, 'deleted_at' => null]);

    }


}