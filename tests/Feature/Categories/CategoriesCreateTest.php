<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class CategoriesCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda un campo string donde debe ser numérico, no se envían dos campos requeridos y se envía files en formato incorrecto
     */
    public function testValidator()
    {
        $this->json('POST', '/api/v1/category', [

            'order' => 'r',
            'name' => ['EN' => 'Cualquiera Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'files' => [0 => 'r']])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["files.0"=>["The files.0 must be an integer."],"order" => ["The order must be an integer."],
                "title_seo.PT" => ["The title seo. p t field is required."],
                "name.ES" => ["The name. e s field is required."]
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda campo order negativo y file_id inexistente en base de datos
     */
    public function testOrderNeg()
    {
        $this->json('POST', '/api/v1/category', [

            'order' => '-40',
            'name' => ['ES' => 'Cualquiera Español', 'EN' => 'Cualquiera Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'files' => [0 => 99999999]])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["files.0" => ["The selected files.0 is invalid."],"order" => ["The order must be at least 0."],
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un name con lang erroneo
     */
    public function testLang()
    {

        $this->json('POST', '/api/v1/category', [

            'order' => 1,
            'name' => ['EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués', 'IND' => 'Haname españa'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués']])
            ->assertExactJson([
                'error' => 400, 'error_description' => "The fields are not the required format", 'error_inputs' => [["name.ES" => ["The name. e s field is required."]]]
            ]);
    }

    /**
     * Realiza el test de creación de categoría con sus traducciones correspondientes
     * Comprueba que las traducciones se han insertado correctamente en base de datos
     */
    public function testCrear()
    {
        $this->json('POST', '/api/v1/category', [
            'order' => 5,
            'name' => ['ES' => 'Haname españa', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués test create'],
            'description' => ['ES' => 'Descripcion español test create', 'EN' => 'Descripcion inglés test create', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español test create', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués']])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_category_translation', [
            'language_id' => 1,
            'name' => 'Haname españa',
            'description' => 'Descripcion español test create',
            'short_description' => 'Short descripcion español test create',
            'friendly_url' => 'friendly-url-espanolMAA',
            'title_seo' => 'title_seo español',
            'description_seo' => 'description_seo español',
            'keywords_seo' => 'keywords_seo español'
        ]);

        $this->assertDatabaseHas('mo_category_translation', [
            'language_id' => 2,
            'name' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés',
            'description' => 'Descripcion inglés test create',
            'short_description' => 'Short descripcion inglés',
            'friendly_url' => 'friendly-url-ingles',
            'title_seo' => 'title_seo inglés',
            'description_seo' => 'description_seo inglés',
            'keywords_seo' => 'keywords_seo inglés'
        ]);

        $this->assertDatabaseHas('mo_category_translation', [
            'language_id' => 3,
            'name' => 'Cuanlquiera portugués test create',
            'description' => 'Descripción portugués',
            'short_description' => 'Short descripción portugués',
            'friendly_url' => 'friendly-url-portugues',
            'title_seo' => 'title_seo portugués',
            'description_seo' => 'description_seo portugués',
            'keywords_seo' => 'keywords_seo portugués'
        ]);
    }

    /**
     * Realiza el test de creación de categoría y su traducciones correspondientes, sin order
     *
     */
    public function testCrear2()
    {
        $this->json('POST', '/api/v1/category', [

            'name' => ['ES' => 'Haname españa', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués']])
            ->assertExactJson([
                'error' => 200,
            ]);
    }

    /**
     * Realiza el test de creación de categoría y sus traducciones correspondientes, con order vacío y sin friendly_url
     * Comprueba la generación de friendly-url en español, a partir del name
     */
    public function testCrear3()
    {
        $this->json('POST', '/api/v1/category', [
            'order' => '',
            'name' => ['ES' => 'Haname españa crear url', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués']])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_category_translation', [
            'language_id' => 1,
            'name' => 'Haname españa crear url',
            'description' => 'Descripcion español',
            'short_description' => 'Short descripcion español',
            'friendly_url' => 'haname-espana-crear-url',
            'title_seo' => 'title_seo español',
            'description_seo' => 'description_seo español',
            'keywords_seo' => 'keywords_seo español'
        ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto con una sola traduccion
     */
    public function testCrear4()
    {

        $this->json('POST', '/api/v1/category', [
            'name' => [ 'ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español']])
            ->assertExactJson([
                'error' => 200
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto con una parametro no requerido
     */
    public function testCrear5()
    {
        $this->json('POST', '/api/v1/category', [
            'keywords_seo' => ['ES' => 'keywords_seo español']])
            ->assertExactJson([
                'error' => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [["description.ES" => ["The description. e s field is required."],
                    "description_seo.ES"=>["The description seo. e s field is required."],
                    "name.ES"=>["The name. e s field is required."],
                    "short_description.ES"=>["The short description. e s field is required."],
                    "title_seo.ES"=>["The title seo. e s field is required."]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto sin traduccion
     */
    public function testSinTraducciones()
    {

        $this->json('POST', '/api/v1/category', [])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format","error_inputs"=> [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de files a categorias
     *
     * Se crea factory de file, se asigna file al crear categoria y se comprueba en base de datos
     */
    public function testOk5()
    {

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);


        $this->json('POST', '/api/v1/category', [
            'name' => [ 'ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => $fileFactory->id]])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_category_file', [
            'file_id' => $fileFactory->id,
        ]);

    }
}