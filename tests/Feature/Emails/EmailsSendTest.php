<?php

namespace Tests\Feature;

use App\Mail\Email;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Testing\Fakes\MailFake;
use Tests\TestCase;

class EmailsSendTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba funcionamiento de las validaciones
     * Para poder enviar el email, los campos email y subject_email de cada email dentro del array de emails son obligatorios.
     * También el title común para todos.
     * Se puede también enviar adjuntos aunque no es requerido, y en caso de envío debe ser un array.
     * En este test se especifica un 'template' y 'attachments' no válido.
     */

    public function testBad1()
    {
        Mail::fake();

        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitor@viavox.com',
                    'subject_email' => 'subject_email prueba'
                ]],
            'template' => 'template no válido',
            'title' => 'title para pruebas',
            'mailbox' => 'default',
            'attachments' => 'adjunto sin array'
        ])
            ->assertExactJson(['error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "attachments" => ["The attachments must be an array."],
                "template" => ["Template not exists"],
            ]]
            ]);

        Mail::assertNotSent(Email::class, function ($mail) {
            return $mail->hasTo('e-monitor@viavox.com');
        });
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     * Para poder enviar el email, los campos email y subject_email de cada email dentro del array de emails son obligatorios.
     * También el title común para todos
     * Se puede también enviar adjuntos aunque no es requerido, y en caso de envío debe ser un array.
     * En este test se envía un mailbox que no encuentre en la BBDD (concatenado al resto de parámetros para configurar el servidor de correo)
     * y un email no válido.
     */

    public function testBad2()
    {
        Mail::fake();

        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitorviavox.com',
                    'subject_email' => 'subject_email prueba'
                ]],
            'template' => 'information',
            'title' => 'title para pruebas',
            'mailbox' => 'mailbox no existe en BBDD',
        ])
            ->assertExactJson(['error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "emails.0.email" => ["The emails.0.email must be a valid email address."],
                "mailbox" => ["Mailbox not available"],
            ]]
            ]);

        Mail::assertNotSent(Email::class, function ($mail) {
            return $mail->hasTo('e-monitorviavox.com');
        });
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     * Para poder enviar el email, los campos email y subject_email de cada email dentro del array de emails son obligatorios.
     * También el title común para todos
     * Se puede también enviar adjuntos aunque no es requerido, y en caso de envío debe ser un array.
     * En este test no se envía el title, ni el mailbox, ni el subject_email.
     */

    public function testBad3()
    {
        Mail::fake();

        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitor@viavox.com',
                    //'subject_email' => 'subject_email prueba'
                ]],
            'template' => 'error',
        ])
            ->assertExactJson(['error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "emails.0.subject_email" => ["The emails.0.subject_email field is required."],
                "title" => ["The title field is required."],
            ]]
            ]);

        Mail::assertNotSent(Email::class, function ($mail) {
            return $mail->hasTo('e-monitor@viavox.com');
        });
    }

    /**
     * Test que comprueba el envío de un email a la dirección indicada.
     * Para poder enviar el email, los campos email y subject_email de cada email dentro del array de emails son obligatorios.
     * También el template ['informacion' o 'error'] para el envío de los emails, el title común para todos, y el mailbox a valor default (para configurar el servidor de correo con los parámetros default de la BBDD, ahora no hay más)
     * Se puede también enviar adjuntos aunque no es requerido, y en caso de envío debe ser un array.
     */

    public function testOK1()
    {
        Mail::fake();

        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitor@viavox.com',
                    'subject_email' => 'subject_email prueba testOK1',
                ]],
            'template' => 'information',
            'title' => 'title para pruebas',
            'mailbox' => 'default',
        ])
            ->assertExactJson(['error' => 200
            ]);

        Mail::assertQueued(Email::class, function ($mail) {
            return $mail->hasTo('e-monitor@viavox.com');
        });

        Mail::assertNotSent(Email::class, function ($mail) {
            return $mail->hasTo('e-monitorfallo@viavox.com');
        });

        /*        //Comprobación de parámetros. Para que funcione los atributos tendrían que ser public, no protected.
                Mail::assertQueued(Email::class, function ($mail) {
                    return $mail->title === 'title para pruebas';
                });

                Mail::assertQueued(Email::class, function ($mail) {
                    return $mail->subject_email === 'subject_email prueba';
                });*/
    }

    /**
     * Test que comprueba el envío de un email a la dirección indicada
     * Para poder enviar el email, los campos email y subject_email de cada email dentro del array de emails son obligatorios.
     * También el title común para todos
     * Se puede también enviar adjuntos aunque no es requerido, y en caso de envío debe ser un array.
     * Se envían adjuntos en formato array.
     */

    public function testOK2()
    {
        Mail::fake();

        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitor@viavox.com',
                    'subject_email' => 'subject_email prueba',
                ]],
            'template' => 'information',
            'title' => 'title para pruebas',
            'mailbox' => 'default',
        ])
            ->assertJson(['error' => 200
            ]);

        Mail::assertQueued(Email::class, function ($mail) {
            return $mail->hasTo('e-monitor@viavox.com');
        });

        Mail::assertNotSent(Email::class, function ($mail) {
            return $mail->hasTo('e-monitorfallo@viavox.com');
        });

    }

    /**
     * Test que comprueba el envío de dos emails a las direcciones indicadas
     * Para poder enviar el email, los campos email y subject_email de cada email dentro del array de emails son obligatorios.
     * También el title común para todos
     * Se puede también enviar adjuntos aunque no es requerido, y en caso de envío debe ser un array.
     */

    public function testOK3()
    {
        Mail::fake();

        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitorPrueba1@viavox.com',
                    'subject_email' => 'subject_email prueba',
                ],
                '1' => [
                    'email' => 'e-monitorPrueba2@viavox.com',
                    'subject_email' => 'subject_email prueba',
                ]],
            'template' => 'information',
            'title' => 'title para pruebas',
            'mailbox' => 'default',
        ])
            ->assertJson(['error' => 200
            ]);

        Mail::assertQueued(Email::class, 2);

        Mail::assertQueued(Email::class, function ($mail) {
            return $mail->hasTo('e-monitorPrueba1@viavox.com');
        });

        Mail::assertQueued(Email::class, function ($mail) {
            return $mail->hasTo('e-monitorPrueba2@viavox.com');
        });

        Mail::assertNotQueued(Email::class, function ($mail) {
            return $mail->hasTo('e-monitorfallo@viavox.com');
        });

    }

    /**
     * Test que comprueba el OK del controlador.
     */

    public function testOK4()
    {

        Mail::fake();
        
        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitor@viavox.com',
                    'subject_email' => 'subject_email prueba',
                ]],
            'template' => 'information',
            'title' => 'title para pruebas',
            'mailbox' => 'default',
            'body' => 'prueba',
        ])
            ->assertExactJson(['error' => 200
            ]);
        $this->assertClassHasAttribute('from_email', Email::class);
        $this->assertClassHasAttribute('from_name', Email::class);
        $this->assertClassHasAttribute('subject_email', Email::class);
        $this->assertClassHasAttribute('template', Email::class);
        $this->assertClassHasAttribute('title', Email::class);
        $this->assertClassHasAttribute('adjuntos', Email::class);

    }

    /**
     * Test que comprueba el OK del controlador enviando template recover, para recuperar password
     */

    public function testOK5()
    {

        Mail::fake();
        
        $this->json('POST', '/api/v1/email', [
            'emails' => [
                '0' => [
                    'email' => 'e-monitor@viavox.com',
                    'subject_email' => 'subject_email prueba',
                ]],
            'template' => 'recover',
            'title' => 'title para pruebas recover',
            'mailbox' => 'default',
            'body' => 'prueba',
        ])
            ->assertExactJson(['error' => 200
            ]);
        $this->assertClassHasAttribute('from_email', Email::class);
        $this->assertClassHasAttribute('from_name', Email::class);
        $this->assertClassHasAttribute('subject_email', Email::class);
        $this->assertClassHasAttribute('template', Email::class);
        $this->assertClassHasAttribute('title', Email::class);
        $this->assertClassHasAttribute('adjuntos', Email::class);

    }

}

