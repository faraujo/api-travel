<?php

use App\Models\tenant\Template;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class EmailTemplatesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta plantillas de email
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/email')->assertJsonStructure([
            'error',
            'data' => [['email' => [['template' => [['id','name','description']]]]]],
            'total_results'
        ])->assertJson(["error" => 200]);
    }

    /**
     * Test consulta platillas de email creando una nueva
     */
    public function testOk2()
    {

        $templates = Template::all();
        foreach ($templates as $template){
            $template->delete();
        }

        $templateFact = factory(Template::class)->create([
        ]);

        $this->json('GET', '/api/v1/email')->assertJsonStructure([
            'error',
            'data' => [['email' => [['template' => [['id','name','description']]]]]],
            'total_results'
        ])->assertJson([
            'error' => 200,
            'data' => [['email' => [['template' => [[
                'id' => $templateFact->id,
                'name' => $templateFact->name,
                'description' => $templateFact->description]]]]]],
            'total_results' => 1
        ]);

    }

}