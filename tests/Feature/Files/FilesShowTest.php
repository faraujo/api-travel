<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\tenant\File;
use App\Models\tenant\FileType;
use Tests\TestCase;

class FilesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta files con parámetro lang mal
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/file', [
            'lang' => 'qwe'
        ])->assertJson(["error" => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [["lang" => ["The lang is not exists"]]]]);
    }

    /**
     * Test consulta files con parámetros incorrectos
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/file', [
            'limit' => 'afaf',
        ])->assertJson(["error" => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [
                [
                    "limit" => [
                        "The limit must be an integer."
                    ]
                ]
            ]]);
    }

    /**
     * Test consulta files con algunos parámetros mal
     */
    public function testBad3()
    {
        $this->json('GET', '/api/v1/file', [
            'page' => 'm',
            'limit' => 5
        ])->assertJson(["error" => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [
                [
                    "page" => [
                        "The page must be an integer."
                    ]
                ]
            ]]);
    }

    /**
     * Test consulta files con algunos parámetros mal
     */
    public function testBad4()
    {
        $this->json('GET', '/api/v1/file', [
            'type_id' => 'h',
        ])->assertJson(["error" => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [
                [
                    "type_id" => [
                        "The type id must be an integer."
                    ]
                ]
            ]]);
    }

    /**
     * Test consulta files con parámetro lang en mayúsculas
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/file', [
            'lang' => 'EN'
        ])->assertJson(["error" => 200]);
    }

    /**
     *  Test consulta de files sin parámetros
     */
    public function testOk2()
    {
        $this->json('GET', '/api/v1/file')->assertJsonStructure(['error', 'data', 'total_results']);
    }

    /**
     *  Test consulta de files filtrando por tipo de archivo borrado, no debe devolver resultados
     */
    public function testOk3()
    {

        //crea file type
        $typeFact = factory(FileType::class)->create([
        ]);

        //crea file
        $factory = factory(File::class)->create([
            'order' => 4,
            'type_id' => $typeFact->id,
        ]);

        $filetranslationfactory = factory(\App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $factory->id,
            'language_id' => 1,
        ]);

        //consulta files filtrando por el file_type que acaba de relacionar y devuelve un resultado, ya que viene un campo id
        $this->json('GET', '/api/v1/file', ['type_id' => $typeFact->id])->assertJson([
            "error" => 200,
            "total_results" => 1
        ]);

        //borra type_id de tabla mo_file_type
        $typeFact->delete();

        //vuelve a consultar file filtrando por el file_type que acaba de borrar y debe traer 0 resultados
        $this->json('GET', '/api/v1/file', ['type_id' => $typeFact->id])->assertJson([
            "error" => 200,
            "total_results" => 0
        ]);
    }


}