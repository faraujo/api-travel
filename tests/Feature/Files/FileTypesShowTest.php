<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class FileTypesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test mostrar tipos de archivos idioma incorrecto
     */
    public function testBad1(){
        $this->json('GET','/api/v1/file/types', [
            'lang' => 'FDF',
        ])->assertExactJson([
            'error' => 400,
            "error_description"=>"The fields are not the required format",
            "error_inputs"=>[[
                "lang"=>["The lang is not exists"]
            ]]
        ]);
    }

    /**
     * test consulta de tipos de archivos sin parametros
     */
    public function testOk1(){
        $this->json('GET','/api/v1/file/types', [])
            ->assertJsonStructure(
                ['error','data']
            );
    }

    /**
     * test consulta de tipos de archivos con idioma correcto
     */
    public function testOk2(){
        $this->json('GET','/api/v1/file/types', [
            'lang' => 'ES',
        ])->assertJsonStructure([
            'error',
            'data',
            'total_results',
        ]);
    }



}
