<?php

namespace Tests\Feature;
use App\Models\tenant\FileTranslation;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use App\Models\tenant\File;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class FilesDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/file', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/file', [
            'ids' => [''],
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/file', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' =>[["ids.0" =>["The selected ids.0 is invalid."]]]
            ]);
    }

    /**
     *Test que elimina un registro y su traducción y comprueba que no están en base de datos
     *
     */
    public function testOk1()
    {
        $fileFactory = factory(File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        $this->assertDatabaseHas('mo_file', ['id' => $fileFactory->id]);
        $this->assertDatabaseHas('mo_file_translation', ['id' => $fileFactoryTranslation->id]);

        $this->json('DELETE', '/api/v1/file', [
            'ids' => [$fileFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_file', ['deleted_at' => null, 'id' => $fileFactory->id]);
        $this->assertDatabaseMissing('mo_file_translation', ['deleted_at' => null, 'id' => $fileFactoryTranslation->id]);

    }

    /**
     * Comprueba que si no hay traducciones también se borra
     *
     */
    public function testOk2()
    {
        $fileFactory = factory(File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', ['id' => $fileFactory->id]);

        $this->json('DELETE', '/api/v1/file', [
            'ids' => [$fileFactory->id],
        ])->assertExactJson(['error' => 200]);

        //Comprueba que en la tabla mo_file el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_file', ['deleted_at' => null, 'id' => $fileFactory->id]);

    }

/**
 *Test que elimina dos registros y sus  traducciones y comprueba que no están en base de datos
 *
 */
    public function testOk3()
{
    $fileFactory1 = factory(File::class)->create([
        'type_id' => 3,
        'url_file' => UploadedFile::fake()->image('avatar.jpg'),
    ]);

    $fileFactory2 = factory(File::class)->create([
        'type_id' => 5,
        'url_file' => UploadedFile::fake()->image('avatar.jpg'),
    ]);

    $fileFactoryTranslation1 = factory(FileTranslation::class)->create([
        'file_id' => $fileFactory1->id,
        'language_id' => 1,
        'name' => 'Nombre fichero en español',
        'description' => 'Descripción fichero en español',
    ]);

    $fileFactoryTranslation2 = factory(FileTranslation::class)->create([
        'file_id' => $fileFactory2->id,
        'language_id' => 2,
        'name' => 'Nombre fichero en ingles',
        'description' => 'Descripción fichero en ingles',
    ]);


    $this->assertDatabaseHas('mo_file', ['id' => $fileFactory1->id]);
    $this->assertDatabaseHas('mo_file', ['id' => $fileFactory2->id]);
    $this->assertDatabaseHas('mo_file_translation', ['id' => $fileFactoryTranslation1->id]);
    $this->assertDatabaseHas('mo_file_translation', ['id' => $fileFactoryTranslation2->id]);




    $this->json('DELETE', '/api/v1/file', [
        'ids' => [$fileFactory1->id,$fileFactory2->id],
    ])->assertExactJson(['error' => 200]);

    $this->assertDatabaseMissing('mo_file', ['deleted_at' => null, 'id' => $fileFactory1->id]);
    $this->assertDatabaseMissing('mo_file', ['deleted_at' => null, 'id' => $fileFactory2->id]);
    $this->assertDatabaseMissing('mo_file_translation', ['deleted_at' => null, 'id' => $fileFactoryTranslation1->id]);
    $this->assertDatabaseMissing('mo_file_translation', ['deleted_at' => null, 'id' => $fileFactoryTranslation2->id]);

    }
}