<?php

namespace Tests\Feature;

use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class FileTypesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta tipo promoción inexistente sin filtro
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/file/types/999')
            ->assertExactJson(['error' => 200, 'data' => [['type' => []]]]);
    }

    /**
     * Consultar tipo archivo con filtro lang erróneo
     */
    public function testBad2()
    {
        $fileTypeFactory = factory(FileType::class)->create([
        ]);

        factory(FileTypeTranslation::class)->create([
            'language_id' => 1,
            'type_id' => $fileTypeFactory->id,
        ]);
        factory(FileTypeTranslation::class)->create([
            'language_id' => 2,
            'type_id' => $fileTypeFactory->id,
        ]);
        factory(FileTypeTranslation::class)->create([
            'language_id' => 3,
            'type_id' => $fileTypeFactory->id,
        ]);

        $this->json('GET', '/api/v1/file/types/' . $fileTypeFactory->id, [
            'lang' => 'FDF'])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }
    /**
     * Consulta tipo archivo existente sin filtro lang
     */
    public function testOK1()
    {
        $fileTypeFactory = factory(FileType::class)->create([
        ]);

        $translation1 = factory(FileTypeTranslation::class)->create([
            'language_id' => 1,
            'type_id' => $fileTypeFactory->id,
        ]);
        $translation2 = factory(FileTypeTranslation::class)->create([
            'language_id' => 2,
            'type_id' => $fileTypeFactory->id,
        ]);
        $translation3 = factory(FileTypeTranslation::class)->create([
            'language_id' => 3,
            'type_id' => $fileTypeFactory->id,
        ]);
        $this->json('GET', '/api/v1/file/types/' . $fileTypeFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $fileTypeFactory->id,
                'name' => $translation1->name,
            ]);
    }

    /**
     * Consulta tipo archivo existente con filtro lang EN
     */
    public function testOK2()
    {
        $fileTypeFactory = factory(FileType::class)->create([
        ]);
        $translation = factory(FileTypeTranslation::class)->create([
            'language_id' => 2,
            'type_id' => $fileTypeFactory->id,
        ]);

        $this->json('GET', '/api/v1/file/types/' . $fileTypeFactory->id, [
            'lang' => 'EN'])->assertJsonFragment([
            'error' => 200,
            'id' => $fileTypeFactory->id,
            'name' => $translation->name,
        ]);
    }

}
