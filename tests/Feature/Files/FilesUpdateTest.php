<?php

namespace Tests\Feature;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use App\Models\tenant\File;
use Illuminate\Http\UploadedFile;

class FilesUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     * No se envía ninguno de los campos requeridos
     */
    public function testBad1()
    {

        $this->json('PUT', '/api/v1/file', [

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["alternative_text"=>["The alternative text field is required."],"id"=>["The id field is required."],
                    "name"=>["The name field is required."],"translation"=>["you need at least one translation"]]]]);

    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     * Se manda traducción de name y alternative_text obligatorias, y formato erróneo en cada uno de los campos
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/file', [

            'id' => 'string',
            'name' => ['ES' => 'nameee'],
            'alternative_text' => ['ES' => 'alternative text'],
            'order' => 'string',
            'private' => 'string',
            'type_id' => 'string'
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["id"=>["The id must be an integer."],"order"=>["The order must be an integer."],"private"=>["The private field must be true or false."],
                    "type_id"=>["The type id must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     * Se envía un name de un idioma y no se manda alternative_text
     */
    public function testBad3()
    {
        $fileFactory = factory(File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileTranslationFactory = factory(FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
        ]);

        $this->json('PUT', '/api/v1/file', [

            'id' => $fileFactory->id,
            'name' => ['ES' => 'nameee'],

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["alternative_text"=>["The alternative text field is required."],"alternative_text.ES"=>["The alternative text. e s field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     * Se envía un name de un idioma y un alternative_text de otro
     */
    public function testBad4()
    {
        $fileFactory = factory(File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileTranslationFactory = factory(FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
        ]);

        $this->json('PUT', '/api/v1/file', [

            'id' => $fileFactory->id,
            'name' => ['ES' => 'nameee'],
            'alternative_text' => ['EN' => 'alternative text ingles']

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["alternative_text.ES"=>["The alternative text. e s field is required."],"name.EN"=>["The name. e n field is required."]]]]);

    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     * Se intenta enviar un type_id en inexistente en base de datos
     */
    public function testBad5()
    {
        $fileFactory = factory(File::class)->create([
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileTranslationFactory = factory(FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
        ]);

        $this->json('PUT', '/api/v1/file', [

            'id' => $fileFactory->id,
            'name' => ['ES' => 'nameee español'],
            'alternative_text' => ['ES' => 'alternative text español'],
            'type_id' => 99999999,

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["type_id"=>["The selected type id is invalid."]]]]);
    }

    /**
     * Crea factory, comprueba en base de datos, modifica y comprueba cambios en base de datos. Comprueba fragmentos de la salida demostrando que devuelve datos del archivo y de su tipo de archivo
     */
    public function testOk1()
    {
        $fileFactory = factory(File::class)->create([
            'order' => 1,
            'private' => 1,
        ]);

        $fileTranslationFactory = factory(FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'nombre español archivo',
            'alternative_text' => 'texto alternativo de archivo'
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
            'order' => $fileFactory->order,
            'private' => $fileFactory->private,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'file_id' => $fileTranslationFactory->file_id,
            'language_id' => $fileTranslationFactory->language_id,
            'name' => $fileTranslationFactory->name,
            'alternative_text' => $fileTranslationFactory->alternative_text
        ]);

        $fileTypeFactory = factory(FileType::class)->create([
        ]);

        $fileTypeTranslationFactory = factory(FileTypeTranslation::class)->create([
            'language_id' => 1,
            'type_id' => $fileTypeFactory->id,
            'name' => 'name tipo archivo'
        ]);

        $this->json('PUT', '/api/v1/file', [
            'id' => $fileFactory->id,
            'name' => ['ES' => 'name modificado'],
            'alternative_text' => ['ES' => 'alternative text modificado'],
            'order' => '0',
            'private' => '0',
            'type_id' => $fileTypeFactory->id
        ])
            //fragmentos de archivo y de tipo de archivo
            ->assertJsonFragment(["error" => 200,'id' => $fileFactory->id,'name' => 'name modificado','id' => $fileTypeFactory->id,'name' => 'name tipo archivo']);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
            'order' => '0',
            'private' => '0',
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => 'name modificado',
            'alternative_text' => 'alternative text modificado',
        ]);
    }

    /**
     *Crea factory con traducción en idioma 1, comprueba en base de datos, modifica mandando traducción en idioma 2 y comprueba cambios en base de datos.
     * comprueba estructura de salida con datos de archivo y de tipo de archivo
     */
    public function testOk2()
    {
        $fileFactory = factory(File::class)->create([
            'order' => 1,
            'private' => 1,
        ]);

        $fileTranslationFactory = factory(FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'nombre espanol archivo',
            'alternative_text' => 'texto alternativo de archivo'
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
            'order' => $fileFactory->order,
            'private' => $fileFactory->private,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'file_id' => $fileTranslationFactory->file_id,
            'language_id' => $fileTranslationFactory->language_id,
            'name' => $fileTranslationFactory->name,
            'alternative_text' => $fileTranslationFactory->alternative_text
        ]);

        $this->json('PUT', '/api/v1/file', [

            'id' => $fileFactory->id,
            'name' => ['EN' => 'name modificado'],
            'alternative_text' => ['EN' => 'alternative text modificado'],
            'order' => '0',
            'private' => '0'
        ])
            //comprueba estructura de salida con datos de archivo y de tipo de archivo
            ->assertJsonStructure(["error",'data' => [0 => ['file' => [0 => ['id','order','url_file','type','lang']]]]]);

        $this->assertDatabaseHas('mo_file', [
            'order' => 0,
            'private' => 0,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => 'name modificado',
            'alternative_text' => 'alternative text modificado',
            'language_id' => 2,
        ]);

    }

}