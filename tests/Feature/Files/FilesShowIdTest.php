<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use App\Models\tenant\File;
use App\Models\tenant\FileTranslation;
use Tests\TestCase;

class FilesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta file inexistente
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/file/9999')
            ->assertJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta file existente
     */
    public function testOk2()
    {

        //se añade factories de file y traaduccion para comprobar que se muestra en salida
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation1 = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'name file',
            'description' => 'description file',
            'alternative_text' => 'alternative text',
        ]);

        $this->json('GET', '/api/v1/file/' . $fileFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $fileFactory->id,
                'name' => $fileFactoryTranslation1->name,
                'description' => $fileFactoryTranslation1->description,
                'alternative_text' => $fileFactoryTranslation1->alternative_text,
            ]);
    }

    /**
     * Consulta file creada y comprueba estructura
     */
    public function testOk3(){

        //se añade factories de file y traaduccion para comprobar estructura
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation1 = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'name file',
            'description' => 'description file',
            'alternative_text' => 'alternative text',
        ]);

        $this->json('GET', '/api/v1/file/' . $fileFactory->id)
            ->assertJsonStructure([
                'data' => [0 => ['file' => [0 => ['lang']]]]
            ]);
    }

}