<?php

namespace Tests\Feature;

use App\Models\tenant\FileTranslation;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use App\Models\tenant\File;
use Illuminate\Http\UploadedFile;

class FilesCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *Es requerido enviar cada archivo con su extensión: video, image, document o global
     */
    public function testBad1()
    {
        
        $this->json('POST', '/api/v1/file', [

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["files"=>["The files field is required."],"type"=>["The type field is required."]]]]);

    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *Es requerido enviar cada archivo con su extensión: video, image, document o global. Manda extensión errónea, campo private con formato erróneo y order en formato erroneo
     */
    public function testBad2()
    {

        $this->json('POST', '/api/v1/file', [

            'files' => [
                '0' => [
                    'file' => UploadedFile::fake()->image('imagenPrueba.jpg'),
                    'order' => 'rr']],
            'private' => 'r',
            'type' => 'erroneo',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["files.0.file"=>["The files.0.file must be a file of type: ."],
                    "files.0.order"=>["The files.0.order must be an integer."],"private"=>["The private field must be true or false."],
                    "type"=>["The selected type is invalid."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *Se envía imagen con tipo de extension document
     *
     */
    public function testBad3()
    {
        $this->json('POST', '/api/v1/file', [

            'files' => [
                '0' => [
                    'file' => UploadedFile::fake()->image('imagenPrueba.jpg')]],
            'type' => 'document',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["files.0.file"=>["The files.0.file must be a file of type: application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation."]]]]);

    }


    /**
     * Test que comprueba la existencia de una imagen en el disco local y que se ha creado el registro correctamente en BBDD
     */
    public function testOk1()
    {

        $this->json('POST', '/api/v1/file', [

            'files' => [
                '0' => [
                    'file' => UploadedFile::fake()->image('imagenPrueba.jpg')]],
            'type' => 'image'])
            ->assertJsonFragment(["error"=>200]);

        $imagenTranslation = FileTranslation::where('name', 'imagenPrueba.jpg')->first();

        $imagen = File::where('id', $imagenTranslation->file_id)->first();

        Storage::assertExists($imagen->url_file);

        $this->assertDatabaseHas('mo_file', [
            'url_file' => $imagen->url_file
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => 'imagenPrueba.jpg',
        ]);

    }

    /**
     * Test que comprueba que devuelve datos del archivo subido.
     * Comprueba la existencia de una imagen en el disco local y que se ha creado el registro correctamente en BBDD
     */
    public function testOk2()
    {

        $this->json('POST', '/api/v1/file', [

            'files' => [
                '0' => [
                    'file' => UploadedFile::fake()->image('imagenPrueba.jpg')]],
            'type' => 'image'])
            ->assertJsonStructure(["error",'data' => [0 =>['file' => [0 => ['id','order','url_file','type','lang']]]]]);

        $imagenTranslation = FileTranslation::where('name', 'imagenPrueba.jpg')->first();

        $imagen = File::where('id', $imagenTranslation->file_id)->first();

        Storage::assertExists($imagen->url_file);

        $this->assertDatabaseHas('mo_file', [
            'url_file' => $imagen->url_file
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => 'imagenPrueba.jpg',
        ]);

    }

    /**
     * Test para comprobar que sube y guarda correctamente imagen y video a la vez y devuelve datos de ambos archivos,
     * y comprueba en base de datos que se han creado campos file_name, file_size, file_dimensions y mimetype
     *
     */
    public function testOk3()
    {

        $this->json('POST', '/api/v1/file', [

            'files' => [
                '0' => [
                    'file' => UploadedFile::fake()->image('imagenPrueba.jpg'),
                    'order' => 1],
                '1' => [
                    'file' => UploadedFile::fake()->image('imagenPrueba.mp4')]],
            'type' => 'global',
            'private' => 0])
            ->assertJsonFragment(['error' => 200,
                'name' => 'imagenPrueba.jpg',
                'name' => 'imagenPrueba.mp4',
                'order' => 1,
                ]);

        $imagenTranslation = FileTranslation::where('name', 'imagenPrueba.jpg')->first();

        $imagen = File::where('id', $imagenTranslation->file_id)->first();

        Storage::assertExists($imagen->url_file);

        $this->assertDatabaseHas('mo_file', [
            'url_file' => $imagen->url_file,
            'order' => $imagen->order,
            'private' => $imagen->private,
            'file_name' => $imagen->file_name,
            'file_size' => $imagen->file_size,
            'file_dimensions' => $imagen->file_dimensions,
            'mimetype' => $imagen->mimetype,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => 'imagenPrueba.jpg',
        ]);

        $videoTranslation = FileTranslation::where('name', 'imagenPrueba.mp4')->first();

        $video = File::where('id', $videoTranslation->file_id)->first();

        Storage::assertExists($video->url_file);

        $this->assertDatabaseHas('mo_file', [
            'url_file' => $video->url_file,
            'order' => $video->order,
            'private' => $video->private,
            'file_name' => $video->file_name,
            'file_size' => $video->file_size,
            'file_dimensions' => $video->file_dimensions,
            'mimetype' => $video->mimetype,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => 'imagenPrueba.mp4',
        ]);

    }

}
