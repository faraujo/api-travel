<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SearchShowRoomTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    // Se envía una consulta sin parámetros sin el parámetro info_only
    public function testBad1()
    {

        $this->json('GET', '/api/v1/search/room', [

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "subchannel_id" => ["The subchannel id field is required."],
                "hotel_id" => ["The hotel id field is required."],
                "number_room" => ["The number room field is required unless info only is in 1."],
                "date_start" => ["The date start field is required unless info only is in 1."],
                "date_end" => ["The date end field is required unless info only is in 1."],
                "adult" => ["The adult field is required unless info only is in 1."]
            ]
        ]]);
    }

    //Consulta de un hotel_id inexistente sin el parámetro info_only

    public function testBad2()
    {
        // Se especifica parametro hotel_id incorrecto
        $this->json('GET', '/api/v1/search/room', [
            'hotel_id' => '-1'
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "subchannel_id" => ["The subchannel id field is required."],
                "hotel_id" => ["The hotel id must be at least 0."],
                "number_room" => ["The number room field is required unless info only is in 1."],
                "date_start" => ["The date start field is required unless info only is in 1."],
                "date_end" => ["The date end field is required unless info only is in 1."],
                "adult" => ["The adult field is required unless info only is in 1."]
            ]
        ]]);
    }

    //Consulta de un subchannel_id inexistente sin el parámetro info_only
    public function testBad3()
    {
        // Se especifica parametro subchannel_id incorrecto
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => '-1'
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "subchannel_id" => ["The selected subchannel id is invalid.", "The subchannel id must be at least 0."],
                "hotel_id" => ["The hotel id field is required."],
                "number_room" => ["The number room field is required unless info only is in 1."],
                "date_start" => ["The date start field is required unless info only is in 1."],
                "date_end" => ["The date end field is required unless info only is in 1."],
                "adult" => ["The adult field is required unless info only is in 1."]
            ]
        ]]);
    }

    // Se especifican parámetros en formato incorrecto sin el parámetro info_only

    public function testBad4()
    {

        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => 'a',
            'hotel_id' => 'b',
            'category_id' => 'c',
            'number_room' => 'd',
            'date_start' => '2022/01/02',
            'date_end' => '2022/01/02',
            'adult' => 'e',
            'child' => 'f'

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "adult" => ["The adult must be an integer."],
                "category_id" => ["The category id must be an integer.", "The selected category id is invalid."],
                "child" => ["The child must be an integer."],
                "date_end" => ["The date end does not match the format Y-m-d.", "The date end must be a date after date start."],
                "date_start" => ["The date start does not match the format Y-m-d."],
                "hotel_id" => ["The hotel id must be an integer."],
                "number_room" => ["The number room must be an integer."],
                "subchannel_id" => ["The selected subchannel id is invalid.", "The subchannel id must be an integer."],
                "ages.child.age"=>["The ages.child.age is required"],

            ]
        ]]);
    }

    /**
     * Se realiza una consulta sin especificar la fecha de entrada sin el parámetro info_only
     */
    public function testBad5()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'number_room' => rand(),
            'date_end' => date("Y-m-d", rand(0, time())),
            'adult' => rand(),
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'date_start' => ['The date start field is required unless info only is in 1.'],
                'date_end' => ['The date end must be a date after date start.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta sin especificar la fecha de salida sin el parámetro info_only
     */

    public function testBad6()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'number_room' => rand(),
            'date_start' => date("Y-m-d"),
            'adult' => rand(),
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'date_end' => ['The date end field is required unless info only is in 1.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta especificando una fecha de entrada posterior a la fecha de salida sin el parámetro info_only
     */

    public function testBad7()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'number_room' => rand(),
            'date_start' => '2021-01-05',
            'date_end' => '2021-01-04',
            'adult' => rand(),
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'date_end' => ['The date end must be a date after date start.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta especificando la misma fecha de entrada y salida sin el parámetro info_only
     */
    public function testBad8()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'number_room' => rand(),
            'date_start' => '2021-01-05',
            'date_end' => '2021-01-05',
            'adult' => rand(),
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'date_end' => ['The date end must be a date after date start.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta especificando una fecha de entrada anterior a hoy sin el parámetro info_only
     */
    public function testBad9()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'number_room' => rand(),
            'date_start' => '1999-12-12',
            'date_end' => '2021-01-05',
            'adult' => rand(),
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'date_start' => ['The date start must be a date after or equal to today.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta sin especificar el número de habitaciones sin el parámetro info_only
     */
    public function testBad10()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
            'adult' => rand(),
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'number_room' => ['The number room field is required unless info only is in 1.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta sin especificar el número de adultos sin el parámetro info_only
     */
    public function testBad11()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'number_room' => rand(),
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'adult' => ['The adult field is required unless info only is in 1.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta especificando un número de niños sin especificar el número de adultos sin el parámetro info_only
     */
    public function testBad12()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => $category->id,
            'number_room' => rand(),
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
            'child' => rand()
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'adult' => ['The adult field is required unless info only is in 1.'],
                "ages.child.age"=>["The ages.child.age is required"]
            ]
        ]]);
    }

    /**
     * Se realiza una consulta especificando un category_id inexistente sin el parámetro info_only
     */
    public function testBad13()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);

        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'category_id' => 999999999999,
            'number_room' => rand(),
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
            'adult' => 1,
            'child' => 0
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'category_id' => ['The selected category id is invalid.'],
            ]
        ]]);
    }

    /**
     * Se especifica un parámetro lang erroneo
     */
    public function testBad14()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'number_room' => rand(),
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
            'adult' => 1,
            'child' => 0,
            'lang' => 'asdf'
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'lang' => ['The lang is not exists'],
            ]
        ]]);
    }

    /**
     * Se especifica un parámetro info_only_incorrecto
     */
    public function testBad15()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $this->json('GET', '/api/v1/search/room', [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'number_room' => rand(),
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
            'adult' => 1,
            'child' => 0,
            'info_only' => 6,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                'info_only' => ['The info only field must be true or false.'],
            ]
        ]]);
    }

    /**
     * Se realiza una consulta especificando un room_id inexistente sin el parámetro info_only
     */
    public function testOk1()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $id = rand();
        $this->json('GET', '/api/v1/search/room/' . $id, [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'number_room' => rand(),
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
            'adult' => 1,
            'child' => 0
        ])->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Se especifica un parámetro category id en la ficha de hotel que no coincide con su category_id
     */
    public function testOk2()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);
        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,

        ]);
        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $category = factory(App\Models\tenant\HotelRoomCategory::cLass)->create();
        $hotel_room = factory(App\Models\tenant\HotelRoom::cLass)->create([
                'hotel_id' => $product->id,
            ]

        );
        $this->json('GET', '/api/v1/search/room/' . $hotel_room->id, [
            'subchannel_id' => $subchannel->id,
            'hotel_id' => $product->id,
            'number_room' => rand(),
            'category_id' => $category->id,
            'date_start' => '2050-12-11',
            'date_end' => '2050-12-12',
            'adult' => 1,
            'child' => 0
        ])->assertExactJson(['error' => 200, 'data' => []]);
    }


}