
<?php

use App\Models\tenant\Settings;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Carbon\Carbon;

class SearchCrossPurchaseTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta compra cruzada de un producto sin mandar parametros requeridos
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/search/crosspurchase', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'product_id' => ['The product id field is required.']
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando tipo de dato incorrecto de los parametros requeridos
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/search/crosspurchase', [
            'product_id' => 'r',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'product_id' => ['The product id must be an integer.', 'The selected product id is invalid.']
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando tipo de dato incorrecto de los parametros opcionales
     */
    public function testBad3()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create();
        $product = factory(\App\Models\tenant\Product::class)->create();
        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $product->id,
            'products' => 'r',

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'products' => ['The products must be an array.']
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando parametro lang incorrecto
     */
    public function testBad4()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create();
        $product = factory(\App\Models\tenant\Product::class)->create();
        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $product->id,
            'lang' => 'Incorrecta',

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'lang' => ['The lang is not exists']
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando datos incorrecto de los parametros requeridos
     */
    public function testBad5()
    {

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'product_id' => 9999999999,
            'products' => [
                '0' => 8888888888,
            ]

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'product_id' => ['The selected product id is invalid.'],
                    'products.0' => ['The selected products.0 is invalid.'],
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando parametros duplicados en el parametro opcional products
     */
    public function testBad6()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create();
        $productFactory = factory(\App\Models\tenant\Product::class)->create();
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create();

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'products' => [
                '0' => $productFactory2->id,
                '1' => $productFactory2->id
            ]

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'products.0' => ['The products.0 field has a duplicate value.'],
                    'products.1' => ['The products.1 field has a duplicate value.']
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando formato de fecha incorrecta
     */
    public function testBad7()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create();
        $productFactory = factory(\App\Models\tenant\Product::class)->create();

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => 'formato incorrecto'

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'date' => ['The date does not match the format Y-m-d.', 'The date is not a valid date.', 'The date must be a date after or equal to today.'],
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando formato de fecha incorrecta
     */
    public function testBad8()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create();
        $productFactory = factory(\App\Models\tenant\Product::class)->create();

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2000-01-01'

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'date' => ['The date must be a date after or equal to today.'],
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada de un producto mandando limite incorrecto
     */
    public function testBad9()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create();
        $productFactory = factory(\App\Models\tenant\Product::class)->create();

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-01-01',
            'limit' => 'r',

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'limit' => ['The limit must be an integer.'],
                ]
            ]]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos
     */
    public function testOk1()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create();
        $productFactory = factory(\App\Models\tenant\Product::class)->create();

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], 'total_results' => 0]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos
     */
    public function testOk2()
    {
        
        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([
        ]);

        $productLocationFactory = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory2 = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([

        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2018-01-01',
            'date_end' => '2099-12-31',
            'currency_id' => 1,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);


        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,

        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 1]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos pero con producto de compra cruzada sin tarifa
     */
    public function testOk3()
    {
        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);

        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([

        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2018-01-01',
            'date_end' => '2099-12-31',
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([
        ]);

        $productLocationFactory = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory2 = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 0]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa pero sin traduccion
     */
    public function testOk4()
    {
        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 2,
        ]);

        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([

        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2018-01-01',
            'date_end' => '2099-12-31',
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([
        ]);

        $productLocationFactory = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory2 = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'limit' => 5,
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 0]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa con fecha pasada
     */
    public function testOk5()
    {
        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);

        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([

        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2000-01-01',
            'date_end' => '2000-12-31',
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([
        ]);

        $productLocationFactory = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory2 = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 0]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa posterior a fecha actual
     */
    public function testOk6()
    {
        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);

        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([

        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2099-01-01',
            'date_end' => '2099-12-31',
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([
        ]);

        $productLocationFactory = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory2 = factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 0]);
    }
    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa en fecha especificada, se prueba filtro location
     */
    public function testOk7()
    {
        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);

        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2099-01-01',
            'date_end' => '2099-12-31',
            'currency_id' => 1,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $locationFactory2 = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 1]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 1]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa en fecha especificada,
     * con el producto en la tabla mo_view_product. Debe mostrar los datos de mo_view_product_translation, tanto traduccion como del archivo relacionado
     *
     */
    public function testOk8()
    {
        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product',
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);

        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2099-01-01',
            'date_end' => '2099-12-31',
            'currency_id' => 1,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $locationFactory2 = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        //el producto que se muestra en compra cruzada tiene relacion en tabla mo_view_product, debe mostrarse la traduccion de mo_view_product_translation,
        $viewProductFactory = factory(\App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory2->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
        ]);

        $viewProductTranslationFactory = factory(\App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProductFactory->id,
            'name' => 'name viewproduct',
            'language_id' => 1,
        ]);

        $file1 = factory(\App\Models\tenant\File::class)->create([
            'url_file' => 'url de archivo product',
        ]);

        $fileTranslation1 = factory(\App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $file1->id,
        ]);

        $productFile = factory(\App\Models\tenant\ProductFile::class)->create([
            'product_id' => $productFactory2->id,
            'file_id' => $file1->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            //devuelve datos de viewproduct y url_file del producto general
            ->assertJsonFragment(['error' => 200, 'total_results' => 1, 'name' => $viewProductTranslationFactory->name]);

        //crea archivo y lo relaciona al producto en la tabla viewproduct_file, debe mostrar la url_file del archivo relacionado en esta tabla
        $file2 = factory(\App\Models\tenant\File::class)->create([
            'url_file' => 'url de archivo de viewproduct',
        ]);

        $fileTranslation2 = factory(\App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $file2->id,
        ]);

        $viewProductFile = factory(\App\Models\tenant\ViewProductFile::class)->create([
            'view_product_id' => $viewProductFactory->id,
            'file_id' => $file2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            //devuelve datos de viewproduct y url_file del producto general
            ->assertJsonFragment(['error' => 200, 'total_results' => 1,'name' => $viewProductTranslationFactory->name]);
    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa en fecha especificada,
     * con el producto en la tabla mo_view_product comprobando casos: producto relacionado con registro en mo_view_product y published a 1 debe mostrarse
     *
     */
    public function testOk9()
    {
        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product',
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);



        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([

        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2099-01-01',
            'date_end' => '2099-12-31',
            'currency_id' => 1,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $locationFactory2 = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        //el producto que se muestra en compra cruzada tiene relacion en tabla mo_view_product, debe mostrarse la traduccion de mo_view_product_translation,
        $viewProductFactory = factory(\App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory2->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
        ]);

        $viewProductTranslationFactory = factory(\App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProductFactory->id,
            'name' => 'name viewproduct',
            'language_id' => 1,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 1, 'name' => $viewProductTranslationFactory->name]);

    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa en fecha especificada,
     * con el producto en la tabla mo_view_product comprobando casos: producto relacionado con registro en mo_view_product y published a 0 no debe mostrarse
     *
     */
    public function testOk10()
    {
        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product',
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);



        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([

        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2099-01-01',
            'date_end' => '2099-12-31',
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        //el producto que se muestra en compra cruzada tiene relacion en tabla mo_view_product, debe mostrarse la traduccion de mo_view_product_translation,
        $viewProductFactory = factory(\App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory2->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 0,
        ]);

        $viewProductTranslationFactory = factory(\App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProductFactory->id,
            'name' => 'name viewproduct',
            'language_id' => 1,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 0]);

    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa en fecha especificada,
     * con el producto en la tabla mo_view_product comprobando casos: producto relacionado sin registro en mo_view_product y
     * view_blocked de subchannel a 1 no debe mostrarse,
     *
     */
    public function testOk11()
    {
        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'view_blocked' => 1,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product',
            'language_id' => 1,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);


        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([
            'subchannel_id' => $subchannelFactory->id
        ]);


        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2099-01-01',
            'date_end' => '2099-12-31',
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 0]);

    }

    /**
     * Test consulta compra cruzada mandando parametros requeridos correctos con producto de compra cruzada con tarifa en fecha especificada,
     * con el producto en la tabla mo_view_product comprobando casos: producto relacionado sin registro en mo_view_product y
     * view_blocked de subchannel a 0 debe mostrarse los datos generales del producto
     *
     */
    public function testOk12()
    {
        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);
        
        $contractModelFactory = factory(\App\Models\tenant\ContractModel::class)->create();

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'view_blocked' => 0,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\Type::class)->create();
        factory(\App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslationFactory1 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product',
            'language_id' => 1,
        ]);

        $productTranslationFactory2 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'name' => 'name product',
            'language_id' => 1,
        ]);


        $reservationFactory = factory(\App\Models\tenant\Reservation::class)->create([
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'operation_status_id' => 3,
        ]);
        factory(\App\Models\tenant\ReservationDetail::class)->create([
            'reservation_id' => $reservationFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory2->id,
            'operation_status_id' => 4,
        ]);

        $priceFactory = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => '2099-01-01',
            'date_end' => '2099-12-31',
            'currency_id' => 1,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        factory(\App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'location_id' => $locationFactory->id,
            'product_id' => $productFactory2->id,
        ]);

        $this->json('GET', '/api/v1/search/crosspurchase', [
            'subchannel_id' => $subchannelFactory->id,
            'product_id' => $productFactory->id,
            'date' => '2099-10-01',
        ])
            ->assertJsonFragment(['error' => 200, 'total_results' => 1,'name' => $productTranslationFactory1->name]);

    }
}