<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SearchShowPromotionsTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada con filtros y formato incorrecto
     */
    public function testBad1(){
        $this->json('GET', '/api/v1/search', [
            'client_type_id' => 'TEST',
            'payment_method_id' => 'TEST',
            'pickup_id' => 'TEST',
            'product_id' => 'TEST',
            'product_type_id' => 'TEST',
            'service_id' => 'TEST',
            'subchannel_id' => 'TEST',
            'state_id' => 'TEST',
            'device_id' => 'TEST',
            'lockers_validation' => 'TEST',
            'supervisor_validation' => 'TEST',
            'allow_date_change' => 'TEST',
            'allow_upgrade' => 'TEST',
            'allow_product_change' => 'TEST',
            'pay_difference' => 'TEST',
            'coupon_code' => 'TEST',
            'coupon_type' => 'TEST',
            'order' => 'TEST',
            'order_way' => 'TEST',
            'limit' => 'TEST',
            'page' => 'TEST',
            'file_types' => 'TEST',
        ])
            ->assertExactJson([
                "error"=>400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "file_types"=>["The file types must be an array."],
                    "limit"=>["The limit must be an integer."],
                    "page"=>["The page must be an integer."],
                    "subchannel_id"=>["The selected subchannel id is invalid.","The subchannel id must be an integer."]
                ]]
            ]);
    }

    /**
     * Prueba automatizada correcta sin filtros opcionales
     */
    public function testOk1(){

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel_factory->id
        ])
            ->assertJsonFragment([
                "error"=>200,

            ]);
    }

    /**
     * Prueba automatizada correcta sin filtros opcionales
     */
    public function testOk2(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
        ])
            ->assertJsonFragment([
                'id' => $promotion_factory->id

            ]);
    }


    /**
     * Prueba automatizada correcta con filtro opcional client_type_id sin relación con promociones
     */
    public function testOk3(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
        ])
            ->assertJsonFragment([
                'id' => $promotion_factory->id

            ]);
    }


    /**
     * Prueba automatizada correcta con filtro opcional client_type_id con relación con promociones
     */
    public function testOk4(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
        ])
            ->assertJsonFragment([
                'id' => $promotion_factory->id

            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk5(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id
        ])
            ->assertJsonFragment([
                'id' => $promotion_factory->id

            ]);
    }


    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk6(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id
        ])
            ->assertJsonFragment([
                'total_results' => 0
            ]);
    }


    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk7(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id
        ])
            ->assertJsonFragment([
                'id' => $promotion_factory->id
            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk8(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'lockers_validation' => 1,
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id
        ])
            ->assertJsonFragment([
                'id' => $promotion_factory->id
            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk9(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'lockers_validation' => 1,
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id,
            'lockers_validation' => '0',
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ])
            ->assertJsonFragment([
                'total_results' => 0
            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk10(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'lockers_validation' => 1,
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id,
            'lockers_validation' => '0',
            'supervisor_validation' => '0',
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ])
            ->assertJsonFragment([
                'total_results' => 0
            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk11(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'lockers_validation' => 1,
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id,
            'lockers_validation' => '0',
            'supervisor_validation' => '0',
            'allow_date_change' => '0',
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ])
            ->assertJsonFragment([
                'total_results' => 0
            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk12(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'lockers_validation' => 1,
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id,
            'lockers_validation' => '0',
            'supervisor_validation' => '0',
            'allow_date_change' => '0',
            'allow_upgrade' => '0',
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ])
            ->assertJsonFragment([
                'total_results' => 0
            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk13(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'lockers_validation' => 1,
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id,
            'lockers_validation' => '0',
            'supervisor_validation' => '0',
            'allow_date_change' => '0',
            'allow_upgrade' => '0',
            'allow_product_change' => '0',
            'pay_difference' => 1,
        ])
            ->assertJsonFragment([
                'total_results' => 0
            ]);
    }

    /**
     * Prueba automatizada correcta con filtros opcionales
     */
    public function testOk14(){

        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'front' => 1,
            'abbreviation' => 'TEST'
        ]);

        $subchannel_factory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $campaign_factory = factory(\App\Models\tenant\Campaign::class)->create([
            'date_start' => \Carbon\Carbon::now()->subDay()->startOfDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addMonth()->format('Y-m-d'),
            'active' => 1,

        ]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $promotion_type_translation = factory(\App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_type_id' => $promotion_type->id,
        ]);

        $campaign_translation_factory = factory(\App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaign_factory->id,
            'language_id' => $language_factory->id,
        ]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaign_factory->id,
            'visibility' => 1,
            'promotion_type_id' => $promotion_type->id,
            'apply_client_type_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'lockers_validation' => 1,
            'supervisor_validation' => 1,
            'allow_date_change' => 1,
            'allow_upgrade' => 1,
            'allow_product_change' => 1,
            'pay_difference' => 1,
        ]);

        $promotion_translation_factory = factory(\App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => $language_factory->id,
            'promotion_id' => $promotion_factory->id,
        ]);

        $promotion_subchannel_factory = factory(\App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotion_factory->id,
            'subchannel_id' => $subchannel_factory->id,
        ]);

        $promotion_range_sale_day = factory(\App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotion_factory->id,
            'sale_date_start' => \Carbon\Carbon::now()->subDays(1)->startOfDay(),
            'sale_date_end' => \Carbon\Carbon::now()->addMonth(),
        ]);

        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 1,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 2,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 3,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 4,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 5,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 6,
        ]);
        factory(\App\Models\tenant\PromotionSaleDay::class)->create([
            'promotion_range_sale_day_id' => $promotion_range_sale_day->id,
            'day_id' => 7,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([]);

        factory(\App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
        ]);

        $payment_method = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        factory(\App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotion_factory->id,
            'payment_method_id' => $payment_method->id,
        ]);

        $pickup_factory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        factory(\App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotion_factory->id,
            'pickup_id' => $pickup_factory->id,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        factory(\App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        factory(\App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
        ]);

        $state_factory = factory(\App\Models\tenant\State::class)->create([]);

        factory(\App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotion_factory->id,
            'state_id' => $state_factory->id,
        ]);

        $device_factory = factory(\App\Models\tenant\Device::class)->create([]);

        factory(\App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotion_factory->id,
            'device_id' => $device_factory->id,
        ]);



        $this->json('GET', '/api/v1/search/promotion', [
            'subchannel_id' => $subchannel_factory->id,
            'lang' => $language_factory->abbreviation,
            'client_type_id' => $client_type_factory->id,
            'payment_method_id' => $payment_method->id,
            'pickup_id' => $pickup_factory->id,
            'product_id' => $product_factory->id,
            'product_type_id' => $product_type_factory->id,
            'service_id' => $service_factory->id,
            'state_id' => $state_factory->id,
            'device_id' => $device_factory->id,
            'lockers_validation' => '0',
            'supervisor_validation' => '0',
            'allow_date_change' => '0',
            'allow_upgrade' => '0',
            'allow_product_change' => '0',
            'pay_difference' => '0',
        ])
            ->assertJsonFragment([
                'total_results' => 0
            ]);
    }

}