<?php

use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SearchShowPredictiveTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Búsqueda predictiva con filtros channel y date erróneos
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/search/predictive', [
            'subchannel_id' => 'poi',
            'date' => '2018zz-05-10',
        ])
            ->assertExactJson([
                'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                    'subchannel_id' => ["The subchannel id must be an integer."],
                    'date' => ["The date does not match the format Y-m-d.", "The date is not a valid date.",
                        "The date must be a date after or equal to today."],
                ]]
            ]);
    }

    /**
     * Búsqueda predictiva con filtro subchannel que no existe en BBDD
     */
    public function testBad2()
    {
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id
        ]);

        $subchannelFactory->delete();

        $this->json('GET', '/api/v1/search/predictive', [
            'subchannel_id' => $subchannelFactory->id,
        ])
            ->assertExactJson([
                'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                    'subchannel_id' => ["The selected subchannel id is invalid."],
                ]]
            ]);
    }

    /**
     * Búsqueda predictiva con filtro lang erróneo
     */
    public function testBad3()
    {

        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id
        ]);

        $this->json('GET', '/api/v1/search/predictive', [
            'subchannel_id' => $subchannelFactory->id,
            'lang' => 'FDF'
        ])
            ->assertExactJson([
                'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                    'lang' => ["The lang is not exists"]]]
            ]);
    }

    /**
     * Búsqueda predictiva sin resultados. Se crea un nuevo canal.
     */
    public function testBad4()
    {
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id
        ]);

        $this->json('GET', '/api/v1/search/predictive', [
            'subchannel_id' => $subchannelFactory->id,
        ])
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Búsqueda predictiva, sin filtro lang
     */
    public function testOK1()
    {

        $idioma_defecto = Settings::where('name', 'idioma_defecto_id')->first();
        $id_idioma = $idioma_defecto->value;
        $idioma = Language::where('id', $id_idioma)->first();

        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productTranslation = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $idioma->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/search/predictive', [
            'subchannel_id' => $subchannelFactory->id,
            'date' => $priceFactory->date_start,
        ])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'name' => $productTranslation->name,
            ]);
    }

    /**
     * Búsqueda predictiva, con filtro lang y sin filtro fecha
     */
    public function testOK2()
    {
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d')
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/search/predictive', [
            'subchannel_id' => $subchannelFactory->id,
            'lang' => 'PT'
        ])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'name' => $productTranslation3->name,
            ]);
    }

    /**
     * Búsqueda predictiva, con filtro lang y sin filtro fecha, añadiendo también etiquetas asociadas a productos
     */
    public function testOK3()
    {

        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d')
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
        ]);

        factory(App\Models\tenant\ProductTag::class)->create([
            'tag_id' => $tagFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $tagTranslation2 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 2,
            'tag_id' => $tagFactory->id,
        ]);

        $this->json('GET', '/api/v1/search/predictive', [
            'subchannel_id' => $subchannelFactory->id,
            'lang' => 'EN'
        ])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'name' => $productTranslation2->name,
                'id' => $tagFactory->id,
                'name' => $tagTranslation2->name,
            ]);
    }


}