<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;

class SearchShowProductTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta producto sin subchannel_id requerido
     */
    public function testBad1()
    {
        $product = factory(App\Models\tenant\Product::class)->create([
        ]);
        $this->json('GET', '/api/v1/search/'.$product->id, [
        ])->assertExactJson(['error' => 400,'error_description' => "The fields are not the required format", 'error_inputs' => [['subchannel_id' => ['The subchannel id field is required.']]]]);
    }

    /**
     * Test consulta producto con todos los parámetros posibles en formato incorrecto
     */
    public function testBad2()
    {
        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        //crea factories de moneda para comprobar validador de iso codes
        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $this->json('GET', '/api/v1/search/' . $product->id, [
            'date' => '2050/12/12',
            'subchannel_id' => 'rs',
            'lang' => 'ppp',
            'currency' => 'rrr',
            'location_id' => 'rr',
            'session' => 'rr',
            'avail' => 'rr',
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["avail"=>["The avail must be an integer."],"currency"=>["The currency is not exists"],
                "date"=>["The date does not match the format Y-m-d."],"lang"=>["The lang is not exists"],
                "location_id"=>["The location id must be an integer.","The selected location id is invalid."],
                "subchannel_id"=>["The selected subchannel id is invalid.","The subchannel id must be an integer."]]]]);
    }

    /**
     * Test consulta producto inexistente no devuelve datos
     */
    public function testOk1()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        /*$currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);*/

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        /*$currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);*/

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => 1,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        //devuelve datos porque consulta un producto con tarifa existente para la fecha de hoy, con mismo contract_model que el subcanal y asociado en
        //price_product con la locación y tipo de cliente creados
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'currency_id' => 1
        ])
            ->assertJsonFragment(['id' => $product->id]);

        //borra producto
        $product->delete();

        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'currency_id' => 1
        ])
            ->assertExactJson(['error' => 200,'data' => [],"total_results" => 0]);

    }

    /**
     * Test consulta producto con tarifa con diferente contract model que el canal
     */
    public function testOk2()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model2->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency2->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
        ])
            ->assertExactJson(['error' => 200,'data' => [],"total_results" => 0]);

    }

    /**
     * Test consulta producto sin tarifa asociada
     */
    public function testOk3()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency2->id,
        ]);

        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
        ])
            ->assertExactJson(['error' => 200,'data' => [],"total_results" => 0]);

    }

    /**
     * Test consulta producto sin tarifa para la fecha seleccionada
     */
    public function testOk4()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);
        factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 2,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);
        factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 3,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subDay(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay(1)->format('Y-m-d'),
            'currency_id' => $currency2->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'date' => Carbon::now()->addDays(2)->format('Y-m-d'),
        ])
            ->assertExactJson(['error' => 200,'data' => [],"total_results" => 0]);
    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, y tiene registro en viewproduct con published a 1 con lo que devuelve datos del producto visualizado por canal,
     * se prueba también filtro idioma
     */
    public function testOk5()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '10.00',
        ]);

        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $viewProduct = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $product->id,
            'subchannel_id' => $subchannel->id,
            'published' => 1,
        ]);

        $viewProductTranslation1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 1,
            'name' => 'es viewproduct'
        ]);

        $viewProductTranslation2 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 2,
            'name' => 'en viewproduct'
        ]);

        $viewProductTranslation3 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 3,
            'name' => 'pt viewproduct'
        ]);



        factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 1,
            'exchange' => '25.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 2,
            'exchange' => '25.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => 3,
            'exchange' => '25.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        //devuelve datos de producto visualizado por canal en español, comprueba tambien tarifa y avail del producto
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency_id' => $currency1->id,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $viewProductTranslation1->name,'avail' => $availability->avail]);

        //devuelve datos de producto visualizado por canal en ingles, comprueba tambien tarifa y avail del producto
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'en',
            'currency_id' => $currency1->id,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $viewProductTranslation2->name,'avail' => $availability->avail]);

        //devuelve datos de producto visualizado por canal en portugues, comprueba tambien tarifa y avail del producto
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'pt',
            'currency_id' => $currency1->id,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $viewProductTranslation3->name,'avail' => $availability->avail]);
    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, y tiene registro en viewproduct con published a 0 con lo que no debe devolver datos, ni de producto visualizado por canal ni de producto general
     */
    public function testOk6()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency2->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        $viewProduct = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $product->id,
            'subchannel_id' => $subchannel->id,
            'published' => 0,
        ]);

        $viewProductTranslation1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 1,
            'name' => 'es viewproduct'
        ]);

        $viewProductTranslation2 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 2,
            'name' => 'en viewproduct'
        ]);

        $viewProductTranslation3 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 3,
            'name' => 'pt viewproduct'
        ]);

        //no devuelve datos
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
        ])
            ->assertJsonFragment(['error' => 200,'data' => [],"total_results" => 0]);
    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 1, no debe mostrarse
     */
    public function testOk7()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency2->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        //no devuelve datos
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
        ])
            ->assertJsonFragment(['error' => 200,'data' => [],"total_results" => 0]);
    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto
     * de su tarifa y su disponibilidad y se prueba filtro idioma
     */
    public function testOk8()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISO_DOLAR'
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISO_PESO'
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISO_EURO'
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        //devuelve datos del producto general en español
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'avail' => $availability->avail,'net_price' => $price_product->net_price]);

        //devuelve datos del producto general en ingles
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'en',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation2->name,'avail' => $availability->avail,'net_price' => $price_product->net_price]);

        //devuelve datos del producto general en portugues
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'pt',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation3->name,'avail' => $availability->avail,'net_price' => $price_product->net_price]);
    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Hace prueba sin tarifa, con tarifa pero sin disponibilidad y con tarifa y disponibilidad
     */
    public function testOk9()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        //se consulta producto que no tiene tarifa establecida  ni disponibilidad, no devuelve datos
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency1->iso_code,
        ])
            ->assertExactJson(['error' => 200,'data' => [],"total_results" => 0]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        //se añade tarifa y ahora va a devolver datos del producto pero no su tarifa que para mostrarse necesita que tenga disponibilidad el producto
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'avail' => []]);

        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        //se añade disponibilidad y ahora va a devolver datos del producto, datos de su disponibilidad y de su tarifa
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'avail' => $availability->avail, 'net_price' => $price_product->net_price]);
    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro location_id asociando el producto a dos locaciones y filtrando por una de ellas
     */
    public function testOk10()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location1 = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        $location2 = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 2'
        ]);

        $location3 = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 3'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location1->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location2->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        //disponibilidad del producto en ambas locations
        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location1->id,
            'avail' => 20,
        ]);

        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location2->id,
            'avail' => 20,
        ]);

        //tarifa para el mismo producto en dos locaciones diferentes
        $price_product1 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location1->id,
            'net_price' => '5.00'
        ]);

        $price_product2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location2->id,
            'net_price' => '10.00'
        ]);

        //devuelve datos del producto general y de dos locaciones puesto que está asociado en la tabla product_location a ambas locaciones
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'id' => $location1->id,'name' => $location1->name,
                'id' => $location2->id,'name' => $location2->name,'avail' => $availability1->avail,'net_price' => $price_product1->net_price,
                'avail' => $availability2->avail,'net_price' => $price_product2->net_price]);

        //filtra por location_id
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'location_id' => $location1->id,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'id' => $location1->id,'name' => $location1->name,'avail' => $availability1->avail,'net_price' => $price_product1->net_price]);

        //filtra por location_id existente en base de datos pero que no está asociada al producto, no devuelve datos
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'location_id' => $location3->id,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['error' => 200,'data' => [],"total_results" => 0]);

    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro avail y filtro session que filtra tanto para disponiblidad como para tarifa
     */
    public function testOk11()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange1 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        //tarifa para el mismo producto en dos locaciones diferentes
        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '8.00'
        ]);

        //disponibilidad ilimitada, se solicita filtro avail mayor que campo avail pero manda campo unlimited, devuelve datos del producto
        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        //devuelve datos del producto general y el id de la disponibilidad del producto
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'avail' => 10,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'id' => $availability1->id,'net_price' => $priceProduct->net_price]);

        $availability1->delete();

        //closed a 1 el producto no tiene disponibilidad para ninguna fecha puesto que fecha y sesión están a null ni para ninguna cantidad de personas
        //puesto que el producto está cerrado
        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 1,
            'unlimited' => 0,
        ]);

        //no devuelve datos del producto
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'avail' => 10,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(["data"=>[],"error"=>200,"total_results"=> 0]);

        $availability2->delete();

        //la disponibilidad tiene campo closed a 0 y unlimited a 0, ni está cerrado ni tiene capacidad ilimitada, y tiene disponibles 30 entradas
        $availability3 = factory(App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 30,
            'closed' => 0,
            'unlimited' => 0,
        ]);

        //pide entradas para 30 y devuelve disponibilidad, por defecto con fecha de hoy
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'avail' => 30,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'id' => $availability3->id]);

        //pide entradas para 31 y no devuelve disponibilidad, por defecto con fecha de hoy
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'avail' => 31,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(["data"=>[],"error"=>200,"total_results" => 0]);

        $availability3->delete();

        //borramos registro de precio anterior para probar filtro
        $priceProduct->delete();

        $availability4 = factory(App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '05:00:00',
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 30,
            'closed' => 0,
            'unlimited' => 0,
        ]);

        $availability5 = factory(App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '06:00:00',
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 30,
            'closed' => 0,
            'unlimited' => 0,
        ]);

        $priceProduct2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'session' => '05:00:00',
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '10.00',
        ]);

        $priceProduct3 = factory(App\Models\tenant\PriceProduct::class)->create([
            'session' => '06:00:00',
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '20.00',
        ]);

        //no se manda filtro de session, devuelve ambas disponibilidades y precios para todas las sesiones
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'id' => $availability4->id,'id' => $availability5->id,
                'net_price' => $priceProduct2->net_price,'net_price' => $priceProduct3->net_price]);
    }

    /**
     * Test consulta producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas y con un servicio asociado, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se comprueban datos de servicios asociados: hace prueba sin tarifa, con tarifa pero sin disponibilidad y con tarifa y disponibilidad
     */
    public function testOk12()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $service = factory(App\Models\tenant\Service::class)->create([
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $serviceTranslation1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 1,
            'name' => 'es name service'
        ]);
        $serviceTranslation2 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 2,
            'name' => 'en name service'
        ]);
        $serviceTranslation3 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 3,
            'name' => 'pt name service'
        ]);

        factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        //crea disponibilidad para producto pero no para servicio
        $availability_product = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        //crea tarifa para producto pero no para servicio
        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        //se consulta producto que no tiene tarifa establecida  ni disponibilidad, no devuelve datos
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency1->iso_code,
        ])
            //devuelve datos del producto ya que tiene tarifa pero no del servicio porque no tiene tarifa
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'avail' => $availability_product->avail,'net_price' => $price_product->net_price,
                'service' => []]);

        //crea tarifa para servicio pero no disponibilidad, devuelve datos generales del servicio pero no muestra la tarifa ya que debe tener disponibilidad
        $price_service = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '8.00',
        ]);

        //se añade tarifa y ahora va a devolver datos del servicio pero no su tarifa que para mostrarse necesita que tenga disponibilidad
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'avail' => $availability_product->avail,'net_price' => $price_product->net_price,
                'id' => $service->id,'name' => $serviceTranslation1->name,'avail' => []]);

        $availability_service = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        //se añade disponibilidad y ahora va a devolver datos del servicio, datos de su disponibilidad y de su tarifa
        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'avail' => $availability_product->avail, 'net_price' => $price_product->net_price,
                'id' => $service->id,'name' => $serviceTranslation1->name,'avail' => $availability_service->avail, 'net_price' => $price_service->net_price]);
    }

    public function testOk13(){
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);


        factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $product->id,
            'service_id' => 1,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        //crea disponibilidad para producto pero no para servicio
        $availability_product = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        //crea tarifa para producto pero no para servicio
        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);
        $pickupFactory = factory(App\Models\tenant\TransportationLocation::class)->create([]);
        $price_service = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => 1,
            'pickup_id' => $pickupFactory->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '8.00',
        ]);

        $availability_service = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => 1,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $this->json('GET', '/api/v1/search/'.$product->id, [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id,'name' => $productTranslation1->name,'avail' => $availability_product->avail, 'net_price' => $price_product->net_price,
                'id' => 1,'avail' => $availability_service->avail, 'net_price' => $price_service->net_price]);
    }

    /**
     * Prueba automatizada envio de filtro file_types pidiendo archivos de un tipo y de varios
     */
    public function testOk14()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'TEST',
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1',
            'depends_on' => $product->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => 1.00,
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => 0.50,
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => 0.25,
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation1 = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'name file',
            'description' => 'description file',
            'alternative_text' => 'alternative text',
        ]);

        $productFile = factory(App\Models\tenant\ProductFile::class)->create([
            'file_id' => $fileFactory->id,
            'product_id' => $product->id,
        ]);

        $this->json('GET', '/api/v1/search/' . $product->id, [
            'subchannel_id' => $subchannel->id,
            'file_types' => [0 => 3],
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name,'alternative_text' => $fileFactoryTranslation1->alternative_text]);

        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 7,
            'url_file' => UploadedFile::fake()->image('avatar2.jpg'),
        ]);

        $fileFactoryTranslation2 = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => 1,
            'name' => 'name file 2',
            'description' => 'description file 2',
            'alternative_text' => 'alternative text 2',
        ]);

        $productFile2 = factory(App\Models\tenant\ProductFile::class)->create([
            'file_id' => $fileFactory2->id,
            'product_id' => $product->id,
        ]);

        $this->json('GET', '/api/v1/search/' . $product->id, [
            'subchannel_id' => $subchannel->id,
            'file_types' => [0 => 3,1 => 7],
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name,'alternative_text' => $fileFactoryTranslation1->alternative_text, 'alternative_text' => $fileFactoryTranslation2->alternative_text]);
    }
}
