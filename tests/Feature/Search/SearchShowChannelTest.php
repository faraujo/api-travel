<?php

use App\Models\tenant\Exchange;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Carbon\Carbon;

class SearchShowChannelTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Función para convertir moneda
     *
     * Convierte un valor que viene de base de datos en una moneda determinada a la moneda que especifique el usuario o a la moneda por defecto
     *
     * @param $valor
     * @param $moneda_actual
     * @param $moneda_convertir
     * @return string
     */
    private function conversor($valor, $moneda_actual, $moneda_convertir)
    {
        $fecha_hora = Carbon::now()->format('Y-m-d H:i:s');

        $cambio_actual = Exchange::where('currency_id', $moneda_actual)
            ->where('date_time_start', '<=', $fecha_hora)
            ->orderBy('date_time_start', 'desc')
            ->first()
            ->exchange;
//dd($moneda_convertir);
        $cambio_moneda_convertir = Exchange::where('currency_id', $moneda_convertir)
            ->where('date_time_start', '<=', $fecha_hora)
            ->orderBy('date_time_start', 'desc')
            ->first()
            ->exchange;

        if ($cambio_actual != 0) {
            $valor_convertido = number_format($valor / $cambio_actual * $cambio_moneda_convertir, 2, '.', '');

        } else {
            $valor_convertido = $valor;
        }

        return $valor_convertido;
    }


    /**
     * Búsqueda de productos para un subcanal sin enviar parámetros requeridos
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/search', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'subchannel_id' => ['The subchannel id field is required.'],
            ]]]);
    }

    /**
     * Búsqueda de productos enviando formato incorrecto en todos los posibles filtros
     */
    public function testBad2()
    {
        //crea factories de moneda con iso codes para poder comprobar validador de iso code
        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => 's',
            'type_id' => 's',
            'category_id' => 's',
            'avail' => 's',
            'money' => 's',
            'date' => 's',
            'page' => 's',
            'limit' => 's',
            'paginate' => 's',
            'depends_on' => 's',
            'tag_id' => 's',
            'location_id' => 's',
            'lang' => 'asdf',
            //manda iso_code inexistente
            'currency' => 'erroneo',
            'salable' => 'aaa'
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [[
                "avail" => ["The avail must be an integer."],
                "category_id" => ["The category id must be an integer.", "The selected category id is invalid."],
                "currency" => ["The currency is not exists"],
                "date" => ["The date does not match the format Y-m-d.", "The date is not a valid date."],
                "depends_on" => ["The depends on must be an integer.", "The selected depends on is invalid."],
                "lang" => ["The lang is not exists"],
                "limit" => ["The limit must be an integer."],
                "location_id" => ["The location id must be an integer.", "The selected location id is invalid."],
                "money" => ["The money must be an integer."],
                "page" => ["The page must be an integer."],
                "subchannel_id" => ["The selected subchannel id is invalid.", "The subchannel id must be an integer."],
                "salable" => ["The salable field must be true or false."],
                "tag_id" => ["The tag id must be an integer."], "type_id" => ["The selected type id is invalid.", "The type id must be an integer."]]]]);
    }

    /**
     * Manda requeridos y campo type_id, category_id, depends_on, tag_id y location_id con ids que no están en base de datos
     */
    public function testBad3()
    {
        $subchannelFactory = Factory(App\Models\tenant\SubChannel::class)->create();
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannelFactory->id,
            'type_id' => 9999999999999,
            'category_id' => 99999999999999,
            'depends_on' => 9999999999,
            'tag_id' => 99999999,
            'location_id' => 999999999,
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [["category_id" => ["The selected category id is invalid."],
                "depends_on" => ["The selected depends on is invalid."], "location_id" => ["The selected location id is invalid."], "tag_id" => ["The selected tag id is invalid."],
                "type_id" => ["The selected type id is invalid."]]]]);
    }


    //test correctos

    /**
     * Test consulta producto existente devuelve datos, se borra y ya no devuelve datos
     */
    public function testOk1()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        //devuelve datos porque consulta un subcanal con un producto con tarifa existente para la fecha de hoy, con mismo contract_model que el subcanal y asociado en
        //price_product con la locación y tipo de cliente creados
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id]);

        //borra producto
        $product->delete();

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);

    }

    /**
     * Test consulta un subcanal con un producto con tarifa con diferente contract model que el canal, no devuelve datos
     */
    public function testOk2()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model2->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR',
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);

    }

    /**
     * Test consulta un subcanal con un producto sin tarifa asociada, no devuelve datos
     */
    public function testOk3()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);

    }

    /**
     * Test consulta un subcanal con un producto sin tarifa para la fecha seleccionada, no devuelve datos
     */
    public function testOk4()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subDay(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'date' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);
    }

    /**
     * Test consulta un subcanal con un producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, y tiene registro en viewproduct con published a 1 con lo que devuelve datos del producto visualizado por canal,
     * se prueba también filtro idioma
     */
    public function testOk5()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'exchange' => '1.00',
            'currency_id' => $currency->id,
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '10.00',
        ]);

        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $viewProduct = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $product->id,
            'subchannel_id' => $subchannel->id,
            'published' => 1,
        ]);

        $viewProductTranslation1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 1,
            'name' => 'es viewproduct'
        ]);

        $viewProductTranslation2 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 2,
            'name' => 'en viewproduct'
        ]);

        $viewProductTranslation3 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 3,
            'name' => 'pt viewproduct'
        ]);

        //devuelve datos de producto visualizado por canal en español, comprueba tambien tarifa y avail del producto
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $viewProductTranslation1->name, 'net_price' => $price_product->net_price, 'avail' => $availability->avail]);

        //devuelve datos de producto visualizado por canal en ingles, comprueba tambien tarifa y avail del producto
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'en',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $viewProductTranslation2->name, 'net_price' => $price_product->net_price, 'avail' => $availability->avail]);

        //devuelve datos de producto visualizado por canal en portugues, comprueba tambien tarifa y avail del producto
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'pt',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $viewProductTranslation3->name, 'net_price' => $price_product->net_price, 'avail' => $availability->avail]);
    }

    /**
     * Test consulta un subcanal con un producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, y tiene registro en viewproduct con published a 0 con lo que no debe devolver datos, ni de producto visualizado por canal ni de producto general
     */
    public function testOk6()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        $viewProduct = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $product->id,
            'subchannel_id' => $subchannel->id,
            'published' => 0,
        ]);

        $viewProductTranslation1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 1,
            'name' => 'es viewproduct'
        ]);

        $viewProductTranslation2 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 2,
            'name' => 'en viewproduct'
        ]);

        $viewProductTranslation3 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'view_product_id' => $viewProduct->id,
            'language_id' => 3,
            'name' => 'pt viewproduct'
        ]);

        //no devuelve datos
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);
    }

    /**
     * Test consulta un subcanal con un producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 1, no debe mostrarse
     */
    public function testOk7()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 1,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
        ]);

        //no devuelve datos
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);
    }

    /**
     * Test consulta un subcanal con un producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto
     * de su tarifa y su disponibilidad y se prueba filtro idioma
     */
    public function testOk8()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        //devuelve datos del producto general en español
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);

        //devuelve datos del producto general en ingles
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'en',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation2->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);

        //devuelve datos del producto general en portugues
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'pt',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation3->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);
    }

    /**
     * Test consulta un subcanal con un producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Hace prueba sin tarifa, con tarifa pero sin disponibilidad y con tarifa y disponibilidad
     */
    public function testOk9()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);
        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        //se consulta producto que no tiene tarifa establecida  ni disponibilidad, no devuelve datos
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        //se añade tarifa y ahora va a devolver datos del producto pero no su tarifa que para mostrarse necesita que tenga disponibilidad el producto
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => []]);

        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        //se añade disponibilidad y ahora va a devolver datos del producto, datos de su disponibilidad general y de su tarifa general
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);

        $availability->delete();
        $price_product->delete();

        //crea disponibilidad para fecha de hoy a las 10:00 y crea 2 priceproduct uno general y otro para las 11:00, muestra disponibilidad y tarifa general
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '10:00'
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '11:00'
        ]);

        $price_product2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product2->net_price]);

        $availability->delete();
        $price_product->delete();
        $price_product2->delete();

        //crea disponibilidad general y tarifa a las 10:00, no se debe mostrar tarifa ni disponibilidad sólo los datos del producto
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '10:00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => []]);

        $availability->delete();
        $price_product->delete();

        //crea disponibilidad a las 10:00 de hoy y tarifa a las 10:00 de hoy, debe mostrar tarifa y disponibilidad
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '10:00'
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '10:00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);

        $availability->delete();
        $price_product->delete();

        //crea disponibilidad a las 10:00 de hoy y tarifa a las 10:00 de hoy y una tarifa general, debe mostrar tarifa de las 10:00
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '10:00'
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '10:00'
        ]);

        $price_product2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);

        $availability->delete();
        $price_product->delete();
        $price_product2->delete();


        //pruebas de disponibilidad para servicios


        //crea tarifa para producto para que el producto siempre se devuelva
        $price_product_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        //crea servicios, traducciones y asociación con productos
        $service = factory(App\Models\tenant\Service::class)->create([
        ]);
        //crea sus traducciones si no las tiene no muestra el servicio
        $serviceTranslation1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 1,
            'name' => 'es name service'
        ]);
        $serviceTranslation2 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 2,
            'name' => 'en name service'
        ]);
        $serviceTranslation3 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 3,
            'name' => 'pt name service'
        ]);

        $productService = factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id
        ]);

        //se consulta subcanal que devuelve un producto con un servicio asociado que no es transportacion que no tiene tarifa establecida  ni disponibilidad, no devuelve datos del servicio
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, "service" => []]);

        //se añade tarifa y ahora va a devolver datos del servicio pero no su tarifa que para mostrarse necesita que tenga disponibilidad
        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $service->id, 'name' => $serviceTranslation1->name, 'avail' => []]);

        //se añade disponibilidad y ahora va a devolver datos del servicio, datos de su disponibilidad general y de su tarifa general
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $service->id, 'name' => $serviceTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);

        $availability->delete();
        $price_product->delete();

        //crea disponibilidad para fecha de hoy a las 10:00 y crea 2 priceproduct uno general y otro para las 11:00, muestra disponibilidad y tarifa general
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '10:00'
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '11:00'
        ]);

        $price_product2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $service->id, 'name' => $serviceTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product2->net_price]);

        $availability->delete();
        $price_product->delete();
        $price_product2->delete();

        //crea disponibilidad general y tarifa a las 10:00, no se debe mostrar tarifa ni disponibilidad sólo los datos del servicio
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '10:00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $service->id, 'name' => $serviceTranslation1->name, 'avail' => []]);

        $availability->delete();
        $price_product->delete();

        //crea disponibilidad a las 10:00 de hoy y tarifa a las 10:00 de hoy, debe mostrar tarifa y disponibilidad
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '10:00'
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '10:00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $service->id, 'name' => $serviceTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product->net_price]);

        $availability->delete();
        $price_product->delete();

        //crea disponibilidad a las 10:00 de hoy y tarifa a las 10:00 de hoy y una tarifa general, debe mostrar tarifa general
        $availability = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
            'unlimited' => 1,
            'location_id' => $location->id,
            'avail' => 20,
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '10:00'
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
            'session' => '10:00'
        ]);

        $price_product2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '5.00',
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'lang' => 'es',
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $service->id, 'name' => $serviceTranslation1->name, 'avail' => $availability->avail, 'net_price' => $price_product2->net_price]);

        $availability->delete();
        $price_product->delete();
        $price_product2->delete();
    }


    /**
     * Test consulta un subcanal con un producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro location_id asociando el producto a dos locaciones y filtrando por una de ellas
     */
    public function testOk10()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location1 = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        $location2 = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 2'
        ]);

        $location3 = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 3'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location1->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location2->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        //disponibilidad del producto en ambas locations
        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location1->id,
            'avail' => 20,
        ]);

        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'unlimited' => 1,
            'location_id' => $location2->id,
            'avail' => 20,
        ]);

        //tarifa para el mismo producto en dos locaciones diferentes
        $price_product1 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location1->id,
            'net_price' => '5.00'
        ]);

        $price_product2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location2->id,
            'net_price' => '10.00'
        ]);

        //devuelve datos del producto general y de dos locaciones puesto que está asociado en la tabla product_location a ambas locaciones
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'id' => $location1->id, 'name' => $location1->name,
                'id' => $location2->id, 'name' => $location2->name, 'avail' => $availability1->avail, 'net_price' => $price_product1->net_price,
                'avail' => $availability2->avail, 'net_price' => $price_product2->net_price]);

        //filtra por location_id
        /*$this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            'location_id' => $location1->id,
            'currency' => $currency->iso_code,
        ])
            ->seeJsonContains(['id' => $product->id, 'id' => $location1->id, 'name' => $location1->name, 'avail' => $availability1->avail, 'net_price' => $price_product1->net_price,
                'total_results' => 1]);

        //filtra por location_id existente en base de datos pero que no está asociada al producto, no devuelve datos
        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            'location_id' => $location3->id,
            'currency' => $currency->iso_code,
        ])
            ->seeJsonEquals(['error' => 200, 'data' => [], "total_results" => 0]);*/

    }

    /**
     * Test consulta un subcanal con un producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro avail
     */
    public function testOk11()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        //tarifa para el mismo producto en dos locaciones diferentes
        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '8.00'
        ]);

        //disponibilidad ilimitada, se solicita filtro avail mayor que campo avail pero manda campo unlimited, devuelve datos del producto
        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        //devuelve datos del producto general y el id de la disponibilidad del producto
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'avail' => 10,
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'id' => $availability1->id, 'net_price' => $priceProduct->net_price]);

        $availability1->delete();

        //closed a 1 el producto no tiene disponibilidad para ninguna fecha puesto que fecha y sesión están a null ni para ninguna cantidad de personas
        //puesto que el producto está cerrado
        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 30,
            'closed' => 1,
            'unlimited' => 0,
        ]);

        //no devuelve datos puesto que está cerrado
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'avail' => 10,
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], 'total_results' => 0]);

        $availability2->delete();

        //la disponibilidad tiene campo closed a 0 y unlimited a 0, ni está cerrado ni tiene capacidad ilimitada, y tiene disponibles 30 entradas
        $availability3 = factory(App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 30,
            'closed' => 0,
            'unlimited' => 0,
        ]);

        //pide entradas para 30 y devuelve disponibilidad, por defecto con fecha de hoy
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'avail' => 30,
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'id' => $availability3->id]);

        //no devuelve datos puesto que tenemos un producto con 30 plazas disponibles y se pide disponibilildad para 31
        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'avail' => 31,
            'currency' => $currency->iso_code,
        ])
            ->assertExactJson(['error' => 200, 'data' => [], 'total_results' => 0]);

        $availability3->delete();
    }

    /**
     * Test consulta subcanal con producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro type_id
     */
    public function testOk12()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $productTypeFactory2 = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory2->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory2->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory2->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $product2 = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory2->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation4 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 1,
            'name' => 'es name product2'
        ]);
        $productTranslation5 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 2,
            'name' => 'en name product2'
        ]);
        $productTranslation6 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 3,
            'name' => 'pt name product2'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '10.00',
        ]);

        $priceProduct2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product2->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '15.00',

        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por tipo de producto 2 que hemos asociado a producto 2, devuelve datos del producto 2 en español al no especificar idioma
            'type_id' => $productTypeFactory2->id,
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product2->id, 'name' => $productTranslation4->name, 'avail' => $availability2->avail, 'net_price' => $priceProduct2->net_price,
                'total_results' => 1]);
    }

    /**
     * Test consulta subcanal con producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro category_id
     */
    public function testOk13()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $categoryFactory = Factory(App\Models\tenant\Category::class)->create();

        Factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $categoryFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $categoryFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $categoryFactory->id,
            'language_id' => 3,
        ]);

        $categoryFactory2 = Factory(App\Models\tenant\Category::class)->create();

        Factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $categoryFactory2->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $categoryFactory2->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\CategoryTranslation::class)->create([
            'category_id' => $categoryFactory2->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $product2 = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation4 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 1,
            'name' => 'es name product2'
        ]);
        $productTranslation5 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 2,
            'name' => 'en name product2'
        ]);
        $productTranslation6 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 3,
            'name' => 'pt name product2'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
        ]);

        factory(App\Models\tenant\ProductCategory::class)->create([
            'product_id' => $product->id,
            'category_id' => $categoryFactory->id,
        ]);

        factory(App\Models\tenant\ProductCategory::class)->create([
            'product_id' => $product2->id,
            'category_id' => $categoryFactory2->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '7.00',
        ]);

        $priceProduct2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product2->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '6.00',
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por categoria 2 que hemos asociado a producto 2, devuelve datos del producto 2
            'category_id' => $categoryFactory2->id,
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product2->id, 'name' => $productTranslation4->name, 'avail' => $availability2->avail, 'net_price' => $priceProduct2->net_price,
                'total_results' => 1]);
    }

    /**
     * Test consulta subcanal con producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro tag_id
     */
    public function testOk14()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $tagFactory = Factory(App\Models\tenant\Tag::class)->create();

        Factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $tagFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $tagFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $tagFactory->id,
            'language_id' => 3,
        ]);

        $tagFactory2 = Factory(App\Models\tenant\Tag::class)->create();

        Factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $tagFactory2->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $tagFactory2->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $tagFactory2->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $product2 = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation4 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 1,
            'name' => 'es name product2'
        ]);
        $productTranslation5 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 2,
            'name' => 'en name product2'
        ]);
        $productTranslation6 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 3,
            'name' => 'pt name product2'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
        ]);

        factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $product->id,
            'tag_id' => $tagFactory->id,
        ]);

        factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $product2->id,
            'tag_id' => $tagFactory2->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '7.00',
        ]);

        $priceProduct2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product2->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'net_price' => '9.00',
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por tag 2 que hemos asociado a producto 2, devuelve datos del producto 2
            'tag_id' => $tagFactory2->id,
            'currency' => $currency->iso_code,
        ])
            ->assertJsonFragment(['id' => $product2->id, 'name' => $productTranslation4->name, 'id' => $tagFactory2->id,
                'avail' => $availability2->avail, 'net_price' => $priceProduct2->net_price, 'total_results' => 1]);
    }

    /**
     * Test consulta subcanal con producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro money con conversor
     */
    /*public function testOk15()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $product2 = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);
        //crea sus traducciones si no las tiene no muestra el producto
        $productTranslation4 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 1,
            'name' => 'es name product2'
        ]);
        $productTranslation5 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 2,
            'name' => 'en name product2'
        ]);
        $productTranslation6 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 3,
            'name' => 'pt name product2'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\Subchannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currenc::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currenc::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currenc::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\CurrencyExchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
        ]);

        $currencyExchange2 = factory(App\Models\tenant\CurrencyExchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '0.50',
        ]);

        $currencyExchange3 = factory(App\Models\tenant\CurrencyExchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '0.25',
        ]);

        factory(App\Models\tenant\CurrencyExchange::class)->create([
            'currency_id' => 1,
            'exchange' => '25',
        ]);

        factory(App\Models\tenant\CurrencyExchange::class)->create([
            'currency_id' => 2,
            'exchange' => '25',
        ]);

        factory(App\Models\tenant\CurrencyExchange::class)->create([
            'currency_id' => 3,
            'exchange' => '25',
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);

        $priceProduct2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product2->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '15.00'
        ]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money que abarca los 2 precios de los 2 productos, devuelve los 2 productos
            'money' => 15,
            'currency' => $currency1->iso_code,
        ])
            ->seeJsonContains(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail, 'sale_price' => $priceProduct->sale_price,
                'id' => $product2->id, 'name' => $productTranslation4->name, 'avail' => $availability2->avail, 'sale_price' => $priceProduct2->sale_price,
                'total_results' => 2]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money mayor que el producto 1 pero menor que el producto 2, devuelve solo producto 1
            'money' => 10,
            'currency' => $currency1->iso_code,
        ])
            ->seeJsonContains(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail, 'sale_price' => $priceProduct->sale_price,
                'total_results' => 1]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money menor que ambos productos, no devuelve ninguno
            'money' => 5,
            'currency' => $currency3->iso_code,
        ])
            ->seeJsonEquals(['error' => 200, 'data' => [], "total_results" => 0]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money menor que ambos productos, no devuelve ninguno
            'money' => 7,
            'currency' => $currency1->iso_code,
        ])
            ->seeJsonEquals(['error' => 200, 'data' => [], "total_results" => 0]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money menor que ambos productos, no devuelve ninguno
            'money' => 3,
            'currency' => $currency2->iso_code,
        ])
            ->seeJsonEquals(['error' => 200, 'data' => [], "total_results" => 0]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money igual al producto de mayor precio, envía 30 pesos que equivale a 15 dólares
            'money' => 30,
            'currency' => $currency2->iso_code,
        ])
            ->seeJsonContains(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail,
                'id' => $product2->id, 'name' => $productTranslation4->name, 'avail' => $availability2->avail,
                'total_results' => 2]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money mayor que el producto 2 pero menor que el producto 1, envía 18 pesos que equivale a 9 dólares, devuelve 1 resultado
            'money' => 18,
            'currency' => $currency2->iso_code,
        ])
            ->seeJsonContains(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail,
                'total_results' => 1]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money igual al producto de mayor precio, envía 60 euros que equivale a 15 dólares
            'money' => 60,
            'currency' => $currency3->iso_code,
        ])
            ->seeJsonContains(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail,
                'id' => $product2->id, 'name' => $productTranslation4->name, 'avail' => $availability2->avail,
                'total_results' => 2]);

        $this->json('GET', '/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por money mayor que el producto 2 pero menor que el producto 1, envía 36 euros que equivale a 9 dólares, devuelve 1 resultado
            'money' => 36,
            'currency' => $currency3->iso_code,
        ])
            ->seeJsonContains(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail,
                'total_results' => 1]);
    }*/

    /**
     * Test consulta subcanal con producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro currency_id
     */
    public function testOk16()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1'
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '0.50',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '0.25',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);

        //utiliza función del controlador para comprobar que convierte bien a las 3 monedas posibles
        //se le envían 3 parámetros a la función: valor a convertir(8 en este caso que es el precio de venta que tiene el producto),
        //moneda actual que es la moneda en la que está la tarifa(en este caso el 1, la tarifa está en dólares el id 1), y moneda a convertir que es el filtro que
        //envía(el 1 en este caso, convierte a dolares)
        $tarifa_dolares = $this->conversor(8, $currency1->id, $currency1->id);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por currency_id para que el precio se devuelva en la moneda enviada, en este caso el peso mexicano
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail, 'sale_price' => $tarifa_dolares]);


        //utiliza función del controlador para comprobar que convierte bien a las 3 monedas posibles
        //se le envían 3 parámetros a la función: valor a convertir(8 en este caso que es el precio de venta que tiene el producto),
        //moneda actual que es la moneda en la que está la tarifa(en este caso el 1, la tarifa está en dólares el id 1), y moneda a convertir que es el filtro que
        //envía(el 2 en este caso, convierte a pesos mexicanos)
        $tarifa_pesos = $this->conversor(8, $currency1->id, $currency2->id);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por currency_id para que el precio se devuelva en la moneda enviada, en este caso el peso mexicano
            'currency' => $currency2->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail, 'sale_price' => $tarifa_pesos]);


        //utiliza función del controlador para comprobar que convierte bien a las 3 monedas posibles
        //se le envían 3 parámetros a la función: valor a convertir(8 en este caso que es el precio de venta que tiene el producto),
        //moneda actual que es la moneda en la que está la tarifa(en este caso el 1, la tarifa está en dólares el id 1), y moneda a convertir que es el filtro que
        //envía(el 3 en este caso, convierte a euros)
        $tarifa_euros = $this->conversor(8, $currency1->id, $currency3->id);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            //filtra por currency_id para que el precio se devuelva en la moneda enviada, en este caso el peso mexicano
            'currency' => $currency3->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail, 'sale_price' => $tarifa_euros]);
    }

    /**
     * Test consulta subcanal con producto con tarifa asociada con mismo contract model que el canal y con tarifa en las
     * fechas solicitadas, no tiene registro en viewproduct y tiene campo view_blocked de la tabla mo_subchannel a 0, se muestran los datos generales del producto.
     * Se prueba filtro depends_on
     */
    public function testOk17()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $product2 = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation4 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 1,
            'name' => 'es name product2'
        ]);
        $productTranslation5 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation6 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product2->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1',
            'depends_on' => $product->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '0.50',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '0.25',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);

        $availability2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product2->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product2->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '9.00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'depends_on' => $product->id,
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product2->id, 'name' => $productTranslation4->name, 'avail' => $availability2->avail, 'sale_price' => $priceProduct2->sale_price]);
    }

    /**
     * Prueba automatizada envio de filtro code existiendo un producto con ese code
     */
    public function testOk18()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'TEST',
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1',
            'depends_on' => $product->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '0.50',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '0.25',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'code' => 'TEST',
            'currency' => $currency1->iso_code,
        ])
            ->assertJsonFragment(['id' => $product->id, 'name' => $productTranslation1->name, 'avail' => $availability1->avail, 'sale_price' => $priceProduct->sale_price]);
    }

    /**
     * Prueba automatizada envio de filtro code sin producto con ese code
     */
    public function testOk19()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'TEST',
            'type_id' => $productTypeFactory->id,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1',
            'depends_on' => $product->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '0.50',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '0.25',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'code' => 'TEST1',
        ])
            ->assertJsonFragment(['total_results' => 0]);
    }

    /**
     * Prueba automatizada envio de filtro salable con producto con atributo salable coincidente
     */
    public function testOk20()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'TEST',
            'type_id' => $productTypeFactory->id,
            'salable' => 0,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1',
            'depends_on' => $product->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '0.50',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '0.25',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'salable' => 0,
            'currency' => $currency1->iso_code,

        ])
            ->assertJsonFragment(['id' => $product->id, 'salable' => 0, 'name' => $productTranslation1->name, 'avail' => $availability1->avail, 'sale_price' => $priceProduct->sale_price]);
    }

    /**
     * Prueba automatizada envio de filtro salable sin salable coincidente
     */
    public function testOk21()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $productTypeFactory = Factory(App\Models\tenant\Type::class)->create();

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 1,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 2,
        ]);

        Factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $productTypeFactory->id,
            'language_id' => 3,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'TEST',
            'type_id' => $productTypeFactory->id,
            'salable' => 1,
        ]);

        $productTranslation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1,
            'name' => 'es name product'
        ]);
        $productTranslation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2,
            'name' => 'en name product'
        ]);
        $productTranslation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3,
            'name' => 'pt name product'
        ]);

        $location = factory(App\Models\tenant\Location::class)->create([
            'name' => 'name location 1',
            'depends_on' => $product->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $clientType = factory(App\Models\tenant\ClientType::class)->create([
            'code' => 'cli'
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 1,
            'name' => 'cliente test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 2,
            'name' => 'cliente2 test',
        ]);

        factory(App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => 3,
            'name' => 'cliente3 test',
        ]);

        $subchannel = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract_model->id,
            'view_blocked' => 0,
        ]);

        $currency1 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISODOLAR'
        ]);

        $currency2 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOPESO'
        ]);

        $currency3 = factory(App\Models\tenant\Currency::class)->create([
            'iso_code' => 'ISOEURO'
        ]);

        $currencyExchange = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency1->id,
            'exchange' => '1.00',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange2 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency2->id,
            'exchange' => '0.50',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $currencyExchange3 = factory(App\Models\tenant\Exchange::class)->create([
            'currency_id' => $currency3->id,
            'exchange' => '0.25',
            'date_time_start' => Carbon::now()->subHour()
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
            'currency_id' => $currency1->id,
        ]);

        $availability1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'avail' => 0,
            'closed' => 0,
            'unlimited' => 1,
        ]);

        $priceProduct = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
            'product_id' => $product->id,
            'client_type_id' => $clientType->id,
            'location_id' => $location->id,
            'sale_price' => '8.00'
        ]);

        $this->json('GET', '/api/v1/search', [
            'subchannel_id' => $subchannel->id,
            'code' => 'TEST1',
            'salable' => 0,
        ])
            ->assertJsonFragment(['total_results' => 0]);
    }

}