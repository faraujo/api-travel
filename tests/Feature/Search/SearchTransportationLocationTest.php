<?php
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SearchTransportationLocationTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada comprueba el envio del tipo de dato en los parametros
     */
    public function testBad1(){

        $this->json('GET', '/api/v1/search/transportationlocation', [
            'page' => 'test',
            'limit' => 'test',

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "limit" => ["The limit must be an integer."],
                "page" => ["The page must be an integer."]
            ]
        ]]);
    }

    public function testOk1(){

        $this->json('GET', '/api/v1/search/transportationlocation', [

        ])->assertJsonFragment(['error' => 200]);
    }

}