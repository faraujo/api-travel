<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SubchannelsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * No se envían los campos requeridos 
     */
    public function testBad1()
    {
        $this->json('put', '/api/v1/subchannel', [

        ])
            ->assertExactJson(["error" => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [
                [
                "id" => ["The id field is required."],
                "view_blocked" => ["The view blocked field is required."],
                "channel_id" => ["The channel id field is required."],
                "contract_model_id" => ["The contract model id field is required."],
                "translation" => ["you need at least one translation"]

            ]]]);
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se envían los campos requeridos pero con formato incorrecto
     */
    public function testBad2()
    {
        $this->json('put', '/api/v1/subchannel', [
            'view_blocked' => 'A',
            'id' => 'a',
            'channel_id' => 'a',
            'contract_model_id' => 'a',
            'modules' => [
                ['id' => 'x'],
                ['id' => 'y']
            ]
        ])
            ->assertExactJson([
                "error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [
                    [
                    "id" => ["The id must be an integer."],
                    "view_blocked" => ["The view blocked field must be true or false."],
                    "channel_id" => ["The channel id must be an integer."],
                    "contract_model_id" => ["The contract model id must be an integer."],
                    "translation" => ["you need at least one translation"],
                    "modules.0.id" => ["The modules.0.id must be an integer."],
                    "modules.1.id" => ["The modules.1.id must be an integer."]
                ]]]);
    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     *
     * Se envían los campos requeridos pero con id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('put', '/api/v1/subchannel', [
            'view_blocked' => '1',
            'channel_id' => '1',
            'contract_model_id' => '1',
            'name' => ['ES' => 'Name español'],
            'id' => 9999999999,

        ])
        ->assertExactJson(["error" => 404, "error_description" => "Data not found"]);
    }


    /**
     * Test que comprueba la actualización del campo view_blocked
     *
     * Se crea un subcanal con view_blocked igual a 0 y se actualiza a 1
     *
     */
    public function testOk1()
    {

        $moduleFactory1 = factory(App\Models\tenant\Module::class)->create([]);
        $moduleFactory2 = factory(App\Models\tenant\Module::class)->create([]);

        $channelFactory1 = factory(App\Models\tenant\Channel::class)->create([]);
        $channelFactory2 = factory(App\Models\tenant\Channel::class)->create([]);
        $contractModelFactory1 = factory(App\Models\tenant\ContractModel::class)->create([]);
        $contractModelFactory2 = factory(App\Models\tenant\ContractModel::class)->create([]);

        $subChannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'view_blocked' => '0',
            'channel_id' => $channelFactory1->id,
            'contract_model_id' => $contractModelFactory1->id,
        ]);

        $languageFactory = factory(\App\Models\tenant\Language::class)->create([]);
        $languageFactory2 = factory(\App\Models\tenant\Language::class)->create([]);

        $subChannelTranslationFactory1 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'subchannel_id' => $subChannelFactory->id,
            'name' => 'sub 1'
        ]);

        $this->assertDatabaseHas('mo_subchannel', [
            'id' => $subChannelFactory->id,
            'view_blocked' => $subChannelFactory->view_blocked,
            'channel_id' => $subChannelFactory->channel_id,
            'contract_model_id' => $subChannelFactory->contract_model_id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_subchannel_translation', [
            'language_id' => $languageFactory->id,
            'subchannel_id' => $subChannelTranslationFactory1->subchannel_id,
            'name' => $subChannelTranslationFactory1->name,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_channel', [
            'id' => $channelFactory1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contractModelFactory1->id,
            'deleted_at' => null,
        ]);

        $this->json('put', '/api/v1/subchannel', [
            'view_blocked' => 1,
            'channel_id' => $channelFactory2->id,
            'contract_model_id' => $contractModelFactory2->id,
            'prefix' => 'prefijo_test',
            'reservation_code' => 1000,
            'api_code' => 'api_code_test',
            'modules' => [
                ['id' => $moduleFactory1->id],
                ['id' => $moduleFactory2->id]
            ],
            'id' => $subChannelFactory->id,
            'name' => [$languageFactory2->abbreviation => 'Name test idioma 2'],

        ])
        ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_subchannel', [
            'view_blocked' => 1,
            'channel_id' => $channelFactory2->id,
            'contract_model_id' => $contractModelFactory2->id,
            'id' => $subChannelFactory->id,
            'prefix' => 'prefijo_test',
            'reservation_code' => 1000,
            'api_code' => 'api_code_test',
            'sendconfirmedemail' => 0,
        ]);

        $this->assertDatabaseHas('mo_subchannel_translation', [
            'language_id' => $languageFactory2->id,
            'name' => 'Name test idioma 2',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_subchannel_translation', [
            'language_id' => $languageFactory->id,
            'name' => $subChannelTranslationFactory1->name,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_module_subchannel', [
            'module_id' => $moduleFactory1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_module_subchannel', [
            'module_id' => $moduleFactory2->id,
            'deleted_at' => null,
        ]);
    }
}
