<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ChannelsCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada incorrecta enviando peticion sin parametros requeridos
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/channel', [
        ])
        ->assertExactJson(["error" => 400,
        "error_description" => "The fields are not the required format",
        "error_inputs" => [
            [
            "translation" => ["you need at least one translation"]
        ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando name con lang incorrecta
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/channel', [
            'asdadads' => ['ES' => 'Short descripcion español'],
        ])
        ->assertExactJson(["error" => 400,
        "error_description" => "The fields are not the required format",
        "error_inputs" => [
            [
            "translation" => ["you need at least one translation"]
        ]]]);

    }

    /**
     * Prueba automatizada correcta
     */
    public function testOk1()
    {

        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);

        $this->json('post', '/api/v1/channel', [
            'name' => [$languageFactory->abbreviation => 'test'],
        ])
        ->assertExactJson(["error" => 200]);
        
        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $languageFactory->id,
            'name' => 'test',
        ]);
    }
}