<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ChannelsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consultar channel con parámetro lang erróneo
     */
    public function testBad1()
    {
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);
        factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 1,
            'channel_id' => $channelFactory->id,
        ]);
        factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 2,
            'channel_id' => $channelFactory->id,
        ]);
        factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 3,
            'channel_id' => $channelFactory->id,
        ]);
        $this->json('GET', '/api/v1/channel/' . $channelFactory->id, [
            'lang' => 'FDF'])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Consulta channel inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/channel/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar channel con parámetro lang correcto
     */
    public function testOk2()
    {
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);
        $translation1 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 1,
            'channel_id' => $channelFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 2,
            'channel_id' => $channelFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 3,
            'channel_id' => $channelFactory->id,
        ]);
        $this->json('GET', '/api/v1/channel/' . $channelFactory->id, [
            'lang' => 'PT'])->assertJsonFragment([
            'error' => 200,
            'id' => $channelFactory->id,
        ]);
    }

    /**
     * Consultar channel sin traducciones, devuelve json vacío
     */
    public function testOk3()
    {
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $this->json('GET', '/api/v1/channel/'.$channelFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta channel existente sin parámetro lang
     */
    public function testOk4()
    {
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);
        $translation1 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 1,
            'channel_id' => $channelFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 2,
            'channel_id' => $channelFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 3,
            'channel_id' => $channelFactory->id,
        ]);
        $this->json('GET', '/api/v1/channel/' . $channelFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $channelFactory->id,
                'name' => $translation1->name,
                'name' => $translation2->name,
                'name' => $translation3->name,
            ]);
    }

    /**
     * Prueba automatizada correcta, comprobación json salida
     */
    public function testOk5()
    {
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);
        $translation1 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => $language->id,
            'channel_id' => $channelFactory->id,
        ]);
        $this->json('GET', '/api/v1/channel/' . $channelFactory->id)
        ->assertExactJson(['error' => 200, 'data' => [[
            'channel' => [[
                'id' => $channelFactory->id,
                'lang' => [[
                    $language->abbreviation => [
                        'id' => $translation1->id,
                        'language_id' => $language->id,
                        'name' => $translation1->name,
                    ]
                ]]
            ]]]
        ]]);
    }

}