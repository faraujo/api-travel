<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ChannelsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * No se envían los campos requeridos id y traducción
     */
    public function testBad1()
    {
        $this->json('put', '/api/v1/channel', [

        ])
            ->assertExactJson(["error" => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [
                [
                    "id" => ["The id field is required."],
                    "translation" => ["you need at least one translation"]
            ]]]);
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se envían los campos requeridos id y translation pero con formato incorrecto
     */
    public function testBad2()
    {
        $this->json('put', '/api/v1/channel', [
            'id' => 'a',
            'asdadads' => ['ES' => 'Short descripcion español'],

        ])
            ->assertExactJson([
                "error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [
                    [
                    "id" => ["The id must be an integer."],
                    "translation" => ["you need at least one translation"]

                ]]]);
    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     *
     * Se envía canal a modificar no existente
     */
    public function testBad3()
    {

        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $this->json('put', '/api/v1/channel', [
            'id' => 9999999999,
            'name' => [$language->abbreviation => 'Name español'],

        ])
        ->assertExactJson(["error" => 404, "error_description" => "Data not found"]);
    }


    /**
     * Prueba automatizada correcta
     */
    public function testOk1()
    {

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([
        ]);

        $languageFactory = factory(\App\Models\tenant\Language::class)->create([]);

        $channelTranslationFactory = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'channel_id' => $channelFactory->id,
            'name' => 'chan 1'
        ]);

        $this->assertDatabaseHas('mo_channel', [
            'id' => $channelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $languageFactory->id,
            'channel_id' => $channelFactory->id,
            'name' => $channelTranslationFactory->name,
            'deleted_at' => null,
        ]);

        $this->json('put', '/api/v1/channel', [
            'id' => $channelFactory->id,
            'name' => [$languageFactory->abbreviation => 'Test Español'],
        ])
        ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_channel_translation', [
            'channel_id' => $channelFactory->id,
            'language_id' => $languageFactory->id,
            'name' => 'Test Español',
            'deleted_at' => null,
        ]);
    }
}
