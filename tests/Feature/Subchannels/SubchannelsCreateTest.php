<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SubchannelsCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * No se envían los campos requeridos view_blocked, channel_id, contract_model_id
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/subchannel', [

        ])
            ->assertExactJson(["error" => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [
                [
                "view_blocked" => ["The view blocked field is required."],
                "channel_id" => ["The channel id field is required."],
                "contract_model_id" => ["The contract model id field is required."],
                "translation" => ["you need at least one translation"]
            ]]]);
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se envían los campos requeridos view_blocked, channel_id, contract_model_id pero con formato incorrecto
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/subchannel', [
            'view_blocked' => 'A',
            'channel_id' => 'a',
            'contract_model_id' => 'a',
            'modules' => [
                ['id' => 'x'],
                ['id' => 'y']
            ]
        ])
        ->assertExactJson([
            "error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [
                [
                "view_blocked" => ["The view blocked field must be true or false."],
                "channel_id" => ["The channel id must be an integer."],
                "contract_model_id" => ["The contract model id must be an integer."],
                "translation" => ["you need at least one translation"],
                "modules.0.id" => ["The modules.0.id must be an integer."],
                "modules.1.id" => ["The modules.1.id must be an integer."]
            ]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    public function testOk1()
    {

        $moduleFactory1 = factory(App\Models\tenant\Module::class)->create([]);
        $moduleFactory2 = factory(App\Models\tenant\Module::class)->create([]);

        $channelFactory1 = factory(App\Models\tenant\Channel::class)->create([]);
        $contractModelFactory1 = factory(App\Models\tenant\ContractModel::class)->create([]);

        $languageFactory = factory(\App\Models\tenant\Language::class)->create([]);

        $this->assertDatabaseHas('mo_channel', [
            'id' => $channelFactory1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contractModelFactory1->id,
            'deleted_at' => null,
        ]);

        $this->json('post', '/api/v1/subchannel', [
            'view_blocked' => 1,
            'channel_id' => $channelFactory1->id,
            'contract_model_id' => $contractModelFactory1->id,
            'prefix' => 'prefijo_test',
            'reservation_code' => 1000,
            'api_code' => 'api_code_test',
            'name' => [$languageFactory->abbreviation => 'Short descripcion español'],
            'modules' => [
                ['id' => $moduleFactory1->id],
                ['id' => $moduleFactory2->id]
            ]
        ])
        ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_subchannel', [
            'view_blocked' => 1,
            'channel_id' => $channelFactory1->id,
            'contract_model_id' => $contractModelFactory1->id,
            'prefix' => 'prefijo_test',
            'reservation_code' => 1000,
            'api_code' => 'api_code_test',
            'sendconfirmedemail' => 0,
        ]);

        $this->assertDatabaseHas('mo_subchannel_translation', [
            'language_id' => $languageFactory->id,
            'name' => 'Short descripcion español',
        ]);

        $this->assertDatabaseHas('mo_module_subchannel', [
            'module_id' => $moduleFactory1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_module_subchannel', [
            'module_id' => $moduleFactory2->id,
            'deleted_at' => null,
        ]);
    }
}