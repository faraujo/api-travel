<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SubchannelsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/subchannel', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/subchannel', [
            'ids' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/subchannel', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' => [["ids.0" => ["The selected ids.0 is invalid."]]]
            ]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->assertDatabaseHas('mo_subchannel', [
            'id' => $subchannelFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/subchannel', [
            'ids' => $subchannelFactory->id,
        ])
        ->assertExactJson([
            "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
        ]);

    }


    /**
     * Test para eliminar un subchannel que crea primero un subchannel factory, sus traducciones y relación en tabla intermedia
     */
    public function testOk1()
    {
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);



        $translation1 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 1,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 2,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 3,
            'subchannel_id' => $subchannelFactory->id,
        ]);


        $this->json('DELETE', '/api/v1/subchannel', [
            'ids' => [$subchannelFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_subchannel el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_subchannel', ['deleted_at' => null, 'id' => $subchannelFactory->id]);
        //Comprueba que las traducciones del subchannel que han sido creadas no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_subchannel_translation', ['deleted_at' => null, 'subchannel_id' => $translation1->subchannel_id]);
        $this->assertDatabaseMissing('mo_subchannel_translation', ['deleted_at' => null, 'subchannel_id' => $translation2->subchannel_id]);
        $this->assertDatabaseMissing('mo_subchannel_translation', ['deleted_at' => null, 'subchannel_id' => $translation3->subchannel_id]);

    }


}