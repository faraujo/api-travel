<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SubchannelModulesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada ok
     */
    public function testOk1(){

        $this->json('GET', '/api/v1/subchannel/modules', [

        ])->assertJsonFragment(['error' => 200]);
    }

    /**
     * Test consulta módulos aplicables a subcanal existente en base de datos
     */
    public function testOk2()
    {
        $moduleFactory = factory(\App\Models\tenant\Module::class)->create([]);
        $permissionFactory = factory(\App\Models\tenant\Permission::class)->create([
            'module_id' => $moduleFactory->id,
            'admin' => 0,
            'subchannel' => 1
        ]);

        $this->json('GET', '/api/v1/subchannel/modules', [
        ])->assertJsonStructure(['error', 'data' => [], 'total_results'])
        ->assertJsonFragment(['id' => $moduleFactory->id]);
    }
}