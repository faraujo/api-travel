<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SubchannelsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consultar subchannel con parámetro lang erróneo
     */
    public function testBad1()
    {
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);
        factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 1,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 2,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 3,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $this->json('GET', '/api/v1/subchannel/' . $subchannelFactory->id, [
            'lang' => 'FDF'])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Consulta subchannel inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/subchannel/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar subchannel con parámetro lang correcto
     */
    public function testOk2()
    {
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);
        $translation1 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 1,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 2,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 3,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $this->json('GET', '/api/v1/subchannel/' . $subchannelFactory->id, [
            'lang' => 'PT'])->assertJsonFragment([
            'error' => 200,
            'id' => $subchannelFactory->id,
        ]);
    }

    /**
     * Consultar subchannel sin traducciones, devuelve json vacío
     */
    public function testOk3()
    {
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('GET', '/api/v1/subchannel/'.$subchannelFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta subchannel existente sin parámetro lang
     */
    public function testOk4()
    {
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);
        $translation1 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 1,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 2,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 3,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $this->json('GET', '/api/v1/subchannel/' . $subchannelFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $subchannelFactory->id,
                'name' => $translation1->name,
                'name' => $translation2->name,
                'name' => $translation3->name,
            ]);
    }

    /**
     * Prueba automatizada correcta, comprobación json salida
     */
    public function testOk5()
    {
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $moduleFactory1 = factory(App\Models\tenant\Module::class)->create([]);
        $moduleFactory2 = factory(App\Models\tenant\Module::class)->create([]);

        $channelFactory1 = factory(App\Models\tenant\Channel::class)->create([]);
        $contractModelFactory1 = factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'view_blocked' => 1,
            'channel_id' => $channelFactory1->id,
            'contract_model_id' => $contractModelFactory1->id,
            'prefix' => 'prefijo_test',
            'reservation_code' => 1000,
            'api_code' => 'api_code_test',
            'authorization_token' => 'authorization_token_test',
            'sendconfirmedemail' => 0,
        ]);

        $translation1 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => $language->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('GET', '/api/v1/subchannel/' . $subchannelFactory->id)
        ->assertExactJson(['error' => 200, 'data' => [[
            'subchannel' => [[
                'id' => $subchannelFactory->id,
                'view_blocked' => 1,
                'channel_id' => $channelFactory1->id,
                'contract_model_id' => $contractModelFactory1->id,
                'prefix' => 'prefijo_test',
                'reservation_code' => 1000,
                'api_code' => 'api_code_test',
                'cashier' => null,
                'authorization_token' => 'authorization_token_test',
                'sendconfirmedemail' => 0,
                'lang' => [[
                    $language->abbreviation => [
                        'name' => $translation1->name,
                    ]
                ]],
                'module' => []
            ]]]
        ]]);
    }

}