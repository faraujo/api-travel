<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ChannelsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de canales con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/channel', [
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["limit"=>["The limit must be an integer."],
                    "page"=>["The page must be an integer."]]]]);
    }

    /**
     * test consulta de canales con parametro lang incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/channel', [

            'lang' => 'eeeeeeee',

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["lang"=>["The lang is not exists"]]]]);
    }


    /**
     * test crea factory de canal con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de canal y sus traducciones
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $channelTranslationFactory1 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'channel_id' => $channelFactory->id,
            'language_id' => 1,
            'name' => 'channel 1'
        ]);

        $channelTranslationFactory2 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'channel_id' => $channelFactory->id,
            'language_id' => 2,
            'name' => 'channel 2'
        ]);

        $channelTranslationFactory3 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'channel_id' => $channelFactory->id,
            'language_id' => 3,
            'name' => 'channel 3'
        ]);

        $this->assertDatabaseHas('mo_channel', [
            'id' => $channelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $channelTranslationFactory1->language_id,
            'channel_id' => $channelTranslationFactory1->channel_id,
            'name' => $channelTranslationFactory1->name,
        ]);
        
        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $channelTranslationFactory2->language_id,
            'channel_id' => $channelTranslationFactory2->channel_id,
            'name' => $channelTranslationFactory2->name,
        ]);
        
        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $channelTranslationFactory3->language_id,
            'channel_id' => $channelTranslationFactory3->channel_id,
            'name' => $channelTranslationFactory3->name,
        ]);

        $this->json('GET', '/api/v1/channel?limit=0', [])
            ->assertJsonFragment([
                'error' => 200,
                'name' =>  $channelTranslationFactory1->name,
                'name' =>  $channelTranslationFactory2->name,
                'name' =>  $channelTranslationFactory3->name,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/channel', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['channel' => ['0' => ['lang']]]]
            ]);

    }

    /**
     * test ok con filtro lang de idioma
     */
    public function testOk2(){
        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST',
        ]);

        $channelFactory = factory(\App\Models\tenant\Channel::class)->create([]);

        factory(\App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => $language_factory->id,
            'channel_id' => $channelFactory->id,
        ]);

        $this->json('GET','/api/v1/channel', [
            'lang' => 'TEST',
        ])->assertJsonFragment(['id' => $channelFactory->id]);
    }


}