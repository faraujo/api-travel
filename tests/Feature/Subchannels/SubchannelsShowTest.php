<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SubchannelsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de subcanales con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/subchannel', [
            'page' => 'string',
            'limit' => 'string',
            'channel_id' => 'string',

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["channel_id"=>["The channel id must be an integer."],"limit"=>["The limit must be an integer."],
                    "page"=>["The page must be an integer."]]]]);
    }

    /**
     * test consulta de subcanales con parametro channel_id inexistente en tabla mo_channel
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/subchannel', [

            'channel_id' => 999999,

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["channel_id"=>["The selected channel id is invalid."]]]]);
    }

    /**
     * test consulta de subcanales con parametro lang incorrecto
     */
    public function testBad3()
    {
        $this->json('GET', '/api/v1/subchannel', [

            'lang' => 'eeeeeeee',

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["lang"=>["The lang is not exists"]]]]);
    }


    /**
     * test crea factory de subcanal con traducciones y asociado a un canal con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de canal y sus traducciones
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $channelTranslationFactory1 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 1,
            'channel_id' => $channelFactory->id,
            'name' => 'channel 1'
        ]);

        $channelTranslationFactory2 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 2,
            'channel_id' => $channelFactory->id,
            'name' => 'channel 2'
        ]);

        $channelTranslationFactory3 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 3,
            'channel_id' => $channelFactory->id,
            'name' => 'channel 3'
        ]);

        $this->assertDatabaseHas('mo_channel', [
            'id' => $channelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $channelTranslationFactory1->language_id,
            'channel_id' => $channelTranslationFactory1->channel_id,
            'name' => $channelTranslationFactory1->name,
        ]);
        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $channelTranslationFactory2->language_id,
            'channel_id' => $channelTranslationFactory2->channel_id,
            'name' => $channelTranslationFactory2->name,
        ]);
        $this->assertDatabaseHas('mo_channel_translation', [
            'language_id' => $channelTranslationFactory3->language_id,
            'channel_id' => $channelTranslationFactory3->channel_id,
            'name' => $channelTranslationFactory3->name,
        ]);

        //Factory de subcanal (y sus traducciones) asociado al canal y cliente creados,
        $subChannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'channel_id' => $channelFactory->id,
        ]);

        $subChannelTranslationFactory1 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 1,
            'subchannel_id' => $subChannelFactory->id,
            'name' => 'sub 1'
        ]);

        $subChannelTranslationFactory2 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 2,
            'subchannel_id' => $subChannelFactory->id,
            'name' => 'sub 2'
        ]);

        $subChannelTranslationFactory3 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => 3,
            'subchannel_id' => $subChannelFactory->id,
            'name' => 'sub 3'
        ]);

        $this->assertDatabaseHas('mo_subchannel', [
            'channel_id' => $subChannelFactory->channel_id,
        ]);

        $this->assertDatabaseHas('mo_subchannel_translation', [
            'language_id' => $subChannelTranslationFactory1->language_id,
            'subchannel_id' => $subChannelTranslationFactory1->subchannel_id,
            'name' => $subChannelTranslationFactory1->name,
        ]);
        $this->assertDatabaseHas('mo_subchannel_translation', [
            'language_id' => $subChannelTranslationFactory2->language_id,
            'subchannel_id' => $subChannelTranslationFactory2->subchannel_id,
            'name' => $subChannelTranslationFactory2->name,
        ]);
        $this->assertDatabaseHas('mo_subchannel_translation', [
            'language_id' => $subChannelTranslationFactory3->language_id,
            'subchannel_id' => $subChannelTranslationFactory3->subchannel_id,
            'name' => $subChannelTranslationFactory3->name,
        ]);

        //no hace pruebas con los id de los channel y subchannels porque al devolver array channel con id dentro de él puede llevar a confusión
        $this->json('GET', '/api/v1/subchannel', [])
            ->assertJsonFragment([
                'error' => 200,
                'name' => 'channel 1',
                'name' => 'channel 2',
                'name' => 'channel 3',
                'name' => 'sub 1',
                'name' => 'sub 2',
                'name' => 'sub 3',

            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/subchannel', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['subchannel' => ["0" => ['channel','lang']]]]
            ]);

    }

    /**
     * test ok con filtro lang y name
     */
    public function testOk2(){
        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST',
        ]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);
        $channelTranslationFactory1 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => $language_factory->id,
            'channel_id' => $channelFactory->id,
        ]);

        $subChannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'channel_id' => $channelFactory->id,
        ]);

        $subChannelTranslationFactory1 = factory(App\Models\tenant\SubChannelTranslation::class)->create([
            'language_id' => $language_factory->id,
            'subchannel_id' => $subChannelFactory->id,
            'name' => 'nombre subcanal idioma factory test'
        ]);

        $this->json('GET','/api/v1/subchannel', [
            'lang' => 'TEST',
            'name' => 'factory test'
        ])->assertJsonFragment(['id' => $subChannelFactory->id]);

    }

}