<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ChannelsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/channel', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/channel', [
            'ids' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/channel', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' => [["ids.0" => ["The selected ids.0 is invalid."]]]
            ]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $this->assertDatabaseHas('mo_channel', [
            'id' => $channelFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/channel', [
            'ids' => $channelFactory->id,
        ])
        ->assertExactJson([
            "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
        ]);

    }


    /**
     * Test para eliminar un channel que crea primero un channel factory, sus traducciones y relación en tabla intermedia
     */
    public function testOk1()
    {
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);



        $translation1 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 1,
            'channel_id' => $channelFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 2,
            'channel_id' => $channelFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\ChannelTranslation::class)->create([
            'language_id' => 3,
            'channel_id' => $channelFactory->id,
        ]);


        $this->json('DELETE', '/api/v1/channel', [
            'ids' => [$channelFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_channel el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_channel', ['deleted_at' => null, 'id' => $channelFactory->id]);
        //Comprueba que las traducciones del channel que han sido creadas no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_channel_translation', ['deleted_at' => null, 'channel_id' => $translation1->channel_id]);
        $this->assertDatabaseMissing('mo_channel_translation', ['deleted_at' => null, 'channel_id' => $translation2->channel_id]);
        $this->assertDatabaseMissing('mo_channel_translation', ['deleted_at' => null, 'channel_id' => $translation3->channel_id]);

    }


}