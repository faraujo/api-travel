<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Carbon\Carbon;

class AvailabilitiesCreateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parametros requeridos
     */
    public function testCreateWithOutParameters()
    {
        $this->json('POST', '/api/v1/availability')
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["date_end" => ["The date end field is required."], "date_start" => ["The date start field is required."], "locations" => ["The locations field is required."], "product_id" => ["The product id field is required."]]]]);
    }


    /**
     * Prueba automatizada solo location id presente en array locations
     */
    public function testCreateLocation()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);


        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $location->id,

                ],
            ],

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            "error_inputs" => [[
                "locations.0.avail" => ["The locations.0.avail field is required when none of locations.0.quota / locations.0.closed / locations.0.unlimited / locations.0.availabilities / locations.0.services are present."],
                "locations.0.availabilities" => ["The locations.0.availabilities field is required when none of locations.0.quota / locations.0.avail / locations.0.closed / locations.0.unlimited / locations.0.services are present."],
                "locations.0.closed" => ["The locations.0.closed field is required when none of locations.0.quota / locations.0.avail / locations.0.unlimited / locations.0.availabilities / locations.0.services are present."],
                "locations.0.quota" => ["The locations.0.quota field is required when none of locations.0.avail / locations.0.closed / locations.0.unlimited / locations.0.availabilities / locations.0.services are present."],
                "locations.0.unlimited" => ["The locations.0.unlimited field is required when none of locations.0.quota / locations.0.avail / locations.0.closed / locations.0.availabilities / locations.0.services are present."],
                "locations.0.services" => ["The locations.0.services field is required when none of locations.0.quota / locations.0.avail / locations.0.closed / locations.0.unlimited / locations.0.availabilities are present."]]]]);

    }


    /**
     * Prueba automatizada fecha de fin anterior a fecha inicio
     */
    public function testCreateEndBeforeStart()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);


        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'date_end' => Carbon::now()->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $location->id,
                    'quota' => 50,

                ],
            ],

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ['The date end must be a date after or equal to date start.'],
        ]]]);
    }

    /**
     * Prueba automatizada fecha de inicio anterior a fecha actual
     */
    public function testCreateStartBeforeNow()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->subYears(3)->format('Y-m-d'),
            'date_end' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $location->id,
                    'quota' => 50,

                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_start' => ['The date start must be a date after or equal to today.'],
        ]]]);
    }

    /**
     * Prueba automatizada date_start formato invalido
     */
    public function testDateStartInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => '15/12/2019',
            'date_end' => '2020/1/25',
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $location->id,
                    'quota' => 50,

                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ['The date end does not match the format Y-m-d.', 'The date end must be a date after or equal to date start.'],
            'date_start' => ['The date start does not match the format Y-m-d.', 'The date start is not a valid date.', 'The date start must be a date after or equal to today.'],
        ]]]);
    }

    /**
     * Prueba automatizada date_end formato invalido
     */
    public function testDateEndInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => '2019/1/25',
            'date_end' => '15/12/2019',
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $location->id,
                    'quota' => 50,

                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ['The date end does not match the format Y-m-d.', 'The date end is not a valid date.', 'The date end must be a date after or equal to date start.'],
            'date_start' => ['The date start does not match the format Y-m-d.','The date start must be a date after or equal to today.'],
        ]]]);
    }


    /**
     * Prueba automatizada overbooking sin unidad
     */
    public function testOverBookingUnit()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $location->id,
                    'quota' => 50,
                    'overbooking' => 50,

                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[

            'locations.0.overbooking_unit' => ["The locations.0.overbooking_unit field is required when locations.0.overbooking is present."]

        ]]]);
    }


    /**
     * Prueba automatizada producto_id y location_id no existen en base de datos
     */
    public function testCreateProductNotExist()
    {
        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => 99999999999999999999999999,
            'locations' => [
                0 => [
                    'location_id' => 9999999999999999,
                    'quota' => 50,
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_id' => ['The selected product id is invalid.'],
            'locations.0.location_id' => ['The selected locations.0.location_id is invalid.'],
        ]]]);
    }

    /**
     * Prueba automatizada producto_id formato incorrecto
     */
    public function testProductBadFormat()
    {
        $location = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => 'r',
            'locations' => [
                0 => [
                    'location_id' => $location->id,
                    'quota' => 50,
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_id' => ['The product id must be a number.'],
        ]]]);
    }

    /**
     * Prueba automatizada location_id formato incorrecto
     */
    public function testLocationBadFormat()
    {

        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $this->json('POST', '/api/v1/availability', [
            'date_start' =>  Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => 'r',
                    'quota' => 50,
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.location_id' => ['The locations.0.location_id must be an integer.'],
        ]]]);
    }


    /**
     * Prueba automatizada closed y unlimited formato invalido
     */
    public function testClosedUnlimitedInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'closed' => 4,
                    'unlimited' => 8,
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.closed' => ['The locations.0.closed field must be true or false.'],
            'locations.0.unlimited' => ['The locations.0.unlimited field must be true or false.']

        ]]]);
    }

    /**
     * Prueba automatizada quota formato invalido
     */
    public function testQuotaInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 'asdf',

                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.quota' => ['The locations.0.quota must be an integer.'],
        ]]]);
    }

    /**
     * Prueba automatizada avail formato inválido
     */
    public function testAvailInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'avail' => 'asdf'
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.avail' => ['The locations.0.avail must be an integer.'],
        ]]]);
    }

    /**
     * Prueba automatizada overbooking formato inválido
     */
    public function testOverbookingInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'overbooking' => 'asdf',
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.overbooking' => ['The locations.0.overbooking must be an integer.'],
            'locations.0.overbooking_unit' => ['The locations.0.overbooking_unit field is required when locations.0.overbooking is present.'],
        ]]]);
    }

    /**
     * Prueba automatizada overbooking_unit formato invalido
     */
    public function testOverbookingUnitInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'overbooking' => 40,
                    'overbooking_unit' => 'r',
                ],

            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.overbooking_unit' => ['The selected locations.0.overbooking_unit is invalid.'],
        ]]]);
    }


    /**
     * Prueba automatizada fecha de inicio y fecha de fin con sesión y sin ningun otro parametro
     */
    public function testCreateWithSessionNoOtherParameter()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $location->id,

                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                        ],
                    ],
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            "error_inputs" => [["locations.0.availabilities.0.avail" => ["The locations.0.availabilities.0.avail field is required when none of locations.0.availabilities.0.quota / locations.0.availabilities.0.closed / locations.0.availabilities.0.unlimited are present."],
                "locations.0.availabilities.0.closed" => ["The locations.0.availabilities.0.closed field is required when none of locations.0.availabilities.0.quota / locations.0.availabilities.0.avail / locations.0.availabilities.0.unlimited are present."],
                "locations.0.availabilities.0.quota" => ["The locations.0.availabilities.0.quota field is required when none of locations.0.availabilities.0.avail / locations.0.availabilities.0.closed / locations.0.availabilities.0.unlimited are present."],
                "locations.0.availabilities.0.unlimited" => ["The locations.0.availabilities.0.unlimited field is required when none of locations.0.availabilities.0.quota / locations.0.availabilities.0.avail / locations.0.availabilities.0.closed are present."]
            ]]]);
    }


    /**
     * Prueba automatizada session formato invalido
     */
    public function testSessionInvalidFormat()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,

                    'availabilities' => [
                        0 => [
                            'session' => '88',
                            'quota' => 50,
                        ],
                    ],
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.availabilities.0.session' => ['The locations.0.availabilities.0.session does not match the format H:i.'],
        ]]]);
    }


    /**
     * Prueba automatizada envío parametros arrat availability formato erróneo
     */
    public function testSessionOtherParametersInvalidFormat()
    {

        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,

                    'availabilities' => [
                        0 => [
                            'session' => '88',
                            'quota' => 'r',
                            'avail' => 't',
                            'overbooking' => 'o',
                            'overbooking_unit' => 'p',
                            'closed' => '9',
                            'unlimited' => '7'
                        ],
                    ],
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', "error_inputs" => [["locations.0.availabilities.0.avail" => ["The locations.0.availabilities.0.avail must be an integer."],
            "locations.0.availabilities.0.closed" => ["The locations.0.availabilities.0.closed field must be true or false."],
            "locations.0.availabilities.0.overbooking" => ["The locations.0.availabilities.0.overbooking must be an integer."],
            "locations.0.availabilities.0.overbooking_unit" => ["The selected locations.0.availabilities.0.overbooking_unit is invalid."],
            "locations.0.availabilities.0.quota" => ["The locations.0.availabilities.0.quota must be an integer."],
            "locations.0.availabilities.0.session" => ["The locations.0.availabilities.0.session does not match the format H:i."],
            "locations.0.availabilities.0.unlimited" => ["The locations.0.availabilities.0.unlimited field must be true or false."]]]]);

    }

    /**
     * Prueba automatizada overbooking requerido cuando se envía overbooking en array availability
     */
    public function testSessionOverboolingRequired()
    {

        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,

                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                            'quota' => '50',
                            'overbooking' => '20',

                        ],
                    ],
                ],
            ],
        ])->assertExactJson(['error' => 400,
            "error_description" => "The fields are not the required format",
            'error_inputs' => [["locations.0.availabilities.0.overbooking_unit" => ["The locations.0.availabilities.0.overbooking_unit field is required when locations.0.availabilities.0.overbooking is present."]]]]);

    }


    /**
     * Prueba automatizada envío parámetros array services con formato incorrecto
     */
    public function testServiceWithoutOtherParameters()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $productFactory->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,


                        ],
                    ]
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            'error_inputs' => [["locations.0.services.0.avail" => ["The locations.0.services.0.avail field is required when none of locations.0.services.0.quota / locations.0.services.0.closed / locations.0.services.0.unlimited / locations.0.services.0.availabilities are present."],
                "locations.0.services.0.availabilities" => ["The locations.0.services.0.availabilities field is required when none of locations.0.services.0.quota / locations.0.services.0.avail / locations.0.services.0.closed / locations.0.services.0.unlimited are present."],
                "locations.0.services.0.closed" => ["The locations.0.services.0.closed field is required when none of locations.0.services.0.quota / locations.0.services.0.avail / locations.0.services.0.unlimited / locations.0.services.0.availabilities are present."],
                "locations.0.services.0.quota" => ["The locations.0.services.0.quota field is required when none of locations.0.services.0.avail / locations.0.services.0.closed / locations.0.services.0.unlimited / locations.0.services.0.availabilities are present."],
                "locations.0.services.0.unlimited" => ["The locations.0.services.0.unlimited field is required when none of locations.0.services.0.quota / locations.0.services.0.avail / locations.0.services.0.closed / locations.0.services.0.availabilities are present."]]]]);
    }


    /**
     * Prueba automatizada envío array service con parámetros con formato incorrecto
     */
    public function testServiceWrongFormat()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $productFactory->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'services' => [
                        0 => [
                            'service_id' => 't',
                            'quota' => 'r',
                            'avail' => 't',
                            'overbooking' => 'o',
                            'overbooking_unit' => 'p',
                            'closed' => '9',
                            'unlimited' => '7'

                        ],
                    ]
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            'error_inputs' => [["locations.0.services.0.avail" => ["The locations.0.services.0.avail must be an integer."],
                "locations.0.services.0.closed" => ["The locations.0.services.0.closed field must be true or false."],
                "locations.0.services.0.overbooking" => ["The locations.0.services.0.overbooking must be an integer."],
                "locations.0.services.0.overbooking_unit" => ["The selected locations.0.services.0.overbooking_unit is invalid."],
                "locations.0.services.0.quota" => ["The locations.0.services.0.quota must be an integer."],
                "locations.0.services.0.service_id" => ["The locations.0.services.0.service_id must be an integer."],
                "locations.0.services.0.unlimited" => ["The locations.0.services.0.unlimited field must be true or false."]]]]);
    }

    /**
     * Prueba automatizada envío array service con overbooking, requiere overbooking_unit
     */
    public function testServiceOverBookingUnitRequired()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $productFactory->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'quota' => 20,
                            'overbooking' => '20',
                        ],
                    ]
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            'error_inputs' => [["locations.0.services.0.overbooking_unit" => ["The locations.0.services.0.overbooking_unit field is required when locations.0.services.0.overbooking is present."]]]]);
    }


    /**
     * Prueba automatizada service_id no existe en base de datos
     */
    public function testCreateServiceNotExists()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $productFactory->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'services' => [
                        0 => [
                            'service_id' => 99999999999999999999,
                            'closed' => 1,
                        ],
                    ]
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services.0.service_id' => ['The locations.0.services.0.service_id must be an integer.'],
        ]]]);
    }


    /**
     * Prueba automatizada envío array availability de services con sesion y ningún otro parámetro
     */
    public function testServiceSessionWithoutOtherParameters()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' =>  Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $productFactory->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',

                                ],

                            ]

                        ],
                    ]
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            "error_inputs" => [["locations.0.services.0.availabilities.0.avail" => ["The locations.0.services.0.availabilities.0.avail field is required when none of locations.0.services.0.availabilities.0.quota / locations.0.services.0.availabilities.0.closed / locations.0.services.0.availabilities.0.unlimited are present."],
                "locations.0.services.0.availabilities.0.closed" => ["The locations.0.services.0.availabilities.0.closed field is required when none of locations.0.services.0.availabilities.0.quota / locations.0.services.0.availabilities.0.avail / locations.0.services.0.availabilities.0.unlimited are present."],
                "locations.0.services.0.availabilities.0.quota" => ["The locations.0.services.0.availabilities.0.quota field is required when none of locations.0.services.0.availabilities.0.avail / locations.0.services.0.availabilities.0.closed / locations.0.services.0.availabilities.0.unlimited are present."],
                "locations.0.services.0.availabilities.0.unlimited" => ["The locations.0.services.0.availabilities.0.unlimited field is required when none of locations.0.services.0.availabilities.0.quota / locations.0.services.0.availabilities.0.avail / locations.0.services.0.availabilities.0.closed are present."]]]]);
    }


    /**
     * Prueba automatizada envío array availability de services con errores de formato
     */
    public function testServiceSessionBadFormat()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $productFactory->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'availabilities' => [
                                0 => [
                                    'session' => 'o',
                                    'quota' => 'r',
                                    'avail' => 'u',
                                    'overbooking' => 'o',
                                    'overbooking_unit' => 'p',
                                    'closed' => '9',
                                    'unlimited' => '8'

                                ],

                            ]

                        ],
                    ]
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            "error_inputs" => [[
                "locations.0.services.0.availabilities.0.avail" => ["The locations.0.services.0.availabilities.0.avail must be an integer."],
                "locations.0.services.0.availabilities.0.closed" => ["The locations.0.services.0.availabilities.0.closed field must be true or false."],
                "locations.0.services.0.availabilities.0.overbooking" => ["The locations.0.services.0.availabilities.0.overbooking must be an integer."],
                "locations.0.services.0.availabilities.0.overbooking_unit" => ["The selected locations.0.services.0.availabilities.0.overbooking_unit is invalid."],
                "locations.0.services.0.availabilities.0.quota" => ["The locations.0.services.0.availabilities.0.quota must be an integer."],
                "locations.0.services.0.availabilities.0.session" => ["The locations.0.services.0.availabilities.0.session does not match the format H:i."],
                "locations.0.services.0.availabilities.0.unlimited" => ["The locations.0.services.0.availabilities.0.unlimited field must be true or false."]]]]);

    }


    /**
     * Prueba automatizada envío array availability de services con overbooking, solicita overbooking unit
     */
    public function testServiceSessionOverBookingUnitRequired()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'product_id' => $productFactory->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 50,
                                    'overbooking' => '10',


                                ],

                            ]

                        ],
                    ]
                ],
            ],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
            "error_inputs" => [["locations.0.services.0.availabilities.0.overbooking_unit" => ["The locations.0.services.0.availabilities.0.overbooking_unit field is required when locations.0.services.0.availabilities.0.overbooking is present."]]]]);

    }

    /**
     * Prueba automatizada, crea un registro en la base de datos que no existe con una localización
     */
    public function testOk1()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);

        $fecha_inicio = Carbon::now()->format('Y-m-d');
        $fecha_fin = Carbon::now()->addDays(3)->format('Y-m-d');

        $this->json('POST', '/api/v1/availability', [
            'date_start' => $fecha_inicio,
            'date_end' => $fecha_fin,
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'unlimited' => 0,
                    'closed' => 1,
                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                            'quota' => 40,
                            'avail' => 10,
                            'overbooking' => 5,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 1,
                        ],

                        1 => [
                            'session' => '11:00',
                            'quota' => 30,
                            'avail' => 5,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 0,
                            'closed' => 0,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'quota' => 90,
                            'avail' => 70,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 60,
                                    'avail' => 20,
                                    'overbooking' => 10,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 25,
                                    'avail' => 15,
                                    'overbooking' => 5,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory2->id,
                            'quota' => 10,
                            'avail' => 20,
                            'overbooking' => 30,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 90,
                                    'avail' => 80,
                                    'overbooking' => 70,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 60,
                                    'avail' => 50,
                                    'overbooking' => 40,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 1,
                                ]
                            ],

                        ],


                    ],


                ],
            ],
        ])->assertExactJson(['error' => 200]);


        // cupo general producto para una localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => null,
                'quota' => 50,
                'avail' => 20,
                'overbooking' => 15,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );

        // cupo general primer servicio para producto para una localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory->id,
                'quota' => 90,
                'avail' => 70,
                'overbooking' => 15,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );


        // cupo general segundo servicio para producto para una localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory2->id,
                'quota' => 10,
                'avail' => 20,
                'overbooking' => 30,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );

        $fecha_inicio_carbon = Carbon::createFromFormat('Y-m-d', $fecha_inicio);
        $fecha_fin_carbon = Carbon::createFromFormat('Y-m-d', $fecha_fin);

        $fecha_actual_carbon = $fecha_inicio_carbon;

        while ($fecha_actual_carbon <= $fecha_fin_carbon) { //comprueba sesiones para producto y los dos servicios entre las fechas solicitadas

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 40,
                    'avail' => 10,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 30,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 60,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 25,
                    'avail' => 15,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 90,
                    'avail' => 80,
                    'overbooking' => 70,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 60,
                    'avail' => 50,
                    'overbooking' => 40,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $fecha_actual_carbon->addDay();

        }
    }

    /**
     * Prueba automatizada, crea un registro en la base de datos que no existe con dos localizaciones
     */

    public function testOk2()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([]);


        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory3 = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory4 = factory(App\Models\tenant\Service::class)->create([]);

        $fecha_inicio = Carbon::now()->format('Y-m-d');
        $fecha_fin = Carbon::now()->addDays(3)->format('Y-m-d');

        $this->json('POST', '/api/v1/availability', [
            'date_start' => $fecha_inicio,
            'date_end' => $fecha_fin,
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'unlimited' => 0,
                    'closed' => 1,
                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                            'quota' => 40,
                            'avail' => 10,
                            'overbooking' => 5,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 1,
                        ],

                        1 => [
                            'session' => '11:00',
                            'quota' => 30,
                            'avail' => 5,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 0,
                            'closed' => 0,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'quota' => 90,
                            'avail' => 70,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 60,
                                    'avail' => 20,
                                    'overbooking' => 10,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 25,
                                    'avail' => 15,
                                    'overbooking' => 5,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory2->id,
                            'quota' => 10,
                            'avail' => 20,
                            'overbooking' => 30,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 90,
                                    'avail' => 80,
                                    'overbooking' => 70,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 60,
                                    'avail' => 50,
                                    'overbooking' => 40,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 1,
                                ]
                            ],

                        ],


                    ],


                ],


                1 => [
                    'location_id' => $locationFactory2->id,
                    'quota' => 25,
                    'avail' => 35,
                    'overbooking' => 45,
                    'overbooking_unit' => '%',
                    'unlimited' => 1,
                    'closed' => 0,
                    'availabilities' => [
                        0 => [
                            'session' => '13:00',
                            'quota' => 45,
                            'avail' => 15,
                            'overbooking' => 10,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 0,
                        ],

                        1 => [
                            'session' => '17:00',
                            'quota' => 35,
                            'avail' => 15,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 1,
                            'closed' => 0,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory3->id,
                            'quota' => 95,
                            'avail' => 75,
                            'overbooking' => 25,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '13:00',
                                    'quota' => 65,
                                    'avail' => 25,
                                    'overbooking' => 15,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '17:00',
                                    'quota' => 20,
                                    'avail' => 10,
                                    'overbooking' => 20,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory4->id,
                            'quota' => 15,
                            'avail' => 25,
                            'overbooking' => 35,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '13:00',
                                    'quota' => 95,
                                    'avail' => 85,
                                    'overbooking' => 75,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '17:00',
                                    'quota' => 65,
                                    'avail' => 55,
                                    'overbooking' => 45,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 0,
                                ]
                            ],

                        ],


                    ],


                ],

            ],
        ])->assertExactJson(['error' => 200]);


        // cupo general producto para primera localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => null,
                'quota' => 50,
                'avail' => 20,
                'overbooking' => 15,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );

        // cupo general primer servicio para producto para primera localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory->id,
                'quota' => 90,
                'avail' => 70,
                'overbooking' => 15,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );


        // cupo general segundo servicio para producto para primera localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory2->id,
                'quota' => 10,
                'avail' => 20,
                'overbooking' => 30,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );

        $fecha_inicio_carbon = Carbon::createFromFormat('Y-m-d', $fecha_inicio);
        $fecha_fin_carbon = Carbon::createFromFormat('Y-m-d', $fecha_fin);

        $fecha_actual_carbon = $fecha_inicio_carbon;
        $fecha_actual_carbon2 = $fecha_inicio_carbon;


        while ($fecha_actual_carbon <= $fecha_fin_carbon) { //comprueba sesiones para producto y los dos servicios entre las fechas solicitadas

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 40,
                    'avail' => 10,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 30,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 60,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 25,
                    'avail' => 15,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 90,
                    'avail' => 80,
                    'overbooking' => 70,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 60,
                    'avail' => 50,
                    'overbooking' => 40,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $fecha_actual_carbon->addDay();

        }

        // cupo general producto para segunda localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory2->id,
                'service_id' => null,
                'quota' => 25,
                'avail' => 35,
                'overbooking' => 45,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );

        // cupo general primer servicio para producto para segunda localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory2->id,
                'service_id' => $serviceFactory3->id,
                'quota' => 95,
                'avail' => 75,
                'overbooking' => 25,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );


        // cupo general segundo servicio para producto para segunda localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory2->id,
                'service_id' => $serviceFactory4->id,
                'quota' => 15,
                'avail' => 25,
                'overbooking' => 35,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );


        while ($fecha_actual_carbon2 <= $fecha_fin_carbon) { //comprueba sesiones para producto y los dos servicios entre las fechas solicitadas

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '13:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 45,
                    'avail' => 15,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '17:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 35,
                    'avail' => 15,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '13:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 65,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '17:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 20,
                    'avail' => 10,
                    'overbooking' => 20,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '13:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 95,
                    'avail' => 85,
                    'overbooking' => 75,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '17:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 65,
                    'avail' => 55,
                    'overbooking' => 45,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $fecha_actual_carbon2->addDay();

        }

    }

    /**
     * Prueba automatizada, crea un registro en la base de datos que no existe con una localización y posteriormente modifica dicha petición cambiando algunos registros y creando otros
     */
    public function testOk3()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);

        $fecha_inicio = Carbon::now()->format('Y-m-d');
        $fecha_fin = Carbon::now()->addDays(3)->format('Y-m-d');

        $this->json('POST', '/api/v1/availability', [
            'date_start' => $fecha_inicio,
            'date_end' => $fecha_fin,
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'unlimited' => 0,
                    'closed' => 1,
                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                            'quota' => 40,
                            'avail' => 10,
                            'overbooking' => 5,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 1,
                        ],

                        1 => [
                            'session' => '11:00',
                            'quota' => 30,
                            'avail' => 5,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 0,
                            'closed' => 0,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'quota' => 90,
                            'avail' => 70,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 60,
                                    'avail' => 20,
                                    'overbooking' => 10,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 25,
                                    'avail' => 15,
                                    'overbooking' => 5,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory2->id,
                            'quota' => 10,
                            'avail' => 20,
                            'overbooking' => 30,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 90,
                                    'avail' => 80,
                                    'overbooking' => 70,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 60,
                                    'avail' => 50,
                                    'overbooking' => 40,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 1,
                                ]
                            ],

                        ],


                    ],


                ],
            ],
        ])->assertExactJson(['error' => 200]);


        $fecha_inicio2 = Carbon::now()->addDays(2)->format('Y-m-d');
        $fecha_fin2 = Carbon::now()->addDays(4)->format('Y-m-d');


        $this->json('POST', '/api/v1/availability', [
            'date_start' => $fecha_inicio2,
            'date_end' => $fecha_fin2,
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 55,
                    'avail' => 35,
                    'overbooking' => 24,
                    'overbooking_unit' => '%',
                    'unlimited' => 1,
                    'closed' => 0,
                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                            'quota' => 50,
                            'avail' => 20,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 0,
                        ],

                        1 => [
                            'session' => '11:00',
                            'quota' => 35,
                            'avail' => 25,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 1,
                            'closed' => 1,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'quota' => 95,
                            'avail' => 75,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 65,
                                    'avail' => 25,
                                    'overbooking' => 15,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 20,
                                    'avail' => 10,
                                    'overbooking' => 15,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory2->id,
                            'quota' => 15,
                            'avail' => 25,
                            'overbooking' => 35,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 95,
                                    'avail' => 85,
                                    'overbooking' => 75,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 65,
                                    'avail' => 55,
                                    'overbooking' => 45,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 0,
                                ]
                            ],

                        ],


                    ],


                ],
            ],
        ])->assertExactJson(['error' => 200]);

        // cupo general producto modficado para una localización


        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => null,
                'quota' => 55,
                'avail' => 35,
                'overbooking' => 24,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );

        // cupo general primer servicio para producto para una localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory->id,
                'quota' => 95,
                'avail' => 75,
                'overbooking' => 10,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );


        // cupo general segundo servicio para producto para una localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory2->id,
                'quota' => 15,
                'avail' => 25,
                'overbooking' => 35,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );

        $fecha_inicio_carbon = Carbon::createFromFormat('Y-m-d', $fecha_inicio);
        $fecha_fin_carbon = Carbon::createFromFormat('Y-m-d', $fecha_fin);

        $fecha_inicio2_carbon = Carbon::createFromFormat('Y-m-d', $fecha_inicio2);
        $fecha_fin2_carbon = Carbon::createFromFormat('Y-m-d', $fecha_fin2);

        $fecha_actual_carbon = $fecha_inicio_carbon;

        while ($fecha_actual_carbon < $fecha_inicio2_carbon) { //comprueba sesiones para producto y los dos servicios que no se modifican

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 40,
                    'avail' => 10,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 30,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 60,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 25,
                    'avail' => 15,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 90,
                    'avail' => 80,
                    'overbooking' => 70,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 60,
                    'avail' => 50,
                    'overbooking' => 40,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $fecha_actual_carbon->addDay();

        }

        while ($fecha_actual_carbon <= $fecha_fin_carbon) { //comprueba sesiones para producto y los dos servicios que se han modificado

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 35,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 65,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 20,
                    'avail' => 10,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 95,
                    'avail' => 85,
                    'overbooking' => 75,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 65,
                    'avail' => 55,
                    'overbooking' => 45,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $fecha_actual_carbon->addDay();

        }


        while ($fecha_actual_carbon <= $fecha_fin2_carbon) { //comprueba sesiones para producto y los dos servicios que se han creado con la segunda petición

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 35,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 65,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 20,
                    'avail' => 10,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 95,
                    'avail' => 85,
                    'overbooking' => 75,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 65,
                    'avail' => 55,
                    'overbooking' => 45,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $fecha_actual_carbon->addDay();

        }

    }

    /**
     * Prueba automatizada, crea un registro en la base de datos que no existe con dos localizaciones y posteriormente modifica dicha petición cambiando algunos registros y creando otros
     */
    public function testOk4()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory3 = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory4 = factory(App\Models\tenant\Service::class)->create([]);

        $fecha_inicio = Carbon::now()->format('Y-m-d');
        $fecha_fin = Carbon::now()->addDays(3)->format('Y-m-d');

        $this->json('POST', '/api/v1/availability', [
            'date_start' => $fecha_inicio,
            'date_end' => $fecha_fin,
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'unlimited' => 0,
                    'closed' => 1,
                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                            'quota' => 40,
                            'avail' => 10,
                            'overbooking' => 5,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 1,
                        ],

                        1 => [
                            'session' => '11:00',
                            'quota' => 30,
                            'avail' => 5,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 0,
                            'closed' => 0,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'quota' => 90,
                            'avail' => 70,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 60,
                                    'avail' => 20,
                                    'overbooking' => 10,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 25,
                                    'avail' => 15,
                                    'overbooking' => 5,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory2->id,
                            'quota' => 10,
                            'avail' => 20,
                            'overbooking' => 30,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 90,
                                    'avail' => 80,
                                    'overbooking' => 70,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 60,
                                    'avail' => 50,
                                    'overbooking' => 40,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 1,
                                ]
                            ],

                        ],


                    ],


                ],

                1 => [
                    'location_id' => $locationFactory2->id,
                    'quota' => 75,
                    'avail' => 35,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'unlimited' => 0,
                    'closed' => 0,
                    'availabilities' => [
                        0 => [
                            'session' => '17:00',
                            'quota' => 45,
                            'avail' => 15,
                            'overbooking' => 10,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 0,
                        ],

                        1 => [
                            'session' => '19:00',
                            'quota' => 35,
                            'avail' => 20,
                            'overbooking' => 14,
                            'overbooking_unit' => 'u',
                            'unlimited' => 1,
                            'closed' => 1,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory3->id,
                            'quota' => 65,
                            'avail' => 45,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '11:00',
                                    'quota' => 55,
                                    'avail' => 15,
                                    'overbooking' => 15,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '13:00',
                                    'quota' => 10,
                                    'avail' => 20,
                                    'overbooking' => 15,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory4->id,
                            'quota' => 25,
                            'avail' => 5,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '10:00',
                                    'quota' => 45,
                                    'avail' => 25,
                                    'overbooking' => 15,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '12:00',
                                    'quota' => 40,
                                    'avail' => 35,
                                    'overbooking' => 20,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 0,
                                ]
                            ],

                        ],


                    ],


                ],


            ],
        ])->assertExactJson(['error' => 200]);


        $fecha_inicio2 = Carbon::now()->addDays(2)->format('Y-m-d');
        $fecha_fin2 = Carbon::now()->addDays(4)->format('Y-m-d');


        $this->json('POST', '/api/v1/availability', [
            'date_start' => $fecha_inicio2,
            'date_end' => $fecha_fin2,
            'product_id' => $product->id,
            'locations' => [
                0 => [
                    'location_id' => $locationFactory->id,
                    'quota' => 55,
                    'avail' => 35,
                    'overbooking' => 24,
                    'overbooking_unit' => '%',
                    'unlimited' => 1,
                    'closed' => 0,
                    'availabilities' => [
                        0 => [
                            'session' => '09:00',
                            'quota' => 50,
                            'avail' => 20,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 0,
                        ],

                        1 => [
                            'session' => '11:00',
                            'quota' => 35,
                            'avail' => 25,
                            'overbooking' => 15,
                            'overbooking_unit' => 'u',
                            'unlimited' => 1,
                            'closed' => 1,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory->id,
                            'quota' => 95,
                            'avail' => 75,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 65,
                                    'avail' => 25,
                                    'overbooking' => 15,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 20,
                                    'avail' => 10,
                                    'overbooking' => 15,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory2->id,
                            'quota' => 15,
                            'avail' => 25,
                            'overbooking' => 35,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '09:00',
                                    'quota' => 95,
                                    'avail' => 85,
                                    'overbooking' => 75,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ],

                                1 => [
                                    'session' => '11:00',
                                    'quota' => 65,
                                    'avail' => 55,
                                    'overbooking' => 45,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 0,
                                ]
                            ],

                        ],


                    ],


                ],

                1 => [
                    'location_id' => $locationFactory2->id,
                    'quota' => 46,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'unlimited' => 1,
                    'closed' => 1,
                    'availabilities' => [
                        0 => [
                            'session' => '17:00',
                            'quota' => 15,
                            'avail' => 5,
                            'overbooking' => 25,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 0,
                        ],

                        1 => [
                            'session' => '19:00',
                            'quota' => 30,
                            'avail' => 15,
                            'overbooking' => 5,
                            'overbooking_unit' => '%',
                            'unlimited' => 0,
                            'closed' => 1,
                        ]
                    ],

                    'services' => [
                        0 => [
                            'service_id' => $serviceFactory3->id,
                            'quota' => 40,
                            'avail' => 30,
                            'overbooking' => 5,
                            'overbooking_unit' => 'u',
                            'unlimited' => 0,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '11:00',
                                    'quota' => 50,
                                    'avail' => 20,
                                    'overbooking' => 10,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '13:00',
                                    'quota' => 15,
                                    'avail' => 5,
                                    'overbooking' => 10,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ]
                            ],

                        ],

                        1 => [
                            'service_id' => $serviceFactory4->id,
                            'quota' => 40,
                            'avail' => 25,
                            'overbooking' => 10,
                            'overbooking_unit' => '%',
                            'unlimited' => 1,
                            'closed' => 1,
                            'availabilities' => [
                                0 => [
                                    'session' => '10:00',
                                    'quota' => 23,
                                    'avail' => 5,
                                    'overbooking' => 10,
                                    'overbooking_unit' => '%',
                                    'unlimited' => 0,
                                    'closed' => 1,
                                ],

                                1 => [
                                    'session' => '12:00',
                                    'quota' => 35,
                                    'avail' => 20,
                                    'overbooking' => 10,
                                    'overbooking_unit' => 'u',
                                    'unlimited' => 1,
                                    'closed' => 0,
                                ]
                            ],

                        ],


                    ],


                ],






            ],
        ])->assertExactJson(['error' => 200]);

        // cupo general producto modficado para primera localización


        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => null,
                'quota' => 55,
                'avail' => 35,
                'overbooking' => 24,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );


        // cupo general producto modficado para primera localización


        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory2->id,
                'service_id' => null,
                'quota' => 46,
                'avail' => 20,
                'overbooking' => 15,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 1

            ]
        );


        // cupo general primer servicio para producto para primera localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory->id,
                'quota' => 95,
                'avail' => 75,
                'overbooking' => 10,
                'overbooking_unit' => '%',
                'closed' => 0,
                'unlimited' => 1

            ]
        );

        // cupo general primer servicio para producto para segunda localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory2->id,
                'service_id' => $serviceFactory3->id,
                'quota' => 40,
                'avail' => 30,
                'overbooking' => 5,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );


        // cupo general segundo servicio para producto para una localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory->id,
                'service_id' => $serviceFactory2->id,
                'quota' => 15,
                'avail' => 25,
                'overbooking' => 35,
                'overbooking_unit' => 'u',
                'closed' => 1,
                'unlimited' => 0

            ]
        );


        // cupo general segundo servicio para producto para segunda localización

        $this->assertDatabaseHas('mo_availability', [

                'date' => null,
                'session' => null,
                'product_id' => $product->id,
                'location_id' => $locationFactory2->id,
                'service_id' => $serviceFactory4->id,
                'quota' => 40,
                'avail' => 25,
                'overbooking' => 10,
                'overbooking_unit' => '%',
                'closed' => 1,
                'unlimited' => 1

            ]
        );

        $fecha_inicio_carbon = Carbon::createFromFormat('Y-m-d', $fecha_inicio);
        $fecha_fin_carbon = Carbon::createFromFormat('Y-m-d', $fecha_fin);

        $fecha_inicio2_carbon = Carbon::createFromFormat('Y-m-d', $fecha_inicio2);
        $fecha_fin2_carbon = Carbon::createFromFormat('Y-m-d', $fecha_fin2);

        $fecha_actual_carbon = $fecha_inicio_carbon;

        while ($fecha_actual_carbon < $fecha_inicio2_carbon) { //comprueba sesiones para producto y los dos servicios que no se modifican para las dos localizaciones

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 40,
                    'avail' => 10,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 30,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 60,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 25,
                    'avail' => 15,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 90,
                    'avail' => 80,
                    'overbooking' => 70,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 60,
                    'avail' => 50,
                    'overbooking' => 40,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );


            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '17:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 45,
                    'avail' => 15,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '19:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 35,
                    'avail' => 20,
                    'overbooking' => 14,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );


            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 55,
                    'avail' => 15,
                    'overbooking' => 15,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '13:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 10,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '10:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 45,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '12:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 40,
                    'avail' => 35,
                    'overbooking' => 20,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );







            $fecha_actual_carbon->addDay();

        }

        while ($fecha_actual_carbon <= $fecha_fin_carbon) { //comprueba sesiones para producto y los dos servicios que se han modificado para cada localización

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 35,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 65,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 20,
                    'avail' => 10,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 95,
                    'avail' => 85,
                    'overbooking' => 75,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 65,
                    'avail' => 55,
                    'overbooking' => 45,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '17:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 15,
                    'avail' => 5,
                    'overbooking' => 25,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '19:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 30,
                    'avail' => 15,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );


            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '13:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 15,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '10:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 23,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '12:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 35,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );





            $fecha_actual_carbon->addDay();

        }


        while ($fecha_actual_carbon <= $fecha_fin2_carbon) { //comprueba sesiones para producto y los dos servicios que se han creado con la segunda petición para cada localización

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => null,
                    'quota' => 35,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 65,
                    'avail' => 25,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory->id,
                    'quota' => 20,
                    'avail' => 10,
                    'overbooking' => 15,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '09:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 95,
                    'avail' => 85,
                    'overbooking' => 75,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory->id,
                    'service_id' => $serviceFactory2->id,
                    'quota' => 65,
                    'avail' => 55,
                    'overbooking' => 45,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '17:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 15,
                    'avail' => 5,
                    'overbooking' => 25,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '19:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => null,
                    'quota' => 30,
                    'avail' => 15,
                    'overbooking' => 5,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );


            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '11:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 50,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '13:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory3->id,
                    'quota' => 15,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '10:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 23,
                    'avail' => 5,
                    'overbooking' => 10,
                    'overbooking_unit' => '%',
                    'closed' => 1,
                    'unlimited' => 0

                ]
            );

            $this->assertDatabaseHas('mo_availability', [

                    'date' => $fecha_actual_carbon->toDateString(),
                    'session' => '12:00:00',
                    'product_id' => $product->id,
                    'location_id' => $locationFactory2->id,
                    'service_id' => $serviceFactory4->id,
                    'quota' => 35,
                    'avail' => 20,
                    'overbooking' => 10,
                    'overbooking_unit' => 'u',
                    'closed' => 0,
                    'unlimited' => 1

                ]
            );




            $fecha_actual_carbon->addDay();

        }


    }


}