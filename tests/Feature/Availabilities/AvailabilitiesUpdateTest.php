<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class AvailabilitiesUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin product_id requerido
     */
    public function testWithOutRequired()
    {
        $this->json('PUT', '/api/v1/availability')
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["avail" => ["The avail field is required."], "product_id" => ["The product id field is required."], "location_id" => ["The location id field is required."]]]]);
    }

    /**
     * Prueba automatizada con formato producto inválido
     */
    public function testWithProductInvalidFormat()
    {
        $this->json('PUT', '/api/v1/availability', [
            'product_id' => 'o',
            'location_id' => 1,
            'avail' => 5,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['product_id' => ['The product id must be a number.']]]]);
    }

    /**
     * Prueba automatizada con producto no existente
     */
    public function testWithProductInvalid()
    {
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => 9999999999999999999999999999,
            'location_id' => $locationFactory->id,
            'avail' => 5,
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Prueba automatizada con location no existente
     */
    public function testWithLocationInvalid()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'location_id' => 100000000000000000000000,
            'avail' => 5,
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Prueba automatizada con servicio invalido
     */
    public function testWithServiceInvalidFormat()
    {
        $this->json('PUT', '/api/v1/availability', [
            'product_id' => 1,
            'location_id' => 1,
            'avail' => 5,
            'service_id' => 'o',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['service_id' => ['The service id must be a number.']]]]);
    }

    /**
     * Prueba automatizada con servico no existente
     */
    public function testWithServiceInvalid()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
            'service_id' => 1000000000000000000000,
            'avail' => 5,
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Prueba automatizada fecha de fin anterior a fecha inicio
     */
    public function testCreateEndBeforeStart()
    {
        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/availability', [
            'date_start' => Carbon::now()->addYears(2)->format('Y-m-d'),
            'date_end' => Carbon::now()->subYears(2)->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'avail' => 5,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ['The date end must be a date after or equal to date start.'],
        ]]]);
    }

    /**
     * Prueba automatizada fecha de inicio anterior a fecha actual
     */
    public function testCreateStartBeforeNow()
    {
        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/availability', [
            'date_start' => Carbon::now()->subYears(3)->format('Y-m-d'),
            'date_end' => Carbon::now()->subYears(2)->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'avail' => 5,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_start' => ['The date start must be a date after or equal to today.'],
        ]]]);
    }

    /**
     * Prueba automatizada date_start formato invalido
     */
    public function testDateStartInvalidFormat()
    {
        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/availability', [
            'date_start' => '15/12/2019',
            'date_end' => '2020/1/25',
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'avail' => 5,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ['The date end does not match the format Y-m-d.', 'The date end must be a date after or equal to date start.'],
            'date_start' => ['The date start does not match the format Y-m-d.', 'The date start is not a valid date.', 'The date start must be a date after or equal to today.'],
        ]]]);
    }

    /**
     * Prueba automatizada date_end formato invalido
     */
    public function testDateEndInvalidFormat()
    {
        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/availability', [
            'date_start' => '2019/1/25',
            'date_end' => '15/12/2019',
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'avail' => 5,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ['The date end does not match the format Y-m-d.', 'The date end is not a valid date.', 'The date end must be a date after or equal to date start.'],
            'date_start' => ['The date start does not match the format Y-m-d.', 'The date start must be a date after or equal to today.'],
        ]]]);
    }

    /**
     * Prueba automatizada session sin date_start ni date_end
     */
    public function testSessionWithOutDates()
    {
        $product = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'session' => '08:00:00',
            'avail' => 5,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ['The date end field is required when date start / session is present.'],
            'date_start' => ['The date start field is required when date end / session is present.'],
        ]]]);
    }

    /**
     * Prueba automatizada update
     */
    public function testUpdate()
    {
        $product = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);


        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
            'unlimited' => 1
        ]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'avail' => 3,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'avail' => 33]);
    }

    /**
     * Prueba automatizada update enviando fechas
     */
    public function testUpdateWithDates()
    {
        $product = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);


        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
            'unlimited' => 1
        ]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->format('Y-m-d'),
            'avail' => 15,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'date' => Carbon::now()->format('Y-m-d'), 'avail' => 45]);
    }

    /**
     *Prueba creando factory y modificando su avail
     */
    public function testUpdateWithSession()
    {
        $product = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '14:00:00',
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->format('Y-m-d'),
            'session' => '14:00:00',
            'avail' => 35,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'date' => Carbon::now()->format('Y-m-d'), 'session' => '14:00:00', 'avail' => 65]);
    }

    /**
     * Prueba automatizada con periodo de tiempo
     */
    public function testTimeFrameWithOutSession()
    {
        $product = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);


        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'avail' => 35,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'date' => Carbon::now()->format('Y-m-d'), 'avail' => 65]);
        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'date' => Carbon::now()->addDay()->format('Y-m-d'), 'avail' => 65]);
        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'date' => Carbon::now()->addDays(2)->format('Y-m-d'), 'avail' => 65]);
    }


    /**
     * Prueba automatizada con periodo de tiempo y session
     */
    public function testTimeFrameAndSession()
    {
        $product = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);


        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'session' => '14:00:00',
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'session' => '14:00:00',
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'session' => '14:00:00',
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $product->id,
            'location_id' => $locationFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'session' => '14:00:00',
            'avail' => 35,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'session' => '14:00:00', 'date' => Carbon::now()->format('Y-m-d'), 'avail' => 65]);
        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'session' => '14:00:00', 'date' => Carbon::now()->addDay()->format('Y-m-d'), 'avail' => 65]);
        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'session' => '14:00:00', 'date' => Carbon::now()->addDays(2)->format('Y-m-d'), 'avail' => 65]);
    }

    /**
     * Prueba automatizada con periodo de tiempo, session y service_id
     */
    public function testTimeFrameSessionAndService()
    {
        $product = factory(App\Models\tenant\Product::class)->create([]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/availability', [
            'product_id' => $product->id,
            'service_id' => $serviceFactory->id,
            'location_id' => $locationFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
            'session' => '14:00:00',
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $serviceFactory->id,
            'location_id' => $locationFactory->id,
            'date' => Carbon::now()->format('Y-m-d'),
            'session' => '14:00:00',
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $serviceFactory->id,
            'location_id' => $locationFactory->id,
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
            'session' => '14:00:00',
            'quota' => 70,
            'avail' => 30,
            'overbooking' => 10,
            'overbooking_unit' => '%',
            'closed' => 0,
        ]);

        $this->json('PUT', '/api/v1/availability', [
            'product_id' => $product->id,
            'service_id' => $serviceFactory->id,
            'location_id' => $locationFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
            'session' => '14:00:00',
            'avail' => 35,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'session' => '14:00:00', 'date' => Carbon::now()->format('Y-m-d'), 'avail' => 65]);
        $this->assertDatabaseHas('mo_availability', ['product_id' => $product->id, 'location_id' => $locationFactory->id, 'session' => '14:00:00', 'date' => Carbon::now()->addDay()->format('Y-m-d'), 'avail' => 65]);
    }
}