<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class AvailabilitiesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba sin enviar parámetro requerido date
     */
    public function testBad1()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $this->json('GET', '/api/v1/availability/'.$product->id)
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["date"=>["The date field is required."]]]]);
    }

    /**
     * Prueba parámetro date en formato incorrecto
     */
    public function testBad2()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $this->json('GET', '/api/v1/availability/'.$product->id, [
            'date' => '2099/12/12',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["date"=>["The date does not match the format Y-m-d."]]]]);
    }

    /**
     * Prueba enviando id inexistente en tabla mo_product
     */
    public function testOk1()
    {
        $fecha = Carbon::now()->addYears(100)->format('Y-m-d');
        $this->json('GET', '/api/v1/availability/999999999999', [
            'date' => $fecha,
        ])
            ->assertExactJson(["error" => 200, "data" => []]);
    }

    /**
     * Prueba automatizada id formato incorrecto
     */
    /*public function testOk2()
    {
        $fecha = Carbon::now()->addYears(100)->format('Y-m-d');
        $this->json('GET', '/api/v1/availability/string', [
            'date' => $fecha,
        ])
            ->assertExactJson(['error' => 200, "data" => []]);
    }*/


    /**
     * Consulta producto con disponibilidad pero sin traducciones, no debe mostrarse
     */
    public function testOk3()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id
        ]);

        factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id
        ]);

        $this->json('GET', '/api/v1/availability/'.$product->id, [
            'date' => '2099-12-12',
        ])
            ->assertExactJson(["error" => 200, "data" => []]);
    }

    /**
     * Consulta producto sin relación en product location, devuelve datos de producto pero no de disponibilidad ni de location
     */
    public function testOk4()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $productTranslation1 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1
        ]);
        $productTranslation2 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2
        ]);
        $productTranslation3 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3
        ]);


        factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
        ]);

        $fecha = Carbon::now()->addYears(100)->format('Y-m-d');

        $this->json('GET', '/api/v1/availability/'.$product->id, [
            'date' => $fecha,
        ])
            ->assertExactJson(["error"=>200,"data"=>[["availability"=>[["product"=>[["id"=>$product->id,"lang"=>[["ES"=>["id"=>$productTranslation1->id,"language_id"=>1,"name"=>$productTranslation1->name]],["EN"=>["id"=>$productTranslation2->id,"language_id"=>2,"name"=>$productTranslation2->name]],["PT"=>["id"=>$productTranslation3->id,"language_id"=>3,"name"=>$productTranslation3->name]]]]]]]]]]);
    }

    /**
     * Consulta disponibilidad para producto con traducciones, relación en product_location y fecha de disponibilidad a null
     */
    public function testOk5()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $productTranslation1 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1
        ]);
        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2
        ]);
        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3
        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id
        ]);

        $avail = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'date' => null
        ]);

        $fecha = Carbon::now()->addYears(100)->format('Y-m-d');

        $this->json('GET', '/api/v1/availability/'.$product->id, [
            'date' => $fecha,
        ])
            ->assertJsonFragment([
                "error" => 200,
                'id' => $product->id,
                'name' => $productTranslation1->name,
                'id' => $location->id,
                'name' => $location->name,
                'id' => $avail->id,
                'date' => null,
                ]);
    }

    /**
     * Consulta disponibilidad para producto con traducciones, relación en product_location
     * y dos registros en disponibilidad: uno con fecha de disponibilidad a null y otro con la fecha enviada, devuelve ambos
     */
    public function testOk6()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $productTranslation1 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1
        ]);
        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2
        ]);
        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3
        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id
        ]);

        $avail1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'date' => null
        ]);

        $fecha = Carbon::now()->addYears(100)->format('Y-m-d');

        $avail2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'date' => $fecha
        ]);

        $this->json('GET', '/api/v1/availability/'.$product->id, [
            'date' => $fecha,
        ])
            ->assertJsonFragment([
                "error" => 200,
                'id' => $product->id,
                'name' => $productTranslation1->name,
                'id' => $location->id,
                'name' => $location->name,
                'id' => $avail1->id,
                'date' => null,
                'id' => $avail2->id,
                'date' => $fecha,

            ]);
    }

    /**
     * Consulta disponibilidad para producto con traducciones, relación en product_location, registro en disponibilidad con date a null
     * y también registro en disponibiidad para servicio
     *
     */
    public function testOk7()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $productTranslation1 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 1
        ]);
        $productTranslation2 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 2
        ]);
        $productTranslation3 = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product->id,
            'language_id' => 3
        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id
        ]);

        $service = factory(\App\Models\tenant\Service::class)->create([]);

        $serviceTranslation1 = factory(\App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 1
        ]);
        $serviceTranslation2 = factory(\App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 2
        ]);
        $serviceTranslation3 = factory(\App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $service->id,
            'language_id' => 3
        ]);

        factory(\App\Models\tenant\ProductService::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
        ]);

        $avail1 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
            'date' => null
        ]);

        $avail2 = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'service_id' => $service->id,
            'location_id' => $location->id,
            'date' => null
        ]);

        $fecha = Carbon::now()->addYears(100)->format('Y-m-d');

        $this->json('GET', '/api/v1/availability/'.$product->id, [
            'date' => $fecha,
        ])
            ->assertJsonFragment([
                "error" => 200,
                'id' => $product->id,
                'name' => $productTranslation1->name,
                'name' => $productTranslation2->name,
                'name' => $productTranslation3->name,
                'id' => $service->id,
                'name' => $serviceTranslation1->name,
                'name' => $serviceTranslation2->name,
                'name' => $serviceTranslation3->name,
                'id' => $location->id,
                'name' => $location->name,
                //disponibilidad del producto
                'id' => $avail1->id,
                //disponibilidad del servicio
                'id' => $avail2->id,
                'date' => null,
            ]);
    }
}