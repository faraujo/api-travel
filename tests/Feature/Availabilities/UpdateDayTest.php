<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UpdateDayTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin envio de parametros
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/availability/day', [

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "date" => ["The date field is required."],
            "locations" => ["The locations field is required."],
            "product_id" => ["The product id field is required."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con formato de parametros erroneo
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => 'TEST',
            'locations' => 'TEST',
            'product_id' => 'TEST',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations' => ['The locations must be an array.'],
            'product_id' => ["The product id must be an integer."],
            'date' => ['The date does not match the format Y-m-d.', 'The date is not a valid date.', 'The date must be a date after or equal to today.']
        ],
        ]]);
    }

    /**
     * Prueba automatizada con parametros requeridos de primer nivel (date,locations y product_id) sin parametros requeridos de segundo ninvel
     */
    public function testBad3()
    {
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha,
            'locations' => [["location_id" => $location_factory->id]],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.avail' => ["The locations.0.avail field is required when none of locations.0.quota / locations.0.closed / locations.0.unlimited / locations.0.availabilities / locations.0.services are present."],
            'locations.0.availabilities' => ["The locations.0.availabilities field is required when none of locations.0.quota / locations.0.avail / locations.0.closed / locations.0.unlimited / locations.0.services are present."],
            'locations.0.closed' => ["The locations.0.closed field is required when none of locations.0.quota / locations.0.avail / locations.0.unlimited / locations.0.availabilities / locations.0.services are present."],
            'locations.0.quota' => ["The locations.0.quota field is required when none of locations.0.avail / locations.0.closed / locations.0.unlimited / locations.0.availabilities / locations.0.services are present."],
            'locations.0.unlimited' => ["The locations.0.unlimited field is required when none of locations.0.quota / locations.0.avail / locations.0.closed / locations.0.availabilities / locations.0.services are present."],
            "locations.0.services"=>["The locations.0.services field is required when none of locations.0.quota / locations.0.avail / locations.0.closed / locations.0.unlimited / locations.0.availabilities are present."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con parametros requeridos de primer y segundo nivel pero con tipo de dato equivocado en segundo nivel
     */
    public function testBad4()
    {
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha,
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 'TEST',
                    "quota" => 'TEST',
                    "unlimited" => 'TEST',
                    "closed" => 'TEST',
                    "availabilities" => 'TEST'
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.avail' => ["The locations.0.avail must be an integer."],
            'locations.0.availabilities' => ["The locations.0.availabilities must be an array."],
            'locations.0.closed' => ["The locations.0.closed field must be true or false."],
            'locations.0.quota' => ["The locations.0.quota must be an integer."],
            'locations.0.unlimited' => ["The locations.0.unlimited field must be true or false."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con parametros de primer y segundo nivel correctos pero sin parametros de tercer nivel
     */
    public function testBad5()
    {
        $fecha = \Carbon\Carbon::now()->format('Y-m-d');
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha,
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.availabilities.0.avail' => ["The locations.0.availabilities.0.avail field is required when none of locations.0.availabilities.0.quota / locations.0.availabilities.0.closed / locations.0.availabilities.0.unlimited are present."],
            'locations.0.availabilities.0.closed' => ["The locations.0.availabilities.0.closed field is required when none of locations.0.availabilities.0.quota / locations.0.availabilities.0.avail / locations.0.availabilities.0.unlimited are present."],
            'locations.0.availabilities.0.quota' => ["The locations.0.availabilities.0.quota field is required when none of locations.0.availabilities.0.avail / locations.0.availabilities.0.closed / locations.0.availabilities.0.unlimited are present."],
            'locations.0.availabilities.0.unlimited' => ["The locations.0.availabilities.0.unlimited field is required when none of locations.0.availabilities.0.quota / locations.0.availabilities.0.avail / locations.0.availabilities.0.closed are present."],
            'locations.0.availabilities.0.session' => ["The locations.0.availabilities.0.session field is required."]
        ],
        ]]);
    }

    /**
     * Prueba automatizada con parametros de primer y segundo nivel correctos pero parametros de tercer nivel formato incorrecto
     */
    public function testBad6()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 'TEST',
                            "quota" => 'TEST',
                            "unlimited" => 'TEST',
                            "closed" => 'TEST',
                            "session" => 'TEST'
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.availabilities.0.avail' => ["The locations.0.availabilities.0.avail must be an integer."],
            'locations.0.availabilities.0.closed' => ["The locations.0.availabilities.0.closed field must be true or false."],
            'locations.0.availabilities.0.quota' => ["The locations.0.availabilities.0.quota must be an integer."],
            'locations.0.availabilities.0.unlimited' => ["The locations.0.availabilities.0.unlimited field must be true or false."],
            'locations.0.availabilities.0.session' => ["The locations.0.availabilities.0.session does not match the format H:i."]
        ],
        ]]);
    }

    /**
     * Prueba automatizada con tipo de dato services erroneo
     */
    public function testBad7()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => 'TEST'
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services' => ["The locations.0.services must be an array."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con tipo de dato services erroneo
     */
    public function testBad8()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [

                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services.0.avail' => ["The locations.0.services.0.avail field is required when none of locations.0.services.0.quota / locations.0.services.0.closed / locations.0.services.0.unlimited / locations.0.services.0.availabilities are present."],
            'locations.0.services.0.availabilities' => ["The locations.0.services.0.availabilities field is required when none of locations.0.services.0.quota / locations.0.services.0.avail / locations.0.services.0.closed / locations.0.services.0.unlimited are present."],
            'locations.0.services.0.closed' => ["The locations.0.services.0.closed field is required when none of locations.0.services.0.quota / locations.0.services.0.avail / locations.0.services.0.unlimited / locations.0.services.0.availabilities are present."],
            'locations.0.services.0.quota' => ["The locations.0.services.0.quota field is required when none of locations.0.services.0.avail / locations.0.services.0.closed / locations.0.services.0.unlimited / locations.0.services.0.availabilities are present."],
            'locations.0.services.0.unlimited' => ["The locations.0.services.0.unlimited field is required when none of locations.0.services.0.quota / locations.0.services.0.avail / locations.0.services.0.closed / locations.0.services.0.availabilities are present."],
            'locations.0.services.0.service_id' => ["The locations.0.services.0.service_id field is required when locations.0.services is present."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada tipo de datos icorrecto del array services
     */
    public function testBad9()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => 'TEST',
                            "avail" => 'TEST',
                            "quota" => 'TEST',
                            "unlimited" => 'TEST',
                            "closed" => 'TEST',
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services.0.avail' => ["The locations.0.services.0.avail must be an integer."],
            'locations.0.services.0.closed' => ["The locations.0.services.0.closed field must be true or false."],
            'locations.0.services.0.quota' => ["The locations.0.services.0.quota must be an integer."],
            'locations.0.services.0.unlimited' => ["The locations.0.services.0.unlimited field must be true or false."],
            'locations.0.services.0.service_id' => ["The locations.0.services.0.service_id must be an integer."],

        ],
        ]]);
    }

    /**
     * Prueba automatizada con array availabilities de servicios sin parametros requeridos
     */
    public function testBad10()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 5,
                            "closed" => 5,
                            "availabilities" => [
                                0 => [

                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "locations.0.services.0.availabilities.0.avail" => ["The locations.0.services.0.availabilities.0.avail field is required when none of locations.0.services.0.availabilities.0.quota / locations.0.services.0.availabilities.0.closed / locations.0.services.0.availabilities.0.unlimited are present."],
            "locations.0.services.0.availabilities.0.closed" => ["The locations.0.services.0.availabilities.0.closed field is required when none of locations.0.services.0.availabilities.0.quota / locations.0.services.0.availabilities.0.avail / locations.0.services.0.availabilities.0.unlimited are present."],
            "locations.0.services.0.availabilities.0.quota" => ["The locations.0.services.0.availabilities.0.quota field is required when none of locations.0.services.0.availabilities.0.avail / locations.0.services.0.availabilities.0.closed / locations.0.services.0.availabilities.0.unlimited are present."],
            "locations.0.services.0.availabilities.0.session" => ["The locations.0.services.0.availabilities.0.session field is required."],
            "locations.0.services.0.availabilities.0.unlimited" => ["The locations.0.services.0.availabilities.0.unlimited field is required when none of locations.0.services.0.availabilities.0.quota / locations.0.services.0.availabilities.0.avail / locations.0.services.0.availabilities.0.closed are present."],
            "locations.0.services.0.closed" => ["The locations.0.services.0.closed field must be true or false."],
            "locations.0.services.0.unlimited" => ["The locations.0.services.0.unlimited field must be true or false."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con array availabilities de servicios con parametros requeridos con tipo de dato incorrecto
     */
    public function testBad11()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 5,
                            "closed" => 5,
                            "availabilities" => [
                                0 => [
                                    "avail" => 'TEST',
                                    "quota" => 'TEST',
                                    "unlimited" => 'TEST',
                                    "closed" => 'TEST',
                                    "session" => 'TEST',
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "locations.0.services.0.availabilities.0.avail" => ["The locations.0.services.0.availabilities.0.avail must be an integer."],
            "locations.0.services.0.availabilities.0.closed" => ["The locations.0.services.0.availabilities.0.closed field must be true or false."],
            "locations.0.services.0.availabilities.0.quota" => ["The locations.0.services.0.availabilities.0.quota must be an integer."],
            "locations.0.services.0.availabilities.0.session" => ["The locations.0.services.0.availabilities.0.session does not match the format H:i."],
            "locations.0.services.0.availabilities.0.unlimited" => ["The locations.0.services.0.availabilities.0.unlimited field must be true or false."],
            "locations.0.services.0.closed" => ["The locations.0.services.0.closed field must be true or false."],
            "locations.0.services.0.unlimited" => ["The locations.0.services.0.unlimited field must be true or false."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada envio de parametro overbooking sin overbooking_unit en cupo general
     */
    public function testBad12()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "overbooking" => 10,

                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "locations.0.overbooking_unit" => ["The locations.0.overbooking_unit field is required when locations.0.overbooking is present."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada envio de parametro overbooking con overbooking_unit icorrecto en cupo general
     */
    public function testBad13()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "overbooking" => 10,
                    "overbooking_unit" => "TEST",

                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "locations.0.overbooking_unit" => ["The selected locations.0.overbooking_unit is invalid."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con array availabilities con parametro overbooking sin overbooking_unit
     */
    public function testBad14()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i'),
                            "overbooking" => 10,
                        ],
                    ],

                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "locations.0.availabilities.0.overbooking_unit" => ["The locations.0.availabilities.0.overbooking_unit field is required when locations.0.availabilities.0.overbooking is present."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con array availabilities con parametro overbooking sin overbooking_unit correcto
     */
    public function testBad15()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i'),
                            "overbooking" => 10,
                            "overbooking_unit" => "TEST",
                        ],
                    ],

                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "locations.0.availabilities.0.overbooking_unit" => ["The selected locations.0.availabilities.0.overbooking_unit is invalid."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con array de servicios con overbooking sin overbooking_unit
     */
    public function testBad16()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 1,
                            "closed" => 1,
                            "overbooking" => 10,
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services.0.overbooking_unit' => ["The locations.0.services.0.overbooking_unit field is required when locations.0.services.0.overbooking is present."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con array de servicios con overbooking correcto y overbooking_unit incorrecto
     */
    public function testBad17()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 1,
                            "closed" => 1,
                            "overbooking" => 10,
                            "overbooking_unit" => "TEST",
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services.0.overbooking_unit' => ["The selected locations.0.services.0.overbooking_unit is invalid."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con array availabilities de servicios con overbooking sin overbooking_unit
     */
    public function testBad18()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "availabilities" => [
                                0 => [
                                    "avail" => 5,
                                    "quota" => 5,
                                    "unlimited" => 0,
                                    "closed" => 0,
                                    "session" => $fecha->format('H:i'),
                                    "overbooking" => 10,
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services.0.availabilities.0.overbooking_unit' => ["The locations.0.services.0.availabilities.0.overbooking_unit field is required when locations.0.services.0.availabilities.0.overbooking is present."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con array availabilities de servicios con overbooking y overbooking_unit incorrecto
     */
    public function testBad19()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "availabilities" => [
                                0 => [
                                    "avail" => 5,
                                    "quota" => 5,
                                    "unlimited" => 0,
                                    "closed" => 0,
                                    "session" => $fecha->format('H:i'),
                                    "overbooking" => 10,
                                    "overbooking_unit" => "TEST",
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'locations.0.services.0.availabilities.0.overbooking_unit' => ["The selected locations.0.services.0.availabilities.0.overbooking_unit is invalid."],
        ],
        ]]);
    }

    /**
     * Prueba automatizada con parametros de cupo general por localizacion
     */
    public function testOk1()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "overbooking" => 10,
                    "overbooking_unit" => "u",

                ]
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'avail' => 5, 'quota' => 5, 'unlimited' => 0, 'closed' => 0, "overbooking" => 10, "overbooking_unit" => "u", 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada con parametros de cupo general y con fecha y session por localizacion
     */
    public function testOk2()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 0,
                            "quota" => 8,
                            "unlimited" => 1,
                            "closed" => 0,
                            "session" => $fecha->format('H:i'),
                            "overbooking" => 10,
                            "overbooking_unit" => "u",
                        ],
                    ],
                ],
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'avail' => 5, 'quota' => 5, 'unlimited' => 0, 'closed' => 0, 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_availability', ['date' => $fecha->format('Y-m-d'), 'session' => $fecha->format('H:i'), 'avail' => 0, 'quota' => 8, 'unlimited' => 1, 'closed' => 0, "overbooking" => 10, "overbooking_unit" => "u", 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'deleted_at' => null]);
    }


    /**
     * Prueba automatizada con parametros de cupo general por localizacion
     */
    public function testOk3()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "availabilities" => [
                        0 => [
                            "avail" => 0,
                            "quota" => 8,
                            "unlimited" => 1,
                            "closed" => 0,
                            "session" => $fecha->format('H:i')
                        ],
                    ],
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "avail" => 5,
                            "quota" => 5,
                            "unlimited" => 1,
                            "closed" => 1,
                            "overbooking" => 10,
                            "overbooking_unit" => "u",
                            "availabilities" => [
                                0 => [
                                    "avail" => 3,
                                    "quota" => 3,
                                    "unlimited" => 1,
                                    "closed" => 1,
                                    "session" => $fecha->format('H:i'),
                                ]
                            ]
                        ]
                    ]
                ],
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'avail' => 5, 'quota' => 5, 'unlimited' => 0, 'closed' => 0, 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_availability', ['date' => $fecha->format('Y-m-d'), 'session' => $fecha->format('H:i'), 'avail' => 0, 'quota' => 8, 'unlimited' => 1, 'closed' => 0, 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'avail' => 5, 'quota' => 5, 'unlimited' => 1, 'closed' => 1, "overbooking" => 10, "overbooking_unit" => "u", 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'service_id' => $service_factory->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_availability', ['date' => $fecha->format('Y-m-d'), 'session' => $fecha->format('H:i'), 'avail' => 3, 'quota' => 3, 'unlimited' => 1, 'closed' => 1, 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'service_id' => $service_factory->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada con parametros de cupo general existiendo uno en base de datos y actualizandolo
     */
    public function testOk4()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\Availability::class)->create([
            'location_id' => $location_factory->id,
            'product_id' => $product_factory->id,
            'avail' => 5,
            'quota' => 5,
            'closed' => 0,
            'unlimited' => 0,
            "overbooking" => 10,
            "overbooking_unit" => "u",
        ]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'avail' => 5, 'quota' => 5, 'closed' => 0, 'unlimited' => 0, 'deleted_at' => null]);

        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 0,
                    "quota" => 0,
                    "unlimited" => 0,
                    "closed" => 1,
                    "overbooking" => 5,
                    "overbooking_unit" => "%",
                ],
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'avail' => 0, 'quota' => 0, 'unlimited' => 0, 'closed' => 1, "overbooking" => 5, "overbooking_unit" => "%",'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada con parametros de cupo por fecha y sesión por localización
     */
    public function testOk5()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha->format('Y-m-d'),
            'session' => $fecha->format('H:i'),
            'location_id' => $location_factory->id,
            'product_id' => $product_factory->id,
            'avail' => 5,
            'quota' => 5,
            'closed' => 0,
            'unlimited' => 0,
            "overbooking" => 10,
            "overbooking_unit" => "u",
        ]);

        $this->assertDatabaseHas('mo_availability', ['date' => $fecha->format('Y-m-d'), 'session' => $fecha->format('H:i'), 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'avail' => 5, 'quota' => 5, 'closed' => 0, "overbooking" => 10, "overbooking_unit" => "u", 'unlimited' => 0, 'deleted_at' => null]);


        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "availabilities" => [
                        0 => [
                            "avail" => 0,
                            "quota" => 3,
                            "unlimited" => 0,
                            "closed" => 0,
                            "session" => $fecha->format('H:i'),
                            "overbooking" => 5,
                            "overbooking_unit" => "%",
                        ],
                    ],
                ],
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', [
            'date' => $fecha->format('Y-m-d'),
            'session' => $fecha->format('H:i') . ':00',
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
            'avail' => 0,
            'quota' => 3,
            'closed' => 0,
            'unlimited' => 0,
            'deleted_at' => null]);
    }


    /**
     * Prueba automatizada con parametros de cupo general por localizacion de producto y servicio
     */
    public function testOk6()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\Availability::class)->create([
            'location_id' => $location_factory->id,
            'product_id' => $product_factory->id,
            'avail' => 3,
            'quota' => 3,
            'closed' => 0,
            'unlimited' => 0,
        ]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'avail' => 3, 'quota' => 3, 'closed' => 0, 'unlimited' => 0, 'deleted_at' => null]);

        factory(\App\Models\tenant\Availability::class)->create([
            'location_id' => $location_factory->id,
            'product_id' => $product_factory->id,
            'avail' => 8,
            'quota' => 8,
            'closed' => 0,
            'unlimited' => 0,
            'service_id' => $service_factory->id,
        ]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'service_id' => $service_factory->id, 'avail' => 8, 'quota' => 8, 'closed' => 0, 'unlimited' => 0, 'deleted_at' => null]);


        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "avail" => 3,
                            "quota" => 3,
                            "unlimited" => 1,
                            "closed" => 1,
                        ]
                    ]
                ],
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'avail' => 5, 'quota' => 5, 'unlimited' => 0, 'closed' => 0, 'location_id' => $location_factory->id, 'product_id' => $product_factory->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'service_id' => $service_factory->id, 'avail' => 3, 'quota' => 3, 'closed' => 1, 'unlimited' => 1, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada con parametros de cupo general por localizacion de producto y cupo de servicio por sesion
     */
    public function testOk7()
    {
        $fecha = \Carbon\Carbon::now();
        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);
        $location_factory = factory(\App\Models\tenant\Location::class)->create([]);
        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);

        factory(\App\Models\tenant\Availability::class)->create([
            'location_id' => $location_factory->id,
            'product_id' => $product_factory->id,
            'avail' => 3,
            'quota' => 3,
            'closed' => 0,
            'unlimited' => 0,
        ]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'avail' => 3, 'quota' => 3, 'closed' => 0, 'unlimited' => 0, 'deleted_at' => null]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha->format('Y-m-d'),
            'session' => $fecha->format('H:i'),
            'location_id' => $location_factory->id,
            'product_id' => $product_factory->id,
            'avail' => 8,
            'quota' => 8,
            'closed' => 0,
            'unlimited' => 0,
            'service_id' => $service_factory->id,
        ]);

        $this->assertDatabaseHas('mo_availability', ['date' => $fecha->format('Y-m-d'), 'session' => $fecha->format('H:i'), 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'service_id' => $service_factory->id, 'avail' => 8, 'quota' => 8, 'closed' => 0, 'unlimited' => 0, 'deleted_at' => null]);

        $this->json('PUT', '/api/v1/availability/day', [
            'date' => $fecha->format('Y-m-d'),
            'locations' => [
                [
                    "location_id" => $location_factory->id,
                    "avail" => 5,
                    "quota" => 5,
                    "unlimited" => 0,
                    "closed" => 0,
                    "services" => [
                        0 => [
                            "service_id" => $service_factory->id,
                            "availabilities" => [
                                0 => [
                                    "avail" => 3,
                                    "quota" => 3,
                                    "unlimited" => 1,
                                    "closed" => 1,
                                    "session" => $fecha->format('H:i'),
                                ]
                            ]
                        ]
                    ]
                ],
            ],
            'product_id' => $product_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_availability', ['date' => null, 'session' => null, 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'avail' => 5, 'quota' => 5, 'closed' => 0, 'unlimited' => 0, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_availability', ['date' => $fecha->format('Y-m-d'), 'session' => $fecha->format('H:i'), 'product_id' => $product_factory->id, 'location_id' => $location_factory->id, 'avail' => 3, 'quota' => 3, 'closed' => 1, 'unlimited' => 1, 'deleted_at' => null]);

    }
}