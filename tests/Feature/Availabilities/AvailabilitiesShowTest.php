<?php


use Carbon\Carbon;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class AvailabilitiesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada envio de parametros en formato incorrecto
     */
    public function testBad1(){
        $this->json('GET','/api/v1/availability',[
            'limit' => 'test',
            'page' => 'test',
            'location_id' => 'test',
            'type_id' => 'test,'
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'limit' => ['The limit must be a number.'],
            'location_id' => ['The location id must be an integer.'],
            "month"=>["The month field is required."],
            "page"=>["The page must be a number."],
            "type_id"=>["The type id must be an integer."],
            "year"=>["The year field is required."],
        ]]]);
    }


    /**
     * Prueba automatizada que comprueba la existencia de un producto con una disponibilidad general
     */
    public function testOk1(){
        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => (int) Carbon::now()->format('m'),
            'year' => (int) Carbon::now()->format('Y'),
        ])->assertJsonFragment(['id' => $product_factory->id]);
    }

    /**
     * Prueba automatizada que comprueba la existencia de una disponibilidad existente en fecha y hora
     */
    public function testOk2(){
        $fecha = Carbon::now()->addMonth()->format('Y-m-d');
        $mes = (int) Carbon::now()->addMonth()->format('m');
        $anio = (int) Carbon::now()->addMonth()->format('Y');


        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha,
            'session' => '00:00:00',
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => $mes,
            'year' => $anio,
        ])->assertJsonFragment(["avail"=>1,"closed"=>0,"date"=>$fecha,"unlimited"=>0]);
    }

    /**
     * Prueba automatizada que comprueba filtro location_id
     */
    public function testOk3(){
        $mes = (int) Carbon::now()->addMonth()->format('m');
        $anio = (int) Carbon::now()->addMonth()->format('Y');


        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => $mes,
            'year' => $anio,
            'location_id' => $location_factory->id
        ])->assertJsonFragment(["avail"=>1,"closed"=>0,"unlimited"=>0]);
    }

    /**
     * Prueba automatizada que comprueba filtro location_id sin asociar a ninguna disponibilidad
     */
    public function testOk4(){
        $mes = (int) Carbon::now()->addMonth()->format('m');
        $anio = (int) Carbon::now()->addMonth()->format('Y');


        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $location_factory_2 = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test_2',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory_2->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => $mes,
            'year' => $anio,
            'location_id' => $location_factory_2->id
        ])->assertJsonFragment(["avail"=>null,"closed"=>null,"unlimited"=>null]);
    }

    /**
     * Prueba automatizada comprueba el filtro name
     */
    public function testOk5(){
        $mes = (int) Carbon::now()->addMonth()->format('m');
        $anio = (int) Carbon::now()->addMonth()->format('Y');


        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $location_factory_2 = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test_2',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        $product_translation_factory = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory_2->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => $mes,
            'year' => $anio,
            'name' => substr($product_translation_factory->name, 0, 5)
        ])->assertJsonFragment(["id"=>$product_factory->id]);
    }


    /**
     * Prueba automatizada comprueba el filtro type_id
     */
    public function testOk6(){
        $mes = (int) Carbon::now()->addMonth()->format('m');
        $anio = (int) Carbon::now()->addMonth()->format('Y');


        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $location_factory_2 = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test_2',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        $product_translation_factory = factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory_2->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => $mes,
            'year' => $anio,
            'type_id' => $product_type_factory->id
        ])->assertJsonFragment(["id"=>$product_factory->id]);
    }


    /**
     * Prueba automatizada comprueba la suma correcta de avail cuando existen cupos con cerrado ha 1 con cupo general
     */

    public function testOk7(){
        $mes = (int) Carbon::now()->addMonth()->format('m');
        $anio = (int) Carbon::now()->addMonth()->format('Y');


        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 1,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => $mes,
            'year' => $anio,
        ])->assertJsonFragment(["avail"=>1,"closed"=>0,"unlimited"=>0]);

    }


    /**
     * Prueba automatizada comprueba suma de avail con closed a 1 con fecha y hora
     */
    public function testOk8(){
        $mes = (int) Carbon::now()->addMonth()->format('m');
        $anio = (int) Carbon::now()->addMonth()->format('Y');
        $fecha = Carbon::now()->addMonth()->format('Y-m-d');

        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        $language_factory = factory(\App\Models\tenant\Language::class)->create([]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => $language_factory->id
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha,
            'session' => '00:00:00',
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha,
            'session' => '01:00:00',
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 1,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability', [
            'month' => $mes,
            'year' => $anio,
        ])->assertJsonFragment(["date"=>$fecha, "avail"=>1,"closed"=>0,"unlimited"=>0]);

    }

}