<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Carbon\Carbon;

class AvailabilitiesDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    //test validator

    /**
     * test para comprobar en el validador campos requeridos
     */
    public function required()
    {
        $this->json('DELETE', '/api/v1/availability')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format',
                'error_inputs' => [['product_id' => 'The product id field is required when service id is not present.',
                    'service_id' => 'The service id field is required when product id is not present.']]]);
    }

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testValidatorProductId()
    {
        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => 'r'
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["product_id" => ["The product id must be a number."]]]]);
    }

    /**
     * test para comprobar en el validador service_id sin product_id
     */
    public function testValidatorServiceIdWithOutProductId()
    {
        $this->json('DELETE', '/api/v1/availability', [
            'service_id' => 'r'
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["product_id" => ["The product id field is required."], "service_id" => ["The service id must be a number."]]]]);
    }

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testValidatorServiceId()
    {
        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => 'r'
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["product_id" => ["The product id must be a number."]]]]);
    }


    /**
     * Eliminar una disponibilidad con id inexistente en tabla mo_product
     */
    public function testProductIdNotExist()
    {
        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => 5000000000000,
        ])
            ->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Eliminar una disponibilidad con id inexistente en tabla mo_service
     */
    public function testServiceIdNotExist()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'service_id' => 1000000000000000000000,
        ])
            ->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Eliminar una disponibilidad con campo date inválido
     */
    public function testDateNoRight()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'date_start' => 'dfdf',
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'date_end' => ['The date end must be a date after or equal to date start.'],
                'date_start' => ['The date start does not match the format Y-m-d.', 'The date start is not a valid date.']]
            ]]);
    }

    /**
     * Eliminar una disponibilidad con sólo un campo date
     */
    public function testDateIncomplete()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);
        //si envían fecha deben enviar fecha inicio y fecha fin
        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                //'date_end' => ['The date end must be a date after or equal to date start.'],
                'date_start' => ['The date start field is required when date end / session is present.']]
            ]]);
    }

    /**
     * Eliminar una disponibilidad con campo session inválido, el formato session debe ser 00:00:00
     */
    public function testSessionNoRight()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->format('Y-m-d'),
            'session' => '8:89'
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['session' => ['The session does not match the format H:i.']]]]);
    }

    /**
     * Eliminar una disponibilidad con campo session correcto pero sin fechas, no debe permitir meter una session sin date_start y date_end
     */
    public function testSessionWithNoDates()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);
        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'session' => '08:00'
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [["date_end" => ["The date end field is required when date start / session is present."], "date_start" => ["The date start field is required when date end / session is present."]]]]);
    }

    /**
     * Eliminar una disponibilidad con campo session correcto pero sin una de las dos fechas necesarias, no debe permitir meter una session sin date_start o date_end
     */
    public function testSessionWithNoDateStart()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $productFactory->id,
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'session' => '08:00',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                //'date_end' => ['The date end must be a date after or equal to date start.'],
                "date_start" => ["The date start field is required when date end / session is present."]]
            ]]);
    }

    //FIN TEST VALIDATOR


    /**
     * Test para eliminar un registro de disponibilidad que crea primero un availability factory mandando product_id
     */
    public function testDeleteProductId()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);
        $availabilityFactory = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'location_id' => $location->id,
        ]);

        $this->assertDatabaseHas('mo_availability', [
            'id' => $availabilityFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $availabilityFactory->product_id,
            'location_id' => $availabilityFactory->location_id,
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_availability', ['deleted_at' => null, 'id' => $availabilityFactory->id]);

    }

    /**
     * Test para eliminar un registro de disponibilidad que crea primero un availability factory mandando service_id
     */
    public function testDeleteServiceId()
    {

        $productfactory = factory(App\Models\tenant\Product::class)->create([]);
        $servicefactory = factory(App\Models\tenant\Service::class)->create([]);

        $availabilityFactory = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $productfactory->id,
            'service_id' => $servicefactory->id,
        ]);

        $this->assertDatabaseHas('mo_availability', [
            'id' => $availabilityFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $productfactory->id,
            'service_id' => $servicefactory->id,
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_availability', ['deleted_at' => null, 'id' => $availabilityFactory->id]);

    }

    /**
     * Test para eliminar un registro de disponibilidad que crea primero un availability factory mandando product_id
     */
    public function testDeleteProductIdWithDates()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $availabilityFactory = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $this->assertDatabaseHas('mo_availability', [
            'id' => $availabilityFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $availabilityFactory->product_id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_availability', ['deleted_at' => null, 'id' => $availabilityFactory->id]);

    }

    /**
     * Test para eliminar un registro de disponibilidad que crea primero un availability factory mandando product_id, dates and session
     */
    public function testDeleteProductIdWithDatesAndSession()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $availabilityFactory = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $product->id,
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
            'session' => '08:00:00',
        ]);

        $this->assertDatabaseHas('mo_availability', [
            'id' => $availabilityFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $availabilityFactory->product_id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'session' => '08:00',
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_availability', ['deleted_at' => null, 'id' => $availabilityFactory->id]);

    }


    /**
     * Test para eliminar un registro de disponibilidad que crea primero un availability factory mandando product_id, service_id dates and session
     */
    public function testDeleteAvailabilityWithProductAndService()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([

        ]);
        $availabilityFactory = factory(App\Models\tenant\Availability::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
            'date' => Carbon::now()->addDay()->format('Y-m-d'),
            'session' => '08:00:00',
        ]);

        $this->assertDatabaseHas('mo_availability', [
            'id' => $availabilityFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/availability', [
            'product_id' => $availabilityFactory->product_id,
            'service_id' => $serviceFactory->id,
            'date_start' => Carbon::now()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(3)->format('Y-m-d'),
            'session' => '08:00',
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_availability', ['deleted_at' => null, 'id' => $availabilityFactory->id]);

    }

}