<?php


use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ShowCalendarTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada envio de parametros en formato incorrecto
     */
    public function testBad1(){
        $this->json('GET','/api/v1/availability/calendar',[
            'date_start' => 'test',
            'date_end' => 'test',
            'products' => 'test',
            'lang' => 'test'
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'date_end' => ["The date end does not match the format Y-m-d.","The date end is not a valid date.","The date end must be a date after or equal to date start."],
            'date_start' => ["The date start does not match the format Y-m-d.","The date start is not a valid date."],
            "products"=>["The products must be an array."],
            "lang"=>["The lang is not exists"],
        ]]]);
    }


    /**
     * Prueba automatizada que comprueba la existencia de un producto con una disponibilidad general
     */
    public function testOk1(){
        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => 1
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'location_id' => $location_factory->id
        ]);

        $this->json('GET','/api/v1/availability/calendar', [
            'products' => [
                '0' => $product_factory->id
            ],
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ])->assertJsonFragment(['id' => $product_factory->id]);
    }

    /**
     * Prueba automatizada que comprueba la existencia de una disponibilidad existente en fecha y hora
     */
    public function testOk2(){
        $fecha = Carbon::now()->format('Y-m-d');

        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => 1
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha,
            'session' => '00:00:00',
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id
        ]);

        $this->json('GET','/api/v1/availability/calendar', [
            'products' => [
                '0' => $product_factory->id
            ],
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ])->assertJsonFragment(["avail"=>1,"closed"=>0,"date"=>$fecha,"unlimited"=>0]);
    }

    /**
     * Prueba automatizada comprueba la suma correcta de avail cuando existen cupos con cerrado ha 1 con cupo general
     */

    public function testOk7(){

        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => 1
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 1,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability/calendar', [
            'products' => [
                '0' => $product_factory->id
            ],
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ])->assertJsonFragment(["avail"=>1,"closed"=>0,"unlimited"=>0]);

    }


    /**
     * Prueba automatizada comprueba suma de avail con closed a 1 con fecha y hora
     */
    public function testOk8(){
        //$mes = (int) Carbon::now()->addMonth()->format('m');
        //$anio = (int) Carbon::now()->addMonth()->format('Y');
        $fecha = Carbon::now()->format('Y-m-d');

        $location_factory = factory(\App\Models\tenant\Location::class)->create([
            'name' => 'test',
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => $product_type_factory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $product_factory->id,
            'language_id' => 1
        ]);

        factory(\App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $product_factory->id,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha,
            'session' => '00:00:00',
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 0,
            'location_id' => $location_factory->id,
        ]);

        factory(\App\Models\tenant\Availability::class)->create([
            'date' => $fecha,
            'session' => '01:00:00',
            'product_id' => $product_factory->id,
            'quota' => 3,
            'avail' => 1,
            'unlimited' => 0,
            'closed' => 1,
            'location_id' => $location_factory->id,
        ]);

        $this->json('GET','/api/v1/availability/calendar', [
            'products' => [
                '0' => $product_factory->id
            ],
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ])->assertJsonFragment(["date"=>$fecha, "avail"=>1,"closed"=>0,"unlimited"=>0]);

    }

}