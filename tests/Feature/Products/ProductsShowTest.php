<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ProductsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta productos con parametro lang
     */
    public function testLang()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/product', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta productos con parametro lang en mayusculas
     */
    public function testLangUpper()
    {
        //Se especifica parametro lang correctamente en mayusculas
        $this->json('GET', '/api/v1/product', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta productos con parametro lang mal
     */
    public function testLangBad()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/product', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta productos con parametro tipo incorrecto
     */
    public function testTypeBad()
    {
        // Se especifica incorrectamente parametro type
        $this->json('GET', '/api/v1/product', [
            'type_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta productos con parametro tipo incorrecto
     */
    public function testLocationBad()
    {
        // Se especifica incorrectamente parametro location_id
        $this->json('GET', '/api/v1/product', [
            'location_id' => 'r'
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["location_id"=>["The location id must be an integer."]]]]);
    }

    /**
     * Test consulta productos con parametro tipo inexistente
     */
    public function testTypeNotExists()
    {
        //Se especifica un type inexistente
        $this->json('GET', '/api/v1/product', [
            'lang' => 'es',
            'name' => 'ar',
            'type_id' => '999999999999999',
        ])->assertExactJson(['data' => [], 'error' => 200, "total_results" => 0,]);
    }


    /**
     * Test consulta productos con algunos de los parametros mal
     */
    public function testWithSomeParametersBad()
    {
        // Se especifican incorrectamente parametros
        $this->json('GET', '/api/v1/product', [
            'lang' => 'es',
            'code' => 'ar',
            'type_id' => 'esas',
            'name' => 'cualquiera',
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta productos con parámetros incorrectos
     */
    public function testWithBadParameters()
    {
        //Se especifican incorretamente alguno de los parametros

        $this->json('GET', '/api/v1/product', [
            'lang' => 'es',
            'name' => 'ven',
            'type_id' => 'afaf',
        ])->assertJsonStructure(['error', 'error_description']);
    }

    /**
     * Test consulta productos con algunos parametros mal
     */
    public function testWithAllParametersSomeBad()
    {
        // Se especifican incorrectamente algunos parametros

        $this->json('GET', '/api/v1/product', [
            'lang' => 'es',
            'type_id' => 'h',
            'code' => 'a',
            'name' => 'cualquiera',
            'start' => 'm',
            'limit' => 5
        ])->assertJsonStructure(['error', 'error_description']);
    }

    /**
     *  Test consulta de productos sin parametros
     */
    public function testReadProducts()
    {
        // No se especifica parametros
        $this->json('GET', '/api/v1/product')->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     *  Test consulta de productos comprobar categoría
     */
    public function testReadProductsCategory()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $factory = factory(App\Models\tenant\Category::class)->create([
            'order' => 4,
        ]);
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        factory(\App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductCategory::class)->create([
            'category_id' => $factory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/product',['category_id' => $factory->id,])->assertJsonFragment(['total_results'=>1]);

        $factory->delete();

        $this->json('GET', '/api/v1/product',['category_id' => $factory->id,])->assertJsonFragment(['total_results'=>0]);
    }

    /**
     * Test consulta productos con casi todos los parametros
     */
    public function testWithParameters()
    {

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        // Se especifican correctamente parametros
        $this->json('GET', '/api/v1/product', [
            'lang' => 'es',
            'type' => $typeFactory->id,
            'code' => 'a',
            'name' => 'cualquiera',
        ])->assertJsonStructure(['error', 'data']);
    }

    /**
     * Test consulta productos con parametro location inexistente
     */
    public function testLocationNotExists()
    {
        //Se especifica un location inexistente
        $this->json('GET', '/api/v1/product', [
            'location_id' => '999999999999999',
        ])->assertExactJson(["data"=>[],"error"=>200,"total_results"=>0]);
    }

    /**
     * Test consulta productos con todos los parametros
     */
    public function testWithAllParameters()
    {
        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        // Se especifican correctamente parametros
        $this->json('GET', '/api/v1/product', [
            'lang' => 'es',
            'type_id' => $typeFactory->id,
            'code' => 'a',
            'name' => 'cualquiera',
            'start' => 2,
            'limit' => 5,
            'location_id' => $locationFactory->id,

        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

    /**
     * Test consulta productos con relación en product_location y se devuelve registro. Se comprueba que devuelve campos uom_id, pax, cost_center y accounting_account
     */
    public function testProductLocation()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'company_id' => $companyFactory->id,
            'type_id' => $typeFactory->id,
            'code' => 'code test location',
            'uom_id' => $uomFactory->id,
            'pax' => 1,
            'cost_center' => 'cost center',
            'accounting_account' => 'accounting account',
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        // Se filtra por tipo de producto creado por factory para controlar que sólo devuelve registros creados por factory
        $this->json('GET', '/api/v1/product', [
            'type_id' => $typeFactory->id,
        ])->assertJsonFragment(['uom_id' => $uomFactory->id,'pax' => 1, 'cost_center' => 'cost center', 'accounting_account' => 'accounting account','total_results' => 1]);
    }


    /**
     * Test consulta productos con relación en product_location y se devuelve registro.
     *
     */
    public function testProductLocation2()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
            'name' => 'test location 2'
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
           'code' => 'product test location 1',
            ]);


        $productFactory2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'product test location 2',
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory2->id,
            'language_id' => 1,
        ]);


        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $productLocationFactory2 = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory2->id,
            'location_id' => $locationFactory->id,
        ]);

        // Se filtra por location  producto creado por factory para controlar que sólo devuelve registros creados por factory
        $this->json('GET', '/api/v1/product', [
            'location_id' => $locationFactory->id,
        ])->assertJsonFragment(['total_results' => 2]);
    }



    /**
     * Test consulta productos sin relación en product_location y no devuelve registro
     */
    public function testProductLocation3()
    {

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'company_id' => $companyFactory->id,
            'type_id' => $typeFactory->id,
            'code' => 'code test location',
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'language_id' => 1,
        ]);

        // Se filtra por tipo de producto creado por factory para controlar que sólo devuelve registros creados por factory
        //no devuelve registros porque el producto con ese type_id no está asociado a un location_id
        $this->json('GET', '/api/v1/product', [
            'type_id' => $typeFactory->id,
        ])->assertJsonFragment(['total_results' => 0]);
    }
}