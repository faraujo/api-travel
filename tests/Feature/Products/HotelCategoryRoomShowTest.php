<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class HotelCategoryRoomShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de categorías de habitaciones con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/product/room/categories', [
            'page' => 'string',
            'limit' => 'string',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."]]]]);
    }

    /**
     * test consulta de categorías de habitaciones con parametro lang incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/product/room/categories', [
            'lang' => 'eeeeeeee',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["lang" => ["The lang is not exists"]]]]);
    }


    /**
     * test crea factory de categorías de habitaciones con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de categorías de habitaciones y sus traducciones
        $categoryFactory1 = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $this->assertDatabaseHas('mo_hotel_room_category', [
            'id' => $categoryFactory1->id,
        ]);

        $categoryTranslationFactory1 = factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 1,
            'category_id' => $categoryFactory1->id,
            'name' => 'name español test',
        ]);

        $categoryTranslationFactory2 = factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 2,
            'category_id' => $categoryFactory1->id,
            'name' => 'name ingles test',
        ]);

        $categoryTranslationFactory3 = factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 3,
            'category_id' => $categoryFactory1->id,
            'name' => 'name portugues test',
        ]);

        $this->assertDatabaseHas('mo_hotel_room_category_translation', [
            'language_id' => $categoryTranslationFactory1->language_id,
            'category_id' => $categoryFactory1->id,
            'name' => $categoryTranslationFactory1->name,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_category_translation', [
            'language_id' => $categoryTranslationFactory2->language_id,
            'category_id' => $categoryFactory1->id,
            'name' => $categoryTranslationFactory2->name,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_category_translation', [
            'language_id' => $categoryTranslationFactory3->language_id,
            'category_id' => $categoryFactory1->id,
            'name' => $categoryTranslationFactory3->name,
        ]);

        $this->json('GET', '/api/v1/product/room/categories', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $categoryFactory1->id,
                'name' => $categoryTranslationFactory1->name,
                'name' => $categoryTranslationFactory2->name,
                'name' => $categoryTranslationFactory3->name,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/product/room/categories', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['category' => ["0" => ['id', 'lang']]]]
            ]);
    }

}