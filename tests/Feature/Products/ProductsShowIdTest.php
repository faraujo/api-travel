<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class ProductsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta producto inexistente sin parametros
     */
    public function testProductBad()
    {
        $this->json('GET', '/api/v1/product/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta producto existente sin parametros
     */
    public function testProductRead()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
            'billable' => 0,
            'pax' => 1,
            'cost_center' => 'Centro de Costo 10',
            'accounting_account' => '323',
            'uom_id' => $uomFactory->id,
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $this->json('GET', '/api/v1/product/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'type_id' => $productFactory->type_id,
                'uom_id' => $uomFactory->id,
                'name' => $translation1->name,
                'billable' => $productFactory->billable,
                'address' => $productFactory->address,
                'pax' => $productFactory->pax,
                'cost_center' => $productFactory -> cost_center,
                'accounting_account' => $productFactory -> accounting_account
            ]);
    }

    /**
     * Consulta producto existente con tipos de cliente asignados
     */
    public function testOk1()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);
        //Se crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
            'billable' => 0,
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $this->json('GET', '/api/v1/product/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'type_id' => $productFactory->type_id,
                'name' => $translation1->name,
                'billable' => $productFactory->billable,
                'address' => $productFactory->address,
            ])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['product' => ["0" => ['id', 'lang', 'client_types' => 'id']]]]
            ]);
    }

    /**
     * Consulta producto existente con categorías asignadas
     */
    public function testOk2()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);
        //Se crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
            'billable' => 0,
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'discover_url' => 'descubre parque español'
        ]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        //Se crean factories de categorías
        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);
        $categoryFactory2 = factory(App\Models\tenant\Category::class)->create([]);
        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);
        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory2->id,
        ]);


        //Se crean factories para asociar las categorías al producto
        $productCategoryFactory = factory(App\Models\tenant\ProductCategory::class)->create([
            'product_id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
        ]);
        $productCategoryFactory2 = factory(App\Models\tenant\ProductCategory::class)->create([
            'product_id' => $productFactory->id,
            'category_id' => $categoryFactory2->id,
        ]);

        $this->json('GET', '/api/v1/product/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'type_id' => $productFactory->type_id,
                'name' => $translation1->name,
                'discover_url' => $translation1->discover_url,
                'billable' => $productFactory->billable,
                'id' => $categoryFactory->id,
                'id' => $categoryFactory2->id,
            ])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['product' => ["0" => ['id', 'lang', 'categories' => 'id']]]]
            ]);
    }

    /**
     * Consulta producto existente con tags asignadas y comprueba que en salida viene id de archivo
     */
    public function testOk3()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);
        //Se crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
            'billable' => 0,
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        //Se crean factories de tags
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([]);
        $tagFactory2 = factory(App\Models\tenant\Tag::class)->create([]);
        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);
        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory2->id,
        ]);


        //Se crean factories para asociar las etiquetas al producto
        $productTagFactory = factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $productFactory->id,
            'tag_id' => $tagFactory->id,
        ]);
        $productTagFactory2 = factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $productFactory->id,
            'tag_id' => $tagFactory2->id,
        ]);

        //se añade factories de file y traaduccion para comprobar que se muestra en salida
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $productFileFactory = factory(App\Models\tenant\ProductFile::class)->create([
            'file_id' => $fileFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/product/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'type_id' => $productFactory->type_id,
                'name' => $translation1->name,
                'billable' => $productFactory->billable,
                'id' => $tagFactory->id,
                'id' => $tagFactory2->id,
                'id' => $fileFactory->id,
                'id' => $fileFactory->type_id,
            ])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['product' => ["0" => ['id', 'files' => 'id','uom_id','lang', 'tags' => 'id']]]]
            ]);
    }

    /**
     * Comprueba que al borrar traducción de archivo no se muestra
     */
    public function testOk4()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);
        //Se crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
            'billable' => 0,
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        //se añade factories de file
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileTranslationFactory = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $productFileFactory = factory(App\Models\tenant\ProductFile::class)->create([
            'file_id' => $fileFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/product/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'type_id' => $productFactory->type_id,
                'name' => $translation1->name,
                'billable' => $productFactory->billable,
            ])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['product' => ["0" => ['id', 'files' => [],'uom_id','lang', 'tags' => 'id']]]]
            ]);
    }

    /**
     * Consulta producto existente con servicios asignados y comprueba devolución y estructura con location
     */
    public function testOk5()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([
        ]);
        //Se crea factory de producto
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
            'billable' => 0,
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory2->id,
        ]);

        //Se crean factories de servicios
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory2->id,
        ]);

        //Se crean factories para asociar los servicios al producto
        $productServiceFactory = factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);
        $productServiceFactory2 = factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory2->id,
        ]);

        $this->json('GET', '/api/v1/product/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'type_id' => $productFactory->type_id,
                'name' => $translation1->name,
                'billable' => $productFactory->billable,
                'address' => $productFactory->address,
                'id' => $serviceFactory->id,
                'id' => $serviceFactory2->id,
                'id' => $locationFactory->id,
                'id' => $locationFactory2->id,
            ])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['product' => ["0" => ['id', 'lang', 'services' => 'id','location' => 'id']]]]
            ]);
    }
}