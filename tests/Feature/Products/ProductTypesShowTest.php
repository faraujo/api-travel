<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ProductTypesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de tipos de producto con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/product/types/', [
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."]]]]);
    }

    /**
     * test consulta de tipos de producto con parametro lang incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/product/types/', [

            'lang' => 'eeeeeeee',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["lang" => ["The lang is not exists"]]]]);
    }


    /**
     * test crea factory de tipos de producto con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de tipo de producto y sus traducciones
        $typeFactory1 = factory(App\Models\tenant\Type::class)->create([]);

        $this->assertDatabaseHas('mo_product_type', [
            'id' => $typeFactory1->id,
        ]);

        $typeTranslationFactory1 = factory(App\Models\tenant\TypeTranslation::class)->create([
            'language_id' => 1,
            'type_id' => $typeFactory1->id,
            'name' => 'name español test ',

        ]);

        $typeTranslationFactory2 = factory(App\Models\tenant\TypeTranslation::class)->create([
            'language_id' => 2,
            'type_id' => $typeFactory1->id,
            'name' => 'name ingles test ',

        ]);

        $typeTranslationFactory3 = factory(App\Models\tenant\TypeTranslation::class)->create([
            'language_id' => 3,
            'type_id' => $typeFactory1->id,
            'name' => 'name portugues test ',

        ]);

        $this->assertDatabaseHas('mo_product_type_translation', [
            'language_id' => $typeTranslationFactory1->language_id,
            'type_id' => $typeFactory1->id,
            'name' => $typeTranslationFactory1->name,

        ]);

        $this->assertDatabaseHas('mo_product_type_translation', [
            'language_id' => $typeTranslationFactory2->language_id,
            'type_id' => $typeFactory1->id,
            'name' => $typeTranslationFactory2->name,

        ]);

        $this->assertDatabaseHas('mo_product_type_translation', [
            'language_id' => $typeTranslationFactory3->language_id,
            'type_id' => $typeFactory1->id,
            'name' => $typeTranslationFactory3->name,

        ]);


        $this->json('GET', '/api/v1/product/types/', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $typeFactory1->id,
                'name' => $typeTranslationFactory1->name,
                'name' => $typeTranslationFactory2->name,
                'name' => $typeTranslationFactory3->name,


            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/product/types/', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['type' => ["0" => ['id', 'lang']]]]
            ]);

    }


}