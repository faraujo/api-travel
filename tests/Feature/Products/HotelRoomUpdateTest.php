<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class HotelRoomUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se manda el campo files en formato incorrecto
     */
    public function testBad1()
    {

        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('PUT', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'SGL',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => 'r']])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "files.0" => ["The files.0 must be an integer."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se envían files inexistentes
     */
    public function testBad2()
    {
        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);


        $this->json('PUT', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'SGL',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => 99999999],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "files.0" => ["The selected files.0 is invalid."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el campo files sin formato array.
     */
    public function testBad3()
    {
        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'code' => 'pruebaFactory3',
        ]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('PUT', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'SGL',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'files' => '2',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "files" => ["The files must be an array."],
            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el campo files con valor duplicado.
     */
    public function testBad4()
    {
        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([

        ]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $this->assertDatabaseHas('mo_hotel_room_category', [
            'id' => $categoryFactory->id,
        ]);
        $fileFactory = factory(App\Models\tenant\File::class)->create([]);
        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('PUT', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'SGL',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'files' => [0 => $fileFactory->id, 1 => $fileFactory->id],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "files.0" => ["The files.0 field has a duplicate value."],
                "files.1" => ["The files.1 field has a duplicate value."],
            ]]]);
    }

    /**
     * test para comprobar el validador de campos obligatorios
     */
    public function testBad5()
    {
        $this->json('put', '/api/v1/product/room', [
            'id' => 1,
            'code' => 'SGL',
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" =>
                [[
                    'category_id' => ['The category id field is required.'],
                    'uom_id' => ['The uom id field is required.'],
                    'pax' => ['The pax field is required.'],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto sin traduccion
     */
    public function testBad6()
    {

        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room', [
            'id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_translation', [
            'room_id' => $productFactory->id,
        ]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);


        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('PUT', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'code' => 'SGL',])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     * Test para modificar un room
     *
     * A partir de los datos del room que quiere modificar modifica el usuario los campos que desee
     * Comprueba que los datos se actualizan correctamente en base de datos.
     *
     */
    public function testOk1()
    {


        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([

        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $productFactory->id,
        ]);


        $this->assertDatabaseHas('mo_hotel_room', [
            'id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_translation', [
            'room_id' => $productFactory->id,
        ]);


        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $this->json('put', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'SGL',
            'pax' => 0,
            'name' => ['ES' => 'nombre CON ÑOO ñoo asáaá modificado', 'EN' => 'name update Inglés', 'PT' => 'name update portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 UUmodificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'services_included' => ['ES' => 'services_included modificada español', 'EN' => 'services_included modificada inglés', 'PT' => 'services_included modificada portugués'],
            'services_not_included' => ['ES' => 'services_not_included modificada español', 'EN' => 'services_not_included modificada inglés', 'PT' => 'services_not_included modificada portugués'],

            'index' => ['ES' => 0, 'EN' => 1, 'PT' => 1],
            'breadcrumb' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'rel' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_image' => ['ES' =>$fileFactory->id],
            'twitter_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_image' => ['ES' =>$fileFactory->id],
            'canonical_url' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_head' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_body' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_footer' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],

            'order' => 10,
            'longitude' => -225.3333,
            'latitude' => 152.0000025,
            'address' => 'address product',
            'legal' => ['ES' => 'legal en español', 'EN' => 'legal en inglés', 'PT' => 'legal en portugués'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_hotel_room', [
            'id' => $productFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'SGL',
            'order' => 10,
            'longitude' => -225.3333,
            'latitude' => 152.0000025,
            'address' => 'address product',
            'pax' => 0,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_translation', [
            'room_id' => $productFactory->id,
            'language_id' => 1,
            'services_included' => 'services_included modificada español',
            'services_not_included' => 'services_not_included modificada español',
            'legal' => 'legal en español'
        ]);

        $this->assertDatabaseHas('mo_hotel_room_translation', [
            'room_id' => $productFactory->id,
            'language_id' => 2,
            'services_included' => 'services_included modificada inglés',
            'services_not_included' => 'services_not_included modificada inglés',
            'legal' => 'legal en inglés'
        ]);

        $this->assertDatabaseHas('mo_hotel_room_translation', [
            'room_id' => $productFactory->id,
            'language_id' => 3,
            'services_included' => 'services_included modificada portugués',
            'services_not_included' => 'services_not_included modificada portugués',
            'legal' => 'legal en portugués'
        ]);


    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un room con una sola traduccion
     */
    public function testOk2()
    {

        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room', [
            'id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_translation', [
            'room_id' => $productFactory->id,
        ]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);


        $this->json('PUT', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'code' => 'SGL',
            'name' => ['ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español']])
            ->assertExactJson([
                'error' => 200
            ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación productos a files
     *
     * Se crea factory de files, se hace factory de product_file, al mandar relación con diferente file, borra las relaciones anteriores y crea las nuevas
     */
    public function testOk3()
    {

        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $this->assertDatabaseHas('mo_hotel_room', [
            'id' => $productFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'name file'
        ]);
        $this->assertDatabaseHas('mo_file', [
            'url_file' => $fileFactory->url_file,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => $fileFactoryTranslation->name,
        ]);

        //se crea factory con relación de product y category
        $productFileFactory = factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' => $productFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_file', [
            'room_id' => $productFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        //se crea nueva file para hacer nueva asignación y comprobar que borra la anterior
        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('nueva.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory2->id,
        ]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);


        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);


        $this->json('PUT', '/api/v1/product/room', [
            'id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'SGL',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => $fileFactory2->id]])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_hotel_room_file', [
            'file_id' => $fileFactory2->id,
        ]);

        $this->assertDatabaseMissing('mo_hotel_room_file', [
            'file_id' => $fileFactory->id,
            'deleted_at' => null,
        ]);

    }


}