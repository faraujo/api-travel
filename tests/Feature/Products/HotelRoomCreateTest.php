<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class HotelRoomCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba el funcionamiento de las validaciones
     * No se envía el hotel_id
     */

    public function testBad1()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'hotel_id' => ["The hotel id field is required."],
            ]]]);

    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     * Se envía el id de hotel con un formato erróneo
     */


    public function testBad2()
    {
        $hotelFactory = factory(App\Models\tenant\HotelRoom::class)->create([
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => 'test',
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'hotel_id' => ["The hotel id must be an integer."],
            ]]]);

    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     * Enviamos un Hotel inexistente
     */

    public function testBad3()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => 115,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'hotel_id' => ["The selected hotel id is invalid."],
            ]]]);

    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     * Se envía una categoría inexistente
     */
    public function testBad4()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => 38,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'category_id' => ["The selected category id is invalid."],
            ]]]);

    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     * Envío del campo code vacío
     */
    public function testBad5()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => '',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'code' => ["The code field is required."]
            ]]]);

    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     * Envío del campo name vacío
     */
    public function testBad6()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => [],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'name.ES' => ["The name. e s field is required."]
            ]]]);

    }

    /**
     * Test que comprueba la funcionalidad de las validaciones
     * Se envía el campo views, pero no se envía el campo description_seo
     */
    public function testBad7()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'views' => ['ES' => 'Lorem ipsum dolor sit amet'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'description_seo.ES' => ["The description seo. e s field is required."]

            ]]]);

    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda ningún campo, pide requeridos
     */
    public function testBad8()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

        ])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'category_id' => ['The category id field is required.'],
                'code' => ['The code field is required.'],
                'hotel_id' => ['The hotel id field is required.'],
                'pax' => ['The pax field is required.'],
                'translation' => ['you need at least one translation'],
                'uom_id' => ['The uom id field is required.'],
            ]]]);

    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un name con lang erroneo
     */
    public function testBad9()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['EN' => 'Name Ingles', 'PT' => 'Name PT', 'IND' => 'Name IND'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'name.ES' => ["The name. e s field is required."]
            ]]]);

    }

    /**Test que comprueba funcionamiento de las validaciones
     *
     *Envía una habitación sin traduccion
     */
    public function testBad10()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'translation' => ['you need at least one translation']
            ]]]);

    }


    /**
     * Test que comprueba el funcionamiento correcto
     * Se envían los campos requeridos en el formato adecuado
     */

    public function testOk1()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],

            'index' => ['ES' => 0],
            'breadcrumb' => ['ES' => 'keywords_seo español'],
            'rel' => ['ES' => 'keywords_seo español'],
            'og_title' => ['ES' => 'keywords_seo español'],
            'og_description' => ['ES' => 'keywords_seo español'],
            'og_image' => ['ES' => $fileFactory->id],
            'twitter_title' => ['ES' => 'keywords_seo español'],
            'twitter_description' => ['ES' => 'keywords_seo español'],
            'twitter_image' => ['ES' => $fileFactory->id],
            'canonical_url' => ['ES' => 'keywords_seo español'],
            'script_head' => ['ES' => 'keywords_seo español'],
            'script_body' => ['ES' => 'keywords_seo español'],
            'script_footer' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r']])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $hotelFactory->id,
        ]);
        $this->assertDatabaseHas('mo_hotel_room_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room', [
            'category_id' => $categoryFactory->id,
            'hotel_id' => $hotelFactory->id
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de files a room
     *
     * Se crea factory de file, se asigna file al crear el room y se comprueba en base de datos
     */

    public function testOk2()
    {
        $hotelFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('POST', '/api/v1/product/room', [

            'hotel_id' => $hotelFactory->id,
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'code' => 'SGL',
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'name' => ['ES' => 'Name ES'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r'],
            'files' => [0 => $fileFactory->id]
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_file', [
            'file_id' => $fileFactory->id,
        ]);

    }

}