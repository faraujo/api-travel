<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class HotelRoomShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta room inexistente sin parametros
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/product/room/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta producto existente sin parametros
     */
    public function testBad2()
    {

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([]);

        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'code' => 'SGL',
            'pax' => 1,
            'address' => 'Centro de Costo 10',
            'uom_id' => $uomFactory->id,
            'category_id' => $categoryFactory->id,
        ]);
        $translation1 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $productFactory->id,
        ]);


        $this->json('GET', '/api/v1/product/room/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'hotel_id' => $productFactory->hotel_id,
                'uom_id' => $uomFactory->id,
                'category_id' => $categoryFactory->id,
                'name' => $translation1->name,
                'address' => $productFactory->address,
                'pax' => $productFactory->pax,
            ]);
    }


    /**
     * Comprueba que al borrar traducción de archivo no se muestra
     */
    public function testOk3()
    {

        //Se crea factory de producto
        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([

        ]);
        $translation1 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $productFactory->id,
        ]);

        //se añade factories de file
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileTranslationFactory = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $productFileFactory = factory(App\Models\tenant\HotelRoomFile::class)->create([
            'file_id' => $fileFactory->id,
            'room_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/product/room/' . $productFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $productFactory->id,
                'hotel_id' => $productFactory->hotel_id,
                'name' => $translation1->name,
            ])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['room' => ["0" => ['id', 'files' => [],'uom_id','lang' => 'id']]]]
            ]);
    }

}