<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class HotelRoomShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta habitaciones con parametro lang mal
     */
    public function testBad1()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/product/room', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }
    
    /**
     * Test consulta habitaciones con algunos de los parametros mal
     */
    public function testBad2()
    {
        // Se especifican incorrectamente parametros
        $this->json('GET', '/api/v1/product/room', [
            'lang' => 'ggg',
            'name' => 'cualquiera',
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     *  Test consulta de habitaciones sin parametros
     */
    public function testBad3()
    {
        // No se especifica parametros
        $this->json('GET', '/api/v1/product/room')->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     *  Prueba formato de parámetros
     */
    public function testBad4()
    {
        $this->json('GET', '/api/v1/product/room',[
            'hotel_id' => 'string',
            'category_id' => 'string',
            'page' => 'string',
            'limit' => 'string',
        ])->assertExactJson([
            "error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["category_id"=>["The category id must be an integer."],"hotel_id"=>["The hotel id must be an integer."],
                    "limit"=>["The limit must be a number."],"page"=>["The page must be a number."]]]
        ]);
    }

    /**
     * Test consulta rooms con parametro lang
     */
    public function testOk1()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/product/room', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta productos con parametro lang en mayusculas
     */
    public function testOk2()
    {
        //Se especifica parametro lang correctamente en mayusculas
        $this->json('GET', '/api/v1/product/room', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta rooms con casi todos los parametros
     */
    public function testOk4()
    {
        // Se especifican correctamente parametros
        $this->json('GET', '/api/v1/product/room', [
            'lang' => 'es',
            'code' => 'a',
            'name' => 'cualquiera',
        ])->assertJsonStructure(['error', 'data']);
    }
    /**
     * Test consulta rooms con todos los parametros
     */
    public function testOk5()
    {
        // Se especifican correctamente parametros
        $this->json('GET', '/api/v1/product/room', [
            'lang' => 'es',
            'code' => 'a',
            'name' => 'cualquiera',
            'page' => 2,
            'limit' => 5,
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }
    /**
     *  Test comprueba filtro category_id
     */
    public function testOk6()
    {
        $hotel_category = factory(App\Models\tenant\HotelRoomCategory::class)->create([
        ]);
        factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'category_id' => $hotel_category->id
        ]);
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'category_id' => $hotel_category->id
        ]);

        factory(\App\Models\tenant\HotelRoomTranslation::class)->create([
            'room_id' => $roomFactory->id,
        ]);
        $this->json('GET', '/api/v1/product/room',['category_id' => $hotel_category->id,])->assertJsonFragment(['total_results'=>1]);

        $roomFactory->delete();

        $this->json('GET', '/api/v1/product/room',['category_id' => $hotel_category->id,])->assertJsonFragment(['total_results'=>0]);
    }
}

