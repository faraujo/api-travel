<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ShowUomIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta unidad de medida inexistente
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/product/uom/9999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar unidad de medida sin traducciones, devuelve json vacío
     */
    public function testOk2()
    {
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->json('GET', '/api/v1/product/uom/' . $uomFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta unidad de medida existente
     */
    public function testOk3()
    {
        //Factory de unidad de medida y sus traducciones
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);
        $this->assertDatabaseHas('mo_uom', [
            'id' => $uomFactory->id,
        ]);

        $uomTranslationFactory1 = factory(App\Models\tenant\UomTranslation::class)->create([
            'language_id' => 1,
            'uom_id' => $uomFactory->id,
            'name' => 'name español test',
            'description' => 'description español test',
        ]);

        $uomTranslationFactory2 = factory(App\Models\tenant\UomTranslation::class)->create([
            'language_id' => 2,
            'uom_id' => $uomFactory->id,
            'name' => 'name ingles test',
            'description' => 'description ingles test',
        ]);

        $uomTranslationFactory3 = factory(App\Models\tenant\UomTranslation::class)->create([
            'language_id' => 3,
            'uom_id' => $uomFactory->id,
            'name' => 'name portugues test',
            'description' => 'description portugues test',
        ]);

        $this->assertDatabaseHas('mo_uom_translation', [
            'language_id' => $uomTranslationFactory1->language_id,
            'uom_id' => $uomFactory->id,
            'name' => $uomTranslationFactory1->name,
            'description' => $uomTranslationFactory1->description,
        ]);

        $this->assertDatabaseHas('mo_uom_translation', [
            'language_id' => $uomTranslationFactory2->language_id,
            'uom_id' => $uomFactory->id,
            'name' => $uomTranslationFactory2->name,
            'description' => $uomTranslationFactory2->description,
        ]);

        $this->assertDatabaseHas('mo_uom_translation', [
            'language_id' => $uomTranslationFactory3->language_id,
            'uom_id' => $uomFactory->id,
            'name' => $uomTranslationFactory3->name,
            'description' => $uomTranslationFactory3->description,
        ]);


        $this->json('GET', '/api/v1/product/uom/' . $uomFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $uomFactory->id,
                'name' => $uomTranslationFactory1->name,
                'name' => $uomTranslationFactory2->name,
                'name' => $uomTranslationFactory3->name,
                'description' => $uomTranslationFactory1->description,
                'description' => $uomTranslationFactory2->description,
                'description' => $uomTranslationFactory3->description,

            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/product/uom/' . $uomFactory->id, [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['uom' => ["0" => ['id', 'lang']]]]
            ]);
    }

}