<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ProductTypesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
    
    /**
     * Consulta tipo de producto inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/product/types/9999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar tipo de producto sin traducciones, devuelve json vacío
     */
    public function testOk2()
    {
        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $this->json('GET', '/api/v1/product/types/' . $typeFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta tipo de producto existente
     */
    public function testOk3()
    {
        //Factory de tipo de producto y sus traducciones
        $typeFactory1 = factory(App\Models\tenant\Type::class)->create([]);

        $this->assertDatabaseHas('mo_product_type', [
            'id' => $typeFactory1->id,
        ]);

        $typeTranslationFactory1 = factory(App\Models\tenant\TypeTranslation::class)->create([
            'language_id' => 1,
            'type_id' => $typeFactory1->id,
            'name' => 'name español test',

        ]);

        $typeTranslationFactory2 = factory(App\Models\tenant\TypeTranslation::class)->create([
            'language_id' => 2,
            'type_id' => $typeFactory1->id,
            'name' => 'name ingles test',
        ]);

        $typeTranslationFactory3 = factory(App\Models\tenant\TypeTranslation::class)->create([
            'language_id' => 3,
            'type_id' => $typeFactory1->id,
            'name' => 'name portugues test',
        ]);

        $this->assertDatabaseHas('mo_product_type_translation', [
            'language_id' => $typeTranslationFactory1->language_id,
            'type_id' => $typeFactory1->id,
            'name' => $typeTranslationFactory1->name,

        ]);

        $this->assertDatabaseHas('mo_product_type_translation', [
            'language_id' => $typeTranslationFactory2->language_id,
            'type_id' => $typeFactory1->id,
            'name' => $typeTranslationFactory2->name,

        ]);

        $this->assertDatabaseHas('mo_product_type_translation', [
            'language_id' => $typeTranslationFactory3->language_id,
            'type_id' => $typeFactory1->id,
            'name' => $typeTranslationFactory3->name,

        ]);


        $this->json('GET', '/api/v1/product/types/' . $typeFactory1->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $typeFactory1->id,
                'name' => $typeTranslationFactory1->name,
                'name' => $typeTranslationFactory2->name,
                'name' => $typeTranslationFactory3->name,

            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/product/types/' . $typeFactory1->id, [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['type' => ["0" => ['id', 'lang']]]]
            ]);
    }

}