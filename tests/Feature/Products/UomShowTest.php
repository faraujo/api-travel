<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ShowUomTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de unidades de medida con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/product/uom/', [
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."]]]]);
    }

    /**
     * test consulta de unidades de medida con parametro lang incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/product/uom/', [

            'lang' => 'eeeeeeee',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["lang" => ["The lang is not exists"]]]]);
    }


    /**
     * test crea factory de unidades de medida con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de unidad de medida y sus traducciones
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->assertDatabaseHas('mo_uom', [
            'id' => $uomFactory->id,
        ]);

        $uomTranslationFactory1 = factory(App\Models\tenant\UomTranslation::class)->create([
            'language_id' => 1,
            'uom_id' => $uomFactory->id,
            'name' => 'name español test',
            'description' => 'description español test',
        ]);

        $uomTranslationFactory2 = factory(App\Models\tenant\UomTranslation::class)->create([
            'language_id' => 2,
            'uom_id' => $uomFactory->id,
            'name' => 'name ingles test',
            'description' => 'description ingles test',
        ]);

        $uomTranslationFactory3 = factory(App\Models\tenant\UomTranslation::class)->create([
            'language_id' => 3,
            'uom_id' => $uomFactory->id,
            'name' => 'name portugues test',
            'description' => 'description portugues test',
        ]);

        $this->assertDatabaseHas('mo_uom_translation', [
            'language_id' => $uomTranslationFactory1->language_id,
            'uom_id' => $uomFactory->id,
            'name' => $uomTranslationFactory1->name,
            'description' => $uomTranslationFactory1->description,
        ]);

        $this->assertDatabaseHas('mo_uom_translation', [
            'language_id' => $uomTranslationFactory2->language_id,
            'uom_id' => $uomFactory->id,
            'name' => $uomTranslationFactory2->name,
            'description' => $uomTranslationFactory2->description,
        ]);

        $this->assertDatabaseHas('mo_uom_translation', [
            'language_id' => $uomTranslationFactory3->language_id,
            'uom_id' => $uomFactory->id,
            'name' => $uomTranslationFactory3->name,
            'description' => $uomTranslationFactory3->description,
        ]);


        $this->json('GET', '/api/v1/product/uom/', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $uomFactory->id,
                'name' => $uomTranslationFactory1->name,
                'name' => $uomTranslationFactory2->name,
                'name' => $uomTranslationFactory3->name,
                'description' => $uomTranslationFactory1->description,
                'description' => $uomTranslationFactory2->description,
                'description' => $uomTranslationFactory3->description,

            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/product/uom/', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['uom' => ["0" => ['id', 'lang']]]]
            ]);

    }


}