<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class ProductsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations en formato incorrecto
     */
    public function testBad1()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,

            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r'],
            'files' => [0 => 'r'],
            'services' => [0 => 'r'],
            'locations' => [0 => 'r'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "categories.0" => ["The categories.0 must be an integer."],
                    "files.0" => ["The files.0 must be an integer."],
                    "tags.0" => ["The tags.0 must be an integer."],
                    "services.0" => ["The services.0 must be an integer."],
                    "locations.0" => ["The locations.0 must be an integer."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations inexistentes
     */
    public function testBad2()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,

            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 999999999],
            'tags' => [0 => 99999999],
            'files' => [0 => 99999999],
            'services' => [0 => 999999999],
            'locations' => [0 => 99999999],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "categories.0" => ["The selected categories.0 is invalid."],
                    "files.0" => ["The selected files.0 is invalid."],
                    "tags.0" => ["The selected tags.0 is invalid."],
                    "services.0" => ["The selected services.0 is invalid."],
                    "locations.0" => ["The selected locations.0 is invalid."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y los campos categories, tags, files, services y locations sin formato array.
     */
    public function testBad3()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'categories' => '2',
            'tags' => '2',
            'files' => '2',
            'services' => '2',
            'locations' => '2',
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "categories" => ["The categories must be an array."],
                "tags" => ["The tags must be an array."],
                "files" => ["The files must be an array."],
                "services" => ["The services must be an array."],
                "locations" => ["The locations must be an array."],
            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y los campos categories, tags, files, services y locations con valor duplicado.
     */
    public function testBad4()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);
        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([]);
        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);
        $fileFactory = factory(App\Models\tenant\File::class)->create([]);
        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'categories' => [0 => $categoryFactory->id, 1 => $categoryFactory->id],
            'tags' => [0 => $tagFactory->id, 1 => $tagFactory->id],
            'files' => [0 => $fileFactory->id, 1 => $fileFactory->id],
            'services' => [0 => $serviceFactory->id, 1 => $serviceFactory->id],
            'locations' => [0 => $locationFactory->id, 1 => $locationFactory->id],
            'iva_id' => $ivaFactory->id
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "categories.0" => ["The categories.0 field has a duplicate value."],
                "categories.1" => ["The categories.1 field has a duplicate value."],
                "tags.0" => ["The tags.0 field has a duplicate value."],
                "tags.1" => ["The tags.1 field has a duplicate value."],
                "files.0" => ["The files.0 field has a duplicate value."],
                "files.1" => ["The files.1 field has a duplicate value."],
                "services.0" => ["The services.0 field has a duplicate value."],
                "services.1" => ["The services.1 field has a duplicate value."],
                "locations.0" => ["The locations.0 field has a duplicate value."],
                "locations.1" => ["The locations.1 field has a duplicate value."],
            ]]]);
    }



    /**
     * Prueba automatizada comprueba el tipo de dato del campo iva
     */
    public function testBad5()
    {

        $this->json('PUT', '/api/v1/product', [
            'iva_id' => 'TEST',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "code"=>["The code field is required."],
                    "company_id"=>["The company id field is required."],
                    "id"=>["The id field is required."],
                    "iva_id"=>["The iva id must be an integer."],
                    "locations"=>["The locations field is required."],
                    "pax"=>["The pax field is required."],
                    "translation"=>["you need at least one translation"],
                    "type_id"=>["The type id field is required."],
                    "uom_id"=>["The uom id field is required."]]]]);
    }

    public function testBad7(){

        $product = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 1,
        ]);
        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $product->id,
            'type_id' => 1,
            'product_package' => [
                '0' => 'Test'
            ],
            'iva_id' => $ivaFactory->id,
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "code" => ["The code field is required."],
                    "company_id" => ["The company id field is required."],
                    "locations"=>["The locations field is required."],
                    "pax"=>["The pax field is required."],
                    "product_package.0.id"=>["The product_package.0.id field is required."],
                    "product_package.0.location_id"=>["The product_package.0.location_id field is required."],
                    "translation"=>["you need at least one translation"],
                    "uom_id"=>["The uom id field is required."]]]]);
    }


    /**
     * test para comprobar el validador de campos obligatorios
     */
    public function testValidatorRequired()
    {
        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('put', '/api/v1/product', [
            'id' => 109,
            'code' => '250',

            'type_id' => '',

            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["type_id" => ["The type id field is required."],
                "uom_id" => ["The uom id field is required."],"company_id" => ["The company id field is required."],"locations" => ["The locations field is required."],
                "pax" => ["The pax field is required."]]]]);
    }

    /**
     * test para comprobar el validador de campos billable y pax, manda string en lugar de 0 o 1
     */
    public function testValidatorBillable()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('put', '/api/v1/product', [
            'id' => 109,
            'code' => '250',

            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'billable' => 'r',
            'pax' => 'z',
            'locations' => [0 => $locationFactory->id],
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'company_id' => $companyFactory->id,
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["billable" => ["The billable field must be true or false."],"pax" => ["The pax field must be true or false."]]]]);
    }

    /**
     * test para comprobar el validador de campos billable y pax, manda número mayor de 1, y company inexistente en base de datos
     */
    public function testValidatorBillable2()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('put', '/api/v1/product', [
            'id' => 109,
            'code' => '250',
            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'billable' => 2222,
            'pax' => 25,
            'locations' => [0 => $locationFactory->id],
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'company_id' => 22222222222222222,
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["billable" => ["The billable field must be true or false."],
                "company_id" => ['0' => "The company id is not exists."], "pax" => ["The pax field must be true or false."]]]]);
    }

    /**
     * test para comprobar el validador, formatos incorrectos
     */
    public function testValidatorNumber()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('put', '/api/v1/product', [
            'id' => $productFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => '250',
            'type_id' => 'r',
            'company_id' => 'r',
            'uom_id' => 'r',
            'pax' => 0,
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'iva_id' => $ivaFactory->id

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["company_id"=>["The company id is not exists."],"type_id"=>["The type id is not exists."],"uom_id" =>["The uom id must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador, uom_id inexistente en base de datos
     */
    public function testUomInexistente()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('put', '/api/v1/product', [
            'id' => $productFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => '250',
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => 99999999,
            'pax' => 0,
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["uom_id"=>["The selected uom id is invalid."]]]]);
    }

    /**
     * Test para modificar un producto
     *
     * A partir de los datos del producto que quiere modificar modifica el usuario los campos que desee
     * Comprueba que los datos se actualizan correctamente en base de datos.
     * Comprueba que borra la relación primera con mo_product_location y crea otra con el location_id enviado
     *
     */
    public function testModificar()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
            'pax' => 0,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $productLocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
            'code' => $productFactory->code,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_location', [
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('put', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory2->id],
            'code' => '100',
            'pax' => 1,
            'name' => ['ES' => 'nombre CON ÑOO ñoo asáaá modificado', 'EN' => 'name update Inglés', 'PT' => 'name update portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'company_id' => $companyFactory->id,
            'friendly_url' => ['ES' => '1friendly_url3 UUmodificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'discover_url' => ['ES' => 'discover_url españolMAA', 'EN' => 'discover_url inglés', 'PT' => 'discover_url portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
            'features' => ['ES' => 'Features modificada español', 'EN' => 'Features modificada inglés', 'PT' => 'Features modificada portugués'],
            'recommendations' => ['ES' => 'Recommendations modificada español', 'EN' => 'Recommendations modificada inglés', 'PT' => 'Recommendations modificada portugués'],
            'order' => 10,
            'longitude' => -225.3333,
            'latitude' => 152.0000025,
            'address' => 'address product',
            'cost_center' => 'Centro de Costo 2',
            'accounting_account' => '325',
            'legal' => ['ES' => 'legal en español', 'EN' => 'legal en inglés', 'PT' => 'legal en portugués'],
            'iva_id' => $ivaFactory->id,

            'index' => ['ES' => 0, 'EN' => 1, 'PT' => 1],
            'breadcrumb' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'rel' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_image' => ['ES' => $fileFactory->id,'EN' => $fileFactory->id,'PT' => $fileFactory->id],
            'twitter_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_image' => ['ES' => $fileFactory->id,'EN' => $fileFactory->id,'PT' => $fileFactory->id],
            'canonical_url' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_head' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_body' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_footer' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => '100',
            'height_to' => null,
            'order' => 10,
            'longitude' => -225.3333,
            'latitude' => 152.0000025,
            'address' => 'address product',
            'cost_center' => 'Centro de Costo 2',
            'accounting_account' => '325',
            'pax' => 1,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
            'language_id' => 1,
            'features' => 'Features modificada español',
            'recommendations' => 'Recommendations modificada español',
            'legal' => 'legal en español',
            'discover_url' => 'discover_url españolMAA'
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
            'language_id' => 2,
            'features' => 'Features modificada inglés',
            'recommendations' => 'Recommendations modificada inglés',
            'legal' => 'legal en inglés',
            'discover_url' => 'discover_url inglés'
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
            'language_id' => 3,
            'features' => 'Features modificada portugués',
            'recommendations' => 'Recommendations modificada portugués',
            'legal' => 'legal en portugués',
            'discover_url' => 'discover_url portugués'
        ]);

        //comprueba que se ha borrado la primera relación del producto con location_id
        $this->assertDatabaseMissing('mo_product_location', [
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
            'deleted_at' => null,
        ]);

        //comprueba que se crea nueva relación del producto con location_id enviado al modificar
        $this->assertDatabaseHas('mo_product_location', [
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory2->id,
        ]);

    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto con una sola traduccion
     */
    public function testLangUnico()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
            'code' => $productFactory->code,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'company_id' => $companyFactory->id,
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 200
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto sin traduccion
     */
    public function testSinTraducciones()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
            'code' => $productFactory->code,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,

            'locations' => [0 => $locationFactory->id],
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'company_id' => $companyFactory->id,
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto con un campo no requerido
     */
    public function testLangCampoNoRequerido()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
            'code' => $productFactory->code,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
        ]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'recommendations' => ['ES' => 'Haname'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                "error" => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "company_id" => ["The company id field is required."],
                    "description.ES" => ["The description. e s field is required."],
                    "description_seo.ES" => ["The description seo. e s field is required."],
                    "locations" => ["The locations field is required."],
                    "name.ES" => ["The name. e s field is required."],
                    "short_description.ES" => ["The short description. e s field is required."],
                    "title_seo.ES" => ["The title seo. e s field is required."]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación productos a categorias y a etiquetas
     *
     * Se crea factory de categorías y de etiquetas, se hace factory de product_category y de product_tag, al mandar relación con diferente category y tag, borra las relaciones anteriores y crea las nuevas
     */
    public function testOk1()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        //se crea factory con relación de product y category
        $productCategoryFactory = factory(App\Models\tenant\ProductCategory::class)->create([
            'product_id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_category', [
            'product_id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
        ]);

        //se crea nueva category para hacer nueva asignación y comprobar que borra la anterior
        $categoryFactory2 = factory(App\Models\tenant\Category::class)->create([]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory2->id,
        ]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);

        $productTagFactory = factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $productFactory->id,
            'tag_id' => $tagFactory->id,
        ]);

        //se crea factory con relación de product y tag
        $this->assertDatabaseHas('mo_product_tag', [
            'product_id' => $productFactory->id,
            'tag_id' => $tagFactory->id,
        ]);

        //se crea nueva tag para hacer nueva asignación y comprobar que borra la anterior
        $tagFactory2 = factory(App\Models\tenant\Tag::class)->create([]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory2->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => $categoryFactory2->id],
            'tags' => [0 => $tagFactory2->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_product_category', [
            'category_id' => $categoryFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_product_tag', [
            'tag_id' => $tagFactory2->id,
        ]);


        $this->assertDatabaseMissing('mo_product_category', [
            'category_id' => $categoryFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_product_tag', [
            'tag_id' => $tagFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación productos a files
     *
     * Se crea factory de files, se hace factory de product_file, al mandar relación con diferente file, borra las relaciones anteriores y crea las nuevas
     */
    public function testOk2()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'name file'
        ]);
        $this->assertDatabaseHas('mo_file', [
            'url_file' => $fileFactory->url_file,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => $fileFactoryTranslation->name,
        ]);

        //se crea factory con relación de product y category
        $productFileFactory = factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' => $productFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_file', [
            'product_id' => $productFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        //se crea nueva file para hacer nueva asignación y comprobar que borra la anterior
        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('nueva.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory2->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => $fileFactory2->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_product_file', [
            'file_id' => $fileFactory2->id,
        ]);

        $this->assertDatabaseMissing('mo_product_file', [
            'file_id' => $fileFactory->id,
            'deleted_at' => null,
        ]);

    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación de servicios a productos
     *
     * Se mandan campos requeridos, con una única traducción, y el campo services.
     */

    public function testOk4()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory2->id,
        ]);

        $productServiceFactory = factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);
        $this->assertDatabaseHas('mo_product_service', [
            'id' => $productServiceFactory->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('PUT', '/api/v1/product', [
            'id' => $productFactory->id,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'services' => [0 => $serviceFactory->id, 1 => $serviceFactory2->id],
            'iva_id' => $ivaFactory->id]
            )
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_product_service', [
            'service_id' => $serviceFactory->id,
        ]);
        $this->assertDatabaseHas('mo_product_service', [
            'service_id' => $serviceFactory2->id,
        ]);
        //Comprueba borrado de la anterior relación
        $this->assertDatabaseMissing('mo_product_service', [
            'id' => $productServiceFactory->id,
            'deleted_at' => null,
        ]);
    }

    public function testOk5 () {
        $product1 = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 1,
        ]);

        $product2 = factory(\App\Models\tenant\Product::class)->create([]);

        $productPackage = factory(\App\Models\tenant\ProductPackage::class)->create([
            'package_id' => $product1,
            'product_id' => $product2,
        ]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $type = factory(\App\Models\tenant\Type::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->assertDatabaseHas('mo_product_package', ['package_id' => $product1->id, 'product_id' => $product2->id, 'deleted_at' => null]);

        $this->json('PUT','/api/v1/product', [
            'id' => $product1->id,
            'type_id' => $type->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'iva_id' => $ivaFactory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_product_package', ['package_id' => $product1->id, 'product_id' => $product2->id, 'deleted_at' => null]);

    }

    public function testOk9(){

        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $product_update = factory(\App\Models\tenant\Product::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([]);
        $locationFactory3 = factory(App\Models\tenant\Location::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'id' => $product_update->id,
            'type_id' => 1,
            'product_package' => [
                0 => [
                    'id' => $product->id,
                    'location_id' => $locationFactory3->id
                ]
            ],
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 1,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'locations' => [0 => $locationFactory->id, 1 => $locationFactory2->id],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 200]);
    }
}