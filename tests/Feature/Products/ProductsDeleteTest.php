<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ProductsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/product', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["ids" => ["The ids field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en campo id requerido
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/product', [
            'ids' => ['caracteres'],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda id de usuario inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/product', [
            'ids' => [99999999],
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found", "error_inputs" => [
                [
                    "ids.0" => ["The selected ids.0 is invalid."],
                ]
            ]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/product', [
            'ids' => $productFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }


    /**
     * Borrar un producto sin traducciones ni paquetes
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'ABC',
        ]);

        $this->json('DELETE', '/api/v1/product', [
            'ids' => [$productFactory->id],
        ])->assertExactJson(['error' => 200]);

        //comprueba que en la tabla mo_product el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory->id]);

    }

    /**
     * Test para eliminar un producto que crea primero un product factory y sus traducciones
     */
    public function testOk2()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
        ]);


        $this->json('DELETE', '/api/v1/product', [
            'ids' => [$productFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que en la tabla mo_product el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory->id]);
        //comprueba que las traducciones del producto que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation1->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation2->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation3->product_id]);
    }

    /**
     * Test para eliminar un producto que crea primero un product factory, un paquete del mismo producto y es incluido en un otro paquete
     */
    public function testOk3()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);

        $packageFactory1 = factory(App\Models\tenant\ProductPackage::class)->create([
            'package_id' => $productFactory->id,
        ]);

        $packageFactory2 = factory(App\Models\tenant\ProductPackage::class)->create([
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'package_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'product_id' => $productFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/product', [
            'ids' => [$productFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que en la tabla mo_product el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory->id]);
        //comprueba que los paquetes en los que está incluido no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory1->package_id]);
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory2->product_id]);
    }


    /**
     * Test para eliminar un producto que tiene traducciones, es un paquete y esté incluido en un paquete
     */
    public function testOk4()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([

        ]);

        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $packageFactory1 = factory(App\Models\tenant\ProductPackage::class)->create([
            'package_id' => $productFactory->id,
        ]);

        $packageFactory2 = factory(App\Models\tenant\ProductPackage::class)->create([
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'package_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/product', [
            'ids' => [$productFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que en la tabla mo_product el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory->id]);
        //comprueba que los paquetes en los que está incluido el producto que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory1->package_id]);
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory2->product_id]);
        //comprueba que las traducciones del producto que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation1->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation2->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation3->product_id]);
    }

    /**
     * Borrar tres productos sin traducciones ni paquetes
     */
    public function testOk5()
    {
        $productFactory1 = factory(App\Models\tenant\Product::class)->create([
        ]);
        $productFactory2 = factory(App\Models\tenant\Product::class)->create([
        ]);
        $productFactory3 = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('DELETE', '/api/v1/product', [
            'ids' => [$productFactory1->id, $productFactory2->id, $productFactory3->id],
        ])->assertExactJson(['error' => 200]);

        //comprueba que en la tabla mo_product los registro con los id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory1->id]);
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory2->id]);
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory3->id]);

    }

    /**
     * Borrar dos productos con traducciones y que están incluidos en paquetes
     */
    public function testOk6()
    {

        $productFactory1 = factory(App\Models\tenant\Product::class)->create([
        ]);
        $productFactory2 = factory(App\Models\tenant\Product::class)->create([
        ]);

        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory1->id,
        ]);
        $translation2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory1->id,
        ]);
        $translation3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory1->id,
        ]);
        $translation4 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory2->id,
        ]);
        $translation5 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory2->id,
        ]);
        $translation6 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory2->id,
        ]);


        $packageFactory1 = factory(App\Models\tenant\ProductPackage::class)->create([
            'package_id' => $productFactory1->id,
        ]);
        $packageFactory2 = factory(App\Models\tenant\ProductPackage::class)->create([
            'product_id' => $productFactory1->id,
        ]);

        $packageFactory3 = factory(App\Models\tenant\ProductPackage::class)->create([
            'package_id' => $productFactory2->id,
        ]);
        $packageFactory4 = factory(App\Models\tenant\ProductPackage::class)->create([
            'product_id' => $productFactory2->id,
        ]);


        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory1->id,
        ]);
        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'package_id' => $productFactory1->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'product_id' => $productFactory1->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'package_id' => $productFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_product_package', [
            'product_id' => $productFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory1->id,
        ]);
        $this->assertDatabaseHas('mo_product_translation', [
            'product_id' => $productFactory2->id,
        ]);

        $this->json('DELETE', '/api/v1/product', [
            'ids' => [$productFactory1->id,$productFactory2->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que en la tabla mo_product el registro con los id que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory1->id]);
        $this->assertDatabaseMissing('mo_product', ['deleted_at' => null, 'id' => $productFactory2->id]);
        //comprueba que los paquetes en los que están incluidos los producto que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory1->package_id]);
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory2->product_id]);
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory3->package_id]);
        $this->assertDatabaseMissing('mo_product_package', ['deleted_at' => null, 'product_id' => $packageFactory4->product_id]);
        //comprueba que las traducciones de los productos que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation1->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation2->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation3->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation4->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation5->product_id]);
        $this->assertDatabaseMissing('mo_product_translation', ['deleted_at' => null, 'product_id' => $translation6->product_id]);
    }

}