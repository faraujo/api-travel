<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class ProductsCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations en formato incorrecto
     */
    public function testBad1()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 'r'],
            'tags' => [0 => 'r'],
            'files' => [0 => 'r'],
            'services' => [0 => 'r'],
            'locations' => [0 => 'r'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "categories.0" => ["The categories.0 must be an integer."],
                    "files.0" => ["The files.0 must be an integer."],
                    "tags.0" => ["The tags.0 must be an integer."],
                    "services.0" => ["The services.0 must be an integer."],
                    "locations.0" => ["The locations.0 must be an integer."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations inexistentes
     */
    public function testBad2()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => 999999999],
            'tags' => [0 => 99999999],
            'files' => [0 => 99999999],
            'services' => [0 => 999999999],
            'locations' => [0 => 999999999],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "categories.0" => ["The selected categories.0 is invalid."],
                    "files.0" => ["The selected files.0 is invalid."],
                    "tags.0" => ["The selected tags.0 is invalid."],
                    "services.0" => ["The selected services.0 is invalid."],
                    "locations.0" => ["The selected locations.0 is invalid."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y los campos categories, tags, files, services y locations sin formato array.
     */
    public function testBad3()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'categories' => '2',
            'tags' => '2',
            'files' => '2',
            'services' => '2',
            'locations' => '2',
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "categories" => ["The categories must be an array."],
                    "tags" => ["The tags must be an array."],
                    "files" => ["The files must be an array."],
                    "services" => ["The services must be an array."],
                    "locations" => ["The locations must be an array."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y los campos categories, tags, files, services y locations con valor duplicado.
     */
    public function testBad4()
    {
        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);
        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([]);
        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);
        $fileFactory = factory(App\Models\tenant\File::class)->create([]);
        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $this->assertDatabaseHas('mo_location', [
            'id' => $locationFactory->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'categories' => [0 => $categoryFactory->id, 1 => $categoryFactory->id],
            'tags' => [0 => $tagFactory->id, 1 => $tagFactory->id],
            'files' => [0 => $fileFactory->id, 1 => $fileFactory->id],
            'services' => [0 => $serviceFactory->id, 1 => $serviceFactory->id],
            'locations' => [0 => $locationFactory->id, 1 => $locationFactory->id],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "categories.0" => ["The categories.0 field has a duplicate value."],
                "categories.1" => ["The categories.1 field has a duplicate value."],
                "tags.0" => ["The tags.0 field has a duplicate value."],
                "tags.1" => ["The tags.1 field has a duplicate value."],
                "files.0" => ["The files.0 field has a duplicate value."],
                "files.1" => ["The files.1 field has a duplicate value."],
                "services.0" => ["The services.0 field has a duplicate value."],
                "services.1" => ["The services.1 field has a duplicate value."],
                "locations.0" => ["The locations.0 field has a duplicate value."],
                "locations.1" => ["The locations.1 field has a duplicate value."],
            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda ningún campo, pide requeridos
     */
    public function testBad5()
    {

        $this->json('POST', '/api/v1/product',[])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["code"=>["The code field is required."],"company_id"=>["The company id field is required."],
                    "locations"=>["The locations field is required."],"pax"=>["The pax field is required."],"translation"=>["you need at least one translation"],
                    "type_id"=>["The type id field is required."],"uom_id"=>["The uom id field is required."],
                    'iva_id' => ["The iva id field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y se envía un uom_id
     */
    public function testBad6()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => 9999999,
            'pax' => 0,
            'locations' => [0 => $locationFactory->id],
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'cualquiera español', 'EN' => 'Cualquiera Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["uom_id"=>["The selected uom id is invalid."],
                ]]]);
    }

    /**
     * Prueba automatizada comprueba el tipo de dato del campo iva
     */
    public function testBad7()
    {

        $this->json('POST', '/api/v1/product', [
            'iva_id' => 'TEST',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["code"=>["The code field is required."],"company_id"=>["The company id field is required."],
                    "locations"=>["The locations field is required."],"pax"=>["The pax field is required."],"translation"=>["you need at least one translation"],
                    "type_id"=>["The type id field is required."],"uom_id"=>["The uom id field is required."],
                    'iva_id' => ["The iva id must be an integer."]]]]);
    }

    public function testBad9(){

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => 1,
            'product_package' => [
                '0' => 'Test'
            ],
            'iva_id' => $ivaFactory->id,
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "code" => ["The code field is required."],
                    "company_id" => ["The company id field is required."],
                    "locations"=>["The locations field is required."],
                    "pax"=>["The pax field is required."],
                    "product_package.0.id"=>["The product_package.0.id field is required."],
                    "product_package.0.location_id"=>["The product_package.0.location_id field is required."],
                    "translation"=>["you need at least one translation"],
                    "uom_id"=>["The uom id field is required."]]]]);
    }


    /**
     *Test que comprueba funcionamiento correcto de validación de código repetido
     *
     * Se crea factory de categorías, se asigna categoría al crear producto y se comprueba en base de datos
     */
    public function testBad10()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create(['code'=>'test']);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'test',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => $categoryFactory->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "code" => ["The code already exists."],
                    ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos string donde deben ser numéricos y no se envía company_id ni locations que son requeridos
     */
    public function testValidator()
    {
        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => 'r',
            'uom_id' => 'r',
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'cualquiera español', 'EN' => 'Cualquiera Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["company_id" => ["The company id field is required."],
                "locations" => ["The locations field is required."],"type_id" => ['0' => "The type id is not exists."],"uom_id" => ['0' => "The uom id must be an integer."],
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos billable y pax con formato string, debe ser 0 o 1
     */
    public function testBillableandPax()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'billable' => 'r',
            'pax' => 'z',
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'cualquiera español', 'EN' => 'Cualquiera Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["billable" => ["The billable field must be true or false."],"pax" => ["The pax field must be true or false."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos billable y pax diferente de 0 o 1, es un booleano y debe ser 0 o 1
     */
    public function testBillableandPax2()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'billable' => 2,
            'pax' => 4,
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'cualquiera español', 'EN' => 'Cualquiera Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["billable" => ["The billable field must be true or false."],"pax" => ["The pax field must be true or false."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un name con lang erroneo
     */
    public function testLang()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'locations' => [0 => $locationFactory->id],
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués', 'IND' => 'Haname españa'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'company_id' => $companyFactory->id,
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 400, 'error_description' => "The fields are not the required format", 'error_inputs' => [["name.ES" => ["The name. e s field is required."]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda campo company_id inexistente en base de datos
     */
    public function testCompanyNotExists()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'billable' => 0,
            'type_id' => 1,
            'pax' => 0,
            'company_id' => 1111111111111111,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'cualquiera español', 'EN' => 'Cualquiera Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["company_id" => ['0' => "The company id is not exists."]]]]);
    }


    /**
     * Realiza el test de creación de productos con sus traducciones correspondientes
     * Comprueba primero que el code del producto no existe en base de datos y luego lo crea
     * Comprueba campo billable al no enviarse guarda 1 por defecto
     */
    public function testCrear()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $this->assertDatabaseMissing('mo_product', [
            'code' => '9999999999999999999999999999999999999999'
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'Haname españa', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'company_id' => $companyFactory->id,
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'discover_url' => ['ES' => 'discover_url españolMAA', 'EN' => 'discover_url inglés', 'PT' => 'discover_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],

            'index' => ['ES' => 0, 'EN' => 1, 'PT' => 1],
            'breadcrumb' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'rel' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_image' => ['ES' => $fileFactory->id],
            'twitter_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_image' => ['ES' => $fileFactory->id],
            'canonical_url' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_head' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_body' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_footer' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],

            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_product', [
            'code' => '9999999999999999999999999999999999999999',
            //comprueba que campo billable si no se envía guarda 1 por defecto
            'billable' => 1,
            'pax' => 0,
            'uom_id' => $uomFactory->id
        ]);

    }

    /**
     * Realiza el test de creación de productos con sus traducciones correspondientes.
     * Comprueba primero que el code del producto no existe en base de datos y luego lo crea.
     * Se añaden los parámetros nuevos para comprobar que se insertan correctamente en las tablas.
     *
     */
    public function testCrear2()
    {
        $this->assertDatabaseMissing('mo_product', [
            'code' => '8888888888888888888888888888888888888888'
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'pax' => 0,
            'code' => '8888888888888888888888888888888888888888',
            'name' => ['ES' => 'Nombre españa', 'EN' => 'Nombre Inglés', 'PT' => 'Nombre portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español test create', 'EN' => 'Short descripcion inglés test create', 'PT' => 'Short descripción portugués test create'],
            'company_id' => $companyFactory->id,
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'discover_url' => ['ES' => 'discover_url españolMAA', 'EN' => 'discover_url inglés', 'PT' => 'discover_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'features' => ['ES' => 'Features modificada español test create', 'EN' => 'Features modificada inglés test create', 'PT' => 'Features modificada portugués test create'],
            'recommendations' => ['ES' => 'Recommendations modificada español test create', 'EN' => 'Recommendations modificada inglés test create', 'PT' => 'Recommendations modificada portugués test create'],
            'order' => 2,
            'longitude' => 20.55678843,
            'latitude' => -85.32123,
            'address' => 'address product',
            'cost_center' => 'Centro de Costo 1',
            'accounting_account' => '026',
            'legal' => ['ES' => 'condiciones legales es', 'EN' => 'condiciones legales en', 'PT' => 'condiciones legales pt'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 200,
            ]);
        $this->assertDatabaseHas('mo_product', [
            'code' => '8888888888888888888888888888888888888888',
            'order' => 2,
            'longitude' => 20.55678843,
            'latitude' => -85.32123,
            'address' => 'address product',
            'cost_center' => 'Centro de costo 1',
            'accounting_account' => '026',
            'uom_id' => $uomFactory->id
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'short_description' => 'Short descripcion español test create',
            'language_id' => 1,
            'features' => 'Features modificada español test create',
            'recommendations' => 'Recommendations modificada español test create',
            'legal' => 'condiciones legales es',
            'discover_url' => 'discover_url españolMAA'
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'short_description' => 'Short descripcion inglés test create',
            'language_id' => 2,
            'features' => 'Features modificada inglés test create',
            'recommendations' => 'Recommendations modificada inglés test create',
            'legal' => 'condiciones legales en',
            'discover_url' => 'discover_url inglés'
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'short_description' => 'Short descripción portugués test create',
            'language_id' => 3,
            'features' => 'Features modificada portugués test create',
            'recommendations' => 'Recommendations modificada portugués test create',
            'legal' => 'condiciones legales pt',
            'discover_url' => 'discover_url portugués'
        ]);

    }

    /**
     * Realiza el test de creación de producto y su traducciones correspondientes, con order, y longitude vacío y sin friendly_url, ni feature[ES]
     *
     */
    public function testCrear3()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => '7777777777777777777777777',
            'pax' => 1,
            'name' => ['ES' => 'Nombre españa', 'EN' => 'Nombre Inglés', 'PT' => 'Nombre portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español test create 3', 'EN' => 'Short descripcion inglés test create 3', 'PT' => 'Short descripción portugués test create 3'],
            'company_id' => $companyFactory->id,
            'locations' => [0 => $locationFactory->id],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' > ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'features' => ['EN' => 'Features modificada inglés test create 3', 'PT' => 'Features modificada portugués test create 3'],
            'recommendations' => ['ES' => 'Recommendations modificada español test create 3', 'EN' => 'Recommendations modificada inglés test create 3', 'PT' => 'Recommendations modificada portugués test create 3'],
            'order' => '',
            'longitude' => '',
            'legal' => ['EN' => 'legal inglés', 'PT' => 'legal portugués'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_product', [
            'code' => '7777777777777777777777777',
            'order' => 0,
            'longitude' => null,
            'latitude' => null,
            'address' => null,
            'uom_id' => $uomFactory->id
        ]);

        $this->assertDatabaseHas('mo_product_translation', [
            'short_description' => 'Short descripcion español test create 3',
            'language_id' => 1,
            'features' => null,
            'recommendations' => 'Recommendations modificada español test create 3',
            'friendly_url' => 'Nombre-españa',
            'legal' => null,
        ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto con una sola traduccion
     */
    public function testLangUnico()
    {

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'name' => ['ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'company_id' => $companyFactory->id,
            'locations' => [0 => $locationFactory->id],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 200
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto sin un campo no requerido
     */
    public function testLangCampoNoRequerido()
    {

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'locations' => [0 => $locationFactory->id],
            'recommendations' => ['ES' => 'Haname'],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                "error" => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "company_id" => ["The company id field is required."],
                    "description.ES" => ["The description. e s field is required."],
                    "description_seo.ES" => ["The description seo. e s field is required."],
                    "name.ES" => ["The name. e s field is required."],
                    "short_description.ES" => ["The short description. e s field is required."],
                    "title_seo.ES" => ["The title seo. e s field is required."]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto sin traduccion
     */
    public function testSinTraducciones()
    {
        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [

            'type_id' => $typeFactory->id,
            'uom_id' => $uomFactory->id,
            'pax' => 0,
            'code' => '9999999999999999999999999999999999999999',
            'company_id' => $companyFactory->id,
            'locations' => [0 => $locationFactory->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación categorías a productos
     *
     * Se crea factory de categorías, se asigna categoría al crear producto y se comprueba en base de datos
     */
    public function testOk1()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => $categoryFactory->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_category', [
            'category_id' => $categoryFactory->id,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de categorías a productos
     *
     * Se crean 2 factories de categorías, se asignan categorías al crear producto y se comprueba en base de datos
     */
    public function testOk2()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([]);

        $categoryFactory2 = factory(App\Models\tenant\Category::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 1,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'categories' => [0 => $categoryFactory->id, 1 => $categoryFactory2->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_category', [
            'id' => $categoryFactory2->id,
        ]);


        $this->assertDatabaseHas('mo_product_category', [
            'category_id' => $categoryFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_category', [
            'category_id' => $categoryFactory2->id,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de tags a productos
     *
     * Se crea factory de tag, se asigna tag al crear producto y se comprueba en base de datos
     */
    public function testOk3()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'tags' => [0 => $tagFactory->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_tag', [
            'tag_id' => $tagFactory->id,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de tags a productos
     *
     * Se crean factorys de tags, se asignan tags al crear producto y se comprueban en base de datos
     */
    public function testOk4()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([]);

        $tagFactory2 = factory(App\Models\tenant\Tag::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'tags' => [0 => $tagFactory->id, 1 => $tagFactory2->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_product_tag', [
            'tag_id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_tag', [
            'tag_id' => $tagFactory2->id,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de files a productos
     *
     * Se crea factory de file, se asigna file al crear producto y se comprueba en base de datos
     */
    public function testOk5()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => $fileFactory->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_file', [
            'file_id' => $fileFactory->id,
        ]);

    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de servicios a productos
     *
     * Se mandan campos requeridos, con una única traducción, y el campo services.
     */

    public function testOk6()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);
        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory2->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'locations' => [0 => $locationFactory->id],
            'code' => 'codeee',
            'pax' => 0,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'services' => [0 => $serviceFactory->id, 1 => $serviceFactory2->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_product_service', [
            'service_id' => $serviceFactory->id,
        ]);
        $this->assertDatabaseHas('mo_product_service', [
            'service_id' => $serviceFactory2->id,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de locations a productos
     *
     * Se mandan campos requeridos, con una única traducción, y el campo locations con 2 índices.
     */

    public function testOk7()
    {
        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([]);
        $this->assertDatabaseHas('mo_location', [
            'id' => $locationFactory->id,
        ]);
        $this->assertDatabaseHas('mo_location', [
            'id' => $locationFactory2->id,
        ]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => $typeFactory->id,
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 1,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'locations' => [0 => $locationFactory->id, 1 => $locationFactory2->id],
            'iva_id' => $ivaFactory->id])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_product_location', [
            'location_id' => $locationFactory->id,
        ]);
        $this->assertDatabaseHas('mo_product_location', [
            'location_id' => $locationFactory2->id,
        ]);
    }

    public function testOk8(){

        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $uomFactory = factory(App\Models\tenant\Uom::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        $companyFactory = factory(App\Models\tenant\Company::class)->create([]);

        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([]);
        $locationFactory3 = factory(App\Models\tenant\Location::class)->create([]);
        
        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('POST', '/api/v1/product', [
            'type_id' => 1,
            'product_package' => [
                0 => [
                    'id' => $product->id,
                    'location_id' => $locationFactory3->id
                ]
            ],
            'company_id' => $companyFactory->id,
            'uom_id' => $uomFactory->id,
            'code' => 'codeee',
            'pax' => 1,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'locations' => [0 => $locationFactory->id, 1 => $locationFactory2->id],
            'iva_id' => $ivaFactory->id
        ])
            ->assertExactJson(["error" => 200]);
    }
}