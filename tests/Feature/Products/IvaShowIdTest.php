<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IvaShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta tasa de iva inexistente
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/product/iva/9999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar tasa de iva sin traducciones, devuelve json vacío
     */
    public function testOk2()
    {
        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->json('GET', '/api/v1/product/iva/' . $ivaFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta tasa de iva existente
     */
    public function testOk3()
    {
        //Factory de tasa de iva y sus traducciones
        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);
        $this->assertDatabaseHas('mo_iva', [
            'id' => $ivaFactory->id,
        ]);

        $ivaTranslationFactory1 = factory(App\Models\tenant\IvaTranslation::class)->create([
            'language_id' => 1,
            'iva_id' => $ivaFactory->id,
            'name' => 'name español test',
        ]);

        $ivaTranslationFactory2 = factory(App\Models\tenant\IvaTranslation::class)->create([
            'language_id' => 2,
            'iva_id' => $ivaFactory->id,
            'name' => 'name ingles test',
        ]);

        $ivaTranslationFactory3 = factory(App\Models\tenant\IvaTranslation::class)->create([
            'language_id' => 3,
            'iva_id' => $ivaFactory->id,
            'name' => 'name portugues test',
        ]);

        $this->assertDatabaseHas('mo_iva_translation', [
            'language_id' => $ivaTranslationFactory1->language_id,
            'iva_id' => $ivaFactory->id,
            'name' => $ivaTranslationFactory1->name,
        ]);

        $this->assertDatabaseHas('mo_iva_translation', [
            'language_id' => $ivaTranslationFactory2->language_id,
            'iva_id' => $ivaFactory->id,
            'name' => $ivaTranslationFactory2->name,
        ]);

        $this->assertDatabaseHas('mo_iva_translation', [
            'language_id' => $ivaTranslationFactory3->language_id,
            'iva_id' => $ivaFactory->id,
            'name' => $ivaTranslationFactory3->name,
        ]);


        $this->json('GET', '/api/v1/product/iva/' . $ivaFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $ivaFactory->id,
                'name' => $ivaTranslationFactory1->name,
                'name' => $ivaTranslationFactory2->name,
                'name' => $ivaTranslationFactory3->name,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/product/iva/' . $ivaFactory->id, [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['iva' => ["0" => ['id', 'lang']]]]
            ]);
    }

}