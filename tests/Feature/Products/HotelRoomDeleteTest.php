<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class HotelRoomDeleteTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/product/room', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["ids" => ["The ids field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en campo id requerido
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/product/room', [
            'ids' => ['string'],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda id de usuario inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/product/room', [
            'ids' => [99999999],
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found", "error_inputs" => [
                [
                    "ids.0" => ["The selected ids.0 is invalid."],
                ]
            ]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $hotelFactory = factory(App\Models\tenant\HotelRoom::class)->create([
        ]);

        $this->assertDatabaseHas('mo_hotel_room', [
            'id' => $hotelFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/product/room', [
            'ids' => $hotelFactory->id,
        ])
            ->assertExactJson([
                "error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["ids" => ["The ids must be an array."]]]
            ]);

    }
    /**
     * Borrar un producto sin traducciones ni paquetes
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'code' => 'ABC',
        ]);

        $this->json('DELETE', '/api/v1/product/room', [
            'ids' => [$productFactory->id],
        ])->assertExactJson(['error' => 200]);

        //comprueba que en la tabla mo_hotel_room el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_hotel_room', ['deleted_at' => null, 'id' => $productFactory->id]);

    }
    /**
     * Test para eliminar un room que crea primero un room factory y sus traducciones
     */
    public function testOk2()
    {

        $productFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $translation1 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $productFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $productFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room', [
            'id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_hotel_room_translation', [
            'room_id' => $productFactory->id,
        ]);


        $this->json('DELETE', '/api/v1/product/room', [
            'ids' => [$productFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que en la tabla mo_product el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_hotel_room', ['deleted_at' => null, 'id' => $productFactory->id]);
        //comprueba que las traducciones del producto que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_hotel_room_translation', ['deleted_at' => null, 'room_id' => $translation1->product_id]);
        $this->assertDatabaseMissing('mo_hotel_room_translation', ['deleted_at' => null, 'room_id' => $translation2->product_id]);
        $this->assertDatabaseMissing('mo_hotel_room_translation', ['deleted_at' => null, 'room_id' => $translation3->product_id]);
    }
}