<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IvaShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de tasas de iva con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/product/iva/', [
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."]]]]);
    }

    /**
     * test consulta de tasas de iva con parametro lang incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/product/iva/', [

            'lang' => 'eeeeeeee',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["lang" => ["The lang is not exists"]]]]);
    }


    /**
     * test crea factory de tasas de iva con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de unidad de medida y sus traducciones
        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);

        $this->assertDatabaseHas('mo_iva', [
            'id' => $ivaFactory->id,
        ]);

        $ivaTranslationFactory1 = factory(App\Models\tenant\IvaTranslation::class)->create([
            'language_id' => 1,
            'iva_id' => $ivaFactory->id,
            'name' => 'name español test',
        ]);

        $ivaTranslationFactory2 = factory(App\Models\tenant\IvaTranslation::class)->create([
            'language_id' => 2,
            'iva_id' => $ivaFactory->id,
            'name' => 'name ingles test',
        ]);

        $ivaTranslationFactory3 = factory(App\Models\tenant\IvaTranslation::class)->create([
            'language_id' => 3,
            'iva_id' => $ivaFactory->id,
            'name' => 'name portugues test',
        ]);

        $this->assertDatabaseHas('mo_iva_translation', [
            'language_id' => $ivaTranslationFactory1->language_id,
            'iva_id' => $ivaFactory->id,
            'name' => $ivaTranslationFactory1->name,
        ]);

        $this->assertDatabaseHas('mo_iva_translation', [
            'language_id' => $ivaTranslationFactory2->language_id,
            'iva_id' => $ivaFactory->id,
            'name' => $ivaTranslationFactory2->name,
        ]);

        $this->assertDatabaseHas('mo_iva_translation', [
            'language_id' => $ivaTranslationFactory3->language_id,
            'iva_id' => $ivaFactory->id,
            'name' => $ivaTranslationFactory3->name,
        ]);


        $this->json('GET', '/api/v1/product/iva/', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $ivaFactory->id,
                'name' => $ivaTranslationFactory1->name,
                'name' => $ivaTranslationFactory2->name,
                'name' => $ivaTranslationFactory3->name,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/product/iva/', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['iva' => ["0" => ['id', 'lang']]]]
            ]);

    }


}