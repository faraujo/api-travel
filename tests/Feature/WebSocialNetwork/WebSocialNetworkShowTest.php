<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class WebSocialNetworkShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;


    /**
     * Test consulta productos con parametro lang
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/socialnetwork', [
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }
}