<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class WebSocialNetworkShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta slider inexistente sin parametros
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/socialnetwork/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta página existente sin parametros
     */
    public function testOk1()
    {

        $socialnetworFactory = factory(App\Models\tenant\WebSocialNetwork::class)->create([
            'social_network' => 'test',
        ]);

        $this->json('GET', '/api/v1/socialnetwork/' . $socialnetworFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $socialnetworFactory->id,
                'social_network' => $socialnetworFactory->social_network
            ]);
    }
}