<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class WebSocialNetworkUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campo en formato no válido
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/socialnetwork', [
            'socialnetworks' => 'redessociales'
            ])
        ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [[
                "socialnetworks" => ["The socialnetworks must be an array."],
            ]
        ]]);       
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campo en formato no válido
     */
    public function testBad2()
    {

        $this->json('PUT', '/api/v1/socialnetwork', [
            'socialnetworks' => [
                0 => [
                    'id' => 'idtest', 
                    'external_url' => 'externalurltest'
                ]
            ]])
        ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [[
                "socialnetworks.0.id" => ["The socialnetworks.0.id must be an integer."],
            ]
        ]]);       
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campo en formato no válido
     */
    public function testBad3()
    {
        $this->json('PUT', '/api/v1/socialnetwork', [
            'socialnetworks' => [
                0 => [
                    'id' => 999999999999, 
                    'external_url' => 'externalurltest'
                ]
            ]])
        ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [[
                "socialnetworks.0.id" => ["The selected socialnetworks.0.id is invalid."],
            ]
        ]]);       
    }
    
    /**
     *Test que comprueba funcionamiento
     *
     * Se mandan una red social para actualizar
     */
    public function testOk1()
    {

        $socialnetworFactory = factory(App\Models\tenant\WebSocialNetwork::class)->create([
            'social_network' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/socialnetwork', [
            'socialnetworks' => [
                0 => [
                    'id' => $socialnetworFactory->id, 
                    'external_url' => $socialnetworFactory->external_url
                ]
            ]])
        ->assertExactJson(["error" => 200]);
    }

    /**
     *Test que comprueba funcionamiento
     *
     * Se mandan dos campos de redes sociales simultáneamente, uno de ellos sin external_url (vacía el campo)
     */
    public function testOk2()
    {
        $socialnetworkFactory1 = factory(App\Models\tenant\WebSocialNetwork::class)->create([
            'social_network' => 'test1',
            'external_url' => 'externalurltest1',
        ]);
        $socialnetworkFactory2 = factory(App\Models\tenant\WebSocialNetwork::class)->create([
            'social_network' => 'test2',
            'external_url' => 'externalurltest2',
        ]);

        $this->json('PUT', '/api/v1/socialnetwork', [
            'socialnetworks' => [
                0 => [
                    'id' => $socialnetworkFactory1->id,                     
                ],
                1 => [
                    'id' => $socialnetworkFactory2->id, 
                    'external_url' => $socialnetworkFactory2->external_url
                ]
            ]])
        ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_web_social_network', [
            'id' => $socialnetworkFactory1->id,
            'social_network' => $socialnetworkFactory1->social_network,
            'external_url' => null
        ]);

        $this->assertDatabaseHas('mo_web_social_network', [
            'id' => $socialnetworkFactory2->id,
            'social_network' => $socialnetworkFactory2->social_network,
            'external_url' => $socialnetworkFactory2->external_url
        ]);
    }
}