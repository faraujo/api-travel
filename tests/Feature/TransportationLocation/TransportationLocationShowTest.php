<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

   
    /**
     * Prueba automatizada comprueba el envio del tipo de dato en los parametros
     */
    public function testBad1(){

        $this->json('GET', '/api/v1/transportationlocation', [
            'page' => 'test',
            'limit' => 'test',

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
            [
                "limit" => ["The limit must be an integer."],
                "page" => ["The page must be an integer."]
            ]
        ]]);
    }

    /**
     * Prueba automatizada ok
     */
    public function testOk1(){

        $this->json('GET', '/api/v1/transportationlocation', [

        ])->assertJsonFragment(['error' => 200]);
    }

    /**
     * Test consulta localización de transporte existente en base de datos
     */
    public function testOk2()
    {
        //crea localización de transporte
        $transportationlocationFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        $this->json('GET', '/api/v1/transportationlocation', [
        ])->assertJsonStructure(['error', 'data' => [], 'total_results'])
        ->assertJsonFragment(['id' => $transportationlocationFactory->id]);
    }
}