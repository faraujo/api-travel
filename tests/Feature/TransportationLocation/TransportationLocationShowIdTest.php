<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
   

    /**
     * Consulta localización transporte inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/transportationlocation/999999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar localización transporte existente
     */
    public function testOk2()
    {
        $transportationlocationFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);
        $this->json('GET', '/api/v1/transportationlocation/' . $transportationlocationFactory->id, [])
        ->assertExactJson(['error' => 200, 'data' => [[
            'transportationlocation' => [[
                'id' => $transportationlocationFactory->id,
                'name' => $transportationlocationFactory->name,
                'api_code' => $transportationlocationFactory->api_code,
            ]]]
        ]]);
    }
}