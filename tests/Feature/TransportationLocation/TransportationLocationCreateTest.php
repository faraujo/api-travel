<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía ningún campo
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/transportationlocation', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "name" => ["The name field is required."],
                "api_code" => ["The api code field is required."]
            ]]
            ]);
    }

    /**
     * Realiza el test de creación de localización del transporte
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK1()
    {
        $this->json('POST', '/api/v1/transportationlocation', [
            'name' => 'nombre prueba',
            'api_code' => 'código prueba'])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_transportation_location', [
            'name' => 'nombre prueba',
            'api_code' => 'código prueba'
        ]);

    }
}