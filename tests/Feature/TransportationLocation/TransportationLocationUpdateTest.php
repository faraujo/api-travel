<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TransportationLocationUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía ningún campo
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/transportationlocation', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "id" => ["The id field is required."],
                "name" => ["The name field is required."],
                "api_code" => ["The api code field is required."],
            ]]
            ]);
    }

    /**
     * Realiza el test de creación de localización de transporte
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK1()
    {

        $transportationlocationFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);

        $this->json('PUT', '/api/v1/transportationlocation', [
            'id' => $transportationlocationFactory->id,
            'name' => 'nombre prueba',
            'api_code' => 'código prueba'])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_transportation_location', [
            'id' => $transportationlocationFactory->id,
            'name' => 'nombre prueba',
            'api_code' => 'código prueba',
        ]);

    }
}