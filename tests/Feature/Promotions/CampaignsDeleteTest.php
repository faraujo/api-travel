<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class CampaignsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;


    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/promotion/campaign', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/promotion/campaign', [
            'id' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids" => ["The ids field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/promotion/campaign', [
            'ids' => [5000000000000]
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                "error_inputs" => [["ids.0" => ["The selected ids.0 is invalid."]]]
            ]);
    }


    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([
        ]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/promotion/campaign', [
            'ids' => $campaignFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Test para eliminar una campaña que crea primero una campaña factory y sus traducciones
     */
    public function testOk1()
    {
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
        ]);

        $campaignTranslationFactory1 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name español test 2',
            'description' => 'description español test 2',
        ]);

        $campaignTranslationFactory2 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 2,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name ingles test 2',
            'description' => 'description ingles test 2',
        ]);

        $campaignTranslationFactory3 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 3,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name portugues test 2',
            'description' => 'description portugues test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory2->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory2->name,
            'description' => $campaignTranslationFactory2->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory3->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory3->name,
            'description' => $campaignTranslationFactory3->description,
        ]);


        $this->json('DELETE', '/api/v1/promotion/campaign', [
            'ids' => [$campaignFactory1->id]
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_campaign el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_campaign', ['deleted_at' => null, 'id' => $campaignFactory1->id]);
        //Comprueba que las traducciones de la campaña que han sido creadas no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory1->campaign_id]);
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory2->campaign_id]);
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory3->campaign_id]);
    }

    /**
     * Borrar sin traducciones
     */
    public function testOk2()
    {
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
        ]);

        $this->json('DELETE', '/api/v1/promotion/campaign', [
            'ids' => [$campaignFactory1->id]
        ])->assertExactJson(['error' => 200]);

        //Comprueba que en la tabla mo_campaign el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_campaign', ['deleted_at' => null, 'id' => $campaignFactory1->id]);

    }

    /**
     * Test para eliminar dos campañas,  crea primero dos campañas factory y sus traducciones
     */
    public function testOk3()
    {
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);
        $campaignFactory2 = factory(App\Models\tenant\Campaign::class)->create([]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
        ]);
        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory2->id,
        ]);

        $campaignTranslationFactory1 = factory(App\Models\tenant\CampaignTranslation::class)->create([
        'language_id' => 1,
        'campaign_id' => $campaignFactory1->id,
        'name' => 'name español test ',
        'description' => 'description español test ',
    ]);

        $campaignTranslationFactory2 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 2,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name ingles test ',
            'description' => 'description ingles test ',
        ]);

        $campaignTranslationFactory3 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 3,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name portugues test ',
            'description' => 'description portugues test ',
        ]);

        $campaignTranslationFactory4 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory2->id,
            'name' => 'name español test 2',
            'description' => 'description español test 2',
        ]);

        $campaignTranslationFactory5 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 2,
            'campaign_id' => $campaignFactory2->id,
            'name' => 'name ingles test 2',
            'description' => 'description ingles test 2',
        ]);

        $campaignTranslationFactory6 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 3,
            'campaign_id' => $campaignFactory2->id,
            'name' => 'name portugues test 2',
            'description' => 'description portugues test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory2->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory2->name,
            'description' => $campaignTranslationFactory2->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory3->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory3->name,
            'description' => $campaignTranslationFactory3->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory4->language_id,
            'campaign_id' => $campaignFactory2->id,
            'name' => $campaignTranslationFactory4->name,
            'description' => $campaignTranslationFactory4->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory5->language_id,
            'campaign_id' => $campaignFactory2->id,
            'name' => $campaignTranslationFactory5->name,
            'description' => $campaignTranslationFactory5->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory6->language_id,
            'campaign_id' => $campaignFactory2->id,
            'name' => $campaignTranslationFactory6->name,
            'description' => $campaignTranslationFactory6->description,
        ]);


        $this->json('DELETE', '/api/v1/promotion/campaign', [
            'ids' => [$campaignFactory1->id , $campaignFactory2->id]
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_campaign los registros con id que han sido creados no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_campaign', ['deleted_at' => null, 'id' => $campaignFactory1->id]);
        $this->assertDatabaseMissing('mo_campaign', ['deleted_at' => null, 'id' => $campaignFactory2->id]);
        //Comprueba que las traducciones de la campaña que han sido creadas no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory1->campaign_id]);
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory2->campaign_id]);
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory3->campaign_id]);
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory4->campaign_id]);
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory5->campaign_id]);
        $this->assertDatabaseMissing('mo_campaign_translation', ['deleted_at' => null, 'campaign_id' => $campaignTranslationFactory6->campaign_id]);
    }

}