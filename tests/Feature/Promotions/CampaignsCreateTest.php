<?php

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CampaignsCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda ninguno de los campos requeridos
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/promotion/campaign', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "apply_filter_subchannel"=>["The apply filter subchannel field is required."],
                    "date_end"=>["The date end field is required."],
                    "date_start"=>["The date start field is required."],
                    "translation"=>["you need at least one translation"]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se intenta enviar un campo name de un idioma y el campo description en otro idioma
     */
    public function testBad2()
    {
        $fecha = Carbon::now();
        $this->json('POST', '/api/v1/promotion/campaign', [

            'date_start' => $fecha->addYear()->format('Y-m-d'),
            'date_end' => $fecha->addYears(2)->format('Y-m-d'),
            'apply_filter_subchannel' => 0,
            'name' => ['ES' => 'Cualquiera Español'],
            'description' => ['EN' => 'Descripcion inglés']])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["description.ES" => ["The description. e s field is required."], 'name.EN' => ['The name. e n field is required.']]]]);
    }

    public function testBad3()
    {
        $fecha = Carbon::now();
        $this->json('POST', '/api/v1/promotion/campaign', [

            'date_start' => $fecha->addYear()->format('Y-m-d'),
            'date_end' => $fecha->addYears(2)->format('Y-m-d'),
            'apply_filter_subchannel' => 0,
            'description' => ['EN' => 'Descripcion inglés']])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [['name.EN' => ['The name. e n field is required.']]]]);
    }

    /**
     *Test que comprueba funcionamiento de la validacion del parametro active
     *
     * No se manda ninguno de los campos requeridos
     */
    public function testBad5()
    {
        $fecha = Carbon::now();
        $this->json('POST', '/api/v1/promotion/campaign', [
            'active' => 43,
            'date_start' => $fecha->addYear()->format('Y-m-d'),
            'date_end' => $fecha->addYears(2)->format('Y-m-d'),
            'apply_filter_subchannel' => 0,
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "translation" => ["you need at least one translation"],
                    "active" => ["The active field must be true or false."]
                    ]]]);
    }


    /**
     * Realiza el test de creación de una campaña con sus tres traducciones
     * Comprueba que se han creado las traducciones en base de datos
     */
    public function testOK1()
    {
        $fecha = Carbon::now();
        $this->json('POST', '/api/v1/promotion/campaign', [
            'active' => 1,
            'date_start' => $fecha->addYear()->format('Y-m-d'),
            'date_end' => $fecha->addYears(2)->format('Y-m-d'),
            'apply_filter_subchannel' => 0,
            'name' => ['ES' => 'campaña españa', 'EN' => 'campaña Inglés', 'PT' => 'Campaña portugues'],
            'description' => ['ES' => 'Descripcion campaña español', 'EN' => 'Descripcion campaña inglés', 'PT' => 'Descripción campaña portugués']])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_campaign', [
            'active' => 1,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 1,
            'name' => 'campaña españa',
            'description' => 'Descripcion campaña español',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 2,
            'name' => 'campaña Inglés',
            'description' => 'Descripcion campaña inglés',

        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 3,
            'name' => 'Campaña portugues',
            'description' => 'Descripción campaña portugués',
        ]);
    }

    /**
     * Realiza el test de creación de una campaña con una traducción
     * Comprueba que se ha creado la traducción en base de datos
     */
    public function testOK2()
    {
        $fecha = Carbon::now();
        $this->json('POST', '/api/v1/promotion/campaign', [
            'name' => ['ES' => 'campaña español test2'],
            'description' => ['ES' => 'Descripcion español test 2'],
            'apply_filter_subchannel' => 0,
            'date_start' => $fecha->addYear()->format('Y-m-d'),
            'date_end' => $fecha->addYears(2)->format('Y-m-d')])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 1,
            'name' => 'campaña español test2',
            'description' => 'Descripcion español test 2',
        ]);
    }


}