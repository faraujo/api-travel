<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Carbon\Carbon;


class CampaignsUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda id a modificar
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/promotion/campaign', [])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["apply_filter_subchannel"=>["The apply filter subchannel field is required."],"date_end"
            =>["The date end field is required."],"date_start"=>["The date start field is required."],"id"=>["The id field is required."],"translation"=>["you need at least one translation"]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se envía todos campos posibles en formato incorrecto
     */
    public function testBad2()
    {

        $this->json('PUT', '/api/v1/promotion/campaign', [

            'id' => 'r',
            'name' => ['ES' => 'Cualquiera Español'],
            'description' => ['ES' => 'Descripcion ESPAÑOL'],
            'apply_filter_subchannel' => 'r',
            'date_start' => '2021/12/09',
            'date_end' => '2021/12/20'])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["apply_filter_subchannel"=>["The apply filter subchannel field must be true or false."],
                "date_end"=>["The date end does not match the format Y-m-d."],"date_start"=>["The date start does not match the format Y-m-d."],"id"=>["The id must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se envía id inexistente en base de datos
     */
    public function testBad3()
    {

        $this->json('PUT', '/api/v1/promotion/campaign', [

            'id' => 99999999,
            'name' => ['ES' => 'Cualquiera Español'],
            'description' => ['ES' => 'Descripcion ESPAÑOL'],
            'apply_filter_subchannel' => 0,
            'date_start' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(5)->format('Y-m-d')])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found"]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se intenta enviar un campo name de un idioma y el campo description en otro idioma
     */
    public function testBad4()
    {
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $this->json('PUT', '/api/v1/promotion/campaign', [

            'id' => $campaignFactory1->id,
            'name' => ['ES' => 'Cualquiera Español'],
            'description' => ['EN' => 'Descripcion inglés'],
            'apply_filter_subchannel' => 0,
            'date_start' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(5)->format('Y-m-d')])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["description.ES" => ["The description. e s field is required."], 'name.EN' => ['The name. e n field is required.']]]]);
    }

    /**
     *Test que comprueba funcionamiento de la validación del campo active
     *
     * No se manda id a modificar
     */
    public function testBad5()
    {
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create(['active' => 0]);
        $this->json('PUT', '/api/v1/promotion/campaign', [
            'id' => $campaignFactory1->id,
            'name' => ['ES' => 'campaña modificada test 1'],
            'description' => ['ES' => 'Descripcion modificada test 1'],
            'apply_filter_subchannel' => 0,
            'date_start' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(5)->format('Y-m-d'),
            'active' => 27,
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "active" => ["The active field must be true or false."],
                ]]]);
    }


    /**
     * Realiza el test de actualización de una campaña con una traduccion
     * Comprueba que se ha modificado correctamente en base de datos
     */
    public function testOK1()
    {
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create(['active' => 0]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
        ]);

        $campaignTranslationFactory1 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name español test 1',
            'description' => 'description español test 1',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
        ]);

        $this->json('PUT', '/api/v1/promotion/campaign', [
            'id' => $campaignFactory1->id,
            'active' => 0,
            'name' => ['ES' => 'campaña modificada test 1'],
            'description' => ['ES' => 'Descripcion modificada test 1'],
            'apply_filter_subchannel' => 0,
            'date_start' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(5)->format('Y-m-d')])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
            'active' => 0,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'campaña modificada test 1',
            'description' => 'Descripcion modificada test 1',
        ]);

    }

    /**
     * Realiza el test de creación de una campaña con sus tres traducciónes
     * Comprueba que se han actualizado las traducciones en base de datos
     */
    public function testOK2()
    {
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
        ]);

        $campaignTranslationFactory1 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name español test 2',
            'description' => 'description español test 2',
        ]);

        $campaignTranslationFactory2 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 2,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name ingles test 2',
            'description' => 'description ingles test 2',
        ]);

        $campaignTranslationFactory3 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 3,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name portugues test 2',
            'description' => 'description portugues test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory2->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory2->name,
            'description' => $campaignTranslationFactory2->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory3->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory3->name,
            'description' => $campaignTranslationFactory3->description,
        ]);

        $this->json('PUT', '/api/v1/promotion/campaign', [
            'id' => $campaignFactory1->id,
            'name' => ['ES' => 'campaña español modificada test2', 'EN' => 'campaña ingles modificada test2', 'PT' => 'campaña portugues modificada test2'],
            'description' => ['ES' => 'Descripcion español modificada test 2', 'EN' => 'Descripcion ingles modificada test 2', 'PT' => 'Descripcion portugues modificada test 2'],
            'apply_filter_subchannel' => 0,
            'date_start' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(5)->format('Y-m-d')])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'campaña español modificada test2',
            'description' => 'Descripcion español modificada test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 2,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'campaña ingles modificada test2',
            'description' => 'Descripcion ingles modificada test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 3,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'campaña portugues modificada test2',
            'description' => 'Descripcion portugues modificada test 2',
        ]);
    }

    /**
     * Prueba automatizada correcta, intenta actualizar traduccion de campaña, debe crearla y borrar la traducción de otro idioma que existiera
     */
    function testOk3()
    {

        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
        ]);

        $campaignTranslationFactory1 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name español test 2',
            'description' => 'description español test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
        ]);


        $this->json('PUT', '/api/v1/promotion/campaign', [
            'id' => $campaignFactory1->id,
            'name' => ['EN' => 'campaña INGLES modificada test2'],
            'description' => ['EN' => 'Descripcion INGLES modificada test 2'],
            'apply_filter_subchannel' => 0,
            'date_start' => Carbon::now()->addDays(2)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(5)->format('Y-m-d')])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que crea nueva traducción en inglés
        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => 2,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'campaña INGLES modificada test2',
            'description' => 'Descripcion INGLES modificada test 2',
        ]);

        //comprueba que la traducción en español que había se borra
        $this->assertDatabaseMissing('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
            'deleted_at' => null
        ]);


    }


}