<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class PromotionsShowTypeIdTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta tipo promoción inexistente sin filtro
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/promotion/types/999')
            ->assertExactJson(['error' => 200, 'data' => [['type' => []]]]);
    }

    /**
     * Consultar tipo promoción con filtro lang erróneo
     */
    public function testBad2()
    {
        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([
        ]);

        factory(App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => 1,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);
        factory(App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => 2,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);
        factory(App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => 3,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $this->json('GET', '/api/v1/promotion/types/' . $promotionTypeFactory->id, [
            'lang' => 'FDF'])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Consulta tipo promoción existente sin filtro lang
     */
    public function testOK1()
    {
        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([
        ]);

        $translation1 = factory(App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => 1,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => 2,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => 3,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);
        $this->json('GET', '/api/v1/promotion/types/' . $promotionTypeFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $promotionTypeFactory->id,
                'name' => $translation1->name,
            ]);
    }

    /**
     * Consulta tipo promoción existente con filtro lang EN
     */
    public function testOK2()
    {
        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([
        ]);
        $translation = factory(App\Models\tenant\PromotionTypeTranslation::class)->create([
            'language_id' => 2,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $this->json('GET', '/api/v1/promotion/types/' . $promotionTypeFactory->id, [
            'lang' => 'EN'])->assertJsonFragment([
            'error' => 200,
            'id' => $promotionTypeFactory->id,
            'name' => $translation->name,
        ]);
    }

}