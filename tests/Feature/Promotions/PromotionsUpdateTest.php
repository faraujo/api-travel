<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Carbon\Carbon;

class PromotionsUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    //inicio pruebas erróneas


    //de validacion solo pasa esta prueba para ver que pide el id
    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda ningún requerido
     */
    public function testBad()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["apply_client_type_filter"=>["The apply client type filter field is required."],
                    "apply_country_filter"=>["The apply country filter field is required."],"apply_currency_filter"=>["The apply currency filter field is required."],
                    "apply_device_filter"=>["The apply device filter field is required."],"apply_language_filter"=>["The apply language filter field is required."],
                    "apply_payment_method_filter"=>["The apply payment method filter field is required."],
                    "apply_pickup_filter"=>["The apply pickup filter field is required."],"apply_product_filter"=>["The apply product filter field is required."],
                    "apply_product_type_filter"=>["The apply product type filter field is required."],"apply_service_filter"=>["The apply service filter field is required."],
                    "apply_state_filter"=>["The apply state filter field is required."],"apply_subchannel_filter"=>["The apply subchannel filter field is required."],
                    "campaign_id"=>["The campaign id field is required."],"code"=>["The code field is required."],"id"=>["The id field is required."],
                    "department_id" => ["The department id field is required."],
                    "cost_center_id" => ["The cost center id field is required."],
                    "promotion_type_id"=>["The promotion type id field is required."],"translation"=>["you need at least one translation"]]]]);
    }

    //fin pruebas erróneas


    //pruebas resultado correcto

    /**
     *Test ok envia sólo requeridos con varios pickup en el array pickups
     *
     */
    public function testOk2()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $pickupFactory = factory(App\Models\tenant\TransportationLocation::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionPickupFactory = factory(App\Models\tenant\PromotionPickup::class)->create([
            'pickup_id' => $pickupFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_transportation_location', [
            'id' => $pickupFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_pickup', [
            'id' => $promotionPickupFactory->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $pickupFactory1 = factory(App\Models\tenant\TransportationLocation::class)->create([]);
        $pickupFactory2 = factory(App\Models\tenant\TransportationLocation::class)->create([]);

        //fin comprueba creación de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'code',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'pickups' => [
                '0' => [
                    'id' => $pickupFactory1->id],
                '1' => [
                    'id' => $pickupFactory2->id],],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 1,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_pickup', [
            'id' => $promotionPickupFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_pickup', [
            'promotion_id' => $promotionFactory->id,
            'pickup_id' => $pickupFactory1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_pickup', [
            'promotion_id' => $promotionFactory->id,
            'pickup_id' => $pickupFactory2->id,
            'deleted_at' => null,
        ]);
        //fin comprueba modificaciones

    }

    /**
     *Test ok envia sólo requeridos con varios product en el array products
     *
     */
    public function testOk4()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $productFactory2 = factory(App\Models\tenant\Product::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionProduct::class)->create([
            'product_id' => $productFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionProductFactory2 = factory(App\Models\tenant\PromotionProduct::class)->create([
            'product_id' => $productFactory2->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factpories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory2->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'code',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0],
                '1' => [
                    'id' => $productFactory2->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 1,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con todos los parámetros posibles en el array products
     *
     */
    public function testOk5()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionProduct::class)->create([
            'product_id' => $productFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'product_id' => $productFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creacion de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'asas',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'products' => [
                '0' => [
                    'id' => $productFactory->id, 'min_quantity' => 2,'max_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => 1,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 1,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'product_id' => $productFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 2,
            'max_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => 1,
            'deleted_at' => null,
        ]);

    }

    /**
     *Test ok envia sólo requeridos con varios product types en el array product_types
     *
     */
    public function testOk7()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionProductTypeFactory = factory(App\Models\tenant\PromotionProductType::class)->create([
            'product_type_id' => $productTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_product_type', [
            'id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factopries
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'id' => $promotionFactory->id,
            'code' => 'asas',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'product_types' => [
                '0' => [
                    'id' => 1,
                    'apply_client_type_filter' => 0,],
                '1' => [
                    'id' => 2,
                    'apply_client_type_filter' => 0,]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => 1,
            'promotion_id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => 2,
            'promotion_id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con todos los parámetros posibles en el array product_types
     *
     */
    public function testOk8()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionProductTypeFactory = factory(App\Models\tenant\PromotionProductType::class)->create([
            'product_type_id' => $productTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_product_type', [
            'id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'id' => $promotionFactory->id,
            'code' => 'asas',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'product_types' => [
                '0' => [
                    'id' => 4, 'min_quantity' => 2,'max_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => 1,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => 4,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 2,'max_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios service en el array services
     *
     */
    public function testOk10()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionServiceFactory = factory(App\Models\tenant\PromotionService::class)->create([
            'service_id' => $serviceFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionServiceFactory2 = factory(App\Models\tenant\PromotionService::class)->create([
            'service_id' => $serviceFactory2->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory2->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'code',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => $serviceFactory->id,
                    'promotion_amount' => 0,
                    'apply_client_type_filter' => 0
                ],
                '1' => [
                    'id' => $serviceFactory2->id,
                    'promotion_amount' => 0,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_service', [
            'id' => $promotionServiceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'promotion_id' => $promotionFactory->id,
            'service_id' => $serviceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'promotion_id' => $promotionFactory->id,
            'service_id' => $serviceFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con todos los parámetros posibles en el array services
     *
     */
    public function testOk11()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionServiceFactory = factory(App\Models\tenant\PromotionService::class)->create([
            'service_id' => $serviceFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion_service', [
            'id' => $promotionServiceFactory->id,
            'service_id' => $serviceFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_amount' => 1,
            'promotion_type_id' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'asas',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'services' => [
                '0' => [
                    'id' => $serviceFactory->id, 'min_quantity' => 2,'max_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => 1,
                    'apply_client_type_filter' => 0,]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_service_filter' => 1,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_service', [
            'id' => $promotionServiceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'service_id' => $serviceFactory->id,
            'promotion_id' => $promotionFactory->id,
            'min_quantity' => 2,'max_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios countries en el array countries
     *
     */
    public function testOk13()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionCountryFactory = factory(App\Models\tenant\PromotionCountry::class)->create([
            'country_id' => 143,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionCountryFactory2 = factory(App\Models\tenant\PromotionCountry::class)->create([
            'country_id' => 144,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba crfeación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_country', [
            'id' => 143,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_country', [
            'id' => 144,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_country', [
            'id' => $promotionCountryFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_country', [
            'id' => $promotionCountryFactory2->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'code',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'countries' => [
                '0' => [
                    'id' => 4],
                '1' => [
                    'id' => 5]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 1,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_country', [
            'id' => $promotionCountryFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_country', [
            'promotion_id' => $promotionFactory->id,
            'country_id' => 4,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_country', [
            'promotion_id' => $promotionFactory->id,
            'country_id' => 5,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios states en el array states
     *
     */
    public function testOk15()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $stateFactory = factory(App\Models\tenant\State::class)->create([
        ]);

        $stateFactory2 = factory(App\Models\tenant\State::class)->create([
        ]);

        $promotionStateFactory = factory(App\Models\tenant\PromotionState::class)->create([
            'state_id' => $stateFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionStateFactory2 = factory(App\Models\tenant\PromotionState::class)->create([
            'state_id' => $stateFactory2->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba crfeación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_country', [
            'id' => 143,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_country', [
            'id' => 144,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_state', [
            'id' => $promotionStateFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_state', [
            'id' => $promotionStateFactory2->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'code',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'states' => [
                '0' => [
                    'id' => $stateFactory->id],
                '1' => [
                    'id' => $stateFactory2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 1,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_state', [
            'id' => $promotionStateFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_state', [
            'promotion_id' => $promotionFactory->id,
            'state_id' => $stateFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_state', [
            'promotion_id' => $promotionFactory->id,
            'state_id' => $stateFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios subchannels en el array subchannels
     *
     */
    public function testOk17()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $subchannelFactory_2 = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'subchannel_id' => $subchannelFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionChannelFactory2 = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'subchannel_id' => $subchannelFactory_2->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_subchannel', [
            'id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_subchannel', [
            'id' => $subchannelFactory_2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creacióin de factories
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'code',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'subchannels' => [
                '0' => [
                    'id' => $subchannelFactory->id],
                '1' => [
                    'id' => $subchannelFactory_2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 1,
            'apply_product_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory_2->id,
            'deleted_at' => null,
        ]);
    }


    /**
     *Test ok envia requeridos y modifica filtro apply_product_filter para asignar promotion a todos los product
     *
     */
    public function testOk18()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'apply_product_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'apply_product_filter' => 0,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factopries
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'asas',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [null],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'apply_product_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok modifica campos de tipo fecha
     *
     */
    public function testOk19()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'apply_product_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => 'antes modificar español',
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => 'antes modificar ingles',
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => 'antes modificar portugues',
            'language_id' => 3,
        ]);
        //comprueba en tabla creación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'apply_product_filter' => 0,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'gift' => 'antes modificar español',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'gift' => 'antes modificar ingles',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'gift' => 'antes modificar portugues',
            'deleted_at' => null,
        ]);
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba en tabla creación de factories
        $this->json('PUT', '/api/v1/promotion', [
            'id' => $promotionFactory->id,
            'user_id' => $user_factory->id,

            'code' => 'asas',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [0 => ['id' => $productFactory->id,
                'apply_client_type_filter' => 0]],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'apply_product_filter' => 1,
            'deleted_at' => null,
        ]);
        //fin comprueba modificaciones
    }

    /**
     *Test ok modifica requeridos y campos booleanos y de cupones
     *
     */
    public function testOk20()
    {

        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'code',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'apply_product_filter' => 0,
            'min_quantity' => 2,
            'max_quantity' => 2,
            'anticipation_start' => 10,
            'anticipation_end' => 15,
            'coupon_type' => 0,
            'coupon_code' => 'code_coupon',
            'coupon_sheet_start' => 0,
            'coupon_sheet_end' => 90,
            'coupon_total_uses' => 10,
            'campaign_id' => $campaignFactory1->id,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba creación de factories en tablas
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'code' => 'code',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'apply_product_filter' => 0,
            'min_quantity' => 2,
            'max_quantity' => 2,
            'anticipation_start' => 10,
            'anticipation_end' => 15,
            'coupon_type' => 0,
            'coupon_code' => 'code_coupon',
            'coupon_sheet_start' => 0,
            'coupon_sheet_end' => 90,
            'coupon_total_uses' => 10,
            'deleted_at' => null,
            'campaign_id' => $campaignFactory1->id,

        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $campaignFactory2 = factory(App\Models\tenant\Campaign::class)->create([]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        //fin comprueba creación de factories en tablas
        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 1,
            'accumulate' => 0,
            'promotion_amount' => 20,
            'promotion_type_id' => 1,
            'apply_product_filter' => 1,
            'products' => [0 => ['id' => $productFactory->id,
                'apply_client_type_filter' => 0]],
            'min_quantity' => 2,'max_quantity' => 2,
            'anticipation_start' => 10,
            'anticipation_end' => 15,
            'coupon_type' => 1,
            'coupon_code' => 'modificado',
            'coupon_sheet_start' => 1,
            'coupon_sheet_end' => 99,
            'coupon_total_uses' => 20,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'campaign_id' => $campaignFactory2->id,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);
        //comprueba modificaciones en tablas
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 1,
            'accumulate' => 0,
            'promotion_amount' => 20,
            'promotion_type_id' => 1,
            'apply_product_filter' => 1,
            'min_quantity' => 2,'max_quantity' => 2,
            'anticipation_start' => 10,
            'anticipation_end' => 15,
            'coupon_type' => 1,
            'coupon_code' => 'modificado',
            'coupon_sheet_start' => 1,
            'coupon_sheet_end' => 99,
            'coupon_total_uses' => 20,
            'deleted_at' => null,
            'campaign_id' => $campaignFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'name' => 'name',
            'description' => 'description ES',
            'short_description' => 'short_description ES',
            'title_seo' => 'title_Seo ES',
            'description_seo' => 'DESCRIPTION_SEO ES',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'name' => 'NAME EN',
            'description' => 'description EN',
            'short_description' => 'short_description EN',
            'title_seo' => 'title_Seo EN',
            'description_seo' => 'DESCRIPTION_SEO EN',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'name' => 'NAME PT',
            'description' => 'description PT',
            'short_description' => 'short_description PT',
            'title_seo' => 'title_Seo PT',
            'description_seo' => 'DESCRIPTION_SEO PT',
            'deleted_at' => null,
        ]);
        //fin comprueba modificaciones en tablas

    }

    /**
     * Se intenta enviar array de monedas cuando no se le permite al estar apply_currency_filter con valor 0
     */
    public function testOk21()
    {

        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 4,
            'campaign_id' => $campaignFactory1->id,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 3,
        ]);
        //comprueba creación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'campaign_id' => $campaignFactory1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $campaignFactory2 = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);


        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'legal' => ['ES' => 'Legal en ES', 'EN' => 'Legan en EN', 'PT' => 'Legal en PT'],
            'observations' => ['ES' => 'Observations en ES', 'EN' => 'Observations en EN', 'PT' => 'Observations en PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_service_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])->assertExactJson([
            'error' => 200,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación de promociones a files
     *
     * Se crea factory de files, se hace factory de promotion_file, al mandar relación con diferente file, borra las relaciones anteriores y crea las nuevas
     */
    public function testOk22()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
            'name' => 'name file'
        ]);
        $this->assertDatabaseHas('mo_file', [
            'url_file' => $fileFactory->url_file,
        ]);

        $this->assertDatabaseHas('mo_file_translation', [
            'name' => $fileFactoryTranslation->name,
        ]);

        //se crea factory con relación de product y category
        $promotionFileFactory = factory(App\Models\tenant\PromotionFile::class)->create([
            'promotion_id' => $promotionFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion_file', [
            'promotion_id' => $promotionFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        //se crea nueva file para hacer nueva asignación y comprobar que borra la anterior
        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('nueva.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory2->id,
        ]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotionFactory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'files' => [0 => $fileFactory2->id]])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_promotion_file', [
            'file_id' => $fileFactory2->id,
        ]);

        $this->assertDatabaseMissing('mo_promotion_file', [
            'file_id' => $fileFactory->id,
            'deleted_at' => null,
        ]);

    }

    /**
     * Test comprueba la modificación de los campos apply_client_type_filter e inserción de datos en las tablas asociadas de promociones con promotion_client_type
     */
    public function testOk23()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $promotion_type_factory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'code' => 'Test',
            'order' => 1,
            'visibility' => 1,
            'accumulate' => 1,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_type_id' => $promotion_type_factory->id,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([
            'code' => 'test',
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotion_factory->id,
            'code' => $promotion_factory->code,
            'visibility' => $promotion_factory->visibility,
            'accumulate' => $promotion_factory->accumulate,
            'min_quantity' => $promotion_factory->min_quantity,
            'max_quantity' => $promotion_factory->max_quantity,
            'promotion_type_id' => $promotion_type_factory->id,
            'deleted_at' => null,
        ]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);


        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotion_factory->id,
            'code' => 'test',
            'promotion_type_id' => $promotion_type_factory->id,
            'campaign_id' => $campaignFactory->id,
            'apply_client_type_filter' => 1,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,

            'apply_pickup_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_product_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_service_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_device_filter' => 0,
            'client_types' => [
                0 => ['id' => $client_type_factory->id]],
            'name' => ['ES' => 'test'],
            'description_seo' => ['ES' => 'test'],
            'short_description' => ['ES' => 'test'],
            'title_seo' => ['ES' => 'test'],
            'description' => ['ES' => 'test'],
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotion_factory->id,
            'code' => 'test',
            'apply_client_type_filter' => 1,
            'deleted_at' => null]);

        $this->assertDatabaseHas('mo_promotion_client_type', [
            'promotion_id' => $promotion_factory->id,
            'client_type_id' => $client_type_factory->id,
            'deleted_at' => null]);
    }

    /**
     * Test comprueba la modificación de los campos apply_client_type_filter en la tabla mo_promotion_product y la inserción de registros client_type en tabla mo_promotion_product_client_type
     */

    public function testOk24()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $promotion_type_factory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'code' => 'Test',
            'order' => 1,
            'visibility' => 1,
            'accumulate' => 1,
            'min_quantity' => 1,'max_quantity' => 1,
            'promotion_type_id' => $promotion_type_factory->id,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([
            'code' => 'test',
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotion_factory->id,
            'code' => $promotion_factory->code,
            'visibility' => $promotion_factory->visibility,
            'accumulate' => $promotion_factory->accumulate,
            'min_quantity' => $promotion_factory->min_quantity,'max_quantity' => $promotion_factory->max_quantity,
            'promotion_type_id' => $promotion_type_factory->id,
            'deleted_at' => null,
        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotion_factory->id,
            'code' => 'test',
            'promotion_type_id' => $promotion_type_factory->id,
            'campaign_id' => $campaignFactory->id,
            'apply_client_type_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 0,
            'apply_service_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                0 => [
                    'id' => $product_factory->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => [
                        0 => ['id' => $client_type_factory->id],
                    ],
                ]
            ],
            'name' => ['ES' => 'test'],
            'description_seo' => ['ES' => 'test'],
            'short_description' => ['ES' => 'test'],
            'title_seo' => ['ES' => 'test'],
            'description' => ['ES' => 'test'],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'promotion_id' => $promotion_factory->id,
            'product_id' => $product_factory->id,
            'apply_client_type_filter' => 1,
            'deleted_at' => null]);

        $promotion_product = \App\Models\tenant\PromotionProduct::where('promotion_id', $promotion_factory->id)->where('product_id', $product_factory->id)->where('deleted_at', null)->first();

        $this->assertDatabaseHas('mo_promotion_product_client_type', [
            'promotion_product_id' => $promotion_product->id,
            'client_type_id' => $client_type_factory->id,
            'deleted_at' => null]);
    }

    /**
     * Test comprueba la modificación de los campos apply_client_type_filter en la tabla mo_promotion_product_type y la inserción de registros client_type en tabla mo_promotion_product_type_client_type
     */

    public function testOk25()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $promotion_type_factory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'code' => 'Test',
            'order' => 1,
            'visibility' => 1,
            'accumulate' => 1,
            'min_quantity' => 1,'max_quantity' => 1,
            'promotion_type_id' => $promotion_type_factory->id,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([
            'code' => 'test',
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotion_factory->id,
            'code' => $promotion_factory->code,
            'visibility' => $promotion_factory->visibility,
            'accumulate' => $promotion_factory->accumulate,
            'min_quantity' => $promotion_factory->min_quantity,
            'max_quantity' => $promotion_factory->max_quantity,
            'promotion_type_id' => $promotion_type_factory->id,
            'deleted_at' => null,
        ]);

        $product_type_factory = factory(\App\Models\tenant\ProductType::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotion_factory->id,
            'code' => 'test',
            'promotion_type_id' => $promotion_type_factory->id,
            'campaign_id' => $campaignFactory->id,
            'apply_client_type_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,

            'apply_pickup_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_product_filter' => 0,
            'apply_product_type_filter' => 1,
            'apply_service_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_device_filter' => 0,
            'product_types' => [
                0 => [
                    'id' => $product_type_factory->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => [
                        0 => ['id' => $client_type_factory->id],
                    ],
                ]
            ],
            'name' => ['ES' => 'test'],
            'description_seo' => ['ES' => 'test'],
            'short_description' => ['ES' => 'test'],
            'title_seo' => ['ES' => 'test'],
            'description' => ['ES' => 'test'],
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'promotion_id' => $promotion_factory->id,
            'product_type_id' => $product_type_factory->id,
            'apply_client_type_filter' => 1,
            'deleted_at' => null]);

        $promotion_product_type = \App\Models\tenant\PromotionProductType::where('promotion_id', $promotion_factory->id)->where('product_type_id', $product_type_factory->id)->where('deleted_at', null)->first();

        $this->assertDatabaseHas('mo_promotion_product_type_client_type', [
            'promotion_product_type_id' => $promotion_product_type->id,
            'client_type_id' => $client_type_factory->id,
            'deleted_at' => null]);
    }

    /**
     * Test comprueba la moficación de los campos apply_client_type_filter en la tabla mo_promotion_service y la inserción de registros client_type en tabla mo_promotion_service_client_type
     */
    public function testOk26()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $promotion_type_factory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([
            'code' => 'Test',
            'order' => 1,
            'visibility' => 1,
            'accumulate' => 1,
            'min_quantity' => 1,
            'max_quantity' => 1,
            'promotion_type_id' => $promotion_type_factory->id,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $client_type_factory = factory(\App\Models\tenant\ClientType::class)->create([
            'code' => 'test',
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotion_factory->id,
            'code' => $promotion_factory->code,
            'visibility' => $promotion_factory->visibility,
            'accumulate' => $promotion_factory->accumulate,
            'min_quantity' => $promotion_factory->min_quantity,
            'max_quantity' => $promotion_factory->max_quantity,
            'promotion_type_id' => $promotion_type_factory->id,
            'deleted_at' => null,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'id' => $promotion_factory->id,
            'code' => 'test',
            'promotion_type_id' => $promotion_type_factory->id,
            'campaign_id' => $campaignFactory->id,
            'apply_client_type_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,

            'apply_pickup_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_product_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_service_filter' => 1,
            'apply_device_filter' => 0,
            'services' => [
                0 => [
                    'id' => $service_factory->id,
                    'apply_client_type_filter' => 1,
                    'client_types' =>
                        [0 => ['id' => $client_type_factory->id]],
                ]
            ],
            'name' => ['ES' => 'test'],
            'description_seo' => ['ES' => 'test'],
            'short_description' => ['ES' => 'test'],
            'title_seo' => ['ES' => 'test'],
            'description' => ['ES' => 'test'],
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'promotion_id' => $promotion_factory->id,
            'service_id' => $service_factory->id,
            'apply_client_type_filter' => 1,
            'deleted_at' => null]);

        $promotion_service = \App\Models\tenant\PromotionService::where('promotion_id', $promotion_factory->id)->where('service_id', $service_factory->id)->where('deleted_at', null)->first();

        $this->assertDatabaseHas('mo_promotion_service_client_type', [
            'promotion_service_id' => $promotion_service->id,
            'client_type_id' => $client_type_factory->id,
            'deleted_at' => null]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de rangos de fechas a promociones
     *
     * Se asigna promocion a los diferentes rangos
     */
    public function testOk27()
    {

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);

        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ]);

        $promotionTranslation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 1,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 2,
        ]);
        $promotionTranslation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'gift' => null,
            'language_id' => 3,
        ]);
        //comprueba en tablas creación de factories
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation1->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation3->id,
            'deleted_at' => null,
        ]);

        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([
            'date_start' => Carbon::now()->subDays(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(30)->format('Y-m-d'),
        ]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);


        $sale_date_start = Carbon::now()->addDays(2)->format('Y-m-d H:i:s');
        $sale_date_end = Carbon::now()->addDays(10)->format('Y-m-d H:i:s');
        $enjoy_date_start = Carbon::now()->addDays(2)->format('Y-m-d H:i:s');
        $enjoy_date_end = Carbon::now()->addDays(10)->format('Y-m-d H:i:s');
        $this->json('PUT', '/api/v1/promotion', [
            'id' => $promotionFactory->id,
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,

            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'apply_device_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'campaign_id' => $campaignFactory1->id,
            'range_sale_date' => [0 => ['sale_date_start' => $sale_date_start,
                'sale_date_end' => $sale_date_end,'sale_days' => ['id' => 1]]],
            'range_enjoy_date' => [0 => ['enjoy_date_start' => $enjoy_date_start, 'enjoy_date_end' => $enjoy_date_end,'enjoy_days' => ['id' => 1]]],
        ])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_promotion_range_sale_day', [
            'promotion_id' => $promotionFactory->id,
            'sale_date_start' => $sale_date_start,
            'sale_date_end' => $sale_date_end
        ]);

        $this->assertDatabaseHas('mo_promotion_range_enjoy_day', [
            'promotion_id' => $promotionFactory->id,
            'enjoy_date_start' => $enjoy_date_start,
            'enjoy_date_end' => $enjoy_date_end
        ]);

    }

}