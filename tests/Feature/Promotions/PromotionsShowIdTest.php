<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class PromotionsShowIdTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta promoción inexistente sin filtro
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/promotion/999999999999999999999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta promoción existente sin filtro lang, utilizando filtros con tablas intermedias.
     * Incluye rangos de fechas asociados a tablas nuevas y comprobacion en salida
     */
    public function testOK2()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([
        ]);

        $campaignTranslationFactory = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory->id
        ]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_country_filter' => 1,
            'apply_client_type_filter' => 1,
            'apply_state_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_device_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_language_filter' => 1,
            'apply_pickup_filter' => 1,
            'campaign_id' => $campaignFactory->id

        ]);
        $translation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => 1,
            'promotion_id' => $promotionFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => 2,
            'promotion_id' => $promotionFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => 3,
            'promotion_id' => $promotionFactory->id,
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $promotionServiceFactory = factory(App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotionFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\ProductType::class)->create();

        $promotionProductTypeFactory = factory(App\Models\tenant\PromotionProductType::class)->create([
            'product_type_id' => $productTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $countryFactory = factory(\App\Models\tenant\Country::class)->create();

        $promotionCountryFactory = factory(App\Models\tenant\PromotionCountry::class)->create([
            'country_id' => $countryFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $stateFactory = factory(\App\Models\tenant\State::class)->create();

        $promotionStateFactory = factory(App\Models\tenant\PromotionState::class)->create([
            'state_id' => $stateFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $clientFactory = factory(\App\Models\tenant\ClientType::class)->create();

        $promotionClientFactory = factory(App\Models\tenant\PromotionClientType::class)->create([
            'client_type_id' => $clientFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create();

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'subchannel_id' => $subchannelFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $currencyFactory = factory(\App\Models\tenant\Currency::class)->create();

        $promotionCurrencyFactory = factory(App\Models\tenant\PromotionCurrency::class)->create([
            'currency_id' => $currencyFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $deviceFactory = factory(\App\Models\tenant\Device::class)->create();

        $promotionDeviceFactory = factory(App\Models\tenant\PromotionDevice::class)->create([
            'device_id' => $deviceFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $paymentMethodFactory = factory(\App\Models\tenant\PaymentMethod::class)->create();

        $promotionPaymentMethodFactory = factory(App\Models\tenant\PromotionPaymentMethod::class)->create([
            'payment_method_id' => $paymentMethodFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $languageFactory = factory(\App\Models\tenant\Language::class)->create();

        $promotionLanguageFactory = factory(App\Models\tenant\PromotionLanguage::class)->create([
            'language_id' => $languageFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create();

        $promotionPickupFactory = factory(App\Models\tenant\PromotionPickup::class)->create([
            'pickup_id' => $pickupFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        //se añade factories de file y traaduccion para comprobar que se muestra en salida
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $promotionFileFactory = factory(App\Models\tenant\PromotionFile::class)->create([
            'file_id' => $fileFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionRangeSale = factory(App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotionFactory->id,
            'sale_date_start' => '2099-10-10 12:00:00',
            'sale_date_end' => '2099-10-20 12:00:00',
        ]);

        $promotionRangeEnjoy = factory(App\Models\tenant\PromotionRangeEnjoyDay::class)->create([
            'promotion_id' => $promotionFactory->id,
            'enjoy_date_start' => '2099-10-10 12:00:00',
            'enjoy_date_end' => '2099-10-20 12:00:00',
        ]);



        $promotion_factory_2 = factory(App\Models\tenant\Promotion::class)->create([
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_country_filter' => 1,
            'apply_client_type_filter' => 1,
            'apply_state_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_device_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_language_filter' => 1,
            'apply_pickup_filter' => 1,
            'campaign_id' => $campaignFactory->id
        ]);

        $promotion_accumulate = factory(\App\Models\tenant\PromotionAccumulate::class)->create([
            'promotion_id' => $promotionFactory->id,
            'promotion_accumulate_id' => $promotion_factory_2->id
        ]);


        $this->json('GET', '/api/v1/promotion/' . $promotionFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $promotionFactory->id,
                'code' => $promotionFactory->code,
                'order' => $promotionFactory->order,
                'visibility' => $promotionFactory->visibility,
                'accumulate' => $promotionFactory->accumulate,
                'min_quantity' => $promotionFactory->min_quantity,
                'max_quantity' => $promotionFactory->max_quantity,
                'promotion_amount' => $promotionFactory->promotion_amount,
                'promotion_type_id' => $promotionFactory->promotion_type_id,
                'id' => $fileFactory->id,
                'id' => $promotionFactory->campaign_id,
                'id' => $promotion_factory_2->id,
                'apply_product_filter' => $promotionFactory->apply_product_filter,
                'apply_product_type_filter' => $promotionFactory->apply_product_type_filter,
                'apply_service_filter' => $promotionFactory->apply_service_filter,
                'apply_subchannel_filter' => $promotionFactory->apply_subchannel_filter,
                'apply_country_filter' => $promotionFactory->apply_country_filter,
                'apply_state_filter' => $promotionFactory->apply_state_filter,
                'apply_client_type_filter' => $promotionFactory->apply_country_filter,
                'apply_currency_filter' => $promotionFactory->apply_currency_filter,
                'apply_device_filter' => $promotionFactory->apply_device_filter,
                'apply_language_filter' => $promotionFactory->apply_language_filter,
                'apply_payment_method_filter' => $promotionFactory->apply_payment_method_filter,
                'anticipation_start' => $promotionFactory->anticipation_start,
                'anticipation_end' => $promotionFactory->anticipation_end,
                'coupon_code' => $promotionFactory->coupon_code,
                'coupon_type' => $promotionFactory->coupon_type,
                'coupon_sheet_start' => $promotionFactory->coupon_sheet_start,
                'coupon_sheet_end' => $promotionFactory->coupon_sheet_end,
                'coupon_total_uses' => $promotionFactory->coupon_total_uses,
                'language_id' => $translation1->language_id,
                'language_id' => $translation2->language_id,
                'language_id' => $translation3->language_id,
                'name' => $translation1->name,
                'description' => $translation2->description,
                'gift' => $translation3->gift,
                'short_description' => $translation1->short_description,
                'friendly_url' => $translation2->friendly_url,
                'title_seo' => $translation3->title_seo,
                'description_seo' => $translation3->description_seo,
                'keywords_seo' => $translation3->keywords_seo,
                'observations' => $translation2->observations,
                'legal' => $translation1->legal,
                'product_id' => $promotionProductFactory->product_id,
                'service_id' => $promotionServiceFactory->service_id,
                'product_type_id' => $promotionProductTypeFactory->product_type_id,
                'country_id' => $promotionCountryFactory->country_id,
                'subchannel_id' => $promotionChannelFactory->subchannel_id,
                'currency_id' => $promotionCurrencyFactory->currency_id,
                'device_id' => $promotionDeviceFactory->device_id,
                'payment_method_id' => $promotionPaymentMethodFactory->payment_method_id,
                'language_id' => $promotionLanguageFactory->language_id,
                'pickup_id' => $promotionPickupFactory->pickup_id,
                'sale_date_start' => '2099-10-10 12:00:00',
                'sale_date_end' => '2099-10-20 12:00:00',
                'enjoy_date_start' => '2099-10-10 12:00:00',
                'enjoy_date_end' => '2099-10-20 12:00:00',
            ]);
    }

    /**
     *Test comprueba estructura con campaign encapsulado y file
     */
    public function testOk3()
    {

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_country_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_device_filter' => 1,
            'apply_payment_method_filter' => 1,
            'apply_language_filter' => 1,
            'apply_pickup_filter' => 1,

        ]);
        $translation1 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => 1,
            'promotion_id' => $promotionFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => 2,
            'promotion_id' => $promotionFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'language_id' => 3,
            'promotion_id' => $promotionFactory->id,
        ]);

        $this->json('GET', '/api/v1/promotion/' . $promotionFactory->id, [])
            ->assertJsonStructure(['error', 'data' => ["0" => ['promotion' => ["0" => ['files','campaign', 'lang']]]]
            ]);

    }

}