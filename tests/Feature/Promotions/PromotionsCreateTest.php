<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class PromotionsCreateTest extends TestCase
{
    
    use DatabaseTransactions;
    use WithoutMiddleware;

    //inicio pruebas erróneas

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda ningún requerido
     */
    public function testBad()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["apply_subchannel_filter" => ["The apply subchannel filter field is required."],
                    "apply_country_filter" => ["The apply country filter field is required."], "apply_currency_filter" => ["The apply currency filter field is required."],
                    "apply_language_filter" => ["The apply language filter field is required."],
                    "apply_pickup_filter" => ["The apply pickup filter field is required."], "apply_payment_method_filter" => ["The apply payment method filter field is required."],
                    "apply_device_filter" => ["The apply device filter field is required."],
                    "apply_product_filter" => ["The apply product filter field is required."], "apply_product_type_filter" => ["The apply product type filter field is required."],
                    "apply_service_filter" => ["The apply service filter field is required."],"apply_state_filter" => ["The apply state filter field is required."], "code" => ["The code field is required."],
                    "promotion_type_id" => ["The promotion type id field is required."],
                    "campaign_id" => ["The campaign id field is required."],
                    "apply_client_type_filter" => ["The apply client type filter field is required."],
                    "department_id" => ["The department id field is required."],
                    "cost_center_id" => ["The cost center id field is required."],
                    "translation" => ["you need at least one translation"]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda formato erróneo en todos los campos que tienen validación
     */
    public function testBad1()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'ppppaaa',
            'order' => 'r',
            'visibility' => 'r',
            'accumulate' => 'r',
            'min_quantity' => 'r',
            'max_quantity' => 'r',
            'promotion_amount' => 'r',
            'promotion_type_id' => 'r',
            'coupon_type' => 2,
            'coupon_sheet_start' => 'r',
            'coupon_sheet_end' => 'r',
            'coupon_total_uses' => 'r',
            'anticipation_start' => 'r',
            'anticipation_end' => 'r',
            'apply_product_type_filter' => 'r',
            'apply_subchannel_filter' => 'r',
            'apply_product_filter' => 'r',
            'apply_service_filter' => 'r',
            'apply_country_filter' => 'r',
            'apply_state_filter' => 'r',
            'apply_payment_method_filter' => 'r',
            'apply_device_filter' => 'r',
            'apply_language_filter' => 'r',
            'apply_currency_filter' => 'r',
            'apply_pickup_filter' => 'r',
            'lockers_validation' => 'r',
            'supervisor_validation' => 'r',
            'allow_date_change' => 'r',
            'allow_upgrade' => 'r',
            'allow_product_change' => 'r',
            'pay_difference' => 'r',
            'apply_client_type_filter' => 0,
            'department_id' => 's',
            'cost_center_id' => 's',
            'name' => ['ES' => 'español', 'EN' => 'INGLES'],
            'description' => ['PT' => 'español', 'EN' => 'INGLES'],
            'short_description' => ['ES' => 'español', 'PT' => 'INGLES'],
            'title_seo' => ['ES' => 'español'],
            'description_seo' => '',
            'campaign_id' => 'r',
            'files' => [0 => 'sd'],

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["accumulate" => ["The accumulate field must be true or false."], "anticipation_end" => ["The anticipation end must be an integer."],
                    "anticipation_start" => ["The anticipation start must be an integer."], "apply_subchannel_filter" => ["The apply subchannel filter field must be true or false."],
                    "apply_country_filter" => ["The apply country filter field must be true or false."], "apply_currency_filter" => ["The apply currency filter field must be true or false."],
                    "apply_device_filter" => ["The apply device filter field must be true or false."], "apply_language_filter" => ["The apply language filter field must be true or false."],
                    "apply_pickup_filter" => ["The apply pickup filter field must be true or false."], "apply_payment_method_filter" => ["The apply payment method filter field must be true or false."],
                    "apply_product_filter" => ["The apply product filter field must be true or false."], "apply_product_type_filter" => ["The apply product type filter field must be true or false."],
                    "apply_service_filter" => ["The apply service filter field must be true or false."], "apply_state_filter" => ["The apply state filter field must be true or false."], "campaign_id" => ["The campaign id must be an integer."], "coupon_sheet_end" => ["The coupon sheet end must be an integer."],
                    'lockers_validation' => ['The lockers validation field must be true or false.'],
                    'supervisor_validation' => ['The supervisor validation field must be true or false.'],
                    'allow_date_change' => ['The allow date change field must be true or false.'],
                    'allow_upgrade' => ['The allow upgrade field must be true or false.'],
                    'allow_product_change' => ['The allow product change field must be true or false.'],
                    'pay_difference' => ['The pay difference field must be true or false.'],
                    "coupon_sheet_start" => ["The coupon sheet start must be an integer."], "coupon_total_uses" => ["The coupon total uses must be an integer."],
                    "coupon_type" => ["The coupon type field must be true or false."], "description.ES" => ["The description. e s field is required."],
                    "department_id" => ["The department id must be an integer."],
                    "cost_center_id" => ["The cost center id must be an integer."],
                    "description_seo.EN" => ["The description seo. e n field is required."], "description_seo.ES" => ["The description seo. e s field is required."],
                    "description_seo.PT" => ["The description seo. p t field is required."], "files.0" => ["The files.0 must be an integer."], "name.PT" => ["The name. p t field is required."],
                    "order" => ["The order must be an integer."], "promotion_amount" => ["The promotion amount must be a number."],
                    "promotion_type_id" => ["The promotion type id must be an integer."], "min_quantity" => ["The min quantity must be an integer."],"max_quantity" => ["The max quantity must be an integer."],
                    "short_description.EN" => ["The short description. e n field is required."], "title_seo.EN" => ["The title seo. e n field is required."],
                    "title_seo.PT" => ["The title seo. p t field is required."], "visibility" => ["The visibility field must be true or false."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones de rangos de fechas y de enteros
     *
     * Se mandan requeridos y todos los rangos de fechas poniendo la fecha de fin del rango menor que la de inicio,
     * y dos rangos de enteros poniendo menor el campo final del rango
     *
     * Se comprueba también campo campaign_id que pida uno existe en su tabla
     */
    public function testBad2()
    {
        $promotionTypeFactory = factory(\App\Models\tenant\PromotionType::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'asas',
            'promotion_type_id' => $promotionTypeFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'coupon_sheet_start' => 9,
            'coupon_sheet_end' => 7,
            'anticipation_start' => 12,
            'anticipation_end' => 10,
            'min_quantity' => 12,
            'max_quantity' => 10,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'campaign_id' => 9999999,
            'files' => [0 => 9999999],
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["anticipation_end" => ["anticipation_end must be greater than or equal to anticipation_start"],
                    "anticipation_start" => ["anticipation_start must be less than or equal to anticipation_end"], "campaign_id" => ["The selected campaign id is invalid."],
                    "coupon_sheet_end" => ["coupon_sheet_end must be greater than or equal to coupon_sheet_start"],
                    "coupon_sheet_start" => ["coupon_sheet_start must be less than or equal to coupon_sheet_end"],
                    "files.0" => ["The selected files.0 is invalid."],"max_quantity"=>["The max_quantity must be at least min_quantity"],
                    ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array services con filtro añadir promoción a todos los servicios
     */
    public function testBad4()
    {

        $promotionTypeFactory = factory(\App\Models\tenant\PromotionType::class)->create([]);
        $serviceFactory = factory(\App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_service_filter' => 0,
            'services' => [
                '0' => [
                    'id' => $serviceFactory->id,
                    'apply_client_type_filter' => 0]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "services" => ["If apply_service_filter is 0, can not send services"]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array subchannels con filtro añadir promoción a todos los canales
     */
   /* public function testBad5()
    {
        $subchannelFactory = factory(\App\Subchannel::class)->create([]);
        $promotionTypeFactory = factory(\App\PromotionType::class)->create([]);
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_subchannel_filter' => 0,
            'subchannels' => [
                '0' => [
                    'id' => $subchannelFactory->id]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "subchannels" => ["If apply_subchannel_filter is 0, can not send subchannels"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array countries con filtro añadir promoción a todos los countries
     */
    /*public function testBad6()
    {
        $promotionTypeFactory = factory(\App\PromotionType::class)->create([]);
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $countryFactory = factory(\App\Country::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_country_filter' => 0,
            'countries' => [
                '0' => [
                    'id' => $countryFactory->id]],
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "countries" => ["If apply_country_filter is 0, can not send countries"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array states con filtro añadir promoción a todos los states a cero
     */
    /*public function testBad7()
    {
        $promotionTypeFactory = factory(\App\PromotionType::class)->create([]);
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $stateFactory = factory(\App\State::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'dfdfd',
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_state_filter' => 0,
            'states' => [
                '0' => [
                    'id' => $stateFactory->id]],
            'apply_country_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "states" => ["If apply_state_filter is 0, can not send states"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array products con filtro añadir promoción a todos los productos
     */
    /*public function testBad8()
    {
        $product = factory(\App\Product::class)->create([]);
        $promotionTypeFactory = factory(\App\PromotionType::class)->create([]);
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_product_filter' => 0,
            'products' => [
                '0' => [
                    'id' => $product->id,
                    'apply_client_type_filter' => 0]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "products" => ["If apply_product_filter is 0, can not send products"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array product_types con filtro añadir promoción a todos los tipos de producto
     */
    /*public function testBad9()
    {
        $promotionTypeFactory = factory(\App\PromotionType::class)->create([]);
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $productTypeFactory = factory(\App\ProductType::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_product_type_filter' => 0,
            'product_types' => [
                '0' => [
                    'id' => $productTypeFactory->id,
                    'apply_client_type_filter' => 0]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "product_types" => ["If apply_product_type_filter is 0, can not send product_types"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array days con filtro añadir promoción a todos los days
     */
    /*public function testBad10()
    {
        $promotionTypeFactory = factory(\App\PromotionType::class)->create([]);
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_device_filter' => 0,
            'devices' => [
                '0' => [
                    'id' => 1]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "devices" => ["If apply_device_filter is 0, can not send devices"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array languages con filtro añadir promoción a todos los languages
     */
    /*public function testBad11()
    {
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $promotion_type = factory(\App\PromotionType::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotion_type->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_language_filter' => 0,
            'languages' => [
                '0' => [
                    'id' => 1]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "languages" => ["If apply_language_filter is 0, can not send languages"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array currencies con filtro añadir promoción a todos los currencies
     */
    /*public function testBad12()
    {
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);


        $promotion_type = factory(\App\PromotionType::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotion_type->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_currency_filter' => 0,
            'currencies' => [
                '0' => [
                    'id' => 1]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "currencies" => ["If apply_currency_filter is 0, can not send currencies"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y no se permite enviar al mismo tiempo array payment_methods con filtro añadir promoción a todos los payment_methods
     */
    /*public function testBad13()
    {
        $paymentMethod = factory(\App\PaymentMethod::class)->create([]);
        $campaignFactory = factory(App\Campaign::class)->create([]);
        $user_factory = factory(\App\User::class)->create([]);

        $promotion_type = factory(\App\PromotionType::class)->create([]);

        $this->json('POST', '/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotion_type->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],

            'apply_payment_method_filter' => 0,
            'payment_methods' => [
                '0' => [
                    'id' => $paymentMethod->id]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "payment_methods" => ["If apply_payment_method_filter is 0, can not send payment_methods"]]]]);
    }*/

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y se intenta enviar campos de array product_types sin enviar el id del product_type a asociar a la promoción
     */
    public function testBad14()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotion_type->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'min_quantity' => 7,
                    'max_quantity' => 7,
                    'promotion_amount' => 15,
                    'promotion_type_id' => 1]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "product_types.0.id" => ["The product_types.0.id field is required when product_types.0.promotion_amount / product_types.0.min_quantity / product_types.0.max_quantity / product_types.0.series_one / product_types.0.series_two / product_types.0.promotion_type_id is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y se intenta enviar campos de array products sin enviar el id del product a asociar a la promoción
     */
    public function testBad15()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotion_type->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'min_quantity' => 7,
                    'max_quantity' => 7,
                    'promotion_amount' => 15,
                    'promotion_type_id' => 1]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "products.0.id" => ["The products.0.id field is required when products.0.promotion_amount / products.0.min_quantity / products.0.max_quantity / products.0.series_one / products.0.series_two / products.0.promotion_type_id is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan requeridos y se intenta enviar campos de array services sin enviar el id del service a asociar a la promoción
     */
    public function testBad16()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'dfdfd',
            'promotion_type_id' => $promotion_type->id,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'min_quantity' => 7,
                    'max_quantity' => 7,
                    'promotion_amount' => 15,
                    'promotion_type_id' => 1]],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "services.0.id" => ["The services.0.id field is required when services.0.promotion_amount / services.0.min_quantity / services.0.max_quantity / services.0.series_one / services.0.series_two / services.0.promotion_type_id is present."]]]]);
    }

    /**
     *Test bad envia un campo en portugues y no envía los requeridos de ese idioma
     *
     */
    public function testBad18()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['PT' => 'legal PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [["description.PT" => ["The description. p t field is required."],
                    "description_seo.PT" => ["The description seo. p t field is required."],
                    "name.PT" => ["The name. p t field is required."],
                    "short_description.PT" => ["The short description. p t field is required."],
                    "title_seo.PT" => ["The title seo. p t field is required."]]]
            ]);
    }

    /**
     * Test sin envio de apply_client_type_filter
     */

    public function testBad19()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);


        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'apply_client_type_filter' => ['The apply client type filter field is required.'],
        ]]]);
    }

    /**
     * Test apply_client_type_filter sin array de tipos de cliente
     */
    public function testBad20()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 1,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'client_types' => ['If apply_client_type_filter is 1, the client types field must arrive, null or informed'],
        ]]]);
    }


    /**
     * Test apply_client_type_filter con formato de parametro client_types incorrecto
     */

    public function testBad21()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 1,
            'client_types' => 'test',
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'client_types' => ['The client types must be an array.'],
        ]]]);
    }

    /**
     * Test products.*.apply_client_type_filter sin enviar
     */
    public function testBad22()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                ]
            ]

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'products.0.apply_client_type_filter' => ['The products.0.apply_client_type_filter field is required when products.0.id is present.']
        ]]]);
    }

    /**
     * Test product.*.apply_client_type_filter formato incorrecto
     */
    public function testBad23()
    {

        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'apply_client_type_filter' => 'test',
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'products.0.apply_client_type_filter' => ["The products.0.apply_client_type_filter field must be true or false."]
        ]]]);

    }

    /**
     * Test product.*.apply_client_type_filter sin array client_types
     */
    public function testBad24()
    {

        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'apply_client_type_filter' => 1,
                ]
            ]
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["products.0.client_types"=>["If products.0.apply_client_type_filter is 1, the products.0.client types field must arrive, null or informed"]]]]);
    }

    /**
     * Test product.*.client_types formato incorrecto
     */
    public function testBad25()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => 'test',
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'products.0.client_types' => ["The products.0.client_types must be an array."],
        ]]]);
    }


    /**
     * Test product.*.client_types.*.id inexistente en base de datos
     */
    public function testBad26()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => [
                        0 => ['id' => 999999999999999],
                    ],
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'products.0.client_types.0.id' => ["The selected products.0.client_types.0.id is invalid."],
        ]]]);
    }

    //

    /**
     * Test product_types.*.apply_client_type_filter sin enviar
     */
    public function testBad27()
    {
        $product_type = factory(\App\Models\tenant\ProductType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'product_types' => [
                0 => [
                    'id' => $product_type->id,
                ]
            ]

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_types.0.apply_client_type_filter' => ['The product_types.0.apply_client_type_filter field is required when product_types.0.id is present.']
        ]]]);
    }

    /**
     * Test product_types.*.apply_client_type_filter formato incorrecto
     */
    public function testBad28()
    {
        $product_type = factory(\App\Models\tenant\ProductType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'product_types' => [
                0 => [
                    'id' => $product_type->id,
                    'apply_client_type_filter' => 'test',
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_types.0.apply_client_type_filter' => ["The product_types.0.apply_client_type_filter field must be true or false."]
        ]]]);

    }

    /**
     * Test product_types.*.apply_client_type_filter sin array client_types
     */
    public function testBad29()
    {
        $product_type = factory(\App\Models\tenant\ProductType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'product_types' => [
                0 => [
                    'id' => $product_type->id,
                    'apply_client_type_filter' => 1,
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_types.0.client_types' => ["If product_types.0.apply_client_type_filter is 1, the product_types.0.client types field must arrive, null or informed"],
        ]]]);
    }

    /**
     * Test product_types.*.client_types formato incorrecto
     */
    public function testBad30()
    {
        $product_type = factory(\App\Models\tenant\ProductType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);


        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'product_types' => [
                0 => [
                    'id' => $product_type->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => 'test',
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_types.0.client_types' => ["The product_types.0.client_types must be an array."],
        ]]]);
    }


    /**
     * Test product_types.*.client_types.*.id inexistente en base de datos
     */
    public function testBad31()
    {
        $product_type = factory(\App\Models\tenant\ProductType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'product_types' => [
                0 => [
                    'id' => $product_type->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => [
                        0 => ['id' => 999999999999999],
                    ],
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_types.0.client_types.0.id' => ["The selected product_types.0.client_types.0.id is invalid."],
        ]]]);
    }

    //Servicios

    /**
     * Test product_types.*.apply_client_type_filter sin enviar
     */
    public function testBad32()
    {
        $service = factory(\App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 1,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'services' => [
                0 => [
                    'id' => $service->id,
                ]
            ]

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'services.0.apply_client_type_filter' => ['The services.0.apply_client_type_filter field is required when services.0.id is present.']
        ]]]);
    }

    /**
     * Test product_types.*.apply_client_type_filter formato incorrecto
     */
    public function testBad33()
    {
        $service = factory(\App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 1,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'services' => [
                0 => [
                    'id' => $service->id,
                    'apply_client_type_filter' => 'test',
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'services.0.apply_client_type_filter' => ["The services.0.apply_client_type_filter field must be true or false."]
        ]]]);

    }

    /**
     * Test product_types.*.apply_client_type_filter sin array client_types
     */
    public function testBad34()
    {
        $service = factory(\App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 1,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'services' => [
                0 => [
                    'id' => $service->id,
                    'apply_client_type_filter' => 1,
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'services.0.client_types' => ["If services.0.apply_client_type_filter is 1, the services.0.client types field must arrive, null or informed"],
        ]]]);
    }

    /**
     * Test product_types.*.client_types formato incorrecto
     */
    public function testBad35()
    {
        $service = factory(\App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,


            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 1,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'services' => [
                0 => [
                    'id' => $service->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => 'test',
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'services.0.client_types' => ["The services.0.client_types must be an array."],
        ]]]);
    }


    /**
     * Test product_types.*.client_types.* inexistente en base de datos
     */
    public function testBad36()
    {
        $service = factory(\App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);


        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 1,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'services' => [
                0 => [
                    'id' => $service->id,
                    'apply_client_type_filter' => 1,
                    'client_types' => [
                        0 => ['id' => 999999999999999],
                    ],
                ]
            ]
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'services.0.client_types.0.id' => ["The selected services.0.client_types.0.id is invalid."],
        ]]]);
    }

    /**
     * Test validacion rangos de fechas, manda requeridos y formato incorrecto fechas
     */
    public function testBad37()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);


        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'range_sale_date' => [0 => ['sale_date_start' => 'r', 'sale_date_end' => 'r']],
            'range_enjoy_date' => [0 => ['enjoy_date_start' => 'r', 'enjoy_date_end' => 'r']],
            'range_validity_date' => [0 => ['validity_date_start' => 'r', 'validity_date_end' => 'r']],

        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["range_enjoy_date.0.enjoy_date_end"=>["The range_enjoy_date.0.enjoy_date_end does not match the format Y-m-d H:i:s.","The range_enjoy_date.0.enjoy_date_end is not a valid date.","The range_enjoy_date.0.enjoy_date_end must be a date after or equal to range_enjoy_date.0.enjoy_date_start."],
                "range_enjoy_date.0.enjoy_date_start"=>["The range_enjoy_date.0.enjoy_date_start does not match the format Y-m-d H:i:s.","The range_enjoy_date.0.enjoy_date_start is not a valid date."],"range_enjoy_date.0.enjoy_days"=>["The range_enjoy_date.0.enjoy_days field is required when range_enjoy_date.0.enjoy_date_start / range_enjoy_date.0.enjoy_date_end is present."],
                "range_sale_date"=>["The range_sale_date.0 is outside the range of the campaign"],"range_sale_date.0.sale_date_end"=>["The range_sale_date.0.sale_date_end does not match the format Y-m-d H:i:s.","The range_sale_date.0.sale_date_end is not a valid date.","The range_sale_date.0.sale_date_end must be a date after or equal to range_sale_date.0.sale_date_start."],
                "range_sale_date.0.sale_date_start"=>["The range_sale_date.0.sale_date_start does not match the format Y-m-d H:i:s.","The range_sale_date.0.sale_date_start is not a valid date."],"range_sale_date.0.sale_days"=>["The range_sale_date.0.sale_days field is required when range_sale_date.0.sale_date_start / range_sale_date.0.sale_date_end is present."]]]]);
    }

    /**
     * Test validacion rangos de fechas, manda requeridos y fecha de fin de rango anterior a la fecha de inicio de rango
     */
    public function testBad38()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);


        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'range_sale_date' => [0 => ['sale_date_start' => '2018-11-10 08:00:00', 'sale_date_end' => '2018-11-08 08:00:00']],
            'range_enjoy_date' => [0 => ['enjoy_date_start' => '2018-11-10 08:00:00', 'enjoy_date_end' => '2018-11-08 08:00:00']],
            'range_validity_date' => [0 => ['validity_date_start' => '2018-11-10 08:00:00', 'validity_date_end' => '2018-11-08 08:00:00']],

        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["range_enjoy_date.0.enjoy_date_end"=>["The range_enjoy_date.0.enjoy_date_end must be a date after or equal to range_enjoy_date.0.enjoy_date_start."],
                "range_enjoy_date.0.enjoy_days"=>["The range_enjoy_date.0.enjoy_days field is required when range_enjoy_date.0.enjoy_date_start / range_enjoy_date.0.enjoy_date_end is present."],
                "range_sale_date"=>["The range_sale_date.0 is outside the range of the campaign"],
                "range_sale_date.0.sale_date_end"=>["The range_sale_date.0.sale_date_end must be a date after or equal to range_sale_date.0.sale_date_start."],
                "range_sale_date.0.sale_days"=>["The range_sale_date.0.sale_days field is required when range_sale_date.0.sale_date_start / range_sale_date.0.sale_date_end is present."]]]]);
    }

    /**
     * Test validacion rangos de fechas, manda rangos solapados
     */
    public function testBad39()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'legal' => ['ES' => 'legal ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'range_sale_date' => ['0' => ['sale_date_start' => '2018-11-10 08:00:00', 'sale_date_end' => '2018-11-12 08:00:00'],1 => ['sale_date_start' => '2018-11-09 08:00:00', 'sale_date_end' => '2018-11-11 08:00:00']],
            'range_enjoy_date' => ['0' => ['enjoy_date_start' => '2018-11-10 08:00:00', 'enjoy_date_end' => '2018-11-12 08:00:00'],1 => ['enjoy_date_start' => '2018-11-09 08:00:00', 'enjoy_date_end' => '2018-11-11 08:00:00']],
            'range_validity_date' => ['0' => ['validity_date_start' => '2018-11-10 08:00:00', 'validity_date_end' => '2018-11-12 08:00:00'],1 => ['validity_date_start' => '2018-11-09 08:00:00', 'validity_date_end' => '2018-11-11 08:00:00']],

        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["range_enjoy_date"=>["The range_enjoy_date.1.enjoy_date_start are overlapping"],
                "range_enjoy_date.0.enjoy_days"=>["The range_enjoy_date.0.enjoy_days field is required when range_enjoy_date.0.enjoy_date_start / range_enjoy_date.0.enjoy_date_end is present."],
                "range_enjoy_date.1.enjoy_days"=>["The range_enjoy_date.1.enjoy_days field is required when range_enjoy_date.1.enjoy_date_start / range_enjoy_date.1.enjoy_date_end is present."],
                "range_sale_date"=>["The range_sale_date.1 is outside the range of the campaign"],"range_sale_date.0.sale_days"=>["The range_sale_date.0.sale_days field is required when range_sale_date.0.sale_date_start / range_sale_date.0.sale_date_end is present."],
                "range_sale_date.1.sale_days"=>["The range_sale_date.1.sale_days field is required when range_sale_date.1.sale_date_start / range_sale_date.1.sale_date_end is present."]]]]);
    }

    /**
     *Prueba automatizada enviando benefit card en formato incorrecto
     *
     */
    public function testBad40()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

            'benefit_card' => 'TEST',
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                "error"=>400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "benefit_card"=>["The benefit card field must be true or false."]
                ]]
            ]);
    }

    /**
     *Prueba automatizada enviando benefit card sin benefit card id
     *
     */
    public function testBad41()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

            'benefit_card' => 1,

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                "error"=>400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "benefit_card_id"=>["The benefit card id field is required when benefit card is 1."]
                ]]
            ]);
    }

    /**
     *Prueba automatizada enviando benefit card con benefit card id y con formato incorrecto de benefit_card_total_uses
     *
     */
    public function testBad42()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $identification = factory(\App\Models\tenant\Identification::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

            'benefit_card' => 1,
            'benefit_card_id' => $identification->id,
            'benefit_card_total_uses' => 'TEST',

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([

                    "error"=>400,
                    "error_description"=>"The fields are not the required format",
                    "error_inputs"=>[[
                        "benefit_card_total_uses"=>
                            ["The benefit card total uses must be an integer."]]]
            ]);
    }

    /**
     *Prueba automatizada enviando accumulate sin promociones accumulativas
     *
     */
    public function testBad43()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $identification = factory(\App\Models\tenant\Identification::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'accumulate' => 1,

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([

                "error"=>400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "promotion_accumulate"=>
                        ["The promotion accumulate field is required when accumulate is 1."]]]
            ]);
    }

    /**
     *Prueba automatizada enviando accumulate con accumulate_promotion en formato incorrecto
     *
     */
    public function testBad44()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $identification = factory(\App\Models\tenant\Identification::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'accumulate' => 1,
            'promotion_accumulate' => [
                0 => ['id' => 'TEST']
            ],

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([

                "error"=>400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "promotion_accumulate.0.id"=>
                        ["The promotion_accumulate.0.id must be an integer."]]]
            ]);
    }

    /**
     *Prueba automatizada enviando accumulate con accumulate_promotion id repetido
     *
     */
    public function testBad45()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $identification = factory(\App\Models\tenant\Identification::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);
        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'accumulate' => 1,
            'promotion_accumulate' => [
                0 => ['id' => $promotion_factory->id],
                1 => ['id' => $promotion_factory->id],
            ],

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([

                "error"=>400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "promotion_accumulate.0.id"=>
                        ["The promotion_accumulate.0.id field has a duplicate value."],
                    "promotion_accumulate.1.id" =>
                        ["The promotion_accumulate.1.id field has a duplicate value."]]]
            ]);
    }


    //pruebas resultado correcto

    /**
     *Test ok envia sólo requeridos con un product en el array products, con todos los parametreos posibles en el array products
     * y comprueba que se introducen todos los campos
     *
     */
    public function testOk1()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $promotionTypeFactory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id, 'min_quantity' => 2, 'max_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotionTypeFactory->id, 'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'product_id' => $productFactory->id,
            'min_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => $promotionTypeFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios product en el array products
     *
     */
    public function testOk2()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $productFactory2 = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0],
                '1' => [
                    'id' => $productFactory2->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'product_id' => $productFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con un product type en el array product_types y todos los parámetros posibles y comprueba que se introducen su tabla
     *
     */
    public function testOk3()
    {

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionTypeFactory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => $productTypeFactory->id, 'min_quantity' => 2,'max_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotionTypeFactory->id, 'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => $productTypeFactory->id,
            'min_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => $promotionTypeFactory->id,
            'deleted_at' => null,
        ]);

    }

    /**
     *Test ok envia sólo requeridos con varios product types en el array product_types
     *
     */
    public function testOk4()
    {
        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);
        $productTypeFactory2 = factory(App\Models\tenant\ProductType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => $productTypeFactory->id, 'apply_client_type_filter' => 0],
                '1' => [
                    'id' => $productTypeFactory2->id, 'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => $productTypeFactory2->id,
            'deleted_at' => null,
        ]);
    }



    /**
     *Test ok envia sólo requeridos con un service en el array services y todos los parámetros posibles
     *
     */
    public function testOk5()
    {

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotionTypeFactory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => $serviceFactory->id, 'min_quantity' => 2,'max_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotionTypeFactory->id, 'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_service_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'service_id' => $serviceFactory->id,
            'min_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => $promotionTypeFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios service en el array services
     *
     */
    public function testOk6()
    {

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => $serviceFactory->id, 'apply_client_type_filter' => 0],
                '1' => [
                    'id' => $serviceFactory2->id, 'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_service_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'service_id' => $serviceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'service_id' => $serviceFactory2->id,
            'deleted_at' => null,
        ]);
    }


    /**
     *Test ok envia sólo requeridos con varios countries en el array countries
     *
     */
    public function testOk11()
    {

        $countryFactory = factory(\App\Models\tenant\Country::class)->create([]);
        $countryFactory2 = factory(\App\Models\tenant\Country::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => $countryFactory->id],
                '1' => [
                    'id' => $countryFactory2->id]],
            'apply_state_filter' => 0,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_country_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_country', [
            'country_id' => $countryFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_country', [
            'country_id' => $countryFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios states en el array states
     *
     */
    public function testOk13()
    {
        $stateFactory = factory(\App\Models\tenant\State::class)->create([]);
        $stateFactory2 = factory(\App\Models\tenant\State::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => $stateFactory->id],
                '1' => [
                    'id' => $stateFactory2->id]],
            'apply_country_filter' => 0,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_state_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_state', [
            'state_id' => $stateFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_state', [
            'state_id' => $stateFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios subchannels en el array subchannels
     *
     */
    public function testOk15()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $subchannelFactory_2 = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => $subchannelFactory->id],
                '1' => [
                    'id' => $subchannelFactory_2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_subchannel_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'subchannel_id' => $subchannelFactory_2->id,
            'deleted_at' => null,
        ]);
    }


    /**
     *Test ok envia sólo requeridos con varios devices en el array devices
     *
     */
    public function testOk17()
    {

        $deviceFactory = factory(\App\Models\tenant\Device::class)->create([]);
        $deviceFactory2 = factory(\App\Models\tenant\Device::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => $deviceFactory->id],
                '1' => [
                    'id' => $deviceFactory2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_device_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_device', [
            'device_id' => $deviceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_device', [
            'device_id' => $deviceFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios languages en el array languages
     *
     */
    public function testOk19()
    {
        $languageFactory = factory(\App\Models\tenant\Language::class)->create([]);
        $languageFactory2 = factory(\App\Models\tenant\Language::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => $languageFactory->id],
                '1' => [
                    'id' => $languageFactory2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_language_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_language', [
            'language_id' => $languageFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_language', [
            'language_id' => $languageFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con varios currencies en el array currencies
     *
     */
    public function testOk21()
    {
        $currencyFactory = factory(\App\Models\tenant\Currency::class)->create([]);
        $currencyFactory2 = factory(\App\Models\tenant\Currency::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => $currencyFactory->id],
                '1' => [
                    'id' => $currencyFactory2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_currency_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_currency', [
            'currency_id' => $currencyFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_currency', [
            'currency_id' => $currencyFactory2->id,
            'deleted_at' => null,
        ]);
    }



    /**
     *Test ok envia sólo requeridos con varios payment_methods en el array payment_methods
     *
     */
    public function testOk23()
    {

        $paymentFactory = factory(\App\Models\tenant\PaymentMethod::class)->create([]);
        $paymentFactory_2 = factory(\App\Models\tenant\PaymentMethod::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => $paymentFactory->id],
                '1' => [
                    'id' => $paymentFactory_2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_payment_method', [
            'payment_method_id' => $paymentFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_payment_method', [
            'payment_method_id' => $paymentFactory_2->id,
            'deleted_at' => null,
        ]);
    }



    /**
     *Test ok envia sólo requeridos con varios pickups en el array pickup
     *
     */
    public function testOk25()
    {
        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);
        $pickupFactory2 = factory(\App\Models\tenant\TransportationLocation::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => $pickupFactory->id],
                '1' => [
                    'id' => $pickupFactory2->id]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_pickup_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_pickup', [
            'pickup_id' => $pickupFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_pickup', [
            'pickup_id' => $pickupFactory2->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y registros en todos los arrays products, product_types, services, subchannels, countries, days, langs, currencies, pickups, payment_methods
     *
     */
    public function testOk26()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $paymentFactory = factory(\App\Models\tenant\PaymentMethod::class)->create([]);

        $countryFactory = factory(App\Models\tenant\Country::class)->create([]);

        $stateFactory = factory(App\Models\tenant\State::class)->create([]);

        $deviceFactory = factory(\App\Models\tenant\Device::class)->create([]);

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $pickupFactory = factory(App\Models\tenant\TransportationLocation::class)->create([]);

        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $promotionTypeFactory = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id, 'min_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotionTypeFactory->id, 'apply_client_type_filter' => 0]],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => $productTypeFactory->id, 'min_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotionTypeFactory->id, 'apply_client_type_filter' => 0]],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => $serviceFactory->id, 'min_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotionTypeFactory->id, 'apply_client_type_filter' => 0]],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => $subchannelFactory->id]],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => $countryFactory->id]],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => $stateFactory->id]],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => $deviceFactory->id]],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => $languageFactory->id]],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => $currencyFactory->id]],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => $paymentFactory->id]],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => $pickupFactory->id]],
            'apply_client_type_filter' => 0,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'service_id' => $serviceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_country', [
            'country_id' => $countryFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_device', [
            'device_id' => $deviceFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_language', [
            'language_id' => $languageFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_currency', [
            'currency_id' => $currencyFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_payment_method', [
            'payment_method_id' => $paymentFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_state', [
            'state_id' => $stateFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y asigna promoción a todos products, todos services, todos product_types, todos subchannels, todos countries,
     * todos days, todos langs, todos currencies, todos payment_methods, todos states
     *
     */
    public function testOk27()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_service_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_pickup_filter' => 0,
            'deleted_at' => null,
        ]);

    }

    /**
     *Test ok envia requeridos y filtro apply_product_filter a 0 para asignar promotion a todos los products
     *
     */
    public function testOk28()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_product_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'apply_product_filter' => 0,
            'apply_service_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_product_type_filter a 0 para asignar promotion a todos los product_types
     *
     */
    public function testOk29()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_product_type_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 0,
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_service_filter a 0 para asignar promotion a todos los service
     *
     */
    public function testOk30()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => '1',
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_service_filter' => 0,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_subchannel_filter a 0 para asignar promotion a todos los subchannel
     *
     */
    public function testOk31()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => '1',
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_subchannel_filter' => 0,
            'apply_service_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_country_filter a 0 para asignar promotion a todos los country
     *
     */
    public function testOk32()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => '1',
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_subchannel_filter' => 1,
            'apply_service_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_country_filter' => 0,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_state_filter a 0 para asignar promotion a todos los state
     *
     */
    public function testOk33()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => '1',
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_subchannel_filter' => 1,
            'apply_service_filter' => 1,
            'apply_product_filter' => 1,
            'apply_product_type_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 0,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_device_filter a 0 para asignar promotion a todos los days
     *
     */
    public function testOk34()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);


        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 0,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_language_filter a 0 para asignar promotion a todos los language
     *
     */
    public function testOk35()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_currency_filter a 0 para asignar promotion a todos los currencies
     *
     */
    public function testOk36()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_payment_method_filter a 0 para asignar promotion a todos los payment_methods
     *
     */
    public function testOk37()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 1,
            'pickups' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 1,
            'apply_payment_method_filter' => 0,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y filtro apply_pickup_filter a 0 para asignar promotion a todos los pickups de transportes
     *
     */
    public function testOk38()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,

            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => '']],
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => '']],
            'apply_subchannel_filter' => 1,
            'subchannels' => [
                '0' => [
                    'id' => '']],
            'apply_country_filter' => 1,
            'countries' => [
                '0' => [
                    'id' => '']],
            'apply_state_filter' => 1,
            'states' => [
                '0' => [
                    'id' => '']],
            'apply_device_filter' => 1,
            'devices' => [
                '0' => [
                    'id' => '']],
            'apply_language_filter' => 1,
            'languages' => [
                '0' => [
                    'id' => '']],
            'apply_currency_filter' => 1,
            'currencies' => [
                '0' => [
                    'id' => '']],
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => '']],
            'apply_payment_method_filter' => 1,
            'payment_methods' => [
                '0' => [
                    'id' => '']],
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'apply_product_filter' => 1,
            'apply_service_filter' => 1,
            'apply_subchannel_filter' => 1,
            'apply_country_filter' => 1,
            'apply_state_filter' => 1,
            'apply_device_filter' => 1,
            'apply_language_filter' => 1,
            'apply_currency_filter' => 1,
            'apply_pickup_filter' => 0,
            'apply_payment_method_filter' => 1,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia requeridos y campos de cupones
     *
     */
    public function testOk39()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [

            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'NAME ES', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'coupon_type' => 0,
            'coupon_code' => 'NUEVO CUPON',
            'coupon_sheet_start' => '0',
            'coupon_sheet_end' => 100,
            'coupon_total_uses' => 20,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'coupon_type' => 0,
            'coupon_code' => 'NUEVO CUPON',
            'coupon_sheet_start' => 0,
            'coupon_sheet_end' => 100,
            'coupon_total_uses' => 20,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'name' => 'NAME ES',
            'language_id' => 1,
            'description' => 'description ES',
            'short_description' => 'short_description ES',
            'title_seo' => 'title_Seo ES',
            'description_seo' => 'DESCRIPTION_SEO ES',
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion_translation', [
            'name' => 'NAME EN',
            'language_id' => 2,
            'description' => 'description EN',
            'short_description' => 'short_description EN',
            'title_seo' => 'title_Seo EN',
            'description_seo' => 'DESCRIPTION_SEO EN',
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion_translation', [
            'name' => 'NAME PT',
            'language_id' => 3,
            'description' => 'description PT',
            'short_description' => 'short_description PT',
            'title_seo' => 'title_Seo PT',
            'description_seo' => 'DESCRIPTION_SEO PT',
            'deleted_at' => null,
        ]);

    }

    /**
     *Test ok envia todos los parámetros posibles, parámetros de tipo fecha y booleanos como quantity
     *Test ok envia todos los parámetros posibles, parámetros de tipo fecha y booleanos como quantity
     *
     */
    public function testOk40()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [

            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'name' => ['ES' => 'NAME TEST 22', 'EN' => 'NAME EN TEST 22', 'PT' => 'NAME PT TEST 22'],
            'description' => ['ES' => 'description ES TEST 22', 'EN' => 'description EN TEST 22', 'PT' => 'description PT TEST 22'],
            'short_description' => ['ES' => 'short_description ES TEST 22', 'EN' => 'short_description EN TEST 22', 'PT' => 'short_description PT TEST 22'],
            'title_seo' => ['ES' => 'title_Seo ES TEST 22', 'EN' => 'title_Seo EN TEST 22', 'PT' => 'title_Seo PT TEST 22'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES TEST 22', 'EN' => 'DESCRIPTION_SEO EN TEST 22', 'PT' => 'DESCRIPTION_SEO PT TEST 22'],
            'order' => 1,
            'visibility' => 1,
            'accumulate' => 0,
            'min_quantity' => 2,
            'promotion_amount' => 20,
            'anticipation_start' => 10,
            'anticipation_end' => 15,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'order' => 1,
            'visibility' => 1,
            'accumulate' => 0,
            'min_quantity' => 2,
            'promotion_amount' => 20,
            'anticipation_start' => 10,
            'anticipation_end' => 15,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'name' => 'NAME TEST 22',
            'language_id' => 1,
            'description' => 'description ES TEST 22',
            'short_description' => 'short_description ES TEST 22',
            'title_seo' => 'title_Seo ES TEST 22',
            'description_seo' => 'DESCRIPTION_SEO ES TEST 22',
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion_translation', [
            'name' => 'NAME EN TEST 22',
            'language_id' => 2,
            'description' => 'description EN TEST 22',
            'short_description' => 'short_description EN TEST 22',
            'title_seo' => 'title_Seo EN TEST 22',
            'description_seo' => 'DESCRIPTION_SEO EN TEST 22',
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion_translation', [
            'name' => 'NAME PT TEST 22',
            'language_id' => 3,
            'description' => 'description PT TEST 22',
            'short_description' => 'short_description PT TEST 22',
            'title_seo' => 'title_Seo PT TEST 22',
            'description_seo' => 'DESCRIPTION_SEO PT TEST 22',
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos con un product en el array products
     *
     */
    public function testOk41()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [

            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'campaign_id' => $campaignFactory1->id,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,

        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_filter' => 1,
            'deleted_at' => null,
            'campaign_id' => $campaignFactory1->id
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de files a promociones
     *
     * Se crea factory de file, se asigna file al crear promocion y se comprueba en base de datos
     */
    public function testOk42()
    {

        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            'campaign_id' => $campaignFactory1->id,
            'files' => [0 => $fileFactory->id]])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion_file', [
            'file_id' => $fileFactory->id,
        ]);

    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de rangos de fechas a promociones
     *
     * Se crea factory de rangos de fechas de venta, de disfrute y de validez y se asigna promocion a los diferentes rangos
     */
    public function testOk43()
    {

        //crea campaña con comienzo un dia antes y fin dentro de 30 dias
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create([
            'date_start' => Carbon::now()->subDays(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addDays(30)->format('Y-m-d'),
        ]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'apply_product_filter' => 0,
            'name' => ['ES' => 'name'],
            'description' => ['ES' => 'description ES'],
            'short_description' => ['ES' => 'short_description ES'],
            'title_seo' => ['ES' => 'title_Seo ES'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'campaign_id' => $campaignFactory1->id,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
            //rango de promocion comienza hoy y dura hasta dentro de 3 dias
            'range_sale_date' => [0 => ['sale_date_start' => Carbon::now()->addDays(2)->format('Y-m-d H:i:s'), 'sale_date_end' => Carbon::now()->addDays(28)->format('Y-m-d H:i:s'), 'sale_days' => [0 => ['id' => 1]]]],
            'range_enjoy_date' => [0 => ['enjoy_date_start' => Carbon::now()->addDays(2)->format('Y-m-d H:i:s'), 'enjoy_date_end' => Carbon::now()->addDays(28)->format('Y-m-d H:i:s'), 'enjoy_days' => [0 => ['id' => 1]]]]
            ])
            ->assertExactJson(["error" => 200]);

    }

    /**
     *Test ok envia sólo requeridos y comprueba asociación de promotion_products a tipos de cliente
     *
     */
    public function testOk44()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $clientFactory = factory(App\Models\tenant\ClientType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id, 'min_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotion_type->id, 'apply_client_type_filter' => 1,
                    'client_types' => [0 => ['id' => $clientFactory->id]]]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_service_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'product_id' => $productFactory->id,
            'min_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => $promotion_type->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_client_type', [
            'client_type_id' => $clientFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos y comprueba asociación de promotion_services a tipos de cliente
     *
     */
    public function testOk45()
    {

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);
        $clientFactory = factory(App\Models\tenant\ClientType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_type_filter' => 1,
            'product_types' => [
                '0' => [
                    'id' => $productTypeFactory->id, 'min_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' => $promotion_type->id, 'apply_client_type_filter' => 1,
                    'client_types' => [0 => ['id' => $clientFactory->id]]]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_service_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_product_type_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'product_type_id' => $productTypeFactory->id,
            'min_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' => $promotion_type->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type_client_type', [
            'client_type_id' => $clientFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Test ok envia sólo requeridos y comprueba asociación de promotion_services a tipos de cliente
     *
     */
    public function testOk46()
    {

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);
        $clientFactory = factory(App\Models\tenant\ClientType::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $promotion_type = factory(\App\Models\tenant\PromotionType::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_service_filter' => 1,
            'services' => [
                '0' => [
                    'id' => $serviceFactory->id, 'min_quantity' => 2, 'promotion_amount' => 15, 'promotion_type_id' =>$promotion_type->id, 'apply_client_type_filter' => 1,
                    'client_types' => [0 => ['id' => $clientFactory->id]]]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_product_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        $this->assertDatabaseHas('mo_promotion', [
            'code' => 'NUEVO',
            'apply_service_filter' => 1,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'service_id' => $serviceFactory->id,
            'min_quantity' => 2,
            'promotion_amount' => 15,
            'promotion_type_id' =>$promotion_type->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service_client_type', [
            'client_type_id' => $clientFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     *Prueba automatizada enviando benefit card con benefit card id
     *
     */
    public function testOk47()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $identification = factory(\App\Models\tenant\Identification::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

            'benefit_card' => 1,
            'benefit_card_id' => $identification->id,

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,


        ])
            ->assertExactJson([
                "error"=>200,

            ]);
    }

    /**
     *Prueba automatizada enviando benefit card con benefit card id y benefit card total uses
     *
     */
    public function testOk48()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);

        $identification = factory(\App\Models\tenant\Identification::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,

            'benefit_card' => 1,
            'benefit_card_id' => $identification->id,
            'benefit_card_total_uses' => 3,

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,


        ])
            ->assertExactJson([
                "error"=>200,
            ]);
    }

    /**
     *Prueba automatizada enviando accumulate con accumulate_promotion
     *
     */
    public function testOk49()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $identification = factory(\App\Models\tenant\Identification::class)->create([]);

        $deparment_factory = factory(\App\Models\tenant\Department::class)->create([]);
        $cost_center_factory = factory(\App\Models\tenant\CostCenter::class)->create(['department_id' => $deparment_factory->id]);
        $promotion_factory = factory(\App\Models\tenant\Promotion::class)->create([]);

        $this->json('POST', '/api/v1/promotion', [
            'user_id' => $user_factory->id,
            'code' => 'NUEVO',
            'promotion_type_id' => 1,
            'campaign_id' => $campaignFactory->id,
            'apply_product_filter' => 1,
            'products' => [
                '0' => [
                    'id' => $productFactory->id,
                    'apply_client_type_filter' => 0]],
            'name' => ['ES' => 'name', 'EN' => 'NAME EN', 'PT' => 'NAME PT'],
            'description' => ['ES' => 'description ES', 'EN' => 'description EN', 'PT' => 'description PT'],
            'short_description' => ['ES' => 'short_description ES', 'EN' => 'short_description EN', 'PT' => 'short_description PT'],
            'title_seo' => ['ES' => 'title_Seo ES', 'EN' => 'title_Seo EN', 'PT' => 'title_Seo PT'],
            'description_seo' => ['ES' => 'DESCRIPTION_SEO ES', 'EN' => 'DESCRIPTION_SEO EN', 'PT' => 'DESCRIPTION_SEO PT'],
            'apply_country_filter' => 0,
            'apply_state_filter' => 0,
            'apply_product_type_filter' => 0,
            'apply_subchannel_filter' => 0,
            'apply_payment_method_filter' => 0,
            'apply_device_filter' => 0,
            'apply_language_filter' => 0,
            'apply_currency_filter' => 0,
            'apply_service_filter' => 0,
            'apply_pickup_filter' => 0,
            'apply_client_type_filter' => 0,
            'accumulate' => 1,
            'promotion_accumulate' => [
                0 => ['id' => $promotion_factory->id],
            ],

            'department_id' => $deparment_factory->id ,
            'cost_center_id' => $cost_center_factory->id,
        ])
            ->assertExactJson([

                "error"=>200,
            ]);
    }

}