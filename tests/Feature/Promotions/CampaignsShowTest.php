<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CampaignsShowTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de campañas con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/promotion/campaign', [
            'page' => 'string',
            'limit' => 'string',
            'lang' => 'eeeeeeee',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."],"lang" => ["The lang is not exists"]]]]);
    }


    /**
     * test consulta de campañas con parametros en formato incorrecto
     */
    public function testBad3()
    {
        $this->json('GET', '/api/v1/promotion/campaign', [
            'active' => 27,
            'subchannel_id' => 123456,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["active" => ["The active field must be true or false."],
                    "subchannel_id" => ["The selected subchannel id is invalid."]]]]);
    }

    /**
     * test crea factory de campaña con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de campaña y sus traducciones
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create(['active' => 1]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
            'active' => 1,
        ]);

        $campaignTranslationFactory1 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name español test 2',
            'description' => 'description español test 2',
        ]);

        $campaignTranslationFactory2 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 2,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name ingles test 2',
            'description' => 'description ingles test 2',
        ]);

        $campaignTranslationFactory3 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 3,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name portugues test 2',
            'description' => 'description portugues test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory2->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory2->name,
            'description' => $campaignTranslationFactory2->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory3->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory3->name,
            'description' => $campaignTranslationFactory3->description,
        ]);


        $this->json('GET', '/api/v1/promotion/campaign', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $campaignFactory1->id,
                'name' => $campaignTranslationFactory1->name,
                'name' => $campaignTranslationFactory2->name,
                'name' => $campaignTranslationFactory3->name,
                'description' => $campaignTranslationFactory1->description,
                'description' => $campaignTranslationFactory2->description,
                'description' => $campaignTranslationFactory3->description,
                'active' => 1

            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/promotion/campaign', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['campaign' => ["0" => ['id', 'lang']]]]
            ]);

    }


}