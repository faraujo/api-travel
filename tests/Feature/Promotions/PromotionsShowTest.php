<?php

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class PromotionsShowTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de promociones con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/promotion', [
            'page' => 'poi',
            'limit' => 'poi',
            'visibility' => 'poi',
            'accumulate' => 'poi',
            'promotion_type_id' => '9999999999999999',
            'apply_product_filter' => 'poi',
            'apply_product_type_filter' => 'poi',
            'apply_service_filter' => 'poi',
            'apply_subchannel_filter' => 'poi',
            'apply_country_filter' => 'poi',
            'apply_state_filter' => 'poi',
            'apply_pickup_filter' => 'poi',
            'anticipation_start' => 'poi',
            'anticipation_end' => 'poi',
            //'sale_date' => 'poi',
            //'enjoy_date' => 'poi',
            //'validity_date' => 'poi',
            'coupon_type' => 'poi',
            'coupon_sheet_start' => 'poi',
            'coupon_sheet_end' => 'poi',
            'coupon_total_uses' => 'poi',
            'product_id' => '999999999999999999999999999',
            'service_id' => '999999999999999999999999999',
            'country_id' => '999999999999999999999999999',
            'product_type_id' => '99999999999999999999999999999999',
            'campaign_id' => 'r',
        ])
            ->assertExactJson([
                "error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                    "accumulate" => ["The accumulate field must be true or false."],
                    "anticipation_end" => ["The anticipation end must be an integer."],
                    "anticipation_start" => ["The anticipation start must be an integer."],
                    "apply_subchannel_filter" => ["The apply subchannel filter field must be true or false."],
                    "apply_country_filter" => ["The apply country filter field must be true or false."],
                    "apply_pickup_filter" => ["The apply pickup filter field must be true or false."],
                    "apply_product_filter" => ["The apply product filter field must be true or false."],
                    "apply_product_type_filter" => ["The apply product type filter field must be true or false."],
                    "apply_service_filter" => ["The apply service filter field must be true or false."],
                    "apply_state_filter" => ["The apply state filter field must be true or false."],
                    "campaign_id" => ["The campaign id must be an integer."],
                    "country_id" => ["The country id must be an integer."],
                    "coupon_sheet_end" => ["The coupon sheet end must be an integer."],
                    "coupon_sheet_start" => ["The coupon sheet start must be an integer."],
                    "coupon_total_uses" => ["The coupon total uses must be an integer."],
                    "coupon_type" => ["The coupon type field must be true or false."],
                    //"enjoy_date" => ["The enjoy date does not match the format Y-m-d.", "The enjoy date is not a valid date."],
                    "limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."],
                    "product_id" => ["The product id must be an integer."],
                    "product_type_id" => ["The product type id must be an integer."],
                    "promotion_type_id" => ["The selected promotion type id is invalid."],
                    //"sale_date" => ["The sale date does not match the format Y-m-d.", "The sale date is not a valid date."],
                    "service_id" => ["The service id must be an integer."],
                    //"validity_date" => ["The validity date does not match the format Y-m-d.", "The validity date is not a valid date."],
                    "visibility" => ["The visibility field must be true or false."]]]
            ]);
    }

    /**
     * test consulta de promociones con parametros inexistentes en base de datos
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/promotion', [
            'product_id' => 999999999,
            'service_id' => 999999999,
            'product_type_id' => 999999999,
            'promotion_type_id' => 999999999,
            'campaign_id' => 999999999,
            'pickup_id' => 999999999,
            'currency_id' => 999999999,
            'day_id' => 999999999,
            'language_id' => 999999999,
            'payment_method_id' => 999999999,
            'country_id' => 999999999,
            'state_id' => 999999999,
            'client_type_id' => 999999999,
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["campaign_id"=>["The selected campaign id is invalid."],"client_type_id"=>["The selected client type id is invalid."],"country_id"=>["The selected country id is invalid."],
                "currency_id"=>["The selected currency id is invalid."],"day_id"=>["The selected day id is invalid."],
                "language_id"=>["The selected language id is invalid."],"payment_method_id"=>["The selected payment method id is invalid."],
                "pickup_id"=>["The selected pickup id is invalid."],"product_id"=>["The selected product id is invalid."],
                "product_type_id"=>["The selected product type id is invalid."],"promotion_type_id"=>["The selected promotion type id is invalid."],
                "service_id"=>["The selected service id is invalid."],"state_id"=>["The selected state id is invalid."]]]]);
    }


    /**
     * test de consulta de promociones sin parametros
     */
    public function testOk1()
    {

        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([
            'date_start' => '2099-10-10',
            'date_end' => '2099-10-20',
        ]);

        $campaignFactoryTranslation = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaignFactory->id,
            'name' => 'nombre campaña test',
        ]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaignFactory->id
        ]);


        $promotionTranslationFactory = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 1,
        ]);

        $promotionTranslationFactory2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 2,
        ]);

        $promotionTranslationFactory3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 3,
        ]);

        $this->json('GET', '/api/v1/promotion', ['limit' => 0])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $promotionFactory->id,
            ]);
    }

    /**
     * test de consulta de promociones con parametro product_id
     */
    public function testOk2()
    {
        //asocia promocion a tipo de promocion para prueba filtro
        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);
        //asocia promocion a campaña para prueba filtro
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'promotion_type_id' => $promotionTypeFactory->id,
            'campaign_id' => $campaignFactory->id,
        ]);

        $campaignFactoryTranslation = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaignFactory->id,
            'name' => 'nombre campaña test',
        ]);


        $promotionTranslationFactory = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 1,
        ]);

        $promotionTranslationFactory2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 2,
        ]);

        $promotionTranslationFactory3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 3,
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        factory(App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        factory(App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotionFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $productTypeFactory = factory(\App\Models\tenant\ProductType::class)->create();

        $promotionProductTypeFactory = factory(App\Models\tenant\PromotionProductType::class)->create([
            'product_type_id' => $productTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $deviceFactory = factory(App\Models\tenant\Device::class)->create([]);

        $promotionDeviceFactory = factory(App\Models\tenant\PromotionDevice::class)->create([
            'promotion_id' => $promotionFactory->id,
            'device_id' => $deviceFactory->id,
        ]);

        $paymentMethod = factory(App\Models\tenant\PaymentMethod::class)->create([]);

        $promotionPaymenMethodFactory = factory(App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotionFactory->id,
            'payment_method_id' => $paymentMethod->id,
        ]);

        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $promotionCurrencyFactory = factory(App\Models\tenant\PromotionCurrency::class)->create([
            'promotion_id' => $promotionFactory->id,
            'currency_id' => $currencyFactory->id,
        ]);

        $pickup = factory(App\Models\tenant\TransportationLocation::class)->create([]);

        $promotionPickupFactory = factory(App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotionFactory->id,
            'pickup_id' => $pickup->id,
        ]);

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $promotionLanguageFactory = factory(App\Models\tenant\PromotionLanguage::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $stateFactory = factory(App\Models\tenant\State::class)->create([]);

        $promotionStateFactory = factory(App\Models\tenant\PromotionState::class)->create([
            'promotion_id' => $promotionFactory->id,
            'state_id' => $stateFactory->id,
        ]);

        $countryFactory = factory(App\Models\tenant\Country::class)->create([]);

        $promotionCountryFactory = factory(App\Models\tenant\PromotionCountry::class)->create([
            'promotion_id' => $promotionFactory->id,
            'country_id' => $countryFactory->id,
        ]);

        $clientTypeFactory = factory(App\Models\tenant\ClientType::class)->create([]);

        $promotionClientTypeFactory = factory(App\Models\tenant\PromotionClientType::class)->create([
            'promotion_id' => $promotionFactory->id,
            'client_type_id' => $clientTypeFactory->id,
        ]);

        $this->json('GET', '/api/v1/promotion', [
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'promotion_type_id' => $promotionTypeFactory->id,
            'device_id' => $deviceFactory->id,
            'payment_method_id' => $paymentMethod->id,
            'currency_id' => $currencyFactory->id,
            'pickup_id' => $pickup->id,
            'language_id' => $languageFactory->id,
            'campaign_id' => $campaignFactory->id,
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'client_type_id' => $clientTypeFactory->id,
        ])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $promotionFactory->id,
            ]);
    }


    /**
     * test consulta de promociones con filtro sale_date, enjoy_date y validity_date
     */
    public function testOk15()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([
            'date_start' => '2099-10-10',
            'date_end' => '2099-10-20',
        ]);

        $campaignFactoryTranslation = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaignFactory->id,
            'name' => 'nombre campaña test',
        ]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaignFactory->id,
        ]);

        $promotionTranslationFactory = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 1,
        ]);

        $promotionTranslationFactory2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 2,
        ]);

        $promotionTranslationFactory3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 3,
        ]);

        $promotionRangeSale = factory(App\Models\tenant\PromotionRangeSaleDay::class)->create([
            'promotion_id' => $promotionFactory->id,
            'sale_date_start' => '2099-10-11 00:00:00',
            'sale_date_end' => '2099-10-19 00:00:00',
        ]);

        $this->json('GET', '/api/v1/promotion', [
            'range_sale_date_start' => '2099-10-11',
            'range_sale_date_end' => '2099-10-19',
        ])->assertJsonFragment([
            'error' => 200,
            'id' => $promotionFactory->id,
            'total_results' => 1,
        ]);

        $promotionRangeEnjoy = factory(App\Models\tenant\PromotionRangeEnjoyDay::class)->create([
            'promotion_id' => $promotionFactory->id,
            'enjoy_date_start' => '2099-10-11 12:00:00',
            'enjoy_date_end' => '2099-10-19 12:00:00',
        ]);

        $this->json('GET', '/api/v1/promotion', [
            'enjoy_date_start' => '2099-10-11',
            'enjoy_date_end' => '2099-10-19',
        ])->assertJsonFragment([
            'error' => 200,
            'id' => $promotionFactory->id,
            'total_results' => 1,
        ]);

    }

    /**
     * test consulta de promociones con filtro campaña (nombre)
     */
    public function testOk16()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([
            'date_start' => '2099-10-10',
            'date_end' => '2099-10-20',
        ]);

        $campaignFactoryTranslation = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'campaign_id' => $campaignFactory->id,
            'name' => 'nombre campaña test',
        ]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'campaign_id' => $campaignFactory->id,
        ]);

        $promotionTranslationFactory = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 1,
        ]);

        $promotionTranslationFactory2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 2,
        ]);

        $promotionTranslationFactory3 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => 3,
        ]);


        $this->json('GET', '/api/v1/promotion', [
            'campaign' => 'campaña test'
        ])->assertJsonFragment([
            'error' => 200,
            'id' => $promotionFactory->id,
            'total_results' => 1,
        ]);

    }

}