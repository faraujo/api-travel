<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class PromotionsShowTypesTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test mostrar tipos de promociones idioma incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/promotion/types', [
            'lang' => 'oi',
        ])->assertExactJson([
            'error' => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [[
                "lang" => ["The lang is not exists"]
            ]]
        ]);
    }

    /**
     * test consulta de tipos de promociones sin parametros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/promotion/types', [])
            ->assertJsonStructure(
                ['error', 'data']
            );
    }

    /**
     * test consulta de tipos de promociones con idioma correcto
     */
    public function testOk2()
    {
        $this->json('GET', '/api/v1/promotion/types', [
            'lang' => 'ES',
        ])->assertJsonStructure([
            'error',
            'data',
            'total_results',
        ]);
    }

    /**
     * test consulta de tipos de promociones con parametros validos
     */
    public function testOk3()
    {
        $this->json('GET', '/api/v1/promotion/types', [
            'page' => 1,
            'limit' => 1,
            'lang' => 'ES',
        ])->assertJsonStructure([
            'error',
            'data',
            'total_results',
        ]);
    }
}