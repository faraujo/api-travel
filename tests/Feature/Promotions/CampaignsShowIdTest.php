<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class CampaignsShowIdTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta campaña inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/promotion/campaign/9999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar campaign sin traducciones, devuelve json vacío
     */
    public function testOk2()
    {
        $campaignFactory = factory(App\Models\tenant\Campaign::class)->create([]);

        $this->json('GET', '/api/v1/promotion/campaign/' . $campaignFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta campaña existente
     */
    public function testOk3()
    {
        //Factory de campaña y sus traducciones
        $campaignFactory1 = factory(App\Models\tenant\Campaign::class)->create(['active' => 1]);

        $this->assertDatabaseHas('mo_campaign', [
            'id' => $campaignFactory1->id,
            'active' => 1
        ]);

        $campaignTranslationFactory1 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 1,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name español test 2',
            'description' => 'description español test 2',
        ]);

        $campaignTranslationFactory2 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 2,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name ingles test 2',
            'description' => 'description ingles test 2',
        ]);

        $campaignTranslationFactory3 = factory(App\Models\tenant\CampaignTranslation::class)->create([
            'language_id' => 3,
            'campaign_id' => $campaignFactory1->id,
            'name' => 'name portugues test 2',
            'description' => 'description portugues test 2',
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory1->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory1->name,
            'description' => $campaignTranslationFactory1->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory2->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory2->name,
            'description' => $campaignTranslationFactory2->description,
        ]);

        $this->assertDatabaseHas('mo_campaign_translation', [
            'language_id' => $campaignTranslationFactory3->language_id,
            'campaign_id' => $campaignFactory1->id,
            'name' => $campaignTranslationFactory3->name,
            'description' => $campaignTranslationFactory3->description,
        ]);
        $this->json('GET', '/api/v1/promotion/campaign/' . $campaignFactory1->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $campaignFactory1->id,
                'name' => $campaignTranslationFactory1->name,
                'name' => $campaignTranslationFactory2->name,
                'name' => $campaignTranslationFactory3->name,
                'description' => $campaignTranslationFactory1->description,
                'description' => $campaignTranslationFactory2->description,
                'description' => $campaignTranslationFactory3->description,
                'active' => 1,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/promotion/campaign/' . $campaignFactory1->id, [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['campaign' => ["0" => ['id', 'lang']]]]
            ]);
    }

}