<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class PromotionsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test sin enviar parametros requeridos
     */

    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/promotion')->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [["ids" => ["The ids field is required."]]]]);
    }

    /**
     * Test enviando parametros requeridos en tipo incorrecto
     */

    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => ['r'],

        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test enviando parametros requeridos no existente
     */

    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [99999999999999],

        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found', "error_inputs" => [["ids.0" => ["The selected ids.0 is invalid."]]]]);
    }


    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => $promotionFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }



    /**
     * Test para elminar promociones
     */

    public function testOk1()
    {

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);


        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);


        //Consume el servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     * Test para eliminar promociones con productos asociados
     */

    public function testOk2()
    {
        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        //Consumo de servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     * test para eliminar promociones con tipo de productos asociados
     */

    public function testOk3()
    {

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        //Consumo de servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);


        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_product_type', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     * test para elminar promociones con servicios asociados
     */

    public function testOk4()
    {
        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $productService = factory(App\Models\tenant\Service::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        //Consumo de servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);


        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_service', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     * test para eliminar promociones con productos y servicios asociados
     */

    public function testOk5()
    {
        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $productService = factory(App\Models\tenant\Service::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionServiceFactory = factory(App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'id' => $promotionServiceFactory->id,
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        //Consumo de servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);


        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_service', [
            'id' => $promotionServiceFactory->id,
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     * test para eliminar promociones con tipo de product y servicios asociados
     */

    public function testOk6()
    {

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);

        $productService = factory(App\Models\tenant\Service::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);


        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $promotionProductTypeFactory = factory(App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
        ]);

        $promotionServiceFactory = factory(App\Models\tenant\PromotionService::class)->create([
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
        ]);


        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_service', [
            'id' => $promotionServiceFactory->id,
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        //Consumo de servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);


        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_service', [
            'id' => $promotionServiceFactory->id,
            'promotion_id' => $promotionFactory->id,
            'service_id' => $productService->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     * test para eliminar promociones con productos y tipos de productos asociados
     */
    public function testOk7()
    {
        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $productTypeFactory = factory(App\Models\tenant\ProductType::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $promotionProductTypeFactory = factory(App\Models\tenant\PromotionProductType::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
        ]);

        $promotionProductFactory = factory(App\Models\tenant\PromotionProduct::class)->create([
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
        ]);


        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);

        //Consumo de servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);


        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_product_type', [
            'id' => $promotionProductTypeFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_type_id' => $productTypeFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_product', [
            'id' => $promotionProductFactory->id,
            'promotion_id' => $promotionFactory->id,
            'product_id' => $productFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
    }

    /**
     * test para eliminar promociones con idiomas, moneda, método de pago y pickup asociados
     */
    public function testOk8()
    {
        //$dayFactory = factory(App\Day::class)->create([]);

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $currencyFactory = factory(App\Models\tenant\Currency::class)->create([]);

        $paymentMethod = factory(App\Models\tenant\PaymentMethod::class)->create([]);

        $pickupFactory = factory(App\Models\tenant\TransportationLocation::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        /*$promotionDayFactory = factory(App\PromotionDay::class)->create([
            'promotion_id' => $promotionFactory->id,
            'day_id' => $dayFactory->id,
        ]);*/

        $promotionLanguageFactory = factory(App\Models\tenant\PromotionLanguage::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);

        $promotionPaymenMethodFactory = factory(App\Models\tenant\PromotionPaymentMethod::class)->create([
            'promotion_id' => $promotionFactory->id,
            'payment_method_id' => $paymentMethod->id,
        ]);

        $promotionCurrencyFactory = factory(App\Models\tenant\PromotionCurrency::class)->create([
            'promotion_id' => $promotionFactory->id,
            'currency_id' => $currencyFactory->id,
        ]);

        $promotionPickupFactory = factory(App\Models\tenant\PromotionPickup::class)->create([
            'promotion_id' => $promotionFactory->id,
            'pickup_id' => $pickupFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        /*$this->assertDatabaseHas('mo_promotion_day', [
            'id' => $promotionDayFactory->id,
            'promotion_id' => $promotionFactory->id,
            'day_id' => $dayFactory->id,
            'deleted_at' => null,
        ]);*/

        $this->assertDatabaseHas('mo_promotion_language', [
            'id' => $promotionLanguageFactory->id,
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_payment_method', [
            'id' => $promotionPaymenMethodFactory->id,
            'promotion_id' => $promotionFactory->id,
            'payment_method_id' => $paymentMethod->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_currency', [
            'id' => $promotionCurrencyFactory->id,
            'promotion_id' => $promotionFactory->id,
            'currency_id' => $currencyFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_pickup', [
            'id' => $promotionPickupFactory->id,
            'promotion_id' => $promotionFactory->id,
            'pickup_id' => $pickupFactory->id,
            'deleted_at' => null,
        ]);

        //Consumo de servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id],
        ])->assertExactJson(['error' => 200]);


        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);

        /*$this->assertDatabaseMissing('mo_promotion_day', [
            'id' => $promotionDayFactory->id,
            'promotion_id' => $promotionFactory->id,
            'day_id' => $dayFactory->id,
            'deleted_at' => null,
        ]);*/

        $this->assertDatabaseMissing('mo_promotion_language', [
            'id' => $promotionLanguageFactory->id,
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_payment_method', [
            'id' => $promotionPaymenMethodFactory->id,
            'promotion_id' => $promotionFactory->id,
            'payment_method_id' => $paymentMethod->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_currency', [
            'id' => $promotionCurrencyFactory->id,
            'promotion_id' => $promotionFactory->id,
            'currency_id' => $currencyFactory->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_pickup', [
            'id' => $promotionPickupFactory->id,
            'promotion_id' => $promotionFactory->id,
            'pickup_id' => $pickupFactory->id,
            'deleted_at' => null,
        ]);

    }


    /**
     * Test para elminar dos promociones (con traducciones y subcanal)
     */
    public function testOk9()
    {

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);
        $languageFactory2 = factory(App\Models\tenant\Language::class)->create([]);

        $promotionTypeFactory = factory(App\Models\tenant\PromotionType::class)->create([]);
        $promotionTypeFactory2 = factory(App\Models\tenant\PromotionType::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);
        $subchannelFactory2 = factory(App\Models\tenant\SubChannel::class)->create([]);

        $promotionFactory = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA001',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory->id,
        ]);
        $promotionFactory2 = factory(App\Models\tenant\Promotion::class)->create([
            'code' => 'AAA002',
            'order' => 4,
            'visibility' => 0,
            'accumulate' => 0,
            'promotion_amount' => 0,
            'promotion_type_id' => $promotionTypeFactory2->id,
        ]);

        $promotionTranslation = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory->id,
            'language_id' => $languageFactory->id,
        ]);
        $promotionTranslation2 = factory(App\Models\tenant\PromotionTranslation::class)->create([
            'promotion_id' => $promotionFactory2->id,
            'language_id' => $languageFactory2->id,
        ]);

        $promotionChannelFactory = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);
        $promotionChannelFactory2 = factory(App\Models\tenant\PromotionSubchannel::class)->create([
            'promotion_id' => $promotionFactory2->id,
            'subchannel_id' => $subchannelFactory2->id,
        ]);


        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion', [
            'id' => $promotionFactory2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory2->id,
            'promotion_id' => $promotionFactory2->id,
            'subchannel_id' => $subchannelFactory2->id,
            'deleted_at' => null,
        ]);


        //Consume el servicio delete
        $this->json('DELETE', '/api/v1/promotion', [
            'ids' => [$promotionFactory->id, $promotionFactory2 -> id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory->id,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseMissing('mo_promotion', [
            'id' => $promotionFactory2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation->id,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseMissing('mo_promotion_translation', [
            'id' => $promotionTranslation2->id,
            'deleted_at' => null,
        ]);

        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory->id,
            'promotion_id' => $promotionFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseMissing('mo_promotion_subchannel', [
            'id' => $promotionChannelFactory2->id,
            'promotion_id' => $promotionFactory2->id,
            'subchannel_id' => $subchannelFactory2->id,
            'deleted_at' => null,
        ]);
    }

}