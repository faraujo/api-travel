<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PricesCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parámetros
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/price', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "contract_model_id"=>["The contract model id field is required."],
                "date_end"=>["The date end field is required."],
                "currency_id" => ["The currency id field is required."],
                "date_start"=>["The date start field is required."],
                "user_id"=>["The user id field is required."]]]]);
    }

    /**
     * Prueba automatizada formato parámetros incorrectos
     */
    public function testBad2()
    {
        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => 'asfd',
            'date_end' => 'asfd',
            'date_start' => 'asfd',
            'user_id' => 'asfd',
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => '',
                                    'markup' => '',
                                    'sale_price' => '',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[[
                "contract_model_id"=>["The contract model id must be a number."],
                "currency_id" => ["The currency id field is required."],
                "date_end"=>["The date end does not match the format Y-m-d.","The date end is not a valid date.","The date end must be a date after or equal to date start."],
                "date_start"=>["The date start does not match the format Y-m-d.","The date start is not a valid date."],
                "products.0.locations.0.client_types.0.markup"=>["The products.0.locations.0.client_types.0.markup field is required when none of products.0.locations.0.client_types.0.sale_price / products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.net_price"=>["The products.0.locations.0.client_types.0.net_price field is required when none of products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.sale_price"=>["The products.0.locations.0.client_types.0.sale_price field is required when none of products.0.locations.0.client_types.0.markup / products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.services"=>["The products.0.locations.0.client_types.0.services field is required when none of products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.net_price are present."],
                "products.0.locations.0.client_types.0.sessions"=>["The products.0.locations.0.client_types.0.sessions field is required when none of products.0.locations.0.client_types.0.sale_price / products.0.locations.0.client_types.0.markup / products.0.locations.0.client_types.0.net_price / products.0.locations.0.client_types.0.services are present."],
                "user_id"=>["The selected user id is invalid."]]]]);
    }

    /**
     * Prueba automatizada formato parámetros vacíos
     */
    public function testBad3()
    {
        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => '',
                                    'markup' => '',
                                    'sale_price' => '',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[[
                "contract_model_id"=>["The contract model id field is required."],
                "date_end"=>["The date end field is required."],
                "currency_id" => ["The currency id field is required."],
                "date_start"=>["The date start field is required."],
                "products.0.locations.0.client_types.0.markup"=>["The products.0.locations.0.client_types.0.markup field is required when none of products.0.locations.0.client_types.0.sale_price / products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.net_price"=>["The products.0.locations.0.client_types.0.net_price field is required when none of products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.sale_price"=>["The products.0.locations.0.client_types.0.sale_price field is required when none of products.0.locations.0.client_types.0.markup / products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.services"=>["The products.0.locations.0.client_types.0.services field is required when none of products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.net_price are present."],
                "products.0.locations.0.client_types.0.sessions"=>["The products.0.locations.0.client_types.0.sessions field is required when none of products.0.locations.0.client_types.0.sale_price / products.0.locations.0.client_types.0.markup / products.0.locations.0.client_types.0.net_price / products.0.locations.0.client_types.0.services are present."],
                "user_id"=>["The user id field is required."]]]]);
    }

    /**
     * Prueba automatizada fechas repetidas
     *
     */
    public function testBad4()
    {

        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-27',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);

    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testBad5()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2100-01-01',
            'date_start' => '2099-12-27',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testBad6()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'currency_id' => 1,
            'date_start' => '2099-12-26', 'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testBad7()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2100-01-01',
            'date_start' => '2099-12-29',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testBad8()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-29',
            'date_start' => '2099-12-28',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testBad9()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'currency_id' => 1,
            'date_start' => '2099-12-28', 'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada productos repetidos
     */
    public function testBad10()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $user = factory(App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-28',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                        ],
                    ],
                ],
                1 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                        ],
                    ],
                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                        ],
                    ],
                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            "products.1" => ["The products.1.id field has a duplicate value."],
            "products.0.locations.0.client_types" => ["The products.0.locations.0.client_types field is required."],
            "products.1.locations.0.client_types" => ["The products.1.locations.0.client_types field is required."],
            "products.2.locations.0.client_types" => ["The products.2.locations.0.client_types field is required."],


        ]]]);
    }

    /**
     * test de creacion de tarifa con array de products y service sin id
     */
    public function testBad11()
    {
        $user = factory(App\Models\tenant\User::class)->create([]);
        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-28',
            'promotions' => 0,
            'user_id' => $user->id,
            'currency_id' => 1,
            'products' =>
                [0 => [
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'services' => [
                                        0 => [
                                            'net_price' => 10,
                                            'sale_price' => 12,
                                        ]
                                    ]
                                ]
                            ]
                        ],
                    ],
                ]],
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    "products.0.id" => ["The products.0.id field is required."],
                    "products.0.locations.0.client_types.0.services.0.id" => ["The products.0.locations.0.client_types.0.services.0.id field is required."]
                ]]
            ]);
    }


    /**
     * test crear tarifa con campo user id inexistente en base de datos
     */
    public function testBad12()
    {
        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $product = factory(\App\Models\tenant\Product::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-28',
            'promotions' => 0,
            'user_id' => 99999999999999,
            'currency_id' => 1,
            'products' =>
                [0 => [
                    'id' => $product->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                        ],
                    ],
                ]],
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    "user_id" => ["The selected user id is invalid."],
                    "products.0.locations.0.client_types" => ["The products.0.locations.0.client_types field is required."]]]]);
    }

    /**
     * test prueba crear tarifa con tipo de dato product id invalido
     */
    public function testBad13()
    {
        $user = factory(App\Models\tenant\User::class)->create([]);
        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);
        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-28',
            'promotions' => 0,
            'user_id' => $user->id,
            'currency_id' => 1,
            'products' =>
                [0 => [
                    'id' => 'asdf',
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ]],
        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    "products.0.id" => ["The products.0.id must be an integer."],]]]);
    }


    /**
     * test prueba crear tarifa con tipo de dato service id invalido
     */
    public function testBad14()
    {
        $user = factory(App\Models\tenant\User::class)->create([]);
        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $product_factory = factory(\App\Models\tenant\Product::class)->create([

        ]);
        $this->json('POST', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-28',
            'promotions' => 0,
            'user_id' => $user->id,
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product_factory->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'services' => [
                                        0 => [
                                            'id' => 'test',
                                            'net_price' => 3,
                                            'sale_price' => 3,
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]

        ])
            ->assertExactJson([
                'error' => 400,
                'error_description' => 'The fields are not the required format',
                'error_inputs' => [[
                    "products.0.locations.0.client_types.0.services.0.id" => ["The products.0.locations.0.client_types.0.services.0.id must be an integer."]]]]);
    }

    public function testBad15()
    {

        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([
        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([
        ]);

        $user = factory(App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);
        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);
        $this->json('POST', '/api/v1/price', [

            'contract_model_id' => $contract_model->id,
            'date_end' => '2098-12-31',
            'date_start' => '2098-12-30',
            'user_id' => $user->id,
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                    'tip_amount' => 'TEST',
                                    'tip_percent' => 'TEST',
                                    'commission_amount' => 'TEST',
                                    'commission_percent' => 'TEST',
                                    'sessions' => [
                                        0 => [
                                            'session' => '10:00',
                                            'net_price' => 10,
                                            'sale_price' => 5,
                                            'tip_amount' => 'TEST',
                                            'tip_percent' => 'TEST',
                                            'commission_amount' => 'TEST',
                                            'commission_percent' => 'TEST',
                                        ]
                                    ],
                                    'services' => [
                                        0 => [
                                            'id' => 1,
                                            'net_price' => 15,
                                            'sale_price' => 25,
                                            'tip_amount' => 'TEST',
                                            'tip_percent' => 'TEST',
                                            'commission_amount' => 'TEST',
                                            'commission_percent' => 'TEST',
                                            'pickups' => [
                                                0 => [
                                                    'id' => $pickupFactory->id,
                                                    'net_price' => 20,
                                                    'sale_price' => 30,
                                                    'tip_amount' => 'TEST',
                                                    'tip_percent' => 'TEST',
                                                    'commission_amount' => 'TEST',
                                                    'commission_percent' => 'TEST',
                                                ]
                                            ]
                                        ],
                                        1 => [
                                            'id' => $service_factory->id,
                                            'net_price' => 60,
                                            'sale_price' => 65,
                                            'tip_amount' => 'TEST',
                                            'tip_percent' => 'TEST',
                                            'commission_amount' => 'TEST',
                                            'commission_percent' => 'TEST',
                                            'sessions' => [
                                                0 => [
                                                    'session' => '12:00',
                                                    'net_price' => 85,
                                                    'sale_price' => 100,
                                                    'tip_amount' => 'TEST',
                                                    'tip_percent' => 'TEST',
                                                    'commission_amount' => 'TEST',
                                                    'commission_percent' => 'TEST',
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'promotions' => 0,
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[[
            "products.0.locations.0.client_types.0.commission_amount" => ["The products.0.locations.0.client_types.0.commission_amount must be a number."],
            "products.0.locations.0.client_types.0.commission_percent" => ["The products.0.locations.0.client_types.0.commission_percent must be a number."],
            "products.0.locations.0.client_types.0.services.0.commission_amount" => ["The products.0.locations.0.client_types.0.services.0.commission_amount must be a number."],
            "products.0.locations.0.client_types.0.services.0.commission_percent" => ["The products.0.locations.0.client_types.0.services.0.commission_percent must be a number."],
            "products.0.locations.0.client_types.0.services.0.pickups.0.commission_amount" => ["The products.0.locations.0.client_types.0.services.0.pickups.0.commission_amount must be a number."],
            "products.0.locations.0.client_types.0.services.0.pickups.0.commission_percent" => ["The products.0.locations.0.client_types.0.services.0.pickups.0.commission_percent must be a number."],
            "products.0.locations.0.client_types.0.services.0.pickups.0.tip_amount" => ["The products.0.locations.0.client_types.0.services.0.pickups.0.tip_amount must be a number."],
            "products.0.locations.0.client_types.0.services.0.pickups.0.tip_percent" => ["The products.0.locations.0.client_types.0.services.0.pickups.0.tip_percent must be a number."],
            "products.0.locations.0.client_types.0.services.0.tip_amount" => ["The products.0.locations.0.client_types.0.services.0.tip_amount must be a number."],
            "products.0.locations.0.client_types.0.services.0.tip_percent" => ["The products.0.locations.0.client_types.0.services.0.tip_percent must be a number."],
            "products.0.locations.0.client_types.0.services.1.commission_amount" => ["The products.0.locations.0.client_types.0.services.1.commission_amount must be a number."],
            "products.0.locations.0.client_types.0.services.1.commission_percent" => ["The products.0.locations.0.client_types.0.services.1.commission_percent must be a number."],
            "products.0.locations.0.client_types.0.services.1.sessions.0.commission_amount" => ["The products.0.locations.0.client_types.0.services.1.sessions.0.commission_amount must be a number."],
            "products.0.locations.0.client_types.0.services.1.sessions.0.commission_percent" => ["The products.0.locations.0.client_types.0.services.1.sessions.0.commission_percent must be a number."],
            "products.0.locations.0.client_types.0.services.1.sessions.0.tip_amount" => ["The products.0.locations.0.client_types.0.services.1.sessions.0.tip_amount must be a number."],
            "products.0.locations.0.client_types.0.services.1.sessions.0.tip_percent" => ["The products.0.locations.0.client_types.0.services.1.sessions.0.tip_percent must be a number."],
            "products.0.locations.0.client_types.0.services.1.tip_amount" => ["The products.0.locations.0.client_types.0.services.1.tip_amount must be a number."],
            "products.0.locations.0.client_types.0.services.1.tip_percent" => ["The products.0.locations.0.client_types.0.services.1.tip_percent must be a number."],
            "products.0.locations.0.client_types.0.tip_amount" => ["The products.0.locations.0.client_types.0.tip_amount must be a number."],
            "products.0.locations.0.client_types.0.tip_percent" => ["The products.0.locations.0.client_types.0.tip_percent must be a number."]]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    public function testAllValid()
    {

        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([
        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('POST', '/api/v1/price', [

            'contract_model_id' => $contract_model->id,
            'date_end' => '2098-12-31',
            'date_start' => '2098-12-30',
            'user_id' => $user->id,
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'promotions' => 0,
        ])->assertJsonFragment(['error' => 200]);
    }

    public function testOk1()
    {

        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([
        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $service_factory = factory(\App\Models\tenant\Service::class)->create([
        ]);

        $user = factory(App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);
        $pickupFactory = factory(\App\Models\tenant\TransportationLocation::class)->create([]);
        $this->json('POST', '/api/v1/price', [

            'contract_model_id' => $contract_model->id,
            'date_end' => '2098-12-31',
            'date_start' => '2098-12-30',
            'user_id' => $user->id,
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                    'tip_amount' => '1',
                                    'tip_percent' => '2',
                                    'commission_amount' => '3',
                                    'commission_percent' => '4',
                                    'sessions' => [
                                        0 => [
                                            'session' => '10:00',
                                            'net_price' => 10,
                                            'sale_price' => 5,
                                            'tip_amount' => '5',
                                            'tip_percent' => '6',
                                            'commission_amount' => '7',
                                            'commission_percent' => '8',
                                        ]
                                    ],
                                    'services' => [
                                        0 => [
                                            'id' => 1,
                                            'net_price' => 15,
                                            'sale_price' => 25,
                                            'tip_amount' => '9',
                                            'tip_percent' => '10',
                                            'commission_amount' => '11',
                                            'commission_percent' => '12',
                                            'pickups' => [
                                                0 => [
                                                    'id' => $pickupFactory->id,
                                                    'net_price' => 20,
                                                    'sale_price' => 30,
                                                    'tip_amount' => '13',
                                                    'tip_percent' => '14',
                                                    'commission_amount' => '15',
                                                    'commission_percent' => '16',
                                                ]
                                            ]
                                        ],
                                        1 => [
                                            'id' => $service_factory->id,
                                            'net_price' => 60,
                                            'sale_price' => 65,
                                            'tip_amount' => 17,
                                            'tip_percent' => 18,
                                            'commission_amount' => 19,
                                            'commission_percent' => 20,
                                            'sessions' => [
                                                0 => [
                                                    'session' => '12:00',
                                                    'net_price' => 85,
                                                    'sale_price' => 100,
                                                    'tip_amount' => 21,
                                                    'tip_percent' => 22,
                                                    'commission_amount' => 23,
                                                    'commission_percent' => 24,
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'promotions' => 0,
        ])->assertJsonFragment(["error"=>200]);
    }

}