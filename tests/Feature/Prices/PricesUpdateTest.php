<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PricesUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parámetros
     */
    function testPricesWithOutParameters()
    {
        $this->json('PUT', '/api/v1/price', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "contract_model_id"=>["The contract model id field is required."],
                "date_end"=>["The date end field is required."],
                "date_start"=>["The date start field is required."],
                "user_id"=>["The user id field is required."],
                "id"=>["The id field is required."],
                "currency_id" => ["The currency id field is required."],
                "products"=>["The products field is required."]]]]);
    }

    /**
     * Prueba automatizada formato parametros incorrectos
     */
    public function testPricesParametersFormatIncorrect()
    {
        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => 'asfd',
            'date_end' => 'asfd',
            'date_start' => 'asfd',
            'user_id' => 'asfd',
            'price_id' => 'asdf',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 'test',
                                    'markup' => 'test',
                                    'sale_price' => 'test',
                                ],
                            ],
                        ],
                    ],

                ],
            ],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "contract_model_id" => ["The contract model id must be a number."],
                "date_end" => ["The date end does not match the format Y-m-d.", "The date end is not a valid date.", "The date end must be a date after or equal to date start."],
                "date_start" => ["The date start does not match the format Y-m-d.", "The date start is not a valid date."],
                "products.0.locations.0.client_types.0.markup" => ["The products.0.locations.0.client_types.0.markup must be a number."],
                "products.0.locations.0.client_types.0.net_price" => ["The products.0.locations.0.client_types.0.net_price must be a number."],
                "products.0.locations.0.client_types.0.sale_price" => ["The products.0.locations.0.client_types.0.sale_price must be a number."],
                "user_id" => ["The selected user id is invalid."],
                "id"=>["The id field is required."]]]]);
    }

    /**
     * Prueba automatizada formato parametros vacios
     */
    public function testPricesParametersEmpty()
    {
        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);


        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => '',
            'date_end' => '',
            'date_start' => '',
            'user_id' => '',
            'price_id' => '',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => '',
                                    'markup' => '',
                                    'sale_price' => '',
                                ],
                            ],
                        ],
                    ],

                ],
            ],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "contract_model_id" => ["The contract model id field is required."],
                "date_end" => ["The date end field is required."],
                "date_start" => ["The date start field is required."],
                "products.0.locations.0.client_types.0.markup" => ["The products.0.locations.0.client_types.0.markup field is required when none of products.0.locations.0.client_types.0.sale_price / products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.net_price" => ["The products.0.locations.0.client_types.0.net_price field is required when none of products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.sale_price"=>["The products.0.locations.0.client_types.0.sale_price field is required when none of products.0.locations.0.client_types.0.markup / products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.services are present."],
                "products.0.locations.0.client_types.0.services"=>["The products.0.locations.0.client_types.0.services field is required when none of products.0.locations.0.client_types.0.sessions / products.0.locations.0.client_types.0.net_price are present."],
                "products.0.locations.0.client_types.0.sessions"=>["The products.0.locations.0.client_types.0.sessions field is required when none of products.0.locations.0.client_types.0.sale_price / products.0.locations.0.client_types.0.markup / products.0.locations.0.client_types.0.net_price / products.0.locations.0.client_types.0.services are present."],
                "user_id"=>["The user id field is required."],
                "id"=>["The id field is required."]]]]);
    }

    /**
     * Prueba automatizada actualizar solapando fechas
     */
    public function testPriceRepeatedDates()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $contract_model_2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price_2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model_2->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price_2->id,
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model2->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-27',
            'currency_id' => 1,
            'products' => [

                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
            'id' => $price_2->id,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testOverlappingDate()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $contract_model_2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price_2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model_2->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price_2->id,
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model2->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2100-01-01',
            'date_start' => '2099-12-27',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
            'id' => $price_2->id,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testOverlappingDate2()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $contract_model_2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price_2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model_2->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price_2->id,
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model2->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-26',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
            'id' => $price_2->id,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testOverlappingDate3()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $contract_model_2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price_2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model_2->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price_2->id,
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model2->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);


        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2100-01-01',
            'date_start' => '2099-12-29',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
            'id' => $price_2->id,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testOverlappingDate4()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $contract_model_2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price_2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model_2->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price_2->id,
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model2->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(\App\Models\tenant\User::class)->create([]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-29',
            'date_start' => '2099-12-28',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
            'id' => $price_2->id,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada fechas solapadas
     */
    public function testOverlappingDate5()
    {
        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price->id,
        ]);

        $contract_model_2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price_2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model_2->id,
        ]);

        $price_product = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $price_2->id,
        ]);

        $contract_model2 = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model2->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $user = factory(App\Models\tenant\User::class)->create([]);


        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2099-12-31',
            'date_start' => '2099-12-28',
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
            ],
            'user_id' => $user->id,
            'promotions' => 0,
            'id' => $price_2->id,
        ])->assertExactJson(['error' => 409, 'error_description' => 'There is a price with the specified data']);
    }

    /**
     * Prueba automatizada correcta
     */
    public function testAllValid()
    {

        $contract_model = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $price = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([

        ]);

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'test',
        ]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);

        $product1 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product2 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);
        $product3 = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory2',
        ]);

        $location = factory(\App\Models\tenant\Location::class)->create([]);

        $user = factory(App\Models\tenant\User::class)->create([]);
        $this->json('PUT', '/api/v1/price', [
            'contract_model_id' => $contract_model->id,
            'date_end' => '2098-12-31',
            'date_start' => '2098-12-30',
            'user_id' => $user->id,
            'id' => $price->id,
            'currency_id' => 1,
            'products' => [
                0 => [
                    'id' => $product1->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],

                ],
                1 => [
                    'id' => $product2->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
                2 => [
                    'id' => $product3->id,
                    'locations' => [
                        0 => [
                            'id' => $location->id,
                            'client_types' => [
                                0 => [
                                    'id' => $client_type->id,
                                    'net_price' => 3,
                                    'markup' => 3,
                                    'sale_price' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'promotions' => 0,
            'price_id' => $price->id,
        ])->assertExactJson(['error' => 200]);
    }

}