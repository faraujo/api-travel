
<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PricesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada con envio de parametro en formato incorrecto
     */
    public function testBad1(){
        $this->json('GET','/api/v1/price',[
            'previous_price' => 'TEST',
            'page' => 'TEST',
            'limit' => 'TEST',
        ])
            ->assertExactJson(['error' => 400,'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'previous_price' => ['The previous price field must be true or false.'],
                "limit"=>["The limit must be an integer."],
                "page"=>["The page must be an integer."]]]]);
    }

    /**
     * Test consulta tarifas con parámetro contract_model_id con formato incorrecto
     */
    public function testWithBadFormatRequired()
    {
        $this->json('GET', '/api/v1/price', [
            'contract_model_id' => 'tt'
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["contract_model_id" => ["The contract model id must be an integer."]]]]);
    }

    /**
     * Test consulta tarifa con parámetro date incorrecto
     */
    public function testWithDateBadFormat(){
        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $contractTrasnaltionFactory = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contractFactory,
            'language_id' => 1,
        ]);

        $this->json('GET', '/api/v1/price', [
            'contract_model_id' => $contractFactory->id,
            'date' => 'asdf',
        ])->assertExactJson(['error' => 400,'error_description' => "The fields are not the required format", 'error_inputs' => [['date' => ["The date does not match the format Y-m-d.","The date is not a valid date."]]]]);
    }

    /**
     * Test consulta tarifa con parámetro contract_model_id inexistente en base de datos
     */
    public function testWithContractModelNotExist(){
        $this->json('GET', '/api/v1/price', [
            'contract_model_id' => 99999999999999999999,
        ])->assertExactJson(['error' => 400,'error_description' => "The fields are not the required format", 'error_inputs' => [['contract_model_id' => ['The contract model id must be an integer.']]]]);
    }

    /**
     * Test consulta tarifas con parámetro contract_model_correcto
     */
    public function testWithContractModelGood(){

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $contractTrasnaltionFactory = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contractFactory,
            'language_id' => 1,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'promotions' => 0,
            'date_start' => '2099-12-27',
            'date_end' => '2099-12-31',
            'contract_model_id' => $contractFactory->id
        ]);

        $this->json('GET', '/api/v1/price', [
            'contract_model_id' => $contractFactory->id,
        ])->assertJsonFragment([
            'error' => 200,
            'id' => $priceFactory->id,
            'id' => $contractFactory->id,
        ]);
    }

    /**
     * Test consulta tarifas con parámetro contract_model_correcto y date correcto
     */
    public function testWithContractModelGoodAndDate(){

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $contractTrasnaltionFactory = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contractFactory,
            'language_id' => 1,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'promotions' => 0,
            'date_start' => '2099-12-27',
            'date_end' => '2099-12-31',
            'contract_model_id' => $contractFactory->id
        ]);

        $this->json('GET', '/api/v1/price', [
            'contract_model_id' => $contractFactory->id,
            'date' => '2099-12-28',
        ])->assertJsonFragment([
            'error' => 200,
            'id' => $priceFactory->id,
            'id' => $contractFactory->id,
        ]);
    }

    /**
     * Test consulta tarifas con parámetro contract_model_correcto y date no existente
     */
    public function testWithContractModelGoodAndDateNotExists(){

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $contractTrasnaltionFactory = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contractFactory,
            'language_id' => 1,
        ]);

        factory(App\Models\tenant\Price::class)->create([
            'promotions' => 0,
            'date_start' => '2099-12-27',
            'date_end' => '2099-12-31',
            'contract_model_id' => $contractFactory->id
        ]);

        $this->json('GET', '/api/v1/price', [
            'contract_model_id' => $contractFactory->id,
            'date' => '2099-12-20',
        ])->assertExactJson(["error"=>200,"data"=>[], "total_results" => 0]);
    }

    /**
     * Prueba automatizada con envio de previous_price con tarifas actuales
     */
    public function testOk1(){
        $fecha_inicio = \Carbon\Carbon::now()->format('Y-m-d');
        $fecha_fin = \Carbon\Carbon::now()->addDays('2')->format('Y-m-d');

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $contractTrasnaltionFactory = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contractFactory,
            'language_id' => 1,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'promotions' => 0,
            'date_start' => $fecha_inicio,
            'date_end' => $fecha_fin,
            'contract_model_id' => $contractFactory->id
        ]);

        $this->json('GET', '/api/v1/price', [
            'previous_price' => 0,
        ])->assertJsonFragment([
            'id' => $priceFactory->id,
            'id' => $contractFactory->id,
        ]);

    }

    /**
     * Prueba automatizada con envio de previous_price con tarifas actuales
     */
    public function testOk2(){
        $fecha_inicio = \Carbon\Carbon::now()->subDays('3')->format('Y-m-d');
        $fecha_fin = \Carbon\Carbon::now()->addDays('2')->format('Y-m-d');

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $contractTrasnaltionFactory = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contractFactory,
            'language_id' => 1,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'promotions' => 0,
            'date_start' => $fecha_inicio,
            'date_end' => $fecha_fin,
            'contract_model_id' => $contractFactory->id
        ]);

        $this->json('GET', '/api/v1/price', [
            'previous_price' => 0,
        ])->assertJsonFragment([
            'id' => $priceFactory->id,
            'id' => $contractFactory->id,
        ]);
    }
    /**
     * Prueba automatizada con envio de previous_price con tarifas actuales
     */
    public function testOk3(){
        $fecha_inicio = \Carbon\Carbon::now()->subDays('3')->format('Y-m-d');
        $fecha_fin = \Carbon\Carbon::now()->format('Y-m-d');

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $contractTrasnaltionFactory = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contractFactory,
            'language_id' => 1,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'promotions' => 0,
            'date_start' => $fecha_inicio,
            'date_end' => $fecha_fin,
            'contract_model_id' => $contractFactory->id
        ]);

        $this->json('GET', '/api/v1/price', [
            'previous_price' => 0,
        ])->assertJsonFragment([
            'id' => $priceFactory->id,
            'id' => $contractFactory->id,
        ]);
    }

}