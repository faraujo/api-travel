<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PricesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta tarifa con id inexistente
     */
    public function testPriceBad()
    {
        $this->json('GET', '/api/v1/price/9999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta tarifa existente y devuelve id con id de tarifa
     */
    public function testPriceRead()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);

        $currencyFactory = factory(\App\Models\tenant\Currency::class)->create([]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'currency_id' => $currencyFactory->id,
        ]);
        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/price/' . $priceFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $priceFactory->id,
            ]);
    }

    /**
     * Consulta tarifa existente y devuelve datos con id de un producto creado y con la tarifa creada asignada
     */
    public function testPriceReadProduct()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 1,
            'code' => 'pruebaFactory3',
        ]);
        $translation1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);

        $currencyFactory = factory(\App\Models\tenant\Currency::class)->create([]);


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'currency_id' => $currencyFactory->id,
        ]);
        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/price/' . $priceFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $priceFactory->id,
                'name' => $translation1->name,
            ]);
    }

    /**
     * Se crea tarifa, se consulta y devuelve datos entre ellos el id de la tarifa creada, se borra tarifa y al consultarla ya no devuelve datos
     */
    public function testPriceReadDelete()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'code' => 'pruebaFactory3',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);

        $currencyFactory = factory(\App\Models\tenant\Currency::class)->create([]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'currency_id' => $currencyFactory->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/price/' . $priceFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $priceFactory->id,
            ]);

        $priceFactory->delete();

        $this->json('GET', '/api/v1/price/' . $priceFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Se crea un producto y no se le asigna tarifa, debe mostrarlo aunque no tenga tarifa asignada
     */
    public function testProductWithNoPriceRead()
    {
        $productFactoryNoIncluido = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 1,
            'code' => 'prueba',
        ]);
        $translationNoIncluido = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactoryNoIncluido->id,
        ]);

        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);

        $currencyFactory = factory(\App\Models\tenant\Currency::class)->create([]);

        $priceFactory2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
            'currency_id' => $currencyFactory->id,
        ]);

        $this->json('GET', '/api/v1/price/' . $priceFactory2->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $priceFactory2->id,
                'name' => $translationNoIncluido->name,
            ]);
    }

}