<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PricesDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar existencia de parametros requeridos
     */
    public function testWithOutParameters()
    {
        $this->json('DELETE', '/api/v1/price', [])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids" => ["The ids field is required."]]]]);
    }

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testValidatorNumber()
    {
        $this->json('DELETE', '/api/v1/price', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testValidatorRequired()
    {
        $this->json('DELETE', '/api/v1/price', [
            'ids' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testIdBad()
    {
        $this->json('DELETE', '/api/v1/price', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' => [['ids.0' => ['The selected ids.0 is invalid.']]],
            ]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */

    public function testIdNoArray()
    {

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
        ]);

        $this->assertDatabaseHas('mo_price', [
            'id' => $priceFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/price', [
            'ids' => $priceFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }


    /**
     * Test para eliminar una tarifa que crea primero un tarifa-factory
     */
    public function testDelete()
    {

        $contract_model = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_price', [
            'id' => $priceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_price_product', [
            'id' => $priceProductFactory->id,
        ]);

        $this->assertDatabaseHas('mo_price_product', [
            'price_id' => $priceFactory->id,
        ]);


        $this->json('DELETE', '/api/v1/price', [
            'ids' => [$priceFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que en la tabla mo_price el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_price', ['deleted_at' => null, 'id' => $priceFactory->id]);
        //comprueba que en la tabla mo_price_product el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_price_product', ['deleted_at' => null, 'id' => $priceProductFactory->id]);
    }


    /**
     * Test para eliminar dos tarifas, se crean primero dos tarifa-factory
     */

    public function testDelete2()
    {

        $contract_model1 = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $contract_model2 = factory(\App\Models\tenant\ContractModel::class)->create([]);

        $priceFactory1 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model1->id,
        ]);

        $priceFactory2 = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract_model2->id,
        ]);

        $priceProductFactory1 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory1->id,
        ]);

        $priceProductFactory2 = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_price', [
            'id' => $priceFactory1->id,
        ]);

        $this->assertDatabaseHas('mo_price', [
            'id' => $priceFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_price_product', [
            'id' => $priceProductFactory1->id,
        ]);

        $this->assertDatabaseHas('mo_price_product', [
            'id' => $priceProductFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_price_product', [
            'price_id' => $priceFactory1->id,
        ]);

        $this->assertDatabaseHas('mo_price_product', [
            'price_id' => $priceFactory2->id,
        ]);


        $this->json('DELETE', '/api/v1/price', [
            'ids' => [$priceFactory1->id, $priceFactory2->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que en la tabla mo_price el registro con los ids que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_price', ['deleted_at' => null, 'id' => $priceFactory1->id]);
        $this->assertDatabaseMissing('mo_price', ['deleted_at' => null, 'id' => $priceFactory2->id]);

        //comprueba que en la tabla mo_price_product los registros con ids que hemos creado no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_price_product', ['deleted_at' => null, 'id' => $priceProductFactory1->id]);
        $this->assertDatabaseMissing('mo_price_product', ['deleted_at' => null, 'id' => $priceProductFactory2->id]);
    }

}