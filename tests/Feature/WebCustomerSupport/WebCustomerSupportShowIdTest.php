<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class WebCustomerSupportShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta legal inexistente sin parametros
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/customersupport/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta producto existente sin parametros
     */
    public function testOk1()
    {

        $CustomerSupportFactory = factory(App\Models\tenant\WebCustomerSupport::class)->create([
        ]);
        $translation1 = factory(App\Models\tenant\WebCustomerSupportTranslation::class)->create([
            'language_id' => 1,
            'customer_support_id' => $CustomerSupportFactory->id,
            'name' => 'test ES'
        ]);
        factory(App\Models\tenant\WebCustomerSupportTranslation::class)->create([
            'language_id' => 2,
            'customer_support_id' => $CustomerSupportFactory->id,
            'name' => 'test EN'
        ]);
        factory(App\Models\tenant\WebCustomerSupportTranslation::class)->create([
            'language_id' => 3,
            'customer_support_id' => $CustomerSupportFactory->id,
            'name' => 'test PT'
        ]);

        $this->json('GET', '/api/v1/customersupport/' . $CustomerSupportFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $CustomerSupportFactory->id,
                'name' => $translation1->name
            ]);
    }
}