<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class WebCustomerSupportShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta legal con parametro lang mal
     */
    public function testBad1()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/customersupport', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta productos con parametro lang
     */
    public function testOk1()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/customersupport', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta legales con parametro lang en mayusculas
     */
    public function testOk2()
    {
        //Se especifica parametro lang correctamente en mayusculas
        $this->json('GET', '/api/v1/customersupport', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    public function testOk3()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/customersupport', [
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    public function testOk4()
    {
        $customersupport = factory(App\Models\tenant\WebCustomerSupport::class)->create([
        ]);
        $translation1 = factory(App\Models\tenant\WebCustomerSupportTranslation::class)->create([
            'language_id' => 1,
            'customer_support_id' => $customersupport->id,
            'name' => 'test ES'
        ]);
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/customersupport', [
            'name' => $translation1->name
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }
}