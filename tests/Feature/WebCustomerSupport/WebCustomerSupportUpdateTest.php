<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class WebCustomerSupportUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations en formato incorrecto
     */
    public function testBad1()
    {

        $customersupport = factory(App\Models\tenant\WebCustomerSupport::class)->create([
            ]);

        $this->json('PUT', '/api/v1/customersupport', [
            'id' => $customersupport->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "translation"=>["you need at least one translation"]
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations inexistentes
     */
    public function testOk1()
    {
        $customersupport = factory(App\Models\tenant\WebCustomerSupport::class)->create([
        ]);

        $this->json('PUT', '/api/v1/customersupport', [
            'id' => $customersupport->id,
            'name' => ['ES' => 'cualquiera español'],
            'contact' => ['ES' => 'test@test.com']
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_web_customer_support_translation', [
            'customer_support_id' => $customersupport->id,
            'language_id' => 1,
            'name' => 'cualquiera español',
            'contact' => 'test@test.com'
        ]);
    }
}