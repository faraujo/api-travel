<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class WebCustomerSupportCreateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations en formato incorrecto
     */
    public function testBad1()
    {

        $this->json('POST', '/api/v1/customersupport', [
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "translation"=>["you need at least one translation"]
                ]]]);
    }

    
    public function testBad2()
    {

        $this->json('POST', '/api/v1/customersupport', [
            'name' => ['ES' => 'cualquiera español'],
        ])
        ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
        "error_inputs" => [[
            "contact.ES"=>["The contact. e s field is required."]
        ]]]);
    }

    
    public function testBad3()
    {

        $this->json('POST', '/api/v1/customersupport', [
            'contact' => ['ES' => 'cualquiera español'],
        ])
        ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
        "error_inputs" => [[
            "name.ES"=>["The name. e s field is required."]
        ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos
     */
    public function testOk1()
    {

        $this->json('POST', '/api/v1/customersupport', [
            'name' => ['ES' => 'cualquiera español'],
            'contact' => ['ES' => 'test@test.com']
        ])
            ->assertExactJson(["error" => 200]);
    }
}