<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ServicesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta servicios con parámetro lang incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/service/', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }



    /**
     * Test filtra por producto con formato incorrecto
     */
    public function testBad2()
    {

        $this->json('GET', '/api/v1/service/', [
            'product_id' => 'NN'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta servicios con parámetro page incorrecto
     */
    public function testBad3(){
        $this->json('GET', '/api/v1/service/', [
            'page' => 'asdf',
        ])->assertExactJson(['error' => 400,'error_description' => "The fields are not the required format", 'error_inputs' => [['page' => ["The page must be a number."]]]]);
    }

    /**
     * Test consulta servicios con parámetro limit incorrecto
     */
    public function testBad4(){
        $this->json('GET', '/api/v1/service/', [
            'limit' => 'asdf',
        ])->assertExactJson(['error' => 400,'error_description' => "The fields are not the required format", 'error_inputs' => [['limit' => ["The limit must be a number."]]]]);
    }

    /**
     * Test consulta servicios con parámetro lang correcto en español y en minúsculas
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/service/', [
            'lang' => 'es'
        ])->assertJsonFragment(['language_id' => 1]);
    }

    /**
     * Test consulta servicios con parámetro lang correcto en inglés y en mayúsculas
     */
    public function testOk2()
    {
        $this->json('GET', '/api/v1/service/', [
            'lang' => 'EN'
        ])->assertJsonFragment(['language_id' => 2]);
    }

    /**
     * Test consulta servicios con parámetro lang correcto en inglés y en mayúsculas
     */
    public function testOk3()
    {
        $this->json('GET', '/api/v1/service/', [
            'lang' => 'PT'
        ])->assertJsonFragment(['language_id' => 3]);
    }

    /**
     *  Test consulta de servicios sin parámetros, devuelve las 3 traducciones
     */
    public function testOk4()
    {
        $this->json('GET', '/api/v1/service/')->assertJsonFragment(['language_id' => 1,'language_id' => 2,'language_id' => 3]);
    }

    /**
     * Test consulta servicios con parámetro product_id inexistente
     */
    public function testOk5()
    {
        //Se especifica un type inexistente

        $this->json('GET', '/api/v1/service/', [
            'lang' => 'es',
            'product_id' => 999999999999999,
        ])->assertExactJson(['data' => [], 'error' => 200, "total_results" => 0,]);
    }

    /**
     * Test filtra por producto existente y devuelve datos
     */
    public function testOk6()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([]);

        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $serviceFactory->id,
        ]);

        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $serviceFactory->id,
        ]);

        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $serviceFactory->id,
        ]);

        factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $this->json('GET', '/api/v1/service/', [
            'product_id' => $productFactory->id,
        ])->assertJsonFragment([
            'id' => $serviceFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

    }

    /**
     * Test consulta servicios con todos los parámetros
     */
    public function testOk7()
    {
        $this->json('GET', '/api/v1/service/', [
            'lang' => 'es',
            'product_id' => 2,
            'name' => 'cualquiera',
            'order' => 'id',
            'page' => 2,
            'limit' => 2,
        ])->assertJsonStructure(['error', 'data']);
    }
}