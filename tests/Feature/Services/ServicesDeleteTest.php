<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ServicesDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    //TEST VALIDATOR

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/service', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/service', [
            'ids' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    //FIN TEST VALIDATOR

    /**
     * Eliminar un service con id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/service', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' => [["ids.0" => ["The selected ids.0 is invalid."]]],
            ]);
    }

    /** Test para borrar un servicio ya borrado
     *
     */
    public function testBad4()
    {

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/service', [
            'ids' => [$serviceFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_service', ['deleted_at' => null, 'id' => $serviceFactory->id]);


        //intenta borrar registro ya borrado
        $this->json('DELETE', '/api/v1/service', [
            'ids' => [$serviceFactory->id],
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found', 'error_inputs' => [["ids.0" => ["The selected ids.0 is invalid."]]]]);


    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad5()
    {

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/service', [
            'ids' => $serviceFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Test para eliminar un servicio que crea primero un service factory, sus traducciones y crea relaciones en tabla intermedia
     */
    public function testOk1()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $serviceTranslationFactory1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory2 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory3 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'service_id' => $serviceFactory->id,
        ]);

        factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'service_id' => $serviceFactory->id,
            'id' => $serviceTranslationFactory1->id,
            'id' => $serviceTranslationFactory2->id,
            'id' => $serviceTranslationFactory3->id,
        ]);

        $this->assertDatabaseHas('mo_product_service', [
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/service', [
            'ids' => [$serviceFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);



        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_service', ['deleted_at' => null, 'id' => $serviceFactory->id]);
        //comprueba campo deleted_at de las traducciones de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory1->id]);
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory2->id]);
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory3->id]);
        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_product_service', ['deleted_at' => null, 'service_id' => $serviceFactory->id]);
    }

    /**
     * Test para eliminar dos servicios que crea primero como service factory, con sus traducciones y crea relaciones en tabla intermedia
     */
    public function testOk2()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);
        $productFactory2 = factory(App\Models\tenant\Product::class)->create([
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);
        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([
        ]);

        $serviceTranslationFactory1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory2 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory3 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'service_id' => $serviceFactory->id,
        ]);

        $serviceTranslationFactory4 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory2->id,
        ]);
        $serviceTranslationFactory5 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'service_id' => $serviceFactory2->id,
        ]);
        $serviceTranslationFactory6 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'service_id' => $serviceFactory2->id,
        ]);

        factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory2->id,
            'service_id' => $serviceFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'service_id' => $serviceFactory->id,
            'id' => $serviceTranslationFactory1->id,
            'id' => $serviceTranslationFactory2->id,
            'id' => $serviceTranslationFactory3->id,
        ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'service_id' => $serviceFactory2->id,
            'id' => $serviceTranslationFactory4->id,
            'id' => $serviceTranslationFactory5->id,
            'id' => $serviceTranslationFactory6->id,
        ]);

        $this->assertDatabaseHas('mo_product_service', [
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_service', [
            'product_id' => $productFactory2->id,
            'service_id' => $serviceFactory2->id,
        ]);

        $this->json('DELETE', '/api/v1/service', [
            'ids' => [$serviceFactory->id , $serviceFactory2->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);


        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_service', ['deleted_at' => null, 'id' => $serviceFactory->id]);
        $this->assertDatabaseMissing('mo_service', ['deleted_at' => null, 'id' => $serviceFactory2->id]);

        //comprueba campo deleted_at de las traducciones de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory1->id]);
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory2->id]);
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory3->id]);
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory4->id]);
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory5->id]);
        $this->assertDatabaseMissing('mo_service_translation', ['deleted_at' => null, 'id' => $serviceTranslationFactory6->id]);
        //comprueba campo deleted_at de service no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_product_service', ['deleted_at' => null, 'service_id' => $serviceFactory->id]);
        $this->assertDatabaseMissing('mo_product_service', ['deleted_at' => null, 'service_id' => $serviceFactory2->id]);
    }


}