<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ServicesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consultar service con parámetro lang erróneo
     */
    public function testBad1()
    {
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);
        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory->id,
        ]);
        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'service_id' => $serviceFactory->id,
        ]);
        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'service_id' => $serviceFactory->id,
        ]);
        $this->json('GET', '/api/v1/service/' . $serviceFactory->id, [
            'lang' => 'FDF'])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Consulta service inexistente sin parámetros
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/service/999999999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta service existente sin parámetros
     */
    public function testoK1()
    {
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);
        $translation1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory->id,
            'name' => 'PRUEBA ES',
        ]);
        $translation2 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'service_id' => $serviceFactory->id,
            'name' => 'PRUEBA EN',
        ]);
        $translation3 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'service_id' => $serviceFactory->id,
            'name' => 'PRUEBA PT',
        ]);
        $this->json('GET', '/api/v1/service/' . $serviceFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $serviceFactory->id,
                'service_id' => $serviceFactory->id,
                'name' => $translation1->name,
                'name' => $translation2->name,
                'name' => $translation3->name,
            ]);
    }

    /**
     * Consulta service existente con parámetro lang
     */
    public function testOk2()
    {
        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);
        $translation1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory->id,
            'name' => 'PRUEBA ES',
        ]);
        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'service_id' => $serviceFactory->id,
            'name' => 'PRUEBA EN',
        ]);
        factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'service_id' => $serviceFactory->id,
            'name' => 'PRUEBA PT',
        ]);
        $this->json('GET', '/api/v1/service/' . $serviceFactory->id,[
            'lang' => 'es',
        ])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $serviceFactory->id,
                'service_id' => $serviceFactory->id,
                'name' => $translation1->name,
            ]);
    }

}