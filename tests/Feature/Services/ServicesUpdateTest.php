<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ServicesUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    //TEST VALIDACIONES
    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda ninguno de los campos requeridos
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/service', [

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["id"=>["The id field is required."],"translation"=>["you need at least one translation"]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formatos incorrectos
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/service', [
            'id' => 'r',
            'name' => ['ES' => 'name de testCrear', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[['id' => ["The id must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda id de servicio a modificar que no existe en base de datos
     */
    public function testBad3()
    {

        $this->json('PUT', '/api/v1/service', [
            'id' => 99999999999,
            'name' => ['ES' => 'name de testCrear', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
        ])
            ->assertExactJson(["error"=>404,"error_description"=>"Data not found"]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda campo name en español y no manda campo description en inglés, debe venir al menos una traducción y si viene nombre en un idioma debe venir su descripción en el mismo idioma
     */
    public function testBad4()
    {
        factory(App\Models\tenant\Product::class)->create([]);

        factory(App\Models\tenant\Product::class)->create([]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);
        $serviceTranslationFactory1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'name' => 'primer name es',
            'description' => 'primera description es',
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory2 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'name' => 'primer name en',
            'description' => 'primera description en',
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory3 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'name' => 'primer name pt',
            'description' => 'primera description en',
            'service_id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'id' => $serviceTranslationFactory1->id,
            'id' => $serviceTranslationFactory2->id,
            'id' => $serviceTranslationFactory3->id,
        ]);

        $this->json('PUT', '/api/v1/service', [
            'id' => $serviceFactory->id,
            'name' => ['EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés'],
            'description' => ['ES' => 'Descripcion español'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["description.EN"=>["The description. e n field is required."],"name.ES"=>["The name. e s field is required."]]]]);
    }
    //FIN TEST VALIDACIONES

    /**
     * Test para modificar un service
     *
     * A partir de los datos del service que quiere modificar modifica el usuario los campos que desee
     */
    public function testUpdate()
    {
        factory(App\Models\tenant\Product::class)->create([]);

        factory(App\Models\tenant\Product::class)->create([]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);
        $serviceTranslationFactory1 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'name' => 'primer name es',
            'description' => 'primera description es',
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory2 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 2,
            'name' => 'primer name en',
            'description' => 'primera description en',
            'service_id' => $serviceFactory->id,
        ]);
        $serviceTranslationFactory3 = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 3,
            'name' => 'primer name pt',
            'description' => 'primera description en',
            'service_id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_service', [
            'id' => $serviceFactory->id,
        ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'id' => $serviceTranslationFactory1->id,
            'id' => $serviceTranslationFactory2->id,
            'id' => $serviceTranslationFactory3->id,
        ]);

        $this->json('put', '/api/v1/service', [
            'id' => $serviceFactory->id,
            'name' => ['ES' => 'name modificado español', 'EN' => 'name update english', 'PT' => 'name update portugués'],
            'description' => ['ES' => 'Descripcion modificada español', 'EN' => 'Description update english', 'PT' => 'Description update portuguese'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'name' => 'name modificado español',
            'name' => 'name update english',
            'name' => 'name update portugués',
            'description' => 'Descripcion modificada español',
            'description' => 'Description update english',
            'description' => 'Description update portuguese',
        ]);
    }


}