<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ServicesCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda ninguno de los campos requeridos
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/service', [

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["translation"=>["you need at least one translation"]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda campo name en español y no manda campo description en inglés, debe venir al menos una traducción y si viene nombre en un idioma debe venir su descripción en el mismo idioma
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/service', [
            'name' => ['EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés'],
            'description' => ['ES' => 'Descripcion español'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
        "error_inputs"=>[["description.EN"=>["The description. e n field is required."],"name.ES"=>["The name. e s field is required."]]]]);
    }

    /**
     * Realiza el test de creacion de servicios con sus traducciones
     *
     */
    public function testOk1()
    {

        $this->json('POST', '/api/v1/service', [
            'name' => ['ES' => 'PRUEBA OK1 NAME español', 'EN' => 'PRUEBA OK1 NAME ingles', 'PT' => 'PRUEBA OK1 NAME portugues'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'name' => 'PRUEBA OK1 NAME español',
            'name' => 'PRUEBA OK1 NAME ingles',
            'name' => 'PRUEBA OK1 NAME portugues',
            'description' => 'Descripcion español',
            'description' => 'Descripcion inglés',
            'description' => 'Descripcion portugués',

        ]);

    }

    /**
     * Realiza el test de creacion de servicios con al menos traducción en español
     *
     */
    public function testOk2()
    {

        $this->json('POST', '/api/v1/service', [
            'name' => ['ES' => 'PRUEBA OK1 NAME español'],
            'description' => ['ES' => 'Descripcion español'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'name' => 'PRUEBA OK1 NAME español',
            'description' => 'Descripcion español',
        ]);

    }

    /**
     * Realiza el test de creacion de servicios con al menos traducción en inglés
     *
     */
    public function testOk3()
    {

        $this->json('POST', '/api/v1/service', [
            'name' => ['EN' => 'PRUEBA OK1 NAME ingles'],
            'description' => ['EN' => 'Descripcion ingles'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'name' => 'PRUEBA OK1 NAME ingles',
            'description' => 'Descripcion ingles',
        ]);

    }

    /**
     * Realiza el test de creacion de servicios con al menos traducción en portugués
     *
     */
    public function testOk4()
    {

        $this->json('POST', '/api/v1/service', [
            'name' => ['PT' => 'PRUEBA OK1 NAME portugues'],
            'description' => ['PT' => 'Descripcion portugues'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_service_translation', [
            'name' => 'PRUEBA OK1 NAME portugues',
            'description' => 'Descripcion portugues',
        ]);

    }

}