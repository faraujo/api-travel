<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ServicesAssignTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba las validaciones del método
     *
     * Sin enviar ningún parametro
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/service/assign', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'product_id' => ['The product id field is required.'],
            ]]]);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Enviando formato incorrecto de product_id
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => 'text',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_id' => ['The product id must be a number.'],
        ]]]);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Envio de product_id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => 999999999999999999999999,
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Envio formato incorrecto de services
     */
    public function testBad4()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => $productFactory->id,
            'services' => 'texto',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'services' => ['The services must be an array.'],
        ]]]);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Envio de service inexistente
     */
    public function testBad5()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => $productFactory->id,
            'services' => [0 => 99999999999999999999999],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'services.0' => ['The selected services.0 is invalid.'],
        ]]]);
    }

    /**
     * Test que comprueba funcionamiento del método
     *
     * Envio de product sin services para borrar relaciones en tabla intermedia
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => $productFactory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_product_service', ['product_id' => $productFactory->id, 'deleted_at' => null]);
    }

    /**
     * Test que comprueba funcionamiento del método
     *
     * Envio de product y service
     */
    public function testOk2()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => $productFactory->id,
            'services' => [0 => $serviceFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_product_service', ['product_id' => $productFactory->id, 'service_id' => $serviceFactory->id, 'deleted_at' => null]);

    }

    /**
     * Test que comprueba funcionamiento del método
     *
     * Envio de product y varios services
     */
    public function testOk3()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => $productFactory->id,
            'services' => [0 => $serviceFactory->id,
                1 => $serviceFactory2->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_product_service', ['product_id' => $productFactory->id, 'service_id' => $serviceFactory->id,
            'service_id' => $serviceFactory2->id,'deleted_at' => null]);

    }

    /**
     * Test que comprueba funcionamiento del metodo
     *
     * Envio de product y service sobreescribiendo relaciones ya existentes
     */
    public function testOk4()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => $productFactory->id,
            'services' => [$serviceFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_product_service', ['product_id' => $productFactory->id, 'service_id' => $serviceFactory->id, 'deleted_at' => null]);

        $serviceFactory2 = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->json('POST', '/api/v1/service/assign', [
            'product_id' => $productFactory->id,
            'services' => [$serviceFactory2->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_product_service', ['product_id' => $productFactory->id, 'service_id' => $serviceFactory->id, 'deleted_at' => null]);

        $this->assertDatabaseHas('mo_product_service', ['product_id' => $productFactory->id, 'service_id' => $serviceFactory2->id, 'deleted_at' => null]);

    }


}