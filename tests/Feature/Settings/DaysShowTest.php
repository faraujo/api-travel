<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class DaysShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada enviando parametro lang incorrecto
     */
    public function testBad1(){
        $this->json('GET','/api/v1/settings/day', [
            'lang' => 'asdf'
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format','error_inputs' => [[
                'lang' => ['The lang is not exists']
            ]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    public function testOk1(){
        $this->json('GET', '/api/v1/settings/day')
            ->assertJsonStructure(['error','data','total_results']);
    }

    /**
     * Prueba automatizada correcta enviando parametro lang
     */
    public function testOk2(){
        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $day_factory = factory(\App\Models\tenant\Day::class)->create([]);

        $day_translation = factory(\App\Models\tenant\DayTranslation::class)->create([
            'language_id' => $language_factory->id,
            'day_id' => $day_factory->id,
        ]);

        $this->json('GET','/api/v1/settings/day', [
            'lang' => $language_factory->abbreviation,
        ])
            ->assertExactJson(['error' => 200, 'data' => [[
                'day' => [[
                'id' => $day_factory->id,
                'lang' => [[
                    $language_factory->abbreviation => [
                        'id' => $day_translation->id,
                        'language_id' => $language_factory->id,
                        'name' => $day_translation->name,
                        "abbreviation" => $day_translation->abbreviation,
                    ]
                ]]
            ]]]], 'total_results' => 1]);
    }

}