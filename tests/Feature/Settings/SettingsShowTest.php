<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SettingsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta parámetros de tabla settings. Se comprueba estructura y número de resultados.
     */
    public function testOk1()
    {
        $this->json('get', '/api/v1/settings')
            ->assertJsonStructure([
                'error',
                'data' => ["0" => ['settings' => ["0"]]],
            ])
            ->assertJsonFragment([
                'error' => 200
            ]);
    }

    /**
     * Test consulta parámetros de tabla settings (especificando un filtro cualquiera, el método no contempla filtros).
     * Se comprueba estructura y número de resultados.
     */
    public function testOk2()
    {
        // Se especifica un filtro aleatorio
        $this->json('GET', '/api/v1/settings', ['filtro' => '100'])
            ->assertJsonStructure([
                'error',
                'data' => ["0" => ['settings' => ["0"]]],
            ])
            ->assertJsonFragment([
                'error' => 200
            ]);
    }
}