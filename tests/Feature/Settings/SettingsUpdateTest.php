<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class SettingsUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en todos los campos que se validan y no se mandan requeridos
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/settings', [
            'email_port_default' => 'string',
            'idioma_defecto_id' => 'string',
            'moneda_defecto_id' => 'string',
            //'limit_registers' => 'string',
            'token_validate' => 'string',
            'recover_token_validate' => 'string',
            'time_bloqued_password' => 'string',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["cron_expire_reservation" =>["The cron expire reservation field is required."],"email_from_default"=>["The email from default field is required."],
                "currencies" => ["The currencies field is required."],   
                "languages" => ["The languages field is required."],   
                "discount_max" => ["The discount max field is required."],
                "nombre_cliente" => ["The nombre cliente field is required."],
                "email_cliente_contacto" => ["The email cliente contacto field is required."],
                "nif_cliente" => ["The nif cliente field is required."],
                "reservation_code_length" => ["The reservation code length field is required."],
                "email_fromname_default"=>["The email fromname default field is required."],
                "email_host_default"=>["The email host default field is required."],
                "email_port_default"=>["The email port default must be an integer."],
                "email_username_default"=>["The email username default field is required."],
                "height_unit"=>["The height unit field is required."],
                "idioma_defecto_id"=>["The idioma defecto id must be an integer."],
                //"limit_registers"=>["The limit registers must be an integer."],
                "moneda_defecto_id"=>["The moneda defecto id must be an integer."],
                "recover_token_validate"=>["The recover token validate must be an integer."],
                "time_bloqued_password"=>["The time bloqued password must be an integer."],
                "token_validate"=>["The token validate must be an integer."],
                "weight_unit"=>["The weight unit field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y manda idioma y moneda inexistentes en base de datos
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/settings', [
            'email_port_default' => 25,
            //'limit_registers' => 50,
            'token_validate' => 20,
            'recover_token_validate' => 60,
            'time_bloqued_password' => 60,
            'email_host_default' => 'string',
            'email_username_default' => 'string',
            'email_from_default' => 'string',
            'email_fromname_default' => 'string',
            'height_unit' => 'string',
            'weight_unit' => 'string',
            'cron_expire_reservation' => 10,
            'discount_max' => 10,
            'reservation_code_length' => 5,
            'nombre_cliente' => 'My name',
            'email_cliente_contacto' => 'My email',
            'nif_cliente' => 'My nif',
            'languages' => [1],
            'currencies' => [2],
            'idioma_defecto_id' => 9999999,
            'moneda_defecto_id' => 9999999,
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["idioma_defecto_id"=>["The language default selected is not valid"],
                    "moneda_defecto_id"=>["The currency default selected is not valid"],
                    "email_cliente_contacto" => ["The email cliente contacto must be a valid email address."],
                ]]]);
    }

    /**
     * Realiza el test de actualización de diferentes campos de configuración y comprueba que se han modificado correctamente en base de datos
     *
     */
    public function testOK1()
    {
        $fileFactory = factory(App\Models\tenant\File::class)->create([]);
        $this->json('PUT', '/api/v1/settings', [
            'email_host_default' => 4,
            'email_username_default' => 'email_user',
            'email_password_default' => 'password_test1',
            'email_from_default' => 'email@test1',
            'email_fromname_default' => 'email_From@test1',
            'email_port_default' => 28,
            'email_encryption_default' => 'encription_test1',
            'idioma_defecto_id' => 1,
            'moneda_defecto_id' => 1,
            //'limit_registers' => 20,
            'height_unit' => 'cm',
            'weight_unit' => 'kg',
            'token_validate' => 5,
            'recover_token_validate' => 50,
            'time_bloqued_password' => 50,
            'cron_expire_reservation' => 10,
            'discount_max' => 10,
            'reservation_code_length' => 5,
            'nombre_cliente' => 'My name',
            'email_cliente_contacto' => 'myemailtest@test1',
            'nif_cliente' => 'My nif',
            'direccion_facturacion' => 'My address',
            'languages' => [1],
            'currencies' => [1],
            'logo_image_id' => $fileFactory->id
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_settings', [
            'name' => 'email_host_default',
            'value' => 4,
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'email_username_default',
            'value' => 'email_user',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'email_password_default',
            'value' => 'password_test1',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'email_from_default',
            'value' => 'email@test1',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'email_fromname_default',
            'value' => 'email_From@test1',
        ]);

        $this->assertDatabaseHas('mo_settings', [
            'name' => 'email_port_default',
            'value' => 28,
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'email_encryption_default',
            'value' => 'encription_test1',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'idioma_defecto_id',
            'value' => 1,
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'moneda_defecto_id',
            'value' => 1,
        ]);
        /*$this->assertDatabaseHas('mo_settings', [
            'name' => 'limit_registers',
            'value' => 20,
        ]);*/
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'height_unit',
            'value' => 'cm',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'weight_unit',
            'value' => 'kg',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'token_validate',
            'value' => 5,
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'recover_token_validate',
            'value' => 50,
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'time_bloqued_password',
            'value' => 50,
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'nombre_cliente',
            'value' => 'My name',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'nif_cliente',
            'value' => 'My nif',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'direccion_facturacion',
            'value' => 'My address',
        ]);
        $this->assertDatabaseHas('mo_settings', [
            'name' => 'logo_image_id',
            'value' => $fileFactory->id,
        ]);
    }

}