<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class WebSliderShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta slider inexistente sin parametros
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/slider/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta página existente sin parametros
     */
    public function testOk1()
    {

        $SliderFactory = factory(App\Models\tenant\WebSlider::class)->create([
            'name' => 'test',
        ]);

        $this->json('GET', '/api/v1/slider/' . $SliderFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $SliderFactory->id,
                'name' => $SliderFactory->name
            ]);
    }
}