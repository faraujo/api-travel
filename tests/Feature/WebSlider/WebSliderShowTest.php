<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class WebSliderShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;


    /**
     * Test consulta productos con parametro lang
     */
    public function testOk1()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/slider', [
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta pagees con parametro lang en mayusculas
     */
    public function testOk2()
    {
        $SliderFactory = factory(App\Models\tenant\WebSlider::class)->create([
            'name' => 'test',
        ]);
        //Se especifica parametro lang correctamente en mayusculas
        $this->json('GET', '/api/v1/slider', [
            'name' => $SliderFactory->name
        ])->assertJsonFragment([
            'error' => 200,
            'id' => $SliderFactory->id,
            'name' => $SliderFactory->name
        ]);
    }
}