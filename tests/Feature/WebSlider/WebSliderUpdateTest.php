<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class WebSliderUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    public function testBad1()
    {

        $SliderFactory = factory(App\Models\tenant\WebSlider::class)->create([
            'name' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/slider', [
            ])
            ->assertExactJson([
                "error"=>400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "id"=>["The id field is required."]
                    ]]
                ]);
    }

    public function testBad2()
    {
        $SliderFactory = factory(App\Models\tenant\WebSlider::class)->create([
            'name' => 'test1',
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $this->json('PUT', '/api/v1/slider', [
            'id' => $SliderFactory->id,
            'files' => ['0' => $fileFactory->id, '1' => $fileFactory->id]
        ])
            ->assertExactJson([
                "error" => 400,
                "error_description"=>"The fields are not the required format",
                "error_inputs"=>[[
                    "files.0"=>["The files.0 field has a duplicate value."],
                    "files.1"=>["The files.1 field has a duplicate value."]
                ]]
            ]);
    }

    public function testOk1()
    {

        $SliderFactory = factory(App\Models\tenant\WebSlider::class)->create([
            'name' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/slider', [
            'id' => $SliderFactory->id])
            ->assertExactJson(["error" => 200]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations inexistentes
     */
    public function testOk2()
    {
        $SliderFactory = factory(App\Models\tenant\WebSlider::class)->create([
            'name' => 'test1',
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => 1,
        ]);

        $this->json('PUT', '/api/v1/slider', [
            'id' => $SliderFactory->id,
            'files' => ['0' => $fileFactory->id]
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_web_slider_file', [
            'slider_id' => $SliderFactory->id,
            'file_id' => $fileFactory->id,
        ]);
    }
}