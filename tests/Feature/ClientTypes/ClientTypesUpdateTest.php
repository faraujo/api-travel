<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ClientTypesUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada incorrecta enviando peticion sin parametros requeridos
     */
    function testBad1(){

        $this->json('PUT', '/api/v1/clienttype', [])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                'id' => ['The id field is required.'],
                'code' => ['The code field is required.'],
                //'name' => ['The name field is required.'],
                'from' => ['The from field is required.'],
                'to' => ['The to field is required.'],
                "translation" => ["you need at least one translation"],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametros con tipos incorrectos
     */

    function testBad2(){

        $this->json('PUT', '/api/v1/clienttype', [
            'id' => 'test',
            'code' => 'test',
            'from' => 'test',
            'to' => 'test',
            'name' => 'test',
            'api_id' => 'test',
            'hotel_age_required' => 'test',
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                //'name' => ['The name must be an array.'],
                'from' => ['The from must be an integer.'],
                'to' => ['The to must be an integer.'],
                'api_id' => ['The api id must be an integer.'],
                'hotel_age_required' => ['The hotel age required field must be true or false.'],
                "translation" => ["you need at least one translation"],
                'id' => ['The id must be an integer.']
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametro from mayor que parametro to
     */
    function testBad3(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $clientType = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTypeTranslation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $clientType->id,
        ]);

        $this->json('PUT', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 3,
            'to' => 1,
            'id' => $clientType->id,
            'name' => [$language->abbreviation => $clientTypeTranslation->name],
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                "from" => ["The field from must be less than to."],
            ]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $clientType = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTypeTranslation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $clientType->id,
            'name' => 'nombre test de tipo de cliente'
        ]);

        $this->json('PUT', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 1,
            'to' => 3,
            'api_id' => 100,
            'hotel_id' => 100,
            'hotel_code' => 'hotel code test',
            'hotel_age_required' => 1,
            'id' => $clientType->id,
            'name' => [$language->abbreviation => $clientTypeTranslation->name],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_client_type', [
            'code' => 'test', 
            'from' => 1, 
            'to' => 3,
            'api_id' => 100,
            'hotel_id' => 100,
            'hotel_code' => 'hotel code test',
            'hotel_age_required' => 1,
            'id' => $clientType->id, 
            'deleted_at' => null
        ]);
        $this->assertDatabaseHas('mo_client_type_translation', ['name' => $clientTypeTranslation->name, 'language_id' => $language->id, 'client_type_id' => $clientType->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada correcta comprueba el borrado de idiomas no actualizados y sin envío del parámetro 'hotel_age_required' (se fija a 0)
     */
    function testOk2(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $language2 = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test2']);

        //Generación de tipo de cliente por factory, con traducción en dos idiomas
        $clientType = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTypeTranslation1 = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => $language->id,
            'name' => 'nombre test de tipo de cliente idioma 1'
        ]);
        $clientTypeTranslation2 = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => $language2->id,
            'name' => 'nombre test de tipo de cliente idioma 2'
        ]);

        //Actualización de tipo de cliente enviando una única traducción
        $this->json('PUT', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 1,
            'to' => 3,
            'api_id' => 10,
            'hotel_id' => 20,
            'hotel_code' => 'hotel code test 2',
            'id' => $clientType->id,
            'name' => [$language->abbreviation => $clientTypeTranslation1->name],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_client_type', [
            'code' => 'test', 
            'from' => 1, 
            'to' => 3,
            'api_id' => 10,
            'hotel_id' => 20,
            'hotel_code' => 'hotel code test 2',
            'hotel_age_required' => 0,
            'id' => $clientType->id, 
            'deleted_at' => null
        ]);
        $this->assertDatabaseHas('mo_client_type_translation', ['name' => $clientTypeTranslation1->name, 'client_type_id' => $clientType->id, 'language_id' => $language->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_client_type_translation', ['name' => $clientTypeTranslation2->name, 'client_type_id' => $clientType->id, 'language_id' => $language2->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada correcta, intenta actualizar traduccion de tipo de cliente inextistente, debe crearla y borrar la traducción que de otro idioma que existiera
     */
    function testOk3(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $language2 = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test2']);

        $clientType = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTypeTranslation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'client_type_id' => $clientType->id,
            'language_id' => $language->id,
            'name' => 'name español test 3',
        ]);

        $this->assertDatabaseHas('mo_client_type', ['code' => $clientType->code, 'from' => $clientType->from, 'to' => $clientType->to,'id' => $clientType->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_client_type_translation', ['name' => $clientTypeTranslation->name, 'language_id' => $language->id,'client_type_id' => $clientType->id, 'deleted_at' => null]);


        $this->json('PUT', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 10,
            'to' => 30,
            'id' => $clientType->id,
            'name' => [$language2->abbreviation => 'name english test 3'],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_client_type', ['code' => 'test', 'from' => 10, 'to' => 30,'id' => $clientType->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_client_type_translation', ['name' => 'name english test 3', 'client_type_id' => $clientType->id, 'language_id' => $language2->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_client_type_translation', ['name' => $clientTypeTranslation->name, 'client_type_id' => $clientType->id, 'language_id' => $language->id, 'deleted_at' => null]);

    }

}