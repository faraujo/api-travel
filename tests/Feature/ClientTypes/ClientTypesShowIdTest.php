<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ClientTypesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta id de cliente no existente en base de datos
     */
    function testBad1(){
        $this->json('GET', '/api/v1/clienttype/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);
        $client_type = factory(\App\Models\tenant\ClientType::class)->create([]);
        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);
        $this->json('GET','/api/v1/clienttype/'.$client_type->id)
        ->assertExactJson(['error' => 200, 'data' => [[
            'client_type' => [[
                'id' => $client_type->id,
                'code' => $client_type->code,
                'from' => $client_type->from,
                'to' => $client_type->to,
                'api_id' => $client_type->api_id,
                'hotel_id' => $client_type->hotel_id,
                'hotel_code' => $client_type->hotel_code,
                'hotel_age_required' => $client_type->hotel_age_required,
                'lang' => [[
                    $language->abbreviation => [
                        'id' => $client_type_translation->id,
                        'language_id' => $language->id,
                        'name' => $client_type_translation->name,
                    ]
                ]]
            ]]]
        ]]);
    }

    /**
     * Comprueba base de datos con campos actualizados y estructura
     */
    function testOk2(){

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => 1,
            'client_type_id' => $client_type->id,
        ]);

        $this->assertDatabaseHas('mo_client_type', [
            'id' => $client_type->id,
            'code' => $client_type->code,
            'from' => $client_type->from,
            'to' => $client_type->to,
            'api_id' => $client_type->api_id,
            'hotel_id' => $client_type->hotel_id,
            'hotel_code' => $client_type->hotel_code,
            'hotel_age_required' => $client_type->hotel_age_required,
        ]);

        $this->assertDatabaseHas('mo_client_type_translation', [
            'name' => $client_type_translation->name,
        ]);

        $this->json('GET', '/api/v1/clienttype/'.$client_type->id, [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['client_type' => ["0" => ['id', 'code', 'from', 'to', 'api_id', 'hotel_id', 'hotel_code', 'hotel_age_required', 'lang']]]]
            ]);

    }

}