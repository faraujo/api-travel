<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ClientTypesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada con idioma incorrecto
     */
    function testBad1(){
        $this->json('GET','/api/v1/clienttype?lang=asdf')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'lang' => ['The lang is not exists'],
            ]]]);
    }

    /**
     * Prueba automatizada con formato de datos incorrectos
     */
    function testBad2(){
        $this->json('GET','/api/v1/clienttype?page=TEST&limit=TEST')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'page' => ['The page must be a number.'],
                'limit' => ['The limit must be a number.'],
            ]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $this->json('GET','/api/v1/clienttype')
            ->assertJsonStructure(['error','data','total_results']);
    }

    /**
     * Prueba automatizada con idioma correcto
     */
    function testOk2(){
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);
        $this->json('GET','/api/v1/clienttype?lang='.$language->abbreviation)
            ->assertJsonStructure(['error','data','total_results']);
}

    /**
     * Prueba automatizada con parametros en formato correcto
     */
    function testOk3(){
        $this->json('GET','/api/v1/clienttype?page=1&limit=1')
            ->assertJsonStructure(['error','data','total_results']);
    }

    /**
     * Prueba automatizada con parametros en formato correcto
     */
    function testOk4(){
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);
        $client_type = factory(\App\Models\tenant\ClientType::class)->create([]);
        factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);
        $this->json('GET','/api/v1/clienttype')
            ->assertJsonFragment([
                    'id' => $client_type->id]);
    }

    /**
     * Prueba automatizada con idioma correcto
     */
    function testOk5(){
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);
        $client_type = factory(\App\Models\tenant\ClientType::class)->create([]);
        factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
        ]);
        $this->json('GET','/api/v1/clienttype?lang=TEST')
            ->assertJsonFragment([
                'id' => $client_type->id]);
    }

    /**
     * Prueba automatizada, comprueba base de datos con campos actualizados y estructura
     */
    function testOk6(){

        $client_type = factory(\App\Models\tenant\ClientType::class)->create([]);

        $client_type_translation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => 1,
            'client_type_id' => $client_type->id,
        ]);

        $this->assertDatabaseHas('mo_client_type', [
            'id' => $client_type->id,
            'code' => $client_type->code,
            'from' => $client_type->from,
            'to' => $client_type->to,
            'api_id' => $client_type->api_id,
            'hotel_id' => $client_type->hotel_id,
            'hotel_code' => $client_type->hotel_code,
            'hotel_age_required' => $client_type->hotel_age_required,
        ]);

        $this->assertDatabaseHas('mo_client_type_translation', [
            'name' => $client_type_translation->name,
        ]);

        $this->json('GET', '/api/v1/clienttype', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['client_type' => ["0" => ['id', 'code', 'from', 'to', 'api_id', 'hotel_id', 'hotel_code', 'hotel_age_required', 'lang']]]]
            ]);
    }

    /**
     * Prueba automatizada con filtros (idioma + nombre)
     */
    function testOk7(){
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);
        $client_type = factory(\App\Models\tenant\ClientType::class)->create([]);
        factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $language->id,
            'client_type_id' => $client_type->id,
            'name' => 'nombre tipo cliente TEST'
        ]);
        $this->json('GET','/api/v1/clienttype?lang=TEST&name=nombre tipo cliente TEST')
            ->assertJsonFragment([
                'id' => $client_type->id]);
    }

}