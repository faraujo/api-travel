<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ClientTypesCreateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada incorrecta enviando peticion sin parametros requeridos
     */
    function testBad1(){
        $this->json('POST', '/api/v1/clienttype', [])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                'code' => ['The code field is required.'],
                //'name' => ['The name field is required.'],
                'from' => ['The from field is required.'],
                'to' => ['The to field is required.'],
                "translation" => ["you need at least one translation"],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametros con tipos incorrectos
     */

    function testBad2(){
        $this->json('POST', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 'test',
            'to' => 'test',
            'name' => 'test',
            'api_id' => 'test',
            'hotel_age_required' => 'test',
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                //'name' => ['The name must be an array.'],
                'from' => ['The from must be an integer.'],
                'to' => ['The to must be an integer.'],
                'api_id' => ['The api id must be an integer.'],
                'hotel_age_required' => ['The hotel age required field must be true or false.'],
                "translation" => ["you need at least one translation"],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametro from mayor que parametro to
     */
    function testBad3(){
        $this->json('POST', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 3,
            'to' => 1,
            'name' => ['ES' => 'test'],
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                "from" => ["The field from must be less than to."],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametros requeridos y name con lang incorrecta
     */
    function testBad4(){
        $this->json('POST', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 1,
            'to' => 10,
            'name' => ['bad' => 'test'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["translation"=>["you need at least one translation"]]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){

        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);
        $this->json('POST', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 0,
            'to' => 100,
            'api_id' => 100,
            'hotel_id' => 100,
            'hotel_code' => 'hotel code test',
            'hotel_age_required' => 1,
            'name' => [$languageFactory->abbreviation => 'test'],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_client_type', [
            'code' => 'test',
            'from' => 0,
            'to' => 100,
            'api_id' => 100,
            'hotel_id' => 100,
            'hotel_code' => 'hotel code test',
            'hotel_age_required' => 1,
        ]);

        $this->assertDatabaseHas('mo_client_type_translation', [
            'name' => [$languageFactory->abbreviation => 'test'],
        ]);
    }

    /**
     * Prueba automatizada correcta (sin envío del parámetro 'hotel_age_required' se fija a 0)
     */
    function testOk2(){

        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);
        $this->json('POST', '/api/v1/clienttype', [
            'code' => 'test',
            'from' => 0,
            'to' => 100,
            'api_id' => 10,
            'hotel_id' => 20,
            'hotel_code' => 'hotel code test 2',
            'name' => [$languageFactory->abbreviation => 'test'],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_client_type', [
            'code' => 'test',
            'from' => 0,
            'to' => 100,
            'api_id' => 10,
            'hotel_id' => 20,
            'hotel_code' => 'hotel code test 2',
            'hotel_age_required' => 0,
        ]);

        $this->assertDatabaseHas('mo_client_type_translation', [
            'name' => [$languageFactory->abbreviation => 'test'],
        ]);
    }

}