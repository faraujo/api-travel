<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ClientTypesDeleteTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin campo id requerido
     */
    function testBad1(){
        $this->json('DELETE','/api/v1/clienttype',[])
            ->assertExactJson(['error'=>400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'ids' => ['The ids field is required.']
        ]]]);
    }

    /**
     * Prueba automatizada campo id en formato incorrecto
     */
    function testBad2(){
        $this->json('DELETE','/api/v1/clienttype', [
            'ids' => ['TEST']
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'ids.0' => ['The ids.0 must be an integer.']
            ]]]);
    }

    /**
     * Prueba automatizada campo id inexistente en base de datos
     */
    function testBad3(){
        $this->json('DELETE','/api/v1/clienttype', [
            'ids' => [99999999999999999]
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found', "error_inputs" => [["ids.0" => ["The selected ids.0 is invalid."]]]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $clientFactory = factory(App\Models\tenant\ClientType::class)->create([
        ]);

        $this->assertDatabaseHas('mo_client_type', [
            'id' => $clientFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/clienttype', [
            'ids' => $clientFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Prueba automatizada campo id existente en base de datos
     */
    function testOk1(){
        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);
        $clientFactory = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTranslation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'client_type_id' => $clientFactory->id,
        ]);

        $this->json('DELETE','/api/v1/clienttype', [
            'ids' => [$clientFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_client_type', [
            'id' => $clientFactory->id,
            'code' => $clientFactory->code,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_client_type_translation', [
            'client_type_id' => $clientFactory->id,
            'deleted_at' => null
        ]);
    }

    /**
     * Prueba automatizada campo id existente en base de datos con relacion creada con promotion y promotion_product
     */
    function testOk2(){

        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);
        $clientFactory = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTranslation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'client_type_id' => $clientFactory->id,
        ]);

        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);

        $promotionFactory = factory(\App\Models\tenant\Promotion::class)->create([]);

        $promotionProductFactory = factory(\App\Models\tenant\PromotionProduct::class)->create([]);

        $promotionProductTypeFactory = factory(\App\Models\tenant\PromotionProductType::class)->create([]);


        $promotionClientTypeFactory = factory(\App\Models\tenant\PromotionClientType::class)->create([
            'client_type_id' => $clientFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $promotionProductClientTypeFactory = factory(\App\Models\tenant\PromotionProductClientType::class)->create([
            'client_type_id' => $clientFactory->id,
            'promotion_product_id' => $promotionProductFactory->id,
        ]);

        $promotionProductTypeClientTypeFactory = factory(\App\Models\tenant\PromotionProductTypeClientType::class)->create([
            'client_type_id' => $clientFactory->id,
            'promotion_product_type_id' => $promotionProductTypeFactory->id,
        ]);

        //comprueba que se han creado
        $this->assertDatabaseHas('mo_client_type', [
            'id' => $clientFactory->id,
            'code' => $clientFactory->code,
        ]);

        $this->assertDatabaseHas('mo_client_type_translation', [
            'client_type_id' => $clientFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_id' => $promotionProductFactory->id,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_type_id' => $promotionProductTypeFactory->id,
        ]);
        //fin comprobación creados

        //borra tipo de cliente
        $this->json('DELETE','/api/v1/clienttype', [
            'ids' => [$clientFactory->id],
        ])->assertExactJson(['error' => 200]);

        //comprueba que relaciones en tablas se han borrado
        $this->assertDatabaseMissing('mo_client_type', [
            'id' => $clientFactory->id,
            'code' => $clientFactory->code,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_client_type_translation', [
            'client_type_id' => $clientFactory->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_promotion_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_id' => $promotionFactory->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_promotion_product_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_id' => $promotionProductFactory->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_promotion_product_type_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_type_id' => $promotionProductTypeFactory->id,
            'deleted_at' => null
        ]);
    }

    /**
     * Prueba automatizada para borrar dos registros client_type en base de datos con relacion creada con promotion y promotion_product
     */
    function testOk3(){

        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);
        $clientFactory = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTranslation = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'client_type_id' => $clientFactory->id,
        ]);

        $languageFactory2 = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST2']);
        $clientFactory2 = factory(\App\Models\tenant\ClientType::class)->create([]);
        $clientTranslation2 = factory(\App\Models\tenant\ClientTypeTranslation::class)->create([
            'language_id' => $languageFactory2->id,
            'client_type_id' => $clientFactory2->id,
        ]);



        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $productFactory2 = factory(\App\Models\tenant\Product::class)->create([]);

        $promotionFactory = factory(\App\Models\tenant\Promotion::class)->create([]);
        $promotionFactory2 = factory(\App\Models\tenant\Promotion::class)->create([]);

        $promotionProductFactory = factory(\App\Models\tenant\PromotionProduct::class)->create([]);
        $promotionProductFactory2 = factory(\App\Models\tenant\PromotionProduct::class)->create([]);

        $promotionProductTypeFactory = factory(\App\Models\tenant\PromotionProductType::class)->create([]);
        $promotionProductTypeFactory2 = factory(\App\Models\tenant\PromotionProductType::class)->create([]);


        $promotionClientTypeFactory = factory(\App\Models\tenant\PromotionClientType::class)->create([
            'client_type_id' => $clientFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);
        $promotionClientTypeFactory2 = factory(\App\Models\tenant\PromotionClientType::class)->create([
            'client_type_id' => $clientFactory2->id,
            'promotion_id' => $promotionFactory2->id,
        ]);


        $promotionProductClientTypeFactory = factory(\App\Models\tenant\PromotionProductClientType::class)->create([
            'client_type_id' => $clientFactory->id,
            'promotion_product_id' => $promotionProductFactory->id,
        ]);
        $promotionProductClientTypeFactory2 = factory(\App\Models\tenant\PromotionProductClientType::class)->create([
            'client_type_id' => $clientFactory2->id,
            'promotion_product_id' => $promotionProductFactory2->id,
        ]);


        $promotionProductTypeClientTypeFactory = factory(\App\Models\tenant\PromotionProductTypeClientType::class)->create([
            'client_type_id' => $clientFactory->id,
            'promotion_product_type_id' => $promotionProductTypeFactory->id,
        ]);
        $promotionProductTypeClientTypeFactory = factory(\App\Models\tenant\PromotionProductTypeClientType::class)->create([
            'client_type_id' => $clientFactory2->id,
            'promotion_product_type_id' => $promotionProductTypeFactory2->id,
        ]);

        //comprueba que se han creado
        $this->assertDatabaseHas('mo_client_type', [
            'id' => $clientFactory->id,
            'code' => $clientFactory->code,
        ]);
        $this->assertDatabaseHas('mo_client_type', [
            'id' => $clientFactory2->id,
            'code' => $clientFactory2->code,
        ]);


        $this->assertDatabaseHas('mo_client_type_translation', [
            'client_type_id' => $clientFactory->id,
        ]);
        $this->assertDatabaseHas('mo_client_type_translation', [
            'client_type_id' => $clientFactory2->id,
        ]);


        $this->assertDatabaseHas('mo_promotion_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_id' => $promotionFactory->id,
        ]);
        $this->assertDatabaseHas('mo_promotion_client_type',[
            'client_type_id' => $clientFactory2->id,
            'promotion_id' => $promotionFactory2->id,
        ]);


        $this->assertDatabaseHas('mo_promotion_product_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_id' => $promotionProductFactory->id,
        ]);
        $this->assertDatabaseHas('mo_promotion_product_client_type',[
            'client_type_id' => $clientFactory2->id,
            'promotion_product_id' => $promotionProductFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_promotion_product_type_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_type_id' => $promotionProductTypeFactory->id,
        ]);
        $this->assertDatabaseHas('mo_promotion_product_type_client_type',[
            'client_type_id' => $clientFactory2->id,
            'promotion_product_type_id' => $promotionProductTypeFactory2->id,
        ]);
        //fin comprobación creados

        //borra tipos de cliente
        $this->json('DELETE','/api/v1/clienttype', [
            'ids' => [$clientFactory->id, $clientFactory2->id],
        ])->assertExactJson(['error' => 200]);

        //comprueba que relaciones en tablas se han borrado
        $this->assertDatabaseMissing('mo_client_type', [
            'id' => $clientFactory->id,
            'code' => $clientFactory->code,
            'deleted_at' => null
        ]);
        $this->assertDatabaseMissing('mo_client_type', [
            'id' => $clientFactory2->id,
            'code' => $clientFactory2->code,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_client_type_translation', [
            'client_type_id' => $clientFactory->id,
            'deleted_at' => null
        ]);
        $this->assertDatabaseMissing('mo_client_type_translation', [
            'client_type_id' => $clientFactory2->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_promotion_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_id' => $promotionFactory->id,
            'deleted_at' => null
        ]);
        $this->assertDatabaseMissing('mo_promotion_client_type',[
            'client_type_id' => $clientFactory2->id,
            'promotion_id' => $promotionFactory2->id,
            'deleted_at' => null
        ]);


        $this->assertDatabaseMissing('mo_promotion_product_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_id' => $promotionProductFactory->id,
            'deleted_at' => null
        ]);
        $this->assertDatabaseMissing('mo_promotion_product_client_type',[
            'client_type_id' => $clientFactory2->id,
            'promotion_product_id' => $promotionProductFactory2->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_promotion_product_type_client_type',[
            'client_type_id' => $clientFactory->id,
            'promotion_product_type_id' => $promotionProductTypeFactory->id,
            'deleted_at' => null
        ]);
        $this->assertDatabaseMissing('mo_promotion_product_type_client_type',[
            'client_type_id' => $clientFactory2->id,
            'promotion_product_type_id' => $promotionProductTypeFactory2->id,
            'deleted_at' => null
        ]);
    }

}