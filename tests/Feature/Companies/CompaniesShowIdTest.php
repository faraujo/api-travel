<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CompaniesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;
   

    /**
     * Consulta compañía inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/company/999999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar compañía existente
     */
    public function testOk2()
    {
        $companyFactory = factory(\App\Models\tenant\Company::class)->create([]);
        $this->json('GET', '/api/v1/company/' . $companyFactory->id, [])
        ->assertExactJson(['error' => 200, 'data' => [[
            'company' => [[
                'id' => $companyFactory->id,
                'name' => $companyFactory->name
            ]]]
        ]]);
    }
}