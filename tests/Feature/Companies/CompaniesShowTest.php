<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CompaniesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de compañias con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/company', [
            'page' => 'string',
            'limit' => 'string',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["limit"=>["The limit must be an integer."],
                    "page"=>["The page must be an integer."]]]]);
    }

    /**
     * test crea factory de compañía y comprueba que esta en la salida y comprueba estructura
     */
    public function testOk1()
    {

        //Factory de compañia
        $companyFactory = factory(App\Models\tenant\Company::class)->create([
            'name' => 'name compañia test 1',
        ]);

        $this->assertDatabaseHas('mo_company', [
            'id' => $companyFactory->id,
            'name' => $companyFactory->name,
        ]);

        $this->json('GET', '/api/v1/company', [])
            ->assertJsonFragment([
                'error' => 200,
                'name' => $companyFactory->name,
                'id' => $companyFactory->id,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/company', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['company' => ["0" => ['id','name']]]]
            ]);

    }
}