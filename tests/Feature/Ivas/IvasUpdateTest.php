<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IvasUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se envía un campo value con un string donde debe ser numérico
     */
    public function testBad1()
    {
        $this->json('put', '/api/v1/product/iva', [
            'id' => 1,
            'value' => '15,15',
            'name' => ['ES' => 'Nombre modificado Español']
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["value" => ["The value must be a number."]
            ]]
            ]);
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se envía un campo idioma inexistente
     */
    public function testBad2()
    {
        $this->json('put', '/api/v1/product/iva', [
            'id' => 1,
            'value' => 0.025,
            'name' => ['EFGH' => 'Nombre modificado Inglés']
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]
            ]]
            ]);
    }

    /**
     * Test que comprueba funcionamiento de las validaciones
     *
     * Se envía un campo id inexistente
     */
    public function testBad3()
    {
        $this->json('put', '/api/v1/product/iva', [
            'id' => -1,
            'value' => 0.025,
            'name' => ['ES' => 'Nombre modificado España']
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found"]
            );
    }
   

    
    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *
     */
    public function testOk1()
    {

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([

        ]);

        $this->assertDatabaseHas('mo_iva', [
            'id' => $ivaFactory->id,
        ]);

        $this->json('put', '/api/v1/product/iva', [
            'id' => $ivaFactory->id,
            'name' => ['ES' => 'IvaName'],
            'value' => 0.034])
            ->assertExactJson([
                'error' => 200
            ]);
    }

}