<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IvasShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que consulta una tasa de Iva con el Id erróneo
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/product/iva/99999999999999999999', [
        ])->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Test consulta las tasas de iva de forma correcta 
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/product/iva', [
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }


    /**
     * Test crea una tasa de Iva y lo comprueba de forma correcta
     */
    public function testOk2()
    {
        //crea producto
        $ivaFact = factory(App\Models\tenant\Iva::class)->create([
        ]);

        $this->json('GET', '/api/v1/product/iva/'.$ivaFact->id, [
        ])->assertJsonStructure(['error', 'data']);
    }

    /**
     * Test consulta tasa de iva con traducción
     */
    public function testOk3()
    {

        //crea factory de tasa de iva
        $factory = factory(App\Models\tenant\Iva::class)->create([
        ]);
        //crea traducción en español de tasa de iva
        factory(App\Models\tenant\IvaTranslation::class)->create([
            'iva_id' => $factory->id,
            'language_id' => 1,
            'name' => 'testES'
        ]);


        //se consulta por id de la tasa de iva creada, y devuelve el registro
        $this->json('GET', '/api/v1/product/iva/'.$factory->id, [])->assertJsonFragment(['id' => $factory->id]);

    }
}