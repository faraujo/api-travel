<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IvasCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía el campo requerido value
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/product/iva', [

            'name' => ['ES' => 'Tasa Iva España']
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["value" => ["The value field is required."]
            ]]
            ]);
    }

     /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda el idioma inexistente
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/product/iva', [
            'value' => 1.125,
            'name' => ['ABC' => 'Tasa Iva España']
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda el campo value de forma errónea con un string
     */
    public function testBad3()
    {
        $this->json('POST', '/api/v1/product/iva', [
            'value' => 'MiValue',
            'name' => ['ES' => 'Tasa Iva España']
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["value" => ["The value must be a number."]
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda el campo value de forma errónea con coma como separador decimal
     */
    public function testBad4()
    {
        $this->json('POST', '/api/v1/product/iva', [
            'value' => '0,15',
            'name' => ['ES' => 'Tasa Iva España']
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["value" => ["The value must be a number."]
            ]]
            ]);
    }
    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda el campo name
     */
    public function testBad5()
    {
        $this->json('POST', '/api/v1/product/iva', [
            'value' => 0.25,
            
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]
            ]]
            ]);
    }
    
    
    /**
     * Realiza el test de creación de una tasa de iva con su traducción correspondiente
     * Comprueba que la traducción se han insertado correctamente en base de datos
     */
    public function testOK1()
    {
        $this->json('POST', '/api/v1/product/iva', [
            'value' => 0.05,
            'name' => ['ES' => 'Tasa Iva Test']])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_iva_translation', [
            'language_id' => 1,
            'name' => 'Tasa Iva Test'
        ]);

    }

}