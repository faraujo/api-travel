<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IvasDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/product/iva', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/product/iva', [
            'ids' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/product/iva', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' => [["ids.0" => ["The selected ids.0 is invalid."]]]
            ]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([
        ]);

        $this->assertDatabaseHas('mo_iva', [
            'id' => $ivaFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/product/iva', [
            'ids' => $ivaFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }


    /**
     * Test que crea una tasa de iva y posteriormente la borra
     */
    public function testOk1()
    {

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([
        ]);
        $ivaTranslationFactory = factory(App\Models\tenant\IvaTranslation::class)->create([
            'iva_id' => $ivaFactory->id,
            'language_id' => 1,
            'name' => 'Test'
            ]);

        $this->assertDatabaseHas('mo_iva', [
            'id' => $ivaFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/product/iva', [
            'ids' => [$ivaFactory->id],
        ])
            ->assertExactJson([
                "error"=>200
            ]);

    }

    /**
     * Test que crea dos tasas de iva y posteriormente la borra
     */
    public function testOk2()
    {

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([
        ]);
        $ivaTranslationFactory = factory(App\Models\tenant\IvaTranslation::class)->create([
            'iva_id' => $ivaFactory->id,
            'language_id' => 1,
            'name' => 'Test'
            ]);

        $this->assertDatabaseHas('mo_iva', [
            'id' => $ivaFactory->id,
        ]);

        $ivaFactory2 = factory(App\Models\tenant\Iva::class)->create([
            ]);
            $ivaTranslationFactory2 = factory(App\Models\tenant\IvaTranslation::class)->create([
                'iva_id' => $ivaFactory2->id,
                'language_id' => 1,
                'name' => 'Test'
                ]);
    
            $this->assertDatabaseHas('mo_iva', [
                'id' => $ivaFactory2->id,
            ]);

        $this->json('DELETE', '/api/v1/product/iva', [
            'ids' => [$ivaFactory->id, $ivaFactory2->id],
        ])
            ->assertExactJson([
                "error"=>200
            ]);

    }

}