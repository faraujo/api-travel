<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class ViewRoomsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan files y order en formato incorrecto
     */
    public function testBad1()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'published' => 0,
            'files' => [0 => 'r'],'published' => 1,
            'order' => 'r',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[

                    "files.0" => ["The files.0 must be an integer."],
                    "order" => ["The order must be an integer."],

                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan files inexistentes
     */
    public function testBad2()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,

            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],'published' => 1,

            'files' => [0 => 99999999],

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[

                    "files.0" => ["The selected files.0 is invalid."],

                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el campo files sin formato array.
     */
    public function testBad3()
    {

        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],'published' => 1,
            'files' => '2',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[

                "files" => ["The files must be an array."],

            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el files con valor duplicado.
     */
    public function testBad4()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'files' => [0 => $fileFactory->id, 1 => $fileFactory->id],'published' => 1,
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "files.0" => ["The files.0 field has a duplicate value."],
                "files.1" => ["The files.1 field has a duplicate value."],
            ]]]);
    }

    /**
     * test para comprobar el validador de campos obligatorios
     */
    public function testBad5()
    {
        $this->json('PUT', '/api/v1/viewproduct/room', [

            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],'published' => 1,
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["room_id" => ["The room id field is required."],
                "subchannel_id" => ["The subchannel id field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía una habitación sin traduccion
     */
    public function testBad6()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewroomFactory->id,
        ]);
        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 2,
            'view_room_id' => $viewroomFactory->id,
        ]);
        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 3,
            'view_room_id' => $viewroomFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_room', [
            'id' => $viewroomFactory->id,

        ]);

        $this->assertDatabaseHas('mo_view_room_translation', [
            'view_room_id' => $viewroomFactory->id,
        ]);


        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,'published' => 1,
        ])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía una habitación y como traducción el campo services_included
     */
    public function testBad7()
    {

        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [

            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,

            'services_included' => ['ES' => 'Haname']])
            ->assertExactJson([
                "error" => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [[

                    "description.ES" => ["The description. e s field is required."],
                    "description_seo.ES" => ["The description seo. e s field is required."],
                    "name.ES" => ["The name. e s field is required."],
                    "short_description.ES" => ["The short description. e s field is required."],
                    "title_seo.ES" => ["The title seo. e s field is required."]]]
            ]);
    }

    /**
     *Envía una habitación con una sola traducción y comprueba luego que se borran las otras 2 que había al principio
     *
     */
    public function testOk1()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
            'order' => 0,
        ]);

        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 2,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 3,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name portugues',
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $this->assertDatabaseHas('mo_view_room', [
            'id' => $viewroomFactory->id,
            'order' => 0,
        ]);

        $this->assertDatabaseHas('mo_view_room_translation', [
            'language_id' => 1,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name español',
        ]);

        $this->assertDatabaseHas('mo_view_room_translation', [
            'language_id' => 2,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name ingles',
        ]);

        $this->assertDatabaseHas('mo_view_room_translation', [
            'language_id' => 3,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name portugues',
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'order' => 2,
            'name' => ['ES' => 'name modificado Español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'services_included' => ['ES' => 'services_included español'],
            'services_not_included' => ['ES' => 'services_not_included español'],
            'location' => ['ES' => 'locations español'],
            'views' => ['ES' => 'views español'],
            'size' => ['ES' => 'size español'],
            'capacity' => ['ES' => 'capacity español'],

            'index' => ['ES' => 0],
            'breadcrumb' => ['ES' => 'keywords_seo español'],
            'rel' => ['ES' => 'keywords_seo español'],
            'og_title' => ['ES' => 'keywords_seo español'],
            'og_description' => ['ES' => 'keywords_seo español'],
            'og_image' => ['ES' => $fileFactory->id],
            'twitter_title' => ['ES' => 'keywords_seo español'],
            'twitter_description' => ['ES' => 'keywords_seo español'],
            'twitter_image' => ['ES' => $fileFactory->id],
            'canonical_url' => ['ES' => 'keywords_seo español'],
            'script_head' => ['ES' => 'keywords_seo español'],
            'script_body' => ['ES' => 'keywords_seo español'],
            'script_footer' => ['ES' => 'keywords_seo español'],
            'published' => 1
        ])
            ->assertExactJson([
                'error' => 200
            ]);

        //comprueba cambio de campo order
        $this->assertDatabaseHas('mo_view_room', [
            'id' => $viewroomFactory->id,
            'order' => 2,
        ]);

        //comprueba que se ha modificado la traduccion en español
        $this->assertDatabaseHas('mo_view_room_translation', [
            'language_id' => 1,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name modificado Español',
            'services_included' => ['ES' => 'services_included español'],
            'services_not_included' => ['ES' => 'services_not_included español'],
            'location' => ['ES' => 'locations español'],
            'views' => ['ES' => 'views español'],
            'size' => ['ES' => 'size español'],
            'capacity' => ['ES' => 'capacity español'],
            'deleted_at' => null,
        ]);

        //comprueba que la traduccion que habia en ingles se ha eliminado
        $this->assertDatabaseMissing('mo_view_room_translation', [
            'language_id' => 2,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name ingles',
            'deleted_at' => null,
        ]);

        //comprueba que la traduccion que habia en portugues se ha eliminado
        $this->assertDatabaseMissing('mo_view_room_translation', [
            'language_id' => 3,
            'view_room_id' => $viewroomFactory->id,
            'name' => 'name portugues',
            'deleted_at' => null,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación de archivos a habitaciones.
     *
     * Se crea factory de files, se hace factory de room_file, al mandar relación con diferente file, borra las relaciones anteriores y crea las nuevas
     */
    public function testOk2()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewroomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
        ]);

        $this->assertDatabaseHas('mo_view_room', [
            'id' => $viewroomFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'url_file' => $fileFactory->url_file,
        ]);

        //se crea factory con relación de viewroom y file
        $viewFileFactory = factory(App\Models\tenant\ViewRoomFile::class)->create([
            'view_room_id' => $viewroomFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_room_file', [
            'view_room_id' => $viewroomFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        //se crea nueva file para hacer nueva asignación y comprobar que borra la anterior
        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('nueva.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory2->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => $fileFactory2->id],
            'published' => 1])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_view_room_file', [
            'file_id' => $fileFactory2->id,
        ]);

        $this->assertDatabaseMissing('mo_view_room_file', [
            'file_id' => $fileFactory->id,
            'deleted_at' => null,
        ]);

    }

}