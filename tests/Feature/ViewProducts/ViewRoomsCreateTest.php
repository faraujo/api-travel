<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class ViewRoomsCreateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se envía traduccion y se manda el resto de campos en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => 'r',
            'subchannel_id' => 'r',
            'name' => ['ES' => 'Nombre español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => 'r'],
            'order' => 'r',
            "published" => 0
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["files.0" => ["The files.0 must be an integer."], "order" => ["The order must be an integer."],
                    "room_id" => ["The room id must be an integer."],
                    "subchannel_id" => ["The subchannel id must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se manda traducción y se manda files, habitación y subcanal inexistentes
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => 9999999,
            'subchannel_id' => 9999999,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            "published" => 0,
            'files' => [0 => 99999999],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["files.0" => ["The selected files.0 is invalid."], "room_id" => ["The selected room id is invalid."],
                    "subchannel_id" => ["The selected subchannel id is invalid."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el campos files sin formato array.
     */
    public function testBad3()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            "published" => 0,
            'files' => '2',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "files" => ["The files must be an array."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el campo files con valor duplicado.
     */
    public function testBad4()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);
        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            "published" => 0,
            'files' => [0 => $fileFactory->id, 1 => $fileFactory->id],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "files.0" => ["The files.0 field has a duplicate value."],
                "files.1" => ["The files.1 field has a duplicate value."],
            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se manda ningún campo, pide requeridos
     */
    public function testBad5()
    {
        $this->json('PUT', '/api/v1/viewproduct/room', [
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["room_id" => ["The room id field is required."],"published"=>["The published field is required."],
                    "subchannel_id" => ["The subchannel id field is required."], "translation" => ["you need at least one translation"],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía una habitación y como traducción el campo services_included
     */
    public function testBad6()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            "published" => 0,
            'services_included' => ['ES' => 'Haname']])
            ->assertExactJson([
                "error" => 400,
                "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "description.ES" => ["The description. e s field is required."],
                    "description_seo.ES" => ["The description seo. e s field is required."],
                    "name.ES" => ["The name. e s field is required."],
                    "short_description.ES" => ["The short description. e s field is required."],
                    "title_seo.ES" => ["The title seo. e s field is required."]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía una habitación sin traduccion
     */
    public function testBad7()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            "published" => 0,
            'subchannel_id' => $subchannelFactory->id,
        ])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un name con lang erroneo
     */
    public function testLang()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'Haname españa', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'IND' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            "published" => 0,
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués']])
            ->assertExactJson([
                'error' => 400, 'error_description' => "The fields are not the required format", 'error_inputs' => [["name.PT" => ["The name. p t field is required."]]]
            ]);
    }

    /**
     * Realiza el test de creación de habitaciones con sus traducciones correspondientes
     *
     * Comprueba campo order al no enviarse guarda 0 por defecto
     */
    public function testOk1()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'Haname españa', 'EN' => 'Cualquiera NPO N ñ ÑÑ Ó Inglés', 'PT' => 'Cuanlquiera portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español', 'EN' => 'Short descripcion inglés', 'PT' => 'Short descripción portugués'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],

            'index' => ['ES' => 0, 'EN' => 1, 'PT' => 1],
            'breadcrumb' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'rel' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'og_image' => ['ES' => $fileFactory->id],
            'twitter_title' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_description' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'twitter_image' => ['ES' => $fileFactory->id],
            'canonical_url' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_head' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'script_body' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            "published" => 0,
            'script_footer' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués']])
            ->assertExactJson([
                'error' => 200,
            ]);

        $this->assertDatabaseHas('mo_view_room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            //comprueba que campo order no se envía guarda 0 por defecto
            'order' => 0,

        ]);

    }

    /**
     * Realiza el test de creación de habitaciones con sus traducciones correspondientes.
     *
     */
    public function testOk2()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'Nombre españa', 'EN' => 'Nombre Inglés', 'PT' => 'Nombre portugués'],
            'description' => ['ES' => 'Descripcion español', 'EN' => 'Descripcion inglés', 'PT' => 'Descripción portugués'],
            'short_description' => ['ES' => 'Short descripcion español test create', 'EN' => 'Short descripcion inglés test create', 'PT' => 'Short descripción portugués test create'],
            'friendly_url' => ['ES' => 'friendly_url español', 'EN' => 'friendly_url inglés', 'PT' => 'friendly_url portugués'],
            'title_seo' => ['ES' => 'title_seo español', 'EN' => 'title_seo inglés', 'PT' => 'title_seo portugués'],
            'description_seo' => ['ES' => 'description_seo español', 'EN' => 'description_seo inglés', 'PT' => 'description_seo portugués'],
            'keywords_seo' => ['ES' => 'keywords_seo español', 'EN' => 'keywords_seo inglés', 'PT' => 'keywords_seo portugués'],
            'services_included' => ['ES' => 'services_included modificada español test create', 'EN' => 'services_included modificada inglés test create', 'PT' => 'services_included modificada portugués test create'],
            'services_not_included' => ['ES' => 'services_not_included modificada español test create', 'EN' => 'services_not_included modificada inglés test create', 'PT' => 'services_not_included modificada portugués test create'],
            'legal' => ['ES' => 'condiciones legales es', 'EN' => 'condiciones legales en', 'PT' => 'condiciones legales pt'],
            'order' => 2,
            'location' => ['ES' => 'location modificada español test create español', 'EN' => 'location modificada inglés test create', 'PT' => 'location modificada portugués test create'],
            'views' => ['ES' => 'views modificada español test create español', 'EN' => 'views modificada inglés test create', 'PT' => 'views modificada portugués test create'],
            'size' => ['ES' => 'size modificada español test create español', 'EN' => 'size modificada inglés test create', 'PT' => 'size modificada portugués test create'],
            "published" => 0,
            'capacity' => ['ES' => 'capacity modificada español test create español', 'EN' => 'capacity modificada inglés test create', 'PT' => 'capacity modificada portugués test create'],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);
        $this->assertDatabaseHas('mo_view_room', [
            'order' => 2,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_room_translation', [
            'short_description' => 'Short descripcion español test create',
            'language_id' => 1,
            'services_included' => 'services_included modificada español test create',
            'services_not_included' => 'services_not_included modificada español test create',
            'legal' => 'condiciones legales es',
            'location' => 'location modificada español test create español',
            'views' => 'views modificada español test create español',
            'size' => 'size modificada español test create español',
            'capacity' => 'capacity modificada español test create español'
        ]);

        $this->assertDatabaseHas('mo_view_room_translation', [
            'short_description' => 'Short descripcion inglés test create',
            'language_id' => 2,
            'services_included' => 'services_included modificada inglés test create',
            'services_not_included' => 'services_not_included modificada inglés test create',
            'legal' => 'condiciones legales en',
            'location' => 'location modificada inglés test create',
            'views' => 'views modificada inglés test create',
            'size' => 'size modificada inglés test create',
            'capacity' => 'capacity modificada inglés test create'
        ]);

        $this->assertDatabaseHas('mo_view_room_translation', [
            'short_description' => 'Short descripción portugués test create',
            'language_id' => 3,
            'services_included' => 'services_included modificada portugués test create',
            'services_not_included' => 'services_not_included modificada portugués test create',
            'legal' => 'condiciones legales pt',
            'location' => 'location modificada portugués test create',
            'views' => 'views modificada portugués test create',
            'size' => 'size modificada portugués test create',
            'capacity' => 'capacity modificada portugués test create',
        ]);

    }

    /**
     *Realiza el test de creación de habitaciones con sus traducciones correspondientes.
     *
     *Envía una habitación con una sola traduccion
     */
    public function testOk3()
    {
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'name' => ['ES' => 'Haname'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            "published" => 0,
            'keywords_seo' => ['ES' => 'keywords_seo español']])
            ->assertExactJson([
                'error' => 200
            ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de asignación de archivos a una habitación
     *
     * Se crea factory de file, se asigna file al crear habitación y se comprueba en base de datos
     */
    public function testOk4()
    {
        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([]);
        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('PUT', '/api/v1/viewproduct/room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'order' => 2,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            "published" => 0,
            'files' => [0 => $fileFactory->id]])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);
        $this->assertDatabaseHas('mo_view_room_file', [
            'file_id' => $fileFactory->id,
        ]);
    }
}