<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ViewRoomsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta rooms por subcanal sin mandar requeridos
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/viewproduct/room', [
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [["subchannel_id" => ["The subchannel id field is required."]]]]);
    }

    /**
     *  Test consulta de rooms por subcanal mandando todos los filtros posibles en formato incorrecto excepto code que no tiene validación posible
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/viewproduct/room', [
            'category_id' => 'd',
            'hotel_id' => 'r',
            'page' => 'e',
            'limit' => 'e',
            'subchannel_id' => 'e',
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [["category_id" => ["The category id must be an integer."],"hotel_id" => ["The hotel id must be an integer."],"limit" => ["The limit must be an integer."],
                "page" => ["The page must be an integer."],
                "subchannel_id" => ["The subchannel id must be an integer."]]]]);
    }

    /**
     *  Test consulta de habitaciones para subcanal creado y relacionado a habitación con tarifa, tiene registro en viewroom y traducciones así que mostrará
     * las traducciones de viewroom
     */
    public function testOk1()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'channel_id' => $channelFactory->id,
        ]);


        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $roomFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $roomFactory->id,
            'name' => 'name portugues',
        ]);


        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewRoomTranslationFactory1 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewRoomTranslationFactory2 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 2,
            'view_Room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewRoomTranslationFactory3 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 3,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);
        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id])->assertJsonFragment(['id' => $roomFactory->id, 'name' => $viewRoomTranslationFactory1->name]);

    }

    /**
     *  Test consulta de rooms para subcanal creado y relacionado a room con tarifa, no tiene registro en viewroom así que mostrará
     * las traducciones de room
     */
    public function testOk2()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        $roomTranslationFactory1 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);
        $roomTranslationFactory2 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $roomFactory->id,
            'name' => 'name ingles',
        ]);
        $roomTranslationFactory3 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $roomFactory->id,
            'name' => 'name portugues',
        ]);


        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id])->assertJsonFragment(['id' => $roomFactory->id, 'name' => $roomTranslationFactory1->name]);

    }

    /**
     *  Test consulta de room para subcanal creado y relacionado a room que no tiene tarifa asociada, no debe mostrar habitación
     *
     */
    public function testOk3()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);
        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'channel_id' => $channelFactory->id,
        ]);

        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $roomFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $roomFactory->id,
            'name' => 'name portugues',
        ]);

        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewRoomTranslationFactory1 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewRoomTranslationFactory2 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 2,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewRoomTranslationFactory3 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 3,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);

        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id])->assertExactJson(["data" => [], "error" => 200, "total_results" => 0]);

        //asigna una tarifa al hotel y ahora sí va a mostrar la habitación en la salida
        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id])->assertJsonFragment(["id" => $roomFactory->id, 'total_results' => 1]);

    }

    /**
     *  Test prueba filtro category_id, hotel_id y code
     */
    public function testOk4()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([
        ]);

        factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'name' => 'category español',
        ]);

        factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 2,
            'category_id' => $categoryFactory->id,
            'name' => 'category ingles',
        ]);

        factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 3,
            'category_id' => $categoryFactory->id,
            'name' => 'category portugues',
        ]);

        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
            'code' => 'cohotel'
        ]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'channel_id' => $channelFactory->id,
        ]);


        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 1,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 2,
            'room_id' => $roomFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => 3,
            'room_id' => $roomFactory->id,
            'name' => 'name portugues',
        ]);


        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewRoomTranslationFactory1 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewRoomTranslationFactory2 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 2,
            'view_Room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewRoomTranslationFactory3 = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 3,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name viewproduct',
        ]);
        //prueba filtro category_id
        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id,'category_id' => $categoryFactory->id])->assertJsonFragment(['id' => $roomFactory->id, 'name' => $viewRoomTranslationFactory1->name,'total_results' => 1]);
        //prueba filtro hotel_id
        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id,'hotel_id' => $productFactory->id])->assertJsonFragment(['id' => $roomFactory->id, 'name' => $viewRoomTranslationFactory1->name,'total_results' => 1]);
        //prueba filtro code
        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id,'code' => $roomFactory->code])->assertJsonFragment(['id' => $roomFactory->id, 'name' => $viewRoomTranslationFactory1->name,'total_results' => 1]);
        //prueba los 3 filtros juntos
        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id,'category_id' => $categoryFactory->id,'hotel_id' => $productFactory->id,'code' => $roomFactory->code])->assertJsonFragment(['id' => $roomFactory->id, 'name' => $viewRoomTranslationFactory1->name,'total_results' => 1]);
        $roomFactory->delete();

        $this->json('GET', '/api/v1/viewproduct/room', ['subchannel_id' => $subchannelFactory->id,'category_id' => $categoryFactory->id])->assertJsonFragment(['total_results' => 0]);

    }

}