<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ViewProductsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta productos por subcanal sin mandar requeridos
     */
    public function testBad1()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/viewproduct', [
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["subchannel_id"=>["The subchannel id field is required."]]]]);
    }

    /**
     *  Test consulta de productos por subcanal mandando todos los filtros posibles en formato incorrecto
     */
    public function testBad2()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/viewproduct', [
            'type_id' => 'r',
            'category_id' => 'd',
            'location_id' => 'p',
            'page' => 'e',
            'limit' => 'e',
            'subchannel_id' => 'e',
            'published' => 'r'
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["category_id"=>["The category id must be an integer."],"limit"=>["The limit must be an integer."],
                "location_id"=>["The location id must be an integer."],"page"=>["The page must be an integer."],"published"=>["The published field must be true or false."],
                "subchannel_id"=>["The subchannel id must be an integer."],"type_id"=>["The type id must be an integer."]]]]);
    }


    /**
     *  Test consulta de productos para subcanal creado y relacionado a producto con tarifa, tiene registro en viewproduct y traducciones así que mostrará
     * las traducciones de viewproduct
     */
    public function testOk1()
    {
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory =  factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
            'name' => 'name portugues',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewproductTranslationFactory1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name viewproduct',
            'discover_url' => 'discover parque español'
        ]);

        $viewproductTranslationFactory2 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewproductTranslationFactory3 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name viewproduct',
        ]);
        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id])->assertJsonFragment(['id'=>$productFactory->id,'name' => $viewproductTranslationFactory1->name, 'discover_url' => $viewproductTranslationFactory1->discover_url]);

    }

    /**
     *  Test consulta de productos para subcanal creado y relacionado a producto con tarifa, no tiene registro en viewproduct así que mostrará
     * las traducciones de product
     */
    public function testOk2()
    {
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $productTranslationFactory1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);
        $productTranslationFactory2 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
            'name' => 'name ingles',
        ]);
        $productTranslationFactory3 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
            'name' => 'name portugues',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id])->assertJsonFragment(['id'=>$productFactory->id,'name' => $productTranslationFactory1->name]);

    }

    /**
     *  Test consulta de productos para subcanal creado y relacionado a producto con tarifa, y relacionado a tipo de producto por el que vamos a filtrar.
     * Hacemos 2 factories de tipo de producto, relacionamos uno al producto y filtramos por él, comprobamos en salida que nos devuelve el id del producto y total_results 1,
     * intentamos filtrar por el segundo factory de tipo, que no hemos relacionado al producto, y comprobamos que devuelve total_results 0
     */
    public function testOk3()
    {
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([
        ]);

        $typeFactory2 = factory(App\Models\tenant\Type::class)->create([
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
            'type_id' => $typeFactory->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
            'name' => 'name portugues',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'type_id' => $typeFactory->id])->assertJsonFragment(['id'=>$productFactory->id,'total_results' => 1]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'type_id' => $typeFactory2->id])->assertJsonFragment(['total_results' => 0]);
    }

    /**
     *  Test consulta de productos para subcanal creado y relacionado a producto con tarifa, y relacionado a la categoría por la que vamos a filtrar.
     * Hacemos 2 factories de categoría y relacionamos uno al producto y filtramos por él, comprobamos en salida que nos devuelve el id del producto y total_results 1,
     * intentamos filtrar por el segundo factory de categoría, que no hemos relacionado al producto, y comprobamos que devuelve total_results 0
     */
    public function testOk4()
    {
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $categoryFactory = factory(App\Models\tenant\Category::class)->create([
        ]);

        $categoryFactory2 = factory(App\Models\tenant\Category::class)->create([
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $productCategoryFactory = factory(App\Models\tenant\ProductCategory::class)->create([
            'product_id' => $productFactory->id,
            'category_id' => $categoryFactory->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
            'name' => 'name portugues',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewProductTranslationFactory1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name portugues',
        ]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'category_id' => $categoryFactory->id])->assertJsonFragment(['id'=>$productFactory->id,'name' => $viewProductTranslationFactory1->name,  'total_results' => 1]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'category_id' => $categoryFactory2->id])->assertJsonFragment(['total_results' => 0]);
    }

    /**
     *  Test consulta de productos para subcanal creado y relacionado a producto con tarifa, y relacionado al location_id por el que vamos a filtrar.
     * Hacemos 2 factories de location y relacionamos uno al producto y filtramos por él, comprobamos en salida que nos devuelve el id del producto y total_results 1,
     * intentamos filtrar por el segundo factory de location, que no hemos relacionado al producto, y comprobamos que devuelve total_results 0
     */
    public function testOk5()
    {
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
            'name' => 'name portugues',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $locationFactory2 = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewProductTranslationFactory1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name portugues',
        ]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'location_id' => $locationFactory->id])->assertJsonFragment(['id'=>$productFactory->id,'name' => $viewProductTranslationFactory1->name,'total_results' => 1]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'location_id' => $locationFactory2->id])->assertJsonFragment(['total_results' => 0]);
    }

    /**
     *  Test consulta de productos para subcanal creado y relacionado a producto con tarifa, prueba filtro published
     */
    public function testOk6()
    {
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
            'name' => 'name portugues',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
        ]);

        $viewProductTranslationFactory1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name portugues',
        ]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'published' => 1])->assertJsonFragment(['id'=>$productFactory->id,'total_results' => 1]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id,
            'published' => '0'])->assertJsonFragment(['total_results' => 0]);
    }

    /**
     *  Test consulta de productos para subcanal creado y relacionado a producto que no tiene tarifa asociada, no debe mostrar producto
     *
     */
    public function testOk7()
    {
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory =  factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => Carbon::now()->addDay()->format('Y-m-d'),
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 1,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 2,
            'product_id' => $productFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => 3,
            'product_id' => $productFactory->id,
            'name' => 'name portugues',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
        ]);

        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewproductTranslationFactory1 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewproductTranslationFactory2 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name viewproduct',
        ]);

        $viewproductTranslationFactory3 = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name viewproduct',
        ]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id])->assertExactJson(["data"=>[],"error"=>200,"total_results"=>0]);

        //asignamos una tarifa al producto y ahora sí va a mostrar el producto en la salida
        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $this->json('GET', '/api/v1/viewproduct',['subchannel_id' => $subchannelFactory->id])->assertJsonFragment(["id" => $productFactory->id,'total_results' => 1]);

    }
}