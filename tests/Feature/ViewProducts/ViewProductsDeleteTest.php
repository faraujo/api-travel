<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ViewProductsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar requeridos
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/viewproduct', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["ids" => ["The ids field is required."], "subchannel_id" => ["The subchannel id field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en campo ids y en campo subchannel_id requeridos
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/viewproduct', [
            'ids' => [0 => 'r'],
            'subchannel_id' => 'r',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["ids.0" => ["The ids.0 must be an integer."],
                "subchannel_id" => ["The subchannel id must be an integer."]]]]);
    }

    /**
     * Manda ids inexistentes en campo ids y en campo subchannel id
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/viewproduct', [
            'ids' => [0 => 9999999],
            'subchannel_id' => 9999999,
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found",
                "error_inputs" => [["ids.0" => ["The selected ids.0 is invalid."],
                    "subchannel_id" => ["The selected subchannel id is invalid."]]]]);
    }

    /**
     * Se envían los ids a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
        ]);

        factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_product', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/viewproduct', [
            'ids' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ])
            ->assertExactJson([
                "error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["ids" => ["The ids must be an array."]]]
            ]);

    }

    /**
     * Borra dos productos y comprueba en base de datos que están borrados
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
        ]);

        $viewProductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewProductFactory->id,
        ]);

        $productFactory2 = factory(App\Models\tenant\Product::class)->create([
        ]);

        $viewProductFactory2 = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory2->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewProductFactory2->id,
        ]);

        $this->json('DELETE', '/api/v1/viewproduct', [
            'ids' => [0 => $productFactory->id,
                    1 => $productFactory2->id],
            'subchannel_id' => $subchannelFactory->id,
        ])->assertExactJson(['error' => 200]);

        //comprueba que en la tabla mo_view_product el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_view_product', ['deleted_at' => null, 'id' => $viewProductFactory->id]);
        $this->assertDatabaseMissing('mo_view_product', ['deleted_at' => null, 'id' => $viewProductFactory2->id]);
    }


}