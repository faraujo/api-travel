<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Carbon\Carbon;

class ViewProductsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Envía una petición sin campos requeridos subchannel_id, global
     *
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/viewproduct/1', [
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["global"=>["The global field is required."],"subchannel_id"=>["The subchannel id field is required."]]]]);
    }

    /**
     *Envía una petición con campos requeridos subchannel_id, global en formato incorrecto
     *
     */
    public function testBad2()
    {

        $this->json('GET', '/api/v1/viewproduct/1', [
            'subchannel_id' => 'r',
            'global' => 'r'
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["global"=>["The global field must be true or false."],"subchannel_id"=>["The subchannel id must be an integer."]]]]);

    }

    /**
     *Envía una petición con campo requerido subchannel_id inexistente
     *
     */
    public function testBad3()
    {

        $this->json('GET', '/api/v1/viewproduct/1', [
            'subchannel_id' => '9999999999999999',
            'global' => '0'
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["subchannel_id" => ["The selected subchannel id is invalid."]]]]);

    }

    /**
     *Envía una petición de visualización por canal (existe la relación product_id y subchannel) con parámetro global igual a 0 y muestra el campo order del registro visualización por canal
     * y no el de producto y como viewproduct no tiene traducciones ni files no muestra ninguna traducción ni files (tampoco las que están en producto).
     *
     */
    public function testOk1()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory->id,
        ]);

        $fecha = Carbon::now();

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);

        $ivaFactory = factory(App\Models\tenant\Iva::class)->create([]);
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
            'iva_id' => $ivaFactory->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);


        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' =>  $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        factory(App\Models\tenant\ViewProduct::class)->create([
                'order' => 5,
                'product_id' => $productFactory -> id,
                'subchannel_id' => $subchannelFactory -> id,
        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertExactJson([
            "data"=>[["viewproduct"=>[["accounting_account"=> $productFactory -> accounting_account,"address"=> $productFactory -> address, "billable" => 1,"categories" =>[],"code" => $productFactory -> code,"company_id" => $productFactory -> company_id,"cost_center" => $productFactory -> cost_center,"files"=>[],"height_from" => $productFactory -> heigth_from,"height_to" => $productFactory -> height_to,"id" => $productFactory -> id,"latitude" => $productFactory -> latitude, "iva_id" => $productFactory -> iva_id,"locations" => [["id" => $locationFactory->id,"name" => $locationFactory -> name ]],"longitude" => $productFactory -> longitude,"order"=> 5,"package"=>[],"pax" => 0,"published" => 0, "salable" => 1,"published" => 0, "services"=>[],"subchannel_id" => $subchannelFactory -> id ,"tags" => [],"type_id" => $productFactory -> type_id ,"uom_id" => $productFactory -> uom_id,"weight_from" => $productFactory -> weigth_from,"weight_to" => $productFactory -> weight_to ]]]],"error" => 200 ]);

    }

    /**
     *Envía una petición de visualización por canal (existe la relación product_id y subchannel) con parámetro global igual a 1 y muestra el campo order del producto
     * y muestra traducciones y files de producto.
     *
     */
    public function testOk2()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);

        $fecha = Carbon::now();

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha ->subDay()->format('Y-m-d'),
            'date_end' => $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        $productTranslationFactory1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "name"=> $productTranslationFactory1 -> name,
            "name"=> $fileFactoryTranslation -> name,
            'order' => 4,

        ]);
    }



    /**
     *Envía una petición con visualización por canal (existe la relación product_id y subchannel), con parámetro global = 0 y muestra las traducciones de la visualización.
     *
     */
    public function testOk3()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);

        $fecha = Carbon::now();

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha ->subDay()->format('Y-m-d'),
            'date_end' => $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);


        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);

        $viewproductTranslationFactory = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name español factory',
            'discover_url' => 'discover parque español'
        ]);

        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertJsonFragment([
            "name"=> $viewproductTranslationFactory -> name,
            'discover_url' => $viewproductTranslationFactory -> discover_url,
        ]);

    }


    /**
     *Envía una petición con visualización por canal (existe la relación product_id y subchannel), con parámetro global = 1 y muestra las traducciones del producto (ignora las traducciones de la visualización)
     *
     */
    public function testOk4()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' =>  $channelFactory -> id,
        ]);

        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha ->subDay()->format('Y-m-d'),
            'date_end' => $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        $productTranslationFactory1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);


        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        $productlocationFactory = factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);

        $viewproductTranslationFactory = factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name español factory',
        ]);

        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "name"=> $productTranslationFactory1 -> name,

        ]);

    }


    /**
     *Envía una petición con visualización por canal (existe la relación product_id y subchannel), con parámetro global = 0 y muestra los files de la visualización.
     *
     */
    public function testOk5()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);


        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);

        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha ->subDay()->format('Y-m-d'),
            'date_end' => $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);



        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ViewProductFile::class)->create([
            'view_product_id' =>  $viewproductFactory -> id,
            'file_id' =>  $fileFactory2 -> id,

        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertJsonFragment([
            "id"=>$fileFactory2 -> id,

            ]);

    }

    /**
     *Envía una petición con visualización por canal (existe la relación product_id y subchannel), con parámetro global = 1 y muestra los files del producto.
     *
     */
    public function testOk6()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);


        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha  ->subDay()->format('Y-m-d'),
            'date_end' =>  $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);


        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);



        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

         factory(App\Models\tenant\ViewProductFile::class)->create([
            'view_product_id' =>  $viewproductFactory -> id,
            'file_id' =>  $fileFactory2 -> id,

        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "id"=>$fileFactory -> id,

        ]);

    }


    /**
     *Envía una petición de visualización por canal (NO existe la relación product_id y subchannel) con parámetro global igual a 0,
     * muestra las traducciones y los files del producto .
     *
     */
    public function testOk7()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);

        $fecha = Carbon::now();

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha ->subDay()->format('Y-m-d'),
            'date_end' => $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);


        $productTranslationFactory1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'test url',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);


        /* No introducimos la relación en el view product
        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);
        */


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertJsonFragment([
            "name"=> $productTranslationFactory1 -> name,

            "id"=> $fileFactory -> id,
        ]);
    }

    /**
     *Envía una petición de visualización por canal (NO existe la relación product_id y subchannel) con parámetro global igual a 1,
     * muestra las traducciones y los files del producto .
     *
     */
    public function testOk8()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);

        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha ->subDay()->format('Y-m-d'),
            'date_end' => $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        $productTranslationFactory1 = factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'test url',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);


        /* No introducimos la relación en el view product
        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);
        */


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "name"=> $productTranslationFactory1 -> name,
            "id"=> $fileFactory -> id,
        ]);
    }

    /**
     *Envía una petición con visualización por canal (NO existe la relación product_id y subchannel), con parámetro global = 0 y devuelve datos vacío.
     *
     */
    public function testOk9()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);



        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertExactJson([
            'data' => [], "error"=>200,

        ]);

    }
    /**
     *Envía una petición con visualización por canal (NO existe la relación product_id y subchannel), con parámetro global = 1 y devuelve datos vacío.
     *
     */
    public function testOk10()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory ->is,
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);



        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertExactJson([
            'data' => [], "error"=>200,

        ]);

    }


    /**
     *Envía una petición con visualización por canal (existe la relación product_id y subchannel pero la tarifa de precios es anterior a la fecha actual) con parámetro global = 1 y devuelve datos vacío.
     *
     */
    public function testOk11()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);


        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha  ->subDay()->format('Y-m-d'),
            'date_end' =>  $fecha ->subDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);


        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);



        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ViewProductFile::class)->create([
            'view_product_id' =>  $viewproductFactory -> id,
            'file_id' =>  $fileFactory2 -> id,

        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertExactJson([
            "data" => [],"error" => 200,

        ]);

    }

    /**
     *Envía una petición con visualización por canal (existe la relación product_id y subchannel pero la tarifa de precios es posterior a la fecha actual) con parámetro global = 0 y devuelve datos vacío.
     *
     */
    public function testOk12()
    {

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'channel_id' => $channelFactory -> id,
        ]);


        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha  ->addDay()->format('Y-m-d'),
            'date_end' =>  $fecha ->addDay()->format('Y-m-d'),
        ]);


        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'order' => 4,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);


        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);



        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);


        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);



        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'order' => 5,
            'product_id' => $productFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ViewProductFile::class)->create([
            'view_product_id' =>  $viewproductFactory -> id,
            'file_id' =>  $fileFactory2 -> id,

        ]);


        $this->json('GET', '/api/v1/viewproduct/'.$productFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertExactJson([
            "data" => [],"error" => 200,

        ]);

    }

}