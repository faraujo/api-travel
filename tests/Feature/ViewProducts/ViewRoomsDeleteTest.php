<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ViewRoomsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar requeridos
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/viewproduct/room', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["ids" => ["The ids field is required."], "subchannel_id" => ["The subchannel id field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en campo ids y en campo subchannel_id requeridos
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/viewproduct/room', [
            'ids' => [0 => 'r'],
            'subchannel_id' => 'r',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["ids.0" => ["The ids.0 must be an integer."],
                "subchannel_id" => ["The subchannel id must be an integer."]]]]);
    }

    /**
     * Manda ids inexistentes en campo ids y en campo subchannel id
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/viewproduct/room', [
            'ids' => [0 => 9999999],
            'subchannel_id' => 9999999,
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found",
                "error_inputs" => [["ids.0" => ["The selected ids.0 is invalid."],
                    "subchannel_id" => ["The selected subchannel id is invalid."]]]]);
    }

    /**
     * Se envían los ids a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
        ]);

        factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_room', [
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/viewproduct/room', [
            'ids' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ])
            ->assertExactJson([
                "error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["ids" => ["The ids must be an array."]]]
            ]);

    }

    /**
     * Borra dos room  y comprueba en base de datos que están borrados
     */
    public function testOk1()
    {
        $product = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contract = factory(\App\Models\tenant\ContractModel::class)->create([]
        );
        $price = factory(\App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contract->id,
            'date_start' => \Carbon\Carbon::now()->subDay()->format('Y-m-d'),
            'date_end' => \Carbon\Carbon::now()->addDay()->format('Y-m-d'),
        ]);
        $priceProduct = factory(\App\Models\tenant\PriceProduct::class)->create([
            'product_id' => $product->id,
        ]);
        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $product->id
        ]);


        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contract->id,
        ]);

        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewRoomFactory->id,
        ]);

        $roomFactory2 = factory(App\Models\tenant\HotelRoom::class)->create([
        ]);

        $viewRoomFactory2 = factory(App\Models\tenant\ViewRoom::class)->create([
            'room_id' => $roomFactory2->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewRoomFactory2->id,
        ]);

        $this->json('DELETE', '/api/v1/viewproduct/room', [
            'ids' => [0 => $roomFactory->id,
                1 => $roomFactory2->id],
            'subchannel_id' => $subchannelFactory->id,
        ])->assertExactJson(['error' => 200]);

        //comprueba que en la tabla mo_view_room el registro con id que hemos creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_view_room', ['deleted_at' => null, 'id' => $viewRoomFactory->id]);
        $this->assertDatabaseMissing('mo_view_room', ['deleted_at' => null, 'id' => $viewRoomFactory2->id]);
    }

}