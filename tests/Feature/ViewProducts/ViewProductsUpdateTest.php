<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class ViewProductsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan files y order en formato incorrecto
     */
    public function testBad1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],

            'files' => [0 => 'r'],
            'order' => 'r',

            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[

                    "files.0" => ["The files.0 must be an integer."],
                    "order" => ["The order must be an integer."],

                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan files inexistentes
     */
    public function testBad2()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,

            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],

            'files' => [0 => 99999999],

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[

                    "files.0" => ["The selected files.0 is invalid."],

                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el campo files sin formato array.
     */
    public function testBad3()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'files' => '2',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[

                "files" => ["The files must be an array."],

            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos, con una única traducción, y el files con valor duplicado.
     */
    public function testBad4()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'files' => [0 => $fileFactory->id, 1 => $fileFactory->id],
            ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "files.0" => ["The files.0 field has a duplicate value."],
                "files.1" => ["The files.1 field has a duplicate value."],
            ]]]);
    }

    /**
     * test para comprobar el validador de campos obligatorios
     */
    public function testBad5()
    {
        $this->json('put', '/api/v1/viewproduct', [

            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["product_id"=>["The product id field is required."],
                "published"=>["The published field is required."],"subchannel_id"=>["The subchannel id field is required."]]]]);
    }

    /**
     * test para comprobar el validador de campo published manda string en lugar de 0 o 1
     */
    public function testBad6()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('put', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 'r',
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["published" => ["The published field must be true or false."]]]]);
    }

    /**
     * test para comprobar el validador de campos published manda número mayor de 1
     */
    public function testBad7()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('put', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 5,
            'name' => ['ES' => 'test', 'EN' => 'Nombre modificado888 Inglés', 'PT' => 'Nombre modificado888 portugués'],
            'description' => ['ES' => 'Descripcion1 modificada español', 'EN' => 'Descripcion3 modificada inglés', 'PT' => 'Descripcion3 modificada portugués'],
            'short_description' => ['ES' => 'Short 1descripcion modificada español', 'EN' => 'Short3 descripcion modificada inglés', 'PT' => 'Short 3descripción modificada portugués'],
            'friendly_url' => ['ES' => '1friendly_url3 modificada español', 'EN' => 'friendly_url 3modificada inglés', 'PT' => 'friendly_url3 modificada portugués'],
            'title_seo' => ['ES' => '1title_seo 3modificada español', 'EN' => 'title_seo3  modificada inglés', 'PT' => 'title_seo3 modificada portugués'],
            'description_seo' => ['ES' => '1description_seo3 modificada español', 'EN' => 'description_seo3 modificada inglés', 'PT' => 'description_seo3 modificada portugués'],
            'keywords_seo' => ['ES' => '1keywords_seo 3modificada español', 'EN' => 'keywords_seo3 modificada inglés', 'PT' => 'keywords_seo3 modificada portugués'],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["published" => ["The published field must be true or false."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     *Envía un producto sin traduccion
     */
    public function testBad8()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_product', [
            'id' => $viewproductFactory->id,

        ]);

        $this->assertDatabaseHas('mo_view_product_translation', [
            'view_product_id' => $viewproductFactory->id,
        ]);


        $this->json('PUT', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 0,
        ])
            ->assertExactJson([
                'error' => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["translation" => ["you need at least one translation"]]]
            ]);
    }

    /**
     *Envía un producto con una sola traduccion y comprueba luego que se borran las otras 2 que había al principio
     *
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 0,
            'order' => 0,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([]);

        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name español',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name ingles',
        ]);
        factory(App\Models\tenant\ViewProductTranslation::class)->create([
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name portugues',
        ]);

        $this->assertDatabaseHas('mo_view_product', [
            'id' => $viewproductFactory->id,
            'published' => 0,
            'order' => 0,
        ]);

        $this->assertDatabaseHas('mo_view_product_translation', [
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name español',
        ]);

        $this->assertDatabaseHas('mo_view_product_translation', [
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name ingles',
        ]);

        $this->assertDatabaseHas('mo_view_product_translation', [
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name portugues',
        ]);

        $this->json('PUT', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => 1,
            'order' => 2,
            'name' => ['ES' => 'name modificado Español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url españolMAA'],
            'discover_url' => ['ES' => 'discover_url españolMAA'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],

            'index' => ['ES' => 0],
            'breadcrumb' => ['ES' => 'keywords_seo español'],
            'rel' => ['ES' => 'keywords_seo español'],
            'og_title' => ['ES' => 'keywords_seo español'],
            'og_description' => ['ES' => 'keywords_seo español'],
            'og_image' => ['ES' => $fileFactory->id],
            'twitter_title' => ['ES' => 'keywords_seo español'],
            'twitter_description' => ['ES' => 'keywords_seo español'],
            'twitter_image' => ['ES' => $fileFactory->id],
            'canonical_url' => ['ES' => 'keywords_seo español'],
            'script_head' => ['ES' => 'keywords_seo español'],
            'script_body' => ['ES' => 'keywords_seo español'],
            'script_footer' => ['ES' => 'keywords_seo español']])
            ->assertExactJson([
                'error' => 200
            ]);

        //comprueba cambio de campos published y order
        $this->assertDatabaseHas('mo_view_product', [
            'id' => $viewproductFactory->id,
            'published' => 1,
            'order' => 2,
        ]);

        //comprueba que se ha modificado la traduccion en español
        $this->assertDatabaseHas('mo_view_product_translation', [
            'language_id' => 1,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name modificado Español',
            'deleted_at' => null,
            'discover_url' => 'discover_url españolMAA'
        ]);

        //comprueba que la traduccion que habia en ingles se ha eliminado
        $this->assertDatabaseMissing('mo_view_product_translation', [
            'language_id' => 2,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name ingles',
            'deleted_at' => null,
        ]);

        //comprueba que la traduccion que habia en portugues se ha eliminado
        $this->assertDatabaseMissing('mo_view_product_translation', [
            'language_id' => 3,
            'view_product_id' => $viewproductFactory->id,
            'name' => 'name portugues',
            'deleted_at' => null,
        ]);
    }

    /**
     *Test que comprueba funcionamiento correcto de modificación de asignación productos a files
     *
     * Se crea factory de files, se hace factory de product_file, al mandar relación con diferente file, borra las relaciones anteriores y crea las nuevas
     */
    public function testOk2()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $viewproductFactory = factory(App\Models\tenant\ViewProduct::class)->create([
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_product', [
            'id' => $viewproductFactory->id,
        ]);

        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'url_file' => $fileFactory->url_file,
        ]);

        //se crea factory con relación de product y file
        $productFileFactory = factory(App\Models\tenant\ViewProductFile::class)->create([
            'view_product_id' => $viewproductFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        $this->assertDatabaseHas('mo_view_product_file', [
            'view_product_id' => $viewproductFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        //se crea nueva file para hacer nueva asignación y comprobar que borra la anterior
        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => UploadedFile::fake()->image('nueva.jpg'),
        ]);

        $this->assertDatabaseHas('mo_file', [
            'id' => $fileFactory2->id,
        ]);

        $this->json('PUT', '/api/v1/viewproduct', [
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'published' => '0',
            'name' => ['ES' => 'cualquiera español'],
            'description' => ['ES' => 'Descripcion español'],
            'short_description' => ['ES' => 'Short descripcion español'],
            'friendly_url' => ['ES' => 'friendly_url español'],
            'title_seo' => ['ES' => 'title_seo español'],
            'description_seo' => ['ES' => 'description_seo español'],
            'keywords_seo' => ['ES' => 'keywords_seo español'],
            'files' => [0 => $fileFactory2->id]])
            ->assertExactJson(["error" => 200]);


        $this->assertDatabaseHas('mo_view_product_file', [
            'file_id' => $fileFactory2->id,
        ]);

        $this->assertDatabaseMissing('mo_view_product_file', [
            'file_id' => $fileFactory->id,
            'deleted_at' => null,
        ]);

    }


}