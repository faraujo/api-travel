<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Carbon\Carbon;


class ViewRoomsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Envía una petición sin campos requeridos subchannel_id, global
     *
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/viewproduct/room/1', [
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["global" => ["The global field is required."], "subchannel_id" => ["The subchannel id field is required."]]]]);
    }

    /**
     *Envía una petición con campos requeridos subchannel_id, global en formato incorrecto
     *
     */
    public function testBad2()
    {

        $this->json('GET', '/api/v1/viewproduct/room/1', [
            'subchannel_id' => 'r',
            'global' => 'r'
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["global" => ["The global field must be true or false."], "subchannel_id" => ["The subchannel id must be an integer."]]]]);

    }

    /**
     *Envía una petición con campo requerido subchannel_id inexistente
     *
     */
    public function testBad3()
    {

        $this->json('GET', '/api/v1/viewproduct/room/1', [
            'subchannel_id' => '9999999999999999',
            'global' => '0'
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["subchannel_id" => ["The selected subchannel id is invalid."]]]]);

    }

    /**
     *Envía una petición de visualización por canal (existe la relación room_id y subchannel) con parámetro global igual a 0 y muestra el campo order del registro visualización por canal
     * y no el de habitación y como viewroom no tiene traducciones ni files no muestra ninguna traducción ni files (tampoco las que están en habitación).
     *
     */
    public function testOk1()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);
        $fecha = Carbon::now();
        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);

        $categoryFactory = factory(App\Models\tenant\HotelRoomCategory::class)->create([
        ]);

        factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 1,
            'category_id' => $categoryFactory->id,
            'name' => 'category español',
        ]);
        factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 2,
            'category_id' => $categoryFactory->id,
            'name' => 'category ingles',
        ]);
        factory(App\Models\tenant\HotelRoomCategoryTranslation::class)->create([
            'language_id' => 3,
            'category_id' => $categoryFactory->id,
            'name' => 'category portugues',
        ]);

        $hotelFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
            'category_id' => $categoryFactory->id
        ]);

        $channelFactory = factory(App\Models\tenant\Channel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'channel_id' => $channelFactory->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);


        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $hotelRoomTranslationFactory = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'room_id' => $hotelFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        $fileTranslationFactory = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        $hotelRoomFileFactory = factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' => $hotelFactory->id,
            'file_id' => $fileFactory->id,
        ]);

        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $hotelFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);


        $this->json('GET', '/api/v1/viewproduct/room/' . $hotelFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertExactJson([
            "data" => [["viewroom" => [["hotel_id" => $productFactory->id, "published" => 0, "address" => $hotelFactory->address,'category_id' => $categoryFactory->id, "code" => $hotelFactory->code, "order" => 5, "files" => [], "id" => $hotelFactory->id, "latitude" => $hotelFactory->latitude, "longitude" => $hotelFactory->longitude, "pax" => 0, "subchannel_id" => $subchannelFactory->id, "uom_id" => $hotelFactory->uom_id,]]]], "error" => 200]);

    }

    /**
     *Envía una petición de visualización por canal (existe la relación room_id y subchannel) con parámetro global igual a 1 y muestra el campo order de la habitación
     * y muestra traducciones y files de habitación.
     *
     */
    public function testOk2()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $fecha = Carbon::now();

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);

        $hotelFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
            'order' => 5,
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $hotelRoomTranslationFactory = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'room_id' => $hotelFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        $fileFactoryTranslation = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        $hotelRoomFileFactory = factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' => $hotelFactory->id,
            'file_id' => $fileFactory->id,

        ]);


        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $hotelFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $this->json('GET', '/api/v1/viewproduct/room/' . $hotelFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "name" => $hotelRoomTranslationFactory->name,
            "name" => $fileFactoryTranslation->name,
            'order' => 5,

        ]);

    }

    /**
     *Envía una petición con visualización por canal (existe la relación room_id y subchannel), con parámetro global = 0 y muestra las traducciones de la visualización.
     *
     */
    public function testOk3()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);
        $fecha = Carbon::now();
        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $hotelRoomTranslationFactory = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);


        $viewHotelRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewRoomTranslationFactory = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'view_room_id' => $viewHotelRoomFactory->id,
            'name' => 'name español factory',
        ]);

        $this->json('GET', '/api/v1/viewproduct/room/' . $roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertJsonFragment([
            "name" => $viewRoomTranslationFactory->name,
        ]);

    }

    /**
     *Envía una petición con visualización por canal (existe la relación room_id y subchannel), con parámetro global = 1 y muestra las traducciones de la habitación (ignora las traducciones de la visualización)
     *
     */
    public function testOk4()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        $priceProductFactory = factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        $hotelRoomTranslationFactory1 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);


        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewRoomTranslationFactory = factory(App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => 1,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'name español factory',
        ]);

        $this->json('GET', '/api/v1/viewproduct/room/' . $roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "name" => $hotelRoomTranslationFactory1->name,

        ]);

    }

    /**
     *Envía una petición con visualización por canal (existe la relación room_id y subchannel), con parámetro global = 0 y muestra los files de la visualización.
     *
     */
    public function testOk5()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2,
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);

        $priceProductFactory = factory(\App\Models\tenant\PriceProduct::class)->create([
            'product_id' => $productFactory->id,
            'price_id' => $priceFactory->id,
            'service_id' => null,
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
            'order' => 5
        ]);


        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'name' => 'name español',
            'room_id' => $roomFactory->id,
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' => $roomFactory->id,
            'file_id' => $fileFactory->id,

        ]);

        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ViewRoomFile::class)->create([
            'view_room_id' => $viewRoomFactory->id,
            'file_id' => $fileFactory2->id,

        ]);


        $this->json('GET', '/api/v1/viewproduct/room/' . $roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertJsonFragment([
            "id" => $fileFactory2->id,

        ]);

    }

    /**
     *Envía una petición con visualización por canal (existe la relación room_id y subchannel), con parámetro global = 1 y muestra los files de la habitación.
     *
     */
    public function testOk6()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);

        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);


        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);


        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' => $roomFactory->id,
            'file_id' => $fileFactory->id,

        ]);


        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ViewRoomFile::class)->create([
            'view_room_id' => $viewRoomFactory->id,
            'file_id' => $fileFactory2->id,

        ]);


        $this->json('GET', '/api/v1/viewproduct/room/' . $roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "id" => $fileFactory->id,

        ]);

    }

    /**
     *Envía una petición de visualización por canal (NO existe la relación room_id y subchannel) con parámetro global igual a 0,
     * muestra las traducciones y los files de la habitación .
     *
     */
    public function testOk7()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contractModelFactory = factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory->id,
        ]);

        $fecha = Carbon::now();

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory->id,
            'date_start' => $fecha->subDay()->format('Y-m-d'),
            'date_end' => $fecha->addDay()->format('Y-m-d'),
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
            'order' => 5
        ]);

        $priceProductFactory=factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory->id,
            'product_id' => $productFactory->id,
            'service_id' => null,
        ]);

        $languageFactory = factory(App\Models\tenant\Language::class)->create([]);


        $roomTranslationFactory1 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'room_id' => $roomFactory->id,
            'name' => 'name espanol',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'test url',
        ]);

        $fileTranslationFactory = factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory->id,
            'name' => 'Nombre fichero en espanol',
            'description' => 'Descripcion fichero en espanol',
        ]);

        $hotelRoomFileFactory= factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' => $roomFactory->id,
            'file_id' => $fileFactory->id,

        ]);


        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory->id,
        ]);

        $viewRoomTranslationFactory = factory(\App\Models\tenant\ViewRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'view_room_id' => $viewRoomFactory->id,
            'name' => 'Traduccion en espanol'
    ]);

       $viewRoomFileFactory= factory(App\Models\tenant\ViewRoomFile::class)->create([
            'view_room_id' => $viewRoomFactory->id,
            'file_id' => $fileFactory->id,

        ]);
        $this->json('GET', '/api/v1/viewproduct/room/' . $roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertJsonFragment([
            "name" => $viewRoomTranslationFactory->name,
            "id" => $fileFactory->id,
        ]);
    }
    /**
     *Envía una petición de visualización por canal (NO existe la relación room_id y subchannel) con parámetro global igual a 1,
     * muestra las traducciones y los files de la habitación .
     *
     */
    public function testOk8()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);


        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
         ]);

        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha ->subDay()->format('Y-m-d'),
            'date_end' => $fecha ->addDay()->format('Y-m-d'),
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        $roomTranslationFactory1 = factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);


        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'test url',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' =>  $roomFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);

        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);



        $this->json('GET', '/api/v1/viewproduct/room/'.$roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertJsonFragment([
            "name"=> $roomTranslationFactory1 -> name,
            "id"=> $fileFactory -> id,
        ]);
    }
    /**
     *Envía una petición con visualización por canal (NO existe la relación room_id y subchannel), con parámetro global = 0 y devuelve datos vacío.
     *
     */
    public function testOk9()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
         ]);

        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);


        factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $this->json('GET', '/api/v1/viewproduct/room/'.$roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertExactJson([
            'data' => [], "error"=>200,

        ]);

    }

    /**
     *Envía una petición con visualización por canal (NO existe la relación room_id y subchannel), con parámetro global = 1 y devuelve datos vacío.
     *
     */
    public function testOk10()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
        ]);

        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);

        factory(App\Models\tenant\ProductLocation::class)->create([
            'product_id' => $productFactory->id,
            'location_id' => $locationFactory->id,
        ]);

        factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);

        $this->json('GET', '/api/v1/viewproduct/room/'.$roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertExactJson([
            'data' => [], "error"=>200,

        ]);

    }
    /**
     *Envía una petición con visualización por canal (existe la relación room_id y subchannel pero la tarifa de precios es anterior a la fecha actual) con parámetro global = 1 y devuelve datos vacío.
     *
     */
    public function testOk11()
    {

        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);
        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

         $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
        ]);


        $fecha = Carbon::now();


        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha  ->subDay()->format('Y-m-d'),
            'date_end' =>  $fecha ->subDay()->format('Y-m-d'),
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);


        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\ProductTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'product_id' => $productFactory->id,
            'name' => 'name español',
        ]);



        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ProductFile::class)->create([
            'product_id' =>  $productFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);


        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory->id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ViewRoomFile::class)->create([
            'view_room_id' =>  $viewRoomFactory -> id,
            'file_id' =>  $fileFactory2 -> id,

        ]);


        $this->json('GET', '/api/v1/viewproduct/room/'.$roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 1,
        ])->assertExactJson([
            "data" => [],"error" => 200,

        ]);

    }
    /**
     *Envía una petición con visualización por canal (existe la relación room_id y subchannel pero la tarifa de precios es posterior a la fecha actual) con parámetro global = 0 y devuelve datos vacío.
     *
     */
    public function testOk12()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([
            'type_id' => 2
        ]);

        $contractModelFactory =  factory(App\Models\tenant\ContractModel::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
        ]);

        $fecha = Carbon::now();

        $priceFactory = factory(App\Models\tenant\Price::class)->create([
            'contract_model_id' => $contractModelFactory -> id,
            'date_start' => $fecha  ->addDay()->format('Y-m-d'),
            'date_end' =>  $fecha ->addDay()->format('Y-m-d'),
        ]);


        $roomFactory = factory(App\Models\tenant\HotelRoom::class)->create([
            'hotel_id' => $productFactory->id,
        ]);

        factory(App\Models\tenant\PriceProduct::class)->create([
            'price_id' => $priceFactory -> id,
            'product_id' => $productFactory -> id,
            'service_id' => null,
        ]);


        $languageFactory =  factory(App\Models\tenant\Language::class)->create([]);

        factory(App\Models\tenant\HotelRoomTranslation::class)->create([
            'language_id' => $languageFactory -> id,
            'room_id' => $roomFactory->id,
            'name' => 'name español',
        ]);



        $fileFactory = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\HotelRoomFile::class)->create([
            'room_id' =>  $roomFactory -> id,
            'file_id' =>  $fileFactory -> id,

        ]);




        $viewRoomFactory = factory(App\Models\tenant\ViewRoom::class)->create([
            'order' => 5,
            'room_id' => $roomFactory -> id,
            'subchannel_id' => $subchannelFactory -> id,
        ]);


        $fileFactory2 = factory(App\Models\tenant\File::class)->create([
            'type_id' => 3,
            'url_file' => 'url test',
        ]);

        factory(App\Models\tenant\FileTranslation::class)->create([
            'file_id' => $fileFactory2->id,
            'language_id' => $languageFactory -> id,
            'name' => 'Nombre fichero en español',
            'description' => 'Descripción fichero en español',
        ]);

        factory(App\Models\tenant\ViewRoomFile::class)->create([
            'view_room_id' =>  $viewRoomFactory -> id,
            'file_id' =>  $fileFactory2 -> id,

        ]);

        $this->json('GET', '/api/v1/viewproduct/room/'.$roomFactory->id, [
            'subchannel_id' => $subchannelFactory->id,
            'global' => 0,
        ])->assertExactJson([
            "data" => [],"error" => 200,

        ]);

    }

}