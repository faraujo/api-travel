<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UsersCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda ninguno de los campos requeridos
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/user', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "email" => ["The email field is required."],
                    "password" => ["The password field is required."]
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en todos los campos que se comprueba formato
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'sinarroba',
            'email2' => 'sinarroba',
            'language_id' => 'caracteres',
            'country_id' => 'caracteres',
            'document_type_id' => 'caracteres',
            'number_document' => '444444',
            'sex_id' => 'caracteres',
            'birthdate' => 'caracteres',
            'bloqued_login' => 'caracteres',
            'worker' => 'caracteres',
            'worker_type_id' => 'caracteres'

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["birthdate" => ["The birthdate does not match the format Y-m-d.", "The birthdate is not a valid date.",
                    "The birthdate must be a date before or equal to today."], "bloqued_login" => ["The bloqued login field must be true or false."],
                    "country_id" => ["The country id must be an integer."],
                    "document_type_id" => ["The document type id must be an integer.", "The selected document type id is invalid."],
                    "email" => ["The email must be a valid email address."], "email2" => ["The email2 must be a valid email address."],
                    "language_id" => ["The language id must be an integer.", "The selected language id is invalid."],
                    "sex_id" => ["The selected sex id is invalid.", "The sex id must be an integer."], "worker" => ["The worker field must be true or false."],
                    "worker_type_id" => ["The selected worker type id is invalid.", "The worker type id must be an integer."], "password" => ["The password field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y document_type_id sin número de documento
     */
    public function testBad3()
    {
        $documentFactory = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $this->json('POST', '/api/v1/user', [
            'email' => 'email@yahoo.com',
            'password' => 'passW11!',
            'c_password' => 'passW11!',
            'document_type_id' => $documentFactory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["number_document" => ["The number document field is required when document type id is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y number_document sin document_type_id
     */
    public function testBad4()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email@hotmail.com',
            'password' => 'passW11!',
            'c_password' => 'passW11!',
            'number_document' => '444444',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["document_type_id" => ["The document type id field is required when number document is present."]]]]);
    }

//    /**
//     *Test que comprueba funcionamiento de las validaciones
//     *
//     * Crea factory e intenta crear un registro con el mismo email
//     */
    public function testBad5()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create(
            [
                'name' => 'name test bad5',
                'email' => 'name@gmail.com',
            ]);

        $this->assertDatabaseHas('mo_user', [
            'name' => $userFactory->name,
            'email' => $userFactory->email,
        ]);

        $this->json('POST', '/api/v1/user', [
            'email' => $userFactory->email,
            'password' => 'passW11!',
            'c_password' => 'passW11!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["email" => ["The email has already been taken."]]]]);


        //repite prueba para el campo email2
        $userFactory2 = factory(App\Models\tenant\User::class)->create(
            [
                'name' => 'name test bad5',
                'email' => 'email@gmail.com',
                'email2' => 'email2@gmail.com',
            ]);

        $this->assertDatabaseHas('mo_user', [
            'name' => $userFactory2->name,
            'email' => $userFactory2->email,
            'email2' => $userFactory2->email2,
        ]);

        $this->json('POST', '/api/v1/user', [
            'email' => 'nuevoEmail@gmail.com',
            'email2' => $userFactory2->email2,
            'password' => 'passsW1!',
            'c_password' => 'passsW1!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["email2" => ["The email2 has already been taken."]]]]);
    }

//    /**
//     *Test que comprueba seguridad de contraseña
//     *
//     * Intenta crear contraseña sin mayúsculas, exige al menos 8 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito y un caracter especial
//     * entre estos !$#%
//     */
    public function testBad6()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email6@hotmail.com',
            'password' => 'pass11!!',
            'c_password' => 'pass11!!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

//    /**
//     *Test que comprueba seguridad de contraseña
//     *
//     * Intenta crear contraseña sin minúsculas, exige al menos 6 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito y un caracter especial
//     * entre estos !$#%
//     */
    public function testBad7()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email6@hotmail.com',
            'password' => 'PASSSW1!',
            'c_password' => 'PASSSW1!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

//    /**
//     *Test que comprueba seguridad de contraseña
//     *
//     * Intenta crear contraseña sin números, exige al menos 8 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito y un caracter especial
//     * entre estos !$#%
//     */
    public function testBad8()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email6@hotmail.com',
            'password' => 'passWWW!',
            'c_password' => 'passWWW!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

//    /**
//     *Test que comprueba seguridad de contraseña
//     *
//     * Intenta crear contraseña sin caracteres especiales, exige al menos 8 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito
//     */
    public function testBad9()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email6@hotmail.com',
            'password' => 'passs111',
            'c_password' => 'passs111',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

//    /**
//     *Test para la validación que comprueba los dos campos de contraseña
//     *
//     * Se intenta añadir password y c_password que no son iguales.
//     *
//     */
    public function testBad10()
    {

        $this->json('POST', '/api/v1/user', [
            'email' => 'email6@hotmail.com',
            'password' => 'passsW1#',
            'c_password' => 'passsW1#2',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["c_password" => ["The c password and password must match."]]]]);
    }

//    /**
//     * Intenta crear usuario con password con espacios
//     */
    public function testBad11()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => 'daMar 11!',
            'c_password' => 'daMar 11!',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => ' daMar11!',
            'c_password' => ' daMar11!',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11! ',
            'c_password' => 'daMar11! ',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["You can not enter spaces or accented characters in the password."]]]]);
    }

//    /**
//     * Intenta crear usuario con password con caracteres acentuados
//     */
    public function testBad12()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11!á',
            'c_password' => 'daMar11!á',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11!Á',
            'c_password' => 'daMar11!Á',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["You can not enter spaces or accented characters in the password."]]]]);
    }

//    /**
//     * Intenta crear usuario con campo identification en formato incorrecto
//     */
    public function testBad13()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11!a',
            'c_password' => 'daMar11!a',
            'identification' => 'formato incorrecto'
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["identification" => ["The identification must be an array."]]]]);

    }

//    /**
//     * Intenta crear usuario con campo identification sin campos requeridos
//     */
    public function testBad14()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11!a',
            'c_password' => 'daMar11!a',
            'identification' => ['formato incorrecto']
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "identification.0.identification_id" => ["The identification.0.identification_id field is required."],
                "identification.0.number" => ["The identification.0.number field is required."]]]]);

    }

//    /**
//     * Intenta crear usuario con campo identification con tipo de campos incorrectos
//     */
    public function testBad15()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'email@gmail.com',
            'password' => 'daMar11!a',
            'c_password' => 'daMar11!a',
            'identification' => [0 => [
                "identification_id" => 'formato incorrecto',
                "number" => 'requerido']]
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "identification.0.identification_id" => ["The identification.0.identification_id must be an integer."]]]]);

    }

//    /**
//     * Prueba automatizada campo roles tipo incorrecto
//     */
    public function testBad16()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'test33@test.com',
            'password' => 'passWord1!',
            'c_password' => 'passWord1!',
            'roles' => 'test',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles' => [
                    'The roles must be an array.'
                ]
            ]]]);
    }

//    /**
//     * Prueba automatizada campo roles con tipo de dato role_id incorrecto
//     */
    public function testBad17()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'tesst@test.com',
            'password' => 'passWord1!',
            'c_password' => 'passWord1!',
            'roles' => [
                'role_id' => 'test',
            ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles.role_id' => [
                    'The roles.role_id must be an integer.'
                ]
            ]]]);
    }

//    /**
//     * Prueba automatizada campo roles con rol no existente en base de datos
//     */
    public function testBad18()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'test21@test.com',
            'password' => 'passWord1!',
            'c_password' => 'passWord1!',
            'roles' => [
                'role_id' => 999999999999,
            ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles.role_id' => [
                    'The selected roles.role_id is invalid.'
                ]
            ]]]);
    }

    /**
     * Crea un registro con un state_id inexistente
     */

    public function testBad19()
    {
        $this->json('POST', '/api/v1/user', [
            'email' => 'test21@test.com',
            'password' => 'passWord1!',
            'c_password' => 'passWord1!',
            'state_id' => 999999999999,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => [
                    'The selected state id is invalid.'
                ],
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se crea un estado sin país
     */
    public function testBad20()
    {
        $stateFactory = factory(App\Models\tenant\State::class)->create();
        $this->json('POST', '/api/v1/user', [
            'email' => 'test21@test.com',
            'password' => 'passWord1!',
            'c_password' => 'passWord1!',
            'state_id' => $stateFactory->id,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se crea un estado con un país erróneo
     */

    public function testBad21()
    {
        $stateFactory = factory(App\Models\tenant\State::class)->create([]);
        $countryFactory = factory(App\Models\tenant\Country::class)->create([]);
        $this->json('POST', '/api/v1/user', [
            'email' => 'test21@test.com',
            'password' => 'passWord1!',
            'c_password' => 'passWord1!',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => ['state_id not match with country_id']
            ]]]);
    }

//
//    /**
//     * Crea un registro y comprueba cada campo en base de datos, excepto password que se guarda encriptado
//     *
//     */
    public function testOk1()
    {
        $document = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $worker = factory(\App\Models\tenant\WorkerType::class)->create([]);
        $countryFactory = factory(\App\Models\tenant\Country::cLass)->create([]);
        $stateFactory = factory(\App\Models\tenant\State::cLass)->create([
            'country_id' => $countryFactory->id,
        ]);
        $this->json('POST', '/api/v1/user', [
            'name' => 'nameok1',
            'surname' => 'surname ok1',
            'document_type_id' => $document->id,
            'number_document' => '444444',
            'sex_id' => 1,
            'birthdate' => '2000-12-12',
            'email' => 'email@gmail.com',
            'password' => 'passsW1!',
            'c_password' => 'passsW1!',
            'language_id' => 1,
            'telephone1' => 'telephone ok1',
            'telephone2' => 'tele2 ok1',
            'telephone3' => 'tele3 ok1',
            'address' => 'addres ok1',
            'postal_code' => 'postal_code ok1',
            'city' => 'city ok1',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business ok1',
            'telephone_business' => 'tele business ok1',
            'observations' => 'observations ok1',
            'worker' => 1,
            'worker_type_id' => $worker->id,
        ])
            ->assertExactJson([
                'error' => 200,
            ]);
        //comprueba todos los campos excepto password que se guarda encriptado
        $this->assertDatabaseHas('mo_user', [

            'name' => 'nameok1',
            'surname' => 'surname ok1',
            'document_type_id' => $document->id,
            'number_document' => '444444',
            'sex_id' => 1,
            'birthdate' => '2000-12-12',
            'email' => 'email@gmail.com',
            'language_id' => 1,
            'telephone1' => 'telephone ok1',
            'telephone2' => 'tele2 ok1',
            'telephone3' => 'tele3 ok1',
            'address' => 'addres ok1',
            'postal_code' => 'postal_code ok1',
            'city' => 'city ok1',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business ok1',
            'telephone_business' => 'tele business ok1',
            'observations' => 'observations ok1',
            'worker' => 1,
            'worker_type_id' => $worker->id,
            'bloqued_login' => 0,
            'faults_login' => 0,

        ]);
    }

//    /**
//     * Prueba automatizada campo roles correcto
//     */
    public function testOk2()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $this->json('POST', '/api/v1/user', [
            'email' => 'test4@test.com',
            'password' => 'passWordd1!',
            'c_password' => 'passWordd1!',
            'roles' => [
                'role_id' => $role->id,
            ],
        ])
            ->assertExactJson(['error' => 200]);

    }

    /**
     * Envía un state_id correcto relacionado con su country_id
     */
    public function testOk3()
    {
        $countryFactory = factory(App\Models\tenant\Country::class)->create([]);
        $stateFactory = factory(App\Models\tenant\State::class)->create([
            'country_id' => $countryFactory->id,
        ]);

        $this->json('POST', '/api/v1/user', [
            'email' => 'test21@test.com',
            'password' => 'passWord1!',
            'c_password' => 'passWord1!',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id
        ])
            ->assertExactJson(['error' => 200]);
    }
}