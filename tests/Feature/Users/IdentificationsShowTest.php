<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IdentificationsShowTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test mostrar identificadores idioma incorrecto
     */
    public function testBad1(){
        $this->json('GET','/api/v1/user/identifications', [
            'lang' => 'FDF',
        ])->assertExactJson([
            'error' => 400,
            "error_description"=>"The fields are not the required format",
            "error_inputs"=>[[
                "lang"=>["The lang is not exists"]
            ]]
        ]);
    }

    /**
     * test consulta de identificadores sin parametros
     */
    public function testOk1(){
        $this->json('GET','/api/v1/user/identifications', [])
            ->assertJsonStructure(['error','data']);
    }

    /**
     * test consulta de identificadores con idioma correcto
     */
    public function testOk2(){
        $this->json('GET','/api/v1/user/identifications', [
            'lang' => 'ES',
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

}