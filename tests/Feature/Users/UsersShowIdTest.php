<?php

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UsersShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test pide usuario que no existe en base de datos, devuelve array data vacío
     */
    public function testBad1()
    {

        $this->json('GET', '/api/v1/user/9999999999', [
        ])->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Test comprueba que devuelve los roles que tiene el usuario
     *
     */
    public function testOk1()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create(
            [
                'email' => 'name1@gmail.com',
                'password' => 'passsss',
                'bloqued_to' => Carbon::now()->addMinutes(60)->format('Y-m-d h:i:s'),
            ]);

        $roleFactory = factory(App\Models\tenant\Role::class)->create([]);
        $roleFactory2 = factory(App\Models\tenant\Role::class)->create([]);

        factory(App\Models\tenant\UserRole::class)->create([
            'user_id' => $userFactory->id,
            'role_id' => $roleFactory->id,
        ]);

        factory(App\Models\tenant\UserRole::class)->create([
            'user_id' => $userFactory->id,
            'role_id' => $roleFactory2->id,
        ]);

        $this->json('GET', '/api/v1/user/' . $userFactory->id, [])->assertJsonFragment(["error" => 200, 'id' => $userFactory->id, 'role_id' => $roleFactory->id, 'role_id' => $roleFactory2->id]);

        $roleFactory2->delete();

        $this->json('GET', '/api/v1/user/' . $userFactory->id)
            ->assertExactJson(['error' => 200, 'data' => [
                '0' => [
                    'user' => [
                        '0' => [
                            'id' => $userFactory->id,
                            'name' => $userFactory->name,
                            'surname' => $userFactory->surname,
                            'document_type_id' => $userFactory->document_type_id,
                            'number_document' => $userFactory->number_document,
                            'sex_id' => $userFactory->sex_id,
                            'birthdate' => $userFactory->birthdate,
                            'email' => $userFactory->email,
                            'email2' => $userFactory->email2,
                            'faults_login' => 0,
                            'bloqued_login' => $userFactory->bloqued_login,
                            'bloqued_to' => $userFactory->bloqued_to,
                            'language_id' => $userFactory->language_id,
                            'telephone1' => $userFactory->telephone1,
                            'telephone2' => $userFactory->telephone2,
                            'telephone3' => $userFactory->telephone3,
                            'address' => $userFactory->address,
                            'postal_code' => $userFactory->postal_code,
                            'city' => $userFactory->city,
                            'state_id' => $userFactory->state_id,
                            'country_id' => $userFactory->country_id,
                            'business' => $userFactory->business,
                            'telephone_business' => $userFactory->telephone_business,
                            'worker' => $userFactory->worker,
                            'image_id' => null,
                            'worker_type_id' => $userFactory->worker_type_id,
                            'observations' => $userFactory->observations,
                            'roles' => [
                                '0' => [
                                    'role_id' => $roleFactory->id,
                                ]
                            ],
                            'identification' => [],
                        ]
                    ]
                ]
            ]]);

    }
}