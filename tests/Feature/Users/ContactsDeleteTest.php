<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContactsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda id requerido
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/user/contact', [

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["ids"=>["The ids field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en campo id requerido
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/user/contact', [
            'ids' => ['caracteres'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["ids.0"=>["The ids.0 must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda id de contacto inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/user/contact', [
            'ids' => [99999999],
        ])
            ->assertExactJson(["error"=>404,"error_description"=>"Data not found", "error_inputs" => [
                [
                    "ids.0" => ["The selected ids.0 is invalid."],
                ]
            ]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $contactFactory = factory(App\Models\tenant\Contact::class)->create([
        ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/user/contact', [
            'ids' => $contactFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Crea un registro, comprueba que existe en base de datos, borra y comprueba que ya no existe
     *
     */
    public function testOk1()
    {

        $contactFactory = factory(App\Models\tenant\Contact::class)->create([]);

        //comprueba que el contacto se ha creado
        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/user/contact', [
            'ids' => [$contactFactory->id],
        ])->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que el campo deleted_at del contacto que hemos borrado sea distinto de null
        $this->assertDatabaseMissing('mo_contact', ['deleted_at' => null, 'id' => $contactFactory->id]);
    }

    /**
     * Crea dos registros, comprueba que existen en base de datos, borra y comprueba que ya no existen
     *
     */
    public function testOk2()
    {

        $contactFactory = factory(App\Models\tenant\Contact::class)->create([]);

        $contactFactory2 = factory(App\Models\tenant\Contact::class)->create([]);

        //comprueba que los contactos se han creado
        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory->id,
        ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory2->id,
        ]);

        $this->json('DELETE', '/api/v1/user/contact', [
            'ids' => [$contactFactory->id, $contactFactory2->id],
        ])->assertExactJson([
            'error' => 200,
        ]);

        //comprueba que el campo deleted_at de los contactos que hemos borrado sea distinto de null
        $this->assertDatabaseMissing('mo_contact', ['deleted_at' => null, 'id' => $contactFactory->id]);

        $this->assertDatabaseMissing('mo_contact', ['deleted_at' => null, 'id' => $contactFactory2->id]);
    }


}