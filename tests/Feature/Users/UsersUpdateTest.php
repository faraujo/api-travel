<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UsersUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda ninguno de los campos requeridos
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/user', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "email" => ["The email field is required."],
                "id" => ["The id field is required."],
            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en todos los campos que se comprueba formato
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/user', [
            'id' => 'caracteres',
            'email' => 'sinarroba',
            'language_id' => 'caracteres',
            'country_id' => 'caracteres',
            'document_type_id' => 'caracteres',
            'number_document' => '444444',
            'sex_id' => 'caracteres',
            'birthdate' => 'caracteres',
            'bloqued_login' => 'caracteres',
            'worker' => 'caracteres',
            'worker_type_id' => 'caracteres'

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["birthdate" => ["The birthdate does not match the format Y-m-d.", "The birthdate is not a valid date.",
                    "The birthdate must be a date before or equal to today."], "bloqued_login" => ["The bloqued login field must be true or false."],
                    "country_id" => ["The country id must be an integer."],
                    "document_type_id" => ["The document type id must be an integer.", "The selected document type id is invalid."], "email" => ["The email must be a valid email address."],
                    "id" => ["The id must be an integer."],
                    "language_id" => ["The language id must be an integer.", "The selected language id is invalid."],
                    "sex_id" => ["The selected sex id is invalid.", "The sex id must be an integer."], "worker" => ["The worker field must be true or false."],
                    "worker_type_id" => ["The selected worker type id is invalid.", "The worker type id must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y document_type_id sin número de documento
     */
    public function testBad3()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);
        $documentFactory = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email@yahoo.com',
            'document_type_id' => $documentFactory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["number_document" => ["The number document field is required when document type id is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y number_document sin document_type_id
     */
    public function testBad4()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email@hotmail.com',
            'number_document' => '444444',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["document_type_id" => ["The document type id field is required when number document is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos e intenta modificar un id inexistente en base de datos
     */
    public function testBad5()
    {

        $this->json('PUT', '/api/v1/user', [
            'id' => 999999999,
            'email' => 'email@hotmail.com',
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found"]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Crea dos factorys e intenta poner el email del 1 al 2, el email debe ser único
     */
    public function testBad6()
    {
        $userFactory1 = factory(App\Models\tenant\User::class)->create(
            [
                'name' => 'name test bad6',
                'email' => 'email1@gmail.com',
            ]);

        $userFactory2 = factory(App\Models\tenant\User::class)->create(
            [
                'name' => 'name test bad6',
                'email' => 'email2@gmail.com',
            ]);

        $this->assertDatabaseHas('mo_user', [
            'id' => $userFactory1->id,
            'name' => $userFactory1->name,
            'email' => $userFactory1->email,
        ]);

        $this->assertDatabaseHas('mo_user', [
            'id' => $userFactory2->id,
            'name' => $userFactory2->name,
            'email' => $userFactory2->email,
        ]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory2->id,
            //email del factory 1 se intenta poner en factory 2
            'email' => $userFactory1->email,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["email" => ["The email has already been taken."]]]]);
    }

    /**
     *Test que comprueba seguridad de contraseña
     *
     * Intenta crear contraseña sin mayúsculas, exige al menos 7 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito y un caracter especial
     * entre estos !$#%
     */
    public function testBad7()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email6@hotmail.com',
            'password' => 'pass111!',
            'c_password' => 'pass111!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

    /**
     *Test que comprueba seguridad de contraseña
     *
     * Intenta crear contraseña sin minúsculas, exige al menos 6 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito y un caracter especial
     * entre estos !$#%
     */
    public function testBad8()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email6@hotmail.com',
            'password' => 'PASSW11!',
            'c_password' => 'PASSW11!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

    /**
     *Test que comprueba seguridad de contraseña
     *
     * Intenta crear contraseña sin números, exige al menos 7 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito y un caracter especial
     * entre estos !$#%
     */
    public function testBad9()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email6@hotmail.com',
            'password' => 'passWWW!',
            'c_password' => 'passWWW!',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

    /**
     *Test que comprueba seguridad de contraseña
     *
     * Intenta crear contraseña sin caracteres especiales, exige al menos 7 caracteres, al menos 1 de ellos minúscula, al menos 1 mayúscula, al menos 1 dígito
     */
    public function testBad10()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email6@hotmail.com',
            'password' => 'passss11',
            'c_password' => 'passss11',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["password" => ["The password format is invalid."]]]]);
    }

    /**
     *Test para la validación que comprueba los dos campos de contraseña
     *
     * Se intenta añadir password y c_password que no son iguales.
     *
     */
    public function testBad11()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email6@hotmail.com',
            'password' => 'passsW1#',
            'c_password' => 'passsW1#2',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["c_password" => ["The c password and password must match."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía la confirmación de contraseña
     */
    public function testBad12()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email6@hotmail.com',
            'password' => 'passsW1#',
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [["c_password" => ["The c password field is required when password is present."]]]]);
    }

    /**
     * Introduce 6 password en el histórico de password e intenta modificar el password actual y poner uno de los 4 últimos
     */
    public function testBad13()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        factory(App\Models\tenant\PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura2!'),
        ]);

        factory(App\Models\tenant\PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura3!'),
        ]);

        factory(App\Models\tenant\PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura4!'),
        ]);

        factory(App\Models\tenant\PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura5!'),
        ]);

        factory(App\Models\tenant\PasswordHistory::class)->create([
            'user_id' => $userFactory->id,
            'password' => bcrypt('passSegura6!'),
        ]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'prueba@viavox.com',
            'password' => 'passSegura6!',
            'c_password' => 'passSegura6!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'prueba@viavox.com',
            'password' => 'passSegura5!',
            'c_password' => 'passSegura5!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'prueba@viavox.com',
            'password' => 'passSegura4!',
            'c_password' => 'passSegura4!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'prueba@viavox.com',
            'password' => 'passSegura3!',
            'c_password' => 'passSegura3!',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['password' => ['The password entered must be different from the last four.']]]]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'prueba@viavox.com',
            'password' => 'passSegura2!',
            'c_password' => 'passSegura2!',
        ])
            ->assertExactJson(['error' => 200]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'prueba@viavox.com',
            'password' => 'passSegura1!',
            'c_password' => 'passSegura1!',
        ])
            ->assertExactJson(['error' => 200]);

    }

    /**
     * Intenta crear usuario con password con espacios
     */
    public function testBad14()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar 11!',
            'c_password' => 'daMar 11!',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => ' daMar11!',
            'c_password' => ' daMar11!',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11! ',
            'c_password' => 'daMar11! ',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);
    }

    /**
     * Intenta modificar usuario con password con caracteres acentuados
     */
    public function testBad15()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!á',
            'c_password' => 'daMar11!á',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!Á',
            'c_password' => 'daMar11!Á',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["password"=>["You can not enter spaces or accented characters in the password."]]]]);
    }

    /**
     * Intenta modificar usuario con tipo de campo roles incorrecto
     */
    public function testBad16()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'roles' => 'test'
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["roles"=>["The roles must be an array."]]]]);

    }

    /**
     * Intenta modificar usuario con tipo de campo role_id
     */
    public function testBad17()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'roles' => [
                'role_id' => 'test'
            ],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["roles.role_id"=>["The roles.role_id must be an integer."]]]]);

    }

    /**
     * Intenta modificar un usuario con un state_id inexistente
     */

    public function testBad18()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);
        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'state_id'=>999999999
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => [
                    'The selected state id is invalid.'
                ],
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se intenta modificar un usuario especificando state_id pero no country_id
     */
    public function testBad19()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);
        $stateFactory=factory(App\Models\tenant\State::cLass)->create([]);
        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'state_id'=>$stateFactory->id
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se intenta modificar un usuario con state_id no correspondiente al country_id
     */

    public function testBad20()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);
        $stateFactory=factory(App\Models\tenant\State::cLass)->create([]);
        $countryFactory=factory(App\Models\tenant\Country::cLass)->create([]);
        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'state_id'=>$stateFactory->id,
            'country_id' =>$countryFactory->id,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => ['state_id not match with country_id']
            ]]]);
    }

    /**
     * Crea un registro y comprueba cada campo en base de datos, excepto password que se guarda encriptado. Lo modifica y comprueba en base de datos
     *
     */
    public function testOk1()
    {
        $countryFactory = factory(App\Models\tenant\Country::cLass)->create([]);

        $stateFactory = factory(App\Models\tenant\State::cLass)->create([
            'country_id' => $countryFactory->id,
        ]);

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'faults_login' => 5
        ]);

        //comprueba todos los campos excepto password que se guarda encriptado
        $this->assertDatabaseHas('mo_user', [

            'name' => $userFactory->name,
            'surname' => $userFactory->surname,
            'document_type_id' => $userFactory->document_type_id,
            'number_document' => $userFactory->number_document,
            'sex_id' => $userFactory->sex_id,
            'birthdate' => $userFactory->birthdate,
            'email' => $userFactory->email,
            'language_id' => $userFactory->language_id,
            'telephone1' => $userFactory->telephone1,
            'telephone2' => $userFactory->telephone2,
            'telephone3' => $userFactory->telephone3,
            'address' => $userFactory->address,
            'postal_code' => $userFactory->postal_code,
            'city' => $userFactory->city,
            'state_id' => $userFactory->state_id,
            'country_id' => $userFactory->country_id,
            'business' => $userFactory->business,
            'telephone_business' => $userFactory->telephone_business,
            'observations' => $userFactory->observations,
            'bloqued_login' => $userFactory->bloqued_login,
            'worker' => $userFactory->worker,
            'worker_type_id' => $userFactory->worker_type_id,
            'faults_login' => $userFactory->faults_login,

        ]);

        $documentFactory = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $workerFactory = factory(\App\Models\tenant\WorkerType::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'name' => 'name modificado',
            'surname' => 'surname modificado',
            'password' => 'passsW1!',
            'c_password' => 'passsW1!',
            'document_type_id' => $documentFactory->id,
            'number_document' => '55555',
            'sex_id' => 2,
            'birthdate' => '2000-01-01',
            'email' => 'emailModificado@gmail.com',
            'language_id' => 2,
            'telephone1' => 'telephone modificado',
            'telephone2' => 'tele2 modificado',
            'telephone3' => 'tele3 modificado',
            'address' => 'addres modificado',
            'postal_code' => 'postal_code modificado',
            'city' => 'city modificado',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business modificado',
            'telephone_business' => 'tele business modificado',
            'observations' => 'observations modificado',
            'worker' => 1,
            'worker_type_id' => $workerFactory->id,
            'bloqued_login' => 0,

        ])
            ->assertExactJson(["error" => 200]);

        //comprueba que se han realizado las modificaciones en base de datos
        $this->assertDatabaseHas('mo_user', [

            'id' => $userFactory->id,
            'name' => 'name modificado',
            'surname' => 'surname modificado',
            'document_type_id' => $documentFactory->id,
            'number_document' => '55555',
            'sex_id' => 2,
            'birthdate' => '2000-01-01',
            'email' => 'emailModificado@gmail.com',
            'language_id' => 2,
            'telephone1' => 'telephone modificado',
            'telephone2' => 'tele2 modificado',
            'telephone3' => 'tele3 modificado',
            'address' => 'addres modificado',
            'postal_code' => 'postal_code modificado',
            'city' => 'city modificado',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business modificado',
            'telephone_business' => 'tele business modificado',
            'observations' => 'observations modificado',
            'worker' => 1,
            'worker_type_id' => $workerFactory->id,
            'bloqued_login' => 0,

        ]);
    }

    /**
     * Comprueba que la validación de email unique no salta si se envía el mismo email que ya tiene el usuario que se va a modificar,
     * tanto para campo email como campo email2 y permite repetir email
     *
     */
    public function testOk2()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'email@gmail.com',
            'email2' => 'email2@gmail.com',
        ]);

        $this->json('PUT', '/api/v1/user', [

            'id' => $userFactory->id,
            'email' => 'email@gmail.com',
            'email2' => 'email2@gmail.com',
            'password' => 'passsW1!',
            'c_password' => 'passsW1!',
        ])
            ->assertExactJson(["error" => 200]);

        //repite prueba repitiendo email en ambos campos
        $userFactory2 = factory(App\Models\tenant\User::class)->create([
            'email' => 'emailUser2@gmail.com',
            'email2' => 'emailUser2@gmail.com',
        ]);

        $this->json('PUT', '/api/v1/user', [

            'id' => $userFactory2->id,
            'email' => 'emailUser2@gmail.com',
            'email2' => 'emailUser2@gmail.com',
            'password' => 'passsW1!',
            'c_password' => 'passsW1!',
        ])
            ->assertExactJson(["error" => 200]);
    }




    /**
     * Actualizacion de usuario con identificador
     */
    public function testOk3()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'email@gmail.com',
            'email2' => 'email2@gmail.com',
        ]);
        $identification = factory(\App\Models\tenant\Identification::class)->create([]);
        $identificationDefault = factory(\App\Models\tenant\Identification::class)->create([]);
        $identificationUser = factory(\App\Models\tenant\UserIdentification::class)->create([
            'user_id' => $userFactory->id,
            'identification_id' => $identificationDefault->id,
        ]);
        $useridentification = factory(\App\Models\tenant\UserIdentification::class)->create([

        ]);
        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => 'email@gmail.com',
            'identification' => [
                0 => [
                    'identification_id' => $identification->id,
                    'number' => 'XXX-XXX-XXX',
                ]
            ]

        ])
            ->assertExactJson([
                'error' => 200,
            ]);
        //comprueba todos los campos excepto password que se guarda encriptado
        $this->assertDatabaseHas('mo_user', [
            'email' => 'email@gmail.com',
        ]);
        $this->assertDatabaseHas('mo_user_identification', [
            'user_id' => $userFactory->id,
            'identification_id' => $identification->id,
            'deleted_at' => NULL,
        ]);

        $this->assertDatabaseMissing('mo_user_identification', [
            'user_id' => $userFactory->id,
            'identification_id' => $identificationDefault->id,
            'deleted_at' => NULL,
        ]);
    }

    /**
     * Intenta modificar usuario con tipo de campo role_id
     */
    public function testOk4()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'roles' => [
                'role_id' => $role->id,
            ],
        ])
            ->assertExactJson(["error"=>200]);

        $this->assertDatabaseHas('mo_user_role',[
            'user_id' => $userFactory->id,
            'role_id' => $role->id,
            'deleted_at' => null
        ]);

    }

    /**
     * Intenta modificar usuario comprueba el borrado de roles asignados al usuario e insercion de role nuevo
*/
    public function testOk5()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $userRole = factory(\App\Models\tenant\UserRole::class)->create(['user_id' => $userFactory->id,'role_id' => $role->id]);

        $role2 = factory(\App\Models\tenant\Role::class)->create([]);

        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'roles' => [
                'role_id' => $role2->id,
            ],
        ])
            ->assertExactJson(["error"=>200]);

        $this->assertDatabaseHas('mo_user_role',[
            'user_id' => $userFactory->id,
            'role_id' => $role2->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_user_role',[
            'user_id' => $userFactory->id,
            'role_id' => $role->id,
            'deleted_at' => null
        ]);

    }
    /*Se intenta modificar un usuario pasando los parámetros correctos de state_id y country_id*/
    public function testOk6()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'email' => 'prueba@viavox.com',
            'password' => bcrypt('passSegura1!'),
        ]);

        $countryFactory=factory(App\Models\tenant\Country::cLass)->create([]);
        $stateFactory=factory(App\Models\tenant\State::cLass)->create([
            'country_id'=>$countryFactory->id,
        ]);
        $this->json('PUT', '/api/v1/user', [
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'password' => 'daMar11!',
            'c_password' => 'daMar11!',
            'state_id'=>$stateFactory->id,
            'country_id' =>$countryFactory->id,
        ])
            ->assertExactJson(["error" => 200]);
    }




    /*Se intenta modificar un usuario con buscador pasando los parámetros correctos de state_id y country_id*/
    public function testOk8()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([
            'password' => bcrypt('test'),
        ]);

        $countryFactory = factory(App\Models\tenant\Country::class)->create([]);
        $stateFactory = factory(App\Models\tenant\State::cLass)->create([
            'country_id'=>$countryFactory->id,
        ]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $userFactory->id,
            'token' => 'asdasdsadsad',
        ]);

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([
        ]);

        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);

        $this->json('PUT', '/api/v1/user/profile/search', [
            'subchannel_id' => $subchannelFactory->id,
            'id' => $userFactory->id,
            'email' => $userFactory->email,
            'state_id'=>$stateFactory->id,
            'country_id' =>$countryFactory->id,
            'actual_password' => 'test'
        ], [
            'SubchannelToken' => $subchannelFactory->authorization_token,
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson(["error" => 200]);
    }

}