<?php

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContactsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test pide contacto que no existe en base de datos, devuelve array data vacío
     */
    public function testBad1()
    {

        $this->json('GET', '/api/v1/user/contact/9999999999', [
        ])->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Test comprueba que devuelve los roles que tiene el contacto
     *
     */
    public function testOk1()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create(
            [
                'email' => 'name1@gmail.com',
                'bloqued_to' => Carbon::now()->addMinutes(60)->format('Y-m-d h:i:s'),
            ]);


        $this->json('GET', '/api/v1/user/contact/' . $contactFactory->id, [])->assertJsonFragment(["error" => 200, 'id' => $contactFactory->id]);


        $this->json('GET', '/api/v1/user/contact/' . $contactFactory->id)
            ->assertExactJson(['error' => 200, 'data' => [
                '0' => [
                    'contact' => [
                        '0' => [
                            'id' => $contactFactory->id,
                            'name' => $contactFactory->name,
                            'surname' => $contactFactory->surname,
                            'document_type_id' => $contactFactory->document_type_id,
                            'number_document' => $contactFactory->number_document,
                            'sex_id' => $contactFactory->sex_id,
                            'birthdate' => $contactFactory->birthdate,
                            'email' => $contactFactory->email,
                            'email2' => $contactFactory->email2,
                            'faults_login' => 0,
                            'bloqued_login' => $contactFactory->bloqued_login,
                            'bloqued_to' => $contactFactory->bloqued_to,
                            'language_id' => $contactFactory->language_id,
                            'telephone1' => $contactFactory->telephone1,
                            'telephone2' => $contactFactory->telephone2,
                            'telephone3' => $contactFactory->telephone3,
                            'address' => $contactFactory->address,
                            'postal_code' => $contactFactory->postal_code,
                            'city' => $contactFactory->city,
                            'state_id' => $contactFactory->state_id,
                            'country_id' => $contactFactory->country_id,
                            'business' => $contactFactory->business,
                            'telephone_business' => $contactFactory->telephone_business,
                            'worker' => $contactFactory->worker,
                            'image_id' => null,
                            'worker_type_id' => $contactFactory->worker_type_id,
                            'observations' => $contactFactory->observations,
                        ]
                    ]
                ]
            ]]);

    }
}