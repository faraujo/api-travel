<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContactsCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda ninguno de los campos requeridos
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/user/contact', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "email" => ["The email field is required."],
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en todos los campos que se comprueba formato
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'sinarroba',
            'email2' => 'sinarroba',
            'language_id' => 'caracteres',
            'country_id' => 'caracteres',
            'document_type_id' => 'caracteres',
            'number_document' => '444444',
            'sex_id' => 'caracteres',
            'birthdate' => 'caracteres',
            'bloqued_login' => 'caracteres',
            'worker' => 'caracteres',
            'worker_type_id' => 'caracteres'

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["birthdate" => ["The birthdate does not match the format Y-m-d.", "The birthdate is not a valid date.",
                    "The birthdate must be a date before or equal to today."], "bloqued_login" => ["The bloqued login field must be true or false."],
                    "country_id" => ["The country id must be an integer."],
                    "document_type_id" => ["The document type id must be an integer.", "The selected document type id is invalid."],
                    "email" => ["The email must be a valid email address."], "email2" => ["The email2 must be a valid email address."],
                    "language_id" => ["The language id must be an integer.", "The selected language id is invalid."],
                    "sex_id" => ["The selected sex id is invalid.", "The sex id must be an integer."], "worker" => ["The worker field must be true or false."],
                    "worker_type_id" => ["The selected worker type id is invalid.", "The worker type id must be an integer."],]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y document_type_id sin número de documento
     */
    public function testBad3()
    {
        $documentFactory = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'email@yahoo.com',
            'document_type_id' => $documentFactory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["number_document" => ["The number document field is required when document type id is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y number_document sin document_type_id
     */
    public function testBad4()
    {
        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'email@hotmail.com',
            'number_document' => '444444',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["document_type_id" => ["The document type id field is required when number document is present."]]]]);
    }

//    /**
//     *Test que comprueba funcionamiento de las validaciones
//     *
//     * Crea factory e intenta crear un registro con el mismo email
//     */
    public function testBad5()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'name test bad5',
                'email' => 'name@gmail.com',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'name' => $contactFactory->name,
            'email' => $contactFactory->email,
        ]);

        $this->json('POST', '/api/v1/user/contact', [
            'email' => $contactFactory->email,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["email" => ["The email has already been taken."]]]]);


        //repite prueba para el campo email2
        $contactFactory2 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'name test bad5',
                'email' => 'email@gmail.com',
                'email2' => 'email2@gmail.com',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'name' => $contactFactory2->name,
            'email' => $contactFactory2->email,
            'email2' => $contactFactory2->email2,
        ]);

        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'nuevoEmail@gmail.com',
            'email2' => $contactFactory2->email2,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["email2" => ["The email2 has already been taken."]]]]);
    }

    /**
     * Crea un registro con un state_id inexistente
     */

    public function testBad19()
    {
        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'test21@test.com',
            'state_id' => 999999999999,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => [
                    'The selected state id is invalid.'
                ],
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se crea un estado sin país
     */
    public function testBad20()
    {
        $stateFactory = factory(App\Models\tenant\State::class)->create();
        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'test21@test.com',
            'state_id' => $stateFactory->id,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se crea un estado con un país erróneo
     */

    public function testBad21()
    {
        $stateFactory = factory(App\Models\tenant\State::class)->create([]);
        $countryFactory = factory(App\Models\tenant\Country::class)->create([]);
        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'test21@test.com',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => ['state_id not match with country_id']
            ]]]);
    }

//
//    /**
//     * Crea un registro y comprueba cada campo en base de datos, excepto password que se guarda encriptado
//     *
//     */
    public function testOk1()
    {
        $document = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $worker = factory(\App\Models\tenant\WorkerType::class)->create([]);
        $countryFactory = factory(\App\Models\tenant\Country::cLass)->create([]);
        $stateFactory = factory(\App\Models\tenant\State::cLass)->create([
            'country_id' => $countryFactory->id,
        ]);
        $this->json('POST', '/api/v1/user/contact', [
            'name' => 'nameok1',
            'surname' => 'surname ok1',
            'document_type_id' => $document->id,
            'number_document' => '444444',
            'sex_id' => 1,
            'birthdate' => '2000-12-12',
            'email' => 'email@gmail.com',
            'language_id' => 1,
            'telephone1' => 'telephone ok1',
            'telephone2' => 'tele2 ok1',
            'telephone3' => 'tele3 ok1',
            'address' => 'addres ok1',
            'postal_code' => 'postal_code ok1',
            'city' => 'city ok1',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business ok1',
            'telephone_business' => 'tele business ok1',
            'observations' => 'observations ok1',
            'worker' => 1,
            'worker_type_id' => $worker->id,
        ])
            ->assertExactJson([
                'error' => 200,
            ]);
        //comprueba todos los campos excepto password que se guarda encriptado
        $this->assertDatabaseHas('mo_contact', [

            'name' => 'nameok1',
            'surname' => 'surname ok1',
            'document_type_id' => $document->id,
            'number_document' => '444444',
            'sex_id' => 1,
            'birthdate' => '2000-12-12',
            'email' => 'email@gmail.com',
            'language_id' => 1,
            'telephone1' => 'telephone ok1',
            'telephone2' => 'tele2 ok1',
            'telephone3' => 'tele3 ok1',
            'address' => 'addres ok1',
            'postal_code' => 'postal_code ok1',
            'city' => 'city ok1',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business ok1',
            'telephone_business' => 'tele business ok1',
            'observations' => 'observations ok1',
            'worker' => 1,
            'worker_type_id' => $worker->id,
            'bloqued_login' => 0,
            'faults_login' => 0,

        ]);
    }

    /**
     * Envía un state_id correcto relacionado con su country_id
     */
    public function testOk3()
    {
        $countryFactory = factory(App\Models\tenant\Country::class)->create([]);
        $stateFactory = factory(App\Models\tenant\State::class)->create([
            'country_id' => $countryFactory->id,
        ]);

        $this->json('POST', '/api/v1/user/contact', [
            'email' => 'test21@test.com',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id
        ])
            ->assertExactJson(['error' => 200]);
    }
}