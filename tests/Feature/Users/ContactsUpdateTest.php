<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContactsUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda ninguno de los campos requeridos
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/user/contact', [

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "email" => ["The email field is required."],
                "id" => ["The id field is required."],
            ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en todos los campos que se comprueba formato
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/user/contact', [
            'id' => 'caracteres',
            'email' => 'sinarroba',
            'language_id' => 'caracteres',
            'country_id' => 'caracteres',
            'document_type_id' => 'caracteres',
            'number_document' => '444444',
            'sex_id' => 'caracteres',
            'birthdate' => 'caracteres',
            'bloqued_login' => 'caracteres',
            'worker' => 'caracteres',
            'worker_type_id' => 'caracteres'

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["birthdate" => ["The birthdate does not match the format Y-m-d.", "The birthdate is not a valid date.",
                    "The birthdate must be a date before or equal to today."], "bloqued_login" => ["The bloqued login field must be true or false."],
                    "country_id" => ["The country id must be an integer."],
                    "document_type_id" => ["The document type id must be an integer.", "The selected document type id is invalid."], "email" => ["The email must be a valid email address."],
                    "id" => ["The id must be an integer."],
                    "language_id" => ["The language id must be an integer.", "The selected language id is invalid."],
                    "sex_id" => ["The selected sex id is invalid.", "The sex id must be an integer."], "worker" => ["The worker field must be true or false."],
                    "worker_type_id" => ["The selected worker type id is invalid.", "The worker type id must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y document_type_id sin número de documento
     */
    public function testBad3()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create([]);
        $documentFactory = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory->id,
            'email' => 'email@yahoo.com',
            'document_type_id' => $documentFactory->id,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["number_document" => ["The number document field is required when document type id is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos y number_document sin document_type_id
     */
    public function testBad4()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create([]);

        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory->id,
            'email' => 'email@hotmail.com',
            'number_document' => '444444',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["document_type_id" => ["The document type id field is required when number document is present."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda requeridos e intenta modificar un id inexistente en base de datos
     */
    public function testBad5()
    {

        $this->json('PUT', '/api/v1/user/contact', [
            'id' => 999999999,
            'email' => 'email@hotmail.com',
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found"]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Crea dos factorys e intenta poner el email del 1 al 2, el email debe ser único
     */
    public function testBad6()
    {
        $contactFactory1 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'name test bad6',
                'email' => 'email1@gmail.com',
            ]);

        $contactFactory2 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'name test bad6',
                'email' => 'email2@gmail.com',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory1->id,
            'name' => $contactFactory1->name,
            'email' => $contactFactory1->email,
        ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory2->id,
            'name' => $contactFactory2->name,
            'email' => $contactFactory2->email,
        ]);

        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory2->id,
            //email del factory 1 se intenta poner en factory 2
            'email' => $contactFactory1->email,

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["email" => ["The email has already been taken."]]]]);
    }

    /**
     * Intenta modificar un contacto con un state_id inexistente
     */

    public function testBad18()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create([
            'email' => 'prueba@viavox.com',
        ]);
        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory->id,
            'email' => $contactFactory->email,
            'state_id'=>999999999
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => [
                    'The selected state id is invalid.'
                ],
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se intenta modificar un contacto especificando state_id pero no country_id
     */
    public function testBad19()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create([
            'email' => 'prueba@viavox.com',
        ]);
        $stateFactory=factory(App\Models\tenant\State::cLass)->create([]);
        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory->id,
            'email' => $contactFactory->email,
            'state_id'=>$stateFactory->id
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'country_id' => ['The country id field is required when state id is present.']
            ]]]);
    }

    /**
     * Se intenta modificar un contacto con state_id no correspondiente al country_id
     */

    public function testBad20()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create([
            'email' => 'prueba@viavox.com',
        ]);
        $stateFactory=factory(App\Models\tenant\State::cLass)->create([]);
        $countryFactory=factory(App\Models\tenant\Country::cLass)->create([]);
        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory->id,
            'email' => $contactFactory->email,
            'state_id'=>$stateFactory->id,
            'country_id' =>$countryFactory->id,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'state_id' => ['state_id not match with country_id']
            ]]]);
    }

    /**
     * Crea un registro y comprueba cada campo en base de datos, excepto password que se guarda encriptado. Lo modifica y comprueba en base de datos
     *
     */
    public function testOk1()
    {
        $countryFactory = factory(App\Models\tenant\Country::cLass)->create([]);

        $stateFactory = factory(App\Models\tenant\State::cLass)->create([
            'country_id' => $countryFactory->id,
        ]);

        $contactFactory = factory(App\Models\tenant\Contact::class)->create([
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'faults_login' => 5
        ]);

        //comprueba todos los campos excepto password que se guarda encriptado
        $this->assertDatabaseHas('mo_contact', [

            'name' => $contactFactory->name,
            'surname' => $contactFactory->surname,
            'document_type_id' => $contactFactory->document_type_id,
            'number_document' => $contactFactory->number_document,
            'sex_id' => $contactFactory->sex_id,
            'birthdate' => $contactFactory->birthdate,
            'email' => $contactFactory->email,
            'language_id' => $contactFactory->language_id,
            'telephone1' => $contactFactory->telephone1,
            'telephone2' => $contactFactory->telephone2,
            'telephone3' => $contactFactory->telephone3,
            'address' => $contactFactory->address,
            'postal_code' => $contactFactory->postal_code,
            'city' => $contactFactory->city,
            'state_id' => $contactFactory->state_id,
            'country_id' => $contactFactory->country_id,
            'business' => $contactFactory->business,
            'telephone_business' => $contactFactory->telephone_business,
            'observations' => $contactFactory->observations,
            'bloqued_login' => $contactFactory->bloqued_login,
            'worker' => $contactFactory->worker,
            'worker_type_id' => $contactFactory->worker_type_id,
            'faults_login' => $contactFactory->faults_login,

        ]);

        $documentFactory = factory(\App\Models\tenant\DocumentType::class)->create([]);
        $workerFactory = factory(\App\Models\tenant\WorkerType::class)->create([]);

        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory->id,
            'name' => 'name modificado',
            'surname' => 'surname modificado',
            'document_type_id' => $documentFactory->id,
            'number_document' => '55555',
            'sex_id' => 2,
            'birthdate' => '2000-01-01',
            'email' => 'emailModificado@gmail.com',
            'language_id' => 2,
            'telephone1' => 'telephone modificado',
            'telephone2' => 'tele2 modificado',
            'telephone3' => 'tele3 modificado',
            'address' => 'addres modificado',
            'postal_code' => 'postal_code modificado',
            'city' => 'city modificado',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business modificado',
            'telephone_business' => 'tele business modificado',
            'observations' => 'observations modificado',
            'worker' => 1,
            'worker_type_id' => $workerFactory->id,
            'bloqued_login' => 0,

        ])
            ->assertExactJson(["error" => 200]);

        //comprueba que se han realizado las modificaciones en base de datos
        $this->assertDatabaseHas('mo_contact', [

            'id' => $contactFactory->id,
            'name' => 'name modificado',
            'surname' => 'surname modificado',
            'document_type_id' => $documentFactory->id,
            'number_document' => '55555',
            'sex_id' => 2,
            'birthdate' => '2000-01-01',
            'email' => 'emailModificado@gmail.com',
            'language_id' => 2,
            'telephone1' => 'telephone modificado',
            'telephone2' => 'tele2 modificado',
            'telephone3' => 'tele3 modificado',
            'address' => 'addres modificado',
            'postal_code' => 'postal_code modificado',
            'city' => 'city modificado',
            'state_id' => $stateFactory->id,
            'country_id' => $countryFactory->id,
            'business' => 'business modificado',
            'telephone_business' => 'tele business modificado',
            'observations' => 'observations modificado',
            'worker' => 1,
            'worker_type_id' => $workerFactory->id,
            'bloqued_login' => 0,

        ]);
    }

    /**
     * Comprueba que la validación de email unique no salta si se envía el mismo email que ya tiene el contacto que se va a modificar,
     * tanto para campo email como campo email2 y permite repetir email
     *
     */
    public function testOk2()
    {

        $contactFactory = factory(App\Models\tenant\Contact::class)->create([
            'email' => 'email@gmail.com',
            'email2' => 'email2@gmail.com',
        ]);

        $this->json('PUT', '/api/v1/user/contact', [

            'id' => $contactFactory->id,
            'email' => 'email@gmail.com',
            'email2' => 'email2@gmail.com',
        ])
            ->assertExactJson(["error" => 200]);

        //repite prueba repitiendo email en ambos campos
        $contactFactory2 = factory(App\Models\tenant\Contact::class)->create([
            'email' => 'emailUser2@gmail.com',
            'email2' => 'emailUser2@gmail.com',
        ]);

        $this->json('PUT', '/api/v1/user/contact', [

            'id' => $contactFactory2->id,
            'email' => 'emailUser2@gmail.com',
            'email2' => 'emailUser2@gmail.com',
        ])
            ->assertExactJson(["error" => 200]);
    }

    /*Se intenta modificar un contacto pasando los parámetros correctos de state_id y country_id*/
    public function testOk6()
    {
        $contactFactory = factory(App\Models\tenant\Contact::class)->create([
            'email' => 'prueba@viavox.com',
        ]);

        $countryFactory=factory(App\Models\tenant\Country::cLass)->create([]);
        $stateFactory=factory(App\Models\tenant\State::cLass)->create([
            'country_id'=>$countryFactory->id,
        ]);
        $this->json('PUT', '/api/v1/user/contact', [
            'id' => $contactFactory->id,
            'email' => $contactFactory->email,
            'state_id'=>$stateFactory->id,
            'country_id' =>$countryFactory->id,
        ])
            ->assertExactJson(["error" => 200]);
    }


}