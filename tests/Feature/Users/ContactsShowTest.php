<?php

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContactsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta contactos con parámetros de filtro en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/user/contact/', [
            'page' => 'caracteres',
            'limit' => 'caracteres',
            'country_id' => 'caracteres',
            'language_id' => 'caracteres',
            'sex_id' => 'caracteres',
            'worker_type_id' => 'caracteres',
            'bloqued_login' => 'caracteres',
            'worker' => 'caracteres',
            'document_type_id' => 'caracteres',
            'email' => 'caracteres',
            'date_start' => 'caracteres',

        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [["limit" => ["The limit must be an integer."], "country_id" => ["The country id must be an integer."],
                "page" => ["The page must be an integer."], 'sex_id' => ["The sex id must be an integer."],
                'worker_type_id' => ["The worker type id must be an integer."], 'bloqued_login' => ["The bloqued login field must be true or false."],
                'worker' => ["The worker field must be true or false."], 'document_type_id' => ["The document type id must be an integer."],
                'date_start' => ["The date start does not match the format Y-m-d.", "The date start is not a valid date."],
                'language_id' =>["The language id must be an integer."],
                'date_end' => ["The date end field is required when date start is present."]
            ]]]);
    }

    /**
     * Test filtra por country_id que no existe
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/user/contact/', [
            'country_id' => 999999999999999,
        ])->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
            "error_inputs" => [[
                'country_id' =>["The selected country id is invalid."],

            ]]]);
    }

    /**
     * Test filtra por language_id que no existe
     */
    public function testOk2()
    {
        $this->json('GET', '/api/v1/user/contact/', [
            'language_id' => 9999999999999999,
        ])->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);
    }


    /**
     * Test filtra por sex_id que no existe
     */
    public function testOk4()
    {
        $this->json('GET', '/api/v1/user/contact/', [
            'sex_id' => 9999999999999999,
        ])->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);
    }

    /**
     * Test crea factorys de user, comprueba en base de datos que existen y comprueba el número de resultados al filtrar por cada posible filtro
     */
    public function testOk5()
    {

        $countryFactory1 = factory(App\Models\tenant\Country::class)->create([]);

        $nationalityFactory1 = factory(App\Models\tenant\Country::class)->create([]);

        $sexFactory1 = factory(App\Models\tenant\Sex::class)->create([]);

        $languageFactory1 = factory(App\Models\tenant\Language::class)->create([]);

        $workerTypeFactory1 = factory(App\Models\tenant\WorkerType::class)->create([]);

        $documentTypeFactory1 = factory(App\Models\tenant\DocumentType::class)->create([]);

        $contactFactory1 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'name1',
                'email' => 'name1@gmail.com',
                'email2' => 'name1@gmail.com',
                'country_id' => $countryFactory1->id,
                'language_id' => $languageFactory1->id,
                'sex_id' => $sexFactory1->id,
                'worker_type_id' => $workerTypeFactory1->id,
                'birthdate' => Carbon::now(),
                'document_type_id' => $documentTypeFactory1->id,

            ]);

        factory(App\Models\tenant\Contact::class)->create([
        ]);

        $this->assertDatabaseHas('mo_contact', [
            'name' => $contactFactory1->name,
            'email' => $contactFactory1->email,
            'email2' => $contactFactory1->email2,
            'country_id' => $contactFactory1->country_id,
            'language_id' => $contactFactory1->language_id,
            'sex_id' => $contactFactory1->sex_id,
            'worker_type_id' => $contactFactory1->worker_type_id,
            'document_type_id' => $contactFactory1->document_type_id,

        ]);

        //filtra por country
        $this->json('GET', '/api/v1/user/contact', [
            'country_id' => $contactFactory1->country_id,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id, 'total_results' => 1]);


        //filtra por sexo
        $this->json('GET', '/api/v1/user/contact', [
            'sex_id' => $contactFactory1->sex_id,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id, 'total_results' => 1]);

        //filtra por language_id
        $this->json('GET', '/api/v1/user/contact', [
            'language_id' => $contactFactory1->language_id,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id, 'total_results' => 1]);

        //filtra por worker_type_id
        $this->json('GET', '/api/v1/user/contact', [
            'worker_type_id' => $contactFactory1->worker_type_id,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id, 'total_results' => 1]);

        //filtra por document_type_id
        $this->json('GET', '/api/v1/user/contact', [
            'document_type_id' => $contactFactory1->document_type_id,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id, 'total_results' => 1]);

        //filtra por fecha de nacimiento
        $this->json('GET', '/api/v1/user/contact', [
            'date_start' => Carbon::now()->subYears(1)->format('Y-m-d'),
            'date_end' => Carbon::now()->addYears(1)->format('Y-m-d'),
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por bloqued_login
        $this->json('GET', '/api/v1/user/contact', [
            'bloqued_login' => $contactFactory1->bloqued_login,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por worker
        $this->json('GET', '/api/v1/user/contact', [
            'worker' => $contactFactory1->worker,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por email
        $this->json('GET', '/api/v1/user/contact', [
            'email' => $contactFactory1->email,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por number_document
        $this->json('GET', '/api/v1/user/contact', [
            'number_document' => $contactFactory1->number_document,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por postal_code
        $this->json('GET', '/api/v1/user/contact', [
            'postal_code' => $contactFactory1->postal_code,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por business
        $this->json('GET', '/api/v1/user/contact', [
            'business' => $contactFactory1->business,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por address
        $this->json('GET', '/api/v1/user/contact', [
            'address' => $contactFactory1->address,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por address en province
        $this->json('GET', '/api/v1/user/contact', [
            'address' => $contactFactory1->province,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por address en state
        $this->json('GET', '/api/v1/user/contact', [
            'address' => $contactFactory1->state,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por name
        $this->json('GET', '/api/v1/user/contact', [
            'name' => $contactFactory1->name,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //filtra por name en surname
        $this->json('GET', '/api/v1/user/contact', [
            'name' => $contactFactory1->surname,
        ])
            ->assertJsonFragment(["error" => 200, 'id' => $contactFactory1->id]);

        //no manda filtros
        $this->json('GET', '/api/v1/user/contact')
            ->assertJsonStructure(["error", 'data' => ["0" => ["contact"]], 'total_results']);



    }

    /**
     * Test filtra por telephone que no existe
     */
    public function testOk6()
    {
        $contactFactory1 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'nametest',
                'telephone1' => '111',
                'telephone2' => '222',
                'telephone3' => '333',
                'telephone_business' => '444',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory1->id,
            'name' => $contactFactory1->name,
            'telephone1' => $contactFactory1->telephone1,
            'telephone2' => $contactFactory1->telephone2,
            'telephone3' => $contactFactory1->telephone3,
            'telephone_business' => $contactFactory1->telephone_business,
        ]);

        $this->json('GET', '/api/v1/user/contact/', [
            'name' => 'nametest',
            'telephone' => 9999999999999999,
        ])->assertExactJson(['error' => 200, 'data' => [], "total_results" => 0]);
    }

    /**
     * Test filtra por telephone que está en telephone1
     */
    public function testOk7()
    {
        $contactFactory1 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'nametest',
                'telephone1' => '111',
                'telephone2' => '222',
                'telephone3' => '333',
                'telephone_business' => '444',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory1->id,
            'name' => $contactFactory1->name,
            'telephone1' => $contactFactory1->telephone1,
            'telephone2' => $contactFactory1->telephone2,
            'telephone3' => $contactFactory1->telephone3,
            'telephone_business' => $contactFactory1->telephone_business,

        ]);

        $this->json('GET', '/api/v1/user/contact/', [
            'name' => 'nametest',
            'telephone' => '11',
        ])->assertJsonFragment(["total_results" => 1]);
    }

    /**
     * Test filtra por telephone que está en telephone2
     */
    public function testOk8()
    {
        $contactFactory1 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'nametest',
                'telephone1' => '111',
                'telephone2' => '222',
                'telephone3' => '333',
                'telephone_business' => '444',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory1->id,
            'name' => $contactFactory1->name,
            'telephone1' => $contactFactory1->telephone1,
            'telephone2' => $contactFactory1->telephone2,
            'telephone3' => $contactFactory1->telephone3,
            'telephone_business' => $contactFactory1->telephone_business,

        ]);

        $this->json('GET', '/api/v1/user/contact/', [
            'name' => 'nametest',
            'telephone' => '22',
        ])->assertJsonFragment(["total_results" => 1]);
    }

    /**
     * Test filtra por telephone que está en telephone3
     */
    public function testOk9()
    {
        $contactFactory1 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'nametest',
                'telephone1' => '111',
                'telephone2' => '222',
                'telephone3' => '333',
                'telephone_business' => '444',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory1->id,
            'name' => $contactFactory1->name,
            'telephone1' => $contactFactory1->telephone1,
            'telephone2' => $contactFactory1->telephone2,
            'telephone3' => $contactFactory1->telephone3,
            'telephone_business' => $contactFactory1->telephone_business,

        ]);

        $this->json('GET', '/api/v1/user/contact/', [
            'name' => 'nametest',
            'telephone' => '333',
        ])->assertJsonFragment(["total_results" => 1]);
    }

    /**
     * Test filtra por telephone que está en telephone_business
     */
    public function testOk10()
    {
        $contactFactory1 = factory(App\Models\tenant\Contact::class)->create(
            [
                'name' => 'nametest',
                'telephone1' => '111',
                'telephone2' => '222',
                'telephone3' => '333',
                'telephone_business' => '444',
            ]);

        $this->assertDatabaseHas('mo_contact', [
            'id' => $contactFactory1->id,
            'name' => $contactFactory1->name,
            'telephone1' => $contactFactory1->telephone1,
            'telephone2' => $contactFactory1->telephone2,
            'telephone3' => $contactFactory1->telephone3,
            'telephone_business' => $contactFactory1->telephone_business,

        ]);

        $this->json('GET', '/api/v1/user/contact/', [
            'name' => 'nametest',
            'telephone' => '444',
        ])->assertJsonFragment(["total_results" => 1]);
    }
}