<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UsersDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No manda id requerido
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/user', [

        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["ids"=>["The ids field is required."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda formato incorrecto en campo id requerido
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/user', [
            'ids' => ['caracteres'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["ids.0"=>["The ids.0 must be an integer."]]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Manda id de usuario inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/user', [
            'ids' => [99999999],
        ])
            ->assertExactJson(["error"=>404,"error_description"=>"Data not found", "error_inputs" => [
                [
                    "ids.0" => ["The selected ids.0 is invalid."],
                ]
            ]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([
        ]);

        $this->assertDatabaseHas('mo_user', [
            'id' => $userFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/user', [
            'ids' => $userFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Crea un registro, comprueba que existe en base de datos, borra y comprueba que ya no existe
     *
     */
    public function testOk1()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        //comprueba que el usuario se ha creado
        $this->assertDatabaseHas('mo_user', [
            'id' => $userFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/user', [
            'ids' => [$userFactory->id],
        ])->assertExactJson([
                'error' => 200,
            ]);

        //comprueba que el campo deleted_at del usuario que hemos borrado sea distinto de null
        $this->assertDatabaseMissing('mo_user', ['deleted_at' => null, 'id' => $userFactory->id]);
    }

    /**
     * Crea dos registros, comprueba que existen en base de datos, borra y comprueba que ya no existen
     *
     */
    public function testOk2()
    {

        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $userFactory2 = factory(App\Models\tenant\User::class)->create([]);

        //comprueba que los usuarios se han creado
        $this->assertDatabaseHas('mo_user', [
            'id' => $userFactory->id,
        ]);

        $this->assertDatabaseHas('mo_user', [
            'id' => $userFactory2->id,
        ]);

        $this->json('DELETE', '/api/v1/user', [
            'ids' => [$userFactory->id, $userFactory2->id],
        ])->assertExactJson([
            'error' => 200,
        ]);

        //comprueba que el campo deleted_at de los usuarios que hemos borrado sea distinto de null
        $this->assertDatabaseMissing('mo_user', ['deleted_at' => null, 'id' => $userFactory->id]);

        $this->assertDatabaseMissing('mo_user', ['deleted_at' => null, 'id' => $userFactory2->id]);
    }


}