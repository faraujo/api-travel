<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class WorkerTypesShowTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test mostrar tipos de trabajador idioma incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/user/workers', [
            'lang' => 'FDF',
        ])->assertExactJson([
            'error' => 400,
            "error_description" => "The fields are not the required format",
            "error_inputs" => [[
                "lang" => ["The lang is not exists"]
            ]]
        ]);
    }

    /**
     * test consulta de tipos de trabajador sin parametros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/user/workers', [])
            ->assertJsonStructure(['error', 'data']);
    }

    /**
     * test consulta de tipos de trabajador con idioma correcto
     */
    public function testOk2()
    {
        $this->json('GET', '/api/v1/user/workers', [
            'lang' => 'ES',
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

}