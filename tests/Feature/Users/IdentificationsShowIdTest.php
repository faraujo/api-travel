<?php

use App\Models\tenant\Identification;
use App\Models\tenant\IdentificationTranslation;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class IdentificationsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta identificador inexistente sin filtro
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/user/identifications/999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar identificador con filtro lang erróneo
     */
    public function testBad2()
    {
        $identificationFactory = factory(\App\Models\tenant\Identification::class)->create([
        ]);

        factory(\App\Models\tenant\IdentificationTranslation::class)->create([
            'language_id' => 1,
            'identification_id' => $identificationFactory->id,
        ]);
        factory(\App\Models\tenant\IdentificationTranslation::class)->create([
            'language_id' => 2,
            'identification_id' => $identificationFactory->id,
        ]);
        factory(\App\Models\tenant\IdentificationTranslation::class)->create([
            'language_id' => 3,
            'identification_id' => $identificationFactory->id,
        ]);

        $this->json('GET', '/api/v1/user/identifications/' . $identificationFactory->id, [
            'lang' => 'FDF'])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Consulta identificadores existente sin filtro lang
     */
    public function testOK1()
    {
        $identificationFactory = factory(Identification::class)->create([
        ]);

        $translation1 = factory(IdentificationTranslation::class)->create([
            'language_id' => 1,
            'identification_id' => $identificationFactory->id,
        ]);
        $translation2 = factory(IdentificationTranslation::class)->create([
            'language_id' => 2,
            'identification_id' => $identificationFactory->id,
        ]);
        $translation3 = factory(IdentificationTranslation::class)->create([
            'language_id' => 3,
            'identification_id' => $identificationFactory->id,
        ]);
        $this->json('GET', '/api/v1/user/identifications/' . $identificationFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $identificationFactory->id,
                'name' => $translation1->name,
            ]);
    }

    /**
     * Consulta identificadores existente con filtro lang EN
     */
    public function testOK2()
    {
        $identificationFactory = factory(Identification::class)->create([
        ]);
        $translation = factory(IdentificationTranslation::class)->create([
            'language_id' => 2,
            'identification_id' => $identificationFactory->id,
        ]);

        $this->json('GET', '/api/v1/user/identifications/' . $identificationFactory->id, [
            'lang' => 'EN'])->assertJsonFragment([
            'error' => 200,
            'id' => $identificationFactory->id,
            'name' => $translation->name,
        ]);
    }


}