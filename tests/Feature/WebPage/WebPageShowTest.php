<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class WebPageShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta page con parametro lang mal
     */
    public function testBad1()
    {
        // Se especifica parametro lang incorrectamente
        $this->json('GET', '/api/v1/webpage', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta productos con parametro lang
     */
    public function testOk1()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/webpage', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta pagees con parametro lang en mayusculas
     */
    public function testOk2()
    {
        //Se especifica parametro lang correctamente en mayusculas
        $this->json('GET', '/api/v1/webpage', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    public function testOk3()
    {
        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/webpage', [
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    public function testOk4()
    {
        $PageFactory = factory(App\Models\tenant\WebPage::class)->create([
            'type' => 'test',
        ]);
        $translation1 = factory(App\Models\tenant\WebPageTranslation::class)->create([
            'language_id' => 1,
            'page_id' => $PageFactory->id,
        ]);

        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/webpage', [
            'type' => $PageFactory->type
        ])->assertJsonFragment(['type' => $PageFactory->type]);
    }

    

    public function testOk5()
    {
        $PageFactory = factory(App\Models\tenant\WebPage::class)->create([
            'type' => 'test',
        ]);
        $translation1 = factory(App\Models\tenant\WebPageTranslation::class)->create([
            'language_id' => 1,
            'page_id' => $PageFactory->id,
            'content' => 'texto contenido'
        ]);

        //Se especifica parametro lang correctamente
        $this->json('GET', '/api/v1/webpage', [
            'content' => $translation1->content
        ])->assertJsonFragment(['type' => $PageFactory->type]);
    }
}