<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class WebPageShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta página inexistente sin parametros
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/webpage/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


    /**
     * Consulta página existente sin parametros
     */
    public function testOk1()
    {

        $PageFactory = factory(App\Models\tenant\WebPage::class)->create([
            'type' => 'test',
        ]);
        $translation1 = factory(App\Models\tenant\WebPageTranslation::class)->create([
            'language_id' => 1,
            'page_id' => $PageFactory->id,
        ]);
        factory(App\Models\tenant\WebPageTranslation::class)->create([
            'language_id' => 2,
            'page_id' => $PageFactory->id,
        ]);
        factory(App\Models\tenant\WebPageTranslation::class)->create([
            'language_id' => 3,
            'page_id' => $PageFactory->id,
        ]);

        $this->json('GET', '/api/v1/webpage/' . $PageFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $PageFactory->id,
                'type' => $PageFactory->type
            ]);
    }
}