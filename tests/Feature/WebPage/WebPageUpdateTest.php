<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;


class WebPageUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations en formato incorrecto
     */
    public function testBad1()
    {

        $pageFactory = factory(App\Models\tenant\WebPage::class)->create([
            'type' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/webpage', [
            'id' => $pageFactory->id])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "translation"=>["you need at least one translation"]
                ]]]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se mandan campos requeridos y se mandan categories, tags, files, services y locations inexistentes
     */
    public function testOk1()
    {
        $pageFactory = factory(App\Models\tenant\WebPage::class)->create([
            'type' => 'test1',
        ]);

        $this->json('PUT', '/api/v1/webpage', [
            'id' => $pageFactory->id,
            'content' => ['ES' => 'cualquiera español']
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_web_page_translation', [
            'page_id' => $pageFactory->id,
            'language_id' => 1,
            'content' => 'cualquiera español'
        ]);
    }
}