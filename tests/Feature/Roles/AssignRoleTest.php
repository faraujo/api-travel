<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class AssignRoleTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin envio de parametros
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/role/assign')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'user_id' => ['The user id field is required.'],
            ]]]);
    }

    /**
     * Prueba automatizada envio de parametro user_id incorrecto
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => 'prueba'
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'user_id' => ['The user id must be an integer.'],
            ]]]);
    }

    /**
     * Prueba automatizada envio de parametro user_id no existente
     */
    public function testBad3()
    {
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => 999999999999,
        ])
            ->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Prueba automatizada envio de parametro roles incorrecto
     */
    public function testBad4()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
            'roles' => 'prueba',
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles' => ['The roles must be an array.'],
            ]]]);
    }

    /**
     * Prueba automatizada envio de posicion del parametro roles vacio
     */
    public function testBad5()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
            'roles' => ['0' => null],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles.0' => ['The roles.0 field is required.'],
            ]]]);
    }

    /**
     * Prueba automatizada envio de posicion del parametro roles incorrecto
     */
    public function testBad6()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
            'roles' => ['0' => 'prueba'],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles.0' => ['The roles.0 must be an integer.'],
            ]]]);
    }

    /**
     * Prueba automatizada envio de posicion del parametro roles duplicados
     */
    public function testBad7()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
            'roles' => [$role->id, $role->id],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles.0' => ['The roles.0 field has a duplicate value.'],
                'roles.1' => ['The roles.1 field has a duplicate value.'],
            ]]]);
    }

    /**
     * Prueba automatizada envio de posicion del parametro roles borrado
     */
    public function testBad8()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $role = factory(\App\Models\tenant\Role::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
            'roles' => [$role->id],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'roles.0' => ['The selected roles.0 is invalid.'],
            ]]]);
    }

    /**
     * Prueba automatizada envio de posicion del parametro user_id borrado
     */
    public function testBad9()
    {
        $user = factory(\App\Models\tenant\User::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
        ])
            ->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Prueba automatizada envio de user_id sin roles
     */
    public function testOk1()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
        ])
            ->assertExactJson(['error' => 200]);
    }

    /**
     * Prueba automatizada envio de user_id con roles
     */
    public function testOk2()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $role2 = factory(\App\Models\tenant\Role::class)->create([]);

        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
            'roles' => [
                '0' => $role->id,
                '1' => $role2->id
            ]
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_user_role', ['user_id' => $user->id, 'role_id' => $role->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_user_role', ['user_id' => $user->id, 'role_id' => $role2->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada envio de user_id con roles teniendo roles asociados
     */
    public function testOk3()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $role_2 = factory(\App\Models\tenant\Role::class)->create([]);

        $role_user = factory(\App\Models\tenant\UserRole::class)->create([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        $this->json('POST', '/api/v1/role/assign', [
            'user_id' => $user->id,
            'roles' => ['0' => $role_2->id]
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_user_role', ['user_id' => $user->id, 'role_id' => $role->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_user_role', ['user_id' => $user->id, 'role_id' => $role_2->id, 'deleted_at' => null]);
    }

}