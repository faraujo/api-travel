<?php

use App\Models\tenant\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RolesCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parametros requeridos
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/role')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'name' => ['The name field is required.'],
                ]
            ]]);
    }

    /**
     * Prueba automatizada role duplicado
     */
    public function testBad2()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $permission = factory(\App\Models\tenant\Permission::class)->create([]);

        $this->json('POST', '/api/v1/role', [
            'name' => $role->name,
            'permissions' => [$permission->id],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'name' => ['The name has already been taken.']
                ]
            ]]);
    }

    /**
     * Prueba automatizada con un permiso no existente en base de datos
     */
    public function testBad3()
    {

        $this->json('POST', '/api/v1/role', [
            'name' => 'prueba permiso malo',
            'permissions' => [
                'globalaccess' => [999999],
                ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'permissions.globalaccess.0' => ['The selected permissions.globalaccess.0 is invalid.']
                ]
            ]]);
    }

    /**
     * Prueba automatizada con tipo de dato permiso incorrecto
     */
    public function testBad4()
    {

        $this->json('POST', '/api/v1/role', [
            'name' => 'prueba permiso malo',
            'permissions' => [
                'globalaccess' => ['string'],
            ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'permissions.globalaccess.0' => ['The permissions.globalaccess.0 must be an integer.']
                ]
            ]]);
    }

    /**
     * Prueba automatizada con tipo de dato permiso duplicado
     */
    public function testBad5()
    {

        $permission = factory(\App\Models\tenant\Permission::class)->create([]);

        $this->json('POST', '/api/v1/role', [
            'name' => 'prueba permiso malo',
            'permissions' => [
            'globalaccess' => [$permission->id, $permission->id],
            ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'permissions.globalaccess.0' => ['The permissions.globalaccess.0 field has a duplicate value.'],
                    'permissions.globalaccess.1' => ['The permissions.globalaccess.1 field has a duplicate value.'],
                ]
            ]]);
    }

    /**
     * Prueba automatizada con tipo de dato permiso vacio
     */
    public function testBad6()
    {

        $this->json('POST', '/api/v1/role', [
            'name' => 'prueba permiso malo',
            'permissions' => [
                'globalaccess' => [null],
            ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'permissions.globalaccess.0' => ['The permissions.globalaccess.0 must be an integer.']
                ]
            ]]);
    }

    /**
     * Prueba automatizada role correcto y permisos
     */
    public function testOk1()
    {
        $permission = factory(\App\Models\tenant\Permission::class)->create([]);

        $this->json('POST', '/api/v1/role', [
            'name' => 'nombre rol test prueba automatizada',
            'description' => 'descripción rol test prueba automatizada',
            'permissions' => [
                'globalaccess' => [$permission->id],
            ],
        ])
            ->assertExactJson(['error' => 200])
            ->assertJsonStructure();

        $this->assertDatabaseHas('mo_role', [
            'name' => 'nombre rol test prueba automatizada',
            'description' => 'descripción rol test prueba automatizada',
        ]);

        $rol = Role::where('name', 'nombre rol test prueba automatizada')->first();
        $this->assertDatabaseHas('mo_role_permission', [
            'permission_id' => $permission->id,
            'role_id' => $rol->id,
        ]);
    }

}