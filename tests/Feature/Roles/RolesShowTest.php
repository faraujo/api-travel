<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RolesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada errores tipo de parametros enviados
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/role', [
            'user_id' => 'prueba',
            'page' => 'prueba',
            'limit' => 'prueba',
            'permission_id' => 'prueba',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'limit' => ['The limit must be an integer.'],
            'page' => ['The page must be an integer.'],
            'permission_id' => ['The permission id must be an integer.'],
            'user_id' => ['The user id must be an integer.'],
        ]]]);
    }

    /**
     * Prueba automatizada correcta sin parametros
     */
    public function testOk1()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $this->json('GET', '/api/v1/role', [])
            ->assertJsonStructure(['error', 'data' => ['0' => ['role' => [['id', 'name', 'description']]]], 'total_results']);
    }

    /**
     * Prueba automatizada correcta con parametros
     */
    public function testOk2()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $this->json('GET', '/api/v1/role', [
            'page' => 1,
            'limit' => 1,
        ])
            ->assertJsonStructure(['error', 'data' => ['0' => ['role' => [['id', 'name', 'description']]]], 'total_results']);
    }

    /**
     * Prueba automatizada correcta con parametro user
     */
    public function testOk3()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $role_user = factory(\App\Models\tenant\UserRole::class)->create([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([]);
        $permision_role = factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);
        $this->json('GET', '/api/v1/role', [
            'user_id' => $user->id
        ])
            ->assertExactJson(['error' => 200, 'data' => ['0' => ['role' => [[
                'id' => $role->id,
                'name' => $role->name,
                'description' => $role->description,
                ]]]], 'total_results' => 1]);
    }

    /**
     * Prueba automatizada correcta con parametro name con nombre del rol
     */
    public function testOk4()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $role_user = factory(\App\Models\tenant\UserRole::class)->create([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([]);
        $permision_role = factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);
        $this->json('GET', '/api/v1/role', [
            'name' => $role->name
        ])
            ->assertExactJson(['error' => 200, 'data' => ['0' => ['role' => [[
                'id' => $role->id,
                'name' => $role->name,
                'description' => $role->description,
                ]]]], 'total_results' => 1]);
    }


    /**
     * Prueba automatizada correcta con parametro name con descripcion del rol
     */
    public function testOk5()
    {
        $user = factory(\App\Models\tenant\User::class)->create([]);
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $role_user = factory(\App\Models\tenant\UserRole::class)->create([
            'role_id' => $role->id,
            'user_id' => $user->id,
        ]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([]);
        $permision_role = factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);
        $this->json('GET', '/api/v1/role', [
            'name' => $role->description
        ])
            ->assertExactJson(['error' => 200, 'data' => ['0' => ['role' => [[
                'id' => $role->id,
                'name' => $role->name,
                'description' => $role->description,
                ]]]], 'total_results' => 1]);
    }

    /**
     * Prueba automatizada correcta con parametro permission_id
     */
    public function testOk6()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $role_2 = factory(\App\Models\tenant\Role::class)->create([]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([]);
        $permision_role = factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);
        $this->json('GET', '/api/v1/role', [
            'permission_id' => $permission->id
        ])
            ->assertExactJson(['error' => 200, 'data' => ['0' => ['role' => [[
                'id' => $role->id,
                'name' => $role->name,
                'description' => $role->description,
                ]]]], 'total_results' => 1]);
    }

    /**
     * Prueba automatizada correcta con parametro permission y name sin resultados por estar asociado a distinto rol
     */
    public function testOk7()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $role_2 = factory(\App\Models\tenant\Role::class)->create([]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([]);
        $permision_role = factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role_2->id,
            'permission_id' => $permission->id,
        ]);

        $this->json('GET', '/api/v1/role', [
            'name' => $role->name,
            'permission_id' => $permission->id
        ])
            ->assertExactJson(['error' => 200, 'data' => [], 'total_results' => 0]);
    }

    /**
     * Prueba automatizada correcta con permiso borrado
     */
    public function testOk8()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);
        $permision_role = factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);

        $this->json('GET', '/api/v1/role', [
            'name' => $role->name,
            'permission_id' => $permission->id
        ])
            ->assertExactJson(['error' => 200, 'data' => [], 'total_results' => 0]);
    }

    /**
     * Prueba automatizada correcta con usuario borrado
     */
    public function testOk9()
    {
        $user = factory(\App\Models\tenant\User::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $user_role = factory(\App\Models\tenant\UserRole::class)->create([
            'user_id' => $user->id,
            'role_id' => $role->id,
        ]);

        $this->json('GET', '/api/v1/role', [
            'name' => $role->name,
            'user_id' => $user->id
        ])
            ->assertExactJson(['error' => 200, 'data' => [], 'total_results' => 0]);
    }

}