<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RolesShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada rol inexistente
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/role/99999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Prueba automatizada rol existente sin permisos
     */
    public function testOk2()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $this->json('GET', '/api/v1/role/' . $role->id)
            ->assertJsonFragment(["general"=>[["permissions"=>[]]]]);
    }

    /**
     * Prueba automatizada rol existente con permisos
     */
    public function testOk3()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([]);

        factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);

        $this->json('GET', '/api/v1/role/' . $role->id)
            ->assertJsonFragment(['general' => [['permissions' => [['id' => $permission->id]]]]]);
    }

    /**
     * Prueba automatizada rol existente con permisos borrados
     */
    public function testOk4()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);

        factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);

        $this->json('GET', '/api/v1/role/' . $role->id)
            ->assertJsonFragment(["general"=>[["permissions"=>[]]]]);
    }

    /**
     * Prueba automatizada rol existente con relacion con permisos borrada
     */
    public function testOk5()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([
        ]);

        factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);

        $this->json('GET', '/api/v1/role/' . $role->id)
            ->assertJsonFragment(["general"=>[[
                "permissions"=>[[
                    "id"=>$permission->id
                ]]
            ]]]);
    }

    /**
     * Prueba automatizada rol existente con algunos permisos borrados
     */
    public function testOk6()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);
        $permission = factory(\App\Models\tenant\Permission::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);
        $permission_2 = factory(\App\Models\tenant\Permission::class)->create([]);

        factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission->id,
        ]);

        factory(\App\Models\tenant\RolePermission::class)->create([
            'role_id' => $role->id,
            'permission_id' => $permission_2->id,
        ]);

        $this->json('GET', '/api/v1/role/' . $role->id)
            ->assertJsonFragment(["general"=>[[
                            "permissions"=>[[
                                "id"=>$permission_2->id
                            ]]
                        ]]]);
    }

    /**
     * Prueba automatizada rol borrado
     */
    public function testOk7()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);

        $this->json('GET', '/api/v1/role/' . $role->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


}