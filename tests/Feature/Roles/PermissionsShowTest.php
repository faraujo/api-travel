<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PermissionsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada realiza la consulta y comprueba el formato de respuesta
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/role/permissions')
            ->assertJsonStructure([
                'error','data'
            ]);
    }


}