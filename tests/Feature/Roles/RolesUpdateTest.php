<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RolesUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parametros requeridos
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/role')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'id' => ["The id field is required."],
                    'name' => ['The name field is required.'],
                ]
            ]]);
    }

    /**
     * Prueba automatizada con parámetros requeridos e id inexistente en base de datos
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/role', [
            'id' => 9999999,
            'name' => 'nuevo',
        ])
            ->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Prueba automatizada role duplicado
     */
    public function testBad3()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $role2 = factory(\App\Models\tenant\Role::class)->create([]);

        $this->json('PUT', '/api/v1/role', [
            'id' => $role2->id,
            'name' => $role->name,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'name' => ['The name has already been taken.']
                ]
            ]]);

        $this->json('PUT', '/api/v1/role', [
            'id' => $role->id,
            'name' => $role->name,
            'description' => 'compara si el name es único sólo si mandan un name diferente al que ya tiene el registro',
        ])
            ->assertExactJson(['error' => 200]);
    }

    /**
     * Prueba automatizada con un permiso no existente en base de datos
     */
    public function testBad4()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $this->json('PUT', '/api/v1/role', [
            'id' => $role->id,
            'name' => $role->name,
            'permissions' => [
                'general' => [999999],
            ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'permissions.general.0' => ['The selected permissions.general.0 is invalid.']
                ]
            ]]);
    }

    /**
     * Prueba automatizada con tipo de dato permiso incorrecto
     */
    public function testBad5()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $this->json('PUT', '/api/v1/role', [
            'id' => $role->id,
            'name' => $role->name,
            'permissions' => [
                'general' => ['prueba automatizada'],
            ],

        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'permissions.general.0' => ['The permissions.general.0 must be an integer.']
                ]
            ]]);
    }

    /**
     * Prueba automatizada con tipo de dato permiso duplicado
     */
    public function testBad6()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $permission = factory(\App\Models\tenant\Permission::class)->create([]);

        $this->json('PUT', '/api/v1/role', [
            'id' => $role->id,
            'name' => $role->name,
            'permissions' => [
                'general' => [$permission->id, $permission->id],
            ],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [
                [
                    'permissions.general.0' => ['The permissions.general.0 field has a duplicate value.'],
                    'permissions.general.1' => ['The permissions.general.1 field has a duplicate value.'],
                ]
            ]]);
    }

    /**
     * Prueba automatizada, crea factory de role correcto y le asigna permiso, comprueba que hay relación en tabla role-permission, lo modifica y le asigna otro permiso,
     * comprueba que se ha modificado, que se ha creado la relación en tabla intermedia y que ha eliminado la relación anterior
     */
    public function testOk1()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([]);

        $permission = factory(\App\Models\tenant\Permission::class)->create([]);

        $this->assertDatabaseHas('mo_role', [
            'name' => $role->name,
        ]);

        $this->json('PUT', '/api/v1/role', [
            'id' => $role->id,
            'name' => 'modifi',
            'permissions' => [
                'globalaccess' => [$permission->id],
            ],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_role', [
            'name' => 'modifi',
        ]);

        $this->assertDatabaseHas('mo_role_permission', [
            'permission_id' => $permission->id,
            'role_id' => $role->id,
        ]);

        $permission2 = factory(\App\Models\tenant\Permission::class)->create([]);

        $this->json('PUT', '/api/v1/role', [
            'id' => $role->id,
            'name' => 'modifi',
            'permissions' => [
                'globalaccess' => [$permission2->id],
            ],
        ])
            ->assertExactJson(['error' => 200]);


        $this->assertDatabaseHas('mo_role_permission', [
            'permission_id' => $permission2->id,
            'role_id' => $role->id,
        ]);

        $this->assertDatabaseMissing('mo_role_permission', [
            'permission_id' => $permission->id,
            'role_id' => $role->id,
            'deleted_at' => null,
        ]);

    }

}