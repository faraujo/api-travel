<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RolesDeleteTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parametros requeridos
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/role')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'ids' => ['The ids field is required.'],
            ]]]);
    }

    /**
     * Prueba automatizada tipo de dato incorrecto
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/role', [
            'ids' => ['prueba automatizada'],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'ids.0' => ['The ids.0 must be an integer.'],
        ]]]);
    }

    /**
     * Prueba automatizada role no existente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/role', [
            'ids' => [99999999999],
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found', 'error_inputs' => [['ids.0' => ["The selected ids.0 is invalid."]]]]);
    }

    /**
     * Prueba automatizada role ya borrado en base de datos
     */
    public function testBad4()
    {
        $role = factory(\App\Models\tenant\Role::class)->create([
            'deleted_at' => \Carbon\Carbon::now(),
        ]);
        $this->json('DELETE', '/api/v1/role', [
            'ids' => [$role->id],
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found', 'error_inputs' => [['ids.0' => ["The selected ids.0 is invalid."]]]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad5()
    {

        $roleFactory = factory(App\Models\tenant\Role::class)->create([
        ]);

        $this->assertDatabaseHas('mo_role', [
            'id' => $roleFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/role', [
            'ids' => $roleFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Prueba automatizada correcta borrado de rol y sus relaciones
     */
    public function testOk1()
    {
        $rol = factory(\App\Models\tenant\Role::class)->create([]);
        $permiso = factory(\App\Models\tenant\Permission::class)->create([]);
        $permisoRol = factory(\App\Models\tenant\RolePermission::class)->create(['role_id' => $rol->id, 'permission_id' => $permiso->id]);
        $usuario = factory(\App\Models\tenant\User::class)->create([]);
        $usuarioRol = factory(\App\Models\tenant\UserRole::class)->create(['user_id' => $usuario->id, 'role_id' => $rol->id]);

        $this->assertDatabaseHas('mo_role', ['id' => $rol->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_role_permission', ['id' => $permisoRol->id, 'role_id' => $rol->id, 'permission_id' => $permiso->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_user_role', ['id' => $usuarioRol->id, 'role_id' => $rol->id, 'user_id' => $usuario->id, 'deleted_at' => null]);

        $this->json('DELETE', '/api/v1/role', [
            'ids' => [$rol->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_role', ['id' => $rol->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_role_permission', ['id' => $permisoRol->id, 'role_id' => $rol->id, 'permission_id' => $permiso->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_user_role', ['id' => $usuarioRol->id, 'role_id' => $rol->id, 'user_id' => $usuario->id, 'deleted_at' => null]);

    }

    /**
     * Prueba automatizada correcta borrado de dos roles y sus relaciones
     */
    public function testOk2()
    {
        $rol = factory(\App\Models\tenant\Role::class)->create([]);
        $permiso = factory(\App\Models\tenant\Permission::class)->create([]);
        $permisoRol = factory(\App\Models\tenant\RolePermission::class)->create(['role_id' => $rol->id, 'permission_id' => $permiso->id]);
        $usuario = factory(\App\Models\tenant\User::class)->create([]);
        $usuarioRol = factory(\App\Models\tenant\UserRole::class)->create(['user_id' => $usuario->id, 'role_id' => $rol->id]);

        $rol2 = factory(\App\Models\tenant\Role::class)->create([]);
        $permiso2 = factory(\App\Models\tenant\Permission::class)->create([]);
        $permisoRol2 = factory(\App\Models\tenant\RolePermission::class)->create(['role_id' => $rol2->id, 'permission_id' => $permiso2->id]);
        $usuario2 = factory(\App\Models\tenant\User::class)->create([]);
        $usuarioRol2 = factory(\App\Models\tenant\UserRole::class)->create(['user_id' => $usuario2->id, 'role_id' => $rol2->id]);




        $this->assertDatabaseHas('mo_role', ['id' => $rol->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_role_permission', ['id' => $permisoRol->id, 'role_id' => $rol->id, 'permission_id' => $permiso->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_user_role', ['id' => $usuarioRol->id, 'role_id' => $rol->id, 'user_id' => $usuario->id, 'deleted_at' => null]);

        $this->assertDatabaseHas('mo_role', ['id' => $rol2->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_role_permission', ['id' => $permisoRol2->id, 'role_id' => $rol2->id, 'permission_id' => $permiso2->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_user_role', ['id' => $usuarioRol2->id, 'role_id' => $rol2->id, 'user_id' => $usuario2->id, 'deleted_at' => null]);

        $this->json('DELETE', '/api/v1/role', [
            'ids' => [$rol->id,$rol2->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_role', ['id' => $rol->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_role_permission', ['id' => $permisoRol->id, 'role_id' => $rol->id, 'permission_id' => $permiso->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_user_role', ['id' => $usuarioRol->id, 'role_id' => $rol->id, 'user_id' => $usuario->id, 'deleted_at' => null]);

        $this->assertDatabaseMissing('mo_role', ['id' => $rol2->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_role_permission', ['id' => $permisoRol2->id, 'role_id' => $rol2->id, 'permission_id' => $permiso2->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_user_role', ['id' => $usuarioRol2->id, 'role_id' => $rol2->id, 'user_id' => $usuario2->id, 'deleted_at' => null]);

    }



}