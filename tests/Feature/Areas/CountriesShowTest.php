<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CountriesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada con idioma incorrecto
     */
    function testBad1(){
        $this->json('GET','/api/v1/area/country?lang=TEST')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'lang' => ['The lang is not exists'],
            ]]]);
    }

    /**
     * Prueba automatizada con formato de datos incorrectos
     */
    function testBad2(){
        $this->json('GET','/api/v1/area/country?page=TEST&limit=TEST')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'page' => ['The page must be an integer.'],
                'limit' => ['The limit must be an integer.'],
            ]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $this->json('GET','/api/v1/area/country')
            ->assertJsonStructure(['error','data','total_results']);
    }

    /**
     * Prueba automatizada con filtro lang
     */
    function testOk2(){
        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $continent = factory(\App\Models\tenant\Continent::class)->create([
        ]);

        $country = factory(\App\Models\tenant\Country::class)->create([
            'continent_id' => $continent->id,
        ]);

        $countryTranslation = factory(\App\Models\tenant\CountryTranslation::class)->create([
            'country_id' => $country->id,
            'language_id' => $language->id,
            'name' => 'country test'
        ]);

        $this->json('GET','/api/v1/area/country?lang='.$language->abbreviation)
            ->assertJsonFragment([
                'id' => $country->id,
                'continent_id' => $country->continent_id,
                'name' => $countryTranslation->name,
            ]);
    }

    /**
     * Prueba automatizada con filtro continent_id
     */
    function testOk3(){
        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $continent = factory(\App\Models\tenant\Continent::class)->create([
        ]);

        $country = factory(\App\Models\tenant\Country::class)->create([
            'continent_id' => $continent->id,
        ]);

        $countryTranslation = factory(\App\Models\tenant\CountryTranslation::class)->create([
            'country_id' => $country->id,
            'language_id' => $language->id,
            'name' => 'country test'
        ]);


        $continent2 = factory(\App\Models\tenant\Continent::class)->create([
        ]);

        $country2 = factory(\App\Models\tenant\Country::class)->create([
            'continent_id' => $continent2->id,
        ]);

        $countryTranslation2 = factory(\App\Models\tenant\CountryTranslation::class)->create([
            'country_id' => $country2->id,
            'language_id' => $language->id,
            'name' => 'country2 test'
        ]);

        $this->json('GET','/api/v1/area/country?continent_id='.$continent->id)
            ->assertJsonFragment([
                'id' => $country->id,
                'continent_id' => $country->continent_id,
                'name' => $countryTranslation->name,
                'total_results' => 1,
            ]);

        $this->json('GET','/api/v1/area/country?continent_id='.$continent2->id)
            ->assertJsonFragment([
                'id' => $country2->id,
                'continent_id' => $country2->continent_id,
                'name' => $countryTranslation2->name,
                'total_results' => 1,
            ]);
    }


    /**
     * Prueba automatizada, comprueba base de datos con campos actualizados y estructura
     */
    function testOk4(){

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $continent = factory(\App\Models\tenant\Continent::class)->create([
            'iso2' => 'iso prueba'
        ]);

        $country = factory(\App\Models\tenant\Country::class)->create([
            'continent_id' => $continent->id,
            'iso3' => 'iso test',
            'order' => 2
        ]);

        $countryTranslation = factory(\App\Models\tenant\CountryTranslation::class)->create([
            'country_id' => $country->id,
            'language_id' => $language->id,
            'name' => 'country test'
        ]);

        $this->assertDatabaseHas('mo_language', [
            'id' => $language->id,
            'abbreviation' => $language->abbreviation
        ]);

        $this->assertDatabaseHas('mo_continent', [
            'id' => $continent->id,
            'iso2' => $continent->iso2
        ]);

        $this->assertDatabaseHas('mo_country', [
            'id' => $country->id,
            'continent_id' => $country->continent_id,
            'iso3' => $country->iso3,
            'order' => $country->order
        ]);

        $this->assertDatabaseHas('mo_country_translation', [
            'name' => $countryTranslation->name,
        ]);

        $this->json('GET', '/api/v1/area/country', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['country' => ["0" => ['id', 'lang']]]]
            ]);

    }

}