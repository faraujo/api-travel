<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContinentsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada con idioma incorrecto
     */
    function testBad1(){
        $this->json('GET','/api/v1/area/continent?lang=TEST')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'lang' => ['The lang is not exists'],
            ]]]);
    }

    /**
     * Prueba automatizada con formato de datos incorrectos
     */
    function testBad2(){
        $this->json('GET','/api/v1/area/continent?page=TEST&limit=TEST')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'page' => ['The page must be an integer.'],
                'limit' => ['The limit must be an integer.'],
            ]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $this->json('GET','/api/v1/area/continent')
            ->assertJsonStructure(['error','data','total_results']);
    }

    /**
     * Prueba automatizada con filtro lang
     */
    function testOk2(){
        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $continent = factory(\App\Models\tenant\Continent::class)->create([
        ]);

        $continentTranslation = factory(\App\Models\tenant\ContinentTranslation::class)->create([
            'continent_id' => $continent->id,
            'language_id' => $language->id,
            'name' => 'continente test'
        ]);

        $this->json('GET','/api/v1/area/continent?lang='.$language->abbreviation)
            ->assertJsonFragment([
                'id' => $continent->id,
                'name' => $continentTranslation->name,
            ]);
    }


    /**
     * Prueba automatizada, comprueba base de datos con campos actualizados y estructura
     */
    function testOk3(){

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $continent = factory(\App\Models\tenant\Continent::class)->create([
            'iso2' => 'te'
        ]);

        $continentTranslation = factory(\App\Models\tenant\ContinentTranslation::class)->create([
            'continent_id' => $continent->id,
            'language_id' => $language->id,
            'name' => 'continente test'
        ]);

        $this->assertDatabaseHas('mo_language', [
            'id' => $language->id,
            'abbreviation' => $language->abbreviation
        ]);

        $this->assertDatabaseHas('mo_continent', [
            'id' => $continent->id,
            'iso2' => $continent->iso2
        ]);

        $this->assertDatabaseHas('mo_continent_translation', [
            'name' => $continentTranslation->name,
        ]);

        $this->json('GET', '/api/v1/area/continent', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['continent' => ["0" => ['id', 'lang']]]]
            ]);

    }

}