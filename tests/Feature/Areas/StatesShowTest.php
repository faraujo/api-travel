<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class StatesShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada con formato de datos incorrectos
     */
    function testBad1(){
        $this->json('GET','/api/v1/area/state?page=TEST&limit=TEST')
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'page' => ['The page must be an integer.'],
                'limit' => ['The limit must be an integer.'],
            ]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $this->json('GET','/api/v1/area/state')
            ->assertJsonStructure(['error','data','total_results']);
    }

    /**
     * Prueba automatizada con filtro country_id
     */
    function testOk2(){
        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $continent = factory(\App\Models\tenant\Continent::class)->create([
        ]);

        $country = factory(\App\Models\tenant\Country::class)->create([
            'continent_id' => $continent->id,
        ]);

        $countryTranslation = factory(\App\Models\tenant\CountryTranslation::class)->create([
            'country_id' => $country->id,
            'language_id' => $language->id,
            'name' => 'country1 test'
        ]);

        $state = factory(\App\Models\tenant\State::class)->create([
            'country_id' => $country->id,
            'iso3' => 'iso3 state1',
            'name' => 'name state1'
        ]);


        $country2 = factory(\App\Models\tenant\Country::class)->create([
            'continent_id' => $continent->id,
        ]);

        $countryTranslation2 = factory(\App\Models\tenant\CountryTranslation::class)->create([
            'country_id' => $country2->id,
            'language_id' => $language->id,
            'name' => 'country2 test'
        ]);

        $state2 = factory(\App\Models\tenant\State::class)->create([
            'country_id' => $country2->id,
            'iso3' => 'iso3 state2',
            'name' => 'name state2'
        ]);

        $this->json('GET','/api/v1/area/state?country_id='.$country->id)
            ->assertJsonFragment([
                'id' => $state->id,
                'country_id' => $state->country_id,
                'iso3' => $state->iso3,
                'name' => $state->name,
                'total_results' => 1,
            ]);

        $this->json('GET','/api/v1/area/state?country_id='.$country2->id)
            ->assertJsonFragment([
                'id' => $state2->id,
                'country_id' => $state2->country_id,
                'iso3' => $state2->iso3,
                'name' => $state2->name,
                'total_results' => 1,
            ]);
    }


    /**
     * Prueba automatizada, comprueba base de datos con campos actualizados y estructura
     */
    function testOk4(){

        $language = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);

        $continent = factory(\App\Models\tenant\Continent::class)->create([
            'iso2' => 'iso prueba'
        ]);

        $country = factory(\App\Models\tenant\Country::class)->create([
            'continent_id' => $continent->id,
            'iso3' => 'iso test',
            'order' => 2
        ]);

        $countryTranslation = factory(\App\Models\tenant\CountryTranslation::class)->create([
            'country_id' => $country->id,
            'language_id' => $language->id,
            'name' => 'country test'
        ]);

        $state = factory(\App\Models\tenant\State::class)->create([
            'country_id' => $country->id,
            'iso3' => 'iso test state',
            'name' => 'name state test'
        ]);

        $this->assertDatabaseHas('mo_language', [
            'id' => $language->id,
            'abbreviation' => $language->abbreviation
        ]);

        $this->assertDatabaseHas('mo_continent', [
            'id' => $continent->id,
            'iso2' => $continent->iso2
        ]);

        $this->assertDatabaseHas('mo_country', [
            'id' => $country->id,
            'continent_id' => $country->continent_id,
            'iso3' => $country->iso3,
            'order' => $country->order
        ]);

        $this->assertDatabaseHas('mo_country_translation', [
            'name' => $countryTranslation->name,
        ]);

        $this->assertDatabaseHas('mo_state', [
            'id' => $state->id,
            'iso3' => $state->iso3,
            'name' => $state->name
        ]);

        $this->json('GET', '/api/v1/area/state', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['state' => ["0" => ['id', 'name','iso3']]]]
            ]);

    }

}