<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class StatusShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *  Test consulta de estados de estados de comentarios
     */
    public function testBad1()
    {
        // No se especifica parametros
        $this->json('GET', '/api/v1/comment/status',[
            'lang' => 'esddddd'
        ])->assertExactJson(["error" =>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["lang"=>["The lang is not exists"]]]]);
    }

    /**
     *  Test consulta de estados de estados de comentarios
     */
    public function testOk1()
    {
        // No se especifica parametros
        $this->json('GET', '/api/v1/comment/status')->assertJsonFragment([
            "language_id" => 1,
            "name" => "Pendiente",
            "language_id" => 2,
            "name" => "Pending",
            "language_id" => 3,
            "name" => "Pendente",
            "language_id" => 1,
            "name" => "Validado",
            "language_id" => 2,
            "name" => "Validated",
            "language_id" => 3,
            "name" => "Validado",
            "language_id" => 1,
            "name" => "Rechazado",
            "language_id" => 2,
            "name" => "Rejected",
            "language_id" => 3,
            "name" => "Rejeitado"
        ]);
    }

    /**
     *  Test consulta de estados de comentarios enviando filtro lang
     */
    public function testOk2()
    {
        // No se especifica parametros
        $this->json('GET', '/api/v1/comment/status',[
            'lang' => 'es'
        ])->assertJsonFragment([
            "total_results" => 3,
        ]);
    }

}