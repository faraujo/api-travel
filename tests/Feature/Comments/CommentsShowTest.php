<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CommentsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *  Test consulta de comentarios sin parametros
     */
    public function testReadProducts()
    {
        // No se especifica parametros

        $this->json('GET', '/api/v1/comment')->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta comentarios con parametro lang
     */
    public function testLang()
    {
        //Se especifica parametro lang correctamente

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta comentarios con parametro lang en mayusculas
     */
    public function testLangUpper()
    {
        //Se especifica parametro lang correctamente en mayusculas

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta comentarios con parámetro lang en vacio
     */
    public function testLangEmpty()
    {

        $this->json('GET', '/api/v1/comment', [
            'lang' => '',
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta comentarios con parametro lengua mal
     */
    public function testLangBad()
    {
        // Se especifica parametro lang incorrectamente

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta comentarios con parametro product_id incorrecto
     */
    public function testProductIdBad()
    {
        // Se especifica incorrectamente parametro type

        $this->json('GET', '/api/v1/comment', [
            'product_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta comentarios con parametro product_id inexistente
     */
    public function testProductIdNotExists()
    {
        //Se especifica un product_id inexistente

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'es',
            'user_id' => 1,
            'product_id' => 999999999,
        ])->assertExactJson(["data" => [], "error" => 200, "total_results" => 0]);
    }


    /**
     * Test consulta comentarios con casi todos los parametros
     */
    public function testWithParameters()
    {
        // Se especifican correctamente parametros
        $this->json('GET', '/api/v1/comment', [
            'lang' => 'es',
            'user_id' => 1,
            'product_id' => 1,
            'rating' => 2
        ])->assertJsonStructure(['error', 'data']);
    }


    /**
     * Test consulta comentarios con algunos de los parametros mal
     */
    public function testWithSomeParametersBad()
    {
        // Se especifican incorrectamente parametros

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'es',
            'product_id' => 'esas',
            'user_id' => 'dfdf'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta comentarios con todos los parametros
     */
    public function testWithAllParameters()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'rating' => 2,
            'user_id' => $userFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'page' => 1,
            'limit' => 2
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

    /**
     * Test consulta comentarios con algunos parametros mal
     */
    public function testWithAllParametersSomeBad()
    {
        $userFactory = factory(App\Models\tenant\User::class)->create([]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'rating' => 'dfdf',
            'user_id' => $userFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'page' => 'rdd',
            'limit' => 2
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta comentarios usuario no existe
     */
    public function testUserBad()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $this->json('GET', '/api/v1/comment', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'rating' => 3,
            'user_id' => 999999999999999999,
            'page' => 1,
            'limit' => 2
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

    /**
     * Test validación nuevos filtros
     */
    public function testValidationWithNewFilters()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $commentFactory = factory(App\Models\tenant\Comment::class)->create([
            'comment_status_id' => 1,
            'product_id' => $productFactory->id,
            'created_at' => '2020-10-10 00:00:00'
        ]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'created_at' => '20-10-2018',
            'validated' => 11111,
        ])->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
            "error_inputs"=>[["created_at"=>["The created at does not match the format Y-m-d."],
                "validated"=>["The selected validated is invalid."]]]]);


    }


    //para descartar los demás comentarios que ya hay en base de datos, al crear el comentario le asigna un product_id de un factory que crea
    //y filtra luego siempre por ese product_id más el filtro que quiere poner a prueba
    /**
     * Test consulta comentarios con filtro validated
     */
    public function testWithNewFilterValidated()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $commentFactory = factory(App\Models\tenant\Comment::class)->create([
            'comment_status_id' => 1,
            'product_id' => $productFactory->id,
        ]);

        $commentFactory2 = factory(App\Models\tenant\Comment::class)->create([
            'comment_status_id' => 2,
            'product_id' => $productFactory->id,
        ]);

        $commentFactory3 = factory(App\Models\tenant\Comment::class)->create([
            'comment_status_id' => 3,
            'product_id' => $productFactory->id,
        ]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
        ])->assertJsonFragment(['total_results' => 3]);


        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'validated' => 1,
        ])->assertJsonFragment(['total_results' => 1]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'validated' => 2,
        ])->assertJsonFragment(['total_results' => 1]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'validated' => 3,
        ])->assertJsonFragment(['total_results' => 1]);
    }

    /**
     * Test consulta comentarios con filtro created_at
     */
    public function testWithNewFilterCreatedAt()
    {

        $CommentStatus = factory(App\Models\tenant\CommentStatus::class)->create([]);

        $CommentStatusTranslation1 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 1,
        ]);

        $CommentStatusTranslation2 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 2,
        ]);

        $CommentStatusTranslation3 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 3,
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $commentFactory = factory(App\Models\tenant\Comment::class)->create([
            'product_id' => $productFactory->id,
            'created_at' => '2020-10-10 00:00:00',
            'comment_status_id' => $CommentStatus->id,
        ]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'created_at' => '2020-10-10',
        ])->assertJsonFragment(['total_results' => 1]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'created_at' => '2020-10-11'
        ])->assertJsonFragment(['total_results' => 0]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'created_at' => '2020-10-09'
        ])->assertJsonFragment(['total_results' => 0]);

    }

    /**
     * Test consulta comentarios con filtro name
     */
    public function testWithNewFilterName()
    {
        $CommentStatus = factory(App\Models\tenant\CommentStatus::class)->create([]);

        $CommentStatusTranslation1 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 1,
        ]);

        $CommentStatusTranslation2 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 2,
        ]);

        $CommentStatusTranslation3 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 3,
        ]);

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'name' => 'alberto',
            'email' => 'email@viavox.com',
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);


        $commentFactory = factory(App\Models\tenant\Comment::class)->create([
            'product_id' => $productFactory->id,
            'user_id' => $userFactory->id,
            'comment_status_id' => $CommentStatus->id,
        ]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'name' => 'alb'
        ])->assertJsonFragment(['total_results' => 1]);

        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'name' => 'alv'
        ])->assertJsonFragment(['total_results' => 0]);


    }

    /**
     * Test consulta comentarios con filtro name busca en email
     */
    public function testFilterNameEmail()
    {
        $CommentStatus = factory(App\Models\tenant\CommentStatus::class)->create([]);

        $CommentStatusTranslation1 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 1,
        ]);

        $CommentStatusTranslation2 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 2,
        ]);

        $CommentStatusTranslation3 = factory(App\Models\tenant\CommentStatusTranslation::class)->create([
            'comment_status_id' => $CommentStatus->id,
            'language_id' => 3,
        ]);

        $userFactory = factory(App\Models\tenant\User::class)->create([
            'name' => 'alberto',
            'email' => 'email@viavox.com'
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);


        $commentFactory = factory(App\Models\tenant\Comment::class)->create([
            'product_id' => $productFactory->id,
            'user_id' => $userFactory->id,
            'comment_status_id' => $CommentStatus->id,
        ]);

        //filtro name encuentra coincidencia en name
        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'name' => 'alb'
        ])->assertJsonFragment(['total_results' => 1]);

        //filtro name encuentra coincidencia en email
        $this->json('GET', '/api/v1/comment', [
            'product_id' => $productFactory->id,
            'name' => 'viav'
        ])->assertJsonFragment(['total_results' => 1]);


    }


}