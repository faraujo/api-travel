<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CommentsCreateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test creacion de Comment
     */
    public function testRating()
    {
        $rating = factory(App\Models\tenant\Comment::class)->create([
            'text' => 'Texto creacion en pruebas',
            'rating' => 5,
        ]);

        $this->assertDatabaseHas('mo_comment', [
            'id' => $rating->id,
        ]);
    }

    /**
     * Prueba automatizada rating tipo de dato incorrecto
     */
    public function testRatingBadRating()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas comment mal',
            'rating' => 'casdf'
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'rating' => ['The rating must be a number.']
                ]
            ]
        ]);
    }

    /**
     * Prueba automatica sin rating
     */
    public function testRatingWithOutRating()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas producto_id mal',
            'lang' => 'ES',
            'product_id' => $productFactory->id
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'rating' => [
                        '0' => 'The rating field is required.'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Prueba automatica solo rating
     */
    public function testRatingOnlyRating()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'rating' => 4.5
        ])->assertExactJson([
            'error' => 200
        ]);
    }

    /**
     * Prueba automatizada idioma inexistente
     */
    public function testRatingBadLang()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas idioma mal ',
            'rating' => 5,
            'lang' => 'asdf'
        ])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Prueba Automatizada id producto inexistente
     */
    public function testRatingBadProduct()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas producto_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => 9999999999
        ])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['product_id' => ["The product is not exists"]]]
        ]);
    }

    /**
     * Prueba Automatizada error validador product
     */
    public function testRatingValidatorProduct()
    {
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas producto_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => 'asdfasf'
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'product_id' => [
                        'The product is not exists'
                    ],
                ]
            ]
        ]);
    }

    /**
     * Prueba Automatizada correcta
     */
    public function testRatingProduct()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas ',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id
        ])->assertExactJson([
            'error' => 200
        ]);
    }


    /**
     * Prueba automatica usuario inexistente en base de datos
     */
    public function testRatingBadUser()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas user_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
            'user_id' => 99999999999,
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'user_id' => [
                        '0' => 'The user is not exists'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Prueba automatica usuario error validador
     */
    public function testRatingBadUserFormat()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas user_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
            'user_id' => 'asdfasdf',
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'user_id' => [
                        'The user is not exists'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Prueba automatica valida
     */
    public function testRatingAll()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $userFactory = factory(\App\Models\tenant\User::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas user_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
            'user_id' => $userFactory->id,
        ])->assertExactJson([
            'error' => 200

        ]);
    }

    /**
     * Prueba automatizada sin canal
     */
    public function testChannelWithOutChannel()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $this->json('post', '/api/v1/comment', [
            'text' => 'Test creación de comentario sin canal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [

                    'subchannel_id' => ['The subchannel id field is required.']


                ]
            ]
        ]);
    }

    /**
     * Prueba automatizada canal inexistente
     */
    public function testChannelBadChannel()
    {
        $this->json('post', '/api/v1/comment', [
            'subchannel_id' => 9999999,
            'text' => 'Test creación de comentario con canal inexistente',
            'rating' => 5,
            'lang' => 'ES'
        ])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['subchannel_id' => ["The subchannel is not exists"]]]
        ]);
    }


}