<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CommentsDeleteTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test borrado sin enviar campo requerido
     */
    public function testBad1()
    {
        $this->json('delete', '/api/v1/comment', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => ['0' => ['ids' => ['0' => 'The ids field is required.']]]]);
    }

    /**
     * Test borrado enviando un string en campo id que es numérico
     */
    public function testBad2()
    {
        $this->json('delete', '/api/v1/comment', [
            'ids' => ['asas'],
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['ids.0' => ['The ids.0 must be an integer.']]]]);
    }

    /**
     *Test borrado con id de usuario inexistente en base de datos
     *
     *
     */
    public function testBad3()
    {
        $this->json('delete', '/api/v1/comment', [
            'ids' => [99999999],
        ])
            ->assertExactJson(["error"=>404,"error_description"=>"Data not found", "error_inputs" => [
                [
                    "ids.0" => ["The selected ids.0 is invalid."],
                ]
            ]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $commentFactory = factory(App\Models\tenant\Comment::class)->create([
        ]);

        $this->assertDatabaseHas('mo_comment', [
            'id' => $commentFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/comment', [
            'ids' => $commentFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }




    /**
     * Test borrado de un comment
     */
    public function testOk1()
    {
        $rating = factory(App\Models\tenant\Comment::class)->create([
            'text' => 'Texto creacion en pruebas',
            'rating' => 5,
        ]);

        $this->assertDatabaseHas('mo_comment', [
            'id' => $rating->id,
        ]);

        $this->json('delete', '/api/v1/comment', [
            'ids' => [$rating->id],
        ])->assertJson(['error' => 200]);

        //comprueba campo deleted_at de comment no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_comment', ['deleted_at' => null, 'id' => $rating->id]);

    }

    /**
     * Test borrado de tres comment
     */
    public function testOk2()
    {
        $rating1 = factory(App\Models\tenant\Comment::class)->create([
            'text' => 'Texto creacion en pruebas',
            'rating' => 5,
        ]);

        $rating2 = factory(App\Models\tenant\Comment::class)->create([
            'text' => 'Texto creacion en pruebas2',
            'rating' => 4,
        ]);

        $rating3 = factory(App\Models\tenant\Comment::class)->create([
            'text' => 'Texto creacion en pruebas3',
            'rating' => 8,
        ]);


        $this->assertDatabaseHas('mo_comment', [
            'id' => $rating1->id,
        ]);

        $this->assertDatabaseHas('mo_comment', [
            'id' => $rating2->id,
        ]);

        $this->assertDatabaseHas('mo_comment', [
            'id' => $rating3->id,
        ]);

        $this->json('delete', '/api/v1/comment', [
            'ids' => [$rating1->id,$rating2->id,$rating3->id],
        ])->assertJson(['error' => 200]);

        //comprueba campo deleted_at de los comment no se ve en base de datos como nulo
        $this->assertDatabaseMissing('mo_comment', ['deleted_at' => null, 'id' => $rating1->id]);
        $this->assertDatabaseMissing('mo_comment', ['deleted_at' => null, 'id' => $rating2->id]);
        $this->assertDatabaseMissing('mo_comment', ['deleted_at' => null, 'id' => $rating3->id]);

    }

}