<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CommentsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta producto existente sin parametros
     */
    public function testCommentRead()
    {

        $comment = factory(App\Models\tenant\Comment::class)->create([
            'comment_status_id' => 1,
        ]);

        $this->json('GET', '/api/v1/comment/'.$comment->id)
            ->assertJsonFragment(['id' => $comment->id]);
    }

    /**
     * Prueba automatizada comentario no existente en base de datos
     */
    public function testCommentBad()
    {
        $this->json('GET', '/api/v1/comment/999999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Prueba automatizada id comentario mal
     */
    /*public function testCommentIdBad()
    {
        $this->json('GET', '/api/v1/comment/malo')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }*/

    /**
     * Prueba Automatizada commentario borrado
     */
    public function testCommentDeleted()
    {
        $rating = factory(App\Models\tenant\Comment::class)->create([
            'text' => 'Texto creacion en pruebas',
            'rating' => 5,
        ]);

        //Borramos el primer comentario
        $this->json('DELETE', '/api/v1/comment', [
            'ids' => [$rating->id]
        ]);
        // Fin del borrado

        $this->json('GET', '/api/v1/comment/'.$rating->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }


}
