<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Carbon\Carbon;

class CommentsCreateSearchTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada idioma inexistente
     */
    public function testBad1()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);

        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas idioma mal ',
            'rating' => 5,
            'lang' => 'asdf',
            'user_id' => $user_factory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Prueba Automatizada id producto inexistente
     */
    public function testBad2()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);

        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas producto_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => 9999999999,
            'user_id' => $user_factory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['product_id' => ["The product is not exists"]]]
        ]);
    }

    /**
     * Prueba Automatizada error validador product
     */
    public function testBad3()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas producto_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => 'asdfasf',
            'user_id' => $user_factory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'product_id' => [
                        'The product is not exists'
                    ],
                ]
            ]
        ]);
    }

    /**
     * Prueba automatica token no válido
     */
    public function testBad4()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas user_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
        ], [
            'Authorization' => 'Bearer asdasdsadsad',
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'Authorization' => [
                        '0' => 'The authorization token is not valid'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Prueba automatica usuario error validador
     */
    public function testBad5()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas user_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
            'user_id' => 'asdfasdf',
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [
                    'user_id' => [
                        'The user id must be a number.'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Prueba automatizada sin canal
     */
    public function testBad6()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $this->json('post', '/api/v1/comment/search', [
            'text' => 'Test creación de comentario sin canal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
            'user_id' => $user_factory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [
                [

                    'subchannel_id' => ['The subchannel id field is required.']


                ]
            ]
        ]);
    }

    /**
     * Prueba automatizada canal inexistente
     */
    public function testBad7()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => 9999999,
            'text' => 'Test creación de comentario con canal inexistente',
            'rating' => 5,
            'lang' => 'ES',
            'user_id' => $user_factory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['subchannel_id' => ["The subchannel is not exists"]]]
        ]);
    }

    /**
     * Prueba automatizada sin token
     */
    public function testBad8()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => 9999999,
            'text' => 'Test creación de comentario con canal inexistente',
            'rating' => 5,
            'lang' => 'ES'
        ])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['subchannel_id' => ["The subchannel is not exists"], "Authorization"=>["The authorization token is required"]]]
        ]);
    }

    /**
     * Test creacion de Comment
     */
    public function testOk1()
    {

        $user_factory = factory(\App\Models\tenant\User::class)->create([]);

        $rating = factory(App\Models\tenant\Comment::class)->create([
            'text' => 'Texto creacion en pruebas',
            'rating' => 5,
            'user_id' => $user_factory->id,
        ]);

        $this->assertDatabaseHas('mo_comment', [
            'id' => $rating->id,
        ]);
    }

    /**
     * Prueba automatica solo rating
     */
    public function testOk2()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);        
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'rating' => 4.5,
            'user_id' => $user_factory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 200
        ]);
    }

    /**
     * Prueba Automatizada correcta
     */
    public function testOk3()
    {
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas ',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
            'user_id' => $user_factory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 200
        ]);
    }

    /**
     * Prueba automatica valida
     */
    public function testOk4()
    {
        $productFactory = factory(\App\Models\tenant\Product::class)->create([]);
        $subchannelFactory = factory(\App\Models\tenant\SubChannel::class)->create([]);
        $user_factory = factory(\App\Models\tenant\User::class)->create([]);
        $userTokenFactory = factory(App\Models\tenant\UserToken::class)->create([
            'user_id' => $user_factory->id,
            'token' => 'asdasdsadsad',
            'expired_at' => Carbon::now()->addDay()
        ]);
        $this->assertDatabaseHas('mo_user_token',['token' => $userTokenFactory->token]);
        $this->json('post', '/api/v1/comment/search', [
            'subchannel_id' => $subchannelFactory->id,
            'text' => 'Texto Creacion en pruebas user_id mal',
            'rating' => 5,
            'lang' => 'ES',
            'product_id' => $productFactory->id,
        ], [
            'Authorization' => 'Bearer ' . $userTokenFactory->token,
        ])->assertExactJson([
            'error' => 200

        ]);
    }

}