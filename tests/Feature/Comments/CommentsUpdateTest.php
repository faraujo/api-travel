<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CommentsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *  Prueba automatizada sin parametros
     */
    public function testValidarWithoutParameters()
    {
        $this->json('put', '/api/v1/comment', [])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format",
                "error_inputs"=>[["comment_status_id"=>["The comment_status_id is not exists"],"id"=>["The id field is required."],
                    "updated_by"=>["The user_id is not exists"]]]]);
    }

    /**
     *  Prueba automatizada id frormato invalido
     */
    public function testValidatorWithInvalidId()
    {
        $userFactory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('put', '/api/v1/comment', [
            'id' => 'asdf',
            'updated_by' => $userFactory->id,
            'comment_status_id' => 2,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' =>
                ['0' => [
                    'id' => [
                        'The id must be a number.'

                    ]
                ]
                ]
            ]);
    }

    /**
     *  Prueba automatizada id frormato invalido
     */
    public function testValidatorWithInvalidStatus()
    {
        $userFactory = factory(\App\Models\tenant\User::class)->create([]);
        $commentFactory = factory(\App\Models\tenant\Comment::class)->create([]);

        $this->json('put', '/api/v1/comment', [
            'id' => $commentFactory->id,
            'updated_by' => $userFactory->id,
            'comment_status_id' => 4444444444444,
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' =>
                ['0' => [
                    "comment_status_id"=>["The comment_status_id is not exists"

                    ]
                ]
                ]
            ]);
    }

    /**
     *  Prueba automatizada id inexistente
     */
    public function testValidatorWithBadId()
    {
        $userFactory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('put', '/api/v1/comment', [
            'id' => 99999999999999999999999999,
            'updated_by' => $userFactory->id,
            'comment_status_id' => 3,
        ])
            ->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     *  Prueba automatizada parametros correctos
     */
    public function testValidated()
    {
        $userFactory = factory(\App\Models\tenant\User::class)->create([]);
        $commentFactory = factory(\App\Models\tenant\Comment::class)->create([]);
        $this->json('put', '/api/v1/comment', [
            'id' => $commentFactory->id,
            'updated_by' => $userFactory->id,
            'comment_status_id' => 2,
        ])
            ->assertExactJson(['error' => 200]);
    }

    /**
     *  Prueba automatizada comment_status_id inexistente
     */
    public function testValidatorWithBadCommentStatus()
    {
        $userFactory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('put', '/api/v1/comment', [
            'id' => 99999999999999999999999999,
            'updated_by' => $userFactory->id,
            'comment_status_id' => 3333333,
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["comment_status_id"=>["The comment_status_id is not exists"]]]]);
    }

    /**
     *  Prueba automatizada comment_status_id en formato erroneo
     */
    public function testValidatorWithCommentStatusBadFormat()
    {
        $userFactory = factory(\App\Models\tenant\User::class)->create([]);

        $this->json('put', '/api/v1/comment', [
            'id' => 99999999999999999999999999,
            'updated_by' => $userFactory->id,
            'comment_status_id' => 'rrr',
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["comment_status_id"=>["The comment_status_id is not exists"]]]]);
    }
}