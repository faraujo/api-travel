<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class CommentsShowSearchTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *  Test consulta de comentarios sin parametros
     */
    public function testReadCommentsNoProductId()
    {
        // No se especifica parametros

        $this->json('GET', '/api/v1/comment/search')
            ->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     *  Test consulta de comentarios con parametro obligatorio
     */
    public function testReadCommentsWithProductId()
    {
        // No se especifica parametros

        $this->json('GET', '/api/v1/comment/search', [
        'product_id' => 1
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }


    /**
     * Test consulta comentarios con parametro product_id incorrecto
     */
    public function testProductIdBad()
    {
        // Se especifica incorrectamente parametro type

        $this->json('GET', '/api/v1/comment/search', [
            'product_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta comentarios con parámetro product_id en vacio
     */
    public function testProductIdEmpty()
    {

        $this->json('GET', '/api/v1/comment/search', [
            'product_id' => '',
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta comentarios con parametro lang
     */
    public function testLang()
    {
        //Se especifica parametro lang correctamente

        $this->json('GET', '/api/v1/comment/search', [
            'product_id' => 1,
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta comentarios con parametro lang en mayusculas
     */
    public function testLangUpper()
    {
        //Se especifica parametro lang correctamente en mayusculas

        $this->json('GET', '/api/v1/comment/search', [
            'product_id' => 1,
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta comentarios con parámetro lang en vacio
     */
    public function testLangEmpty()
    {

        $this->json('GET', '/api/v1/comment/search', [
            'product_id' => 1,
            'lang' => '',
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta comentarios con parametro lengua mal
     */
    public function testLangBad()
    {
        // Se especifica parametro lang incorrectamente

        $this->json('GET', '/api/v1/comment/search', [
            'product_id' => 1,
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta comentarios con parametro product_id inexistente
     */
    public function testProductIdNotExists()
    {
        //Se especifica un product_id inexistente

        $this->json('GET', '/api/v1/comment/search', [
            'lang' => 'es',
            'user_id' => 1,
            'product_id' => 999999999,
        ])->assertExactJson(["data" => [], "error" => 200, "total_results" => 0]);
    }

    /**
     * Test consulta comentarios con todos los parametros
     */
    public function testWithAllParameters()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('GET', '/api/v1/comment/search', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'page' => 1,
            'limit' => 2
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

    /**
     * Test consulta comentarios con subchannel_id mal
     */
    public function testWithAllParametersSubchannelBad()
    {


        $productFactory = factory(App\Models\tenant\Product::class)->create([]);


        $this->json('GET', '/api/v1/comment/search', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'subchannel_id' => 'asdad',
            'page' => 1,
            'limit' => 2
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }


    /**
     * Test consulta comentarios con página mal
     */
    public function testWithAllParametersPageBad()
    {


        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);

        $this->json('GET', '/api/v1/comment/search', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'page' => 'rdd',
            'limit' => 2
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }



    /**
     * Test consulta comentarios con límite de paginación mal
     */
    public function testWithAllParametersLimitBad()
    {


        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $subchannelFactory = factory(App\Models\tenant\SubChannel::class)->create([]);


        $this->json('GET', '/api/v1/comment/search', [
            'lang' => 'es',
            'product_id' => $productFactory->id,
            'subchannel_id' => $subchannelFactory->id,
            'page' => 1,
            'limit' => 'dsdsf'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

}