<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContractsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de contratos con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/contract', [
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."]]]]);
    }

    /**
     * test consulta de contrato con parametro lang incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/contract', [

            'lang' => 'eeeeeeee',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["lang" => ["The lang is not exists"]]]]);
    }

    /**
     * test crea factory de contrato con traducciones y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        //Factory de contrato y sus traducciones
        $contractFactory1 = factory(App\Models\tenant\ContractModel::class)->create([]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contractFactory1->id,
        ]);

        $contractTranslationFactory1 = factory(App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => 1,
            'contract_model_id' => $contractFactory1->id,
            'name' => 'name español test ',

        ]);

        $contractTranslationFactory2 = factory(App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => 2,
            'contract_model_id' => $contractFactory1->id,
            'name' => 'name ingles test ',

        ]);

        $contractTranslationFactory3 = factory(App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => 3,
            'contract_model_id' => $contractFactory1->id,
            'name' => 'name portugues test ',

        ]);

        $this->assertDatabaseHas('mo_contract_model_translation', [
            'language_id' => $contractTranslationFactory1->language_id,
            'contract_model_id' => $contractFactory1->id,
            'name' => $contractTranslationFactory1->name,

        ]);

        $this->assertDatabaseHas('mo_contract_model_translation', [
            'language_id' => $contractTranslationFactory2->language_id,
            'contract_model_id' => $contractFactory1->id,
            'name' => $contractTranslationFactory2->name,

        ]);

        $this->assertDatabaseHas('mo_contract_model_translation', [
            'language_id' => $contractTranslationFactory3->language_id,
            'contract_model_id' => $contractFactory1->id,
            'name' => $contractTranslationFactory3->name,

        ]);


        $this->json('GET', '/api/v1/contract?limit=0', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory1->id,
                'name' => $contractTranslationFactory1->name,
                'name' => $contractTranslationFactory2->name,
                'name' => $contractTranslationFactory3->name,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/contract', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['contract' => ["0" => ['id', 'lang']]]]
            ]);

    }

    /**
     * test ok con filtro lang de idioma
     */
    public function testOk2(){
        $language_factory = factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST',
        ]);

        $contract_model_factory = factory(\App\Models\tenant\ContractModel::class)->create([]);

        factory(\App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => $language_factory->id,
            'contract_model_id' => $contract_model_factory->id,
        ]);

        $this->json('GET','/api/v1/contract', [
            'lang' => 'TEST',
        ])->assertJsonFragment(['id' => $contract_model_factory->id]);
    }


}