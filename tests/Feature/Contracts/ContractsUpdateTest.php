<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContractsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada incorrecta enviando peticion sin parametros requeridos
     */
    function testBad1(){

        $this->json('PUT', '/api/v1/contract', [])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                'id' => ['The id field is required.'],
                "translation" => ["you need at least one translation"],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametros con tipos incorrectos
     */

    function testBad2(){

        $this->json('PUT', '/api/v1/contract', [
            'id' => 'test',
            'time_lock_avail' => 'test',
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                'time_lock_avail' => ['The time lock avail must be an integer.'],
                "translation" => ["you need at least one translation"],
                'id' => ['The id must be an integer.']
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametro negativo
     */
    function testBad3(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $contract = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $contractTranslation = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => $language->id,
            'contract_model_id' => $contract->id,
            'name' => 'nombre test de tipo de cliente'
        ]);

        $this->json('PUT', '/api/v1/contract', [
            'time_lock_avail' => -100,
            'id' => $contract->id,
            'name' => [$language->abbreviation => $contractTranslation->name],
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                "time_lock_avail" => ["The time lock avail must be at least 0."],
            ]]]);
    }

    /**
     * Test que comprueba el funcionamiento de las validaciones
     *
     * Se envía contrato a modificar no existente
     */
    function testBad4(){

        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);

        $this->json('PUT', '/api/v1/contract', [
            'id' => 999999999999,
            'name' => [$language->abbreviation => 'Name español'],
        ])
        ->assertExactJson(["error" => 404, "error_description" => "Data not found"]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $contract = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $contractTranslation = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => $language->id,
            'contract_model_id' => $contract->id,
        ]);

        $this->json('PUT', '/api/v1/contract', [
            'time_lock_avail' => 100,
            'id' => $contract->id,
            'name' => [$language->abbreviation => $contractTranslation->name],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_contract_model', [
            'time_lock_avail' => 100,
            'id' => $contract->id, 
            'deleted_at' => null
        ]);
        $this->assertDatabaseHas('mo_contract_model_translation', ['name' => $contractTranslation->name, 'language_id' => $language->id, 'contract_model_id' => $contract->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada correcta comprueba el borrado de idiomas no actualizados
     */
    function testOk2(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $language2 = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test2']);

        //Generación de contrato por factory, con traducción en dos idiomas
        $contract = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $contractTranslation1 = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contract->id,
            'language_id' => $language->id,
            'name' => 'nombre test de contrato idioma 1'
        ]);
        $contractTranslation2 = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contract->id,
            'language_id' => $language2->id,
            'name' => 'nombre test de contrato idioma 2'
        ]);

        //Actualización de contrato enviando una única traducción
        $this->json('PUT', '/api/v1/contract', [
            'time_lock_avail' => 10,
            'id' => $contract->id,
            'name' => [$language->abbreviation => $contractTranslation1->name],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_contract_model', [
            'time_lock_avail' => 10,
            'id' => $contract->id, 
            'deleted_at' => null
        ]);
        $this->assertDatabaseHas('mo_contract_model_translation', ['name' => $contractTranslation1->name, 'contract_model_id' => $contract->id, 'language_id' => $language->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_contract_model_translation', ['name' => $contractTranslation2->name, 'contract_model_id' => $contract->id, 'language_id' => $language2->id, 'deleted_at' => null]);
    }

    /**
     * Prueba automatizada correcta, intenta actualizar traduccion de contrato inextistente, debe crearla y borrar la traducción que de otro idioma que existiera
     */
    function testOk3(){
        $language = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test']);
        $language2 = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'test2']);

        $contract = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $contractTranslation = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'contract_model_id' => $contract->id,
            'language_id' => $language->id,
            'name' => 'name español test 3',
        ]);

        $this->assertDatabaseHas('mo_contract_model', ['time_lock_avail' => $contract->time_lock_avail,'id' => $contract->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_contract_model_translation', ['name' => $contractTranslation->name, 'language_id' => $language->id,'contract_model_id' => $contract->id, 'deleted_at' => null]);

        $this->json('PUT', '/api/v1/contract', [
            'time_lock_avail' => 30,
            'id' => $contract->id,
            'name' => [$language2->abbreviation => 'name english test 3'],
        ])
        ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_contract_model', ['time_lock_avail' => 30,'id' => $contract->id, 'deleted_at' => null]);
        $this->assertDatabaseHas('mo_contract_model_translation', ['name' => 'name english test 3', 'contract_model_id' => $contract->id, 'language_id' => $language2->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing('mo_contract_model_translation', ['name' => $contractTranslation->name, 'contract_model_id' => $contract->id, 'language_id' => $language->id, 'deleted_at' => null]);

    }

}