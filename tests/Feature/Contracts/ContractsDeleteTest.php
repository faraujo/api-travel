<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContractsDeleteTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin campo id requerido
     */
    function testBad1(){
        $this->json('DELETE','/api/v1/contract',[])
            ->assertExactJson(['error'=>400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'ids' => ['The ids field is required.']
        ]]]);
    }

    /**
     * Prueba automatizada campo id en formato incorrecto
     */
    function testBad2(){
        $this->json('DELETE','/api/v1/contract', [
            'ids' => ['TEST']
        ])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'ids.0' => ['The ids.0 must be an integer.']
            ]]]);
    }

    /**
     * Prueba automatizada campo id inexistente en base de datos
     */
    function testBad3(){
        $this->json('DELETE','/api/v1/contract', [
            'ids' => [99999999999999999]
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found', "error_inputs" => [["ids.0" => ["The selected ids.0 is invalid."]]]]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contractFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/contract', [
            'ids' => $contractFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }

    /**
     * Prueba automatizada campo id existente en base de datos
     */
    function testOk1(){
        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);
        $contractFactory = factory(\App\Models\tenant\ContractModel::class)->create([]);
        $contractTranslation = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => $languageFactory->id,
            'contract_model_id' => $contractFactory->id,
        ]);

        $this->json('DELETE','/api/v1/contract', [
            'ids' => [$contractFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_contract_model', [
            'id' => $contractFactory->id,
            'time_lock_avail' => $contractFactory->time_lock_avail,
            'deleted_at' => null
        ]);

        $this->assertDatabaseMissing('mo_contract_model_translation', [
            'contract_model_id' => $contractFactory->id,
            'deleted_at' => null
        ]);
    }
   
}