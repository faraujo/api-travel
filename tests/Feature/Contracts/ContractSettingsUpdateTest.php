<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContractSettingsUpdateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada sin parámetros requeridos
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/price/contract/settings', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "id" => ["The id field is required."]]]]);
    }

    /**
     * Prueba automatizada formato parámetro products array incorrecto
     */
    public function testBad2()
    {
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            //no manda formato array
            'products' => $product->id,
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "products" => ["The products must be an array."],
            ]]]);
    }

    /**
     * Prueba automatizada formato parámetros de configuración incorrectos
     */
    public function testBad3()
    {
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            //no manda formato array
            'products' => [0 => ['id' => 'string', 'time_expiration' => 'string', 'apply_cancel_penalization' => 'string', 'individual_sale' => 'string', 'required_service' => 'string']],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["products.0.apply_cancel_penalization" => ["The products.0.apply_cancel_penalization must be an integer."],
                    "products.0.id" => ["The products.0.id must be an integer."], "products.0.individual_sale" => ["The products.0.individual_sale field must be true or false."],
                    "products.0.time_expiration" => ["The products.0.time_expiration must be an integer."],
                    "products.0.required_service" => ["The products.0.required_service field must be true or false."]]]]);
    }

    /**
     * Prueba automatizada envía requeridos y un product_id para asociar al contrato pero no envía ninguno de los campos de configuración,
     * al menos uno es requerido cuando envían un producto
     */
    public function testBad4()
    {
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            //no manda formato array
            'products' => [0 => ['id' => $product->id]],
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [[
                    "products.0.individual_sale" => ["The products.0.individual_sale field is required when none of products.0.time_limit_enjoyment / products.0.apply_cancel_penalization / products.0.time_expiration are present."],
                    "products.0.time_expiration" => ["The products.0.time_expiration field is required when none of products.0.time_limit_enjoyment / products.0.apply_cancel_penalization / products.0.individual_sale are present."],
                    "products.0.time_limit_enjoyment" => ["The products.0.time_limit_enjoyment field is required when none of products.0.time_expiration / products.0.apply_cancel_penalization / products.0.individual_sale are present."]]]]);
    }


    /**
     * Prueba automatizada enviando parametro services en formato incorrecto
     */
    public function testBad5()
    {
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'services' => 'Test'
                ]
            ],
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [[
                'products.0.services' => ['The products.0.services must be an array.'],
            ]]
        ]);
    }

    /**
     * Prueba automatizada enviando array servicios sin campos requeridos
     */
    public function testBad6(){
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'services' => [0=> []]
                ]
            ],
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [[
                "products.0.services.0.id"=>["The products.0.services.0.id field is required."],
                "products.0.services.0.individual_sale"=>["The products.0.services.0.individual_sale field is required when none of products.0.services.0.time_limit_enjoyment / products.0.services.0.apply_cancel_penalization / products.0.services.0.time_expiration are present."],
                "products.0.services.0.time_expiration"=>["The products.0.services.0.time_expiration field is required when none of products.0.services.0.time_limit_enjoyment / products.0.services.0.apply_cancel_penalization / products.0.services.0.individual_sale are present."],
                "products.0.services.0.time_limit_enjoyment"=>["The products.0.services.0.time_limit_enjoyment field is required when none of products.0.services.0.time_expiration / products.0.services.0.apply_cancel_penalization / products.0.services.0.individual_sale are present."]
            ]]
        ]);
    }

    /**
     * Prueba automatizada enviando parametros de servicio en formato incorrecto
     */
    public function testBad7(){
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'services' => [0=> [
                        'id' => 'Test',
                        'individual_sale' => 'Test',
                        'time_expiration' => 'Test',
                        'time_limit_enjoyment' => 'Test',
                        'apply_cancel_penalization' => 'Test'
                    ]]
                ]
            ],
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [[
                "products.0.services.0.id"=>["The products.0.services.0.id must be an integer."],
                "products.0.services.0.apply_cancel_penalization"=>["The products.0.services.0.apply_cancel_penalization must be an integer."],
                "products.0.services.0.individual_sale"=>["The products.0.services.0.individual_sale field must be true or false."],
                "products.0.services.0.time_expiration"=>["The products.0.services.0.time_expiration must be an integer."],
                "products.0.services.0.time_limit_enjoyment"=>["The products.0.services.0.time_limit_enjoyment must be an integer."]
            ]]
        ]);
    }

    /**
     * Prueba automatizada enviando id de servicio no existente en base de datos
     */
    public function testBad8 (){
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'services' => [0=> [
                        'id' => 99999999,
                        'individual_sale' => '0',
                    ]]
                ]
            ],
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [[
                "products.0.services.0.id"=>["The selected products.0.services.0.id is invalid."],
            ]]
        ]);
    }

    /**
     * Prueba automatizada enviando apply_cancel_penalization sin sus datos requereidos days y penalization_amount
     */
    public function testBad9 (){
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $service = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'apply_cancel_penalization' => 1,
                    'services' => [0=> [
                        'id' => $service->id,
                        'individual_sale' => '0',
                        'apply_cancel_penalization' => 1,
                    ]]
                ]
            ],
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [[
                "products.0.apply_cancel_penalization"=>["If apply_cancel_penalization is 1, the penalization fields must be filled"],
            ]]
        ]);
    }

    /**
     * Prueba automatizada enviando apply_cancel_penalization sin sus datos requereidos days y penalization_amount
     */
    public function testBad10 (){
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $service = factory(App\Models\tenant\Service::class)->create([
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'apply_cancel_penalization' => 1,
                    'penalizations' => [
                        ['days' => '',
                        'penalization_amount' => '']
                    ],
                    'services' => [0=> [
                        'id' => $service->id,
                        'individual_sale' => '0',
                        'apply_cancel_penalization' => 1,
                        'penalizations' => [
                            ['days' => '',
                            'penalization_amount' => '']
                        ],
                    ]]
                ]
            ],
        ])->assertExactJson([
            'error' => 400,
            'error_description' => 'The fields are not the required format',
            'error_inputs' => [[
                "products.0.penalizations.0.days"=>["The products.0.penalizations.0.days field is required when products.0.apply_cancel_penalization is 1."],
                "products.0.penalizations.0.penalization_amount"=>["The products.0.penalizations.0.penalization_amount field is required when products.0.apply_cancel_penalization is 1."],
                "products.0.services.0.penalizations.0.days"=>["The products.0.services.0.penalizations.0.days field is required when products.0.services.0.apply_cancel_penalization is 1."],
                "products.0.services.0.penalizations.0.penalization_amount"=>["The products.0.services.0.penalizations.0.penalization_amount field is required when products.0.services.0.apply_cancel_penalization is 1."],
            ]]
        ]);
    }

    /**
     * Prueba automatizada actualiza campo time_lock_avail de tabla contratos y ningún campo de tabla intermedia de contratos y productos
     */
    public function testOk1()
    {
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 5,
        ]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contract->id,
            'time_lock_avail' => 5,
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'time_lock_avail' => 10,
        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contract->id,
            'time_lock_avail' => 10,
        ]);
    }

    /**
     * Prueba automatizada borra registros de tabla intermedia y crea nuevos con los datos enviados
     */
    public function testOk2()
    {
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 5,
        ]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contract->id,
            'time_lock_avail' => $contract->time_lock_avail,
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $product2 = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $product->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $product2->id,
        ]);

        $contract_product = factory(App\Models\tenant\ContractModelProduct::class)->create([
            'contract_model_id' => $contract->id,
            'product_id' => $product->id,
            'apply_cancel_penalization' => 0,
            'individual_sale' => 0,
            'time_expiration' => 5,
            'time_limit_enjoyment' => 5,
            'required_service' => 0
        ]);

        $this->assertDatabaseHas('mo_contract_model_product', [
            'id' => $contract_product->id,
            'contract_model_id' => $contract_product->contract_model_id,
            'product_id' => $contract_product->product_id,
            'apply_cancel_penalization' => $contract_product->apply_cancel_penalization,
            'individual_sale' => $contract_product->individual_sale,
            'time_expiration' => $contract_product->time_expiration,
            'time_limit_enjoyment' => $contract_product->time_limit_enjoyment,
            'required_service' => 0
        ]);

        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'time_lock_avail' => 10,
            'products' => [0 => [
                'id' => $product->id,
                'apply_cancel_penalization' => 0,
                'individual_sale' => 1,
                'time_expiration' => 10,
                'time_limit_enjoyment' => 10],
                1 => [
                    'id' => $product2->id,
                    'apply_cancel_penalization' => 0,
                    'individual_sale' => 0,
                    'time_expiration' => 20,
                    'time_limit_enjoyment' => 20,
                    'required_service' => 1]],

        ])
            ->assertExactJson(["error" => 200]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contract->id,
            'time_lock_avail' => 10,
        ]);

        //comprueba que los registros para el contrato enviado se borran
        $this->assertDatabaseMissing('mo_contract_model_product', [
            'id' => $contract_product->id,
            'contract_model_id' => $contract_product->contract_model_id,
            'product_id' => $contract_product->product_id,
            'apply_cancel_penalization' => $contract_product->apply_cancel_penalization,
            'individual_sale' => $contract_product->individual_sale,
            'time_expiration' => $contract_product->time_expiration,
            'time_limit_enjoyment' => $contract_product->time_limit_enjoyment,
            'required_service' => 0,
            'deleted_at' => null
        ]);

        //comprueba nueva creacion de registros
        $this->assertDatabaseHas('mo_contract_model_product', [
            'contract_model_id' => $contract->id,
            'product_id' => $product->id,
            'apply_cancel_penalization' => 0,
            'individual_sale' => 1,
            'time_expiration' => 10,
            'time_limit_enjoyment' => 10,
        ]);

        $this->assertDatabaseHas('mo_contract_model_product', [
            'contract_model_id' => $contract->id,
            'product_id' => $product2->id,
            'apply_cancel_penalization' => 0,
            'individual_sale' => 0,
            'time_expiration' => 20,
            'time_limit_enjoyment' => 20,
            'required_service' => 1,
        ]);
    }

    /**
     * Prueba automatizada enviando id de servicio e individual sale a 0
     */
    public function testOk3 (){
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $service = factory(App\Models\tenant\Service::class)->create([
        ]);


        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'services' => [0=> [
                        'id' => $service->id,
                        'individual_sale' => '0',
                    ]]
                ]
            ],
        ])->assertExactJson([
            'error' => 200,
        ]);
    }

    public function testOk4(){
        $contract = factory(App\Models\tenant\ContractModel::class)->create([
        ]);

        $product = factory(App\Models\tenant\Product::class)->create([
        ]);

        $service = factory(App\Models\tenant\Service::class)->create([
        ]);


        $this->json('PUT', '/api/v1/price/contract/settings', [
            'id' => $contract->id,
            'products' => [
                0 => [
                    'id' => $product->id,
                    'individual_sale' => 0,
                    'apply_cancel_penalization' => 1,
                    'penalizations' => [
                        ['days' => 1234,
                        'penalization_amount' => 1234,]
                    ],
                    'services' => [0 => [
                        'id' => $service->id,
                        'individual_sale' => 1,
                        'time_expiration' => 10,
                        'time_limit_enjoyment' => 20,
                        'apply_cancel_penalization' => 1,
                        'penalizations' => [

                            ['days' => 5678,
                        'penalization_amount' => 5678]
                            ]
                    ]]
                ]
            ],
        ])->assertExactJson([
            'error' => 200,
        ]);


        $this->assertDatabaseHas('mo_contract_model_product', [
            'contract_model_id' => $contract->id,
            'product_id' => $product->id,
            'service_id' => $service->id,
            'apply_cancel_penalization' => 1,
            'individual_sale' => 1,
            'time_expiration' => 10,
            'time_limit_enjoyment' => 20,
        ]);

        $this->assertDatabaseHas('mo_contract_model_penalization', [
            'days' => 5678,
            'penalization_amount' => 5678,
            'deleted_at' => null,
        ]);
        $this->assertDatabaseHas('mo_contract_model_penalization', [
            'days' => 1234,
            'penalization_amount' => 1234,
            'deleted_at' => null,
        ]);
    }
}