<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContractsCreateTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Prueba automatizada incorrecta enviando peticion sin parametros requeridos
     */
    function testBad1(){
        $this->json('POST', '/api/v1/contract', [])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                "translation" => ["you need at least one translation"],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametros con tipos incorrectos y sin parametros requeridos
     */

    function testBad2(){
        $this->json('POST', '/api/v1/contract', [
            'time_lock_avail' => 'test',
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                'time_lock_avail' => ['The time lock avail must be an integer.'],
                "translation" => ["you need at least one translation"],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando parametro negativo
     */
    function testBad3(){
        $this->json('POST', '/api/v1/contract', [
            'time_lock_avail' => -3,
            'name' => ['ES' => 'test'],
        ])
            ->assertExactJson(['error' => 400, 'error_description'=>"The fields are not the required format", 'error_inputs' => [[
                "time_lock_avail" => ["The time lock avail must be at least 0."],
            ]]]);
    }

    /**
     * Prueba automatizada incorrecta enviando name con lang incorrecta
     */
    function testBad4(){
        $this->json('POST', '/api/v1/contract', [
            'time_lock_avail' => 60,
            'name' => ['bad' => 'test'],
        ])
            ->assertExactJson(["error"=>400,"error_description"=>"The fields are not the required format","error_inputs"=>[["translation"=>["you need at least one translation"]]]]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){

        $languageFactory = factory(\App\Models\tenant\Language::class)->create(['abbreviation' => 'TEST']);
        $this->json('POST', '/api/v1/contract', [
            'time_lock_avail' => 100,
            'name' => [$languageFactory->abbreviation => 'test'],
        ])
            ->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_contract_model', [
            'time_lock_avail' => 100,
        ]);

        $this->assertDatabaseHas('mo_contract_model_translation', [
            'language_id' => $languageFactory->id,
            'name' => 'test',
        ]);
    }
}