<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContractsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta id de contrato no existente en base de datos
     */
    function testBad1(){
        $this->json('GET', '/api/v1/contract/999999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Prueba automatizada correcta
     */
    function testOk1(){
        $language=factory(\App\Models\tenant\Language::class)->create([
            'abbreviation' => 'TEST'
        ]);
        $contract = factory(\App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 100,
        ]);
        $contract_translation = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => $language->id,
            'contract_model_id' => $contract->id,
        ]);
        $this->json('GET','/api/v1/contract/'.$contract->id)
        ->assertExactJson(['error' => 200, 'data' => [[
            'contract' => [[
                'id' => $contract->id,
                'time_lock_avail' => $contract->time_lock_avail,
                'lang' => [[
                    $language->abbreviation => [
                        'id' => $contract_translation->id,
                        'language_id' => $language->id,
                        'name' => $contract_translation->name,
                    ]
                ]]
            ]]]
        ]]);
    }

    /**
     * Comprueba base de datos con campos actualizados y estructura
     */
    function testOk2(){

        $contract = factory(\App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 100,
        ]);

        $contract_translation = factory(\App\Models\tenant\ContractTranslation::class)->create([
            'language_id' => 1,
            'contract_model_id' => $contract->id,
        ]);

        $this->assertDatabaseHas('mo_contract_model', [
            'id' => $contract->id,
            'time_lock_avail' => $contract->time_lock_avail,
        ]);

        $this->assertDatabaseHas('mo_contract_model_translation', [
            'name' => $contract_translation->name,
        ]);

        $this->json('GET', '/api/v1/contract/'.$contract->id, [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['contract' => ["0" => ['id', 'time_lock_avail', 'lang']]]]
            ]);

    }

    /**
     * Consultar contrato sin traducciones, devuelve json vacío
     */
    public function testOk3()
    {
        $contract = factory(App\Models\tenant\ContractModel::class)->create([]);

        $this->json('GET', '/api/v1/contract/'.$contract->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

}