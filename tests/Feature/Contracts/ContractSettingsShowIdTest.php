<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ContractSettingsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consulta configuración de contrato con id inexistente y con id en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/price/contract/settings/999999')
            ->assertExactJson(['error' => 200, 'data' => []]);

        $this->json('GET', '/api/v1/price/contract/settings/999999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta contrato existente y devuelve datos de contrato
     */
    public function testOk1()
    {

        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 20,
        ]);

        $this->json('GET', '/api/v1/price/contract/settings/' . $contractFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory->id,
                'time_lock_avail' => $contractFactory->time_lock_avail,
            ]);
    }

    /**
     * Consulta contrato existente, crea producto y le asocia un servicio pero no le crea parametros de configuración para ningún contrato.
     * Devuelve datos de contrato, del producto creado, del servicio asociado al mismo y datos de asociación a null puesto que no están asociados en tabla mo_contract_model_product
     */
    public function testOk2()
    {
        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 20,
        ]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([
        ]);

        $typeTranslationFactory = factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $typeFactory->id,
            'name' => 'name type',
            'language_id' => 1
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $typeFactory->id,
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product testok2',
            'language_id' => 1
        ]);

        $serviceFactory = factory(App\Models\tenant\Service::class)->create([
        ]);

        $serviceTranslationFactory = factory(App\Models\tenant\ServiceTranslation::class)->create([
            'service_id' => $serviceFactory->id,
            'name' => 'name service testok2',
            'language_id' => 1
        ]);

        $productServiceFactory = factory(App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $this->json('GET', '/api/v1/price/contract/settings/' . $contractFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory->id,
                'time_lock_avail' => $contractFactory->time_lock_avail,
                'id' => $productFactory->id,
                'name' => $productTranslationFactory->name,
                'id' => $serviceFactory->id,
                'name' => $serviceTranslationFactory->name,
            ]);
    }

    /**
     * Crea 2 contratos. Crea producto y le crea parametros de configuración para un contrato y lo consulta.
     * Devuelve datos de contrato y de los parámetros de configuracion para ese producto. Al consultar otro contrato tambien devuelve datos del producto pero los de configuracion a null.
     * Crea después asociación para el mismo producto al otro contrato y al consultarlo devuelve datos de configuracion para contrato 2
     */
    public function testOk3()
    {
        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 20,
        ]);

        $contractFactory2 = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 10,
        ]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([
        ]);

        $typeTranslationFactory = factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $typeFactory->id,
            'name' => 'name type',
            'language_id' => 1
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $typeFactory->id,
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product testok2',
            'language_id' => 1
        ]);

        $contractModelProduct = factory(App\Models\tenant\ContractModelProduct::class)->create([
            'product_id' => $productFactory->id,
            'contract_model_id' => $contractFactory->id,
            'time_expiration' => 10,'time_limit_enjoyment' => 10,'apply_cancel_penalization' => 1,'individual_sale' => 1,
            'required_service' => 1,
        ]);

        $this->json('GET', '/api/v1/price/contract/settings/' . $contractFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory->id,
                'time_lock_avail' => $contractFactory->time_lock_avail,
                'id' => $productFactory->id,
                'name' => $productTranslationFactory->name,
                'time_expiration' => $contractModelProduct->time_expiration,'time_limit_enjoyment' => $contractModelProduct->time_limit_enjoyment,
                'apply_cancel_penalization' => $contractModelProduct->apply_cancel_penalization,'individual_sale' => $contractModelProduct->individual_sale,
                'required_service' => 1
            ]);

        //al consultar el contrato sin asociacion devuelve los campos de tabla intermedia a null
        $this->json('GET', '/api/v1/price/contract/settings/' . $contractFactory2->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory2->id,
                'time_lock_avail' => $contractFactory2->time_lock_avail,
                'id' => $productFactory->id,
                'name' => $productTranslationFactory->name,
                'time_expiration' => null,'time_limit_enjoyment' => null,
                'apply_cancel_penalization' => null,'individual_sale' => null
            ]);

        //hace asociacion del producto al otro contract_model creado y ahora mostrara datos de la asociacion creada
        $contractModelProduct2 = factory(App\Models\tenant\ContractModelProduct::class)->create([
            'product_id' => $productFactory->id,
            'contract_model_id' => $contractFactory2->id,
            'time_expiration' => 30,'time_limit_enjoyment' => 30,'apply_cancel_penalization' => 1,'individual_sale' => 0
        ]);

        $this->json('GET', '/api/v1/price/contract/settings/' . $contractFactory2->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory2->id,
                'time_lock_avail' => $contractFactory2->time_lock_avail,
                'id' => $productFactory->id,
                'name' => $productTranslationFactory->name,
                'time_expiration' => $contractModelProduct2->time_expiration,'time_limit_enjoyment' => $contractModelProduct2->time_limit_enjoyment,
                'apply_cancel_penalization' => $contractModelProduct2->apply_cancel_penalization,'individual_sale' => $contractModelProduct2->individual_sale
            ]);
    }

    /**
     * Prueba automatizada comprueba la salida de settings de contrato de los servicios
     */
    public function testOk4()
    {
        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 20,
        ]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([
        ]);

        $typeTranslationFactory = factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $typeFactory->id,
            'name' => 'name type',
            'language_id' => 1
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $typeFactory->id,
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product testok3',
            'language_id' => 1
        ]);

        $serviceFactory = factory(\App\Models\tenant\Service::class)->create([

        ]);

        $serviceTranslationFactory = factory(\App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory->id,
            'name' => 'name Servicio testOk3'
        ]);

        $productServiceFactory = factory(\App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $contractModelProduct = factory(App\Models\tenant\ContractModelProduct::class)->create([
            'product_id' => $productFactory->id,
            'contract_model_id' => $contractFactory->id,
            'time_expiration' => 10,'time_limit_enjoyment' => 10,'apply_cancel_penalization' => 1,'individual_sale' => 1
        ]);

        $this->json('GET', '/api/v1/price/contract/settings/' . $contractFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory->id,
                'time_lock_avail' => $contractFactory->time_lock_avail,
                'id' => $productFactory->id,
                'name' => $productTranslationFactory->name,
                'time_expiration' => $contractModelProduct->time_expiration,'time_limit_enjoyment' => $contractModelProduct->time_limit_enjoyment,
                'apply_cancel_penalization' => $contractModelProduct->apply_cancel_penalization,'individual_sale' => $contractModelProduct->individual_sale,
                'id' => $serviceFactory->id,
                'time_expiration' => null,'time_limit_enjoyment' => null,
                'apply_cancel_penalization' => null,'individual_sale' => null,
            ]);

    }


    /**
     * Prueba automatizada comprueba la salida de settings de contrato de los servicios con datos en contract_model_product y en contract_model_penalization
     */
    public function testOk5()
    {
        $contractFactory = factory(App\Models\tenant\ContractModel::class)->create([
            'time_lock_avail' => 20,
        ]);

        $typeFactory = factory(App\Models\tenant\Type::class)->create([
        ]);

        $typeTranslationFactory = factory(App\Models\tenant\TypeTranslation::class)->create([
            'type_id' => $typeFactory->id,
            'name' => 'name type',
            'language_id' => 1
        ]);

        $productFactory = factory(App\Models\tenant\Product::class)->create([
            'type_id' => $typeFactory->id,
        ]);

        $productTranslationFactory = factory(App\Models\tenant\ProductTranslation::class)->create([
            'product_id' => $productFactory->id,
            'name' => 'name product testok3',
            'language_id' => 1
        ]);

        $serviceFactory = factory(\App\Models\tenant\Service::class)->create([

        ]);

        $serviceTranslationFactory = factory(\App\Models\tenant\ServiceTranslation::class)->create([
            'language_id' => 1,
            'service_id' => $serviceFactory->id,
            'name' => 'name Servicio testOk3'
        ]);

        $productServiceFactory = factory(\App\Models\tenant\ProductService::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
        ]);

        $contractModelProduct = factory(App\Models\tenant\ContractModelProduct::class)->create([
            'product_id' => $productFactory->id,
            'contract_model_id' => $contractFactory->id,
            'time_expiration' => 10,'time_limit_enjoyment' => 10,'apply_cancel_penalization' => 1,'individual_sale' => 1
        ]);

        $contractModelProductServicio = factory(App\Models\tenant\ContractModelProduct::class)->create([
            'product_id' => $productFactory->id,
            'service_id' => $serviceFactory->id,
            'contract_model_id' => $contractFactory->id,
            'time_expiration' => 20,'time_limit_enjoyment' => 20,'apply_cancel_penalization' => 1,'individual_sale' => 0
        ]);

        $this->json('GET', '/api/v1/price/contract/settings/' . $contractFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $contractFactory->id,
                'time_lock_avail' => $contractFactory->time_lock_avail,
                'id' => $productFactory->id,
                'name' => $productTranslationFactory->name,
                'time_expiration' => $contractModelProduct->time_expiration,'time_limit_enjoyment' => $contractModelProduct->time_limit_enjoyment,
                'apply_cancel_penalization' => $contractModelProduct->apply_cancel_penalization,'individual_sale' => $contractModelProduct->individual_sale,
                'id' => $serviceFactory->id,
                'time_expiration' => $contractModelProductServicio->time_expiration,'time_limit_enjoyment' => $contractModelProductServicio->time_limit_enjoyment,
                'apply_cancel_penalization' => $contractModelProductServicio->apply_cancel_penalization,'individual_sale' => $contractModelProductServicio->individual_sale,
            ]);

    }

}