<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TagsAssignTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test que comprueba las validaciones del método
     *
     * Sin enviar ningún parámetro
     */

    public function testBad1()
    {
        $this->json('POST', '/api/v1/tag/assign', [])
            ->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
                'product_id' => ['The product id field is required.'],
            ]]]);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Enviando formato incorrecto de product_id
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => 'text',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'product_id' => ['The product id must be a number.'],
        ]]]);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Envío de product_id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => 999999999999999999999999,
        ])->assertExactJson(['error' => 404, 'error_description' => 'Data not found']);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Envío formato incorrecto de tags
     */
    public function testBad4()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => $productFactory->id,
            'tags' => 'texto',
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'tags' => ['The tags must be an array.'],
        ]]]);
    }

    /**
     * Test que comprueba validaciones del método
     *
     * Envío de tag inexistente
     */
    public function testBad5()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => $productFactory->id,
            'tags' => [99999999999999999999999],
        ])->assertExactJson(['error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [[
            'tags.0' => ['The selected tags.0 is invalid.'],
        ]]]);
    }

    /**
     * Test que comprueba funcionamiento del método
     *
     * Envío de product sin tags
     */
    public function testOK1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => $productFactory->id,
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_product_tag', ['product_id' => $productFactory->id, 'deleted_at' => null]);
    }

    /**
     * Test que comprueba funcionamiento del método
     *
     * Envío de product y tags
     */
    public function testOK2()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
        ]);

        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => $productFactory->id,
            'tags' => [$tagFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_product_tag', ['product_id' => $productFactory->id, 'tag_id' => $tagFactory->id, 'deleted_at' => null]);

    }

    /**
     * Test que comprueba funcionamiento del método
     *
     * Envío de product y tag sobreescribiendo relaciones ya existentes
     */
    public function testOK3()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
        ]);

        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => $productFactory->id,
            'tags' => [$tagFactory->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseHas('mo_product_tag', ['product_id' => $productFactory->id, 'tag_id' => $tagFactory->id, 'deleted_at' => null]);

        $tagFactory2 = factory(App\Models\tenant\Tag::class)->create([
        ]);

        $this->json('POST', '/api/v1/tag/assign', [
            'product_id' => $productFactory->id,
            'tags' => [$tagFactory2->id],
        ])->assertExactJson(['error' => 200]);

        $this->assertDatabaseMissing('mo_product_tag', ['product_id' => $productFactory->id, 'tag_id' => $tagFactory->id, 'deleted_at' => null]);

        $this->assertDatabaseHas('mo_product_tag', ['product_id' => $productFactory->id, 'tag_id' => $tagFactory2->id, 'deleted_at' => null]);

    }


}