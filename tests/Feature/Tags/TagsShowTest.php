<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TagsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Test consulta tags con parámetro lang mal
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/tag', [
            'lang' => 'qwe'
        ])->assertJsonStructure(['error', 'error_description', "error_inputs"]);
    }

    /**
     * Test consulta tags con parámetro product incorrecto
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/tag', [
            'product_id' => 'es'
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta tags con algunos de los parámetros mal
     */
    public function testBad3()
    {
        $this->json('GET', '/api/v1/tag', [
            'lang' => 'es',
            'product_id' => 'esas',
            'name' => 'cualquiera',
        ])->assertJsonStructure(['error', 'error_description', 'error_inputs']);
    }

    /**
     * Test consulta tags con parámetros incorrectos
     */
    public function testBad4()
    {
        $this->json('GET', '/api/v1/tag', [
            'lang' => 'es',
            'name' => 'ven',
            'limit' => 'afaf',
        ])->assertJsonStructure(['error', 'error_description']);
    }

    /**
     * Test consulta tags con algunos parámetros mal
     */
    public function testBad5()
    {
        $this->json('GET', '/api/v1/tag', [
            'lang' => 'es',
            'product_id' => 'h',
            'name' => 'cualquiera',
            'start' => 'm',
            'limit' => 5
        ])->assertJsonStructure(['error', 'error_description']);
    }

    /**
     * Test consulta tags con parámetro lang en minúsculas
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/tag', [
            'lang' => 'es'
        ])->assertJsonStructure(['error', 'data' => [], "total_results"]);
    }

    /**
     * Test consulta tags con parámetro lang en mayúsculas
     */
    public function testOk2()
    {
        $this->json('GET', '/api/v1/tag', [
            'lang' => 'EN'
        ])->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     * Test consulta tags con parámetro product inexistente
     */
    public function testOk3()
    {
        $this->json('GET', '/api/v1/tag', [
            'lang' => 'es',
            'product_id' => '999999999999999',
        ])->assertExactJson(['data' => [], 'error' => 200, "total_results" => 0,]);
    }

    /**
     * Test consulta tags con parámetro product existente en base de datos
     */
    public function testOk4()
    {
        //crea producto
        $productFact = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('GET', '/api/v1/tag', [
            'product_id' => $productFact->id,
        ])->assertJsonStructure(['error', 'data']);
    }

    /**
     *  Test consulta de tags sin parámetros
     */
    public function testOk5()
    {
        $this->json('GET', '/api/v1/tag')->assertJsonStructure(['error', 'data' => [], 'total_results']);
    }

    /**
     *  Test consulta de tags filtrando por product borrado, no debe devolver resultados
     */
    public function testOk6()
    {
        //crea tag
        $factory = factory(App\Models\tenant\Tag::class)->create([
            'order' => 4,
        ]);

        //crea translation de tag
        factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $factory->id,
            'language_id' => 1,
        ]);

        //crea producto
        $productFact = factory(App\Models\tenant\Product::class)->create([
        ]);

        //crea una relación product tag
        $proTag = factory(App\Models\tenant\ProductTag::class)->create([
            'tag_id' => $factory->id,
            'product_id' => $productFact->id,
        ]);
        //consulta tags filtrando por el product que acaba de relacionar y devuelve un resultado
        $this->json('GET', '/api/v1/tag', ['product_id' => $productFact->id])->assertJsonFragment(['total_results' => 1]);
        //borra relación product tag
        $proTag->delete();
        //vuelve a consultar tag filtrando por el product tag que acaba de borrar
        $this->json('GET', '/api/v1/tag', ['product_id' => $productFact->id])->assertJsonFragment(['total_results' => 0]);
    }

    /**
     * Test consulta tags con casi todos los parámetros
     */
    public function testOk7()
    {
        //crea producto
        $productFact = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('GET', '/api/v1/tag', [
            'lang' => 'en',
            'product_id' => $productFact->id,
            'name' => 'EN',
        ])->assertJsonStructure(['error', 'data']);
    }

    /**
     * Test consulta tags con todos los parámetros
     */
    public function testOk8()
    {
        //crea producto
        $productFact = factory(App\Models\tenant\Product::class)->create([
        ]);

        $this->json('GET', '/api/v1/tag', [
            'lang' => 'en',
            'product_id' => $productFact->id,
            'name' => 'EN',
            'start' => 2,
            'limit' => 5
        ])->assertJsonStructure(['error', 'data', 'total_results']);
    }

    /**
     * Test consulta tags y filtra por idioma sin traducción, no devuelve registro
     */
    public function testOk9()
    {

        //crea factory de tag
        $factory = factory(App\Models\tenant\Tag::class)->create([
            'order' => 4,
        ]);
        //crea translation en español de tag
        factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $factory->id,
            'language_id' => 1,
        ]);

        //crea translation en inglés de tag
        factory(App\Models\tenant\TagTranslation::class)->create([
            'tag_id' => $factory->id,
            'language_id' => 2,
        ]);

        //se consulta por id del tag creado, se filtra por lang es y devuelve el registro
        $this->json('GET', '/api/v1/tag/'.$factory->id, [
            'lang' => 'es'])->assertJsonFragment(['id' => $factory->id]);

        //se consulta por id del tag creado, se filtra por lang en y devuelve el registro
        $this->json('GET', '/api/v1/tag/'.$factory->id, [
            'lang' => 'en'])->assertJsonFragment(['id' => $factory->id]);

        //se consulta por id del tag creado, se filtra por lang pt que no existe y no devuelve registros
        $this->json('GET', '/api/v1/tag/'.$factory->id, [
            'lang' => 'pt'])->assertExactJson(['error' => 200, 'data' => []]);
    }
}