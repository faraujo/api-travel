<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TagsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * Consultar tag con parámetro lang erróneo
     */
    public function testBad1()
    {
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
            'order' => 4,
        ]);
        factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 1,
            'tag_id' => $tagFactory->id,
        ]);
        factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 2,
            'tag_id' => $tagFactory->id,
        ]);
        factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 3,
            'tag_id' => $tagFactory->id,
        ]);
        $this->json('GET', '/api/v1/tag/' . $tagFactory->id, [
            'lang' => 'FDF'])->assertExactJson([
            'error' => 400, 'error_description' => 'The fields are not the required format', 'error_inputs' => [['lang' => ["The lang is not exists"]]]
        ]);
    }

    /**
     * Consulta tag inexistente sin parámetros
     */
    public function testOk1()
    {
        $this->json('GET', '/api/v1/tag/9999')
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consultar tag con parámetro lang correcto
     */
    public function testOk2()
    {
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
            'order' => 4,
        ]);
        $translation1 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 1,
            'tag_id' => $tagFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 2,
            'tag_id' => $tagFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 3,
            'tag_id' => $tagFactory->id,
        ]);
        $this->json('GET', '/api/v1/tag/' . $tagFactory->id, [
            'lang' => 'PT'])->assertJsonFragment([
            'error' => 200,
            'id' => $tagFactory->id,
            'name' => $translation3->name,
            'description' => $translation3->description,
            'short_description' => $translation3->short_description,
        ]);
    }

    /**
     * Consultar tag sin traducciones, devuelve json vacío
     */
    public function testOk3()
    {
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
            'order' => 4,
        ]);

        $this->json('GET', '/api/v1/tag/'.$tagFactory->id)
            ->assertExactJson(['error' => 200, 'data' => []]);
    }

    /**
     * Consulta tag existente sin parámetro lang
     */
    public function testOk4()
    {
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
            'order' => 4,
        ]);
        $translation1 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 1,
            'tag_id' => $tagFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 2,
            'tag_id' => $tagFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 3,
            'tag_id' => $tagFactory->id,
        ]);
        $this->json('GET', '/api/v1/tag/' . $tagFactory->id)
            ->assertJsonFragment([
                'error' => 200,
                'id' => $tagFactory->id,
                'name' => $translation1->name,
                'name' => $translation2->name,
                'name' => $translation3->name,
                'description' => $translation2->description,
                'short_description' => $translation3->short_description,
            ]);
    }

}