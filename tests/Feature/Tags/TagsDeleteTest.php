<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class TagsDeleteTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test para comprobar en el validador un campo numérico
     */
    public function testBad1()
    {
        $this->json('DELETE', '/api/v1/tag', [
            'ids' => ['r']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 must be an integer."]]]]);
    }

    /**
     * test para comprobar el validador de campo obligatorio
     */
    public function testBad2()
    {
        $this->json('DELETE', '/api/v1/tag', [
            'ids' => ['']
        ])
            ->assertExactJson(["error" => 400, "error_description" => 'The fields are not the required format', 'error_inputs' => [["ids.0" => ["The ids.0 field is required."]]]]);
    }

    /**
     * Id inexistente en base de datos
     */
    public function testBad3()
    {
        $this->json('DELETE', '/api/v1/tag', [
            'ids' => [5000000000000],
        ])
            ->assertExactJson([
                'error' => 404,
                'error_description' => 'Data not found',
                'error_inputs' => [["ids.0" => ["The selected ids.0 is invalid."]]]
            ]);
    }

    /** Se envía el id a borrar en formato no array.
     *
     */
    public function testBad4()
    {

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
        ]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);

        $this->json('DELETE', '/api/v1/tag', [
            'ids' => $tagFactory->id,
        ])
            ->assertExactJson([
                "error"=>400,"error_description" =>"The fields are not the required format","error_inputs" => [["ids"=>["The ids must be an array."]]]
            ]);

    }


    /**
     * Test para eliminar un tag que crea primero un tag factory, sus traducciones y relación en tabla intermedia
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
        ]);


        $translation1 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 1,
            'tag_id' => $tagFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 2,
            'tag_id' => $tagFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 3,
            'tag_id' => $tagFactory->id,
        ]);


        factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $productFactory->id,
            'tag_id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_tag_translation', [
            'tag_id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_tag', [
            'tag_id' => $tagFactory->id,
        ]);


        $this->json('DELETE', '/api/v1/tag', [
            'ids' => [$tagFactory->id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_tag el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_tag', ['deleted_at' => null, 'id' => $tagFactory->id]);
        //Comprueba que las traducciones del tag que han sido creadas no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation1->tag_id]);
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation2->tag_id]);
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation3->tag_id]);
        //Comprueba que la relación creada en la tabla intermedia mo_product_tag no tiene el campo deleted_at como nulo

        $this->assertDatabaseMissing('mo_product_tag', ['deleted_at' => null, 'tag_id' => $tagFactory->tag_id]);

    }

    /**
     * Borrar sin traducciones
     */
    public function testOk2()
    {
        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
        ]);

        $this->json('DELETE', '/api/v1/tag', [
            'ids' => [$tagFactory->id],
        ])->assertExactJson(['error' => 200]);

        //Comprueba que en la tabla mo_tag el registro con id que ha sido creado no tiene el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_tag', ['deleted_at' => null, 'id' => $tagFactory->id]);

    }

    /**
     * Test para eliminar dos tag que crea primero con tag factory, sus traducciones y relaciones en tablas intermedias
     */
    public function testOk3()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([
        ]);

        $tagFactory = factory(App\Models\tenant\Tag::class)->create([
        ]);

        $productFactory2 = factory(App\Models\tenant\Product::class)->create([
        ]);

        $tagFactory2 = factory(App\Models\tenant\Tag::class)->create([
        ]);


        $translation1 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 1,
            'tag_id' => $tagFactory->id,
        ]);
        $translation2 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 2,
            'tag_id' => $tagFactory->id,
        ]);
        $translation3 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 3,
            'tag_id' => $tagFactory->id,
        ]);

        $translation4 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 1,
            'tag_id' => $tagFactory2->id,
        ]);
        $translation5 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 2,
            'tag_id' => $tagFactory2->id,
        ]);
        $translation6 = factory(App\Models\tenant\TagTranslation::class)->create([
            'language_id' => 3,
            'tag_id' => $tagFactory2->id,
        ]);



        factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $productFactory->id,
            'tag_id' => $tagFactory->id,
        ]);

        factory(App\Models\tenant\ProductTag::class)->create([
            'product_id' => $productFactory2->id,
            'tag_id' => $tagFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_tag', [
            'id' => $tagFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_tag_translation', [
            'tag_id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_tag_translation', [
            'tag_id' => $tagFactory2->id,
        ]);

        $this->assertDatabaseHas('mo_product_tag', [
            'tag_id' => $tagFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product_tag', [
            'tag_id' => $tagFactory2->id,
        ]);

        //Borramos los dos tags
        $this->json('DELETE', '/api/v1/tag', [
            'ids' => [$tagFactory->id,$tagFactory2 -> id],
        ])
            ->assertExactJson([
                'error' => 200,
            ]);

        //Comprueba que en la tabla mo_tag los registros con id que han sido creados no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_tag', ['deleted_at' => null, 'id' => $tagFactory->id]);
        $this->assertDatabaseMissing('mo_tag', ['deleted_at' => null, 'id' => $tagFactory2->id]);

        //Comprueba que las traducciones de lod tagd que han sido creadas no tienen el campo deleted_at como nulo
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation1->tag_id]);
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation2->tag_id]);
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation3->tag_id]);
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation4->tag_id]);
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation5->tag_id]);
        $this->assertDatabaseMissing('mo_tag_translation', ['deleted_at' => null, 'tag_id' => $translation6->tag_id]);

        //Comprueba que las relaciones creadas en la tabla intermedia mo_product_tag no tienen el campo deleted_at como nulo

        $this->assertDatabaseMissing('mo_product_tag', ['deleted_at' => null, 'tag_id' => $tagFactory->tag_id]);
        $this->assertDatabaseMissing('mo_product_tag', ['deleted_at' => null, 'tag_id' => $tagFactory2->tag_id]);

    }


}