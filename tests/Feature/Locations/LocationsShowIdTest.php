<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LocationsShowIdTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de localización inexistente
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/location/999999999999')
            ->assertExactJson(['error' => 200, 'total_results' => 0,'data' => []]);
    }


    /**
     * test crea factory de location y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
            'name' => 'Nombre locación',
            'depends_on' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_location', [
            'id' => $locationFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);


        $this->json('GET', '/api/v1/location/'. $locationFactory->id, [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $locationFactory->id,
                'name' => 'Nombre locación',
                //'depends_on' => $productFactory->id,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/location', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['location' => ["0" => ['id', 'name', 'depends_on']]]]
            ]);

    }


}