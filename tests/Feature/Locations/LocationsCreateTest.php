<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LocationsCreateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía ningún campo
     */
    public function testBad1()
    {
        $this->json('POST', '/api/v1/location', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "name" => ["The name field is required."]
            ]]
            ]);
    }
    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se envía depends_on inexistente
     */
    public function testBad2()
    {
        $this->json('POST', '/api/v1/location', [
            'name' => 'MyTest',
            'depends_on' => 99999999,
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "depends_on" => ["The selected depends on is invalid."]
            ]]
            ]);
    }
     /**
     * Realiza el test de creación de localizacion
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK1()
    {
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $this->json('POST', '/api/v1/location', [
            'name' => $locationFactory->name
            ])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_location', [
            'name' => $locationFactory->name
        ]);

    }

    /**
     * Realiza el test de creación de localizacion con depends_on asignado
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK2()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(App\Models\tenant\Location::class)->create([]);
        $this->json('POST', '/api/v1/location', [
            'name' => $locationFactory->name,
            'depends_on' => $productFactory->id
            ])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_location', [
            'name' => $locationFactory->name,
            'depends_on' => $productFactory->id
        ]);

    }
   
}