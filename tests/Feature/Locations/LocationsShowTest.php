<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LocationsShowTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     * test consulta de localizaciones con parametros en formato incorrecto
     */
    public function testBad1()
    {
        $this->json('GET', '/api/v1/location', [
            'page' => 'string',
            'limit' => 'string',

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."]]]]);
    }

    /**
     * Test que consulta las localizaciones con un depends on inexistente
     */
    public function testBad2()
    {
        $this->json('GET', '/api/v1/location', [
            'page' => '0',
            'limit' => '0',
            'depends_on' => 9999999999

        ])
            ->assertExactJson(["error" => 200, "data" => [],
                "total_results" => 0 ]);
    }

    /**
     * test consulta de localizaciones con parametros en formato incorrecto
     */
    public function testBad3()
    {
        $this->json('GET', '/api/v1/location', [
            'page' => 'string',
            'limit' => 'string',
            'depends_on' => 'string'

        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format",
                "error_inputs" => [["limit" => ["The limit must be an integer."],
                    "page" => ["The page must be an integer."],
                    "depends_on" => ["The depends on must be an integer."]]]]);
    }


    /**
     * test crea factory de location y comprueba que se muestra en la salida y su estructura
     */
    public function testOk1()
    {
        $productFactory = factory(App\Models\tenant\Product::class)->create([]);

        $locationFactory = factory(App\Models\tenant\Location::class)->create([
            'name' => 'Nombre locación',
            'depends_on' => $productFactory->id,
        ]);

        $this->assertDatabaseHas('mo_location', [
            'id' => $locationFactory->id,
        ]);

        $this->assertDatabaseHas('mo_product', [
            'id' => $productFactory->id,
        ]);


        $this->json('GET', '/api/v1/location', [])
            ->assertJsonFragment([
                'error' => 200,
                'id' => $locationFactory->id,
                'name' => 'Nombre locación',
                //'depends_on' => $productFactory->id,
            ]);

        //comprueba estructura
        $this->json('GET', '/api/v1/location', [])
            ->assertJsonStructure(['error',
                'data' => ["0" => ['location' => ["0" => ['id', 'name', 'depends_on']]]]
            ]);

    }


}