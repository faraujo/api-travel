<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LocationsUpdateTest extends TestCase
{

    use DatabaseTransactions;
    use WithoutMiddleware;

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * No se envía ningún campo
     */
    public function testBad1()
    {
        $this->json('PUT', '/api/v1/location', [])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "id" => ["The id field is required."],
                "name" => ["The name field is required."]
            ]]
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se envía un id inexistente
     */
    public function testBad2()
    {
        $this->json('PUT', '/api/v1/location', [
            'id' => 999999999999999999,
            'name' => 'Test'
        ])
            ->assertExactJson(["error" => 404, "error_description" => "Data not found"
            ]);
    }

    /**
     *Test que comprueba funcionamiento de las validaciones
     *
     * Se envía un depends on inexistente
     */
    public function testBad3()
    {
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);
        $this->json('PUT', '/api/v1/location', [
            'id' => $locationFactory->id,
            'name' => $locationFactory->name,
            'depends_on' => 999999999999999999
        ])
            ->assertExactJson(["error" => 400, "error_description" => "The fields are not the required format", "error_inputs" => [[
                "depends_on" => ["The selected depends on is invalid."]
            ]]
            ]);
    }

    /**
     * Realiza el test de actualización de localización
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK1()
    {

        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/location', [
            'id' => $locationFactory->id,
            'name' => 'nombre prueba'])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_location', [
            'id' => $locationFactory->id,
            'name' => 'nombre prueba'
        ]);

    }

    /**
     * Realiza el test de actualización de localización con el campo depends_on
     * Comprueba que se ha insertado correctamente en base de datos
     */
    public function testOK2()
    {

        $productFactory = factory(App\Models\tenant\Product::class)->create([]);
        $locationFactory = factory(\App\Models\tenant\Location::class)->create([]);

        $this->json('PUT', '/api/v1/location', [
            'id' => $locationFactory->id,
            'name' => 'nombre prueba',
            'depends_on' => $productFactory->id
            ])
            ->assertExactJson([
                'error' => 200,
            ]);
        
        $this->assertDatabaseHas('mo_location', [
            'id' => $locationFactory->id,
            'name' => 'nombre prueba',
            'depends_on' => $productFactory->id
        ]);

    }
}