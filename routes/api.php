<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


//Rutas Saas abiertas (Token)
Route::middleware(['saasautorizacionsubchannel'])->prefix('/saas')->group(function (){
    Route::post('login', 'SaasUserController@login');
    Route::post('login/refresh', 'SaasUserController@refresh'); 
    Route::post('login/register', 'SaasUserController@register');
    Route::post('login/forgot', 'SaasUserController@forgot'); //? log
    Route::post('login/recover', 'SaasUserController@recover');
    Route::post('login/validate', 'SaasUserController@verifyEmail');

    Route::post('payment/monthlySubscription', 'SaasPaymentController@paypalSubscription');
});

// Rutas SaaS abiertas
Route::post('saas/email', 'SaasEmailController@send');
Route::post('saas/payment/ipn/notify','SaasPaymentController@paypalIpn');
Route::post('saas/payment/redsys/notify', 'SaasPaymentController@redsysSubscription');



// Rutas SaaS cerradas +plus
Route::middleware(['saasautorizacion', 'saasautorizacionsubchannel'])->prefix('/saas')->group(function (){
    Route::post('login/logout', 'SaasUserController@logout');
    Route::put('user/profile', 'SaasUserController@updateProfile');
    Route::get('user/profile', 'SaasUserController@showProfile');
    Route::post('user/sendvalidate', 'SaasUserController@sendVerifyEmail');
    Route::post('valid_token', 'SaasUserController@valid_token');
    
    Route::post('login/engine', 'SaasUserController@engineLogin');

    Route::post('website', 'SaasWebsiteController@createWebsite');
    Route::post('websiteplan', 'SaasWebsiteController@createWebsitePlan');
    Route::put('website', 'SaasWebsiteController@updateWebsite');
    Route::get('website', 'SaasWebsiteController@showWebsites');
    Route::get('website/{id}', 'SaasWebsiteController@showWebsiteId')->where('id', '[0-9]+');
    Route::get('websiteplan/{id}', 'SaasWebsiteController@showWebsitePlanId')->where('id', '[0-9]+');
    Route::get('currency', 'SaasWebsiteController@showCurrencies');
    Route::get('language', 'SaasWebsiteController@showLanguages');
    Route::get('rule', 'SaasWebsiteController@showRule');
    
    Route::post('payment', 'SaasPaymentController@payment');
    Route::post('payment/subscription', 'SaasPaymentController@cancelSubscription');
    Route::post('payment/webhook', 'SaasPaymentController@webhook');
});

// Rutas cerradas
Route::middleware(['autorizacion'])->prefix('/saas')->group(function (){
    Route::get('rulecp', 'SaasWebsiteController@showRule');
});


Route::namespace('tenant')->group(function (){

    Route::get('/', 'GeneralController@index');

    $start_petition = \Carbon\Carbon::now();
    $input_data = Request::capture();

    //Rutas sin heimdall
    Route::middleware([])->prefix('/v1')->group(function () use ($start_petition, $input_data){
        Route::post('login/refresh', 'UserController@refresh');
    });

    Route::middleware(['autorizacion'])->prefix('/v1')->group(function () use ($start_petition, $input_data){
        Route::post('valid_token', 'GeneralController@valid_token');
        Route::post('login/saaslogin', 'UserController@saasLogin');
        
    });

    //Rutas abiertas
    Route::middleware(['heimdall'])->prefix('/v1')->group(function () use ($start_petition, $input_data){
       
        //Rutas Login
        Route::post('login/reservation', 'UserController@reservationLogin'); 
        Route::post('login/register', 'UserController@register'); 
        Route::post('login/forgot', 'UserController@forgot')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::post('login/recover', 'UserController@recover'); 
        //Route::post('login/refresh', 'UserController@refresh'); 
        Route::post('login/loginsocial', 'UserController@loginsocial'); 
        Route::post('login', 'UserController@login'); 
        //Fin rutas Login

        //Rutas de slack
        Route::post('slack', 'SlackController@send'); 
        //FIN Rutas de slack

        //Rutas Reservas - reserva
        Route::get('reservation/pdf/search', 'ReservationsController@generateReservationPDF');
        Route::post('reservation/sendconfirmedemail/search', 'ReservationsController@sendConfirmedReservationMail')->middleware('log:' . $start_petition . ',' . $input_data);

    });

    //Rutas abiertas (subcanal)
    Route::middleware(['heimdall', 'subchannelauthorizacion', 'autorizacionsearch'])->prefix('/v1')->group(function () use ($start_petition, $input_data){
        
        //Rutas de emails
        Route::post('email_without_token', 'EmailController@send');

        //Rutas Categorías
        Route::get('category/search', 'CategoryController@show');
        //Fin rutas Categorías

        //Rutas tipo de productos
        Route::get('product/types/search', 'TypeController@showTypes');

        //Rutas habitaciones
        Route::get('product/room/categories/search', 'CategoryRoomController@showCategories');

        //Rutas locaciones
        Route::get('location/search', 'LocationController@show');

        //Rutas opiniones
        Route::get('comment/search', 'CommentController@show');

        //Rutas areas
        Route::get('area/country/search', 'AreaController@showCountry');
        Route::get('area/state/search', 'AreaController@showState');
        //FIN Rutas areas

        //Rutas disponibilidades
        Route::get('availability/calendar/search', 'AvailabilityController@showCalendar');
        //FIN Rutas disponibilidades

        //Rutas archivos
        Route::get('file/search/{id}', 'FileController@showId');

        //Rutas buscador
        Route::get('search/additionalServices', 'SearchController@addicionalServices');
        Route::get('search/promotion', 'SearchController@showPromotions');
        Route::get('search/calendarHotel', 'SearchController@showCalendarHotel');
        Route::get('search/pickups', 'SearchController@showPickups');
        Route::get('search' , 'SearchController@showSubchannel');
        Route::get('search/room', 'SearchController@showRoom');
        Route::get('search/room/{id}', 'SearchController@showRoom')->where('id', '[0-9]+');
        Route::get('search/house', 'SearchController@showHouse');
        Route::get('search/crosspurchase', 'SearchController@crosspurchase');
        Route::get('search/predictive', 'SearchController@showPredictive');
        Route::get('search/{id}', 'SearchController@showSubchannel')->where('id', '[0-9]+');
        //FIN Rutas buscador

        //Rutas reservas

        //Rutas reservas - promociones
        Route::post('reservation/promotion/aplicate/search', 'ReservationsPromotionController@autoaplicatePromotion')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::delete('reservation/promotion/search', 'ReservationsPromotionController@removePromotion')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::get('reservation/promotion/search', 'ReservationsPromotionController@showPromotions');

        //Rutas reservas - métodos de pago
        Route::get('reservation/payment_method/search', 'ReservationsPaymentMethodController@showPaymentMethod');
        Route::get('reservation/payment_method/mop/search', 'ReservationsPaymentMethodController@paymentMethodsMop');
        Route::post('reservation/payment/search', 'ReservationsPaymentMethodController@payment')->middleware('log:' . $start_petition . ',' . $input_data);

        //Rutas reservas - seguro
        Route::post('reservation/insurance/search', 'ReservationInsuranceController@assignInsurance')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::delete('reservation/insurance/search', 'ReservationInsuranceController@unassignInsurance')->middleware('log:' . $start_petition . ',' . $input_data);

        //Rutas reservas - reserva
        Route::get('reservation/operation_status/search', 'ReservationsController@showOperationStatus');
        Route::get('reservation/payment_status/search', 'ReservationsController@showPaymentStatus');
        Route::put('reservation/updatereservationclient/search', 'ReservationsController@updateReservationClient')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/updatereservationuser/search', 'ReservationsController@updateReservationUser')->middleware('log:' . $start_petition . ',' . $input_data); 
        Route::put('reservation/updatereservationcurrency/search', 'ReservationsController@updateReservationCurrency')->middleware('log:' . $start_petition . ',' . $input_data);  
        Route::put('reservation/changeUser/search', 'ReservationsController@changeReservationUser')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/cancel/search', 'ReservationsController@cancel')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/pickup/search', 'ReservationsController@setReservationPickup')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/shuttlehotel/search', 'ReservationsController@setShuttleHotelPickup')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/notes/search', 'ReservationsController@setNotes')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/search','ReservationsController@updateReservation')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/descale/search','ReservationsController@descale')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/secondstep/search', 'ReservationsController@secondStep')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::get('reservation/firststep/search', 'ReservationsController@firstStep');        
        Route::delete('reservation/search', 'ReservationsController@delete')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/sendemail/search', 'ReservationsController@sendReservationMail')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/search', 'ReservationsController@create')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::get('reservation/search', 'ReservationsController@show');
        Route::get('reservation/shoppingcart/search', 'ReservationsController@showId');

        Route::post('reservation/payment/paypalprocesspayment', 'ReservationsPaymentMethodController@paypalProcessPayment');

        //Rutas páginas
        Route::get('webpage/{type}/search', 'WebPageController@showType');

        //Rutas front de venta
        Route::namespace('web')->group(function (){
            Route::get('home', 'HomeController@precharge');
            Route::get('settings/web', 'SettingsController@precharge');
            Route::get('language/web', 'LanguageController@show');
            Route::get('currency/web', 'CurrencyController@show');
        });

    });

    //Rutas cerradas +plus (autorización + subcanal)
    Route::middleware(['heimdall', 'autorizacion', 'subchannelauthorizacion'])->prefix('/v1')->group(function () use ($start_petition, $input_data){

        //Rutas opiniones
        Route::post('comment/search', 'CommentController@create')->middleware('log:' . $start_petition . ',' . $input_data);

        //Rutas usuarios
        Route::get('user/profile/search', 'UserController@profileSearch');
        Route::put('user/profile/search', 'UserController@updateProfileSearch')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::delete('user/profile/search', 'UserController@deleteProfileSearch')->middleware('log:' . $start_petition . ',' . $input_data);
        //FIN Rutas usuarios

        //Rutas archivos
        Route::post('file/search', 'FileController@create')->middleware('log:' . $start_petition . ',' . $input_data);

        //Rutas reservas - reserva
        Route::get('reservation/showdetails/search','ReservationsController@showAllDetails');

    });
    //Rutas cerradas
    Route::middleware(['heimdall', 'autorizacion'])->prefix('/v1')->group(function () use ($start_petition, $input_data){
        //Rutas login
        Route::post('login/logout', 'UserController@logout')->middleware('log:' . $start_petition . ',' . $input_data);
        Route::post('login/changepassword', 'UserController@changepassword');
        
        //Rutas Idiomas
        Route::get('language', 'LanguageController@show')->middleware('roles:0,34/108/109,1,0');
        
        //Rutas Monedas
        Route::get('currency/exchange', 'CurrencyController@showExchange')->middleware('roles:1,100,1,0');
        Route::get('currency', 'CurrencyController@show')->middleware('roles:0,25/108,1,0');
        Route::post('currency/exchange', 'CurrencyController@createExchange')->middleware('roles:1,101,1,0', 'log:' . $start_petition . ',' . $input_data);

        //Rutas Categorías
        Route::post('category/assign', 'CategoryController@assign')->middleware('roles:0,19/108,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::post('category', 'CategoryController@create')->middleware('roles:1,15,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('category', 'CategoryController@show')->middleware('roles:0,14/108/109,1,0');
        Route::delete('category', 'CategoryController@delete')->middleware('roles:1,17,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('category', 'CategoryController@update')->middleware('roles:1,18,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('category/{id}', 'CategoryController@showId')->where('id', '[0-9]+')->middleware('roles:1,16,1,0');

        //Rutas tipo de productos
        Route::get('product/types/{id}', 'TypeController@showTypeId')->where('id', '[0-9]+')->middleware('roles:1,9,1,0');
        Route::get('product/types', 'TypeController@showTypes')->middleware('roles:0,8/108/109,1,0');

        //Rutas unidades de medida
        Route::get('product/uom', 'UomController@showUom')->middleware('roles:0,93/108/109,1,0');
        Route::get('product/uom/{id}', 'UomController@showUomId')->where('id', '[0-9]+')->middleware('roles:1,94,1,0');

        //Rutas tasas de iva
        Route::post('product/iva', 'IvaController@createIva')->middleware('roles:1,212,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('product/iva', 'IvaController@showIva')->middleware('roles:0,176/108/109,1,0');
        Route::delete('product/iva', 'IvaController@deleteIva') ->middleware('roles:1,214,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('product/iva', 'IvaController@updateIva')->middleware('roles:1,213,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('product/iva/{id}', 'IvaController@showIvaId')->where('id', '[0-9]+')->middleware('roles:1,177,1,0');

        //Rutas de productos
        Route::post('product', 'ProductController@create')->middleware('roles:1,7,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('product', 'ProductController@show')->middleware('roles:0,3/108/109,1,0');
        Route::delete('product', 'ProductController@delete')->middleware('roles:1,4,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('product', 'ProductController@update')->middleware('roles:1,5,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('product/{id}', 'ProductController@showId')->where('id', '[0-9]+')->middleware('roles:1,6,1,0');

        //Rutas habitaciones
        Route::get('product/room', 'HotelRoomController@show')->middleware('roles:0,110/108/109,1,0');
        Route::delete('product/room', 'HotelRoomController@delete')->middleware('roles:1,111,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('product/room', 'HotelRoomController@update')->middleware('roles:1,112,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::post('product/room', 'HotelRoomController@create')->middleware('roles:1,114,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('product/room/categories', 'CategoryRoomController@showCategories')->middleware('roles:0,119/108/109,1,0');
        Route::get('product/room/{id}', 'HotelRoomController@showId')->where('id', '[0-9]+')->middleware('roles:1,113,1,0');

        //Rutas etiquetas
        Route::post('tag/assign', 'TagController@assign')->middleware('roles:0,60/108,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::post('tag', 'TagController@create')->middleware('roles:1,64,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('tag', 'TagController@show')->middleware('roles:0,63/108/109,1,0');
        Route::delete('tag', 'TagController@delete')->middleware('roles:1,65,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('tag', 'TagController@update')->middleware('roles:1,61,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('tag/{id}', 'TagController@showId')->where('id', '[0-9]+')->middleware('roles:1,62,1,0');

        //Rutas servicios
        Route::post('service/assign', 'ServiceController@assign')->middleware('roles:0,59/108,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::post('service', 'ServiceController@create')->middleware('roles:1,54,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('service', 'ServiceController@show')->middleware('roles:0,55/108/109,1,0');
        Route::delete('service', 'ServiceController@delete')->middleware('roles:1,57,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('service', 'ServiceController@update')->middleware('roles:1,58,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('service/{id}', 'ServiceController@showId')->where('id', '[0-9]+')->middleware('roles:1,56,1,0');

        //Rutas localizaciones
        Route::post('location', 'LocationController@create')->middleware('roles:1,209,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('location', 'LocationController@delete')->middleware('roles:1,210,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('location', 'LocationController@update')->middleware('roles:1,211,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('location', 'LocationController@show')->middleware('roles:0,92/108/109,1,0');
        Route::get('location/{id}', 'LocationController@showId')->where('id', '[0-9]+')->middleware('roles:1,208,1,0');
        

        //Rutas tipos cliente
        Route::post('clienttype', 'ClientTypeController@create')->middleware('roles:1,80,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('clienttype', 'ClientTypeController@show')->middleware('roles:1,78,1,0');
        Route::put('clienttype', 'ClientTypeController@update')->middleware('roles:1,81,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('clienttype', 'ClientTypeController@delete')->middleware('roles:1,82,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('clienttype/{id}', 'ClientTypeController@showId')->where('id', '[0-9]+')->middleware('roles:1,79,1,0');

        //Rutas opiniones
        Route::get('comment/status', 'CommentStatusController@showStatus')->middleware('roles:0,72/108,1,0');
        Route::post('comment', 'CommentController@create')->middleware('roles:1,21,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('comment', 'CommentController@delete')->middleware('roles:1,23,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('comment', 'CommentController@show')->middleware('roles:1,20,1,0');
        Route::put('comment', 'CommentController@validated')->middleware('roles:1,24,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('comment/{id}', 'CommentController@showId')->where('id', '[0-9]+')->middleware('roles:1,22,1,0');

        //Rutas settings
        Route::get('settings/day', 'DayController@show')->middleware('roles:0,104/108/109,1,0');
        Route::get('settings', 'SettingsController@show')->middleware('roles:0,71/108/109,1,0');
        Route::put('settings', 'SettingsController@update')->middleware('roles:1,106,1,0', 'log:' . $start_petition . ',' . $input_data);

        //Rutas subcanales
        Route::get('subchannel', 'SubChannelController@show')->middleware('roles:0,73/108/109,1,0');
        Route::get('subchannel/{id}', 'SubChannelController@showId')->where('id', '[0-9]+')->middleware('roles:1,197,1,0');
        Route::post('subchannel', 'SubChannelController@create')->middleware('roles:1,198,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('subchannel', 'SubChannelController@update')->middleware('roles:1,91,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('subchannel', 'SubChannelController@delete')->middleware('roles:1,196,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('subchannel/modules', 'SubChannelController@showSubchannelModules')->middleware('roles:0,198/91/108/109,1,0');

        //Rutas canales
        Route::get('channel', 'SubChannelController@showChannels')->middleware('roles:0,199/108/109,1,0');
        Route::get('channel/{id}', 'SubChannelController@showIdChannel')->where('id', '[0-9]+')->middleware('roles:1,203,1,0');
        Route::post('channel', 'SubChannelController@createChannel')->middleware('roles:1,200,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('channel', 'SubChannelController@updateChannel')->middleware('roles:1,202,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('channel', 'SubChannelController@deleteChannel')->middleware('roles:1,201,1,0', 'log:' . $start_petition . ',' . $input_data);

        //Rutas areas
        Route::get('area/continent', 'AreaController@showContinent')->middleware('roles:0,123/108/109,1,0');
        Route::get('area/country', 'AreaController@showCountry')->middleware('roles:0,124/108/109,1,0');
        Route::get('area/state', 'AreaController@showState')->middleware('roles:0,125/108/109,1,0');
        //FIN rutas areas

        //Rutas disponibilidades
        Route::get('availability/calendar', 'AvailabilityController@showCalendar')->middleware('roles:1,145,0,1');
        Route::put('availability/day', 'AvailabilityController@updateDay')->middleware('roles:1,105,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::post('availability', 'AvailabilityController@create')->middleware('roles:1,12,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('availability', 'AvailabilityController@delete')->middleware('roles:1,13,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('availability', 'AvailabilityController@update')->middleware('roles:1,11,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('availability', 'AvailabilityController@show')->middleware('roles:1,10,1,0');
        Route::get('availability/{id}', 'AvailabilityController@showId')->where('id', '[0-9]+')->middleware('roles:0,103/108,1,0');
        //FIN Rutas disponibilidades

        //Rutas contratos
        Route::post('contract', 'ContractController@create')->middleware('roles:1,204,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('contract', 'ContractController@delete')->middleware('roles:1,205,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('contract', 'ContractController@update')->middleware('roles:1,206,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('contract/{id}', 'ContractController@showId')->where('id', '[0-9]+')->middleware('roles:1,207,1,0');
        Route::get('contract', 'ContractController@showContracts')->middleware('roles:0,102/108,1,0');
        //Rutas configuración de contratos
        Route::get('price/contract/settings/{id}', 'ContractController@showIdSettingsContracts')->where('id', '[0-9]+')->middleware('roles:1,121,1,0');
        Route::put('price/contract/settings', 'ContractController@updateSettingsContracts')->middleware('roles:1,120,1,0', 'log:' . $start_petition . ',' . $input_data);
        //FIN Rutas contratos

        //Rutas tarifas
        Route::post('price', 'PriceController@create')->middleware('roles:1,38,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('price', 'PriceController@show')->middleware('roles:1,35,1,0');
        Route::delete('price', 'PriceController@delete')->middleware('roles:1,36,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('price', 'PriceController@create')->middleware('roles:1,107,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('price/{id}', 'PriceController@showId')->where('id', '[0-9]+')->middleware('roles:1,37,1,0');
        //FIN Rutas tarifas

        //Rutas promociones
        Route::get('promotion/types', 'PromotionTypeController@showTypes')->middleware('roles:1,44,1,0');
        Route::get('promotion/types/{id}', 'PromotionTypeController@showTypeId')->where('id', '[0-9]+')->middleware('roles:1,45,1,0');

        Route::post('promotion/campaign/duplicate', 'CampaignController@duplicateCampaignId')->middleware('roles:1,155,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::post('promotion/campaign', 'CampaignController@createCampaign')->middleware('roles:1,85,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('promotion/campaign', 'CampaignController@showCampaigns')->middleware('roles:1,86,1,0');
        Route::get('promotion/campaign/{id}', 'CampaignController@showCampaignId')->where('id', '[0-9]+')->middleware('roles:1,87,1,0');
        Route::delete('promotion/campaign', 'CampaignController@deleteCampaign')->middleware('roles:1,84,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('promotion/campaign', 'CampaignController@createCampaign')->middleware('roles:1,83,1,0', 'log:' . $start_petition . ',' . $input_data);

        Route::get('promotion/costcenter', 'CostCenterController@showCostCenters')->middleware('roles:1,153,1,0');
        Route::get('promotion/device', 'DeviceController@showDevices')->middleware('roles:1,154,1,0');
        Route::get('promotion/department', 'DepartmentController@showDepartments')->middleware('roles:1,152,1,0');

        Route::post('promotion/duplicate', 'PromotionController@duplicatePromotion')->middleware('roles:1,156,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::post('promotion', 'PromotionController@create')->middleware('roles:1,42,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('promotion', 'PromotionController@show')->middleware('roles:1,39,1,0');
        Route::delete('promotion', 'PromotionController@delete')->middleware('roles:1,40,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('promotion', 'PromotionController@create')->middleware('roles:1,43,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('promotion/{id}', 'PromotionController@showId')->where('id', '[0-9]+')->middleware('roles:1,41,1,0');
        //FIN Rutas promociones

        //Rutas roles
        Route::post('role/assign', 'RoleController@assign')->middleware('roles:1,52,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('role/permissions', 'PermissionController@showPermissions')->middleware('roles:0,53/108,1,0');
        Route::post('role', 'RoleController@create')->middleware('roles:1,49,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::put('role', 'RoleController@update')->middleware('roles:1,51,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('role', 'RoleController@delete')->middleware('roles:1,50,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('role', 'RoleController@show')->middleware('roles:0,47/108,1,0');
        Route::get('role/{id}', 'RoleController@showId')->where('id', '[0-9]+')->middleware('roles:1,48,1,0');
        //FIN Rutas roles

        //Rutas usuarios
        Route::get('user/profile', 'UserController@profile')->middleware('roles:0,122/108/109,1,0');

        Route::get('user/identifications/{id}', 'IdentificationController@showIdentificationId')->where('id', '[0-9]+')->middleware('roles:0,75/108,1,0');
        Route::get('user/identifications', 'IdentificationController@showIdentifications')->middleware('roles:0,74/108/109,1,0');

        Route::get('user/documents', 'DocumentTypeController@showDocumentTypes')->middleware('roles:0,76/108,1,0');
        Route::get('user/workers', 'WorkerTypeController@showWorkerTypes')->middleware('roles:0,77/108,1,0');

        Route::get('user/contact/pos', 'ContactController@showContactsPos')->middleware('roles:1,145,0,1');
        Route::post('user/contact', 'ContactController@create')->middleware('roles:1,66,1,0');
        Route::put('user/contact', 'ContactController@create')->middleware('roles:1,70,1,0');
        Route::get('user/contact', 'ContactController@show')->middleware('roles:1,67,1,0');
        Route::delete('user/contact', 'ContactController@delete')->middleware('roles:1,69,1,0', 'log:' . $start_petition . ',' . $input_data);
        
        Route::get('user/contact/{id}', 'ContactController@showId')->where('id', '[0-9]+')->middleware('roles:0,68/109,1,0');

        
        Route::post('user', 'UserController@create')->middleware('roles:1,66,1,0');
        Route::put('user', 'UserController@create')->middleware('roles:1,70,1,0');
        Route::get('user', 'UserController@show')->middleware('roles:1,67,1,0');
        Route::get('user/pos', 'UserController@show')->middleware('roles:1,143,0,1');
        Route::delete('user', 'UserController@delete')->middleware('roles:1,69,1,0', 'log:' . $start_petition . ',' . $input_data);

        Route::get('user/{id}', 'UserController@showId')->middleware('roles:0,68/109,1,0');
        //FIN Rutas usuarios
        
        //Rutas de visualización habitaciones
        Route::get('viewproduct/room', 'ViewRoomController@show')->middleware('roles:1,115,0,1');
        Route::put('viewproduct/room', 'ViewRoomController@update')->middleware('roles:1,118,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('viewproduct/room', 'ViewRoomController@delete')->middleware('roles:1,117,0,1', 'log:' . $start_petition . ',' . $input_data); 
        Route::get('viewproduct/room/{id}', 'ViewRoomController@showId')->where('id', '[0-9]+')->middleware('roles:1,116,0,1'); 

        //Rutas de visualización productos
        Route::get('viewproduct', 'ViewProductController@show')->middleware('roles:1,96,0,1');
        Route::delete('viewproduct', 'ViewProductController@delete')->middleware('roles:1,98,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('viewproduct', 'ViewProductController@update')->middleware('roles:1,99,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::get('viewproduct/{id}', 'ViewProductController@showId')->where('id', '[0-9]+')->middleware('roles:1,97,0,1');

        //Rutas de archivos
        Route::get('file/types', 'FileTypeController@showTypes')->middleware('roles:0,32/108/109,1,0');
        Route::get('file/types/{id}', 'FileTypeController@showTypeId')->where('id', '[0-9]+')->middleware('roles:1,33,1,0');

        Route::post('file', 'FileController@create')->middleware('roles:0,30/108/109,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('file', 'FileController@show')->middleware('roles:0,29/108/109,1,0');
        Route::put('file','FileController@update')->middleware('roles:0,88/108/109,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('file','FileController@delete')->middleware('roles:0,31/108/109,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('file/{id}', 'FileController@showId')->middleware('roles:0,89/108/109,1,0');
        
        //Rutas de emails
        Route::post('email', 'EmailController@send')->middleware('roles:1,27,1,0', 'log:' . $start_petition . ',' . $input_data);
        Route::get('email', 'EmailController@showTemplate')->middleware('roles:1,28,1,0');
        //FIN Rutas de emails

        //Rutas de buscador
        Route::get('search/pickups/pos', 'SearchController@showPickups')->middleware('roles:0,140/109,0,1');
        Route::get('search/transportationlocation', 'SearchController@showTransportationLocation')->middleware('roles:0,140/109,0,1');
        Route::get('search/pos' , 'SearchController@showSubchannel')->middleware('roles:1,134,0,1');
        Route::get('search/room/pos', 'SearchController@showRoom')->middleware('roles:1,134,0,1');
        //FIN Rutas de buscador

        //Ruta dashboard
        Route::get('dashboard', 'DashboardController@show')->middleware('roles:0,108/109,1,0');

        ////Rutas de reservas

        //Rutas reservas - promociones
        Route::post('reservation/promotion/aplicate', 'ReservationsPromotionController@autoaplicatePromotion')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('reservation/promotion', 'ReservationsPromotionController@removePromotion')->middleware('roles:1,158,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::get('reservation/promotion', 'ReservationsPromotionController@showPromotions')->middleware('roles:1,158,0,1');

        //Rutas reservas - métodos de pago
        Route::get('reservation/payment_method', 'ReservationsPaymentMethodController@showPaymentMethod')->middleware('roles:0,126/108/109,1,0');
        Route::get('reservation/payment_method/mop', 'ReservationsPaymentMethodController@paymentMethodsMop')->middleware('roles:0,139/109,0,1');
        Route::post('reservation/payment', 'ReservationsPaymentMethodController@payment')->middleware('roles:1,146,0,1', 'log:' . $start_petition . ',' . $input_data); 

        //Rutas reservas - seguro
        Route::post('reservation/insurance', 'ReservationInsuranceController@assignInsurance')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('reservation/insurance', 'ReservationInsuranceController@unassignInsurance')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);

        //Rutas reservas - reserva
        Route::get('reservation/operation_status', 'ReservationsController@showOperationStatus')->middleware('roles:0,131/109,0,1');
        Route::get('reservation/payment_status', 'ReservationsController@showPaymentStatus')->middleware('roles:0,132/109,0,1');
        Route::get('reservation/payments', 'ReservationsController@showPayments')->middleware('roles:1,145,0,1');
        Route::put('reservation/updatereservationclient', 'ReservationsController@updateReservationClient')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/updatereservationuser', 'ReservationsController@updateReservationUser')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/updatereservationcurrency', 'ReservationsController@updateReservationCurrency')->middleware('roles:1,149,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/changecommissionagent', 'ReservationsController@changeCommissionAgent')->middleware('roles:1,136,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/recover', 'ReservationsController@recoverReservation')->middleware('roles:1,150,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/cancel', 'ReservationsController@cancel')->middleware('roles:1,151,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/pickup', 'ReservationsController@setReservationPickup')->middleware('roles:1,157,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/shuttlehotel', 'ReservationsController@setShuttleHotelPickup')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/notes', 'ReservationsController@setNotes')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation','ReservationsController@updateReservation')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::put('reservation/secondstep', 'ReservationsController@secondStep')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::delete('reservation', 'ReservationsController@delete')->middleware('roles:1,130,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/discount', 'ReservationsController@applyDiscount')->middleware('roles:0,138/109,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/sendemail', 'ReservationsController@sendReservationMail')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::post('reservation/sendconfirmedemail', 'ReservationsController@sendConfirmedReservationMail')->middleware('roles:1,145,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::post('reservation', 'ReservationsController@create')->middleware('roles:1,129,0,1', 'log:' . $start_petition . ',' . $input_data);
        Route::get('reservation', 'ReservationsController@show')->middleware('roles:1,127,0,1');
        Route::get('reservation/shoppingcart', 'ReservationsController@showId')->middleware('roles:1,128,0,1');

        //Rutas paradas transporte   
        Route::post('transportationlocationstop', 'TransportationLocationDetailController@create')->middleware('roles:1,179,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::delete('transportationlocationstop', 'TransportationLocationDetailController@delete')->middleware('roles:1,181,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::put('transportationlocationstop', 'TransportationLocationDetailController@update')->middleware('roles:1,182,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::get('transportationlocationstop/{id}', 'TransportationLocationDetailController@showId')->where('id', '[0-9]+')->middleware('roles:1,180,1,0');
        Route::get('transportationlocationstop', 'TransportationLocationDetailController@show')->middleware('roles:1,178,1,0');

        //Rutas locaciones transporte   
        Route::post('transportationlocation', 'TransportationLocationController@create')->middleware('roles:1,184,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::delete('transportationlocation', 'TransportationLocationController@delete')->middleware('roles:1,185,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::put('transportationlocation', 'TransportationLocationController@update')->middleware('roles:1,186,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::get('transportationlocation/{id}', 'TransportationLocationController@showId')->where('id', '[0-9]+')->middleware('roles:1,187,1,0');
        Route::get('transportationlocation', 'TransportationLocationController@show')->middleware('roles:0,183/108/109,1,0');

        //Rutas compañías
        Route::post('company', 'CompanyController@create')->middleware('roles:1,188,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::delete('company', 'CompanyController@delete')->middleware('roles:1,189,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::put('company', 'CompanyController@update')->middleware('roles:1,190,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::get('company/{id}', 'CompanyController@showId')->where('id', '[0-9]+')->middleware('roles:1,191,1,0');
        Route::get('company', 'CompanyController@show')->middleware('roles:0,90/108/109,1,0');

        //Rutas suppliers
        Route::post('supplier', 'SupplierController@create')->middleware('roles:1,192,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::delete('supplier', 'SupplierController@delete')->middleware('roles:1,193,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::put('supplier', 'SupplierController@update')->middleware('roles:1,194,1,0', 'log:' . $start_petition . ',' . $input_data); 
        Route::get('supplier/{id}', 'SupplierController@showId')->where('id', '[0-9]+')->middleware('roles:1,195,1,0');
        Route::get('supplier', 'SupplierController@show')->middleware('roles:0,95/108/109,1,0');
        
        //Rutas especiales de API
        Route::get('cpcommonrequests', 'GeneralController@cpcommon')->middleware('roles:0,108/109,1,0');
        //Route::post('valid_token', 'GeneralController@valid_token');


        //Rutas legales
        Route::put('legal', 'WebLegalController@update')->middleware('roles:1,218,1,0');
        Route::get('legal', 'WebLegalController@show')->middleware('roles:1,216,1,0');
        Route::get('legal/{id}', 'WebLegalController@showId')->where('id', '[0-9]+')->middleware('roles:1,217,1,0');

        //Rutas literales
        Route::put('literal', 'WebLiteralController@update')->middleware('roles:1,221,1,0');
        Route::get('literal', 'WebLiteralController@show')->middleware('roles:1,219,1,0');
        Route::get('literal/{id}', 'WebLiteralController@showId')->where('id', '[0-9]+')->middleware('roles:1,220,1,0');

        //Rutas redes sociales
        Route::put('socialnetwork', 'WebSocialNetworkController@update')->middleware('roles:1,223,1,0');
        Route::get('socialnetwork', 'WebSocialNetworkController@show')->middleware('roles:1,222,1,0');
        Route::get('socialnetwork/{id}', 'WebSocialNetworkController@showId')->where('id', '[0-9]+')->middleware('roles:1,224,1,0');

        //Rutas atención al cliente
        Route::post('customersupport', 'WebCustomerSupportController@create')->middleware('roles:1,228,1,0');
        Route::put('customersupport', 'WebCustomerSupportController@update')->middleware('roles:1,227,1,0');
        Route::delete('customersupport', 'WebCustomerSupportController@delete')->middleware('roles:1,229,1,0');
        Route::get('customersupport', 'WebCustomerSupportController@show')->middleware('roles:1,225,1,0');
        Route::get('customersupport/{id}', 'WebCustomerSupportController@showId')->where('id', '[0-9]+')->middleware('roles:1,226,1,0');

        //Rutas páginas front de venta
        Route::put('webpage', 'WebPageController@update')->middleware('roles:1,232,1,0');
        Route::get('webpage', 'WebPageController@show')->middleware('roles:1,230,1,0');
        Route::get('webpage/{id}', 'WebPageController@showId')->where('id', '[0-9]+')->middleware('roles:1,231,1,0');

        //Rutas slider
        Route::put('slider', 'WebSliderController@update')->middleware('roles:1,235,1,0');
        Route::get('slider', 'WebSliderController@show')->middleware('roles:1,233,1,0');
        Route::get('slider/{id}', 'WebSliderController@showId')->where('id', '[0-9]+')->middleware('roles:1,234,1,0');
    });

});