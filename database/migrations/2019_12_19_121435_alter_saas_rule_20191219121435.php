<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasRule20191219121435 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saas_rule', function (Blueprint $table) {
            $table->decimal('sale_price', 20, 5)->nullable()->after('priority');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saas_rule', function (Blueprint $table) {
            $table->dropcolumn('sale_price');
        });

    }
}
