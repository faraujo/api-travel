<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaasWebsite20191022010107 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('saas_website', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('saas_user');
			$table->biginteger('website_id')->unsigned()->nullable();
			$table->foreign('website_id')->references('id')->on('websites');
			$table->integer('status_id')->unsigned()->nullable()->comment('1:Pendiente de Pago // 2:Asistente // 3:Pendiente de Desplegar // 4:Desplegado');
			$table->string('name')->nullable();
			$table->string('domain')->nullable();
			$table->string('nif')->nullable();
			$table->integer('default_language_id')->unsigned()->nullable();
			$table->foreign('default_language_id')->references('id')->on('saas_language');
			$table->integer('default_currency_id')->unsigned()->nullable();
			$table->foreign('default_currency_id')->references('id')->on('saas_currency');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('saas_website');
	}

}
