<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasWebsitePlanRule20200120153200 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_website_plan_rule', function(Blueprint $table)
		{
            $table->boolean('cancelled')->after('status_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_website_plan_rule', function(Blueprint $table)
		{
            $table->dropColumn('cancelled');
        });
    }
}
