<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasPayment20200120121700 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_payment', function(Blueprint $table)
		{
            $table->string('profileid_suscription_paypal')->after('response_suscription_paypal')->nullable();
            $table->boolean('suscription_status')->after('status_ok')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_payment', function (Blueprint $table) {
            $table->dropColumn('profileid_suscription_paypal');
            $table->dropColumn('suscription_status');
        });
    }
}
