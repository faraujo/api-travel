<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('saas_currency')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Dolar Estadounidense',
                'simbol' => '$',
                'iso_number' => 840,
                'iso_code' => 'USD',
                'iso2' => 'USD',
                'presentation_simbol' => 'US$',
                'position' => 'I',
                'decimal_separator' => '.',
                'thousand_separator' => ',',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 2,
                'name' => 'Peso mexicano',
                'simbol' => '$',
                'iso_number' => 484,
                'iso_code' => 'MXN',
                'iso2' => 'MXPESO',
                'presentation_simbol' => 'MXN$',
                'position' => 'I',
                'decimal_separator' => '.',
                'thousand_separator' => '',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 3,
                'name' => 'Euro',
                'simbol' => '€',
                'iso_number' => 978,
                'iso_code' => 'EUR',
                'iso2' => 'Euro',
                'presentation_simbol' => '€',
                'position' => 'D',
                'decimal_separator' => ',',
                'thousand_separator' => '',
                'automatic_conversion' => 1,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 4,
                'name' => 'Libra',
                'simbol' => '£',
                'iso_number' => 826,
                'iso_code' => 'GBP',
                'iso2' => 'Libra Esterlina',
                'presentation_simbol' => '£',
                'position' => 'D',
                'decimal_separator' => '.',
                'thousand_separator' => ',',
                'automatic_conversion' => 1,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 5,
                'name' => 'Dólar Canadiense',
                'simbol' => '$',
                'iso_number' => 124,
                'iso_code' => 'CAD',
                'iso2' => 'Dolar Canadiense',
                'presentation_simbol' => '$',
                'position' => 'D',
                'decimal_separator' => '.',
                'thousand_separator' => ',',
                'automatic_conversion' => 1,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 6,
                'name' => 'Peso colombiano',
                'simbol' => '$',
                'iso_number' => 170,
                'iso_code' => 'COP',
                'iso2' => 'Peso Colombiano',
                'presentation_simbol' => '$',
                'position' => 'D',
                'decimal_separator' => ',',
                'thousand_separator' => '.',
                'automatic_conversion' => 1,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 7,
                'name' => 'Peso chileno',
                'simbol' => '$',
                'iso_number' => 152,
                'iso_code' => 'CLP',
                'iso2' => 'Peso Chileno',
                'presentation_simbol' => '$',
                'position' => 'D',
                'decimal_separator' => '',
                'thousand_separator' => '.',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 8,
                'name' => 'Peso uruguayo',
                'simbol' => '$',
                'iso_number' => 858,
                'iso_code' => 'UYU',
                'iso2' => 'Peso Uruguayo',
                'presentation_simbol' => '$',
                'position' => 'D',
                'decimal_separator' => ',',
                'thousand_separator' => '',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 9,
                'name' => 'Rublo ruso',
                'simbol' => 'руб.',
                'iso_number' => 643,
                'iso_code' => 'RUB',
                'iso2' => 'Rublo',
                'presentation_simbol' => 'руб.',
                'position' => 'D',
                'decimal_separator' => ',',
                'thousand_separator' => '.',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 10,
                'name' => 'Yuan chino',
                'simbol' => '¥',
                'iso_number' => 156,
                'iso_code' => 'CNY',
                'iso2' => 'Yuán',
                'presentation_simbol' => '¥',
                'position' => 'D',
                'decimal_separator' => '.',
                'thousand_separator' => ',',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 11,
                'name' => 'Won surcoreano',
                'simbol' => '₩',
                'iso_number' => 410,
                'iso_code' => 'KRW',
                'iso2' => 'Won Surcoreano',
                'presentation_simbol' => '₩',
                'position' => 'D',
                'decimal_separator' => '',
                'thousand_separator' => ',',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 12,
                'name' => 'Quetzal guatemalteco',
                'simbol' => 'Q',
                'iso_number' => 320,
                'iso_code' => 'GTQ',
                'iso2' => 'Quetzal',
                'presentation_simbol' => 'Q',
                'position' => 'D',
                'decimal_separator' => '.',
                'thousand_separator' => ',',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 13,
                'name' => 'Nuevo sol peruano',
                'simbol' => 'S/.',
                'iso_number' => 604,
                'iso_code' => 'PEN',
                'iso2' => 'Peso Chileno',
                'presentation_simbol' => 'S/.',
                'position' => 'D',
                'decimal_separator' => '.',
                'thousand_separator' => ',',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 14,
                'name' => 'Colón costarricense',
                'simbol' => '₡',
                'iso_number' => 188,
                'iso_code' => 'CRC',
                'iso2' => 'Colón',
                'presentation_simbol' => '₡',
                'position' => 'D',
                'decimal_separator' => ',',
                'thousand_separator' => '.',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 15,
                'name' => 'Dolar australiano',
                'simbol' => '$₡',
                'iso_number' => 36,
                'iso_code' => 'AUD',
                'iso2' => 'Dólar Australiano',
                'presentation_simbol' => '$',
                'position' => 'D',
                'decimal_separator' => '.',
                'thousand_separator' => '',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 16,
                'name' => 'Yen japonés',
                'simbol' => '¥',
                'iso_number' => 392,
                'iso_code' => 'JPY',
                'iso2' => 'Yen',
                'presentation_simbol' => '¥',
                'position' => 'D',
                'decimal_separator' => '',
                'thousand_separator' => ',',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 17,
                'name' => 'Peso argentino',
                'simbol' => '$',
                'iso_number' => 32,
                'iso_code' => 'ARS',
                'iso2' => 'Peso Argentino',
                'presentation_simbol' => '$',
                'position' => 'D',
                'decimal_separator' => ',',
                'thousand_separator' => '.',
                'automatic_conversion' => 0,
                'automatic_service' => NULL,
                'automatic_environment' => NULL,
                'automatic_user' => NULL,
                'automatic_password' => NULL,
                'automatic_token' => NULL,
                'deleted_at' => NULL,
            ),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
