<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('saas_language')->insert(array (
            
            array (
                'id' => 1,
                'name_lang' => 'spanish',
                'abbreviation' => 'ES',
                'flag' => 'spanish.gif',
                'front' => 1,
                'back' => 1,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 2,
                'name_lang' => 'english',
                'abbreviation' => 'EN',
                'flag' => 'english.gif',
                'front' => 1,
                'back' => 1,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 3,
                'name_lang' => 'portuguese',
                'abbreviation' => 'PT',
                'flag' => 'portuguese.gif',
                'front' => 1,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 4,
                'name_lang' => 'french',
                'abbreviation' => 'FR',
                'flag' => 'french.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 5,
                'name_lang' => 'german',
                'abbreviation' => 'DE',
                'flag' => 'german.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 6,
                'name_lang' => 'chinese',
                'abbreviation' => 'ZH',
                'flag' => 'chinese.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 7,
                'name_lang' => 'rusian',
                'abbreviation' => 'RU',
                'flag' => 'rusian.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 8,
                'name_lang' => 'italian',
                'abbreviation' => 'IT',
                'flag' => 'italian.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 9,
                'name_lang' => 'japanese',
                'abbreviation' => 'JA',
                'flag' => 'japanese.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 10,
                'name_lang' => 'dutch',
                'abbreviation' => 'NL',
                'flag' => 'dutch.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 11,
                'name_lang' => 'slovak',
                'abbreviation' => 'SK',
                'flag' => 'slovak.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 12,
                'name_lang' => 'czech',
                'abbreviation' => 'CS',
                'flag' => 'czech.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 13,
                'name_lang' => 'estonian',
                'abbreviation' => 'ET',
                'flag' => 'estonian.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 14,
                'name_lang' => 'finnish',
                'abbreviation' => 'FI',
                'flag' => 'finnish.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
            
            array (
                'id' => 15,
                'name_lang' => 'croatian',
                'abbreviation' => 'HR',
                'flag' => 'croatian.gif',
                'front' => 0,
                'back' => 0,
                'deleted_at' => NULL,
            ),
		));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
