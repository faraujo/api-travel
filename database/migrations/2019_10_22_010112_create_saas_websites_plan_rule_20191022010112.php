<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaasWebsitesPlanRule20191022010112 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('saas_website_plan_rule', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('website_id')->unsigned()->nullable();
			$table->foreign('website_id')->references('id')->on('saas_website');
			$table->integer('plan_id')->unsigned()->nullable();
			$table->foreign('plan_id')->references('id')->on('saas_plan');
			$table->integer('rule_id')->unsigned()->nullable();
			$table->foreign('rule_id')->references('id')->on('saas_rule');
			$table->integer('value')->unsigned()->nullable();
			$table->boolean('unlimited')->default(0);
			$table->decimal('sale_price', 20, 5)->nullable();	
			$table->string('code')->nullable();
			$table->dateTime('date_start')->nullable();
			$table->dateTime('date_end')->nullable();
			$table->dateTime('date_replace')->nullable();
			$table->integer('status_id')->unsigned()->nullable()->comment('1:Pendiente de Pago // 2:Pagado');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('saas_website_plan_rule');
	}

}
