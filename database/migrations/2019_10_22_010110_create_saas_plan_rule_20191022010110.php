<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaasPlanRule20191022010110 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('saas_plan_rule', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('plan_id')->unsigned()->nullable();
			$table->foreign('plan_id')->references('id')->on('saas_plan');
			$table->integer('rule_id')->unsigned()->nullable();
			$table->foreign('rule_id')->references('id')->on('saas_rule');
			$table->integer('value')->unsigned()->nullable();
			$table->boolean('unlimited')->default(0);
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('saas_plan_rule');
	}

}
