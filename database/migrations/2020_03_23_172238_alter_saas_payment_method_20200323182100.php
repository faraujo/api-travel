<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasPaymentMethod20200323182100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_payment_method', function (Blueprint $table) {
            $table->string('redsys_merchant_code')->after('paypal_secret')->nullable();
            $table->string('redsys_merchant_terminal')->after('redsys_merchant_code')->nullable();
            $table->string('redsys_merchant_secret')->after('redsys_merchant_terminal')->nullable();
            $table->string('redsys_enviroment')->after('redsys_merchant_secret')->nullable();
            $table->string('redsys_recurrent_endpoint')->after('redsys_enviroment')->nullable();
        });

        DB::table('saas_payment_method')->insert([
            'type' => 'tarjeta',
            'redsys_merchant_code' => '159089226',
            'redsys_merchant_terminal' => '002',
            'redsys_merchant_secret' => 'sq7HjrUOBfKmC576ILgskD5srU870gJ7',
            'redsys_enviroment' => 'test',
            'redsys_recurrent_endpoint' => 'https://sis-t.redsys.es:25443/sis/rest/trataPeticionREST',

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_payment_method', function (Blueprint $table) {
            $table->dropColumn('redsys_merchant_code');
            $table->dropColumn('redsys_merchant_terminal');
            $table->dropColumn('redsys_merchant_secret');
            $table->dropColumn('redsys_enviroment');
            $table->dropColumn('redsys_recurrent_endpoint');
        });

    }
}
