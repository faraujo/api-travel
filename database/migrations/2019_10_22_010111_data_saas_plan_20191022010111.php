<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DataSaasPlan20191022010111 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('saas_plan')->insert(array (
            
            array (
                'id' => 1,
				'code' => 'FREE',
				'sale_price' => 0,
				'priority' => 1,
            ),
            
            array (
                'id' => 2,
				'code' => 'STARTER',
				'sale_price' => 29,
				'priority' => 2,
			),
			
			array (
                'id' => 3,
				'code' => 'ADVANCED',
				'sale_price' => 49,
				'priority' => 3,
            )
		));

		// RULES
		DB::table('saas_rule')->insert(array (
            
            array (
				'id' => 1,
				'code' => 'user_admin',
			),
            
            array (
				'id' => 2,
            	'code' => 'user_other',
			),

			array (
				'id' => 3,
            	'code' => 'user_role',
			),
			
			array (
				'id' => 4,
            	'code' => 'user_social_login',
			),
			
			array (
				'id' => 5,
            	'code' => 'search',
			),
			
			array (
				'id' => 6,
            	'code' => 'conf_language',
			),
			
			array (
				'id' => 7,
            	'code' => 'conf_currencie',
			),
			
			array (
				'id' => 8,
            	'code' => 'products',
			),
			
			array (
				'id' => 9,
            	'code' => 'product_hotel',
			),
			
			array (
				'id' => 10,
            	'code' => 'product_benefitcard',
			),
			
			array (
				'id' => 11,
            	'code' => 'product_availability',
			),
			
			array (
				'id' => 12,
            	'code' => 'product_category',
			),
			
			array (
				'id' => 13,
            	'code' => 'product_company',
			),

			array (
				'id' => 14,
            	'code' => 'product_location',
			),

			array (
				'id' => 15,
            	'code' => 'product_service',
			),

			array (
				'id' => 16,
            	'code' => 'product_file',
			),

			array (
				'id' => 17,
            	'code' => 'tag',
			),

			array (
				'id' => 18,
            	'code' => 'contract',
			),

			array (
				'id' => 19,
            	'code' => 'price',
			),

			array (
				'id' => 20,
            	'code' => 'promo_campaign',
			),

			array (
				'id' => 21,
            	'code' => 'promo_promo',
			),

			array (
				'id' => 22,
            	'code' => 'pos',
			),

			array (
				'id' => 23,
            	'code' => 'bi',
			),

			array (
				'id' => 24,
            	'code' => 'subchannel',
			),

			array (
				'id' => 25,
            	'code' => 'view_product',
			),
		));

		// FREE
		DB::table('saas_plan_rule')->insert(array (
            
            array (
                'plan_id' => 1,
				'rule_id' => 1,
				'value' => 1,
				'unlimited' => 0,
            ),
            
            array (
                'plan_id' => 1,
				'rule_id' => 2,
				'value' => 0,
				'unlimited' => 0,
            ),

			array (
                'plan_id' => 1,
				'rule_id' => 3,
				'value' => 0,
				'unlimited' => 1,
            ),
			
			array (
                'plan_id' => 1,
				'rule_id' => 4,
				'value' => 0,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 5,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 6,
				'value' => 1,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 7,
				'value' => 1,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 8,
				'value' => 20,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 9,
				'value' => 1,
				'unlimited' => 0,
			),
			
			
			
			array (
                'plan_id' => 1,
				'rule_id' => 10,
				'value' => 0,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 11,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 12,
				'value' => 5,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 1,
				'rule_id' => 13,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 14,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 15,
				'value' => 0,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 16,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 17,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 18,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 19,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 20,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 21,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 22,
				'value' => 0,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 23,
				'value' => 0,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 24,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 1,
				'rule_id' => 25,
				'value' => 0,
				'unlimited' => 0,
			),


		));
		
		// STARTED
		DB::table('saas_plan_rule')->insert(array (
            
            array (
                'plan_id' => 2,
				'rule_id' => 1,
				'value' => 1,
				'unlimited' => 0,
            ),
            
            array (
                'plan_id' => 2,
				'rule_id' => 2,
				'value' => 20,
				'unlimited' => 0,
            ),

			array (
                'plan_id' => 2,
				'rule_id' => 3,
				'value' => 0,
				'unlimited' => 1,
            ),
			
			array (
                'plan_id' => 2,
				'rule_id' => 4,
				'value' => 0,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 5,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 6,
				'value' => 3,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 7,
				'value' => 2,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 8,
				'value' => 40,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 9,
				'value' => 10,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 10,
				'value' => 0,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 11,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 12,
				'value' => 20,
				'unlimited' => 0,
			),
			
			array (
                'plan_id' => 2,
				'rule_id' => 13,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 14,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 15,
				'value' => 0,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 16,
				'value' => 3,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 17,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 18,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 19,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 20,
				'value' => 2,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 21,
				'value' => 5,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 22,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 23,
				'value' => 0,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 24,
				'value' => 1,
				'unlimited' => 0,
			),

			array (
                'plan_id' => 2,
				'rule_id' => 25,
				'value' => 0,
				'unlimited' => 0,
			),
		));
		
		// ADVANTAGE
		DB::table('saas_plan_rule')->insert(array (
            
            array (
                'plan_id' => 3,
				'rule_id' => 1,
				'value' => 0,
				'unlimited' => 1,
            ),
            
            array (
                'plan_id' => 3,
				'rule_id' => 2,
				'value' => 0,
				'unlimited' => 1,
            ),

			array (
                'plan_id' => 3,
				'rule_id' => 3,
				'value' => 0,
				'unlimited' => 1,
            ),
			
			array (
                'plan_id' => 3,
				'rule_id' => 4,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 5,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 6,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 7,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 8,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 9,
				'value' => 0,
				'unlimited' => 1,
			),
			
			
			
			array (
                'plan_id' => 3,
				'rule_id' => 10,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 11,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 12,
				'value' => 0,
				'unlimited' => 1,
			),
			
			array (
                'plan_id' => 3,
				'rule_id' => 13,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 14,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 15,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 16,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 17,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 18,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 19,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 20,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 21,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 22,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 23,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 24,
				'value' => 0,
				'unlimited' => 1,
			),

			array (
                'plan_id' => 3,
				'rule_id' => 25,
				'value' => 0,
				'unlimited' => 1,
			),
        ));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
