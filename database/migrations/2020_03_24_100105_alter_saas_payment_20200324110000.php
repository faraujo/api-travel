<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasPayment20200324110000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_payment', function (Blueprint $table) {
            $table->longText('request_redsys')->nullable()->after('response_suscription_paypal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_payment', function (Blueprint $table) {
            $table->dropColumn('request_redsys');
        });
    }
}
