<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasSettings20200319134800 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('saas_settings')->insert([
            'name' => 'redsys_recurrent_endpoint',
            'value' => 'https://sis-t.redsys.es:25443/sis/rest/trataPeticionREST'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
