<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasSettings20200324062800 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('saas_settings')->where('name', 'redsys_merchant_code')->delete();
        DB::table('saas_settings')->where('name', 'redsys_merchant_terminal')->delete();
        DB::table('saas_settings')->where('name', 'redsys_merchant_secret')->delete();
        DB::table('saas_settings')->where('name', 'redsys_enviroment')->delete();
        DB::table('saas_settings')->where('name', 'redsys_recurrent_endpoint')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
