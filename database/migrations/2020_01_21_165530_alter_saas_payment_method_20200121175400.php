<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasPaymentMethod20200121175400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_payment_method', function(Blueprint $table)
		{
            $table->renameColumn('api_username', 'paypal_username');
            $table->renameColumn('api_password', 'paypal_password');
            $table->renameColumn('api_secret', 'paypal_secret');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_payment_method', function(Blueprint $table)
		{
            $table->renameColumn('paypal_username', 'api_username');
            $table->renameColumn('paypal_password', 'api_password');
            $table->renameColumn('paypal_secret', 'api_secret');
        });
    }
}
