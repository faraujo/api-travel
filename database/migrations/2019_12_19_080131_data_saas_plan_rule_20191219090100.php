<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasPlanRule20191219090100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('saas_plan_rule')->where('plan_id',1)->where('rule_id', 13)->update([
            'value'=> 5
        ]);

        DB::table('saas_plan_rule')->where('plan_id',2)->where('rule_id', 13)->update([
            'value'=> 5
        ]);

        DB::table('saas_plan_rule')->where('plan_id',2)->where('rule_id', 15)->update([
            'unlimited'=> 1
        ]);

        DB::table('saas_plan_rule')->where('plan_id',2)->where('rule_id', 18)->update([
            'value'=> 3
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
