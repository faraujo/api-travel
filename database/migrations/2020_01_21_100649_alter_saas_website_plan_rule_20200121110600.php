<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasWebsitePlanRule20200121110600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_website_plan_rule', function(Blueprint $table)
		{
            $table->string('suscription_code')->after('cancelled')->nullable();
            $table->integer('payment_method_id')->after('suscription_code')->nullable()->unsigned();
            $table->foreign('payment_method_id')->references('id')->on('saas_payment_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_website_plan_rule', function(Blueprint $table)
		{
            $table->dropColumn('suscription_code');
            $table->dropForeign('saas_website_plan_rule_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
        });
    }
}
