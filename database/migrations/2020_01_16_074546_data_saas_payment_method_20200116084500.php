<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasPaymentMethod20200116084500 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::table('saas_payment_method')->insert([
            'type' => 'paypal'
        ]);

        DB::table('saas_payment_method_translation')->insert([
            'payment_method_id' => 1,
            'language_id' => 1,
            'name' => 'Paypal'
        ]);

        DB::table('saas_payment_method_translation')->insert([
            'payment_method_id' => 1,
            'language_id' => 2,
            'name' => 'Paypal'
        ]);

        DB::table('saas_payment_method_translation')->insert([
            'payment_method_id' => 1,
            'language_id' => 3,
            'name' => 'Paypal'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
