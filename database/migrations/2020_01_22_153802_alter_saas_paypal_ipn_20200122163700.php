<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasPaypalIpn20200122163700 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_paypal_ipn', function (Blueprint $table) {
            $table->longtext('paypal_response')->nullable()->after('paypal_request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_paypal_ipn', function (Blueprint $table) {
            $table->dropColumn('paypal_response');
        });
    }
}
