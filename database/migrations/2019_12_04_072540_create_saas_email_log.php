<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaasEmailLog extends Migration
{
/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('saas_email_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('date')->nullable();
			$table->string('from')->nullable();
			$table->string('to')->nullable();
			$table->string('cc')->nullable();
			$table->string('bcc')->nullable();
			$table->string('subject')->nullable();
			$table->text('body', 65535)->nullable();
			$table->text('headers', 65535)->nullable();
			$table->text('attachments', 65535)->nullable();
			$table->text('description_error')->nullable();
			$table->boolean('received_email')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('saas_email_log');
	}
}
