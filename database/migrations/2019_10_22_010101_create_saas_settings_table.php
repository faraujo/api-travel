<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaasSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('saas_settings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('value')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
		});

		DB::table('saas_settings')->insert(array (
            
            array (
                'name' => 'nombre_cliente',
                'value' => 'Viavox Experience',
            ),
            
            array (
                'name' => 'maximo_accesos_failed',
                'value' => '5',
            ),
            
            array (
                'name' => 'captcha',
                'value' => '1',
            ),
            
            array (
                'name' => 'email_error_sistema',
                'value' => 'e-monitor@viavox.com',
            ),
            
            array (
                'name' => 'send_email_error',
                'value' => '1',
            ),
            
            array (
                'name' => 'send_slack_error',
                'value' => '1',
            ),
            
            array (
                'name' => 'slack_webhook_error_sistema',
                'value' => 'https://hooks.slack.com/services/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',
            ),
            
            array (
                'name' => 'slack_channel_error_sistema',
                'value' => '#viavox_travel_mtr_noti',
            ),
            
            array (
                'name' => 'email_host_default',
                'value' => 'mail.viavox.com',
            ),
            
            array (
                'name' => 'email_username_default',
                'value' => 'info@viavoxexperience.com',
            ),
            
            array (
                'name' => 'email_password_default',
                'value' => 'change-this',
            ),
            
            array (
                'name' => 'email_from_default',
                'value' => 'info@viavoxexperience.com',
            ),
            
            array (
                'name' => 'email_fromname_default',
                'value' => 'Viavox Experience',
            ),
            
            array (
                'name' => 'email_template_default',
                'value' => 'information',
            ),
            
            array (
                'name' => 'email_copia_sistema',
                'value' => '',
            ),
            
            array (
                'name' => 'email_driver_default',
                'value' => 'smtp',
            ),
            
            array (
                'name' => 'email_port_default',
                'value' => '25',
            ),
            
            array (
                'name' => 'email_encryption_default',
                'value' => '',
            ),
            
            array (
                'name' => 'slack_webhook_default',
                'value' => 'https://hooks.slack.com/services/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',
            ),
            
            array (
                'name' => 'slack_channel_default',
                'value' => '#viavox_travel_mtr_noti',
            ),
            
            array (
                'name' => 'numero_decimales',
                'value' => '2',
            ),
            
            array (
                'name' => 'separador_decimales',
                'value' => ',',
            ),
            
            array (
                'name' => 'separador_miles',
                'value' => '.',
            ),
                       
            array (
                'name' => 'upload_max_size',
                'value' => '10485760',
            ),
            
            array (
                'name' => 'limit_registers',
                'value' => '50',
            ),
            
            array (
                'name' => 'height_unit',
                'value' => 'cm',
            ),
            
            array (
                'name' => 'weight_unit',
                'value' => 'kg',
            ),
            
            array (
                'name' => 'token_validate',
                'value' => '30',
            ),
            
            array (
                'name' => 'recover_token_validate',
                'value' => '60',
			),
			
			array (
                'name' => 'recover_route_front',
                'value' => 'https://www.viavoxexperience.loc/recover',
			),
			
			array (
                'name' => 'email_token_validate',
                'value' => '0',
			),
			
			array (
                'name' => 'validate_email_route_front',
                'value' => 'https://www.viavoxexperience.loc/validate',
			),
            
            array (
                'name' => 'time_bloqued_password',
                'value' => '60',
            ),

            array (
                'name' => 'subchannel_token',
                'value' => Str::random(16),
            ),
            

            array (
                'name' => 'tenant_database_prefix',
                'value' => 'saas_vxt_',
            ),

            array (
                'name' => 'cron_create_tenant',
                'value' => '60',
            ),

            array (
                'name' => 'base_path',
                'value' => 'mtr.viavoxexperience.loc',
            ),

            array (
                'name' => 'base_path_control_panel',
                'value' => 'cp.viavoxexperience.loc',
            ),

            array (
                'name' => 'protocol',
                'value' => 'https://',
            ),

            array(
                'name' => 'heimdall_middleware',
                'value' => 1,
            ),
            
        ));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('saas_settings');
	}

}
