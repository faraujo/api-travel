<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasSettings20200316104500 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('saas_settings')->insert([
            'name' => 'redsys_merchant_code',
            'value' => '159089226'
        ]);

        DB::table('saas_settings')->insert([
            'name' => 'redsys_merchant_terminal',
            'value' => '002'
        ]);

        DB::table('saas_settings')->insert([
            'name' => 'redsys_merchant_secret',
            'value' => 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'
        ]);

        DB::table('saas_settings')->insert([
            'name' => 'redsys_enviroment',
            'value' => 'test'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
