<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataSaasPaymentMethod20200324181200 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('saas_payment_method')->where('type', 'tarjeta')->update([
            'redsys_merchant_code' => '',
            'redsys_merchant_terminal' => '',
            'redsys_merchant_secret' => '',
            'redsys_enviroment' => '',
            'redsys_recurrent_endpoint' => '',

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
