<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaasPayment202014011600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saas_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_method');
            $table->integer('currency_id')->unsigned()->nullable();
            $table->foreign('currency_id')->references('id')->on('saas_currency');
            $table->integer('website_id')->unsigned()->nullable();
            $table->foreign('website_id')->references('id')->on('saas_website_plan_rule');
            $table->decimal('amount', 20, 5)->nullable();
            $table->boolean('status_ok')->default(0);
            $table->longText('request_paypal')->nullable();
            $table->longText('response_paypal')->nullable();
            $table->longText('request_suscription_paypal')->nullable();
            $table->longText('response_suscription_paypal')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saas_payment');
    }
}