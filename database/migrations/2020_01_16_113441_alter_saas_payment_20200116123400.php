<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasPayment20200116123400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_payment', function(Blueprint $table)
		{
            $table->dropForeign('saas_payment_website_id_foreign');
            $table->dropColumn('website_id');
            $table->integer('website_plan_rule_id')->nullable()->unsigned()->after('currency_id');
            $table->foreign('website_plan_rule_id')->references('id')->on('saas_website_plan_rule');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_payment', function(Blueprint $table)
		{
            $table->dropForeign('saas_payment_website_plan_rule_id_foreign');
            $table->dropColumn('website_plan_rule_id');
            $table->integer('website_id')->unsigned()->nullable()->after('currency_id');
            $table->foreign('website_id')->references('id')->on('saas_website_plan_rule');
        });
    }
}
