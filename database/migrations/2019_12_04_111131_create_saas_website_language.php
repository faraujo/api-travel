<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaasWebsiteLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saas_website_language', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('website_id')->unsigned()->nullable();
			$table->foreign('website_id')->references('id')->on('saas_website');
			$table->integer('language_id')->unsigned()->nullable();
			$table->foreign('language_id')->references('id')->on('saas_language');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saas_website_language');
    }
}
