<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasWebsite20200121162200 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_website_plan_rule', function(Blueprint $table)
		{
            $table->renameColumn('suscription_code', 'subscription_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_website_plan_rule', function(Blueprint $table)
		{
            $table->renameColumn('subscription_code', 'suscription_code');
        });
    }
}
