<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasPayment20200116091700 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('saas_payment', function(Blueprint $table)
		{
            $table->dropColumn('payment_method');
            $table->integer('payment_method_id')->nullable()->unsigned()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('saas_payment', function(Blueprint $table)
		{
            $table->dropForeign('saas_payment_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
            $table->text('payment_method')->after('id');
        });
    }
}
