<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaasWebsiteCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saas_website_currency', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('website_id')->unsigned()->nullable();
			$table->foreign('website_id')->references('id')->on('saas_website');
			$table->integer('currency_id')->unsigned()->nullable();
			$table->foreign('currency_id')->references('id')->on('saas_currency');
			$table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saas_website_currency');
    }
}
