<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSaasCurrency201912191715 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saas_currency', function (Blueprint $table) {
            $table->integer('order')->nullable()->after('thousand_separator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saas_currency', function (Blueprint $table) {
            $table->dropcolumn('order');
        });
    }
}
