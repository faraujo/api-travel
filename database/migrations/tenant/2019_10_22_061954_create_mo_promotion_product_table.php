<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPromotionProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_promotion_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('promotion_id')->unsigned()->nullable()->index('mo_promotion_product_promotion_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_promotion_product_product_id_foreign');
			$table->integer('location_id')->unsigned()->nullable()->index('mo_promotion_product_location_id_foreign');
			$table->integer('min_quantity')->unsigned()->nullable();
			$table->integer('max_quantity')->nullable();
			$table->integer('series_one')->nullable();
			$table->integer('series_two')->nullable();
			$table->decimal('promotion_amount', 20, 5)->nullable();
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_promotion_product_currency_id_foreign');
			$table->integer('promotion_type_id')->unsigned()->nullable()->index('mo_promotion_product_promotion_type_id_foreign');
			$table->boolean('apply_client_type_filter')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_promotion_product');
	}

}
