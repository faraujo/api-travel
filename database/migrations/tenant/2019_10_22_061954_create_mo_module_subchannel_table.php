<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoModuleSubchannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_module_subchannel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('module_id')->unsigned()->nullable()->index('mo_module_subchannel_module_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_module_subchannel_subchannel_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_module_subchannel');
	}

}
