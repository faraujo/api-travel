<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoModule20200127103100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_module')->where('id', 19)->update([
            'name' => 'Proveedores'
        ]);

        DB::table('mo_module')->where('id', 21)->update([
            'deleted_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('mo_permission')->where('module_id', 21)->update([
            'deleted_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('mo_permission')->where('id', 90)->update([
            'name' => 'Listar proveedores',
            'description' => 'permiso para listar proveedores'
        ]);

        DB::table('mo_permission')->where('id', 188)->update([
            'name' => 'Crear proveedores',
            'description' => 'Permiso para crear proveedores'
        ]);

        DB::table('mo_permission')->where('id', 189)->update([
            'name' => 'Borrar proveedores',
            'description' => 'Permiso para borrar proveedores'
        ]);

        DB::table('mo_permission')->where('id', 190)->update([
            'name' => 'Actualizar proveedores',
            'description' => 'Permiso para actualizar proveedores'
        ]);

        DB::table('mo_permission')->where('id', 191)->update([
            'name' => 'Visualizar un proveedor',
            'description' => 'Permiso para listar un proveedor'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
