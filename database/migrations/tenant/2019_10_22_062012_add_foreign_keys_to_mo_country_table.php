<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_country', function(Blueprint $table)
		{
			$table->foreign('continent_id')->references('id')->on('mo_continent')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_country', function(Blueprint $table)
		{
			$table->dropForeign('mo_country_continent_id_foreign');
		});
	}

}
