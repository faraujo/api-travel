<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionProductTypeClientTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_product_type_client_type', function(Blueprint $table)
		{
			$table->foreign('client_type_id')->references('id')->on('mo_client_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_product_type_id', 'prom_prod_type_client_type')->references('id')->on('mo_promotion_product_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_product_type_client_type', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_product_type_client_type_client_type_id_foreign');
			$table->dropForeign('prom_prod_type_client_type');
		});
	}

}
