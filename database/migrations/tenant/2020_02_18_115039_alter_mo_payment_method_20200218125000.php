<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMoPaymentMethod20200218125000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('mo_payment_method', function (Blueprint $table) {
            $table->text('url_image')->nullable()->after('type');
            $table->boolean('front')->default(0)->after('url_image');
            $table->boolean('back')->default(0)->after('front');
        });

        DB::table('mo_payment_method')->insert([
            'api_code' => 'PP',
            'type' => 'PayPal',
            'front' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mo_payment_method', function (Blueprint $table) {
            $table->dropcolumn('url_image');
            $table->dropcolumn('front');
            $table->dropcolumn('back');
        });
    }
}
