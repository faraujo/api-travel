<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoChannelTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_channel_translation', function(Blueprint $table)
		{
			$table->foreign('channel_id')->references('id')->on('mo_channel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_channel_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_channel_translation_channel_id_foreign');
			$table->dropForeign('mo_channel_translation_language_id_foreign');
		});
	}

}
