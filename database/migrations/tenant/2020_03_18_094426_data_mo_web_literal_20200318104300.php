<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoWebLiteral20200318104300 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_web_literal')->where('id', 1)->update([
            'name' => "Home - Cabecera de productos",
        ]);

        DB::table('mo_web_literal')->where('id', 2)->update([
            'name' => "Home - Cabecera de cambinaciones"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
