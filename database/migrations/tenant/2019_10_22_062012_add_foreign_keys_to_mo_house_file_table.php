<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHouseFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_house_file', function(Blueprint $table)
		{
			$table->foreign('file_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('house_id')->references('id')->on('mo_house')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_house_file', function(Blueprint $table)
		{
			$table->dropForeign('mo_house_file_file_id_foreign');
			$table->dropForeign('mo_house_file_house_id_foreign');
		});
	}

}
