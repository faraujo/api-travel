<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion', function(Blueprint $table)
		{
			$table->foreign('benefit_card_id')->references('id')->on('mo_identification')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('campaign_id')->references('id')->on('mo_campaign')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cost_center_id')->references('id')->on('mo_cost_center')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('department_id')->references('id')->on('mo_department')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_type_id')->references('id')->on('mo_promotion_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_benefit_card_id_foreign');
			$table->dropForeign('mo_promotion_campaign_id_foreign');
			$table->dropForeign('mo_promotion_cost_center_id_foreign');
			$table->dropForeign('mo_promotion_currency_id_foreign');
			$table->dropForeign('mo_promotion_department_id_foreign');
			$table->dropForeign('mo_promotion_promotion_type_id_foreign');
			$table->dropForeign('mo_promotion_user_id_foreign');
		});
	}

}
