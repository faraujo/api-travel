<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoTransportationLocationDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_transportation_location_detail', function(Blueprint $table)
		{
			$table->foreign('transportation_location_id', 'mo_transportation_location_detail_trans_location_id_foreign')->references('id')->on('mo_transportation_location')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id', 'mo_transportation_location_detail_product_id_foreign')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id', 'mo_transportation_location_detail_subchannel_id_foreign')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_transportation_location_detail', function(Blueprint $table)
		{
			$table->dropForeign('mo_transportation_location_detail_trans_location_id_foreign');
			$table->dropForeign('mo_transportation_location_detail_product_id_foreign');
			$table->dropForeign('mo_transportation_location_detail_subchannel_id_foreign');
		});
	}

}
