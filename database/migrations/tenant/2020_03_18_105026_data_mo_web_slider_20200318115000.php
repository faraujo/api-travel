<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoWebSlider20200318115000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_web_slider')->where('id', 1)->update([
            'name' => 'Slider Home Superior'
        ]);

        DB::table('mo_web_slider')->where('id', 2)->update([
            'name' => 'Slider Home Inferior'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
