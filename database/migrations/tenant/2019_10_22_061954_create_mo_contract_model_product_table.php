<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoContractModelProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_contract_model_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contract_model_id')->unsigned()->nullable()->index('mo_contract_model_product_contract_model_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_contract_model_product_product_id_foreign');
			$table->integer('service_id')->unsigned()->nullable()->index('mo_contract_model_product_service_id_foreign');
			$table->integer('time_expiration')->unsigned()->nullable()->comment('Tiempo para completar una reserva. Una vez tramitada la reserva se le queda guardada a falta de confirmación por parte del usuario, pasadas las horas que diga este campo si no se ha confirmado la reserva expirará. Horas.');
			$table->integer('time_limit_enjoyment')->unsigned()->nullable()->comment('Tiempo limite para reservar antes de disfrutar el producto, por ejemplo hasta 48 horas antes. Horas.');
			$table->boolean('individual_sale')->default(0);
			$table->boolean('apply_cancel_penalization')->default(0)->comment('Aplica penalización por cancelamiento');
			$table->boolean('required_service')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_contract_model_product');
	}

}
