<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->unsigned()->nullable()->index('mo_reservation_detail_reservation_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_reservation_detail_subchannel_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_reservation_detail_product_id_foreign');
			$table->string('product_name')->nullable();
			$table->string('product_code')->nullable();
			$table->integer('service_id')->unsigned()->nullable()->index('mo_reservation_detail_service_id_foreign');
			$table->string('service_name')->nullable();
			$table->integer('location_id')->unsigned()->nullable()->index('mo_reservation_detail_location_id_foreign');
			$table->string('location_name')->nullable();
			$table->integer('contract_id')->unsigned()->nullable()->index('mo_reservation_detail_contract_id_foreign');
			$table->integer('price_id')->unsigned()->nullable()->index('mo_reservation_detail_price_id_foreign');
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_reservation_detail_currency_id_foreign');
			$table->integer('currency_exchange_id')->unsigned()->nullable()->index('mo_reservation_detail_currency_exchange_id_foreign');
			$table->decimal('discount', 20, 5)->nullable();
			$table->integer('discount_user_id')->unsigned()->nullable()->index('mo_reservation_detail_discount_user_id_foreign');
			$table->decimal('iva', 20, 5)->nullable();
			$table->dateTime('date_time_start')->nullable();
			$table->dateTime('date_time_quotation')->nullable();
			$table->dateTime('date_time_processed')->nullable();
			$table->dateTime('date_time_operation')->nullable();
			$table->dateTime('date_time_cancellation')->nullable();
			$table->dateTime('date_time_expiration')->nullable();
			$table->integer('operation_status_id')->unsigned()->nullable()->index('mo_reservation_detail_operation_status_id_foreign');
			$table->integer('payment_status_id')->unsigned()->nullable()->index('mo_reservation_detail_payment_status_id_foreign');
			$table->integer('agent_id')->unsigned()->nullable()->index('mo_reservation_detail_agent_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_detail');
	}

}
