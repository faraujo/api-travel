<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoHotelRoomCategoryTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_hotel_room_category_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_hotel_room_category_translation_language_id_foreign');
			$table->integer('category_id')->unsigned()->nullable()->index('mo_hotel_room_category_translation_category_id_foreign');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_hotel_room_category_translation');
	}

}
