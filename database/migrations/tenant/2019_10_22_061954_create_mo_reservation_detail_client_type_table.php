<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationDetailClientTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_detail_client_type', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_detail_id')->unsigned()->nullable()->index('mo_reservation_client_type_reservation_detail_id_foreign');
			$table->integer('client_type_id')->unsigned()->nullable()->index('mo_reservation_client_type_client_type_id_foreign');
			$table->integer('age')->nullable();
			$table->string('client_type_name')->nullable();
			$table->integer('client_id')->unsigned()->nullable()->index('mo_reservation_client_type_client_id_foreign');
			$table->string('client_name')->nullable();
			$table->string('client_surname')->nullable();
			$table->integer('client_country_id')->unsigned()->nullable()->index('mo_reservation_detail_client_type_client_country_id_foreign');
			$table->integer('client_state_id')->unsigned()->nullable()->index('mo_reservation_detail_client_type_client_state_id_foreign');
			$table->string('client_telephone1')->nullable();
			$table->string('client_email')->nullable();
			$table->decimal('original_net_price', 20, 5)->nullable();
			$table->decimal('original_sale_price', 20, 5)->nullable();
			$table->decimal('original_markup', 20, 5)->nullable();
			$table->decimal('net_price', 20, 5)->nullable();
			$table->decimal('sale_price', 20, 5)->nullable();
			$table->decimal('markup', 20, 5)->unsigned()->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_detail_client_type');
	}

}
