<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_log', function(Blueprint $table)
		{
			$table->foreign('action_id')->references('id')->on('mo_reservation_log_action')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('agent_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_detail_id')->references('id')->on('mo_reservation_detail')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_id')->references('id')->on('mo_reservation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_log', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_log_action_id_foreign');
			$table->dropForeign('mo_reservation_log_agent_id_foreign');
			$table->dropForeign('mo_reservation_log_client_id_foreign');
			$table->dropForeign('mo_reservation_log_reservation_detail_id_foreign');
			$table->dropForeign('mo_reservation_log_reservation_id_foreign');
			$table->dropForeign('mo_reservation_log_subchannel_id_foreign');
		});
	}

}
