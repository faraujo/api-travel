<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationDetailHotelRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_detail_hotel_room', function(Blueprint $table)
		{
			$table->foreign('room_id')->references('id')->on('mo_hotel_room')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_hotel_id', 'mo_reservation_hotel_room_reservation_hotel_id_foreign')->references('id')->on('mo_reservation_detail_hotel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_detail_hotel_room', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_detail_hotel_room_room_id_foreign');
			$table->dropForeign('mo_reservation_hotel_room_reservation_hotel_id_foreign');
		});
	}

}
