<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHotelRoomCategoryTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_hotel_room_category_translation', function(Blueprint $table)
		{
			$table->foreign('category_id')->references('id')->on('mo_hotel_room_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_hotel_room_category_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_hotel_room_category_translation_category_id_foreign');
			$table->dropForeign('mo_hotel_room_category_translation_language_id_foreign');
		});
	}

}
