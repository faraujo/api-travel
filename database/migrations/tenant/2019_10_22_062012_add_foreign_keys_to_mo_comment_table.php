<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCommentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_comment', function(Blueprint $table)
		{
			$table->foreign('comment_status_id')->references('id')->on('mo_comment_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('updated_by')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_comment', function(Blueprint $table)
		{
			$table->dropForeign('mo_comment_comment_status_id_foreign');
			$table->dropForeign('mo_comment_language_id_foreign');
			$table->dropForeign('mo_comment_product_id_foreign');
			$table->dropForeign('mo_comment_subchannel_id_foreign');
			$table->dropForeign('mo_comment_updated_by_foreign');
			$table->dropForeign('mo_comment_user_id_foreign');
		});
	}

}
