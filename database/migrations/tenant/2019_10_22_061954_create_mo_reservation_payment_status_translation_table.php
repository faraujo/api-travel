<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationPaymentStatusTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_payment_status_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('payment_status_id')->unsigned()->nullable()->index('reser_payment_status_translation_payment_status_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_reservation_payment_status_translation_language_id_foreign');
			$table->string('name')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_payment_status_translation');
	}

}
