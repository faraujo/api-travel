<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCommentStatusTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_comment_status_translation', function(Blueprint $table)
		{
			$table->foreign('comment_status_id')->references('id')->on('mo_comment_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_comment_status_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_comment_status_translation_comment_status_id_foreign');
			$table->dropForeign('mo_comment_status_translation_language_id_foreign');
		});
	}

}
