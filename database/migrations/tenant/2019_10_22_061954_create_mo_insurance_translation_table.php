<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoInsuranceTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_insurance_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('insurance_id')->unsigned()->nullable()->index('mo_insurance_translation_insurance_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_insurance_translation_language_id_foreign');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_insurance_translation');
	}

}
