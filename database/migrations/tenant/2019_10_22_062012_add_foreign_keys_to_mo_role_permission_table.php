<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoRolePermissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_role_permission', function(Blueprint $table)
		{
			$table->foreign('permission_id')->references('id')->on('mo_permission')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('role_id')->references('id')->on('mo_role')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_role_permission', function(Blueprint $table)
		{
			$table->dropForeign('mo_role_permission_permission_id_foreign');
			$table->dropForeign('mo_role_permission_role_id_foreign');
			$table->dropForeign('mo_role_permission_subchannel_id_foreign');
		});
	}

}
