<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMoWebCustomerSupport20200130131500 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('mo_web_customer_support', function (Blueprint $table) {
            $table->dropForeign('mo_web_customer_support_language_id_foreign');
            $table->dropColumn('language_id');
            $table->dropColumn('name');
            $table->dropColumn('contact');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mo_web_customer_support', function (Blueprint $table) {
            $table->integer('language_id')->unsigned()->nullable();
            $table->foreign('language_id')
                ->references('id')
                ->on('mo_language');
            $table->string('name')->nullable();
            $table->string('contact')->nullable();
        });
    }
}
