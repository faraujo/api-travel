<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoModuleSubchannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_module_subchannel', function(Blueprint $table)
		{
			$table->foreign('module_id')->references('id')->on('mo_module')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_module_subchannel', function(Blueprint $table)
		{
			$table->dropForeign('mo_module_subchannel_module_id_foreign');
			$table->dropForeign('mo_module_subchannel_subchannel_id_foreign');
		});
	}

}
