<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationOperationStatusTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_operation_status_translation', function(Blueprint $table)
		{
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('operation_status_id', 'reserv_operation_status_translation_operation_status_id')->references('id')->on('mo_reservation_operation_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_operation_status_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_operation_status_translation_language_id_foreign');
			$table->dropForeign('reserv_operation_status_translation_operation_status_id');
		});
	}

}
