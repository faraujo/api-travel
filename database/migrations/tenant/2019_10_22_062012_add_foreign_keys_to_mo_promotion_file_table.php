<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_file', function(Blueprint $table)
		{
			$table->foreign('file_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_file', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_file_file_id_foreign');
			$table->dropForeign('mo_promotion_file_promotion_id_foreign');
		});
	}

}
