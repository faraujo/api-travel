<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoCommentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_comment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_comment_product_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_comment_language_id_foreign');
			$table->integer('user_id')->unsigned()->nullable()->index('mo_comment_user_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_comment_subchannel_id_foreign');
			$table->text('text', 65535)->nullable();
			$table->decimal('rating', 4)->nullable();
			$table->integer('updated_by')->unsigned()->nullable()->index('mo_comment_updated_by_foreign');
			$table->integer('comment_status_id')->unsigned()->nullable()->index('mo_comment_comment_status_id_foreign');
			$table->dateTime('updated_when')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_comment');
	}

}
