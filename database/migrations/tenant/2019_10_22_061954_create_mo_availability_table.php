<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoAvailabilityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_availability', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('date')->nullable();
			$table->time('session')->nullable();
			$table->integer('product_id')->unsigned()->nullable()->index('mo_availability_product_id_foreign');
			$table->integer('service_id')->unsigned()->nullable()->index('mo_availability_service_id_foreign');
			$table->integer('location_id')->unsigned()->nullable()->index('mo_availability_location_id_foreign');
			$table->integer('quota')->unsigned()->nullable();
			$table->integer('avail')->nullable();
			$table->integer('overbooking')->nullable();
			$table->enum('overbooking_unit', array('%','u'))->nullable();
			$table->boolean('closed')->default(0);
			$table->boolean('unlimited')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_availability');
	}

}
