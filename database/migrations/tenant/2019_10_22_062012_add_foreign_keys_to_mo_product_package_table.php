<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoProductPackageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_product_package', function(Blueprint $table)
		{
			$table->foreign('location_id')->references('id')->on('mo_location')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('package_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_product_package', function(Blueprint $table)
		{
			$table->dropForeign('mo_product_package_location_id_foreign');
			$table->dropForeign('mo_product_package_package_id_foreign');
			$table->dropForeign('mo_product_package_product_id_foreign');
		});
	}

}
