<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission20200203151000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_permission')->where('id', 225)->update([
            'name' => 'Visualizar registros de atención al cliente',
            'description' => 'Permiso para listar registros de atención al cliente del front de venta'
        ]);

        DB::table('mo_permission')->where('id', 226)->update([
            'name' => 'Visualizar un registro de atención al cliente',
            'description' => 'Permiso para listar un registro de atención al cliente del front de venta',
        ]);

        DB::table('mo_permission')->where('id', 227)->update([
            'name' => 'Actualizar registros de atención al cliente',
            'description' => 'Permiso para actualizar registros de atención al cliente del front de venta',
        ]);

        DB::table('mo_permission')->where('id', 228)->update([
            'name' => 'Crear registros de atención al cliente',
            'description' => 'Permiso para crear registros de atención al cliente del front de venta',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
