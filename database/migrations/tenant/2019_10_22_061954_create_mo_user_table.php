<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('system_user_id')->unsigned()->nullable()->index('mo_user_system_user_id_foreign');
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->integer('document_type_id')->unsigned()->nullable()->index('mo_user_document_type_id_foreign');
			$table->string('number_document')->nullable();
			$table->integer('sex_id')->unsigned()->nullable()->index('mo_user_sex_id_foreign');
			$table->date('birthdate')->nullable();
			$table->string('email')->nullable();
			$table->string('email2')->nullable();
			$table->string('password')->nullable();
			$table->integer('faults_login')->default(0);
			$table->boolean('bloqued_login')->default(0);
			$table->dateTime('bloqued_to')->nullable();
			$table->integer('language_id')->unsigned()->nullable()->index('mo_user_language_id_foreign');
			$table->string('telephone1')->nullable();
			$table->string('telephone2')->nullable();
			$table->string('telephone3')->nullable();
			$table->string('address')->nullable();
			$table->string('postal_code')->nullable();
			$table->string('city')->nullable();
			$table->integer('state_id')->unsigned()->nullable()->index('mo_user_state_id_foreign');
			$table->integer('country_id')->unsigned()->nullable()->index('mo_user_country_id_foreign');
			$table->string('business')->nullable();
			$table->string('telephone_business')->nullable();
			$table->boolean('worker')->default(0);
			$table->integer('worker_type_id')->unsigned()->nullable()->index('mo_user_worker_type_id_foreign');
			$table->text('observations')->nullable();
			$table->integer('image_id')->unsigned()->nullable()->index('mo_user_image_id_foreign');
			$table->string('facebook_id')->nullable();
			$table->string('google_id')->nullable();
			$table->boolean('social_register')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_user');
	}

}
