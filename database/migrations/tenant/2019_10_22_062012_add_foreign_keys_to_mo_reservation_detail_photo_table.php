<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationDetailPhotoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_detail_photo', function(Blueprint $table)
		{
			$table->foreign('reservation_detail_id', 'mo_reservation_photo_reservation_detail_id_foreign')->references('id')->on('mo_reservation_detail')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_detail_photo', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_photo_reservation_detail_id_foreign');
		});
	}

}
