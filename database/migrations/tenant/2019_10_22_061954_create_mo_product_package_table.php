<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoProductPackageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_product_package', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('package_id')->unsigned()->nullable()->index('mo_product_package_package_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_product_package_product_id_foreign');
			$table->integer('location_id')->unsigned()->nullable()->index('mo_product_package_location_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_product_package');
	}

}
