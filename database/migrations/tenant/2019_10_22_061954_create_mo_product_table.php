<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('type_id')->unsigned()->nullable()->index('mo_product_type_id_foreign');
			$table->string('code')->nullable();
			$table->integer('company_id')->unsigned()->nullable()->index('mo_product_company_id_foreign');
			$table->integer('uom_id')->unsigned()->nullable()->index('mo_product_uom_id_foreign');
			$table->integer('order')->unsigned()->nullable();
			$table->boolean('billable')->default(1);
			$table->decimal('height_from', 20, 5)->nullable();
			$table->decimal('height_to', 20, 5)->nullable();
			$table->decimal('weight_from', 20, 5)->nullable();
			$table->decimal('weight_to', 20, 5)->nullable();
			$table->decimal('longitude', 20, 8)->nullable();
			$table->decimal('latitude', 20, 8)->nullable();
			$table->text('address', 65535)->nullable();
			$table->boolean('pax')->default(0);
			$table->string('cost_center')->nullable();
			$table->string('accounting_account')->nullable();
			$table->boolean('external_connection')->default(0);
			$table->text('external_connection_url')->nullable();
			$table->string('external_code')->nullable();
			$table->string('external_service_user')->nullable();
			$table->string('external_service_password')->nullable();
			$table->boolean('salable')->default(1);
			$table->integer('iva_id')->unsigned()->nullable()->index('mo_product_iva_id_foreign');
			$table->boolean('home_app_hotel')->default(0);
			$table->boolean('app_hotel_service')->default(0);
			$table->boolean('app_hotel_without_reservation')->default(0);
			$table->boolean('app_hotel_afi_park')->default(0);
			$table->boolean('app_hotel_afi_tour')->default(0);
			$table->boolean('app_hotel_menu_service')->default(0);
			$table->boolean('app_hotel_with_service')->default(0);
			$table->boolean('app_hotel_with_service_direct')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_product');
	}

}
