<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_payment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->unsigned()->nullable()->index('mo_reservation_payment_reservation_id_foreign');
			$table->integer('reservation_detail_id')->unsigned()->nullable()->index('mo_reservation_payment_reservation_detail_id_foreign');
			$table->integer('payment_method_id')->unsigned()->nullable()->index('mo_reservation_payment_payment_method_id_foreign');
			$table->text('credit_card', 65535)->nullable();
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->string('bank')->nullable();
			$table->string('bank_name')->nullable();
			$table->string('months_no_interest')->nullable();
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_reservation_payment_subchannel_id_foreign');
			$table->integer('client_id')->unsigned()->nullable()->index('mo_reservation_payment_client_id_foreign');
			$table->integer('agent_id')->unsigned()->nullable()->index('mo_reservation_payment_agent_id_foreign');
			$table->string('client_session')->nullable();
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_reservation_payment_currency_id_foreign');
			$table->decimal('amount', 20, 5)->nullable();
			$table->integer('currency_exchange_id')->unsigned()->nullable()->index('mo_reservation_payment_currency_exchange_id_foreign');
			$table->dateTime('date_time')->nullable();
			$table->boolean('status_ok')->default(0);
			$table->string('code')->nullable();
			$table->text('response')->nullable();
			$table->string('response_code')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_payment');
	}

}
