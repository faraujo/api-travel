<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_detail', function(Blueprint $table)
		{
			$table->foreign('agent_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('contract_id')->references('id')->on('mo_contract_model')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_exchange_id')->references('id')->on('mo_currency_exchange')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('discount_user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('location_id')->references('id')->on('mo_location')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('operation_status_id')->references('id')->on('mo_reservation_operation_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('payment_status_id')->references('id')->on('mo_reservation_payment_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('price_id')->references('id')->on('mo_price')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_id')->references('id')->on('mo_reservation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('service_id')->references('id')->on('mo_service')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_detail', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_detail_agent_id_foreign');
			$table->dropForeign('mo_reservation_detail_contract_id_foreign');
			$table->dropForeign('mo_reservation_detail_currency_exchange_id_foreign');
			$table->dropForeign('mo_reservation_detail_currency_id_foreign');
			$table->dropForeign('mo_reservation_detail_discount_user_id_foreign');
			$table->dropForeign('mo_reservation_detail_location_id_foreign');
			$table->dropForeign('mo_reservation_detail_operation_status_id_foreign');
			$table->dropForeign('mo_reservation_detail_payment_status_id_foreign');
			$table->dropForeign('mo_reservation_detail_price_id_foreign');
			$table->dropForeign('mo_reservation_detail_product_id_foreign');
			$table->dropForeign('mo_reservation_detail_reservation_id_foreign');
			$table->dropForeign('mo_reservation_detail_service_id_foreign');
			$table->dropForeign('mo_reservation_detail_subchannel_id_foreign');
		});
	}

}
