<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermissionMoPermissionRole202001071555 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::Table('mo_permission')->insert(
            [
                    'id' => 215,
                    'name' => 'Visualizar un idioma',
                    'description' => 'Permiso para visualizar un idioma',
                    'module_id' => 13,
                    'order' => 2,
                    'admin' => 1,
                    'subchannel' => 0,
                    'deleted_at' => NULL,
                
            ]
        );

        DB::Table('mo_role_permission')->insert(
            [
                'permission_id' => 215,
                'role_id' => 1,
                'subchannel_id' => NULL,
                'deleted_at' => NULL,
                
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
