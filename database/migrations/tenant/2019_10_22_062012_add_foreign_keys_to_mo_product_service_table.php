<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoProductServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_product_service', function(Blueprint $table)
		{
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('service_id')->references('id')->on('mo_service')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_product_service', function(Blueprint $table)
		{
			$table->dropForeign('mo_product_service_product_id_foreign');
			$table->dropForeign('mo_product_service_service_id_foreign');
		});
	}

}
