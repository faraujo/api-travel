<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMoPaymentMethod20200218172600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('mo_payment_method', function (Blueprint $table) {
            $table->string('paypal_username')->nullable()->after('type');
            $table->string('paypal_password')->nullable()->after('paypal_username');
            $table->string('paypal_secret')->nullable()->after('paypal_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mo_payment_method', function (Blueprint $table) {
            $table->dropcolumn('paypal_username');
            $table->dropcolumn('paypal_password');
            $table->dropcolumn('paypal_secret');
        });
    }
}
