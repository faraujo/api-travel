<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoProductTypeTranslation20200224153600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_product_type_translation')->where('type_id', 6)->where('language_id', 1)
        ->update([
            'name' => 'Entradas'
        ]);
        DB::table('mo_product_type_translation')->where('type_id', 6)->where('language_id', 2)
        ->update([
            'name' => 'Entrances'
        ]);
        DB::table('mo_product_type_translation')->where('type_id', 6)->where('language_id', 3)
        ->update([
            'name' => 'Entradas'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
