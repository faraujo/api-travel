<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoAdditionalServiceTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_additional_service_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('service_id')->unsigned()->nullable()->index('mo_additional_service_translation_service_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_additional_service_translation_language_id_foreign');
			$table->string('name')->nullable();
			$table->string('description')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_additional_service_translation');
	}

}
