<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoSubchannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_subchannel', function(Blueprint $table)
		{
			$table->foreign('channel_id')->references('id')->on('mo_channel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('contract_model_id')->references('id')->on('mo_contract_model')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_subchannel', function(Blueprint $table)
		{
			$table->dropForeign('mo_subchannel_channel_id_foreign');
			$table->dropForeign('mo_subchannel_contract_model_id_foreign');
		});
	}

}
