<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationPaymentStatusTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_payment_status_translation', function(Blueprint $table)
		{
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('payment_status_id', 'reser_payment_status_translation_payment_status_id_foreign')->references('id')->on('mo_reservation_payment_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_payment_status_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_payment_status_translation_language_id_foreign');
			$table->dropForeign('reser_payment_status_translation_payment_status_id_foreign');
		});
	}

}
