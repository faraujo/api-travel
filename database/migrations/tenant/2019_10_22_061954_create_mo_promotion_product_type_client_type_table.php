<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPromotionProductTypeClientTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_promotion_product_type_client_type', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('promotion_product_type_id')->unsigned()->nullable()->index('prom_prod_type_client_type');
			$table->integer('client_type_id')->unsigned()->nullable()->index('mo_promotion_product_type_client_type_client_type_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_promotion_product_type_client_type');
	}

}
