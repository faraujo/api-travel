<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoProductTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_product_tag', function(Blueprint $table)
		{
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('tag_id')->references('id')->on('mo_tag')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_product_tag', function(Blueprint $table)
		{
			$table->dropForeign('mo_product_tag_product_id_foreign');
			$table->dropForeign('mo_product_tag_tag_id_foreign');
		});
	}

}
