<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoRolePermissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_role_permission', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('permission_id')->unsigned()->nullable()->index('mo_role_permission_permission_id_foreign');
			$table->integer('role_id')->unsigned()->nullable()->index('mo_role_permission_role_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_role_permission_subchannel_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_role_permission');
	}

}
