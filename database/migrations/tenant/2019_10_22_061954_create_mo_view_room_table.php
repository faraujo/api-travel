<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoViewRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_view_room', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('room_id')->unsigned()->nullable()->index('mo_view_room_room_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_view_room_subchannel_id_foreign');
			$table->boolean('published')->default(0);
			$table->integer('order')->unsigned()->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_view_room');
	}

}
