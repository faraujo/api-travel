<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationDetailHotelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_detail_hotel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_detail_id')->unsigned()->nullable()->index('mo_reservation_hotel_reservation_detail_id_foreign');
			$table->date('date_start')->nullable();
			$table->date('date_end')->nullable();
			$table->integer('shuttle_required')->unsigned()->nullable();
			$table->string('arrival_airline')->nullable();
			$table->string('arrival_session')->nullable();
			$table->string('arrival_flight_number')->nullable();
			$table->string('arrival_terminal')->nullable();
			$table->string('departure_airline')->nullable();
			$table->string('departure_session')->nullable();
			$table->string('departure_flight_number')->nullable();
			$table->string('departure_terminal')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_detail_hotel');
	}

}
