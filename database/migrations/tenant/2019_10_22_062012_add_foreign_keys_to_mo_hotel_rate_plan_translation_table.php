<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHotelRatePlanTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_hotel_rate_plan_translation', function(Blueprint $table)
		{
			$table->foreign('hotel_rate_plan_id')->references('id')->on('mo_hotel_rate_plan')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_hotel_rate_plan_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_hotel_rate_plan_translation_hotel_rate_plan_id_foreign');
			$table->dropForeign('mo_hotel_rate_plan_translation_language_id_foreign');
		});
	}

}
