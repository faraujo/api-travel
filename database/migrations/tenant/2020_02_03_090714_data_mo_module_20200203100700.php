<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoModule20200203100700 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_module')->insert([
            'id' => 29,
            'name' => 'Web de venta',
            'order' => 0
        ]);

        DB::table('mo_permission')->insert([
            'id' => 216,
            'name' => 'Visualizar legales',
            'description' => 'Permiso para listar los legales del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 217,
            'name' => 'Visualizar legal',
            'description' => 'Permiso para listar un legal del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 218,
            'name' => 'Actualizar legales',
            'description' => 'Permiso para actualizar legales del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 219,
            'name' => 'Visualizar literales',
            'description' => 'Permiso para listar los literales del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 220,
            'name' => 'Visualizar literal',
            'description' => 'Permiso para listar un literal del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 221,
            'name' => 'Actualizar literales',
            'description' => 'Permiso para actualizar literales del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 222,
            'name' => 'Visualizar redes sociales',
            'description' => 'Permiso para listar las redes sociales del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 223,
            'name' => 'Visualizar red social',
            'description' => 'Permiso para listar una red social del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 224,
            'name' => 'Actualizar redes sociales',
            'description' => 'Permiso para actualizar redes sociales del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
