<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoCategoryTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_category_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->nullable()->index('mo_category_translation_category_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_category_translation_language_id_foreign');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->text('short_description')->nullable();
			$table->string('friendly_url')->nullable();
			$table->string('title_seo')->nullable();
			$table->text('description_seo')->nullable();
			$table->text('keywords_seo')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_category_translation');
	}

}
