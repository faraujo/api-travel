<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionCouponTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_coupon_translation', function(Blueprint $table)
		{
			$table->foreign('coupon_id')->references('id')->on('mo_promotion_coupon')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('file_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_coupon_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_coupon_translation_coupon_id_foreign');
			$table->dropForeign('mo_promotion_coupon_translation_file_id_foreign');
			$table->dropForeign('mo_promotion_coupon_translation_language_id_foreign');
		});
	}

}
