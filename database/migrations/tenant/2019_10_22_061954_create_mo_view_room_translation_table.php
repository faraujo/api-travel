<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoViewRoomTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_view_room_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_view_room_translation_language_id_foreign');
			$table->integer('view_room_id')->unsigned()->nullable()->index('mo_view_room_translation_view_room_id_foreign');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->text('services_included')->nullable();
			$table->text('services_not_included')->nullable();
			$table->text('short_description')->nullable();
			$table->string('location')->nullable();
			$table->string('views')->nullable();
			$table->string('size')->nullable();
			$table->string('capacity')->nullable();
			$table->string('url_360')->nullable();
			$table->string('friendly_url')->nullable();
			$table->string('title_seo')->nullable();
			$table->text('description_seo')->nullable();
			$table->text('keywords_seo')->nullable();
			$table->string('breadcrumb')->nullable();
			$table->string('rel')->default('follow');
			$table->string('og_title')->nullable();
			$table->text('og_description')->nullable();
			$table->integer('og_image')->unsigned()->nullable()->index('mo_view_room_translation_og_image_foreign');
			$table->string('twitter_title')->nullable();
			$table->text('twitter_description')->nullable();
			$table->integer('twitter_image')->unsigned()->nullable()->index('mo_view_room_translation_twitter_image_foreign');
			$table->text('script_head')->nullable();
			$table->text('script_body')->nullable();
			$table->text('script_footer')->nullable();
			$table->boolean('index')->default(1);
			$table->string('canonical_url')->nullable();
			$table->text('legal')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_view_room_translation');
	}

}
