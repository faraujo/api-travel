<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCountryTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_country_translation', function(Blueprint $table)
		{
			$table->foreign('country_id')->references('id')->on('mo_country')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_country_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_country_translation_country_id_foreign');
			$table->dropForeign('mo_country_translation_language_id_foreign');
		});
	}

}
