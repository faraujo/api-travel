<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoRolePermission20200203113300 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 216
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 217
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 218
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 219
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 220
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 221
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 222
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 223
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 224
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
