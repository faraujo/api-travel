<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoProductTypeTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_product_type_translation', function(Blueprint $table)
		{
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('type_id')->references('id')->on('mo_product_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_product_type_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_product_type_translation_language_id_foreign');
			$table->dropForeign('mo_product_type_translation_type_id_foreign');
		});
	}

}
