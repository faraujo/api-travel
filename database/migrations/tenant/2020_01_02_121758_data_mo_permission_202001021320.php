<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission202001021320 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::Table('mo_permission')->insert(
            [
                    'id' => 208,
                    'name' => 'Visualizar una localización',
                    'description' => 'Permiso para listar una localización',
                    'module_id' => 20,
                    'order' => 2,
                    'admin' => 1,
                    'subchannel' => 0,
                    'deleted_at' => NULL,
                
            ]
        );

        DB::Table('mo_role_permission')->insert(
            [
                'permission_id' => 208,
                'role_id' => 1,
                'subchannel_id' => NULL,
                'deleted_at' => NULL,
                
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
