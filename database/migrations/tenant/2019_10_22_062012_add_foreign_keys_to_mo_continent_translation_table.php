<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoContinentTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_continent_translation', function(Blueprint $table)
		{
			$table->foreign('continent_id')->references('id')->on('mo_continent')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_continent_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_continent_translation_continent_id_foreign');
			$table->dropForeign('mo_continent_translation_language_id_foreign');
		});
	}

}
