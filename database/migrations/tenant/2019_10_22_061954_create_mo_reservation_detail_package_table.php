<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationDetailPackageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_detail_package', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_detail_id')->unsigned()->nullable()->index('mo_reservation_detail_package_reservation_detail_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_reservation_detail_package_product_id_foreign');
			$table->string('product_name')->nullable();
			$table->integer('location_id')->unsigned()->nullable()->index('mo_reservation_detail_package_location_id_foreign');
			$table->string('location_name')->nullable();
			$table->date('date')->nullable();
			$table->date('date_visit_limit')->nullable();
			$table->time('session')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_detail_package');
	}

}
