<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoSearchLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_search_log', function(Blueprint $table)
		{
			$table->foreign('category_id')->references('id')->on('mo_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('depends_on')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('hotel_room_category_id')->references('id')->on('mo_hotel_room_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('hotel_room_id')->references('id')->on('mo_hotel_room')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_type_id')->references('id')->on('mo_product_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('tag_id')->references('id')->on('mo_tag')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_search_log', function(Blueprint $table)
		{
			$table->dropForeign('mo_search_log_category_id_foreign');
			$table->dropForeign('mo_search_log_currency_id_foreign');
			$table->dropForeign('mo_search_log_depends_on_foreign');
			$table->dropForeign('mo_search_log_hotel_room_category_id_foreign');
			$table->dropForeign('mo_search_log_hotel_room_id_foreign');
			$table->dropForeign('mo_search_log_language_id_foreign');
			$table->dropForeign('mo_search_log_product_id_foreign');
			$table->dropForeign('mo_search_log_product_type_id_foreign');
			$table->dropForeign('mo_search_log_subchannel_id_foreign');
			$table->dropForeign('mo_search_log_tag_id_foreign');
			$table->dropForeign('mo_search_log_user_id_foreign');
		});
	}

}
