<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_service', function(Blueprint $table)
		{
			$table->foreign('iva_id')->references('id')->on('mo_iva')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_service', function(Blueprint $table)
		{
			$table->dropForeign('mo_service_iva_id_foreign');
		});
	}

}
