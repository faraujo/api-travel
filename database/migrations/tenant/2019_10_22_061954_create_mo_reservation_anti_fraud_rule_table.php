<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationAntiFraudRuleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_anti_fraud_rule', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('level_id')->unsigned()->nullable()->index('mo_reservation_anti_fraud_rule_level_id_foreign');
			$table->integer('anticipation_days')->nullable();
			$table->boolean('diferent_buyer')->nullable();
			$table->string('payment_method_type')->nullable();
			$table->integer('amount')->nullable();
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_reservation_anti_fraud_rule_currency_id_foreign');
			$table->integer('product_type_id')->unsigned()->nullable()->index('mo_reservation_anti_fraud_rule_product_type_id_foreign');
			$table->boolean('same_card')->nullable();
			$table->boolean('same_user')->nullable();
			$table->integer('user_week')->nullable();
			$table->integer('card_day')->nullable();
			$table->integer('card_week')->nullable();
			$table->time('limit_hour')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_anti_fraud_rule');
	}

}
