<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoCurrencyExchangeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_currency_exchange', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_currency_exchange_currency_id_foreign');
			$table->integer('user_id')->unsigned()->nullable()->index('mo_currency_exchange_user_id_foreign');
			$table->dateTime('date_time_start')->nullable();
			$table->decimal('exchange', 20, 5)->default(0.00000);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_currency_exchange');
	}

}
