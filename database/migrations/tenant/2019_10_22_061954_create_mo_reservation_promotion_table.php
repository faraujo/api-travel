<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationPromotionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_promotion', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->unsigned()->nullable()->index('mo_reservation_promotion_reservation_id_foreign');
			$table->integer('reservation_detail_id')->unsigned()->nullable()->index('mo_reservation_promotion_reservation_detail_id_foreign');
			$table->integer('reservation_detail_client_type_id')->unsigned()->nullable()->index('reservation_detail_client_type');
			$table->integer('promotion_id')->unsigned()->nullable()->index('mo_reservation_promotion_promotion_id_foreign');
			$table->integer('promotion_order')->nullable();
			$table->integer('promotion_type_id')->unsigned()->nullable()->index('mo_reservation_promotion_promotion_type_id_foreign');
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_reservation_promotion_currency_id_foreign');
			$table->string('promotion_type_name')->nullable();
			$table->string('code')->nullable();
			$table->string('coupon_code')->nullable();
			$table->decimal('promotion_amount', 20, 5)->nullable();
			$table->decimal('promotion_amount_total', 20, 5)->nullable();
			$table->string('promotion_name')->nullable();
			$table->text('promotion_short_description', 65535)->nullable();
			$table->integer('benefit_card_id')->unsigned()->nullable()->index('mo_reservation_promotion_benefit_card_id_foreign');
			$table->string('benefit_card_name')->nullable();
			$table->string('benefit_card_number')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_promotion');
	}

}
