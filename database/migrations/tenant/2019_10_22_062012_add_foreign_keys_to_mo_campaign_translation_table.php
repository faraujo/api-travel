<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCampaignTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_campaign_translation', function(Blueprint $table)
		{
			$table->foreign('campaign_id')->references('id')->on('mo_campaign')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_campaign_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_campaign_translation_campaign_id_foreign');
			$table->dropForeign('mo_campaign_translation_language_id_foreign');
		});
	}

}
