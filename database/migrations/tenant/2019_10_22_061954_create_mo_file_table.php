<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_file', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('type_id')->unsigned()->nullable()->index('mo_file_type_id_foreign');
			$table->string('file_name')->nullable();
			$table->string('file_size')->nullable();
			$table->string('file_dimensions')->nullable();
			$table->string('mimetype')->nullable();
			$table->integer('order')->unsigned()->nullable();
			$table->text('url_file')->nullable();
			$table->integer('device_id')->unsigned()->nullable()->index('mo_file_device_id_foreign');
			$table->string('key_file')->nullable();
			$table->boolean('private')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_file');
	}

}
