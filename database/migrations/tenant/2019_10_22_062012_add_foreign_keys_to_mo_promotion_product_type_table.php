<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionProductTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_product_type', function(Blueprint $table)
		{
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_type_id')->references('id')->on('mo_product_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_type_id')->references('id')->on('mo_promotion_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_product_type', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_product_type_currency_id_foreign');
			$table->dropForeign('mo_promotion_product_type_product_type_id_foreign');
			$table->dropForeign('mo_promotion_product_type_promotion_id_foreign');
			$table->dropForeign('mo_promotion_product_type_promotion_type_id_foreign');
		});
	}

}
