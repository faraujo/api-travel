<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHotelRatePlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_hotel_rate_plan', function(Blueprint $table)
		{
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_hotel_rate_plan', function(Blueprint $table)
		{
			$table->dropForeign('mo_hotel_rate_plan_product_id_foreign');
			$table->dropForeign('mo_hotel_rate_plan_subchannel_id_foreign');
		});
	}

}
