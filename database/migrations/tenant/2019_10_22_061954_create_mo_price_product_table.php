<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPriceProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_price_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->time('session')->nullable();
			$table->integer('price_id')->unsigned()->nullable()->index('mo_price_product_price_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_price_product_product_id_foreign');
			$table->integer('service_id')->unsigned()->nullable()->index('mo_price_product_service_id_foreign');
			$table->integer('location_id')->unsigned()->nullable()->index('mo_price_product_location_id_foreign');
			$table->integer('pickup_id')->unsigned()->nullable()->index('mo_price_product_pickup_id_foreign');
			$table->integer('client_type_id')->unsigned()->nullable()->index('mo_price_product_client_type_id_foreign');
			$table->decimal('tip_amount', 20, 5)->nullable();
			$table->decimal('tip_percent', 20, 5)->nullable();
			$table->decimal('tip_min_amount', 20, 5)->nullable();
			$table->decimal('tip_max_amount', 20, 5)->nullable();
			$table->decimal('commission_amount', 20, 5)->nullable();
			$table->decimal('commission_percent', 20, 5)->nullable();
			$table->decimal('net_price', 20, 5)->nullable();
			$table->decimal('sale_price', 20, 5)->nullable();
			$table->decimal('markup', 20, 5)->unsigned()->nullable();
			$table->decimal('price_before', 20, 5)->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_price_product');
	}

}
