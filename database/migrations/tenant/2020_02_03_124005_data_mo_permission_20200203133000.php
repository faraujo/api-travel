<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission20200203133000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_permission')->insert([
            'id' => 225,
            'name' => 'Visualizar contactas',
            'description' => 'Permiso para listar los contacta del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 226,
            'name' => 'Visualizar contacta',
            'description' => 'Permiso para listar un contacta del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 227,
            'name' => 'Actualizar contacta',
            'description' => 'Permiso para actualizar contacta del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 228,
            'name' => 'Crear contacta',
            'description' => 'Permiso para crear contacta del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 225
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 226
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 227
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 228
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
