<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationDetailTransportationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_detail_transportation', function(Blueprint $table)
		{
			$table->foreign('pickup_id')->references('id')->on('mo_transportation_location')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_detail_id', 'mo_reservation_transportation_reservation_detail_id_foreign')->references('id')->on('mo_reservation_detail')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_detail_transportation', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_detail_transportation_pickup_id_foreign');
			$table->dropForeign('mo_reservation_transportation_reservation_detail_id_foreign');
		});
	}

}
