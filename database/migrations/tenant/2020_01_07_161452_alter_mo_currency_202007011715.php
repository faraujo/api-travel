<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMoCurrency202007011715 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mo_currency', function (Blueprint $table) {
            $table->boolean('front')->nullable()->after('thousand_separator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mo_currency', function (Blueprint $table) {
            $table->dropcolumn('front');
        });
    }
}
