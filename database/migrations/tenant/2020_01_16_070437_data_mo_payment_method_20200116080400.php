<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPaymentMethod20200116080400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_payment_method')->where('mo_payment_method.id','!=', 10)
            ->update([
                'deleted_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]);

        DB::table('mo_payment_method_translation')
            ->where('mo_payment_method_translation.payment_method_id', '!=', 10)
            ->update([
                'deleted_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]);

        DB::table('mo_payment_method_translation')
                ->where('mo_payment_method_translation.payment_method_id', 10)
                ->where('mo_payment_method_translation.language_id', 1)
                ->update(['name' => 'Otros']);

        DB::table('mo_payment_method_translation')
            ->where('mo_payment_method_translation.payment_method_id', 10)
            ->where('mo_payment_method_translation.language_id', 2)
            ->update(['name' => 'Others']);

        DB::table('mo_payment_method_translation')
            ->where('mo_payment_method_translation.payment_method_id', 10)
            ->where('mo_payment_method_translation.language_id', 3)
            ->update(['name' => 'Outros']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
