<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_currency', function(Blueprint $table)
		{
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_currency', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_currency_currency_id_foreign');
			$table->dropForeign('mo_promotion_currency_promotion_id_foreign');
		});
	}

}
