<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationInsuranceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_insurance', function(Blueprint $table)
		{
			$table->foreign('agent_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_exchange_id')->references('id')->on('mo_currency_exchange')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('discount_user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('insurance_id')->references('id')->on('mo_insurance')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('operation_status_id')->references('id')->on('mo_reservation_operation_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('payment_status_id')->references('id')->on('mo_reservation_payment_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_id')->references('id')->on('mo_reservation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_insurance', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_insurance_agent_id_foreign');
			$table->dropForeign('mo_reservation_insurance_currency_exchange_id_foreign');
			$table->dropForeign('mo_reservation_insurance_currency_id_foreign');
			$table->dropForeign('mo_reservation_insurance_discount_user_id_foreign');
			$table->dropForeign('mo_reservation_insurance_insurance_id_foreign');
			$table->dropForeign('mo_reservation_insurance_operation_status_id_foreign');
			$table->dropForeign('mo_reservation_insurance_payment_status_id_foreign');
			$table->dropForeign('mo_reservation_insurance_reservation_id_foreign');
			$table->dropForeign('mo_reservation_insurance_subchannel_id_foreign');
		});
	}

}
