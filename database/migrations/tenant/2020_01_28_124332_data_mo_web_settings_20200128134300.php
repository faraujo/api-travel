<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoWebSettings20200128134300 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::table('mo_settings')
            ->insert([
                'name' => 'contact_email',
                'value' => 'info@viavoxexperience.com'
            ]);

        DB::table('mo_settings')
            ->insert([
                'name' => 'contact_telephone',
                'value' => '+34 942 888 375'
            ]);

        DB::table('mo_settings')
            ->insert([
                'name' => 'contact_direction',
                'value' => 'HQ Santander. Parque tecnológico de Santander, Edificio Salia, C/ Albert Einstein, 18, 39011 Santander (Cantabria)'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
