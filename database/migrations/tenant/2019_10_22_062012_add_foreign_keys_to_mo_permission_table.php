<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPermissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_permission', function(Blueprint $table)
		{
			$table->foreign('module_id')->references('id')->on('mo_module')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_permission', function(Blueprint $table)
		{
			$table->dropForeign('mo_permission_module_id_foreign');
		});
	}

}
