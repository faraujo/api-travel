<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationConfirmedLegalTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_confirmed_legal_translation', function(Blueprint $table)
		{
			$table->foreign('reservation_confirmed_legal_id', 'mo_r_c_legal_translation_r_c_legal_id_foreign')->references('id')->on('mo_reservation_confirmed_legal')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_confirmed_legal_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_r_c_legal_translation_r_c_legal_id_foreign');
			$table->dropForeign('mo_reservation_confirmed_legal_translation_language_id_foreign');
		});
	}

}
