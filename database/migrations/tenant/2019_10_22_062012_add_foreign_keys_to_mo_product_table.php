<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_product', function(Blueprint $table)
		{
			$table->foreign('company_id')->references('id')->on('mo_company')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('iva_id')->references('id')->on('mo_iva')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('type_id')->references('id')->on('mo_product_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('uom_id')->references('id')->on('mo_uom')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_product', function(Blueprint $table)
		{
			$table->dropForeign('mo_product_company_id_foreign');
			$table->dropForeign('mo_product_iva_id_foreign');
			$table->dropForeign('mo_product_type_id_foreign');
			$table->dropForeign('mo_product_uom_id_foreign');
		});
	}

}
