<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHouseTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_house_translation', function(Blueprint $table)
		{
			$table->foreign('house_id')->references('id')->on('mo_house')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_house_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_house_translation_house_id_foreign');
			$table->dropForeign('mo_house_translation_language_id_foreign');
		});
	}

}
