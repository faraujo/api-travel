<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoContactTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_contact', function(Blueprint $table)
		{
			$table->foreign('country_id')->references('id')->on('mo_country')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('document_type_id')->references('id')->on('mo_document_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('image_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('sex_id')->references('id')->on('mo_sex')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('state_id')->references('id')->on('mo_state')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('worker_type_id')->references('id')->on('mo_worker_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_contact', function(Blueprint $table)
		{
			$table->dropForeign('mo_contact_country_id_foreign');
			$table->dropForeign('mo_contact_document_type_id_foreign');
			$table->dropForeign('mo_contact_image_id_foreign');
			$table->dropForeign('mo_contact_language_id_foreign');
			$table->dropForeign('mo_contact_sex_id_foreign');
			$table->dropForeign('mo_contact_state_id_foreign');
			$table->dropForeign('mo_contact_worker_type_id_foreign');
		});
	}

}
