<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationInsuranceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_insurance', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->unsigned()->nullable()->index('mo_reservation_insurance_reservation_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_reservation_insurance_subchannel_id_foreign');
			$table->integer('insurance_id')->unsigned()->nullable()->index('mo_reservation_insurance_insurance_id_foreign');
			$table->string('insurance_key')->nullable();
			$table->integer('quantity')->unsigned()->nullable();
			$table->decimal('unity_price', 20, 5)->nullable();
			$table->decimal('original_unity_price', 20, 5)->nullable();
			$table->decimal('total_price', 20, 5)->nullable();
			$table->decimal('original_total_price', 20, 5)->nullable();
			$table->date('purchase_date')->nullable();
			$table->date('visit_date')->nullable();
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_reservation_insurance_currency_id_foreign');
			$table->integer('currency_exchange_id')->unsigned()->nullable()->index('mo_reservation_insurance_currency_exchange_id_foreign');
			$table->decimal('discount', 20, 5)->nullable();
			$table->integer('discount_user_id')->unsigned()->nullable()->index('mo_reservation_insurance_discount_user_id_foreign');
			$table->decimal('iva', 20, 5)->nullable();
			$table->dateTime('date_time_start')->nullable();
			$table->dateTime('date_time_quotation')->nullable();
			$table->dateTime('date_time_processed')->nullable();
			$table->dateTime('date_time_operation')->nullable();
			$table->dateTime('date_time_cancellation')->nullable();
			$table->dateTime('date_time_expiration')->nullable();
			$table->integer('operation_status_id')->unsigned()->nullable()->index('mo_reservation_insurance_operation_status_id_foreign');
			$table->integer('payment_status_id')->unsigned()->nullable()->index('mo_reservation_insurance_payment_status_id_foreign');
			$table->integer('agent_id')->unsigned()->nullable()->index('mo_reservation_insurance_agent_id_foreign');
			$table->string('response_code')->nullable();
			$table->text('response')->nullable();
			$table->dateTime('datetime_update')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_insurance');
	}

}
