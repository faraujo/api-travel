<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionDeviceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_device', function(Blueprint $table)
		{
			$table->foreign('device_id')->references('id')->on('mo_device')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_device', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_device_device_id_foreign');
			$table->dropForeign('mo_promotion_device_promotion_id_foreign');
		});
	}

}
