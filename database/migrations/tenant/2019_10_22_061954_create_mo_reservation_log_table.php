<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('date')->nullable();
			$table->integer('client_id')->unsigned()->nullable()->index('mo_reservation_log_client_id_foreign');
			$table->integer('agent_id')->unsigned()->nullable()->index('mo_reservation_log_agent_id_foreign');
			$table->integer('action_id')->unsigned()->nullable()->index('mo_reservation_log_action_id_foreign');
			$table->integer('reservation_id')->unsigned()->nullable()->index('mo_reservation_log_reservation_id_foreign');
			$table->integer('reservation_detail_id')->unsigned()->nullable()->index('mo_reservation_log_reservation_detail_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_reservation_log_subchannel_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_log');
	}

}
