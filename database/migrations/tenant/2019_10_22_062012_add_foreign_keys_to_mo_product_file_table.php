<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoProductFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_product_file', function(Blueprint $table)
		{
			$table->foreign('file_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_product_file', function(Blueprint $table)
		{
			$table->dropForeign('mo_product_file_file_id_foreign');
			$table->dropForeign('mo_product_file_product_id_foreign');
		});
	}

}
