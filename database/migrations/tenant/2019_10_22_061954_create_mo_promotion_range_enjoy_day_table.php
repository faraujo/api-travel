<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPromotionRangeEnjoyDayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_promotion_range_enjoy_day', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('promotion_id')->unsigned()->nullable()->index('mo_promotion_range_enjoy_day_promotion_id_foreign');
			$table->dateTime('enjoy_date_start');
			$table->dateTime('enjoy_date_end');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_promotion_range_enjoy_day');
	}

}
