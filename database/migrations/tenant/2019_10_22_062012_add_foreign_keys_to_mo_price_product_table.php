<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPriceProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_price_product', function(Blueprint $table)
		{
			$table->foreign('client_type_id')->references('id')->on('mo_client_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('location_id')->references('id')->on('mo_location')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('pickup_id')->references('id')->on('mo_transportation_location')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('price_id')->references('id')->on('mo_price')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('service_id')->references('id')->on('mo_service')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_price_product', function(Blueprint $table)
		{
			$table->dropForeign('mo_price_product_client_type_id_foreign');
			$table->dropForeign('mo_price_product_location_id_foreign');
			$table->dropForeign('mo_price_product_pickup_id_foreign');
			$table->dropForeign('mo_price_product_price_id_foreign');
			$table->dropForeign('mo_price_product_product_id_foreign');
			$table->dropForeign('mo_price_product_service_id_foreign');
		});
	}

}
