<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermissionMoPermissionRole202002011550 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::Table('mo_permission')->insert(
            [
                    'id' => 211,
                    'name' => 'Actualizar localizaciones',
                    'description' => 'Permiso para actualizar localizaciones',
                    'module_id' => 20,
                    'order' => 3,
                    'admin' => 1,
                    'subchannel' => 0,
                    'deleted_at' => NULL,
                
            ]
        );

        DB::Table('mo_role_permission')->insert(
            [
                'permission_id' => 211,
                'role_id' => 1,
                'subchannel_id' => NULL,
                'deleted_at' => NULL,
                
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
