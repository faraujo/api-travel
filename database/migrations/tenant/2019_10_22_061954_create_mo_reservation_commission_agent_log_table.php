<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationCommissionAgentLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_commission_agent_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_id')->unsigned()->nullable()->index('mo_reservation_commission_agent_log_reservation_id_foreign');
			$table->integer('supervisor_reassignment_id')->unsigned()->nullable()->index('mo_reservation_commission_agent_log_super_reas_foreign');
			$table->integer('old_agent_id')->unsigned()->nullable()->index('mo_reservation_commission_agent_log_old_agent_id_foreign');
			$table->integer('new_agent_id')->unsigned()->nullable()->index('mo_reservation_commission_agent_log_new_agent_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_commission_agent_log');
	}

}
