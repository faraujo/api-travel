<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPromotionSaleDayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_promotion_sale_day', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('promotion_range_sale_day_id')->unsigned()->nullable()->index('mo_promotion_sale_day_promotion_range_sale_day_id_foreign');
			$table->integer('day_id')->unsigned()->nullable()->index('mo_promotion_sale_day_day_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_promotion_sale_day');
	}

}
