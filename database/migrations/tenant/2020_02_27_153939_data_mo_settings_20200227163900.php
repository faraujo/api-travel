<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoSettings20200227163900 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::table('mo_settings')->insert([
            'name'  => 'protocol',
            'value' => 'https://'
        ]);

        DB::table('mo_settings')->insert([
            'name'  => 'base_path',
            'value' => 'viavoxexperience.com'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
