<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission20200203152600 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_permission')->insert([
            'id' => 229,
            'name' => 'Eliminar registros de atención al cliente',
            'description' => 'Permiso para eliminar registros de atención al cliente del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 229
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
