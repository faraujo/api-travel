<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoIdentificationTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_identification_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('identification_id')->unsigned()->nullable()->index('mo_identification_translation_identification_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_identification_translation_language_id_foreign');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_identification_translation');
	}

}
