<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoCommentStatusTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_comment_status_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('comment_status_id')->unsigned()->nullable()->index('mo_comment_status_translation_comment_status_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_comment_status_translation_language_id_foreign');
			$table->string('name')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_comment_status_translation');
	}

}
