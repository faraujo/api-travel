<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPage20200204162000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_web_page')->where('id', 1)->update([
            'type' => 'about us'
        ]);

        DB::table('mo_web_legal')->where('id', 1)->update([
            'type' => 'cookie policy'
        ]);

        DB::table('mo_web_legal')->where('id', 2)->update([
            'type' => 'privacy conditions'
        ]);

        DB::table('mo_web_legal')->where('id', 3)->update([
            'type' => 'confidentiality policy'
        ]);

        DB::table('mo_web_legal')->where('id', 4)->update([
            'type' => 'legal warning'
        ]);

        DB::table('mo_web_literal')->where('id', 1)->update([
            'name' => 'home text section 1'
        ]);

        DB::table('mo_web_literal')->where('id', 2)->update([
            'name' => 'home text section 2'
        ]);

        DB::table('mo_web_slider')->where('id', 1)->update([
            'name' => 'home top'
        ]);

        DB::table('mo_web_slider')->where('id', 2)->update([
            'name' => 'home bottom'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
