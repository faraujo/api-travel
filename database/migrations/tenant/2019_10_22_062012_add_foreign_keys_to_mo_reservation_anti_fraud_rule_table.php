<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationAntiFraudRuleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_anti_fraud_rule', function(Blueprint $table)
		{
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('level_id')->references('id')->on('mo_reservation_anti_fraud_level')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_type_id')->references('id')->on('mo_product_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_anti_fraud_rule', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_anti_fraud_rule_currency_id_foreign');
			$table->dropForeign('mo_reservation_anti_fraud_rule_level_id_foreign');
			$table->dropForeign('mo_reservation_anti_fraud_rule_product_type_id_foreign');
		});
	}

}
