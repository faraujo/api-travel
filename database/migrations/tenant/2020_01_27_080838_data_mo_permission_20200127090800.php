<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission20200127090800 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_permission')->where('id', 178)->update([
            'name' => 'Listar pickups de transporte',
            'description' => 'Permiso para listar pickups de transporte'
        ]);

        DB::table('mo_permission')->where('id', 179)->update([
            'name' => 'Crear pickups de transporte',
            'description' => 'Permiso para crear pickups de transporte'
        ]);

        DB::table('mo_permission')->where('id', 180)->update([
            'name' => 'Visualizar pickups de transporte',
            'description' => 'Permiso para listar un pickup de transporte'
        ]);

        DB::table('mo_permission')->where('id', 181)->update([
            'name' => 'Borrar pickups de transporte',
            'description' => 'Permiso para borrar pickups de transporte'
        ]);

        DB::table('mo_permission')->where('id', 182)->update([
            'name' => 'Actualizar pickups de transporte',
            'description' => 'Permiso para actualizar pickups de transporte'
        ]);

        DB::table('mo_permission')->where('id', 183)->update([
            'name' => 'Listar zonas de transporte',
            'description' => 'Permiso para listar zonas de transporte'
        ]);

        DB::table('mo_permission')->where('id', 184)->update([
            'name' => 'Crear zonas de transporte',
            'description' => 'Permiso para crear zonas de transporte'
        ]);

        DB::table('mo_permission')->where('id', 185)->update([
            'name' => 'Borrar zonas de transporte',
            'description' => 'Permiso para borrar zonas de transporte'
        ]);

        DB::table('mo_permission')->where('id', 186)->update([
            'name' => 'Actualizar zonas de transporte',
            'description' => 'Permiso para actualizar zonas de transporte'
        ]);

        DB::table('mo_permission')->where('id', 187)->update([
            'name' => 'Visualizar una zona de transporte',
            'description' => 'Permiso para listar una zona de transporte'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
