<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPromotionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_promotion', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->nullable();
			$table->integer('order')->unsigned()->nullable();
			$table->boolean('visibility')->default(0);
			$table->boolean('accumulate')->default(0);
			$table->integer('min_quantity')->unsigned()->nullable();
			$table->integer('max_quantity')->nullable();
			$table->decimal('promotion_amount', 20, 5)->nullable();
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_promotion_currency_id_foreign');
			$table->integer('promotion_type_id')->unsigned()->nullable()->index('mo_promotion_promotion_type_id_foreign');
			$table->integer('campaign_id')->unsigned()->nullable()->index('mo_promotion_campaign_id_foreign');
			$table->integer('department_id')->unsigned()->nullable()->index('mo_promotion_department_id_foreign');
			$table->integer('cost_center_id')->unsigned()->nullable()->index('mo_promotion_cost_center_id_foreign');
			$table->boolean('apply_payment_method_filter')->default(0);
			$table->boolean('apply_language_filter')->default(0);
			$table->boolean('apply_currency_filter')->default(0);
			$table->boolean('apply_pickup_filter')->default(0);
			$table->boolean('apply_product_filter')->default(0);
			$table->boolean('apply_product_type_filter')->default(0);
			$table->boolean('apply_service_filter')->default(0);
			$table->boolean('apply_subchannel_filter')->default(0);
			$table->boolean('apply_client_type_filter')->default(0);
			$table->boolean('apply_country_filter')->default(0);
			$table->boolean('apply_state_filter')->default(0);
			$table->boolean('apply_device_filter')->default(0);
			$table->boolean('lockers_validation')->nullable()->default(0);
			$table->boolean('supervisor_validation')->nullable()->default(0);
			$table->boolean('allow_date_change')->default(1);
			$table->boolean('allow_upgrade')->default(1);
			$table->boolean('allow_product_change')->default(1);
			$table->boolean('pay_difference')->default(1);
			$table->integer('anticipation_start')->unsigned()->nullable();
			$table->integer('anticipation_end')->unsigned()->nullable();
			$table->string('coupon_code')->nullable();
			$table->boolean('coupon_type')->default(0);
			$table->integer('coupon_sheet_start')->unsigned()->nullable();
			$table->integer('coupon_sheet_end')->unsigned()->nullable();
			$table->integer('coupon_total_uses')->unsigned()->nullable();
			$table->boolean('benefit_card')->default(0);
			$table->integer('benefit_card_total_uses')->nullable();
			$table->integer('benefit_card_id')->unsigned()->nullable()->index('mo_promotion_benefit_card_id_foreign');
			$table->integer('user_id')->unsigned()->nullable()->index('mo_promotion_user_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_promotion');
	}

}
