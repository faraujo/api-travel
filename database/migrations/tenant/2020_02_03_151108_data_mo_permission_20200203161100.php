<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission20200203161100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_permission')->insert([
            'id' => 230,
            'name' => 'Visualizar sobre nosotros',
            'description' => 'Permiso para listar sobre nosotros del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 231,
            'name' => 'Visualizar sobre nosotros',
            'description' => 'Permiso para listar un sobre nosotros del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 232,
            'name' => 'Actualizar sobre nosotros',
            'description' => 'Permiso para actualizar sobre nosotros del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 230
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 231
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 232
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
