<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoCurrencyExchange20200327083000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 1,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 2,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 3,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 4,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 5,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 6,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 7,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 8,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 9,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 10,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 11,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 12,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 13,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 14,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 15,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 16,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);

        DB::table('mo_currency_exchange')->insert([
            'currency_id' => 17,
            'date_time_start' => \Carbon\Carbon::now()->subYear()->format('Y-m-d H:i:s'),
            'exchange' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
