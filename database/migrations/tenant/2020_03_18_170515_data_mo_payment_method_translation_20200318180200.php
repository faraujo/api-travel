<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPaymentMethodTranslation20200318180200 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_payment_method_translation')->insert([
            'payment_method_id' => '11',
            'language_id' => 1,
            'name' => 'Paypal',
        ]);

        DB::table('mo_payment_method_translation')->insert([
            'payment_method_id' => '11',
            'language_id' => 2,
            'name' => 'Paypal',
        ]);

        DB::table('mo_payment_method_translation')->insert([
            'payment_method_id' => '11',
            'language_id' => 3,
            'name' => 'Paypal',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
