<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoInsuranceTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_insurance_translation', function(Blueprint $table)
		{
			$table->foreign('insurance_id')->references('id')->on('mo_insurance')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_insurance_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_insurance_translation_insurance_id_foreign');
			$table->dropForeign('mo_insurance_translation_language_id_foreign');
		});
	}

}
