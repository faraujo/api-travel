<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionPaymentMethodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_payment_method', function(Blueprint $table)
		{
			$table->foreign('payment_method_id')->references('id')->on('mo_payment_method')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_payment_method', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_payment_method_payment_method_id_foreign');
			$table->dropForeign('mo_promotion_payment_method_promotion_id_foreign');
		});
	}

}
