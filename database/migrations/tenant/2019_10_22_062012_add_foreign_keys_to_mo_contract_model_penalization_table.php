<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoContractModelPenalizationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_contract_model_penalization', function(Blueprint $table)
		{
			$table->foreign('contract_model_product_id', 'FK_mo_contract_model_penalization_mo_contract_model')->references('id')->on('mo_contract_model_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_contract_model_penalization', function(Blueprint $table)
		{
			$table->dropForeign('FK_mo_contract_model_penalization_mo_contract_model');
		});
	}

}
