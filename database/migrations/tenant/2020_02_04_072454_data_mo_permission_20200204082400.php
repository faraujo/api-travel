<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission20200204082400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_permission')->where('id', 230)->update([
            'name' => 'Visualizar páginas del front de venta',
            'description' => 'Permiso para listar páginas del front de venta',
        ]);
        DB::table('mo_permission')->where('id', 231)->update([
            'name' => 'Visualizar página',
            'description' => 'Permiso para listar una página del front de venta',
        ]);
        DB::table('mo_permission')->where('id', 232)->update([
            'name' => 'Actualizar páginas',
            'description' => 'Permiso para actualizar páginas del front de venta',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
