<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationPromotionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_promotion', function(Blueprint $table)
		{
			$table->foreign('benefit_card_id')->references('id')->on('mo_identification')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_type_id')->references('id')->on('mo_promotion_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_detail_id')->references('id')->on('mo_reservation_detail')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_id')->references('id')->on('mo_reservation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_detail_client_type_id', 'reservation_detail_client_type')->references('id')->on('mo_reservation_detail_client_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_promotion', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_promotion_benefit_card_id_foreign');
			$table->dropForeign('mo_reservation_promotion_currency_id_foreign');
			$table->dropForeign('mo_reservation_promotion_promotion_id_foreign');
			$table->dropForeign('mo_reservation_promotion_promotion_type_id_foreign');
			$table->dropForeign('mo_reservation_promotion_reservation_detail_id_foreign');
			$table->dropForeign('mo_reservation_promotion_reservation_id_foreign');
			$table->dropForeign('reservation_detail_client_type');
		});
	}

}
