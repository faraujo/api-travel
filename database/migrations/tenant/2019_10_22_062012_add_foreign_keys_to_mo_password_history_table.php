<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPasswordHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_password_history', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_password_history', function(Blueprint $table)
		{
			$table->dropForeign('mo_password_history_user_id_foreign');
		});
	}

}
