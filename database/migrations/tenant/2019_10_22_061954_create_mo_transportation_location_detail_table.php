<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoTransportationLocationDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_transportation_location_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('transportation_location_id')->unsigned()->nullable()->index('mo_transportation_location_detail_trans_location_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_product_product_id_foreign');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_subchannel_subchannel_id_foreign');
			$table->string('name')->nullable();
			$table->string('code')->nullable();
			$table->date('date')->nullable();
			$table->time('session')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_transportation_location_detail');
	}

}
