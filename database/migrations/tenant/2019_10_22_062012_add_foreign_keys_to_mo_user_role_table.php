<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoUserRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_user_role', function(Blueprint $table)
		{
			$table->foreign('role_id')->references('id')->on('mo_role')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_user_role', function(Blueprint $table)
		{
			$table->dropForeign('mo_user_role_role_id_foreign');
			$table->dropForeign('mo_user_role_user_id_foreign');
		});
	}

}
