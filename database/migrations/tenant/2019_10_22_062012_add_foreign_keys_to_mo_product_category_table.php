<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoProductCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_product_category', function(Blueprint $table)
		{
			$table->foreign('category_id')->references('id')->on('mo_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_product_category', function(Blueprint $table)
		{
			$table->dropForeign('mo_product_category_category_id_foreign');
			$table->dropForeign('mo_product_category_product_id_foreign');
		});
	}

}
