<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation', function(Blueprint $table)
		{
			$table->foreign('anti_fraud_level')->references('id')->on('mo_reservation_anti_fraud_level')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_country_id')->references('id')->on('mo_country')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_state_id')->references('id')->on('mo_state')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('commission_agent_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('credit_card_type')->references('id')->on('mo_payment_method')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('payment_method_id')->references('id')->on('mo_payment_method')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_anti_fraud_level_foreign');
			$table->dropForeign('mo_reservation_client_country_id_foreign');
			$table->dropForeign('mo_reservation_client_id_foreign');
			$table->dropForeign('mo_reservation_client_state_id_foreign');
			$table->dropForeign('mo_reservation_commission_agent_id_foreign');
			$table->dropForeign('mo_reservation_credit_card_type_foreign');
			$table->dropForeign('mo_reservation_language_id_foreign');
			$table->dropForeign('mo_reservation_payment_method_id_foreign');
			$table->dropForeign('mo_reservation_subchannel_id_foreign');
		});
	}

}
