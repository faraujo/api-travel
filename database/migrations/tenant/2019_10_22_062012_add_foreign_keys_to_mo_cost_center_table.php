<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCostCenterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_cost_center', function(Blueprint $table)
		{
			$table->foreign('department_id')->references('id')->on('mo_department')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_cost_center', function(Blueprint $table)
		{
			$table->dropForeign('mo_cost_center_department_id_foreign');
		});
	}

}
