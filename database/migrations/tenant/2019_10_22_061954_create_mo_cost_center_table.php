<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoCostCenterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_cost_center', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('department_id')->unsigned()->nullable()->index('mo_cost_center_department_id_foreign');
			$table->string('name')->nullable();
			$table->string('code')->nullable();
			$table->string('accounting_cost_center')->nullable();
			$table->string('accounting_code')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_cost_center');
	}

}
