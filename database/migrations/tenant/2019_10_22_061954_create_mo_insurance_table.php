<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoInsuranceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_insurance', function(Blueprint $table)
		{
			$table->increments('id');
			$table->decimal('sale_price', 20, 5)->nullable();
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_insurance_currency_id_foreign');
			$table->integer('iva_id')->unsigned()->nullable()->index('mo_insurance_iva_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_insurance');
	}

}
