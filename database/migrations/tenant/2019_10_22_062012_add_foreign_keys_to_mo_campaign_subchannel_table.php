<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCampaignSubchannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_campaign_subchannel', function(Blueprint $table)
		{
			$table->foreign('campaign_id')->references('id')->on('mo_campaign')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_campaign_subchannel', function(Blueprint $table)
		{
			$table->dropForeign('mo_campaign_subchannel_campaign_id_foreign');
			$table->dropForeign('mo_campaign_subchannel_subchannel_id_foreign');
		});
	}

}
