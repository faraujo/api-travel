<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoWebPageTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mo_web_page_translation', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('page_id')->nullable()->unsigned();
            $table->foreign('page_id')
                ->references('id')
                ->on('mo_web_page');
            $table->integer('language_id')->nullable()->unsigned();
            $table->foreign('language_id')
                ->references('id')
                ->on('mo_language');
            $table->longText('content')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mo_web_page_translation');
    }
}
