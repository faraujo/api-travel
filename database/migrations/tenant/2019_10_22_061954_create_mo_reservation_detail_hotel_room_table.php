<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationDetailHotelRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_detail_hotel_room', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_hotel_id')->unsigned()->nullable()->index('mo_reservation_hotel_room_reservation_hotel_id_foreign');
			$table->integer('room_id')->unsigned()->nullable()->index('mo_reservation_detail_hotel_room_room_id_foreign');
			$table->string('room_type_name')->nullable();
			$table->string('rate_plan_code')->nullable();
			$table->string('confirmation_code')->nullable();
			$table->text('external_request')->nullable();
			$table->text('external_response')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_detail_hotel_room');
	}

}
