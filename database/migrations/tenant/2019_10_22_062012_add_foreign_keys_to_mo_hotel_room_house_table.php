<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHotelRoomHouseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_hotel_room_house', function(Blueprint $table)
		{
			$table->foreign('house_id')->references('id')->on('mo_house')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('room_id')->references('id')->on('mo_hotel_room')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_hotel_room_house', function(Blueprint $table)
		{
			$table->dropForeign('mo_hotel_room_house_house_id_foreign');
			$table->dropForeign('mo_hotel_room_house_room_id_foreign');
		});
	}

}
