<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoSearchLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_search_log', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_search_log_subchannel_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_search_log_language_id_foreign');
			$table->integer('category_id')->unsigned()->nullable()->index('mo_search_log_category_id_foreign');
			$table->integer('tag_id')->unsigned()->nullable()->index('mo_search_log_tag_id_foreign');
			$table->integer('product_id')->unsigned()->nullable()->index('mo_search_log_product_id_foreign');
			$table->integer('user_id')->unsigned()->nullable()->index('mo_search_log_user_id_foreign');
			$table->integer('product_type_id')->unsigned()->nullable()->index('mo_search_log_product_type_id_foreign');
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_search_log_currency_id_foreign');
			$table->integer('depends_on')->unsigned()->nullable()->index('mo_search_log_depends_on_foreign');
			$table->integer('money')->unsigned()->nullable();
			$table->integer('availability')->unsigned()->nullable();
			$table->date('date')->nullable();
			$table->integer('hotel_room_id')->unsigned()->nullable()->index('mo_search_log_hotel_room_id_foreign');
			$table->integer('hotel_room_category_id')->unsigned()->nullable()->index('mo_search_log_hotel_room_category_id_foreign');
			$table->date('hotel_date_start')->nullable();
			$table->date('hotel_date_end')->nullable();
			$table->integer('hotel_adult')->unsigned()->nullable();
			$table->integer('hotel_child')->unsigned()->nullable();
			$table->integer('hotel_number_room')->unsigned()->nullable();
			$table->integer('hotel_info_only')->unsigned()->nullable();
			$table->integer('total_first_search_results')->unsigned()->nullable();
			$table->integer('total_second_search_results')->unsigned()->nullable();
			$table->string('searched_word')->nullable();
			$table->boolean('result')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_search_log');
	}

}
