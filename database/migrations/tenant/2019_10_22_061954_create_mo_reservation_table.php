<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->nullable();
			$table->integer('anti_fraud_level')->unsigned()->nullable()->index('mo_reservation_anti_fraud_level_foreign');
			$table->string('anti_fraud_code')->nullable();
			$table->text('notes')->nullable();
			$table->integer('subchannel_id')->unsigned()->nullable()->index('mo_reservation_subchannel_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_reservation_language_id_foreign');
			$table->integer('client_id')->unsigned()->nullable()->index('mo_reservation_client_id_foreign');
			$table->string('client_name')->nullable();
			$table->string('client_surname')->nullable();
			$table->integer('client_country_id')->unsigned()->nullable()->index('mo_reservation_client_country_id_foreign');
			$table->integer('client_state_id')->unsigned()->nullable()->index('mo_reservation_client_state_id_foreign');
			$table->string('client_telephone1')->nullable();
			$table->string('client_email')->nullable();
			$table->text('credit_card', 65535)->nullable();
			$table->string('credit_card_number')->nullable();
			$table->string('credit_card_cvv')->nullable();
			$table->string('credit_expiry_date')->nullable();
			$table->integer('credit_card_type')->unsigned()->nullable()->index('mo_reservation_credit_card_type_foreign');
			$table->string('credit_card_name')->nullable();
			$table->string('credit_card_surname')->nullable();
			$table->integer('commission_agent_id')->unsigned()->nullable()->index('mo_reservation_commission_agent_id_foreign');
			$table->string('client_session')->nullable();
			$table->integer('payment_method_id')->unsigned()->nullable()->index('mo_reservation_payment_method_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation');
	}

}
