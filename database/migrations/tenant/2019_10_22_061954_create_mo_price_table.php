<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPriceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_price', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contract_model_id')->unsigned()->nullable()->index('mo_price_contract_model_id_foreign');
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_price_currency_id_foreign');
			$table->boolean('promotions')->default(0);
			$table->date('date_start')->nullable();
			$table->date('date_end')->nullable();
			$table->integer('user_id')->unsigned()->nullable()->index('mo_price_user_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_price');
	}

}
