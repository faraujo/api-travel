<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationDetailPhotoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_detail_photo', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reservation_detail_id')->unsigned()->nullable()->index('mo_reservation_photo_reservation_detail_id_foreign');
			$table->date('date')->nullable();
			$table->time('session')->nullable();
			$table->time('product_session')->nullable();
			$table->string('code_download')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_detail_photo');
	}

}
