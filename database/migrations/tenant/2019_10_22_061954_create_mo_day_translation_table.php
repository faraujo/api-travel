<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoDayTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_day_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('day_id')->unsigned()->nullable()->index('mo_day_translation_day_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_day_translation_language_id_foreign');
			$table->string('name')->nullable();
			$table->string('abbreviation')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_day_translation');
	}

}
