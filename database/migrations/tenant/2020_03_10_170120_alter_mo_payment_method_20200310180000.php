<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMoPaymentMethod20200310180000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_payment_method')->where('id', 10)->update([
            'front' => 0,
            'back' => 1,
        ]);

        DB::table('mo_payment_method')->where('id', 11)->update([
            'front' => 1,
            'back' => 0,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('mo_payment_method')->where('id', 10)->update([
            'front' => 0,
            'back' => 0,
        ]);

        DB::table('mo_payment_method')->where('id', 11)->update([
            'front' => 0,
            'back' => 0,
        ]);
    }
}
