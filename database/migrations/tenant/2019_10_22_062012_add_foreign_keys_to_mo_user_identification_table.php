<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoUserIdentificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_user_identification', function(Blueprint $table)
		{
			$table->foreign('identification_id')->references('id')->on('mo_identification')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_user_identification', function(Blueprint $table)
		{
			$table->dropForeign('mo_user_identification_identification_id_foreign');
			$table->dropForeign('mo_user_identification_user_id_foreign');
		});
	}

}
