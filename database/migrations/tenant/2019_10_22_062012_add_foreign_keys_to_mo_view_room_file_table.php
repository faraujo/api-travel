<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoViewRoomFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_view_room_file', function(Blueprint $table)
		{
			$table->foreign('file_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('view_room_id')->references('id')->on('mo_view_room')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_view_room_file', function(Blueprint $table)
		{
			$table->dropForeign('mo_view_room_file_file_id_foreign');
			$table->dropForeign('mo_view_room_file_view_room_id_foreign');
		});
	}

}
