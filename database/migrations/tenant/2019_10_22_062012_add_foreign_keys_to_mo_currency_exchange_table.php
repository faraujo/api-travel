<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCurrencyExchangeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_currency_exchange', function(Blueprint $table)
		{
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_currency_exchange', function(Blueprint $table)
		{
			$table->dropForeign('mo_currency_exchange_currency_id_foreign');
			$table->dropForeign('mo_currency_exchange_user_id_foreign');
		});
	}

}
