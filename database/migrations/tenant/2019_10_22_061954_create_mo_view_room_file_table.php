<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoViewRoomFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_view_room_file', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('view_room_id')->unsigned()->nullable()->index('mo_view_room_file_view_room_id_foreign');
			$table->integer('file_id')->unsigned()->nullable()->index('mo_view_room_file_file_id_foreign');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_view_room_file');
	}

}
