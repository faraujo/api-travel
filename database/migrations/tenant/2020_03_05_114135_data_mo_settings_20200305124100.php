<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoSettings20200305124100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_settings')->insert([
            'name' => 'paypal_username',
            'value' => ''
        ]);

        DB::table('mo_settings')->insert([
            'name' => 'paypal_password',
            'value' => ''
        ]);

        DB::table('mo_settings')->insert([
            'name' => 'paypal_secret',
            'value' => ''
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
