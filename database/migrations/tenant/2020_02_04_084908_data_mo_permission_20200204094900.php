<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoPermission20200204094900 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_permission')->insert([
            'id' => 233,
            'name' => 'Visualizar sliders',
            'description' => 'Permiso para listar sliders del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 234,
            'name' => 'Visualizar slider',
            'description' => 'Permiso para listar un slider del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_permission')->insert([
            'id' => 235,
            'name' => 'Actualizar sliders',
            'description' => 'Permiso para actualizar sliders del front de venta',
            'order' => 0,
            'module_id' => 29,
            'admin' => 1,
            'subchannel' => 0,
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 233
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 234
        ]);

        DB::table('mo_role_permission')->insert([
            'role_id' => 1,
            'permission_id' => 235
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
