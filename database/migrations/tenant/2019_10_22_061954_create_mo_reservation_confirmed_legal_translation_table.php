<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoReservationConfirmedLegalTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_reservation_confirmed_legal_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_reservation_confirmed_legal_translation_language_id_foreign');
			$table->integer('reservation_confirmed_legal_id')->unsigned()->nullable()->index('mo_r_c_legal_translation_r_c_legal_id_foreign');
			$table->text('legal_1')->nullable();
			$table->text('legal_2')->nullable();
			$table->text('legal_3')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_reservation_confirmed_legal_translation');
	}

}
