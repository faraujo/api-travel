<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoContractModelPenalizationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_contract_model_penalization', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('contract_model_product_id')->unsigned()->index('FK_mo_contract_model_penalization_mo_contract_model');
			$table->integer('days')->nullable()->comment('Días de cancelación ');
			$table->decimal('penalization_amount', 20, 5)->nullable()->comment('Porcentaje de penalización');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_contract_model_penalization');
	}

}
