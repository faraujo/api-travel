<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPriceProductIntercompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_price_product_intercompany', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('price_product_id')->unsigned()->nullable()->index('mo_price_product_intercompany_price_product_id_foreign');
			$table->integer('currency_id')->unsigned()->nullable()->index('mo_price_product_intercompany_currency_id_foreign');
			$table->integer('company_id')->unsigned()->nullable()->index('mo_price_product_intercompany_company_id_foreign');
			$table->decimal('amount', 20, 5)->nullable();
			$table->decimal('percent', 20, 5)->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_price_product_intercompany');
	}

}
