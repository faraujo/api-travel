<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoAdditionalServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_additional_service', function(Blueprint $table)
		{
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('type_id')->references('id')->on('mo_additional_service_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_additional_service', function(Blueprint $table)
		{
			$table->dropForeign('mo_additional_service_product_id_foreign');
			$table->dropForeign('mo_additional_service_type_id_foreign');
		});
	}

}
