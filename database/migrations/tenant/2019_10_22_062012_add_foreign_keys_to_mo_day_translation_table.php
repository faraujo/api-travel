<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoDayTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_day_translation', function(Blueprint $table)
		{
			$table->foreign('day_id')->references('id')->on('mo_day')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_day_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_day_translation_day_id_foreign');
			$table->dropForeign('mo_day_translation_language_id_foreign');
		});
	}

}
