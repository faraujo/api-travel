<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoHouseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_house', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code')->nullable();
			$table->integer('order')->unsigned()->nullable();
			$table->decimal('longitude', 20, 8)->nullable();
			$table->decimal('latitude', 20, 8)->nullable();
			$table->text('address', 65535)->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_house');
	}

}
