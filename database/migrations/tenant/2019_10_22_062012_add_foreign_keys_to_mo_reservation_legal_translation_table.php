<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationLegalTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_legal_translation', function(Blueprint $table)
		{
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_legal_id')->references('id')->on('mo_reservation_legal')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_legal_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_legal_translation_language_id_foreign');
			$table->dropForeign('mo_reservation_legal_translation_reservation_legal_id_foreign');
		});
	}

}
