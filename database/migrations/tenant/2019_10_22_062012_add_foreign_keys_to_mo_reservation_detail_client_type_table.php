<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationDetailClientTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_detail_client_type', function(Blueprint $table)
		{
			$table->foreign('client_id', 'mo_reservation_client_type_client_id_foreign')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_type_id', 'mo_reservation_client_type_client_type_id_foreign')->references('id')->on('mo_client_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_detail_id', 'mo_reservation_client_type_reservation_detail_id_foreign')->references('id')->on('mo_reservation_detail')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_country_id')->references('id')->on('mo_country')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_state_id')->references('id')->on('mo_state')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_detail_client_type', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_client_type_client_id_foreign');
			$table->dropForeign('mo_reservation_client_type_client_type_id_foreign');
			$table->dropForeign('mo_reservation_client_type_reservation_detail_id_foreign');
			$table->dropForeign('mo_reservation_detail_client_type_client_country_id_foreign');
			$table->dropForeign('mo_reservation_detail_client_type_client_state_id_foreign');
		});
	}

}
