<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoWebLegalTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mo_web_legal_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('legal_id')->nullable()->unsigned();
            $table->foreign('legal_id')
                ->references('id')
                ->on('mo_web_legal');
            $table->integer('language_id')->nullable()->unsigned();
            $table->foreign('language_id')
                ->references('id')
                ->on('mo_language');
            $table->longText('legal');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mo_web_legal_translation');
    }
}
