<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoPromotionCouponTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_promotion_coupon_translation', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('coupon_id')->unsigned()->nullable()->index('mo_promotion_coupon_translation_coupon_id_foreign');
			$table->integer('language_id')->unsigned()->nullable()->index('mo_promotion_coupon_translation_language_id_foreign');
			$table->integer('file_id')->unsigned()->nullable()->index('mo_promotion_coupon_translation_file_id_foreign');
			$table->text('legal', 65535)->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_promotion_coupon_translation');
	}

}
