<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoContractModelProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_contract_model_product', function(Blueprint $table)
		{
			$table->foreign('contract_model_id')->references('id')->on('mo_contract_model')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('product_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('service_id')->references('id')->on('mo_service')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_contract_model_product', function(Blueprint $table)
		{
			$table->dropForeign('mo_contract_model_product_contract_model_id_foreign');
			$table->dropForeign('mo_contract_model_product_product_id_foreign');
			$table->dropForeign('mo_contract_model_product_service_id_foreign');
		});
	}

}
