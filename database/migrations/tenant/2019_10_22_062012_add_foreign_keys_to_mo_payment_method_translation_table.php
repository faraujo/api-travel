<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPaymentMethodTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_payment_method_translation', function(Blueprint $table)
		{
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('payment_method_id')->references('id')->on('mo_payment_method')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_payment_method_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_payment_method_translation_language_id_foreign');
			$table->dropForeign('mo_payment_method_translation_payment_method_id_foreign');
		});
	}

}
