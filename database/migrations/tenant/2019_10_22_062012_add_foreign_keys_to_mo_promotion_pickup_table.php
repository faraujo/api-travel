<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionPickupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_pickup', function(Blueprint $table)
		{
			$table->foreign('pickup_id')->references('id')->on('mo_transportation_location')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_pickup', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_pickup_pickup_id_foreign');
			$table->dropForeign('mo_promotion_pickup_promotion_id_foreign');
		});
	}

}
