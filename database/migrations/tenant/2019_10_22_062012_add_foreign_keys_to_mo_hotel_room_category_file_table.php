<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHotelRoomCategoryFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_hotel_room_category_file', function(Blueprint $table)
		{
			$table->foreign('file_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('hotel_room_category_id')->references('id')->on('mo_hotel_room_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_hotel_room_category_file', function(Blueprint $table)
		{
			$table->dropForeign('mo_hotel_room_category_file_file_id_foreign');
			$table->dropForeign('mo_hotel_room_category_file_hotel_room_category_id_foreign');
		});
	}

}
