<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoSubchannelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_subchannel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('channel_id')->unsigned()->nullable()->index('mo_subchannel_channel_id_foreign');
			$table->integer('contract_model_id')->unsigned()->nullable()->index('mo_subchannel_contract_model_id_foreign');
			$table->string('prefix')->nullable();
			$table->integer('reservation_code')->default(0);
			$table->boolean('view_blocked')->default(0);
			$table->text('authorization_token')->nullable();
			$table->string('api_code')->nullable();
			$table->string('cashier')->nullable();
			$table->boolean('sendconfirmedemail')->default(1);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_subchannel');
	}

}
