<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoFileType20200210171400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_file_type_translation')->where('type_id','!=', 1)->where('type_id','!=', 2)->where('type_id','!=', 3)->where('type_id','!=', 4)->delete();

        DB::table('mo_file_type')->where('id','!=', 1)->where('id','!=', 2)->where('id','!=', 3)->where('id','!=', 4)->delete();

        DB::table('mo_file_type_translation')->where('type_id', 1)->where('language_id', 1)->update([
            'name' => 'Imagen destacada'
        ]);

        DB::table('mo_file_type_translation')->where('type_id', 1)->where('language_id', 2)->update([
            'name' => 'Outstanding image'
        ]);

        DB::table('mo_file_type_translation')->where('type_id', 1)->where('language_id', 3)->update([
            'name' => 'Imagem em destaque'
        ]);

        DB::table('mo_file_type_translation')->where('type_id', 4)->where('language_id', 1)->update([
            'name' => 'Imagen de cabecera'
        ]);

        DB::table('mo_file_type_translation')->where('type_id', 4)->where('language_id', 2)->update([
            'name' => 'Header image'
        ]);

        DB::table('mo_file_type_translation')->where('type_id', 4)->where('language_id', 3)->update([
            'name' => 'Imagem do cabeçalho'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
