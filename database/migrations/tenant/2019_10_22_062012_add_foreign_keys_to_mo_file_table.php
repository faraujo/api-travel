<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_file', function(Blueprint $table)
		{
			$table->foreign('device_id')->references('id')->on('mo_device')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('type_id')->references('id')->on('mo_file_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_file', function(Blueprint $table)
		{
			$table->dropForeign('mo_file_device_id_foreign');
			$table->dropForeign('mo_file_type_id_foreign');
		});
	}

}
