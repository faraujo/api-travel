<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_payment', function(Blueprint $table)
		{
			$table->foreign('agent_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('client_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_exchange_id')->references('id')->on('mo_currency_exchange')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('payment_method_id')->references('id')->on('mo_payment_method')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_detail_id')->references('id')->on('mo_reservation_detail')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_id')->references('id')->on('mo_reservation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('subchannel_id')->references('id')->on('mo_subchannel')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_payment', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_payment_agent_id_foreign');
			$table->dropForeign('mo_reservation_payment_client_id_foreign');
			$table->dropForeign('mo_reservation_payment_currency_exchange_id_foreign');
			$table->dropForeign('mo_reservation_payment_currency_id_foreign');
			$table->dropForeign('mo_reservation_payment_payment_method_id_foreign');
			$table->dropForeign('mo_reservation_payment_reservation_detail_id_foreign');
			$table->dropForeign('mo_reservation_payment_reservation_id_foreign');
			$table->dropForeign('mo_reservation_payment_subchannel_id_foreign');
		});
	}

}
