<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionStateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_state', function(Blueprint $table)
		{
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('state_id')->references('id')->on('mo_state')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_state', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_state_promotion_id_foreign');
			$table->dropForeign('mo_promotion_state_state_id_foreign');
		});
	}

}
