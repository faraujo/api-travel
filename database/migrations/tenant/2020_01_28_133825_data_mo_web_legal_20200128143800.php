<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoWebLegal20200128143800 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_web_legal')->insert([
            'id' => 1,
            'type' => 'cookie_policy'
        ]);

        DB::table('mo_web_legal')->insert([
            'id' => 2,
            'type' => 'privacy_conditions'
        ]);

        DB::table('mo_web_legal')->insert([
            'id' => 3,
            'type' => 'confidentiality_policy'
        ]);

        DB::table('mo_web_legal')->insert([
            'id' => 4,
            'type' => 'legal_warning'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
