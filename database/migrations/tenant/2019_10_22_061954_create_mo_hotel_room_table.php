<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoHotelRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_hotel_room', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('hotel_id')->unsigned()->nullable()->index('mo_hotel_room_hotel_id_foreign');
			$table->integer('category_id')->unsigned()->nullable()->index('mo_hotel_room_category_id_foreign');
			$table->string('code')->nullable();
			$table->integer('uom_id')->unsigned()->nullable()->index('mo_hotel_room_uom_id_foreign');
			$table->integer('order')->unsigned()->nullable();
			$table->decimal('longitude', 20, 8)->nullable();
			$table->decimal('latitude', 20, 8)->nullable();
			$table->text('address', 65535)->nullable();
			$table->boolean('pax')->default(0);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_hotel_room');
	}

}
