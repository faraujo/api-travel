<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionClientTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_client_type', function(Blueprint $table)
		{
			$table->foreign('client_type_id')->references('id')->on('mo_client_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_id')->references('id')->on('mo_promotion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_client_type', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_client_type_client_type_id_foreign');
			$table->dropForeign('mo_promotion_client_type_promotion_id_foreign');
		});
	}

}
