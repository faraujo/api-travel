<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoWebAboutUsTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mo_web_about_us_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('about_us_id')->unsigned()->nullable();
            $table->foreign('about_us_id')
                ->references('id')
                ->on('mo_web_about_us');
            $table->integer('language_id')->unsigned()->nullable();
            $table->foreign('language_id')
                ->references('id')
                ->on('mo_language');
            $table->longtext('description')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mo_web_about_us_translation');
    }
}
