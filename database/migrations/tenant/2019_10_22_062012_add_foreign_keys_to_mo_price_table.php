<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPriceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_price', function(Blueprint $table)
		{
			$table->foreign('contract_model_id')->references('id')->on('mo_contract_model')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_price', function(Blueprint $table)
		{
			$table->dropForeign('mo_price_contract_model_id_foreign');
			$table->dropForeign('mo_price_currency_id_foreign');
			$table->dropForeign('mo_price_user_id_foreign');
		});
	}

}
