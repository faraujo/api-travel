<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMoReservationPayment20200219080000 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('mo_reservation_payment', function (Blueprint $table) {
            $table->longText('request_paypal')->nullable()->after('response_code');
            $table->longText('response_paypal')->nullable()->after('request_paypal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mo_reservation_payment', function (Blueprint $table) {
            $table->dropColumn('request_paypal');
            $table->dropColumn('response_paypal');
        });
    }
}
