<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoSettings20200306085500 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_settings')->where('name', 'protocol')->delete();
        DB::table('mo_settings')->where('name', 'base_path')->delete();
        DB::table('mo_settings')->where('name', 'paypal_invoice_name')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('mo_settings')->insert([
            'name'  => 'protocol',
            'value' => 'https://'
        ]);

        DB::table('mo_settings')->insert([
            'name'  => 'base_path',
            'value' => 'viavoxexperience.com'
        ]);

        DB::table('mo_settings')->insert([
            'name' => 'paypal_invoice_name',
            'value' => 'Nombre de pedido paypal'
        ]);
    }
}
