<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoHotelRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_hotel_room', function(Blueprint $table)
		{
			$table->foreign('category_id')->references('id')->on('mo_hotel_room_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('hotel_id')->references('id')->on('mo_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('uom_id')->references('id')->on('mo_uom')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_hotel_room', function(Blueprint $table)
		{
			$table->dropForeign('mo_hotel_room_category_id_foreign');
			$table->dropForeign('mo_hotel_room_hotel_id_foreign');
			$table->dropForeign('mo_hotel_room_uom_id_foreign');
		});
	}

}
