<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataMoWebSocialNetwork20200130120900 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Facebook'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Instagram'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Twitter'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'LinkedIn'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Tiktok'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Snapchat'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Youtube'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Vimeo'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Pinterest'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'WhatsApp'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Telegram'
        ]);

        DB::table('mo_web_social_network')->insert([
            'social_network' => 'Blog '
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
