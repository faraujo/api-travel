<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoCategoryFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_category_file', function(Blueprint $table)
		{
			$table->foreign('category_id')->references('id')->on('mo_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('file_id')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_category_file', function(Blueprint $table)
		{
			$table->dropForeign('mo_category_file_category_id_foreign');
			$table->dropForeign('mo_category_file_file_id_foreign');
		});
	}

}
