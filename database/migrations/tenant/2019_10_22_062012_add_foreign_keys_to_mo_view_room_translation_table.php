<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoViewRoomTranslationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_view_room_translation', function(Blueprint $table)
		{
			$table->foreign('language_id')->references('id')->on('mo_language')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('og_image')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('twitter_image')->references('id')->on('mo_file')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('view_room_id')->references('id')->on('mo_view_room')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_view_room_translation', function(Blueprint $table)
		{
			$table->dropForeign('mo_view_room_translation_language_id_foreign');
			$table->dropForeign('mo_view_room_translation_og_image_foreign');
			$table->dropForeign('mo_view_room_translation_twitter_image_foreign');
			$table->dropForeign('mo_view_room_translation_view_room_id_foreign');
		});
	}

}
