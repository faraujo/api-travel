<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPriceProductIntercompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_price_product_intercompany', function(Blueprint $table)
		{
			$table->foreign('company_id')->references('id')->on('mo_company')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('currency_id')->references('id')->on('mo_currency')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('price_product_id')->references('id')->on('mo_price_product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_price_product_intercompany', function(Blueprint $table)
		{
			$table->dropForeign('mo_price_product_intercompany_company_id_foreign');
			$table->dropForeign('mo_price_product_intercompany_currency_id_foreign');
			$table->dropForeign('mo_price_product_intercompany_price_product_id_foreign');
		});
	}

}
