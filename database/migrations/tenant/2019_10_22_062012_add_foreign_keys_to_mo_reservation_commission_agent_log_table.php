<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoReservationCommissionAgentLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_reservation_commission_agent_log', function(Blueprint $table)
		{
			$table->foreign('new_agent_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('old_agent_id')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('reservation_id')->references('id')->on('mo_reservation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('supervisor_reassignment_id', 'mo_reservation_commission_agent_log_super_reas_foreign')->references('id')->on('mo_user')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_reservation_commission_agent_log', function(Blueprint $table)
		{
			$table->dropForeign('mo_reservation_commission_agent_log_new_agent_id_foreign');
			$table->dropForeign('mo_reservation_commission_agent_log_old_agent_id_foreign');
			$table->dropForeign('mo_reservation_commission_agent_log_reservation_id_foreign');
			$table->dropForeign('mo_reservation_commission_agent_log_super_reas_foreign');
		});
	}

}
