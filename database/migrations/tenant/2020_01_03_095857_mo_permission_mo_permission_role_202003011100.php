<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoPermissionMoPermissionRole202003011100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::Table('mo_permission')->insert(
            [
                    'id' => 212,
                    'name' => 'Crear tasas de Iva',
                    'description' => 'Permiso para crear tasas de Iva',
                    'module_id' => 2,
                    'order' => 3,
                    'admin' => 1,
                    'subchannel' => 0,
                    'deleted_at' => NULL,
                
            ]
        );

        DB::Table('mo_role_permission')->insert(
            [
                'permission_id' => 212,
                'role_id' => 1,
                'subchannel_id' => NULL,
                'deleted_at' => NULL,
                
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
