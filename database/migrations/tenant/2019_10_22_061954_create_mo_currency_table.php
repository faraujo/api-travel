<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mo_currency', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('simbol')->nullable();
			$table->integer('iso_number')->nullable();
			$table->string('iso_code')->nullable();
			$table->string('iso2')->nullable();
			$table->string('presentation_simbol')->nullable();
			$table->enum('position', array('I','D'))->nullable();
			$table->string('decimal_separator')->nullable();
			$table->string('thousand_separator')->nullable();
			$table->boolean('automatic_conversion')->default(0);
			$table->string('automatic_service')->nullable();
			$table->string('automatic_environment')->nullable();
			$table->string('automatic_user')->nullable();
			$table->string('automatic_password')->nullable();
			$table->string('automatic_token')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mo_currency');
	}

}
