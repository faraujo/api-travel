<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMoPromotionSaleDayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mo_promotion_sale_day', function(Blueprint $table)
		{
			$table->foreign('day_id')->references('id')->on('mo_day')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('promotion_range_sale_day_id')->references('id')->on('mo_promotion_range_sale_day')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mo_promotion_sale_day', function(Blueprint $table)
		{
			$table->dropForeign('mo_promotion_sale_day_day_id_foreign');
			$table->dropForeign('mo_promotion_sale_day_promotion_range_sale_day_id_foreign');
		});
	}

}
