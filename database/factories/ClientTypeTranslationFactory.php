<?php

$factory->define(App\Models\tenant\ClientTypeTranslation::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text,
    ];
});
