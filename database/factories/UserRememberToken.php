<?php

use Faker\Generator as Faker;

$factory->define(App\Models\tenant\UserRememberToken::class, function (Faker $faker) {
    return [
        'token' => $faker->randomNumber(),
        'ip' => '::1',
    ];
});
