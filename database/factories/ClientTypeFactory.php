<?php

$factory->define(App\Models\tenant\ClientType::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->text,
        'from' => $faker->numberBetween(1,15),
        'to' => $faker->numberBetween(16,90),
        'api_id' => $faker->numberBetween(1,100),
        'hotel_id' => $faker->text,
        'hotel_code' => $faker->text,
        'hotel_age_required' => $faker->numberBetween(0, 1),
    ];
});
