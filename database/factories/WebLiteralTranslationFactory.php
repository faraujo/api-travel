<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Literal Translation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebLiteralTranslation::class, function (Faker $faker) {
    return [
    ];
});