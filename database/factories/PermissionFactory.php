<?php
$factory->define(App\Models\tenant\Permission::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
    ];
});
