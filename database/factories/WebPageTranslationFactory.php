<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Page Translation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebPageTranslation::class, function (Faker $faker) {
    return [
    ];
});