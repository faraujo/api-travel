<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Category factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Category::class, function (Faker $faker) {
    return [
        'order' => 1,
    ];
});
