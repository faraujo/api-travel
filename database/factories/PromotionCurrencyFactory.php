<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Promotion Currency factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PromotionCurrency::class, function (Faker $faker) {
    return [
    ];
});