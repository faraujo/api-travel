<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\tenant\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'telephone1' => $faker->phoneNumber,
        'telephone2' => $faker->phoneNumber,
        'telephone3' => $faker->phoneNumber,
        'city' => $faker->text,
        'business' => $faker->text,
        'telephone_business' => $faker->phoneNumber,
        'observations' => $faker->text,
        'email' => $faker->unique()->safeEmail,
        'name' => $faker->text,
        'surname' => $faker->text,
        'number_document' => '11111',
        'birthdate' => '1983-12-28',
        'email2' => 'dada@viavox.com',
        'password' => bcrypt($faker->text),
        'language_id' => 1,
        'address' => $faker->address,
        'postal_code' => '43300',
        'city' => 'Santander',
        'bloqued_login' => 0,
        'worker' => $faker->numberBetween(0, 1),
        //'remember_token' => str_random(10),
    ];
});
