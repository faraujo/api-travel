<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Product factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PriceProduct::class, function (Faker $faker) {
    return [
        'net_price' => 10,
        'sale_price' => 11,
        'markup' => 10,
        'price_before' => 15,
    ];
});
