<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Category factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\HotelRoom::class, function (Faker $faker) {
    return [
        'order' => 2,
    ];
});