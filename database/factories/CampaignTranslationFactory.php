<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un CampaignTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\CampaignTranslation::class, function (Faker $faker) {
    return [
    ];
});