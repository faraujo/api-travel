<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un User factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
        'surname' => $faker->text,
        'number_document' => '11111',
        'birthdate' => '1983-12-28',
        'email' => 'dada@viavox.com',
        'email2' => 'dada@viavox.com',
        'language_id' => 1,
        'telephone1' => '999999',
        'telephone2' => '7777',
        'telephone3' => '8888',
        'address' => $faker->address,
        'postal_code' => '43300',
        'city' => 'Santander',
        'business' => 'business',
        'telephone_business' => '45454545',
        'observations' => $faker->text,
        'bloqued_login' => $faker->numberBetween(0, 1),
        'worker' => $faker->numberBetween(0, 1),

    ];
});