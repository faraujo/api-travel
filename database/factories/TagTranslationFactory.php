<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un TagTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\TagTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
        'description' => $faker->text,
        'short_description' => $faker->text,
        'friendly_url' => $faker->text,
        'title_seo' => $faker->text,
        'description_seo' => $faker->text,
        'keywords_seo' => $faker->text,
    ];
});