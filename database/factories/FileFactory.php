<?php

use Faker\Generator as Faker;


$factory->define(/**
* Función que crea un File factory para los test
* @param Faker $faker
* @return array
*/
    App\Models\tenant\File::class, function (Faker $faker) {
    return [
        'order' => 1,
        'private' => 0,
        'url_file' => $faker->url,
    ];
});