<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Product factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Product::class, function (Faker $faker) {
    return [
        'code' => $faker->text,
        'address' => $faker->text,
    ];
});