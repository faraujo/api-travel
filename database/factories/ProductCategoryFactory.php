<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un ProductCategory factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\ProductCategory::class, function (Faker $faker) {
    return [
    ];
});