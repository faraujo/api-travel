<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Literal factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebLiteral::class, function (Faker $faker) {
    return [
    ];
});