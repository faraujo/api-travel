<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un ChannelTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\ChannelTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
    ];
});