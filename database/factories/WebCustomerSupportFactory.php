<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Customer Support factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebCustomerSupport::class, function (Faker $faker) {
    return [
    ];
});