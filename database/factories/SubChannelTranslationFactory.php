<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un SubChannelTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\SubChannelTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
    ];
});