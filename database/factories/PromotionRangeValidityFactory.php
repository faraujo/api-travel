<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Promotion range validity factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PromotionRangeValidity::class, function (Faker $faker) {
    return [
    ];
});