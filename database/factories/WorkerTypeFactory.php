<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un WorkerType factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WorkerType::class, function (Faker $faker) {
    return [
    ];
});