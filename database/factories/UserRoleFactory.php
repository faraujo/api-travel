<?php
$factory->define(App\Models\tenant\UserRole::class, function (Faker\Generator $faker) {
    return [
        'role_id' => $faker->numberBetween(1,3),
        'user_id' => $faker->numberBetween(1,3),
    ];
});
