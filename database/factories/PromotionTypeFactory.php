<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un PromotionType factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PromotionType::class, function (Faker $faker) {
    return [
    ];
});