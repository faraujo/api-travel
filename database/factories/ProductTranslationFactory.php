<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un ProductTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\ProductTranslation::class, function (Faker $faker) {
    return [
        'language_id' => 1,
        'name' => $faker->text,
        'description' => $faker->text,
        'features' => $faker->text,
        'recommendations' => $faker->text,
        'short_description' => $faker->text,
        'friendly_url' => $faker->text,
        'title_Seo' => $faker->text,
        'description_seo' => $faker->text,
        'keywords_seo' => $faker->text,
        'legal' => $faker->text,
    ];
});
