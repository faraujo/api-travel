<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un ServiceTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\ServiceTranslation::class, function (Faker $faker) {
    return [
        'language_id' => 1,
        'service_id' => 1,
        'name' => $faker->text,
        'description' => $faker->text,
    ];
});