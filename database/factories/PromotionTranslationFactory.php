<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Promotion factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PromotionTranslation::class, function (Faker $faker) {
    return [
        'gift' => $faker->text,
        'name' => $faker->text,
        'description' => $faker->text,
        'short_description' => $faker->text,
        'friendly_url' => $faker->url,
        'title_seo' => $faker->text,
        'description_seo' => $faker->text,
        'keywords_seo' => $faker->text,
        'observations' => $faker->text,
        'legal' => $faker->text,
    ];
});