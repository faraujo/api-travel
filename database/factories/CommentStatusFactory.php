<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un CommentStatus factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\CommentStatus::class, function (Faker $faker) {
    return [];
});