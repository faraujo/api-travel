<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un CommentStatusTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\CommentStatusTranslation::class, function (Faker $faker) {
    return [];
});