<?php
$factory->define(App\Models\tenant\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
    ];
});
