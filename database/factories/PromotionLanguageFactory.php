<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Promotion Language factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PromotionLanguage::class, function (Faker $faker) {
    return [
    ];
});