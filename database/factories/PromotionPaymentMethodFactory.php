<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Promotion Payment Method factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PromotionPaymentMethod::class, function (Faker $faker) {
    return [
    ];
});