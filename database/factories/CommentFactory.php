<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Comment factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Comment::class, function (Faker $faker) {
    return [
        'language_id' => 1,
        'text' => $faker->text,
        'rating' => 3,
    ];
});