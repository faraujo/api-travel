<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Promotion factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Promotion::class, function (Faker $faker) {
    return [
        'code' => $faker->text,
        //'order' => $faker->numberBetween(0,4),
        'visibility' => 1,
        'accumulate' => $faker->numberBetween(0,1),
        'min_quantity' => $faker->numberBetween(0,100),
        'max_quantity' => $faker->numberBetween(0,100),
        'promotion_amount' => '1500.25555',
        'promotion_type_id' => $faker->numberBetween(1,3),
        //'apply_product_filter' => $faker->numberBetween(0,1),
        //'apply_product_type_filter' => $faker->numberBetween(0,1),
        //'apply_service_filter' => $faker->numberBetween(0,1),
        //'apply_subchannel_filter' => $faker->numberBetween(0,1),
        //'apply_country_filter' => $faker->numberBetween(0,1),
        //'apply_state_filter' => $faker->numberBetween(0,1),
        //'apply_client_type_filter' => $faker->numberBetween(0,1),
        //'apply_currency_filter' => $faker->numberBetween(0,1),
        //'apply_day_filter' => $faker->numberBetween(0,1),
        //'apply_language_filter' => $faker->numberBetween(0,1),
        //'apply_payment_method_filter' => $faker->numberBetween(0,1),
        //'apply_pickup_filter' => $faker->numberBetween(0,1),
        'anticipation_start' => $faker->randomNumber(),
        'anticipation_end' => $faker->numberBetween(0,1),
        'coupon_code' => $faker->text,
        'coupon_type' => $faker->numberBetween(0,1),
        'coupon_sheet_start' => 1,
        'coupon_sheet_end' => 100,
        'coupon_total_uses' => $faker->numberBetween(0,100),
    ];
});
