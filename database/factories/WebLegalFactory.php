<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Legal factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebLegal::class, function (Faker $faker) {
    return [
    ];
});