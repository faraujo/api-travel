<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Page factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebPage::class, function (Faker $faker) {
    return [
    ];
});