<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un ContractTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\ContractTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
    ];
});