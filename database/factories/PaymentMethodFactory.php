<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Payment Method factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\PaymentMethod::class, function (Faker $faker) {
    return [
    ];
});