<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Availability factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Availability::class, function (Faker $faker) {
    return [
        'quota' => 200,
    ];
});