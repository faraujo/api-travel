<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un FileTypeTranslation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\FileTypeTranslation::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
        'description' => $faker->text,
    ];
});