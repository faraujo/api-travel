<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un ProductService factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\ProductService::class, function (Faker $faker) {
    return [
    ];
});