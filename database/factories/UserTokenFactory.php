<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Subchannel factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\UserToken::class, function (Faker $faker) {
    return [
        'token' => $faker->randomNumber(),
    ];
});