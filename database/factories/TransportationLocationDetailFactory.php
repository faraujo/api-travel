<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Availability factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\TransportationLocationDetail::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
        'code' => $faker->text,
        'date' => '2099-12-27',
        'session' => '10:00',
    ];
});