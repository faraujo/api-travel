<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Legal Translation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebLegalTranslation::class, function (Faker $faker) {
    return [
    ];
});