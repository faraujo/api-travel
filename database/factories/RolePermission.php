<?php
$factory->define(App\Models\tenant\RolePermission::class, function (Faker\Generator $faker) {
    return [
        'role_id' => $faker->numberBetween(1,3),
        'permission_id' => $faker->numberBetween(1,3),
    ];
});
