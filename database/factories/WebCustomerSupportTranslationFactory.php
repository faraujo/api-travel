<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Web Customer Support Translation factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\WebCustomerSupportTranslation::class, function (Faker $faker) {
    return [
    ];
});