<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Tag factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Tag::class, function (Faker $faker) {
    return [
        'order' => 2,
    ];
});