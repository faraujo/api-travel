<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Currency factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Currency::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
        'simbol' => '$',
        'iso_number' => 555,
        'iso_code' => $faker->text,
        'presentation_simbol' => 'US$',
        'position' => 'I',
        'decimal_separator' => ',',
        'thousand_separator' => '.',
    ];
});