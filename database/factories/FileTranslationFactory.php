<?php

use Faker\Generator as Faker;


$factory->define(/**
* Función que crea un File factory para los test
* @param Faker $faker
* @return array
*/
    App\Models\tenant\FileTranslation::class, function (Faker $faker) {
    return [
        'file_id' => 1,
    ];
});