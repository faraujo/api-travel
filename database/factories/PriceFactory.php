<?php

use Faker\Generator as Faker;


$factory->define(/**
 * Función que crea un Product factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\Price::class, function (Faker $faker) {
    return [

        'promotions' => 0,
        'date_start' => '2099-12-27',
        'date_end' => '2099-12-31',
    ];
});
