<?php

use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un DocumentType factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\DocumentType::class, function (Faker $faker) {
    return [
    ];
});