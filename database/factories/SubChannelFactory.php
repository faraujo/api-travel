<?php
use Faker\Generator as Faker;

$factory->define(/**
 * Función que crea un Subchannel factory para los test
 * @param Faker $faker
 * @return array
 */
    App\Models\tenant\SubChannel::class, function (Faker $faker) {
    return [
        'authorization_token' => $faker->text(16),
    ];
});