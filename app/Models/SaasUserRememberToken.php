<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasUserRememberToken extends Model
{
    use SoftDeletes;
    
    protected $table = 'saas_user_remember_token';

    protected $fillable = ['user_id','ip','token'];


}