<?php
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class SaasUserToken extends Model
{
    use SoftDeletes;
    
    protected $table = 'saas_user_token';

    protected $fillable = ['user_id', 'token', 'expired_at'];


}