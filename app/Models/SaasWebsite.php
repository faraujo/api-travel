<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasWebsite extends Model
{
    use SoftDeletes;

    protected $table = 'saas_website';

    protected $fillable = [
        'user_id',
        'website_id',
        'status_id',
        'name',
        'domain',
        'nif',
        'default_language_id',
        'default_currency_id',
    ];

    /**
     * Define relación de la tabla mo_website con la tabla mo_language
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function saasWebsiteLanguage()
    {
        return $this->hasMany('App\Models\SaasWebsiteLanguage', 'website_id');
    }

        /**
     * Define relación de la tabla mo_website con la tabla mo_currency
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function saasWebsiteCurrency()
    {
        return $this->hasMany('App\Models\SaasWebsiteCurrency', 'website_id');
    }

}
