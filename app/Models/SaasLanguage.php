<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasLanguage extends Model
{
    use SoftDeletes;

    protected $table = 'saas_language';

    protected $fillable = [        
    ];

}
