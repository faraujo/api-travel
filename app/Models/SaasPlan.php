<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasPlan extends Model
{
    use SoftDeletes;

    protected $table = 'saas_plan';

    protected $fillable = [
    ];

}
