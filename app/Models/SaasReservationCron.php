<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Traits\UsesSystemConnection;

class SaasReservationCron extends Model
{
    //
    use SoftDeletes, UsesSystemConnection;

    protected $table = 'saas_reservation_cron';

    protected $fillable = [   
        'date_start',
        'date_end',
        'log'         
    ];

}
