<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasUserValidateEmailToken extends Model
{
    use SoftDeletes;

    protected $table = 'saas_user_validate_email_token';

    protected $fillable = ['user_id', 'token'];

}