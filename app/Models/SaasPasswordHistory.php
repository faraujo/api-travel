<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasPasswordHistory extends Model
{
    use SoftDeletes;
    
    protected $table = 'saas_password_history';

    protected $fillable = ['user_id', 'password'];


}