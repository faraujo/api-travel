<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryFile extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_category_file';
    protected $fillable = [
        'category_id',
        'file_id',
    ];

}