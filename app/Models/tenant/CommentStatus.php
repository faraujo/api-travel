<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentStatus extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $fillable = [];

    protected $table = 'mo_comment_status';


    /**
     * Define relación de la tabla mo_comment_status con la tabla mo_comment_status_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentStatusTranslation()
    {
        return $this->hasMany('App\Models\tenant\CommentStatusTranslation', 'comment_status_id');
    }

}