<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Type extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_product_type';


    /**
     * Degine relación de la tabla mo_product_type con la tabla mo_product_type_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function typeTranslation()
    {
        return $this->hasMany('\App\Models\tenant\TypeTranslation', 'type_id');
    }

    /**
     * Define relación de la tabla mo_type con la tabla mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product()
    {
        return $this->hasMany('\App\Models\tenant\Product', 'type_id');
    }

}