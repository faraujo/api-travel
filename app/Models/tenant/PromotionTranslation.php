<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_translation';

    public $fillable = [

        'language_id',
        'promotion_id',
        'gift',
        'name',
        'description',
        'short_description',
        'friendly_url',
        'external_url',
        'title_seo',
        'description_seo',
        'keywords_seo',
        'observations',
        'legal',
        'notes'
    ];


}