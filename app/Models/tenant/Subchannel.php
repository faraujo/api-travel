<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubChannel extends Model
{
    use softDeletes, UsesTenantConnection;
    
    protected $table = 'mo_subchannel';

    protected $fillable = ['channel_id', 'contract_model_id','prefix', 'reservation_code','authorization_token','view_blocked', 'sendconfirmedemail',
    'api_code'];

    /**
     * Define relación de la tabla mo_subchannel con la tabla mo_subchannel_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subchannelTranslation()
    {
        return $this->hasMany('App\Models\tenant\SubChannelTranslation', 'subchannel_id');
    }

    /**
     * Define relación de la tabla mo_subchannel con la tabla mo_module_subchannel
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function moduleSubchannel()
    {
        return $this->hasMany('App\Models\tenant\ModuleSubchannel', 'subchannel_id');
    }

    /**
     * Define relación de la tabla mo_subchannel con la tabla mo_role_permission
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rolePermissionSubchannel()
    {
        return $this->hasMany('App\Models\tenant\RolePermission', 'subchannel_id');
    }

}