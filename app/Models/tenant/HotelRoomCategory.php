<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 21/09/2018
 * Time: 12:27
 */

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelRoomCategory extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_hotel_room_category";

}