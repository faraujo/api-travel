<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CountryTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_country_translation";

    public $fillable = [
        'id',
        'country_id',
        'language_id',
        'name',
    ];



}