<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileExtension extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_file_extension';

    protected $fillable = ['name', 'extensions','max_size'];


}