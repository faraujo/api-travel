<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_campaign';

    protected $fillable = [
        'active',
        'date_start',
        'date_end',
        'apply_filter_subchannel'
    ];


    /**
     * Define relación de la tabla mo_campaign con la tabla mo_campaign_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function campaignTranslation()
    {
        return $this->hasMany('App\Models\tenant\CampaignTranslation', 'campaign_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_subchannel, a través de subchannel_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function campaignChannel()
    {
        return $this->hasMany('App\Models\tenant\CampaignSubchannel', 'campaign_id');
    }

}