<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionService extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_service';

    public $fillable = [
        'id',
        'promotion_id',
        'service_id',
        'min_quantity',
        'max_quantity',
        'series_one',
        'series_two',
        'promotion_amount',
        'currency_id',
        'promotion_type_id',
        'apply_client_type_filter',
    ];

}