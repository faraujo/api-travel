<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IvaTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_iva_translation';

    protected $fillable = ['language_id', 'iva_id', 'name'];

}