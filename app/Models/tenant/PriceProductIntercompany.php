<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceProductIntercompany extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_price_product_intercompany';

    protected $fillable = [
        'price_product_id',
        'currency_id',
        'company_id',
        'amount',
        'percent',
    ];

}