<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


/**
 * App\Contact
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $photo
 * @property int|null $document_type_id
 * @property string|null $number_document
 * @property int|null $sex_id
 * @property string|null $birthdate
 * @property string|null $email
 * @property int $faults_login
 * @property int $bloqued_login
 * @property int|null $language_id
 * @property string|null $telephone1
 * @property string|null $telephone2
 * @property string|null $telephone3
 * @property string|null $address
 * @property string|null $postal_code
 * @property string|null $province
 * @property string|null $state
 * @property int|null $country_id
 * @property int|null $nationality_id
 * @property string|null $business
 * @property string|null $telephone_business
 * @property int $worker
 * @property int $worker_type
 * @property int $cms
 * @property string|null $observations
 * @property string|null $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Contact onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereBloquedLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereBusiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereCms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereDocumentTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereFaultsLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereNationalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereNumberDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereObservations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereSexId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereTelephone1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereTelephone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereTelephone3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereTelephoneBusiness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereWorker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contact whereWorkerType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Contact withoutTrashed()
 * @mixin \Eloquent
 */
class Contact extends Model implements AuthenticatableContract
{

    use Authenticatable, SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'name',
        'surname',
        'document_type_id',
        'number_document',
        'sex_id',
        'birthdate',
        'email',
        'email2',
        'language_id',
        'telephone1',
        'telephone2',
        'telephone3',
        'address',
        'postal_code',
        'city',
        'state_id',
        'country_id',
        'business',
        'telephone_business',
        'observations',
        'bloqued_login',
        'worker',
        'worker_type_id',
        'bloqued_to',
        'image_id',
        'faults_login'
    ];

    protected $table = 'mo_contact';
    
}
