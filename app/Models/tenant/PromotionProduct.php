<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionProduct extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_product';

    public $fillable = [
        'id',
        'promotion_id',
        'product_id',
        'location_id',
        'min_quantity',
        'max_quantity',
        'quantity',
        'promotion_amount',
        'promotion_type_id',
        'series_one',
        'series_two',
        'currency_id',
        'apply_client_type_filter',
    ];

}