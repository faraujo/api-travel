<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IdentificationTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_identification_translation';

    protected $fillable = ['identification_id','language_id','name','description'];

}