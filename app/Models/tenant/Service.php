<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{

    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_service';

    protected $fillable = [
    ];


    /**
     * Define relación de la tabla mo_service con la tabla mo_service_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function serviceTranslation()
    {
        return $this->hasMany('App\Models\tenant\ServiceTranslation', 'service_id');
    }

    /**
     * Define relación de la tabla mo_service con la tabla mo_product_service
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productService()
    {
        return $this->hasMany('\App\Models\tenant\ProductService', 'service_id');
    }


}