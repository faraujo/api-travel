<?php


namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserIdentification extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_user_identification';

    protected $fillable = ['user_id','identification_id', 'identification_number'];

}