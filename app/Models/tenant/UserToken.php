<?php
/**
 * Created by PhpStorm.
 * User: VIAVOX
 * Date: 08/03/2018
 * Time: 9:08
 */

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserToken extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_user_token';

    protected $fillable = ['user_id', 'token', 'remember_token', 'expired_at'];


}