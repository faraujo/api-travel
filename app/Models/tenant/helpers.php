<?php

use Illuminate\Support\Carbon;
use App\Models\tenant\Currency;
use App\Models\tenant\CurrencyExchange;
use App\Models\tenant\Settings;
use App\Models\tenant\File;
use App\Models\tenant\Subchannel;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;

use App\Models\tenant\Device;
//use App\EncryptCustom;
use App\Models\tenant\IdentificationTranslation;
use App\Models\tenant\IdentificationUser;
//use App\Insurance;
use App\Models\tenant\Language;
use App\Models\tenant\Promotion;
use App\Models\tenant\PromotionAccumulate;
use App\Models\tenant\PromotionClientType;
use App\Models\tenant\PromotionCountry;
use App\Models\tenant\PromotionCurrency;
use App\Models\tenant\PromotionDevice;
use App\Models\tenant\PromotionEnjoy;
use App\Models\tenant\PromotionEnjoyDay;
use App\Models\tenant\PromotionLanguage;
use App\Models\tenant\PromotionPaymentMethod;
use App\Models\tenant\PromotionPickup;
use App\Models\tenant\PromotionProduct;
use App\Models\tenant\PromotionProductClientType;
use App\Models\tenant\PromotionProductType;
use App\Models\tenant\PromotionProductTypeClientType;
use App\Models\tenant\PromotionService;
use App\Models\tenant\PromotionServiceClientType;
use App\Models\tenant\PromotionState;
use App\Models\tenant\PromotionTranslation;
use App\Models\tenant\PromotionTypeTranslation;
use App\Models\tenant\ReservationDetail;
use App\Models\tenant\ReservationDetailActivity;
use App\Models\tenant\ReservationDetailBenefitCard;
use App\Models\tenant\ReservationDetailClientType;
use App\Models\tenant\ReservationDetailDayMore;
use App\Models\tenant\ReservationDetailEvent;
use App\Models\tenant\ReservationDetailFood;
use App\Models\tenant\ReservationDetailHotel;
use App\Models\tenant\ReservationDetailPackage;
use App\Models\tenant\ReservationDetailPark;
use App\Models\tenant\ReservationDetailPhoto;
use App\Models\tenant\ReservationDetailRestaurant;
use App\Models\tenant\ReservationDetailTour;
use App\Models\tenant\ReservationDetailTransportation;
use App\Models\tenant\ReservationInsurance;
use App\Models\tenant\ReservationPromotion;
use App\Models\tenant\UserToken;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Str;

/**
 * función para convertir formato de archivo en bytes a formato mayor posible, siempre que sea al menos 1.
 * Recibe parámetro $from en bytes y devuelve tamaño de archivo convertido y concatenada la abreviatura de su extensión
 *
 * @param $from
 * @return string
 */
function convertExtension($from){

    switch($from){
        case ($from >= 1024 && $from < 1048576):
            return ($from/1024).' KB';
        case ($from >= 1048576 && $from < 1073741824):
            return ($from/pow(1024,2)).' MB';
        case ($from >= 1073741824 && $from < 1099511627776):
            return ($from/pow(1024,3)).' GB';
        default:
            return $from.' B';
    }

}

/**
     * Función para convertir moneda
     *
     * Convierte un valor que viene de base de datos en una moneda determinada a la moneda que especifique el usuario o a la moneda por defecto
     *
     * @param $valor
     * @param $moneda_actual
     * @param $moneda_convertir
     * @return string
     */
function conversor($valor, $moneda_actual, $moneda_convertir, $settings_decimales = null)
{
    $fecha_hora = Carbon::now()->format('Y-m-d H:i:s');

    $settings_decimales = $settings_decimales ? $settings_decimales : Settings::where('name', 'numero_decimales')->first()->value;

    $cambio_actual = CurrencyExchange::where('currency_id', $moneda_actual)
        ->where('date_time_start', '<=', $fecha_hora)
        ->orderBy('date_time_start', 'desc')
        ->first()
        ->exchange;

    $cambio_moneda_convertir = CurrencyExchange::where('currency_id', $moneda_convertir)
        ->where('date_time_start', '<=', $fecha_hora)
        ->orderBy('date_time_start', 'desc')
        ->first()
        ->exchange;

    if ($cambio_actual != 0) {
        $valor_convertido = number_format($valor / $cambio_actual * $cambio_moneda_convertir, $settings_decimales, '.', '');
    } else {
        $valor_convertido = $valor;
    }

    return $valor_convertido;
}

function conversorCustomSearch($valor, $conversion_actual, $conversion_final, $settings_decimales){
    if($conversion_actual != 0) {
        $valor_convertido = number_format($valor / $conversion_actual * $conversion_final, $settings_decimales, '.', '');
    } else {
        $valor_convertido = $valor;
    }

    return $valor_convertido;
}

function conversorCustomReservation($valor, $moneda_actual, $moneda_convertir, $factor_conversion) {
    $fecha_hora = Carbon::now()->format('Y-m-d H:i:s');

    $settings_decimales = Settings::where('name', 'numero_decimales')->first()->value;

    $cambio_actual = CurrencyExchange::where('currency_id', $moneda_actual)
        ->where('id', $factor_conversion)
        ->first()
        ->exchange;

    $cambio_moneda_convertir = CurrencyExchange::where('currency_id', $moneda_convertir)
        ->where('date_time_start', '<=', $fecha_hora)
        ->orderBy('date_time_start', 'desc')
        ->first()
        ->exchange;

    if ($cambio_actual != 0) {
        $valor_convertido = number_format($valor / $cambio_actual * $cambio_moneda_convertir, $settings_decimales, '.', '');

    } else {
        $valor_convertido = $valor;
    }

    return $valor_convertido;
}

function reportService(Exception $exception, String $service = null)
    {

        $envio_correo = Settings::where('name', '=', 'send_email_error')->first()->value;

        $environment = Settings::where('name','=','nombre_cliente')->first()->value;
        //if ($exception instanceof \ErrorException) { Condicional si es un tipo de excepcion concreta
            if ($envio_correo && $service !== 'Emails') {
               
                $mensaje = 'Microservicio de '.$service.': File' . $exception->getFile() . ' , Error: ' . $exception->getMessage() . ', Line: ' . $exception->getLine() . '';
                
                $controller = app(\App\Http\Controllers\tenant\EmailController::class);

                $peticion = new Request();

                $peticion->setMethod('POST');

                $peticion->request->add(['title' => 'Error']);
              
                $peticion->request->add(['body' => $mensaje]);

                $peticion->request->add(['template' => 'error']);

                $peticion->request->add(['emails' => [
                    '0' => [
                        'email' => Settings::where('name', '=', 'email_error_sistema')->first()->value,
                        'subject_email' => $environment . ' - Error',
                    ]]]);

                
                $controller->send($peticion);

            }

            $envio_slack = Settings::where('name', '=', 'send_slack_error')->first()->value;
            if ($envio_slack) {

                $mensaje = 'File' . $exception->getFile() . ' , Error: ' . $exception->getMessage() . ', Line: ' . $exception->getLine() . '';

                $controller = app(\App\Http\Controllers\tenant\SlackController::class);

                $peticion = new Request();

                $peticion->setMethod('POST');

                $peticion->request->add(['from' => 'Microservicio de '.$service]);

                $peticion->request->add(['content' => 'Excepción o error de sistema']);

                $peticion->request->add(['type' => 'error']);

                $peticion->request->add(['attachment' => [
                    'title' => $environment . ' - Microservicio de '.$service.':',
                    'content' => $mensaje
                ]]);

                $peticion->request->add(['space' => 'error_sistema']);

                $peticion->request->add(['channel' => [
                    '0' => Settings::where('name', '=', 'slack_channel_error_sistema')->first()->value
                ]]);
                
                $controller->send($peticion);
            }
        //}
    }

    //Helpers para Reservas (rutas promociones)

    function promotionsApplicable ($datos_promotions, $request, $idioma, $device){

        $sql_reservation = DB::connection('tenant')->table('mo_reservation')
            ->whereNull('mo_reservation.deleted_at')
            ->where('mo_reservation.id', $request->get('id'))
            ->where('mo_reservation_detail.operation_status_id', '!=', 4)
            ->where('mo_reservation_detail.operation_status_id', '!=', 5)
            ->where('mo_reservation_detail.operation_status_id', '!=', 6)
            ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
            ->whereNull('mo_reservation_detail.deleted_at');
    
        $array_promotions = array();
    
        foreach ($datos_promotions as $promotion) {
    
            $aplica = 1;
            //Promociones por cupon
    
            if ($promotion->coupon_type == 1) {
                if ($request->get('coupon_code') != '') {
                    //Si la promocion tiene total de usos
                    if (!is_null($promotion->coupon_total_uses) && $aplica == 1) {
                        $reservation_promotion = DB::connection('tenant')->table('mo_reservation_promotion')->where('coupon_code', $request->get('coupon_code'))
                            ->whereNull('mo_reservation_promotion.deleted_at')->count();
                        if ($reservation_promotion >= $promotion->coupon_total_uses) {
                            $aplica = 0;
                        }
                    }
                } else {
                    $aplica = 0;
                }
    
            }
            //FIN promociones por cupon
            //Promociones acumulables con otras
    
    
            if ($promotion->accumulate == 1 && $aplica == 1) {
    
                $aplica_aux = 1;
                //Comprueba que la promociones preasignadas sean acumulables con la recorrida
                if (count($array_promotions) > 0) {
                    if (strpos($request->path(), '/promotion') === false || ($request->get('coupon_code') == '' && $request->get('benefit_card_id') == '')) {
                        $promotions_aplicate = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                            ->whereNull('mo_reservation_promotion.deleted_at')
                            ->get();
                        foreach ($promotions_aplicate as $promotion_aplicate) {
    
                            //Si en algún momento detecta alguna promoción que no aplica no prueba mas promociones
                            if ($aplica_aux == 1) {
                                $promo = Promotion::where('id', $promotion_aplicate->promotion_id)->first();
    
                                if ($promo) {
                                    if ($promo->accumulate == 1) {
                                        //Si la promoción recorrida en base de datos permite acumular
                                        //Comprueba que la promoción guardad permita acumular con la recorrida
                                        $accumulate = PromotionAccumulate::where('promotion_id', $promo->id)
                                            ->where('promotion_accumulate_id', $promotion->id)
                                            ->first();
    
                                        $accumulate_promotion = PromotionAccumulate::where('promotion_id', $promotion->id)
                                            ->where('promotion_accumulate_id', $promo->id)
                                            ->first();
    
                                        if (!$accumulate || !$accumulate_promotion) {
                                            //Si no permite acumular con la recorrida
                                            $aplica_aux = 0;
                                        }
                                    } else {
                                        $aplica_aux = 0;
                                    }
                                }
                            }
                        }
                    }
    
                    foreach ($array_promotions as $preasignadas) {
    
                        if ($aplica_aux == 1) {
                            if ($preasignadas->accumulate == 1) {
    
                                $accumulate = PromotionAccumulate::where('promotion_id', $promotion->id)
                                    ->where('promotion_accumulate_id', $preasignadas->id)
                                    ->first();
    
                                $preasignadas_accumulate = PromotionAccumulate::where('promotion_id', $preasignadas->id)
                                    ->where('promotion_accumulate_id', $promotion->id)
                                    ->first();
    
                                if (!$accumulate || !$preasignadas_accumulate) {
                                    //Si no permite acumular con la recorrida
                                    $aplica_aux = 0;
                                }
    
                            } else {
                                $aplica_aux = 0;
                            }
                        }
                    }
                } else {
                    $promotions_aplicate = DB::connection('tenant')->table('mo_reservation_promotion')
                        ->whereNull('mo_reservation_promotion.deleted_at')
                        ->where('reservation_id', $request->get('id'))->get();
    
                    foreach ($promotions_aplicate as $promotion_aplicate) {
    
                        //Si en algún momento detecta alguna promoción que no aplica no prueba mas promociones
                        if ($aplica_aux == 1) {
                            $promo = Promotion::where('id', $promotion_aplicate->promotion_id)->first();
    
                            if ($promo) {
                                if ($promo->accumulate == 1) {
                                    //Si la promoción recorrida en base de datos permite acumular
                                    //Comprueba que la promoción guardad permita acumular con la recorrida
                                    $accumulate = PromotionAccumulate::where('promotion_id', $promo->id)
                                        ->where('promotion_accumulate_id', $promotion->id)
                                        ->first();
    
                                    $accumulate_promotion = PromotionAccumulate::where('promotion_id', $promotion->id)
                                        ->where('promotion_accumulate_id', $promo->id)
                                        ->first();
    
                                    if (!$accumulate || !$accumulate_promotion) {
                                        //Si no permite acumular con la recorrida
                                        $aplica_aux = 0;
                                    }
                                } else {
                                    $aplica_aux = 0;
                                }
                            }
                        }
                    }
                }
    
                if ($aplica_aux == 0) {
                    //Si hay promociones guardadas en base de datos
                    $aplica = 0;
                }
    
            } else {
    
                if (strpos($request->path(), '/promotion') === false || ($request->get('coupon_code') == '' && $request->get('benefit_card_id') == '')) {
                    $promotion_aplicate = DB::connection('tenant')->table('mo_reservation_promotion')
                        ->whereNull('mo_reservation_promotion.deleted_at')
                        ->where('reservation_id', $request->get('id'))->first();
    
                    if ($promotion_aplicate) {
                        $aplica = 0;
                    }
    
                }
    
                if (count($array_promotions) > 0) {
                    $aplica = 0;
                }
            }
            //FIN de promociones acumulables con otras
            if(!is_null($promotion->anticipation_start) && !is_null($promotion->anticipation_end)) {
                $sql_reservation_clone = clone $sql_reservation;
    
                $sql_reservation_clone
                    ->select(
                        'mo_reservation_detail.id',
                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                    )
                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                    ->leftJoin('mo_reservation_detail_activity', function ($join) {
                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                        $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_event', function ($join) {
                        $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_event.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_package', function ($join) {
                        $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_package.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_park', function ($join) {
                        $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_park.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                        $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_tour', function ($join) {
                        $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_tour.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                        $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_transportation.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_photo', function ($join) {
                        $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_photo.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_food', function ($join) {
                        $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_food.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                        $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_day_more.deleted_at');
                    })
                    ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                        $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_hotel.deleted_at');
                    });
    
    
                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                    ->mergeBindings($sql_reservation_clone);
                $datos_reservation_anticipation = $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                    ->where('date_aux','<=', $fecha_fin)->first();
    
    
                if(!$datos_reservation_anticipation){
                    $aplica = 0;
                }
            }
    
            //Promociones por tipos de cliente
            if ($promotion->apply_client_type_filter == 1 && $aplica == 1) {
                $client_types = PromotionClientType::where('promotion_id', $promotion->id)->get();
                $client_types_array = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
                if (count($client_types) <= 0) {
                    $aplica = 0;
                } else {
                    $aplica_aux = 0;
    
                    $sql_reservation_clone = clone $sql_reservation;
    
                    $sql_reservation_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_client_type.deleted_at');
    
                    $sql_reservation_clone
                        ->whereIn('mo_reservation_detail_client_type.client_type_id',$client_types_array);
    
                    if ($promotion->min_quantity != '' && !is_null($promotion->min_quantity)) {
                        $sql_reservation_clone
                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $promotion->min_quantity));
                    }
    
                    if ($promotion->max_quantity != '' && !is_null($promotion->max_quantity)) {
                        $sql_reservation_clone
                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $promotion->max_quantity));
                    }
    
                    $datos_reservation = $sql_reservation_clone->first();
    
                    if (!$datos_reservation) {
                        $aplica = 0;
                    }
    
                }
            } else {
                if($aplica == 1) {
                    //Promociones por cantidad mínima de pax
                    if ($promotion->min_quantity != '' && !is_null($promotion->min_quantity) && $aplica == 1) {
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $datos_reservation = $sql_reservation_clone
                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')->select(
                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as count_client_types')
                            )
                            ->first();
    
                        if ($datos_reservation->count_client_types < $promotion->min_quantity) {
                            $aplica = 0;
                        }
                    }
                    //FIN promociones por cantidad mínima de pax
    
                    //Promociones por cantidad máxima de pax
                    if (($promotion->max_quantity != '' && !is_null($promotion->max_quantity)) && $aplica == 1) {
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $datos_reservation = $sql_reservation_clone
                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                            ->select(
                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as count_client_types')
                            )->first();
    
                        if ($datos_reservation->count_client_types > $promotion->max_quantity) {
                            $aplica = 0;
                        }
                    }
                    //FIN promociones por cantidad máxima de pax
                }
            }
            //FIN promociones por tipos de cliente
    
            if($promotion->apply_product_filter == 0 && $promotion->apply_product_type_filter == 0 && $promotion->apply_service_filter == 0){
                //Promociones por dias mínimos de anticipación
    
                if (($promotion->anticipation_start != '' || !is_null($promotion->anticipation_start)) && ($promotion->anticipation_end != '' || !is_null($promotion->anticipation_end)) && $aplica == 1) {
    
                    $sql_reservation_clone = clone $sql_reservation;
    
                    $sql_reservation_clone->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')->groupBy('mo_reservation_detail.id')
                        ->select(
                            'mo_reservation_detail.id',
                            'mo_reservation_detail.product_id',
                            'mo_reservation_detail.service_id',
                            'mo_product.type_id',
                            DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                        )
                        ->leftJoin('mo_reservation_detail_activity', function ($join) {
                            $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_activity.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                            $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_event', function ($join) {
                            $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_event.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_package', function ($join) {
                            $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_package.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_park', function ($join) {
                            $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_park.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                            $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_tour', function ($join) {
                            $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_tour.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                            $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_transportation.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_photo', function ($join) {
                            $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_photo.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_food', function ($join) {
                            $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_food.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                            $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_day_more.deleted_at');
                        })
                        ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                            $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_hotel.deleted_at');
                        });
    
                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                    $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                        ->mergeBindings($sql_reservation_clone);
    
                    $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                        ->where('date_aux','<=', $fecha_fin);
    
                    $datos_reservation = $sql_reservation_clone->first();
    
    
                    if(!$datos_reservation) {
                         $aplica = 0;
                    }
                }
                //FIN promociones por dias mínimos de anticipación
    
            }
            //FIN promociones por dias máximos de anticipación
    
            //Promociones por moneda
            if ($promotion->apply_currency_filter == 1 && $aplica == 1) {
    
                $promotion_currency = PromotionCurrency::where('promotion_id', $promotion->id)->get();
    
    
                if (count($promotion_currency) <= 0) {
                    $aplica = 0;
                } else {
                    $aplica_aux = 0;
                    foreach ($promotion_currency as $currency) {
    
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $datos_reservation_currency = $sql_reservation_clone->where('mo_reservation_detail.currency_id', $currency->currency_id)->first();
    
                        if ($datos_reservation_currency) {
                            $aplica_aux = 1;
                        }
                    }
    
                    if ($aplica_aux == 0) {
                        $aplica = 0;
                    }
                }
            }
            //FIN de promociones por moneda
    
            //Promociones por pais
            if ($promotion->apply_country_filter == 1 && $aplica == 1) {
    
                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get();
    
    
                if (count($promotion_country) <= 0) {
                    $aplica = 0;
                } else {
                    $aplica_aux = 0;
    
                    foreach ($promotion_country as $country) {
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $datos_reservation_country = $sql_reservation_clone
                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                            ->where('mo_reservation_detail_client_type.client_country_id', $country->country_id)->first();
    
                        if ($datos_reservation_country) {
                            $aplica_aux = 1;
                        }
                    }
    
                    if ($aplica_aux == 0) {
                        $aplica = 0;
                    }
                }
            }
            //FIN de promociones por pais
    
            //Promociones por estado
            if ($promotion->apply_state_filter == 1 && $aplica == 1) {
                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get();
    
                if (count($promotion_state) <= 0) {
                    $aplica = 0;
                } else {
    
                    $aplica_aux = 0;
    
                    foreach ($promotion_state as $state) {
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $datos_reservation_state = $sql_reservation_clone
                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                            ->where('mo_reservation_detail_client_type.client_state_id', $state->state_id)->first();
    
                        if ($datos_reservation_state) {
                            $aplica_aux = 1;
                        }
                    }
    
                    if ($aplica_aux == 0) {
                        $aplica = 0;
                    }
    
                }
            }
            //FIN promociones por estado
    
            //Promociones por idioma
            if ($promotion->apply_language_filter == 1 && $aplica == 1) {
    
                if ($request->get('lang') != '') {
    
                    $promotion_language = PromotionLanguage::where('promotion_id', $promotion->id)->where('language_id', $idioma->id)->first();
    
                    if (!$promotion_language) {
                        $aplica = 0;
                    }
    
                } else {
                    $aplica = 0;
                }
    
            }
            //FIN promociones por idioma
    
            //Promociones por dispositivo
            if ($promotion->apply_device_filter == 1 && $aplica == 1) {
    
                if ($request->get('device') != '') {
    
                    $promotion_device = PromotionDevice::where('promotion_id', $promotion->id)->where('device_id', $device->id)->first();
    
                    if (!$promotion_device) {
                        $aplica = 0;
                    }
                } else {
                    $aplica = 0;
                }
            }
            //FIN promociones por dispositivo
    
            //Promociones por método de pago
            if ($promotion->apply_payment_method_filter == 1 && $aplica == 1) {
                if ($request->get('payment_method_id') != '') {
                    $promotion_payment_method = PromotionPaymentMethod::where('promotion_id', $promotion->id)
                        ->where('payment_method_id', $request->get('payment_method_id'))
                        ->first();
    
                    if (!$promotion_payment_method) {
                        $aplica = 0;
                    }
                } else {
                    $aplica = 0;
                }
            }
            //FIN promociones por método de pago
    
            //Promociones por lugar de pickup
            if ($promotion->apply_pickup_filter == 1 && $aplica == 1) {
                $promotion_pickup = PromotionPickup::where('promotion_id', $promotion->id)->get();
    
                if (count($promotion_pickup) <= 0) {
                    $aplica = 0;
                } else {
                    $aplica_aux = 0;
    
                    foreach ($promotion_pickup as $pickup) {
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $datos_reservation_pickup = $sql_reservation_clone
                            ->where('mo_reservation_detail_transportation.pickup_id', $pickup->pickup_id)
                            ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_transportation.deleted_at');
                            })
                            ->first();
    
                        if ($datos_reservation_pickup) {
                            $aplica_aux = 1;
                        }
                    }
    
                    if ($aplica_aux == 0) {
                        $aplica = 0;
                    }
                }
            }
            //FIN promociones por lugar de pickup
    
            //Promociones por fecha de disfrute
            if ($aplica == 1) {
                $sql_reservation_clone = clone $sql_reservation;
    
                $datos_reservation = $sql_reservation_clone->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')->groupBy('mo_reservation_detail.id')
                    ->select(
                        'mo_reservation_detail.id',
                        'mo_reservation_detail.product_id',
                        'mo_reservation_detail.service_id',
                        'mo_product.type_id'
                    )->get();
    
                $aplica_aux = 0;
    
                $promotion_enjoy_range = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                if(count($promotion_enjoy_range) > 0){
                    foreach ($datos_reservation as $detail) {
                        if (is_null($detail->service_id)) {
                            $fecha_disfrute = '';
                            if ($detail->type_id == 1) {
                                $detalle_reserva = ReservationDetailPackage::where('reservation_detail_id', $detail->id)->orderBy('date', 'asc')->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date . ' ' . $detalle_reserva->session);
                            }
    
                            if ($detail->type_id == 2) {
                                $detalle_reserva = ReservationDetailHotel::where('reservation_detail_id', $detail->id)->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date_start);
                            }
    
                            if ($detail->type_id == 3) {
                                $detalle_reserva = ReservationDetailActivity::where('reservation_detail_id', $detail->id)->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date . ' ' . $detalle_reserva->session);
                            }
    
                            if ($detail->type_id == 4) {
                                $detalle_reserva = ReservationDetailEvent::where('reservation_detail_id', $detail->id)->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date . ' ' . $detalle_reserva->session);
                            }
    
                            if ($detail->type_id == 5) {
                                $detalle_reserva = ReservationDetailTour::where('reservation_detail_id', $detail->id)->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date . ' ' . $detalle_reserva->session);
                            }
    
                            if ($detail->type_id == 6) {
                                $detalle_reserva = ReservationDetailPark::where('reservation_detail_id', $detail->id)->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date . ' ' . $detalle_reserva->session);
                            }
    
                            if ($detail->type_id == 7) {
                                $detalle_reserva = ReservationDetailRestaurant::where('reservation_detail_id', $detail->id)->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date . ' ' . $detalle_reserva->session);
                            }
    
                            if ($detail->type_id == 8) {
                                $detalle_reserva = ReservationDetailBenefitCard::where('reservation_detail_id', $detail->id)->first();
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date . ' ' . $detalle_reserva->session);
                            }
    
    
                        } else {
                            if ($detail->service_id == 1) {
                                $datos = ReservationDetailTransportation::where('reservation_detail_id', $detail->id)->first();
    
                                $fecha_disfrute = Carbon::parse($datos->date);
                            }
    
                            if ($detail->service_id == 2) {
                                $detalle_reserva = ReservationDetailFood::where('reservation_detail_id', $detail->id)->first();
    
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date);
                            }
    
                            if ($detail->service_id == 3) {
                                $detalle_reserva = ReservationDetailPhoto::where('reservation_detail_id', $detail->id)->first();
    
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date);
                            }
    
    
                            if ($detail->service_id == 4) {
                                $detalle_reserva = ReservationDetailDayMore::where('reservation_detail_id', $detail->id)->first();
    
                                $fecha_disfrute = Carbon::parse($detalle_reserva->date);
                            }
                        }
    
                        if (count($promotion_enjoy_range) > 0) {
                            $promotion_enjoy_range_comp = $promotion_enjoy_range->where('enjoy_date_start', '<=', $fecha_disfrute)->where('enjoy_date_end', '>=', $fecha_disfrute)->first();
    
                            if ($promotion_enjoy_range_comp) {
                                $week_day = $fecha_disfrute->format('N');
    
                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $promotion_enjoy_range_comp->id)->where('day_id', $week_day)->first();
    
                                if ($promotion_enjoy) {
                                    $aplica_aux = 1;
    
                                }
                            }
                        }
                    }
                } else {
                    $aplica_aux = 1;
                }
    
    
                if ($aplica_aux == 0) {
                    $aplica = 0;
                }
            }
    
            //FIN promociones por fecha de disfrute
            //Promociones por tarjeta beneficio
            if ($promotion->benefit_card == 1 && $aplica == 1) {
                if ($request->get('benefit_card_number') != '') {
                    $datos_identification = IdentificationUser::where('identification_id', $request->get('benefit_card_id'))
                        ->where('identification_number', $request->get('benefit_card_number'))
                        ->first();
    
                    if (!$datos_identification) {
                        $aplica = 0;
                    }
                } else {
                    $aplica = 0;
                }
            }
            //FIN de promociones por tarjeta beneficio
    
            $array_reservation_detail = array();
            //Promotciones por producto
            if ($promotion->apply_product_filter == 1 && $aplica == 1) {
    
                $promotion_product = PromotionProduct::where('promotion_id', $promotion->id)->get();
    
                if (count($promotion_product) <= 0) {
                    $aplica = 0;
                } else {
    
                    $aplica_aux = 1;
    
                    //Recorre los productos asociados a la promoción
    
                    $count = 0;
                    foreach ($promotion_product as $product) {
    
                        $promotion_product_count = PromotionProduct::where('promotion_id', $promotion->id)->where('product_id', $product->product_id)->count();
    
                        $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $sql_reservation_clone->where('mo_reservation_detail.product_id', $product->product_id)
                            ->whereNull('mo_reservation_detail.service_id')
                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                            ->groupBy('mo_reservation_detail.id');
    
                        if (!is_null($product->location_id)) {
                            $sql_reservation_clone->where('mo_reservation_detail.location_id', $product->location_id);
                        }
    
                        if ($promotion->apply_client_type_filter == 1){
    
                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                        }
    
                        $datos_count_products = $sql_reservation_clone->get();
    
                        if ($promotion_product_count > count($datos_count_products)) {
                            $aplica_aux = 0;
                        }
                        //Promociones por tipo de cliente de producto
                        if ($product->apply_client_type_filter == 1) {
    
                            $promotion_product_client_types = PromotionProductClientType::where('promotion_product_id', $product->id);
    
                            if(!is_null($product->series_one) && !is_null($product->series_two)){
                                $product_client_types = $promotion_product_client_types->groupBy('client_type_id')->pluck('client_type_id')->toArray();
    
                                $sql_reservation_clone = clone $sql_reservation;
    
                                $sql_reservation_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->where('mo_reservation_detail.product_id', $product->product_id)
                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $product_client_types)
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->series_one));
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                $datos_count_series = $sql_reservation_clone->first();
    
                                if(!$datos_count_series){
                                    $aplica_aux = 0;
                                }
    
                            }
                            $promotion_product_client_types = PromotionProductClientType::where('promotion_product_id', $product->id);
    
                            $datos_promotion_product_client_types = $promotion_product_client_types->groupBy('client_type_id')->get();
    
                            if (count($datos_promotion_product_client_types) <= 0) {
                                $aplica_aux = 0;
                            } else {
    
                                $product_client_types = $promotion_product_client_types->groupBy('client_type_id')->pluck('client_type_id')->toArray();
    
                                $sql_reservation_clone = clone $sql_reservation;
    
                                $sql_reservation_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $product_client_types)
                                    ->where('mo_reservation_detail.product_id', $product->product_id);
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if (!is_null($product->location_id)) {
                                    $sql_reservation_clone->where('mo_reservation_detail.location_id', $product->location_id);
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if (!is_null($product->min_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                                }
    
                                if (!is_null($product->max_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity));
                                }
    
                                $sql_reservation_clone->whereNotIn('mo_reservation_detail.id', $array_reservation_detail);
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                    )
                                    ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                        $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_event', function ($join) {
                                        $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_event.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_package', function ($join) {
                                        $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_package.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_park', function ($join) {
                                        $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_park.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                        $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                        $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_tour.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                        $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                        $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_photo.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_food', function ($join) {
                                        $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_food.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                        $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                        $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                    });
    
                                $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                if (count($promotion_enjoy) > 0) {
    
                                    $datos_reservation_client_type = null;
    
                                    foreach ($promotion_enjoy as $enjoy) {
    
                                        $sql_reservation_enjoy = clone $sql_reservation_clone;
    
                                        if (is_null($datos_reservation_client_type)) {
    
                                            $sql_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                ->mergeBindings($sql_reservation_enjoy)
                                                ->where(function ($query) use ($enjoy) {
                                                    $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                        ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                });
    
                                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                $sql_enjoy->where('date_aux','>=', $fecha_inicio)
                                                    ->where('date_aux','<=', $fecha_fin);
                                            }
    
                                            $products_enjoy = $sql_enjoy->get();
    
                                            foreach ($products_enjoy as $product_enjoy) {
                                                $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                if ($promotion_enjoy) {
                                                    $datos_reservation_client_type = $product_enjoy;
                                                }
                                            }
                                        }
                                    }
    
                                } else {
    
                                    $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_clone);
    
                                    if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                        $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                        $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                        $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                                            ->where('date_aux','<=', $fecha_fin);
                                    }
    
                                    $datos_reservation_client_type = $sql_reservation_clone->first();
                                }
    
                                if ($datos_reservation_client_type) {
                                    $array_reservation_detail[] = $datos_reservation_client_type->id;
                                } else {
                                    $aplica_aux = 0;
                                }
    
                            }
                            //FIN de promociones de tipos de cliente por producto
                        } else {
                            //Promociones por producto sin tipo de cliente
    
    
                            if(!is_null($product->series_one) && !is_null($product->series_two)){
    
                                $sql_reservation_series_clone = clone $sql_reservation;
    
                                 $sql_reservation_series_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->where('mo_reservation_detail.product_id', $product->product_id)
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->series_one));
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                $datos_count_series = $sql_reservation_series_clone->first();
    
                                if(!$datos_count_series){
                                    $aplica_aux = 0;
                                }
    
                            }
    
                            $sql_reservation_clone_clone = clone $sql_reservation;
    
                            $sql_reservation_clone_clone->select(
                                'mo_reservation_detail.id',
                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                        IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                        IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                        IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                        IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                        IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                        IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                        IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                        IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                        IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                        IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                        mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                )
                                ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                ->whereNotIn('mo_reservation_detail.id', $array_reservation_detail)
                                ->groupBy('mo_reservation_detail.id')
                                ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                    $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_activity.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                    $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_event', function ($join) {
                                    $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_event.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_package', function ($join) {
                                    $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_package.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_park', function ($join) {
                                    $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_park.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                    $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                    $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_tour.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                    $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                    $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_photo.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_food', function ($join) {
                                    $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_food.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                    $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                    $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                });
    
                            if ($promotion->apply_country_filter == 1) {
                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                            }
    
                            if($promotion->apply_state_filter == 1){
                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                            }
    
                            if ($promotion->apply_client_type_filter == 1){
                                $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                            }
    
                            //Promociones por cantidad de pax por producto
                            if (!is_null($product->min_quantity)) {
    
                                $sql_reservation_clone_clone
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                            }
    
                            if (!is_null($product->max_quantity)) {
                                $sql_reservation_clone_clone
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity));
                            }
    
    
                            $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                ->mergeBindings($sql_reservation_clone_clone);
    
                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                $sql_reservation_clone_clone->where('date_aux','>=', $fecha_inicio)
                                    ->where('date_aux','<=', $fecha_fin);
                            }
    
                            $datos_reservation_count = $sql_reservation_clone_clone->first();
    
                            if ($datos_reservation_count) {
                                $array_reservation_detail[] = $datos_reservation_count->id;
                            } else {
                                $aplica_aux = 0;
                            }
                            //FIN de promociones por cantidad de pax
                            //FIN de promociones de productos sin tipo de cliente
                        }
                    }
    
                    if ($aplica_aux == 0) {
                        $aplica = 0;
                    }
                }
            }
            //FIN promociones por producto
    
            //Promociones por tipo de producto
            if ($promotion->apply_product_type_filter == 1 && $aplica == 1) {
                $promotion_product_type = PromotionProductType::where('promotion_id', $promotion->id)->get();
                if (count($promotion_product_type) <= 0) {
                    $aplica = 0;
                } else {
    
                    $aplica_aux = 1;
                    foreach ($promotion_product_type as $product_type) {
                        $promotion_product_type_count = PromotionProductType::where('promotion_id', $promotion->id)->where('product_type_id', $product_type->product_type_id)->count();
    
                        $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $sql_reservation_clone
                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                            ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                            ->whereNull('mo_product.deleted_at')
                            ->whereNull('mo_reservation_detail.service_id')
                            ->where('mo_product.type_id', $product_type->product_type_id)
                            ->groupBy('mo_reservation_detail.id');
    
                        if ($promotion->apply_client_type_filter == 1){
                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                        }
    
                        $datos_count_product_types = $sql_reservation_clone->get();
    
                        if ($promotion_product_type_count > count($datos_count_product_types)) {
                            $aplica_aux = 0;
                        }
    
                        //Promociones por tipo de cliente del tipo de producto
                        if ($product_type->apply_client_type_filter == 1) {
                            $promotion_product_type_client_types = PromotionProductTypeClientType::where('promotion_product_type_id', $product_type->id);
    
                            if(!is_null($product_type->series_one) && !is_null($product_type->series_two)){
                                $product_client_types = $promotion_product_type_client_types->groupBy('client_type_id')->pluck('client_type_id')->toArray();
    
                                $sql_reservation_series_clone = clone $sql_reservation;
    
                                $sql_reservation_series_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                    ->whereNull('mo_product.deleted_at')
                                    ->where('mo_product.type_id', $product_type->product_type_id)
                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $product_client_types)
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->series_one));
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                $datos_count_series = $sql_reservation_series_clone->first();
    
                                if(!$datos_count_series){
                                    $aplica_aux = 0;
                                }
    
                            }
    
                            $datos_promotion_product_type_client_types = $promotion_product_type_client_types->groupBy('client_type_id')->get();
    
                            if (count($datos_promotion_product_type_client_types) <= 0) {
                                $aplica_aux = 0;
                            } else {
    
                                $product_client_types = $promotion_product_type_client_types->groupBy('client_type_id')->pluck('client_type_id')->toArray();
    
                                $sql_reservation_clone = clone $sql_reservation;
    
                                $sql_reservation_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                    ->whereNull('mo_product.deleted_at')
                                    ->where('mo_product.type_id', $product_type->product_type_id)
                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $product_client_types)
                                    ->groupBy('mo_reservation_detail.id');
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
    
                                if (!is_null($product_type->min_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                                }
    
                                if (!is_null($product_type->max_quantity) && is_null($product_type->min_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity));
                                }
    
                                $sql_reservation_clone->whereNotIn('mo_reservation_detail.id', $array_reservation_detail);
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                    )
                                    ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                        $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_event', function ($join) {
                                        $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_event.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_package', function ($join) {
                                        $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_package.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_park', function ($join) {
                                        $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_park.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                        $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                        $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_tour.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                        $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                        $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_photo.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_food', function ($join) {
                                        $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_food.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                        $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                        $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                    });
    
    
                                $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                if (count($promotion_enjoy) > 0) {
    
                                    $datos_reservation_client_type = null;
    
                                    foreach ($promotion_enjoy as $enjoy) {
    
                                        $sql_reservation_enjoy = clone $sql_reservation_clone;
    
                                        if (is_null($datos_reservation_client_type)) {
    
                                            $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                ->mergeBindings($sql_reservation_enjoy)
                                                ->where(function ($query) use ($enjoy) {
                                                    $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                        ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                });
    
                                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                    ->where('date_aux','<=', $fecha_fin);
                                            }
    
                                            $products_enjoy = $sql_reservation_enjoy->get();
    
                                            foreach ($products_enjoy as $product_enjoy) {
                                                $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                if ($promotion_enjoy) {
                                                    $datos_reservation_client_type = $product_enjoy;
                                                }
                                            }
                                        }
                                    }
    
                                } else {
    
                                    $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_clone);
    
                                    if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                        $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                        $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                        $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                                            ->where('date_aux','<=', $fecha_fin);
                                    }
    
                                    $datos_reservation_client_type = $sql_reservation_clone->first();
                                }
    
                                if ($datos_reservation_client_type) {
                                    $array_reservation_detail[] = $datos_reservation_client_type->id;
                                } else {
                                    $aplica_aux = 0;
                                }
                            }
    
                            //FIN de promociones de tipos de cliente por tipo de producto
                        } else {
    
                            //Promociones por tipo de producto sin tipo de cliente
                            if(!is_null($product_type->series_one) && !is_null($product_type->series_two)){
    
                                $sql_reservation_series_clone = clone $sql_reservation;
    
                                $sql_reservation_series_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                    ->whereNull('mo_product.deleted_at')
                                    ->where('mo_product.type_id', $product_type->product_type_id)
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->series_one));
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                $datos_count_series = $sql_reservation_series_clone->first();
    
                                if(!$datos_count_series){
                                    $aplica_aux = 0;
                                }
    
                            }
    
                            $sql_reservation_clone = clone $sql_reservation;
    
                            $sql_reservation_clone->select(
                                'mo_reservation_detail.id',
                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                        IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                        IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                        IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                        IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                        IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                        IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                        IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                        IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                        IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                        IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                        mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                )
                                ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                ->whereNull('mo_product.deleted_at')
                                ->whereNull('mo_reservation_detail.service_id')
                                ->where('mo_product.type_id', $product_type->product_type_id)
                                ->whereNotIn('mo_reservation_detail.id', $array_reservation_detail)
                                ->groupBy('mo_reservation_detail.id')
                                ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                    $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_activity.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                    $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_event', function ($join) {
                                    $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_event.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_package', function ($join) {
                                    $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_package.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_park', function ($join) {
                                    $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_park.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                    $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                    $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_tour.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                    $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                    $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_photo.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_food', function ($join) {
                                    $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_food.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                    $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                    $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                });
    
    
                            if ($promotion->apply_country_filter == 1) {
                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                            }
    
                            if($promotion->apply_state_filter == 1){
                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                            }
    
                            if ($promotion->apply_client_type_filter == 1){
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                            }
    
                            //Promociones por cantidad de pax por tipo de producto
                            if (!is_null($product_type->min_quantity)) {
    
                                $sql_reservation_clone
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                            }
    
                            if (!is_null($product_type->max_quantity)) {
                                $sql_reservation_clone
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity));
                            }
    
                            $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                ->mergeBindings($sql_reservation_clone);
    
                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                                    ->where('date_aux','<=', $fecha_fin);
                            }
    
                            $datos_reservation_count = $sql_reservation_clone->first();
    
                            if ($datos_reservation_count) {
                                $array_reservation_detail[] = $datos_reservation_count->id;
                            } else {
                                $aplica_aux = 0;
                            }
                            //FIN de promociones por catidad de pax por tipo de producto
                            //FIN de promociones de tipos de productos sin tipo de cliente
                        }
                    }
        //
                    //dd('entra');
                    if ($aplica_aux == 0) {
                        $aplica = 0;
                    }
                }
            }
            //FIN promociones por tipo de producto
    
            //Promociones por servicio
            if ($promotion->apply_service_filter == 1 && $aplica == 1) {
                $promotion_service = PromotionService::where('promotion_id', $promotion->id)->get();
    
                if (count($promotion_service) <= 0) {
                    $aplica = 0;
                } else {
    
                    $aplica_aux = 1;
                    $count = 1;
                    foreach ($promotion_service as $service) {
                        $promotion_service_count = PromotionService::where('promotion_id', $promotion->id)->where('service_id', $service->service_id)->count();
                        $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
                        $sql_reservation_clone = clone $sql_reservation;
    
                        $sql_reservation_clone
                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                            ->where('mo_reservation_detail.service_id', $service->service_id)
                            ->groupBy('mo_reservation_detail.id');
    
                        if ($promotion->apply_client_type_filter == 1){
                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                        }
    
                        $datos_count_services = $sql_reservation_clone->get();
    
                        if ($promotion_service_count > count($datos_count_services)) {
                            $aplica_aux = 0;
                        }
                        //Promociones por tipo de cliente de servicio
                        if ($service->apply_client_type_filter == 1) {
                            $promotion_service_client_types = PromotionServiceClientType::where('promotion_service_id', $service->id);
    
                            if(!is_null($service->series_one) && !is_null($service->series_two)){
                                $service_client_types = $promotion_service_client_types->groupBy('client_type_id')->pluck('client_type_id')->toArray();
    
                                $sql_reservation_series_clone = clone $sql_reservation;
    
                                $sql_reservation_series_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->where('mo_reservation_detail.service_id', $service->service_id)
                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $service_client_types)
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->series_one));
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                $datos_count_series = $sql_reservation_series_clone->first();
    
                                if(!$datos_count_series){
                                    $aplica_aux = 0;
                                }
    
                            }
    
                            $datos_promotion_service_client_types = $promotion_service_client_types->groupBy('client_type_id')->get();
    
                            if (count($datos_promotion_service_client_types) <= 0) {
                                $aplica_aux = 0;
                            } else {
                                $service_client_types = $promotion_service_client_types->groupBy('client_type_id')->pluck('client_type_id')->toArray();
    
                                $sql_reservation_clone = clone $sql_reservation;
    
                                $sql_reservation_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->where('mo_reservation_detail.service_id', $service->service_id)
                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $service_client_types)
                                    ->groupBy('mo_reservation_detail.id');
    
    
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if (!is_null($service->min_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity));
                                }
    
                                if (!is_null($service->max_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
                                }
    
                                $sql_reservation_clone->whereNotIn('mo_reservation_detail.id', $array_reservation_detail);
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                    )
                                    ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                        $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_event', function ($join) {
                                        $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_event.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_package', function ($join) {
                                        $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_package.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_park', function ($join) {
                                        $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_park.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                        $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                        $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_tour.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                        $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                        $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_photo.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_food', function ($join) {
                                        $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_food.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                        $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                    })
                                    ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                        $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                    });
    
                                $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                if (count($promotion_enjoy) > 0) {
    
    
                                    $datos_reservation_client_type = null;
    
                                    foreach ($promotion_enjoy as $enjoy) {
                                        $sql_reservation_enjoy = clone $sql_reservation_clone;
    
                                        if (is_null($datos_reservation_client_type)) {
    
                                            $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                ->mergeBindings($sql_reservation_enjoy)
                                                ->where(function ($query) use ($enjoy) {
                                                    $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                        ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                });
    
                                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                    ->where('date_aux','<=', $fecha_fin);
                                            }
    
                                            $products_enjoy = $sql_reservation_enjoy->get();
    
                                            foreach ($products_enjoy as $product_enjoy) {
                                                $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                if ($promotion_enjoy) {
                                                    $datos_reservation_client_type = $product_enjoy;
                                                }
                                            }
                                        }
                                    }
                                } else {
    
                                    $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_clone);
    
                                    if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                        $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                        $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                        $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                                            ->where('date_aux','<=', $fecha_fin);
                                    }
    
                                    $datos_reservation_client_type = $sql_reservation_clone->first();
                                }
    
                                if ($datos_reservation_client_type) {
                                    $array_reservation_detail[] = $datos_reservation_client_type->id;
                                } else {
                                    $aplica_aux = 0;
                                }
                            }
                            //FIN de promociones de tipos de cliente por servicio
                        } else {
                            //Promociones por servicio sin tipo de cliente
    
                            if(!is_null($service->series_one) && !is_null($service->series_two)){
    
                                $sql_reservation_series_clone = clone $sql_reservation;
    
                                $sql_reservation_series_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->where('mo_reservation_detail.service_id', $service->service_id)
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->series_one));
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $sql_reservation_series_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                $datos_count_series = $sql_reservation_series_clone->first();
    
                                if(!$datos_count_series){
                                    $aplica_aux = 0;
                                }
    
                            }
    
                            $sql_reservation_clone_clone = clone $sql_reservation;
    
                            $sql_reservation_clone_clone
                                ->select(
                                    'mo_reservation_detail.id',
                                    DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                        IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                        IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                        IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                        IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                        IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                        IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                        IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                        IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                        IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                        IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                        mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                )->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                ->whereNotIn('mo_reservation_detail.id', $array_reservation_detail)
                                ->where('mo_reservation_detail.service_id', $service->service_id)
                                ->groupBy('mo_reservation_detail.id')
                                ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                    $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_activity.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                    $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_event', function ($join) {
                                    $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_event.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_package', function ($join) {
                                    $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_package.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_park', function ($join) {
                                    $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_park.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                    $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                    $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_tour.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                    $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                    $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_photo.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_food', function ($join) {
                                    $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_food.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                    $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                })
                                ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                    $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                });
    
                            if ($promotion->apply_country_filter == 1) {
                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                            }
    
                            if($promotion->apply_state_filter == 1){
                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                            }
    
                            if ($promotion->apply_client_type_filter == 1){
                                $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                            }
    
                            //Promociones por cantidad de pax por servicio
                            if (!is_null($service->min_quantity)) {
                                $sql_reservation_clone_clone
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity));
    
                            }
    
                            if (!is_null($service->max_quantity)) {
    
                                $sql_reservation_clone_clone
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
                            }
    
                            $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                ->mergeBindings($sql_reservation_clone_clone);
    
                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                $sql_reservation_clone_clone->where('date_aux','>=', $fecha_inicio)
                                    ->where('date_aux','<=', $fecha_fin);
                            }
    
                            $datos_reservation_count = $sql_reservation_clone_clone->first();
    
                            if ($datos_reservation_count) {
                                $array_reservation_detail[] = $datos_reservation_count->id;
                            } else {
                                $aplica_aux = 0;
                            }
    
                            //FIN promociones por cantidad de pax por servicio
                            //FIN de promociones por servicio sin tipo de cliente
                        }
                    }
                    if ($aplica_aux == 0) {
                        $aplica = 0;
                    }
                }
            }
            //FIN de promociones por servicio
    
            //Añade promociones aplicables a la reserva
    
            if ($aplica == 1) {
                $array_promotions[] = $promotion;
            }
        }
    
        return $array_promotions;
    }
    
    function aplicatePromotionHelper($request, $emulated_subchannel_id, $idioma)
    {
    
        $fecha = Carbon::now();
    
        $fecha_actual = $fecha->format('Y-m-d H:i:s');
    
        $week_day = $fecha->format('N');
    
        $sql_promotions = DB::connection('tenant')->table('mo_promotion')
            ->select(
                'mo_promotion.id',
                'mo_promotion.code',
                'mo_promotion.order',
                'mo_promotion.visibility',
                'mo_promotion.accumulate',
                'mo_promotion.min_quantity',
                'mo_promotion.max_quantity',
                'mo_promotion.promotion_amount',
                'mo_promotion.currency_id',
                'mo_promotion.promotion_type_id',
                'mo_promotion.campaign_id',
                'mo_promotion.department_id',
                'mo_promotion.cost_center_id',
                'mo_promotion.apply_payment_method_filter',
                'mo_promotion.apply_language_filter',
                'mo_promotion.apply_currency_filter',
                'mo_promotion.apply_pickup_filter',
                'mo_promotion.apply_product_filter',
                'mo_promotion.apply_product_type_filter',
                'mo_promotion.apply_service_filter',
                'mo_promotion.apply_subchannel_filter',
                'mo_promotion.apply_client_type_filter',
                'mo_promotion.apply_country_filter',
                'mo_promotion.apply_state_filter',
                'mo_promotion.apply_device_filter',
                'mo_promotion.lockers_validation',
                'mo_promotion.supervisor_validation',
                'mo_promotion.allow_date_change',
                'mo_promotion.allow_upgrade',
                'mo_promotion.allow_product_change',
                'mo_promotion.pay_difference',
                'mo_promotion.anticipation_start',
                'mo_promotion.anticipation_end',
                'mo_promotion.coupon_code',
                'mo_promotion.coupon_type',
                'mo_promotion.coupon_sheet_start',
                'mo_promotion.coupon_sheet_end',
                'mo_promotion.coupon_total_uses',
                'mo_promotion.benefit_card',
                'mo_promotion.benefit_card_total_uses',
                'mo_promotion.benefit_card_id',
                'mo_promotion.user_id',
                'mo_promotion_translation.name',
                'mo_promotion_translation.description',
                'mo_promotion_translation.short_description',
                'mo_promotion_translation.friendly_url',
                'mo_promotion_translation.external_url',
                'mo_promotion_translation.title_seo',
                'mo_promotion_translation.description_seo',
                'mo_promotion_translation.keywords_seo',
                'mo_promotion_translation.observations',
                'mo_promotion_translation.legal',
                'mo_promotion_translation.notes',
                'mo_promotion_translation.gift',
                'mo_promotion_type_translation.name as type_name',
                'mo_campaign_translation.name as campaign_name'
            )
            ->whereNull('mo_promotion.deleted_at')
            ->join('mo_promotion_type', 'mo_promotion_type.id', 'mo_promotion.promotion_type_id')
            ->whereNull('mo_promotion_type.deleted_at')
            ->join('mo_promotion_type_translation', 'mo_promotion_type_translation.promotion_type_id', 'mo_promotion_type.id')
            ->whereNull('mo_promotion_type_translation.deleted_at')
            ->where('mo_promotion_type_translation.language_id', $idioma->id)
            ->join('mo_promotion_translation', 'mo_promotion_translation.promotion_id', 'mo_promotion.id')
            ->whereNull('mo_promotion_translation.deleted_at')
            ->join('mo_campaign', 'mo_campaign.id', 'mo_promotion.campaign_id')
            ->whereNull('mo_campaign.deleted_at')
            ->where('mo_campaign.date_start', '<=', $fecha->format('Y-m-d'))
            ->where('mo_campaign.date_end', '>=', $fecha->format('Y-m-d'))
            ->where('mo_campaign.active', 1)
            ->join('mo_campaign_translation', 'mo_campaign_translation.campaign_id', 'mo_campaign.id')
            ->whereNull('mo_campaign_translation.deleted_at')
            ->leftJoin('mo_campaign_subchannel', function ($join) {
                $join->on('mo_campaign_subchannel.campaign_id', 'mo_campaign.id')
                    ->whereNull('mo_campaign_subchannel.deleted_at');
            })
            ->where(function ($query) use ($emulated_subchannel_id) {
                $query->where('mo_campaign.apply_filter_subchannel', 0)
                    ->orWhere('mo_campaign_subchannel.subchannel_id', $emulated_subchannel_id);
            })
            ->join('mo_promotion_range_sale_day', 'mo_promotion_range_sale_day.promotion_id', 'mo_promotion.id')
            ->whereNull('mo_promotion_range_sale_day.deleted_at')
            ->where('mo_promotion_range_sale_day.sale_date_start', '<=', $fecha_actual)
            ->where('mo_promotion_range_sale_day.sale_date_end', '>=', $fecha_actual)
            ->join('mo_promotion_sale_day', 'mo_promotion_sale_day.promotion_range_sale_day_id', 'mo_promotion_range_sale_day.id')
            ->whereNull('mo_promotion_sale_day.deleted_at')
            ->where('mo_promotion_sale_day.day_id', $week_day)
            ->leftJoin('mo_promotion_subchannel', function ($join) {
                $join->on('mo_promotion_subchannel.promotion_id', '=', 'mo_promotion.id')
                    ->whereNull('mo_promotion_subchannel.deleted_at');
            })
            ->where(function ($query) use ($emulated_subchannel_id) {
                $query->where('mo_promotion.apply_subchannel_filter', 0)
                    ->orWhere('mo_promotion_subchannel.subchannel_id', $emulated_subchannel_id);
            })
            ->orderBy('mo_promotion.order', 'desc')
            ->groupBy('mo_promotion.id');
    
        $sql_price = DB::connection('tenant')->table('mo_price')
            ->select(
                'mo_price.id'
            )
            ->whereNull('mo_price.deleted_at')
            ->where('mo_price.promotions', 1)
            ->join('mo_reservation_detail', 'mo_reservation_detail.price_id', 'mo_price.id')
            ->whereNull('mo_reservation_detail.deleted_at')
            ->join('mo_reservation', 'mo_reservation.id', 'mo_reservation_detail.reservation_id')
            ->whereNull('mo_reservation.deleted_at')
            ->where('mo_reservation.id', $request->get('id'));
    
        $sql_reservation = DB::connection('tenant')->table('mo_reservation')
            ->whereNull('mo_reservation.deleted_at')
            ->where('mo_reservation.id', $request->get('id'))
            ->where('mo_reservation_detail.operation_status_id', '!=', 4)
            ->where('mo_reservation_detail.operation_status_id', '!=', 5)
            ->where('mo_reservation_detail.operation_status_id', '!=', 6)
            ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
            ->whereNull('mo_reservation_detail.deleted_at');
    
        $settings_language = Settings::where('name','idioma_defecto_id')->first()->value;
    
        $settings_device = Settings::where('name','dispositivo_defecto_id')->first()->value;
    
        $idiomas = Language::where('front', 1)->get();
    
        $devices = Device::all();
    
        if ($request->get('lang') != '') {
            $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
            if (!$idioma) {
                $idioma = $idiomas->where('id', $settings_language)->where('front', 1)->first();
            }
        } else {
            $idioma = $idiomas->where('id', $settings_language)->where('front', 1)->first();
        }
    
        if ($request->get('device') != '') {
            $device = $devices->where('code', Str::upper($request->get('device')))->where('active', 1)->first();
            if (!$device) {
                $device = $devices->where('id', $settings_device)->where('active', 1)->first();
            }
        } else {
            $device = $devices->where('id', $settings_device)->where('active', 1)->first();
        }
    
        $datos_price = $sql_price->first();
    
        $array_promotions = array();
    
    
        if ($datos_price) {
            if ($request->get('promotion_id') != '') {
                $sql_promotions->where('mo_promotion.id', $request->get('promotion_id'));
            }
    
            if ($request->get('coupon_code') != '') {
    
                $coupon = explode('-', $request->get('coupon_code'));
    
                $sql_promotions->where('mo_promotion.coupon_type', 1);
    
                if (isset($coupon[0])) {
                    $sql_promotions->where('mo_promotion.coupon_code', $coupon[0]);
                } else {
                    $sql_promotions->where('mo_promotion.coupon_code', $request->get('coupon_code'));
                }
    
                if (isset($coupon[1])) {
                    $sql_promotions->where('mo_promotion.coupon_sheet_start', '<=', $coupon[1])
                        ->where('mo_promotion.coupon_sheet_end', '>=', $coupon[1]);
                }
    
            }
    
            if ($request->get('benefit_card_id') != '') {
                $sql_promotions->where('mo_promotion.benefit_card', 1)
                    ->where('mo_promotion.benefit_card_id', $request->get('benefit_card_id'));
            }
    
    
            $datos_promotions = $sql_promotions->get();
    
            //Recorre las promociones vigentes
            $array_promotions = promotionsApplicable($datos_promotions, $request, $idioma, $device);
        }
    
        //FIN promociones vigentes
        foreach ($array_promotions as $promotion) {
    
            $promotion_translate = PromotionTranslation::where('promotion_id', $promotion->id)
                ->where('language_id', $idioma->id)
                ->first();
    
            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                ->where('language_id', $idioma->id)
                ->first();
    
            $identification_translation = null;
            if ($request->get('benefit_card_id') != '') {
                $identification_translation = IdentificationTranslation::where('identification_id', $request->get('benefit_card_id'))
                    ->where('language_id', $idioma->id)
                    ->first();
            }
    
            $tipo_promocion = null;
            $currency_id = null;
            if (!is_null($promotion->promotion_type_id)) {
                $tipo_promocion = $promotion->promotion_type_id;
                $currency_id = $promotion->currency_id;
            }
    
            $promotion_auto_accumulate = PromotionAccumulate::where('promotion_id', $promotion->id)
                ->where('promotion_accumulate_id', $promotion->id)
                ->first();
    
            $sql_reservation_detail = clone $sql_reservation;
            $sql_reservation_detail
                ->leftJoin('mo_reservation_detail_activity', function ($join) {
                    $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_activity.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                    $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_day_more.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_event', function ($join) {
                    $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_event.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_food', function ($join) {
                    $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_food.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_package', function ($join) {
                    $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_package.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                    $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_hotel.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_park', function ($join) {
                    $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_park.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_photo', function ($join) {
                    $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_photo.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                    $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_tour', function ($join) {
                    $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_tour.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                    $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_transportation.deleted_at');
                })
                ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                    $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                })
                ->select(
                    'mo_reservation_detail.id'
                )
                ->orderBy('mo_reservation_detail_activity.date', 'asc')
                ->orderBy('mo_reservation_detail_day_more.date', 'asc')
                ->orderBy('mo_reservation_detail_event.date', 'asc')
                ->orderBy('mo_reservation_detail_food.date', 'asc')
                ->orderBy('mo_reservation_detail_package.date', 'asc')
                ->orderBy('mo_reservation_detail_park.date', 'asc')
                ->orderBy('mo_reservation_detail_photo.date', 'asc')
                ->orderBy('mo_reservation_detail_restaurant.date', 'asc')
                ->orderBy('mo_reservation_detail_tour.date', 'asc')
                ->orderBy('mo_reservation_detail_transportation.date', 'asc')
                ->orderBy('mo_reservation_detail_hotel.date_start', 'asc');
    
            $settings_decimales = Settings::where('name', 'numero_decimales')->first()->value;
    
            $benefit_card_name = null;
            $benefit_card_id = null;
            $benefit_card_number = null;
            if ($promotion->benefit_card == 1) {
                $benefit_card_id = $request->get('benefit_card_id');
                $benefit_card_number = $request->get('benefit_card_number');
                if ($request->get('benefit_card_id') != '') {
                    $benefit_card_name = IdentificationTranslation::where('identification_id', $request->get('benefit_card_id'))
                        ->where('language_id', $idioma->id)
                        ->first()->name;
                }
            }
    
            $coupon_code = null;
    
            if ($promotion->coupon_type == 1) {
                $coupon_code = $request->get('coupon_code');
            }
    
            if (!$promotion_auto_accumulate) {
                $auto_accumulate = 0;
            } else {
                $auto_accumulate = 1;
            }
            $repetir = 1;
            $array_detalles_repetir = array();
    
            ReservationPromotion::where('promotion_id', $promotion->id)->where('reservation_id', $request->get('id'))->delete();
    
            $array_excluidos = array();
    
            $promotion_product_type = array();
    
            $promotion_product = array();
    
            $promotion_service = array();
    
            if ($promotion->apply_product_type_filter == 1 || $promotion->apply_product_filter == 1 || $promotion->apply_service_filter == 1) {
    
    
                $count = 0;
                while ($repetir) {
                    $repetir = 1;
                    $array_detalles = $array_excluidos;
    
                    if ($promotion->apply_product_filter == 1) {
                        $promotion_product = PromotionProduct::where('promotion_id', $promotion->id)->get();
    
                        foreach ($promotion_product as $product) {
                            if(!is_null($product->promotion_amount) || (!is_null($product->series_one) && !is_null($product->series_two)) || ($product->promotion_type_id == 4 || $product->promotion_type_id == 5 || is_null($product->promotion_type_id))) {
                                $sql_reservation_clone = clone $sql_reservation_detail;
    
                                if ($product->apply_client_type_filter == 1) {
                                    $promotions_client_type = PromotionProductClientType::where('promotion_product_id', $product->id)->groupBy('client_type_id')->get();
    
                                    if (count($promotions_client_type)) {
                                        $sql_reservation_clone = clone $sql_reservation;
    
                                        $sql_reservation_clone
                                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                            ->whereNull('mo_reservation_detail.service_id')
                                            ->groupBy('mo_reservation_detail.id')
                                            ->where('mo_reservation_detail.product_id', $product->product_id)
                                            ->whereNotIn('mo_reservation_detail.id', $array_detalles);
    
                                        if ($promotion->apply_country_filter == 1) {
                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                        }
    
                                        if ($promotion->apply_state_filter == 1) {
                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                        }
    
    
                                        if (!is_null($product->location_id)) {
                                            $sql_reservation_clone->where('mo_reservation_detail.location_id', $product->location_id);
                                        }
    
                                        if (!is_null($product->series_one) && !is_null($product->series_two)) {
                                            $datos_reservation_client_type = null;
    //
                                            $array_client_types_ids = PromotionProductClientType::where('promotion_product_id', $product->id)->pluck('client_type_id')->toArray();
    
                                            $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                            if (!is_null($product->min_quantity) && is_null($product->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
    
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                                            }
    
                                            if (!is_null($product->max_quantity) && is_null($product->min_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity));
                                            }
    
                                            if (!is_null($product->min_quantity) && !is_null($product->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity))
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                                            }
    
                                            if (is_null($product->min_quantity) && is_null($product->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    );
                                            }
                                            $sql_reservation_clone_clone->addSelect(
                                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                            )->leftJoin('mo_reservation_detail_activity', function ($join) {
                                                $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                                    ->whereNull('mo_reservation_detail_activity.deleted_at');
                                            })
                                                ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                                    $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_event', function ($join) {
                                                    $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_event.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_package', function ($join) {
                                                    $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_package.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_park', function ($join) {
                                                    $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_park.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                                    $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                                    $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_tour.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                                    $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                                    $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_photo.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_food', function ($join) {
                                                    $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_food.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                                    $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                                    $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                                });
    
                                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                            if (count($promotion_enjoy) > 0) {
    
    
                                                foreach ($promotion_enjoy as $enjoy) {
                                                    $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
                                                    if (is_null($datos_reservation_client_type)) {
    
                                                        $sql_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                            ->mergeBindings($sql_reservation_enjoy)
                                                            ->where(function ($query) use ($enjoy) {
                                                                $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                                    ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                            });
    
                                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                            $sql_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                                ->where('date_aux', '<=', $fecha_fin);
                                                        }
    
                                                        $products_enjoy = $sql_enjoy->get();
                                                        foreach ($products_enjoy as $product_enjoy) {
                                                            $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                            $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                            if ($promotion_enjoy) {
                                                                $datos_reservation_client_type = $product_enjoy;
                                                            }
                                                        }
                                                    }
                                                }
    
                                            } else {
                                                $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_clone_clone)
                                                    ->orderBy('total_client_types', 'DESC');
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_clone_clone->where('date_aux', '>=', $fecha_inicio)
                                                        ->where('date_aux', '<=', $fecha_fin);
                                                }
    
                                                $datos_reservation_detail = $sql_reservation_clone_clone->first();
                                            }
                                            $array_detalles[] = $datos_reservation_client_type->id;
    
                                            $repetir_tipos = 1;
                                            $array_tipos_cliente = array();
    
                                            while ($repetir_tipos) {
                                                $sql_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                    ->whereIn('client_type_id', $array_client_types_ids)
                                                    ->whereNotIn('id', $array_tipos_cliente)
                                                    ->orderBy('client_type_id', 'ASC')
                                                    ->limit($product->series_one);
    
                                                if ($promotion->apply_country_filter == 1) {
                                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                    $sql_detail_client_type->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                }
    
                                                if ($promotion->apply_state_filter == 1) {
                                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                    $sql_detail_client_type->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                }
    
                                                $reservation_detail_client_type = $sql_detail_client_type->get();
    
                                                if (count($reservation_detail_client_type) > 0) {
                                                    $count_tipos = $product->series_one;
    
                                                    foreach ($reservation_detail_client_type as $detail_client_type) {
    
                                                        $array_tipos_cliente[] = $detail_client_type->id;
    
                                                        if ($count_tipos <= $product->series_one - $product->series_two) {
    
                                                            $promotion_amount_total = $detail_client_type->sale_price;
    
                                                            $amount = 100;
    
                                                            $promotion_type_id = null;
                                                            $promotion_type_translation = null;
                                                            $currency_id = null;
    
                                                            $promotion_type_id = 1;
                                                            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', 1)
                                                                ->where('language_id', $idioma->id)
                                                                ->first()->name;
    
                                                            ReservationPromotion::create([
                                                                'reservation_id' => $request->get('id'),
                                                                'promotion_id' => $promotion->id,
                                                                'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                'promotion_amount' => $amount,
                                                                'promotion_amount_total' => $promotion_amount_total,
                                                                'promotion_name' => $promotion_translate->name,
                                                                'promotion_short_description' => $promotion_translate->short_description,
                                                                'promotion_order' => $promotion->order,
                                                                'promotion_type_id' => $promotion_type_id,
                                                                'promotion_type_name' => $promotion_type_translation,
                                                                'coupon_code' => $coupon_code,
                                                                'benefit_card_id' => $benefit_card_id,
                                                                'benefit_card_number' => $benefit_card_number,
                                                                'benefit_card_name' => $benefit_card_name,
                                                                'code' => $promotion->code,
                                                                'currency_id' => $currency_id,
                                                            ]);
                                                        }
                                                        $count_tipos--;
                                                    }
    
                                                    $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                        ->whereIn('client_type_id', $array_client_types_ids)
                                                        ->whereNotIn('id', $array_tipos_cliente)
                                                        ->orderBy('client_type_id', 'ASC')
                                                        ->limit($product->series_one)
                                                        ->get();
    
                                                    if (count($reservation_detail_client_type) < $product->series_one) {
                                                        $repetir_tipos = 0;
                                                    }
    
                                                } else {
                                                    $repetir_tipos = 0;
                                                }
                                            }
                                        } else {
    
                                            if (!is_null($product->min_quantity)) {
                                                $sql_reservation_clone
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                                            }
    
                                            if (!is_null($product->max_quantity)) {
                                                $sql_reservation_clone
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity));
                                            }
    
                                            $count = 0;
                                            foreach ($promotions_client_type as $promotion_client_type) {
                                                $datos_reservation_client_type = null;
    
                                                $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                $sql_reservation_clone_clone
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types'),
                                                        'mo_reservation_detail_client_type.id as detail_client_type_id',
                                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                    IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                    IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                    IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                    IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                    IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                    IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                    IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                    IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                    IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                    IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                    mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                                    )
                                                    ->where('mo_reservation_detail_client_type.client_type_id', $promotion_client_type->client_type_id)
                                                    ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                                        $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_event', function ($join) {
                                                        $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_event.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_package', function ($join) {
                                                        $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_package.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_park', function ($join) {
                                                        $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_park.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                                        $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                                        $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_tour.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                                        $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                                        $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_photo.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_food', function ($join) {
                                                        $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_food.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                                        $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                                        $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                                    });
    
                                                $count++;
    
                                                $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                                if (count($promotion_enjoy) > 0) {
    
                                                    foreach ($promotion_enjoy as $enjoy) {
                                                        $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
                                                        if (is_null($datos_reservation_client_type)) {
    
                                                            $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                                ->mergeBindings($sql_reservation_enjoy)
                                                                ->where(function ($query) use ($enjoy) {
                                                                    $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                                        ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                                });
    
                                                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                                $sql_reservation_clone_clone->where('date_aux', '>=', $fecha_inicio)
                                                                    ->where('date_aux', '<=', $fecha_fin);
                                                            }
    
                                                            $products_enjoy = $sql_reservation_enjoy->get();
                                                            foreach ($products_enjoy as $product_enjoy) {
                                                                $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                                if ($promotion_enjoy) {
                                                                    $datos_reservation_client_type = $product_enjoy;
                                                                }
                                                            }
                                                        }
                                                    }
    
                                                } else {
    
                                                    $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                                        ->mergeBindings($sql_reservation_clone_clone);
    
                                                    if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                        $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                        $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                        $sql_reservation_clone_clone->where('date_aux', '>=', $fecha_inicio)
                                                            ->where('date_aux', '<=', $fecha_fin);
                                                    }
    
                                                    $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                                }
    
                                                if ($datos_reservation_client_type) {
                                                    $array_detalles[] = $datos_reservation_client_type->id;
    
                                                    if (!is_null($product->promotion_amount) && ($product->promotion_type_id != 4 && $product->promotion_type_id != 5)) {
                                                        $amount = $product->promotion_amount;
    
                                                        $promotion_type_id = null;
                                                        $promotion_type_translation = null;
                                                        $promotion_amount_total = null;
                                                        $currency_id = null;
    
                                                        $promotion_type_id = $product->promotion_type_id;
                                                        $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product->promotion_type_id)
                                                            ->where('language_id', $idioma->id)
                                                            ->first()->name;
                                                        $currency_id = $product->currency_id;
    
                                                        //obtener array de ids de tipos de cliente
    
                                                        $sql_clientes_detail = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                            ->where('client_type_id', $promotion_client_type->client_type_id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $tipos_clientes_detail = $sql_clientes_detail->get();
    
                                                        foreach ($tipos_clientes_detail as $tipo_cliente_detail) {
    
                                                            $sql_reservation_clone_detail = clone $sql_reservation_detail;
    
                                                            if ($product->promotion_type_id == 1) {
                                                                $sql_reservation_clone_clone = clone $sql_reservation_clone_detail;
                                                                $datos_suma = $sql_reservation_clone_clone
                                                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                                    ->where('mo_reservation_detail.id', $datos_reservation_client_type->id)
                                                                    ->where('mo_reservation_detail_client_type.id', $tipo_cliente_detail->id)
                                                                    ->sum('mo_reservation_detail_client_type.sale_price');
    
                                                                $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                                    ->whereNull('mo_reservation_promotion.deleted_at')
                                                                    ->where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                    ->where('reservation_detail_client_type_id', $tipo_cliente_detail->id)
                                                                    ->get();
    
                                                                foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                                    if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    if ($dato_promocion->promotion_type_id == 3) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                                }
                                                                $promotion_amount_total = $datos_suma * ($product->promotion_amount / 100);
                                                            }
    
                                                            if ($product->promotion_type_id == 2) {
                                                                $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                                $datos_reservation_currency = $sql_reservation_currency
                                                                    ->select(
                                                                        'mo_reservation_detail.currency_id'
                                                                    )->first();
    
                                                                if ($product->currency_id != $datos_reservation_currency->currency_id) {
                                                                    $promotion_amount_total = conversor($product->promotion_amount, $product->currency_id, $datos_reservation_currency->currency_id);
                                                                } else {
                                                                    $promotion_amount_total = $product->promotion_amount;
                                                                }
    
    
                                                            }
    
                                                            if ($product->promotion_type_id == 3) {
                                                                $sql_reservation_clone_clone = clone $sql_reservation_clone_detail;
                                                                $datos_suma = $sql_reservation_clone_clone
                                                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                                    ->where('mo_reservation_detail.id', $datos_reservation_client_type->id)
                                                                    ->where('mo_reservation_detail_client_type.id', $tipo_cliente_detail->id)
                                                                    ->sum('mo_reservation_detail_client_type.sale_price');
    
                                                                $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                                    ->where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                    ->whereNull('mo_reservation_promotion.deleted_at')
                                                                    ->where('reservation_detail_client_type_id', $tipo_cliente_detail->id)
                                                                    ->get();
    
                                                                foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                                    if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    if ($dato_promocion->promotion_type_id == 3) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                                }
    
                                                                $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                                $datos_reservation_currency = $sql_reservation_currency
                                                                    ->select(
                                                                        'mo_reservation_detail.currency_id'
                                                                    )->first();
    
                                                                if ($product->currency_id != $datos_reservation_currency->currency_id) {
    
                                                                    $descuento = $datos_suma - conversor($product->promotion_amount, $product->currency_id, $datos_reservation_currency->currency_id);
                                                                    $promotion_amount_total = $descuento;
                                                                } else {
                                                                    $descuento = $datos_suma - $product->promotion_amount;
                                                                    $promotion_amount_total = $descuento;
                                                                }
    
                                                            }
    
                                                            ReservationPromotion::create([
                                                                'reservation_id' => $request->get('id'),
                                                                'promotion_id' => $promotion->id,
                                                                'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                'reservation_detail_client_type_id' => $tipo_cliente_detail->id,
                                                                'promotion_amount' => $amount,
                                                                'promotion_amount_total' => $promotion_amount_total,
                                                                'promotion_name' => $promotion_translate->name,
                                                                'promotion_short_description' => $promotion_translate->short_description,
                                                                'promotion_order' => $promotion->order,
                                                                'promotion_type_id' => $promotion_type_id,
                                                                'promotion_type_name' => $promotion_type_translation,
                                                                'coupon_code' => $coupon_code,
                                                                'benefit_card_id' => $benefit_card_id,
                                                                'benefit_card_number' => $benefit_card_number,
                                                                'benefit_card_name' => $benefit_card_name,
                                                                'code' => $promotion->code,
                                                                'currency_id' => $currency_id,
                                                            ]);
    
                                                            $array_client_types[] = $datos_reservation_client_type->detail_client_type_id;
                                                        }
                                                    } else {
                                                        if ($product->promotion_type_id == 4 || $product->promotion_type_id == 5) {
                                                            $promotion_type_id = null;
                                                            $promotion_type_translation = null;
    
                                                            $promotion_type_id = $product->promotion_type_id;
                                                            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product->promotion_type_id)
                                                                ->where('language_id', $idioma->id)
                                                                ->first()->name;
    
                                                            //obtener array de ids de tipos de cliente
                                                            $sql_clientes_detail = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                ->where('client_type_id', $promotion_client_type->client_type_id);
    
                                                            if ($promotion->apply_country_filter == 1) {
                                                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                                $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                            }
    
                                                            if ($promotion->apply_state_filter == 1) {
                                                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                                $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                            }
    
                                                            $tipos_clientes_detail = $sql_clientes_detail->get();
    
                                                            foreach ($tipos_clientes_detail as $tipo_cliente_detail) {
    
                                                                ReservationPromotion::create([
                                                                    'reservation_id' => $request->get('id'),
                                                                    'promotion_id' => $promotion->id,
                                                                    'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                    'reservation_detail_client_type_id' => $tipo_cliente_detail->id,
                                                                    'promotion_name' => $promotion_translate->name,
                                                                    'promotion_short_description' => $promotion_translate->short_description,
                                                                    'promotion_order' => $promotion->order,
                                                                    'promotion_type_id' => $promotion_type_id,
                                                                    'promotion_type_name' => $promotion_type_translation,
                                                                    'coupon_code' => $coupon_code,
                                                                    'benefit_card_id' => $benefit_card_id,
                                                                    'benefit_card_number' => $benefit_card_number,
                                                                    'benefit_card_name' => $benefit_card_name,
                                                                    'code' => $promotion->code,
                                                                ]);
    
                                                                $array_client_types[] = $datos_reservation_client_type->detail_client_type_id;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $sql_reservation_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                        ->whereNull('mo_reservation_detail.service_id')
                                        ->whereNotIn('mo_reservation_detail.id', $array_detalles)
                                        ->where('mo_reservation_detail.product_id', $product->product_id)
                                        ->groupBy('mo_reservation_detail.id')
                                        ->select(
                                            'mo_reservation_detail.id'
                                        );
    
    
                                    if ($promotion->apply_country_filter == 1) {
                                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                        $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                    }
    
                                    if ($promotion->apply_state_filter == 1) {
                                        $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                        $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                    }
    
                                    if (!is_null($product->location_id)) {
                                        $sql_reservation_clone->where('mo_reservation_detail.location_id', $product->location_id);
                                    }
    
                                    //Promociones por cantidad de pax por producto
                                    if (!is_null($product->min_quantity) && is_null($product->max_quantity)) {
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                                    }
    
                                    if (!is_null($product->max_quantity) && is_null($product->min_quantity)) {
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity));
                                    }
    
                                    if (!is_null($product->max_quantity) && !is_null($product->min_quantity)) {
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity))
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                                    }
    
                                    if (is_null($product->min_quantity) && is_null($product->max_quantity)) {
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            );
                                    }
    
                                    $sql_reservation_clone->addSelect(
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                            IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                            IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                            IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                            IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                            IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                            IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                            IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                            IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                            IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                            IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                            mo_reservation_detail_hotel.date_start))))))))))) AS date_aux'));
    
                                    $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                    if (count($promotion_enjoy) > 0) {
    
    
                                        $datos_reservation_detail = null;
    
                                        foreach ($promotion_enjoy as $enjoy) {
                                            $sql_reservation_enjoy = clone $sql_reservation_clone;
                                            if (is_null($datos_reservation_detail)) {
    
                                                $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_enjoy)
                                                    ->where(function ($query) use ($enjoy) {
                                                        $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                            ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                    });
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                        ->where('date_aux', '<=', $fecha_fin);
                                                }
    
                                                $products_enjoy = $sql_reservation_enjoy->get();
                                                foreach ($products_enjoy as $product_enjoy) {
                                                    $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                    $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                    if ($promotion_enjoy) {
                                                        $datos_reservation_detail = $product_enjoy;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_clone)
                                            ->orderBy('total_client_types', 'DESC');
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_clone->where('date_aux', '>=', $fecha_inicio)
                                                ->where('date_aux', '<=', $fecha_fin);
                                        }
    
                                        $datos_reservation_detail = $sql_reservation_clone->first();
                                    }
    
                                    $array_detalles[] = $datos_reservation_detail->id;
    
                                    if (!is_null($product->series_one) && !is_null($product->series_two)) {
    
                                        $repetir_tipos = 1;
                                        $array_tipos_cliente = array();
                                        while ($repetir_tipos) {
                                            $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_detail->id)
                                                ->whereNotIn('id', $array_tipos_cliente)
                                                ->orderBy('client_type_id', 'ASC')
                                                ->limit($product->series_one)
                                                ->get();
    
                                            if (count($reservation_detail_client_type) > 0) {
                                                $count_tipos = $product->series_one;
    
                                                foreach ($reservation_detail_client_type as $detail_client_type) {
    
                                                    $array_tipos_cliente[] = $detail_client_type->id;
    
                                                    if ($count_tipos <= $product->series_one - $product->series_two) {
    
                                                        $promotion_amount_total = $detail_client_type->sale_price;
    
                                                        $amount = 100;
    
                                                        $promotion_type_id = null;
                                                        $promotion_type_translation = null;
                                                        $currency_id = null;
    
                                                        $promotion_type_id = 1;
                                                        $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', 1)
                                                            ->where('language_id', $idioma->id)
                                                            ->first()->name;
    
                                                        ReservationPromotion::create([
                                                            'reservation_id' => $request->get('id'),
                                                            'promotion_id' => $promotion->id,
                                                            'reservation_detail_id' => $datos_reservation_detail->id,
                                                            'promotion_amount' => $amount,
                                                            'promotion_amount_total' => $promotion_amount_total,
                                                            'promotion_name' => $promotion_translate->name,
                                                            'promotion_short_description' => $promotion_translate->short_description,
                                                            'promotion_order' => $promotion->order,
                                                            'promotion_type_id' => $promotion_type_id,
                                                            'promotion_type_name' => $promotion_type_translation,
                                                            'coupon_code' => $coupon_code,
                                                            'benefit_card_id' => $benefit_card_id,
                                                            'benefit_card_number' => $benefit_card_number,
                                                            'benefit_card_name' => $benefit_card_name,
                                                            'code' => $promotion->code,
                                                            'currency_id' => $currency_id,
                                                        ]);
                                                    }
                                                    $count_tipos--;
                                                }
    
                                                $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_detail->id)
                                                    ->whereNotIn('id', $array_tipos_cliente)
                                                    ->orderBy('client_type_id', 'ASC')
                                                    ->limit($product->series_one)
                                                    ->get();
    
                                                if (count($reservation_detail_client_type) < $product->series_one) {
                                                    $repetir_tipos = 0;
                                                }
                                            } else {
                                                $repetir_tipos = 0;
                                            }
    
                                        }
                                    } else {
                                        if (!is_null($product->promotion_amount) && ($product->promotion_type_id != 4 && $product->promotion_type_id != 5)) {
                                            $amount = $product->promotion_amount;
    
                                            $promotion_type_id = null;
                                            $promotion_type_translation = null;
                                            $currency_id = null;
                                            $promotion_amount_total = 0;
    
                                            if (!is_null($product->promotion_type_id)) {
    
                                                $promotion_type_id = $product->promotion_type_id;
                                                $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product->promotion_type_id)
                                                    ->where('language_id', $idioma->id)
                                                    ->first()->name;
                                                $currency_id = $product->currency_id;
    
                                                if (!is_null($product->promotion_amount)) {
                                                    $sql_reservation_clone = clone $sql_reservation_detail;
    
                                                    if ($product->promotion_type_id == 1) {
                                                        $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                        $sql_reservation_clone_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                            ->where('mo_reservation_detail.id', $datos_reservation_detail->id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $datos_suma = $sql_reservation_clone_clone->sum('mo_reservation_detail_client_type.sale_price');
    
                                                        $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                            ->where('reservation_detail_id', $datos_reservation_detail->id)
                                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                                            ->orderBy('promotion_type_id', 'asc')
                                                            ->get();
    
                                                        foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            if ($dato_promocion->promotion_type_id == 3) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                        }
    
                                                        $promotion_amount_total = $datos_suma * ($product->promotion_amount / 100);
                                                    }
    
                                                    if ($product->promotion_type_id == 2) {
                                                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                        $datos_reservation_currency = $sql_reservation_currency
                                                            ->select(
                                                                'mo_reservation_detail.currency_id'
                                                            )->first();
    
                                                        if ($product->currency_id != $datos_reservation_currency->currency_id) {
                                                            $promotion_amount_total = conversor($product->promotion_amount, $product->currency_id, $datos_reservation_currency->currency_id);
                                                        } else {
                                                            $promotion_amount_total = $product->promotion_amount;
                                                        }
                                                    }
    
                                                    if ($product->promotion_type_id == 3) {
                                                        $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                        $sql_reservation_clone_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                            ->where('mo_reservation_detail.id', $datos_reservation_detail->id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $datos_suma = $sql_reservation_clone_clone->sum('mo_reservation_detail_client_type.sale_price');
    
                                                        $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                            ->where('reservation_detail_id', $datos_reservation_detail->id)
                                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                                            ->orderBy('promotion_type_id', 'asc')
                                                            ->get();
    
                                                        foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            if ($dato_promocion->promotion_type_id == 3) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                        }
    
                                                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                        $datos_reservation_currency = $sql_reservation_currency
                                                            ->select(
                                                                'mo_reservation_detail.currency_id'
                                                            )->first();
    
                                                        if ($product->currency_id != $datos_reservation_currency->currency_id) {
                                                            $descuento = $datos_suma - conversor($product->promotion_amount, $product->currency_id, $datos_reservation_currency->currency_id);
                                                            $promotion_amount_total = $descuento;
                                                        } else {
                                                            $descuento = $datos_suma - $product->promotion_amount;
                                                            $promotion_amount_total = $descuento;
                                                        }
                                                    }
                                                }
                                            } else {
                                                $promotion_type_id = $promotion->promotion_type_id;
                                                $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                                                    ->where('language_id', $idioma->id)
                                                    ->first()->name;
    
                                                $currency_id = $promotion->currency_id;
                                            }
    
                                            ReservationPromotion::create([
                                                'reservation_id' => $request->get('id'),
                                                'promotion_id' => $promotion->id,
                                                'reservation_detail_id' => $datos_reservation_detail->id,
                                                'promotion_amount' => $amount,
                                                'promotion_amount_total' => $promotion_amount_total,
                                                'promotion_name' => $promotion_translate->name,
                                                'promotion_short_description' => $promotion_translate->short_description,
                                                'promotion_order' => $promotion->order,
                                                'promotion_type_id' => $promotion_type_id,
                                                'promotion_type_name' => $promotion_type_translation,
                                                'coupon_code' => $coupon_code,
                                                'benefit_card_id' => $benefit_card_id,
                                                'benefit_card_number' => $benefit_card_number,
                                                'benefit_card_name' => $benefit_card_name,
                                                'code' => $promotion->code,
                                                'currency_id' => $currency_id,
                                            ]);
                                        } else {
    
                                            if ($product->promotion_type_id == 4 || $product->promotion_type_id == 5) {
    
                                                $promotion_type_id = null;
                                                $promotion_type_translation = null;
    
                                                if (!is_null($product->promotion_type_id)) {
    
                                                    $promotion_type_id = $product->promotion_type_id;
                                                    $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product->promotion_type_id)
                                                        ->where('language_id', $idioma->id)
                                                        ->first()->name;
    
                                                } else {
                                                    $promotion_type_id = $promotion->promotion_type_id;
                                                    $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                                                        ->where('language_id', $idioma->id)
                                                        ->first()->name;
                                                }
    
                                                ReservationPromotion::create([
                                                    'reservation_id' => $request->get('id'),
                                                    'promotion_id' => $promotion->id,
                                                    'reservation_detail_id' => $datos_reservation_detail->id,
                                                    'promotion_name' => $promotion_translate->name,
                                                    'promotion_short_description' => $promotion_translate->short_description,
                                                    'promotion_order' => $promotion->order,
                                                    'promotion_type_id' => $promotion_type_id,
                                                    'promotion_type_name' => $promotion_type_translation,
                                                    'coupon_code' => $coupon_code,
                                                    'benefit_card_id' => $benefit_card_id,
                                                    'benefit_card_number' => $benefit_card_number,
                                                    'benefit_card_name' => $benefit_card_name,
                                                    'code' => $promotion->code,
                                                ]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
    
                        $array_detalles_repetir = array_merge($array_detalles_repetir, $array_detalles);
                        $array_excluidos = array_merge($array_excluidos, $array_detalles);
                    }
    
                    if ($promotion->apply_product_type_filter == 1) {
    
                        $promotion_product_type = PromotionProductType::where('promotion_id', $promotion->id)->get();
    
                        foreach ($promotion_product_type as $product_type) {
    
                            if(!is_null($product_type->promotion_amount) || (!is_null($product_type->series_one) && !is_null($product_type->series_two)) || ($product_type->promotion_type_id == 4 || $product_type->promotion_type_id == 5 || is_null($product_type->promotion_type_id))) {
    
                                $sql_reservation_clone = clone $sql_reservation_detail;
    
                                if ($product_type->apply_client_type_filter == 1) {
    
                                    $promotions_client_type = PromotionProductTypeClientType::where('promotion_product_type_id', $product_type->id)->groupBy('client_type_id')->get();
    
                                    if (count($promotions_client_type) > 0) {
                                        $sql_reservation_clone = clone $sql_reservation;
    
                                        $sql_reservation_clone
                                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                            ->whereNull('mo_reservation_detail.service_id')
                                            ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                            ->whereNull('mo_product.deleted_at')
                                            ->where('mo_product.type_id', $product_type->product_type_id)
                                            ->groupBy('mo_reservation_detail.id')
                                            ->whereNotIn('mo_reservation_detail.id', $array_detalles);
    
                                        if ($promotion->apply_country_filter == 1) {
                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                        }
    
                                        if ($promotion->apply_state_filter == 1) {
                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                        }
    
                                        if (!is_null($product_type->series_one) && !is_null($product_type->series_two)) {
                                            $datos_reservation_client_type = null;
    
                                            $array_client_types_ids = PromotionProductTypeClientType::where('promotion_product_type_id', $product_type->id)->pluck('client_type_id')->toArray();
    
    
                                            $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                            if (!is_null($product_type->min_quantity) && is_null($product_type->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                                            }
    
                                            if (!is_null($product_type->max_quantity) && is_null($product_type->min_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity));
                                            }
    
    
                                            if (!is_null($product_type->min_quantity) && !is_null($product_type->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity))
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                                            }
    
                                            if (is_null($product_type->min_quantity) && is_null($product_type->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    );
                                            }
    
                                            $sql_reservation_clone_clone->addSelect(
                                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                    IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                    IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                    IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                    IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                    IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                    IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                    IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                    IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                    IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                    IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                    mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                            )->leftJoin('mo_reservation_detail_activity', function ($join) {
                                                $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                                    ->whereNull('mo_reservation_detail_activity.deleted_at');
                                            })
                                                ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                                    $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_event', function ($join) {
                                                    $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_event.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_package', function ($join) {
                                                    $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_package.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_park', function ($join) {
                                                    $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_park.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                                    $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                                    $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_tour.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                                    $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                                    $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_photo.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_food', function ($join) {
                                                    $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_food.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                                    $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                                    $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                                });
    
                                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                            if (count($promotion_enjoy) > 0) {
    
                                                foreach ($promotion_enjoy as $enjoy) {
    
                                                    $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
    
                                                    if (is_null($datos_reservation_client_type)) {
    
                                                        $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                            ->mergeBindings($sql_reservation_enjoy)
                                                            ->where(function ($query) use ($enjoy) {
                                                                $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                                    ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                            });
    
                                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                            $sql_reservation_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                                ->where('date_aux', '<=', $fecha_fin);
                                                        }
    
                                                        $products_enjoy = $sql_reservation_enjoy->get();
    
                                                        foreach ($products_enjoy as $product_enjoy) {
                                                            $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                            $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                            if ($promotion_enjoy) {
                                                                $datos_reservation_client_type = $product_enjoy;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_clone_clone)
                                                    ->orderBy('total_client_types', 'DESC');
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_clone_clone->where('date_aux', '>=', $fecha_inicio)
                                                        ->where('date_aux', '<=', $fecha_fin);
                                                }
    
                                                $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                            }
    
                                            $array_detalles[] = $datos_reservation_client_type->id;
    
                                            $repetir_tipos = 1;
                                            $array_tipos_cliente = array();
    
                                            while ($repetir_tipos) {
                                                $sql_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                    ->whereIn('client_type_id', $array_client_types_ids)
                                                    ->whereNotIn('id', $array_tipos_cliente)
                                                    ->orderBy('client_type_id', 'ASC')
                                                    ->limit($product_type->series_one);
    
                                                if ($promotion->apply_country_filter == 1) {
                                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                    $sql_detail_client_type->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                }
    
                                                if ($promotion->apply_state_filter == 1) {
                                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                    $sql_detail_client_type->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                }
    
                                                $reservation_detail_client_type = $sql_detail_client_type->get();
    
                                                if (count($reservation_detail_client_type) > 0) {
                                                    $count_tipos = $product_type->series_one;
                                                    foreach ($reservation_detail_client_type as $detail_client_type) {
    
                                                        $array_tipos_cliente[] = $detail_client_type->id;
    
                                                        if ($count_tipos <= $product_type->series_one - $product_type->series_two) {
    
                                                            $promotion_amount_total = $detail_client_type->sale_price;
    
                                                            $amount = 100;
    
                                                            $promotion_type_id = null;
                                                            $promotion_type_translation = null;
                                                            $currency_id = null;
    
                                                            $promotion_type_id = 1;
                                                            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', 1)
                                                                ->where('language_id', $idioma->id)
                                                                ->first()->name;
    
                                                            ReservationPromotion::create([
                                                                'reservation_id' => $request->get('id'),
                                                                'promotion_id' => $promotion->id,
                                                                'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                'promotion_amount' => $amount,
                                                                'promotion_amount_total' => $promotion_amount_total,
                                                                'promotion_name' => $promotion_translate->name,
                                                                'promotion_short_description' => $promotion_translate->short_description,
                                                                'promotion_order' => $promotion->order,
                                                                'promotion_type_id' => $promotion_type_id,
                                                                'promotion_type_name' => $promotion_type_translation,
                                                                'coupon_code' => $coupon_code,
                                                                'benefit_card_id' => $benefit_card_id,
                                                                'benefit_card_number' => $benefit_card_number,
                                                                'benefit_card_name' => $benefit_card_name,
                                                                'code' => $promotion->code,
                                                                'currency_id' => $currency_id,
                                                            ]);
                                                        }
                                                        $count_tipos--;
                                                    }
    
                                                    $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                        ->whereIn('client_type_id', $array_client_types_ids)
                                                        ->whereNotIn('id', $array_tipos_cliente)
                                                        ->orderBy('client_type_id', 'ASC')
                                                        ->limit($product_type->series_one)
                                                        ->get();
    
                                                    if (count($reservation_detail_client_type) < $product_type->series_one) {
                                                        $repetir_tipos = 0;
                                                    }
    
                                                } else {
                                                    $repetir_tipos = 0;
                                                }
                                            }
                                        } else {
    
    
                                            if (!is_null($product_type->min_quantity)) {
                                                $sql_reservation_clone
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                                            }
    
                                            if (!is_null($product_type->max_quantity)) {
                                                $sql_reservation_clone
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity));
                                            }
    
                                            foreach ($promotions_client_type as $promotion_client_type) {
                                                $datos_reservation_client_type = null;
    
                                                $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                $sql_reservation_clone_clone
                                                    ->where('mo_reservation_detail_client_type.client_type_id', $promotion_client_type->client_type_id)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types'),
                                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                    IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                    IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                    IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                    IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                    IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                    IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                    IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                    IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                    IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                    IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                    mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                                    )->leftJoin('mo_reservation_detail_activity', function ($join) {
                                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                                        $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_event', function ($join) {
                                                        $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_event.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_package', function ($join) {
                                                        $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_package.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_park', function ($join) {
                                                        $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_park.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                                        $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                                        $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_tour.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                                        $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                                        $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_photo.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_food', function ($join) {
                                                        $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_food.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                                        $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                                        $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                                    });
    
                                                $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                                if (count($promotion_enjoy) > 0) {
    
    
                                                    foreach ($promotion_enjoy as $enjoy) {
                                                        $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
    
                                                        if (is_null($datos_reservation_client_type)) {
    
                                                            $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                                ->mergeBindings($sql_reservation_enjoy)
                                                                ->where(function ($query) use ($enjoy) {
                                                                    $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                                        ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                                });
    
                                                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                                $sql_reservation_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                                    ->where('date_aux', '<=', $fecha_fin);
                                                            }
    
                                                            $products_enjoy = $sql_reservation_enjoy->get();
    
                                                            foreach ($products_enjoy as $product_enjoy) {
                                                                $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                                if ($promotion_enjoy) {
                                                                    $datos_reservation_client_type = $product_enjoy;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                                        ->mergeBindings($sql_reservation_clone_clone);
    
                                                    if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                        $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                        $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                        $sql_reservation_clone_clone->where('date_aux', '>=', $fecha_inicio)
                                                            ->where('date_aux', '<=', $fecha_fin);
                                                    }
    
                                                    $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                                }
    
                                                if ($datos_reservation_client_type) {
    
                                                    $array_detalles[] = $datos_reservation_client_type->id;
    
                                                    if (!is_null($product_type->promotion_amount) && ($product_type->promotion_type_id != 4 && $product_type->promotion_type_id != 5)) {
    
                                                        $amount = $product_type->promotion_amount;
    
                                                        $promotion_type_id = null;
                                                        $promotion_type_translation = null;
                                                        $currency_id = null;
    
                                                        $promotion_amount_total = 0;
                                                        $promotion_type_id = $product_type->promotion_type_id;
                                                        $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product_type->promotion_type_id)
                                                            ->where('language_id', $idioma->id)
                                                            ->first()->name;
                                                        $currency_id = $product_type->currency_id;
    
                                                        $sql_clientes_detail = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                            ->where('client_type_id', $promotion_client_type->client_type_id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $tipos_clientes_detail = $sql_clientes_detail->get();
    
                                                        foreach ($tipos_clientes_detail as $tipo_cliente_detail) {
                                                            //obtener array de ids de tipos de cliente
                                                            $array_client_types_ids = PromotionProductTypeClientType::where('promotion_product_type_id', $product_type->id)->pluck('client_type_id')->toArray();
    
                                                            $sql_reservation_clone = clone $sql_reservation_detail;
    
                                                            if ($product_type->promotion_type_id == 1) {
                                                                $sql_reservation_clone_clone = clone $sql_reservation_clone;
                                                                $datos_suma = $sql_reservation_clone_clone
                                                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                                    ->where('mo_reservation_detail.id', $datos_reservation_client_type->id)
                                                                    ->where('mo_reservation_detail_client_type.id', $tipo_cliente_detail->id)
                                                                    ->sum('mo_reservation_detail_client_type.sale_price');
    
                                                                $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                                    ->whereNull('mo_reservation_promotion.deleted_at')
                                                                    ->where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                    ->where('reservation_detail_client_type_id', $tipo_cliente_detail->id)
                                                                    ->get();
    
                                                                foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                                    if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    if ($dato_promocion->promotion_type_id == 3) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                                }
    
                                                                $promotion_amount_total = $datos_suma * ($product_type->promotion_amount / 100);
    
                                                            }
    
                                                            if ($product_type->promotion_type_id == 2) {
                                                                $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                                $datos_reservation_currency = $sql_reservation_currency
                                                                    ->select(
                                                                        'mo_reservation_detail.currency_id'
                                                                    )->first();
    
    
                                                                if ($product_type->currency_id != $datos_reservation_currency->currency_id) {
                                                                    $promotion_amount_total = conversor($product_type->promotion_amount, $product_type->currency_id, $datos_reservation_currency->currency_id);
                                                                } else {
    
                                                                    $promotion_amount_total = $product_type->promotion_amount;
                                                                }
    
                                                            }
    
                                                            if ($product_type->promotion_type_id == 3) {
                                                                $sql_reservation_clone_clone = clone $sql_reservation_clone;
                                                                $datos_suma = $sql_reservation_clone_clone
                                                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                                    ->where('mo_reservation_detail.id', $datos_reservation_client_type->id)
                                                                    ->where('mo_reservation_detail_client_type.id', $tipo_cliente_detail->id)
                                                                    ->sum('mo_reservation_detail_client_type.sale_price');
    
                                                                $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                                    ->whereNull('mo_reservation_promotion.deleted_at')
                                                                    ->where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                    ->where('reservation_detail_client_type_id', $tipo_cliente_detail->id)
                                                                    ->get();
    
                                                                foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                                    if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    if ($dato_promocion->promotion_type_id == 3) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                                }
    
                                                                $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                                $datos_reservation_currency = $sql_reservation_currency
                                                                    ->select(
                                                                        'mo_reservation_detail.currency_id'
                                                                    )->first();
    
    
                                                                if ($product_type->currency_id != $datos_reservation_currency->currency_id) {
                                                                    $descuento = $datos_suma - conversor($product_type->promotion_amount, $product_type->currency_id, $datos_reservation_currency->currency_id);
                                                                    $promotion_amount_total = $descuento;
                                                                } else {
                                                                    $descuento = $datos_suma - $product_type->promotion_amount;
                                                                    $promotion_amount_total = $descuento;
                                                                }
                                                            }
    
    
                                                            ReservationPromotion::create([
                                                                'reservation_id' => $request->get('id'),
                                                                'promotion_id' => $promotion->id,
                                                                'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                'reservation_detail_client_type_id' => $tipo_cliente_detail->id,
                                                                'promotion_amount' => $amount,
                                                                'promotion_amount_total' => $promotion_amount_total,
                                                                'promotion_name' => $promotion_translate->name,
                                                                'promotion_short_description' => $promotion_translate->short_description,
                                                                'promotion_order' => $promotion->order,
                                                                'promotion_type_id' => $promotion_type_id,
                                                                'promotion_type_name' => $promotion_type_translation,
                                                                'coupon_code' => $coupon_code,
                                                                'benefit_card_id' => $benefit_card_id,
                                                                'benefit_card_number' => $benefit_card_number,
                                                                'benefit_card_name' => $benefit_card_name,
                                                                'code' => $promotion->code,
                                                                'currency_id' => $currency_id,
                                                            ]);
    
    //                                                    $array_client_types[] = $datos_reservation_client_type->detail_client_type_id;
                                                        }
                                                    } else {
    
                                                        if($product_type->promotion_type_id == 4 || $product_type->promotion_type_id == 5) {
                                                            $promotion_type_id = null;
                                                            $promotion_type_translation = null;
    
                                                            $promotion_type_id = $product_type->promotion_type_id;
    
                                                            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product_type->promotion_type_id)
                                                                ->where('language_id', $idioma->id)
                                                                ->first()->name;
    
                                                            $sql_clientes_detail = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                ->where('client_type_id', $promotion_client_type->client_type_id);
    
                                                            if ($promotion->apply_country_filter == 1) {
                                                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                                $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                            }
    
                                                            if ($promotion->apply_state_filter == 1) {
                                                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                                $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                            }
    
                                                            $tipos_clientes_detail = $sql_clientes_detail->get();
    
                                                            foreach ($tipos_clientes_detail as $tipo_cliente_detail) {
    
                                                                ReservationPromotion::create([
                                                                    'reservation_id' => $request->get('id'),
                                                                    'promotion_id' => $promotion->id,
                                                                    'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                    'reservation_detail_client_type_id' => $tipo_cliente_detail->id,
                                                                    'promotion_name' => $promotion_translate->name,
                                                                    'promotion_short_description' => $promotion_translate->short_description,
                                                                    'promotion_order' => $promotion->order,
                                                                    'promotion_type_id' => $promotion_type_id,
                                                                    'promotion_type_name' => $promotion_type_translation,
                                                                    'coupon_code' => $coupon_code,
                                                                    'benefit_card_id' => $benefit_card_id,
                                                                    'benefit_card_number' => $benefit_card_number,
                                                                    'benefit_card_name' => $benefit_card_name,
                                                                    'code' => $promotion->code,
                                                                ]);
    
    //                                                    $array_client_types[] = $datos_reservation_client_type->detail_client_type_id;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
    
                                    $sql_reservation_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                        ->whereNull('mo_reservation_detail.service_id')
                                        ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                        ->whereNull('mo_product.deleted_at')
                                        ->where('mo_product.type_id', $product_type->product_type_id)
                                        ->whereNotIn('mo_reservation_detail.id', $array_detalles)
                                        ->groupBy('mo_reservation_detail.id')
                                        ->select(
                                            'mo_reservation_detail.id'
                                        );
    
                                    if ($promotion->apply_country_filter == 1) {
                                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                        $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                    }
    
                                    if ($promotion->apply_state_filter == 1) {
                                        $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                        $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                    }
    
                                    $datos_reservation_detail = null;
                                    //Promociones por cantidad de pax por tipo de producto
                                    if (!is_null($product_type->min_quantity) && is_null($product_type->max_quantity)) {
    
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
    
                                    }
    
                                    if (!is_null($product_type->max_quantity) && is_null($product_type->min_quantity)) {
    
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity));
                                    }
    
    
                                    if (!is_null($product_type->max_quantity) && !is_null($product_type->min_quantity)) {
    
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity))
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                                    }
    
    
                                    if (is_null($product_type->max_quantity) && is_null($product_type->min_quantity)) {
    
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            );
                                    }
    
                                    $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                    $sql_reservation_clone->addSelect(
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux'));
    
                                    if (count($promotion_enjoy) > 0) {
    
    
                                        $datos_reservation_detail = null;
    
    
                                        foreach ($promotion_enjoy as $enjoy) {
                                            $sql_reservation_enjoy = clone $sql_reservation_clone;
    
                                            if (is_null($datos_reservation_detail)) {
    
                                                $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_enjoy)
                                                    ->where(function ($query) use ($enjoy) {
                                                        $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                            ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                    });
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
                                                    $sql_reservation_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                        ->where('date_aux', '<=', $fecha_fin);
                                                }
    
                                                $products_enjoy = $sql_reservation_enjoy->get();
    
                                                foreach ($products_enjoy as $product_enjoy) {
                                                    $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                    $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                    if ($promotion_enjoy) {
                                                        $datos_reservation_detail = $product_enjoy;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
    
                                        $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_clone)
                                            ->orderBy('total_client_types', 'DESC');
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_clone->where('date_aux', '>=', $fecha_inicio)
                                                ->where('date_aux', '<=', $fecha_fin);
                                        }
    
                                        $datos_reservation_detail = $sql_reservation_clone->first();
                                    }
    
                                    $array_detalles[] = $datos_reservation_detail->id;
                                    if (!is_null($product_type->series_one) && !is_null($product_type->series_two)) {
                                        $repetir_tipos = 1;
                                        $array_tipos_cliente = array();
                                        while ($repetir_tipos) {
                                            $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_detail->id)
                                                ->whereNotIn('id', $array_tipos_cliente)
                                                ->orderBy('client_type_id', 'ASC')
                                                ->limit($product_type->series_one)
                                                ->get();
                                            if (count($reservation_detail_client_type) > 0) {
                                                $count_tipos = $product_type->series_one;
                                                foreach ($reservation_detail_client_type as $detail_client_type) {
    
                                                    $array_tipos_cliente[] = $detail_client_type->id;
    
                                                    if ($count_tipos <= $product_type->series_one - $product_type->series_two) {
    
                                                        $promotion_amount_total = $detail_client_type->sale_price;
    
                                                        $amount = 100;
    
                                                        $promotion_type_id = null;
                                                        $promotion_type_translation = null;
                                                        $currency_id = null;
    
                                                        $promotion_type_id = 1;
                                                        $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', 1)
                                                            ->where('language_id', $idioma->id)
                                                            ->first()->name;
    
                                                        ReservationPromotion::create([
                                                            'reservation_id' => $request->get('id'),
                                                            'promotion_id' => $promotion->id,
                                                            'reservation_detail_id' => $datos_reservation_detail->id,
                                                            'promotion_amount' => $amount,
                                                            'promotion_amount_total' => $promotion_amount_total,
                                                            'promotion_name' => $promotion_translate->name,
                                                            'promotion_short_description' => $promotion_translate->short_description,
                                                            'promotion_order' => $promotion->order,
                                                            'promotion_type_id' => $promotion_type_id,
                                                            'promotion_type_name' => $promotion_type_translation,
                                                            'coupon_code' => $coupon_code,
                                                            'benefit_card_id' => $benefit_card_id,
                                                            'benefit_card_number' => $benefit_card_number,
                                                            'benefit_card_name' => $benefit_card_name,
                                                            'code' => $promotion->code,
                                                            'currency_id' => $currency_id,
                                                        ]);
                                                    }
                                                    $count_tipos--;
                                                }
    
                                                $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_detail->id)
                                                    ->whereNotIn('id', $array_tipos_cliente)
                                                    ->orderBy('client_type_id', 'ASC')
                                                    ->limit($product_type->series_one)
                                                    ->get();
    
                                                if (count($reservation_detail_client_type) < $product_type->series_one) {
                                                    $repetir_tipos = 0;
                                                }
                                            } else {
                                                $repetir_tipos = 0;
                                            }
                                        }
                                    } else {
    
                                        if (!is_null($product_type->promotion_amount) && ($product_type->promotion_type_id != 4 && $product_type->promotion_type_id != 5)) {
    
                                            $amount = $product_type->promotion_amount;
    
                                            $promotion_type_id = null;
                                            $promotion_type_translation = null;
                                            $currency_id = null;
                                            $promotion_amount_total = 0;
                                            if (!is_null($product_type->promotion_type_id)) {
                                                $promotion_type_id = $product_type->promotion_type_id;
                                                $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product_type->promotion_type_id)
                                                    ->where('language_id', $idioma->id)
                                                    ->first()->name;
                                                $currency_id = $product_type->currency_id;
                                                if (!is_null($product_type->promotion_amount)) {
                                                    $sql_reservation_clone = clone $sql_reservation_detail;
    
                                                    if ($product_type->promotion_type_id == 1) {
                                                        $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                        $sql_reservation_clone_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                            ->where('mo_reservation_detail.id', $datos_reservation_detail->id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $datos_suma = $sql_reservation_clone_clone->sum('mo_reservation_detail_client_type.sale_price');
    
                                                        $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                                            ->where('reservation_detail_id', $datos_reservation_detail->id)
                                                            ->orderBy('promotion_type_id', 'asc')
                                                            ->get();
    
                                                        foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            if ($dato_promocion->promotion_type_id == 3) {
                                                                $datos_suma = $dato_promocion->promotion_amount_total;
                                                            }
                                                            $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                        }
                                                        $promotion_amount_total = $datos_suma * ($product_type->promotion_amount / 100);
                                                    }
    
                                                    if ($product_type->promotion_type_id == 2) {
                                                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                        $datos_reservation_currency = $sql_reservation_currency
                                                            ->select(
                                                                'mo_reservation_detail.currency_id'
                                                            )->first();
    
                                                        if ($product_type->currency_id != $datos_reservation_currency->currency_id) {
                                                            $promotion_amount_total = conversor($product_type->promotion_amount, $product_type->currency_id, $datos_reservation_currency->currency_id);
                                                        } else {
                                                            $promotion_amount_total = $product_type->promotion_amount;
                                                        }
                                                    }
    
                                                    if ($product_type->promotion_type_id == 3) {
    
                                                        $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                        $sql_reservation_clone_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                            ->where('mo_reservation_detail.id', $datos_reservation_detail->id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $datos_suma = $sql_reservation_clone_clone->sum('mo_reservation_detail_client_type.sale_price');
    
                                                        $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                            ->where('reservation_detail_id', $datos_reservation_detail->id)
                                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                                            ->orderBy('promotion_type_id', 'asc')
                                                            ->get();
    
                                                        foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            if ($dato_promocion->promotion_type_id == 3) {
                                                                $datos_suma = $dato_promocion->promotion_amount_total;
                                                            }
                                                            $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                        }
    
                                                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                        $datos_reservation_currency = $sql_reservation_currency
                                                            ->select(
                                                                'mo_reservation_detail.currency_id'
                                                            )->first();
    
                                                        if ($product_type->currency_id != $datos_reservation_currency->currency_id) {
                                                            $descuento = $datos_suma - conversor($product_type->promotion_amount, $product_type->currency_id, $datos_reservation_currency->currency_id);
                                                            $promotion_amount_total = $descuento;
                                                        } else {
                                                            $descuento = $datos_suma - $product_type->promotion_amount;
                                                            $promotion_amount_total = $descuento;
                                                        }
    
    
                                                    }
                                                }
                                            } else {
                                                $promotion_type_id = $promotion->promotion_type_id;
                                                $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                                                    ->where('language_id', $idioma->id)
                                                    ->first()->name;
    
                                                $currency_id = $promotion->currency_id;
                                            }
    
                                            ReservationPromotion::create([
                                                'reservation_id' => $request->get('id'),
                                                'promotion_id' => $promotion->id,
                                                'reservation_detail_id' => $datos_reservation_detail->id,
                                                'promotion_amount' => $amount,
                                                'promotion_amount_total' => $promotion_amount_total,
                                                'promotion_name' => $promotion_translate->name,
                                                'promotion_short_description' => $promotion_translate->short_description,
                                                'promotion_order' => $promotion->order,
                                                'promotion_type_id' => $promotion_type_id,
                                                'promotion_type_name' => $promotion_type_translation,
                                                'coupon_code' => $coupon_code,
                                                'benefit_card_id' => $benefit_card_id,
                                                'benefit_card_number' => $benefit_card_number,
                                                'benefit_card_name' => $benefit_card_name,
                                                'code' => $promotion->code,
                                                'currency_id' => $currency_id,
                                            ]);
                                        } else {
    
                                            if ($product_type->promotion_type_id == 4 || $product_type->promotion_type_id == 5) {
                                                if (!is_null($product_type->promotion_type_id)) {
                                                    $promotion_type_id = $product_type->promotion_type_id;
                                                    $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $product_type->promotion_type_id)
                                                        ->where('language_id', $idioma->id)
                                                        ->first()->name;
                                                } else {
                                                    $promotion_type_id = $promotion->promotion_type_id;
                                                    $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                                                        ->where('language_id', $idioma->id)
                                                        ->first()->name;
                                                }
    
                                                ReservationPromotion::create([
                                                    'reservation_id' => $request->get('id'),
                                                    'promotion_id' => $promotion->id,
                                                    'reservation_detail_id' => $datos_reservation_detail->id,
                                                    'promotion_name' => $promotion_translate->name,
                                                    'promotion_short_description' => $promotion_translate->short_description,
                                                    'promotion_order' => $promotion->order,
                                                    'promotion_type_id' => $promotion_type_id,
                                                    'promotion_type_name' => $promotion_type_translation,
                                                    'coupon_code' => $coupon_code,
                                                    'benefit_card_id' => $benefit_card_id,
                                                    'benefit_card_number' => $benefit_card_number,
                                                    'benefit_card_name' => $benefit_card_name,
                                                    'code' => $promotion->code,
                                                ]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
    
                        $array_detalles_repetir = array_merge($array_detalles_repetir, $array_detalles);
    
                        $array_excluidos = array_merge($array_excluidos, $array_detalles);
                    }
    
                    if ($promotion->apply_service_filter == 1) {
    
                        $promotion_service = PromotionService::where('promotion_id', $promotion->id)->get();
    
                        foreach ($promotion_service as $service) {
    
                            if(!is_null($service->promotion_amount) || (!is_null($service->series_one) && !is_null($service->series_two)) || ($service->promotion_type_id == 4 || $service->promotion_type_id == 5 || is_null($service->promotion_type_id))) {
                                $sql_reservation_clone = clone $sql_reservation_detail;
    
                                if ($service->apply_client_type_filter == 1) {
    
                                    $promotions_client_type = PromotionServiceClientType::where('promotion_service_id', $service->id)
                                        ->groupBy('client_type_id')
                                        ->get();
    
                                    if (count($promotions_client_type) > 0) {
                                        $sql_reservation_clone = clone $sql_reservation;
    
                                        $sql_reservation_clone
                                            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                            ->where('mo_reservation_detail.service_id', $service->service_id)
                                            ->groupBy('mo_reservation_detail.id')
                                            ->whereNotIn('mo_reservation_detail.id', $array_detalles);
    
                                        if ($promotion->apply_country_filter == 1) {
                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                        }
    
                                        if ($promotion->apply_state_filter == 1) {
                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                            $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                        }
    
                                        if (!is_null($service->series_one) && !is_null($service->series_two)) {
                                            $datos_reservation_client_type = null;
    
                                            $array_client_types_ids = PromotionServiceClientType::where('promotion_service_id', $service->id)->pluck('client_type_id')->toArray();
    
                                            $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                            if (!is_null($service->min_quantity) && is_null($service->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->min_quantity));
                                            }
    
                                            if (!is_null($service->max_quantity) && is_null($service->min_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
                                            }
    
                                            if (!is_null($service->min_quantity) && !is_null($service->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    )
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity))
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity));
                                            }
    
                                            if (is_null($service->min_quantity) && is_null($service->max_quantity)) {
                                                $sql_reservation_clone_clone
                                                    ->whereIn('mo_reservation_detail_client_type.client_type_id', $array_client_types_ids)
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                                    );
                                            }
    
                                            $sql_reservation_clone_clone->addSelect(
                                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                            )->leftJoin('mo_reservation_detail_activity', function ($join) {
                                                $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                                    ->whereNull('mo_reservation_detail_activity.deleted_at');
                                            })
                                                ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                                    $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_event', function ($join) {
                                                    $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_event.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_package', function ($join) {
                                                    $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_package.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_park', function ($join) {
                                                    $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_park.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                                    $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                                    $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_tour.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                                    $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                                    $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_photo.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_food', function ($join) {
                                                    $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_food.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                                    $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                                })
                                                ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                                    $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                        ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                                });
    
                                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                            if (count($promotion_enjoy) > 0) {
    
    
                                                foreach ($promotion_enjoy as $enjoy) {
                                                    $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
                                                    if (is_null($datos_reservation_client_type)) {
    
                                                        $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                            ->mergeBindings($sql_reservation_enjoy)
                                                            ->where(function ($query) use ($enjoy) {
                                                                $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                                    ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                            });
    
                                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                            $sql_reservation_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                                ->where('date_aux', '<=', $fecha_fin);
                                                        }
    
                                                        $products_enjoy = $sql_reservation_enjoy->get();
    
                                                        foreach ($products_enjoy as $product_enjoy) {
                                                            $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                            $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                            if ($promotion_enjoy) {
                                                                $datos_reservation_client_type = $product_enjoy;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
    
                                                $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_clone_clone)
                                                    ->orderBy('total_client_types', 'DESC');
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_clone_clone->where('date_aux', '>=', $fecha_inicio)
                                                        ->where('date_aux', '<=', $fecha_fin);
                                                }
    
                                                $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                            }
    
                                            $array_detalles[] = $datos_reservation_client_type->id;
    
                                            $repetir_tipos = 1;
                                            $array_tipos_cliente = array();
    
                                            while ($repetir_tipos) {
                                                $sql_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                    ->whereIn('client_type_id', $array_client_types_ids)
                                                    ->whereNotIn('id', $array_tipos_cliente)
                                                    ->orderBy('client_type_id', 'ASC')
                                                    ->limit($service->series_one);
    
                                                if ($promotion->apply_country_filter == 1) {
                                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                    $sql_detail_client_type->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                }
    
                                                if ($promotion->apply_state_filter == 1) {
                                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                    $sql_detail_client_type->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                }
    
                                                $reservation_detail_client_type = $sql_detail_client_type->get();
    
                                                if (count($reservation_detail_client_type) > 0) {
                                                    $count_tipos = $service->series_one;
    
                                                    foreach ($reservation_detail_client_type as $detail_client_type) {
    
                                                        $array_tipos_cliente[] = $detail_client_type->id;
    
                                                        if ($count_tipos <= $service->series_one - $service->series_two) {
    
                                                            $promotion_amount_total = $detail_client_type->sale_price;
    
                                                            $amount = 100;
    
                                                            $promotion_type_id = null;
                                                            $promotion_type_translation = null;
                                                            $currency_id = null;
    
                                                            $promotion_type_id = 1;
                                                            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', 1)
                                                                ->where('language_id', $idioma->id)
                                                                ->first()->name;
    
                                                            ReservationPromotion::create([
                                                                'reservation_id' => $request->get('id'),
                                                                'promotion_id' => $promotion->id,
                                                                'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                'promotion_amount' => $amount,
                                                                'promotion_amount_total' => $promotion_amount_total,
                                                                'promotion_name' => $promotion_translate->name,
                                                                'promotion_short_description' => $promotion_translate->short_description,
                                                                'promotion_order' => $promotion->order,
                                                                'promotion_type_id' => $promotion_type_id,
                                                                'promotion_type_name' => $promotion_type_translation,
                                                                'coupon_code' => $coupon_code,
                                                                'benefit_card_id' => $benefit_card_id,
                                                                'benefit_card_number' => $benefit_card_number,
                                                                'benefit_card_name' => $benefit_card_name,
                                                                'code' => $promotion->code,
                                                                'currency_id' => $currency_id,
                                                            ]);
                                                        }
                                                        $count_tipos--;
                                                    }
    
                                                    $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                        ->whereIn('client_type_id', $array_client_types_ids)
                                                        ->whereNotIn('id', $array_tipos_cliente)
                                                        ->orderBy('client_type_id', 'ASC')
                                                        ->limit($service->series_one)
                                                        ->get();
    
                                                    if (count($reservation_detail_client_type) < $service->series_one) {
                                                        $repetir_tipos = 0;
                                                    }
    
                                                } else {
                                                    $repetir_tipos = 0;
                                                }
                                            }
                                        } else {
                                            if (!is_null($service->min_quantity)) {
                                                $sql_reservation_clone
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->min_quantity));
                                            }
    
                                            if (!is_null($service->max_quantity)) {
                                                $sql_reservation_clone
                                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
                                            }
    
                                            foreach ($promotions_client_type as $promotion_client_type) {
                                                $datos_reservation_client_type = null;
    
                                                $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                $sql_reservation_clone_clone
                                                    ->select(
                                                        'mo_reservation_detail.id',
                                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types'),
                                                        'mo_reservation_detail_client_type.id as detail_client_type_id',
                                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                                    )
                                                    ->where('mo_reservation_detail_client_type.client_type_id', $promotion_client_type->client_type_id)
                                                    ->leftJoin('mo_reservation_detail_activity', function ($join) {
                                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                                        $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_event', function ($join) {
                                                        $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_event.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_package', function ($join) {
                                                        $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_package.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_park', function ($join) {
                                                        $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_park.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                                        $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                                        $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_tour.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                                        $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                                        $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_photo.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_food', function ($join) {
                                                        $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_food.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                                        $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                                    })
                                                    ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                                        $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                                    });
    
                                                $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                                if (count($promotion_enjoy) > 0) {
    
                                                    foreach ($promotion_enjoy as $enjoy) {
                                                        $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
                                                        if (is_null($datos_reservation_client_type)) {
    
                                                            $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                                ->mergeBindings($sql_reservation_enjoy)
                                                                ->where(function ($query) use ($enjoy) {
                                                                    $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                                        ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                                });
    
                                                            if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                                $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                                $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                                $sql_reservation_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                                    ->where('date_aux', '<=', $fecha_fin);
                                                            }
    
                                                            $products_enjoy = $sql_reservation_enjoy->get();
    
                                                            foreach ($products_enjoy as $product_enjoy) {
                                                                $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                                if ($promotion_enjoy) {
                                                                    $datos_reservation_client_type = $product_enjoy;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
    
                                                    $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                                        ->mergeBindings($sql_reservation_clone_clone);
    
                                                    if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                        $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                        $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                        $sql_reservation_clone_clone->where('date_aux', '>=', $fecha_inicio)
                                                            ->where('date_aux', '<=', $fecha_fin);
                                                    }
    
                                                    $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                                }
    
                                                if ($datos_reservation_client_type) {
    
                                                    $array_detalles[] = $datos_reservation_client_type->id;
    
                                                    if (!is_null($service->promotion_amount) && ($service->promotion_type_id != 4 && $service->promotion_type_id != 5)) {
    
                                                        $amount = $service->promotion_amount;
    
                                                        $promotion_type_id = null;
                                                        $promotion_type_translation = null;
                                                        $currency_id = null;
                                                        $promotion_amount_total = 0;
                                                        $promotion_type_id = $service->promotion_type_id;
                                                        $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $service->promotion_type_id)
                                                            ->where('language_id', $idioma->id)
                                                            ->first()->name;
                                                        $currency_id = $service->currency_id;
    
                                                        $sql_clientes_detail = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                            ->where('client_type_id', $promotion_client_type->client_type_id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $tipos_clientes_detail = $sql_clientes_detail->get();
    
    
                                                        foreach ($tipos_clientes_detail as $tipo_cliente_detail) {
                                                            //obtener array de ids de tipos de cliente
                                                            $sql_reservation_clone = clone $sql_reservation_detail;
    
                                                            if ($service->promotion_type_id == 1) {
                                                                $sql_reservation_clone_clone = clone $sql_reservation_clone;
                                                                $datos_suma = $sql_reservation_clone_clone
                                                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                                    ->where('mo_reservation_detail.id', $datos_reservation_client_type->id)
                                                                    ->where('mo_reservation_detail_client_type.id', $tipo_cliente_detail->id)
                                                                    ->sum('mo_reservation_detail_client_type.sale_price');
    
                                                                $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                                    ->where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                    ->where('reservation_detail_client_type_id', $tipo_cliente_detail->id)
                                                                    ->whereNull('mo_reservation_promotion.deleted_at')
                                                                    ->orderBy('promotion_type_id', 'asc')
                                                                    ->get();
                                                                foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                                    if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    if ($dato_promocion->promotion_type_id == 3) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                                }
    
                                                                $promotion_amount_total = $datos_suma * ($service->promotion_amount / 100);
    
                                                            }
    
                                                            if ($service->promotion_type_id == 2) {
                                                                $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                                $datos_reservation_currency = $sql_reservation_currency
                                                                    ->select(
                                                                        'mo_reservation_detail.currency_id'
                                                                    )->first();
    
                                                                if ($service->currency_id != $datos_reservation_currency->currency_id) {
                                                                    $promotion_amount_total = conversor($service->promotion_amount, $service->currency_id, $datos_reservation_currency->currency_id);
                                                                } else {
                                                                    $promotion_amount_total = $service->promotion_amount;
                                                                }
    
                                                            }
    
                                                            if ($service->promotion_type_id == 3) {
    
                                                                $sql_reservation_clone_clone = clone $sql_reservation_clone;
                                                                $datos_suma = $sql_reservation_clone_clone
                                                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                                    ->where('mo_reservation_detail.id', $datos_reservation_client_type->id)
                                                                    ->where('mo_reservation_detail_client_type.id', $tipo_cliente_detail->id)
                                                                    ->sum('mo_reservation_detail_client_type.sale_price');
    
                                                                $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                                    ->where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                    ->where('reservation_detail_client_type_id', $tipo_cliente_detail->id)
                                                                    ->whereNull('mo_reservation_promotion.deleted_at')
                                                                    ->orderBy('promotion_type_id', 'asc')
                                                                    ->get();
    
                                                                foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                                    if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    if ($dato_promocion->promotion_type_id == 3) {
                                                                        $datos_suma -= $dato_promocion->promotion_amount_total;
                                                                    }
    
                                                                    $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                                }
    
                                                                $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                                $datos_reservation_currency = $sql_reservation_currency
                                                                    ->select(
                                                                        'mo_reservation_detail.currency_id'
                                                                    )->first();
    
                                                                if ($service->currency_id != $datos_reservation_currency->currency_id) {
                                                                    $descuento = $datos_suma - conversor($service->promotion_amount, $service->currency_id, $datos_reservation_currency->currency_id);
                                                                    $promotion_amount_total = $descuento;
                                                                } else {
                                                                    $descuento = $datos_suma - $service->promotion_amount;
                                                                    $promotion_amount_total = $descuento;
                                                                }
                                                            }
    
                                                            ReservationPromotion::create([
                                                                'reservation_id' => $request->get('id'),
                                                                'promotion_id' => $promotion->id,
                                                                'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                'promotion_amount' => $amount,
                                                                'promotion_amount_total' => $promotion_amount_total,
                                                                'promotion_name' => $promotion_translate->name,
                                                                'promotion_short_description' => $promotion_translate->short_description,
                                                                'promotion_order' => $promotion->order,
                                                                'promotion_type_id' => $promotion_type_id,
                                                                'promotion_type_name' => $promotion_type_translation,
                                                                'coupon_code' => $coupon_code,
                                                                'benefit_card_id' => $benefit_card_id,
                                                                'benefit_card_number' => $benefit_card_number,
                                                                'benefit_card_name' => $benefit_card_name,
                                                                'code' => $promotion->code,
                                                                'currency_id' => $currency_id,
                                                            ]);
                                                        }
                                                    } else {
                                                        if ($service->promotion_type_id == 4 || $service->promotion_type_id == 5) {
    
                                                            $promotion_type_id = null;
                                                            $promotion_type_translation = null;
    
                                                            $promotion_type_id = $service->promotion_type_id;
                                                            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $service->promotion_type_id)
                                                                ->where('language_id', $idioma->id)
                                                                ->first()->name;
    
                                                            $sql_clientes_detail = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_client_type->id)
                                                                ->where('client_type_id', $promotion_client_type->client_type_id);
    
                                                            if ($promotion->apply_country_filter == 1) {
                                                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                                $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                            }
    
                                                            if ($promotion->apply_state_filter == 1) {
                                                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                                $sql_clientes_detail->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                            }
    
                                                            $tipos_clientes_detail = $sql_clientes_detail->get();
    
    
                                                            foreach ($tipos_clientes_detail as $tipo_cliente_detail) {
                                                                //obtener array de ids de tipos de cliente
    
                                                                ReservationPromotion::create([
                                                                    'reservation_id' => $request->get('id'),
                                                                    'promotion_id' => $promotion->id,
                                                                    'reservation_detail_id' => $datos_reservation_client_type->id,
                                                                    'promotion_name' => $promotion_translate->name,
                                                                    'promotion_short_description' => $promotion_translate->short_description,
                                                                    'promotion_order' => $promotion->order,
                                                                    'promotion_type_id' => $promotion_type_id,
                                                                    'promotion_type_name' => $promotion_type_translation,
                                                                    'coupon_code' => $coupon_code,
                                                                    'benefit_card_id' => $benefit_card_id,
                                                                    'benefit_card_number' => $benefit_card_number,
                                                                    'benefit_card_name' => $benefit_card_name,
                                                                    'code' => $promotion->code,
                                                                ]);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $sql_reservation_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                        ->where('mo_reservation_detail.service_id', $service->service_id)
                                        ->whereNotIn('mo_reservation_detail.id', $array_detalles)
                                        ->groupBy('mo_reservation_detail.id')
                                        ->select(
                                            'mo_reservation_detail.id'
                                        );
    
                                    $datos_reservation_detail = null;
    
                                    if ($promotion->apply_country_filter == 1) {
                                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                        $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                    }
    
                                    if ($promotion->apply_state_filter == 1) {
                                        $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                        $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                    }
    
                                    //Promociones por cantidad de pax por servicio
                                    if (!is_null($service->min_quantity) && is_null($service->max_quantity)) {
    
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity));
                                    }
    
                                    if (!is_null($service->max_quantity) && is_null($service->min_quantity)) {
    
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
                                    }
    
                                    if (!is_null($service->max_quantity) && !is_null($service->min_quantity)) {
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            )
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity))
                                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
                                    }
    
                                    if (is_null($service->max_quantity) && is_null($service->min_quantity)) {
                                        $sql_reservation_clone
                                            ->select(
                                                'mo_reservation_detail.id',
                                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                            );
                                    }
    
                                    $sql_reservation_clone->addSelect(
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                            IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                            IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                            IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                            IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                            IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                            IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                            IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                            IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                            IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                            IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                            mo_reservation_detail_hotel.date_start))))))))))) AS date_aux'));
    
                                    $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                    if (count($promotion_enjoy) > 0) {
    
    
                                        $datos_reservation_detail = null;
    
    
                                        foreach ($promotion_enjoy as $enjoy) {
                                            $sql_reservation_enjoy = clone $sql_reservation_clone;
                                            if (is_null($datos_reservation_detail)) {
    
                                                $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_enjoy)
                                                    ->where(function ($query) use ($enjoy) {
                                                        $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                            ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                    });
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_enjoy->where('date_aux', '>=', $fecha_inicio)
                                                        ->where('date_aux', '<=', $fecha_fin);
                                                }
    
                                                $products_enjoy = $sql_reservation_enjoy->get();
    
                                                foreach ($products_enjoy as $product_enjoy) {
                                                    $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                    $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                    if ($promotion_enjoy) {
                                                        $datos_reservation_detail = $product_enjoy;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_clone)
                                            ->orderBy('total_client_types', 'DESC');
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_clone->where('date_aux', '>=', $fecha_inicio)
                                                ->where('date_aux', '<=', $fecha_fin);
                                        }
    
                                        $datos_reservation_detail = $sql_reservation_clone->first();
                                    }
    
                                    $array_detalles[] = $datos_reservation_detail->id;
    
                                    if (!is_null($service->series_one) && !is_null($service->series_two)) {
                                        $repetir_tipos = 1;
                                        $array_tipos_cliente = array();
                                        while ($repetir_tipos) {
                                            $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_detail->id)
                                                ->whereNotIn('id', $array_tipos_cliente)
                                                ->orderBy('client_type_id', 'ASC')
                                                ->limit($service->series_one)
                                                ->get();
    
                                            if (count($reservation_detail_client_type) > 0) {
                                                $count_tipos = $service->series_one;
    
                                                foreach ($reservation_detail_client_type as $detail_client_type) {
    
                                                    $array_tipos_cliente[] = $detail_client_type->id;
    
                                                    if ($count_tipos <= $service->series_one - $service->series_two) {
    
                                                        $promotion_amount_total = $detail_client_type->sale_price;
    
                                                        $amount = 100;
    
                                                        $promotion_type_id = null;
                                                        $promotion_type_translation = null;
                                                        $currency_id = null;
    
                                                        $promotion_type_id = 1;
                                                        $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', 1)
                                                            ->where('language_id', $idioma->id)
                                                            ->first()->name;
    
                                                        ReservationPromotion::create([
                                                            'reservation_id' => $request->get('id'),
                                                            'promotion_id' => $promotion->id,
                                                            'reservation_detail_id' => $datos_reservation_detail->id,
                                                            'promotion_amount' => $amount,
                                                            'promotion_amount_total' => $promotion_amount_total,
                                                            'promotion_name' => $promotion_translate->name,
                                                            'promotion_short_description' => $promotion_translate->short_description,
                                                            'promotion_order' => $promotion->order,
                                                            'promotion_type_id' => $promotion_type_id,
                                                            'promotion_type_name' => $promotion_type_translation,
                                                            'coupon_code' => $coupon_code,
                                                            'benefit_card_id' => $benefit_card_id,
                                                            'benefit_card_number' => $benefit_card_number,
                                                            'benefit_card_name' => $benefit_card_name,
                                                            'code' => $promotion->code,
                                                            'currency_id' => $currency_id,
                                                        ]);
                                                    }
                                                    $count_tipos--;
                                                }
    
                                                $reservation_detail_client_type = ReservationDetailClientType::where('reservation_detail_id', $datos_reservation_detail->id)
                                                    ->whereNotIn('id', $array_tipos_cliente)
                                                    ->orderBy('client_type_id', 'ASC')
                                                    ->limit($service->series_one)
                                                    ->get();
    
                                                if (count($reservation_detail_client_type) < $service->series_one) {
                                                    $repetir_tipos = 0;
                                                }
    
                                            } else {
                                                $repetir_tipos = 0;
                                            }
                                        }
                                    } else {
                                        if (!is_null($service->promotion_amount) && ($service->promotion_type_id != 4 && $service->promotion_type_id != 5)) {
                                            $amount = $service->promotion_amount;
    
                                            $promotion_type_id = null;
                                            $promotion_type_translation = null;
                                            $currency_id = null;
                                            $promotion_amount_total = 0;
                                            if (!is_null($service->promotion_type_id)) {
                                                $promotion_type_id = $service->promotion_type_id;
                                                $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $service->promotion_type_id)
                                                    ->where('language_id', $idioma->id)
                                                    ->first()->name;
                                                $currency_id = $service->currency_id;
                                                if (!is_null($service->promotion_amount)) {
                                                    $sql_reservation_clone = clone $sql_reservation_detail;
    
                                                    if ($service->promotion_type_id == 1) {
                                                        $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                        $sql_reservation_clone_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                            ->where('mo_reservation_detail.id', $datos_reservation_detail->id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $datos_suma = $sql_reservation_clone_clone->sum('mo_reservation_detail_client_type.sale_price');
    
                                                        $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                                            ->where('reservation_detail_id', $datos_reservation_detail->id)
                                                            ->get();
    
                                                        foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            if ($dato_promocion->promotion_type_id == 3) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                        }
                                                        $promotion_amount_total = $datos_suma * ($service->promotion_amount / 100);
    
                                                    }
    
                                                    if ($service->promotion_type_id == 2) {
                                                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                        $datos_reservation_currency = $sql_reservation_currency
                                                            ->select(
                                                                'mo_reservation_detail.currency_id'
                                                            )->first();
    
                                                        if ($service->promotion_amount != $datos_reservation_currency->currency_id) {
                                                            $promotion_amount_total = conversor($service->promotion_amount, $service->currency_id, $datos_reservation_currency->currency_id);
                                                        } else {
                                                            $promotion_amount_total = $service->promotion_amount;
                                                        }
    
    
                                                    }
    
                                                    if ($service->promotion_type_id == 3) {
                                                        $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                                        $sql_reservation_clone_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                                            ->where('mo_reservation_detail.id', $datos_reservation_detail->id);
    
                                                        if ($promotion->apply_country_filter == 1) {
                                                            $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_country_id', array_column($promotion_country->toArray(), 'country_id'));
                                                        }
    
                                                        if ($promotion->apply_state_filter == 1) {
                                                            $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                                            $sql_reservation_clone_clone->whereIn('mo_reservation_detail_client_type.client_state_id', array_column($promotion_state->toArray(), 'state_id'));
                                                        }
    
                                                        $datos_suma = $sql_reservation_clone_clone->sum('mo_reservation_detail_client_type.sale_price');
    
                                                        $datos_promociones_aplicadas = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                                            ->where('reservation_detail_id', $datos_reservation_detail->id)
                                                            ->get();
    
                                                        foreach ($datos_promociones_aplicadas as $dato_promocion) {
                                                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            if ($dato_promocion->promotion_type_id == 3) {
                                                                $datos_suma -= $dato_promocion->promotion_amount_total;
                                                            }
    
                                                            $datos_suma = number_format($datos_suma, $settings_decimales, '.', '');
                                                        }
    
                                                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                                                        $datos_reservation_currency = $sql_reservation_currency
                                                            ->select(
                                                                'mo_reservation_detail.currency_id'
                                                            )->first();
    
                                                        if ($service->promotion_amount != $datos_reservation_currency->currency_id) {
                                                            $descuento = $datos_suma - conversor($service->promotion_amount, $service->currency_id, $datos_reservation_currency->currency_id);
                                                            $promotion_amount_total = $descuento;
                                                        } else {
                                                            $descuento = $datos_suma - $service->promotion_amount;
                                                            $promotion_amount_total = $descuento;
                                                        }
                                                    }
                                                }
                                            } else {
                                                $promotion_type_id = $promotion->promotion_type_id;
                                                $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                                                    ->where('language_id', $idioma->id)
                                                    ->first()->name;
    
                                                $currency_id = $promotion->currency_id;
                                            }
    
    
                                            ReservationPromotion::create([
                                                'reservation_id' => $request->get('id'),
                                                'promotion_id' => $promotion->id,
                                                'reservation_detail_id' => $datos_reservation_detail->id,
                                                'promotion_amount' => $amount,
                                                'promotion_amount_total' => $promotion_amount_total,
                                                'promotion_name' => $promotion_translate->name,
                                                'promotion_short_description' => $promotion_translate->short_description,
                                                'promotion_order' => $promotion->order,
                                                'promotion_type_id' => $promotion_type_id,
                                                'promotion_type_name' => $promotion_type_translation,
                                                'coupon_code' => $coupon_code,
                                                'benefit_card_id' => $benefit_card_id,
                                                'benefit_card_number' => $benefit_card_number,
                                                'benefit_card_name' => $benefit_card_name,
                                                'code' => $promotion->code,
                                                'currency_id' => $currency_id,
                                            ]);
                                        } else {
                                            if ($service->promotion_type_id == 4 || $service->promotion_type_id == 5) {
    
                                                $promotion_type_id = null;
                                                $promotion_type_translation = null;
                                                if (!is_null($service->promotion_type_id)) {
                                                    $promotion_type_id = $service->promotion_type_id;
                                                    $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $service->promotion_type_id)
                                                        ->where('language_id', $idioma->id)
                                                        ->first()->name;
                                                } else {
                                                    $promotion_type_id = $promotion->promotion_type_id;
                                                    $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                                                        ->where('language_id', $idioma->id)
                                                        ->first()->name;
                                                }
    
    
                                                ReservationPromotion::create([
                                                    'reservation_id' => $request->get('id'),
                                                    'promotion_id' => $promotion->id,
                                                    'reservation_detail_id' => $datos_reservation_detail->id,
                                                    'promotion_name' => $promotion_translate->name,
                                                    'promotion_short_description' => $promotion_translate->short_description,
                                                    'promotion_order' => $promotion->order,
                                                    'promotion_type_id' => $promotion_type_id,
                                                    'promotion_type_name' => $promotion_type_translation,
                                                    'coupon_code' => $coupon_code,
                                                    'benefit_card_id' => $benefit_card_id,
                                                    'benefit_card_number' => $benefit_card_number,
                                                    'benefit_card_name' => $benefit_card_name,
                                                    'code' => $promotion->code,
                                                ]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
    
                        $array_detalles_repetir = array_merge($array_detalles_repetir, $array_detalles);
    
                        $array_excluidos = array_merge($array_excluidos, $array_detalles);
                    }
    
                    foreach ($promotion_product as $product) {
    
                        if ($product->apply_client_type_filter == 1) {
    
                            $promotion_client_type = PromotionProductClientType::where('promotion_product_id', $product->id)
                                ->get();
    
                            if (count($promotion_client_type)) {
    
                                $sql_reservation_clone = clone $sql_reservation;
    
                                $sql_reservation_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir)
                                    ->groupBy('mo_reservation_detail.id')
                                    ->where('mo_reservation_detail.product_id', $product->product_id);
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if (!is_null($product->location_id)) {
                                    $sql_reservation_clone->where('mo_reservation_detail.location_id', $product->location_id);
                                }
    
                                if (!is_null($product->min_quantity) && is_null($product->max_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                                }
    
                                if (!is_null($product->max_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity));
                                }
    
                                $datos_reservation_client_type = null;
    
                                foreach ($promotion_client_type as $client_type) {
                                    $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                    $sql_reservation_clone_clone->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir);
    
                                    $sql_reservation_clone_clone
                                        ->where('mo_reservation_detail_client_type.client_type_id', $client_type->client_type_id)
                                        ->select(
                                            'mo_reservation_detail.id',
                                            DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                        );
    
                                    $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                    $sql_reservation_clone_clone->addSelect(
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                    )->leftJoin('mo_reservation_detail_activity', function ($join) {
                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                    })
                                        ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                            $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_event', function ($join) {
                                            $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_event.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_package', function ($join) {
                                            $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_package.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_park', function ($join) {
                                            $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_park.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                            $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                            $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_tour.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                            $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                            $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_photo.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_food', function ($join) {
                                            $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_food.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                            $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                            $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                        });
    
                                    if (count($promotion_enjoy) > 0) {
    
    
    
                                        foreach ($promotion_enjoy as $enjoy) {
                                            $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
                                            if (is_null($datos_reservation_client_type)) {
    
                                                $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_enjoy)
                                                    ->where(function ($query) use ($enjoy) {
                                                        $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                            ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                    });
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                        ->where('date_aux','<=', $fecha_fin);
                                                }
    
                                                $products_enjoy = $sql_reservation_enjoy->get();
    
                                                foreach ($products_enjoy as $product_enjoy) {
                                                    $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                    $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                    if ($promotion_enjoy) {
                                                        $datos_reservation_client_type = $product_enjoy;
                                                    }
                                                }
                                            }
                                        }
    
                                    } else {
    
                                        $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_clone_clone);
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_clone_clone->where('date_aux','>=', $fecha_inicio)
                                                ->where('date_aux','<=', $fecha_fin);
                                        }
    
                                        $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                    }
    
    
                                    if (!$datos_reservation_client_type) {
                                        $repetir = 0;
                                    } else {
                                        if ($auto_accumulate == 0) {
                                            $repetir = 0;
                                        }
                                        $array_detalles_repetir[] = $datos_reservation_client_type->id;
                                    }
                                }
                            }
                        } else {
                            $sql_reservation_clone = clone $sql_reservation_detail;
    
                            $sql_reservation_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                ->whereNull('mo_reservation_detail.service_id')
                                ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir)
                                ->groupBy('mo_reservation_detail.id')
                                ->where('mo_reservation_detail.product_id', $product->product_id)
                                ->select(
                                    'mo_reservation_detail.id'
                                );
    
    
                            if ($promotion->apply_country_filter == 1) {
                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                            }
    
                            if($promotion->apply_state_filter == 1){
                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                            }
    
                            if (!is_null($product->location_id)) {
                                $sql_reservation_clone->where('mo_reservation_detail.location_id', $product->location_id);
                            }
    
                            $datos_reservation_detail = null;
    
                            //Promociones por cantidad de pax por producto
                            if (!is_null($product->min_quantity) && is_null($product->max_quantity)) {
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                            }
    
                            if (!is_null($product->max_quantity) && is_null($product->min_quantity)) {
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity));
                            }
    
                            if (!is_null($product->max_quantity) && !is_null($product->min_quantity)) {
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product->max_quantity))
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product->min_quantity));
                            }
    
                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                            $sql_reservation_clone->addSelect(
                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                            );
    
                            if (count($promotion_enjoy) > 0) {
    
                                foreach ($promotion_enjoy as $enjoy) {
                                    $sql_reservation_enjoy = clone $sql_reservation_clone;
                                    if (is_null($datos_reservation_detail)) {
    
                                        $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_enjoy)
                                            ->where(function ($query) use ($enjoy) {
                                                $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                    ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                            });
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                ->where('date_aux','<=', $fecha_fin);
                                        }
    
                                        $products_enjoy = $sql_reservation_enjoy->get();
    
                                        foreach ($products_enjoy as $product_enjoy) {
                                            $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                            $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                            if ($promotion_enjoy) {
                                                $datos_reservation_detail = $product_enjoy;
                                            }
                                        }
                                    }
                                }
    
                            } else {
    
                                $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                    ->mergeBindings($sql_reservation_clone);
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                    $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                                        ->where('date_aux','<=', $fecha_fin);
                                }
    
                                $datos_reservation_detail = $sql_reservation_clone->first();
                            }
    
                            if (!$datos_reservation_detail) {
                                $repetir = 0;
                            } else {
                                if ($auto_accumulate == 0) {
                                    $repetir = 0;
                                }
                                $array_detalles_repetir[] = $datos_reservation_detail->id;
                            }
                        }
                    }
    
                    foreach ($promotion_product_type as $product_type) {
    
                        if ($product_type->apply_client_type_filter == 1) {
    
                            $promotion_client_type = PromotionProductTypeClientType::where('promotion_product_type_id', $product_type->id)->get();
    
                            if (count($promotion_client_type)) {
                                $sql_reservation_clone = clone $sql_reservation;
    
                                $sql_reservation_clone
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                    ->whereNull('mo_product.deleted_at')
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir)
                                    ->groupBy('mo_reservation_detail.id')
                                    ->where('mo_product.type_id', $product_type->product_type_id);
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                $datos_reservation_client_type = null;
    
                                if (!is_null($product_type->min_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                                }
    
                                if (!is_null($product_type->max_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity));
                                }
    
                                $datos_reservation_client_type = null;
    
                                foreach ($promotion_client_type as $client_type) {
                                    $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                    $sql_reservation_clone_clone->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir);
    
                                    $sql_reservation_clone_clone
                                        ->where('mo_reservation_detail_client_type.client_type_id', $client_type->client_type_id);
    
                                    //$datos_reservation_detail = null;
    
                                    $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                    $sql_reservation_clone_clone->addSelect(
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                    )->leftJoin('mo_reservation_detail_activity', function ($join) {
                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                    })
                                        ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                            $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_event', function ($join) {
                                            $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_event.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_package', function ($join) {
                                            $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_package.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_park', function ($join) {
                                            $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_park.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                            $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                            $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_tour.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                            $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                            $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_photo.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_food', function ($join) {
                                            $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_food.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                            $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                            $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                        });
    
                                    if (count($promotion_enjoy) > 0) {
                                        foreach ($promotion_enjoy as $enjoy) {
                                            $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
                                            if (is_null($datos_reservation_client_type)) {
    
                                                $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_enjoy)
                                                    ->where(function ($query) use ($enjoy) {
                                                        $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                            ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                    });
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                        ->where('date_aux','<=', $fecha_fin);
                                                }
    
                                                $products_enjoy = $sql_reservation_enjoy->get();
    
                                                foreach ($products_enjoy as $product_enjoy) {
                                                    $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                    $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                    if ($promotion_enjoy) {
                                                        $datos_reservation_detail = $product_enjoy;
                                                    }
                                                }
                                            }
                                        }
    
                                    } else {
    
                                        $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_clone_clone);
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_clone_clone->where('date_aux','>=', $fecha_inicio)
                                                ->where('date_aux','<=', $fecha_fin);
                                        }
    
                                        $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                    }
    
                                    if (!$datos_reservation_client_type) {
                                        $repetir = 0;
                                    } else {
                                        if($auto_accumulate == 0){
                                            $repetir = 0;
                                        }
                                        $array_detalles_repetir[] = $datos_reservation_client_type->id;
                                    }
                                }
                            }
                        } else {
    
                            $sql_reservation_clone = clone $sql_reservation_detail;
    
                            $sql_reservation_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                ->whereNull('mo_reservation_detail.service_id')
                                ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                                ->whereNull('mo_product.deleted_at')
                                ->where('mo_product.type_id', $product_type->product_type_id)
                                ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir)
                                ->groupBy('mo_reservation_detail.id')
                                ->select(
                                    'mo_reservation_detail.id'
                                );
    
                            if ($promotion->apply_country_filter == 1) {
                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                            }
    
                            if($promotion->apply_state_filter == 1){
                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                            }
    
                            $datos_reservation_detail = null;
    
                            //Promociones por cantidad de pax por tipo de producto
                            if (!is_null($product_type->min_quantity) && is_null($product_type->max_quantity)) {
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
    
                            }
    
                            if (!is_null($product_type->max_quantity) & is_null($product_type->min_quantity)) {
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity));
                            }
    
    
                            if (!is_null($product_type->max_quantity) && !is_null($product_type->min_quantity)) {
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $product_type->max_quantity))
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $product_type->min_quantity));
                            }
    
                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                            $sql_reservation_clone->addSelect(
                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                            );
    
                            if (count($promotion_enjoy) > 0) {
    
    
    
    
                                foreach ($promotion_enjoy as $enjoy) {
                                    $sql_reservation_enjoy = clone $sql_reservation_clone;
                                    if (is_null($datos_reservation_detail)) {
    
                                        $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_enjoy)
                                            ->where(function ($query) use ($enjoy) {
                                                $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                    ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                            });
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                ->where('date_aux','<=', $fecha_fin);
                                        }
    
                                        $products_enjoy = $sql_reservation_enjoy->get();
    
                                        foreach ($products_enjoy as $product_enjoy) {
                                            $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                            $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                            if ($promotion_enjoy) {
                                                $datos_reservation_detail = $product_enjoy;
                                            }
                                        }
                                    }
                                }
    
                            } else {
    
                                $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                    ->mergeBindings($sql_reservation_clone);
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                    $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                                        ->where('date_aux','<=', $fecha_fin);
                                }
    
                                $datos_reservation_detail = $sql_reservation_clone->first();
                            }
    
                            if (!$datos_reservation_detail) {
                                $repetir = 0;
                            } else {
                                if ($auto_accumulate == 0) {
                                    $repetir = 0;
                                }
                                $array_detalles_repetir[] = $datos_reservation_detail->id;
                            }
                        }
                    }
    
                    foreach ($promotion_service as $service) {
    
                        if ($service->apply_client_type_filter == 1) {
                            $promotion_client_type = PromotionServiceClientType::where('promotion_service_id', $service->id)->get();
    
                            if (count($promotion_client_type)) {
                                $sql_reservation_clone = clone $sql_reservation;
    
                                $sql_reservation_clone
                                    ->addSelect(
                                        'mo_reservation_detail.id'
                                    )
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->where('mo_reservation_detail.service_id', $service->service_id)
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir)
                                    ->groupBy('mo_reservation_detail.id');
    
                                $datos_reservation_client_type = null;
    
                                if (!is_null($service->min_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity));
                                }
                                if (!is_null($service->max_quantity)) {
                                    $sql_reservation_clone
                                        ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
    
                                }
    
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                foreach ($promotion_client_type as $client_type) {
                                    $sql_reservation_clone_clone = clone $sql_reservation_clone;
    
                                    $sql_reservation_clone->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir);
    
                                    $sql_reservation_clone_clone
                                        ->where('mo_reservation_detail_client_type.client_type_id', $client_type->client_type_id);
    
                                    $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                                    $sql_reservation_clone_clone->addSelect(
                                        DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                    )->leftJoin('mo_reservation_detail_activity', function ($join) {
                                        $join->on('mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                                            ->whereNull('mo_reservation_detail_activity.deleted_at');
                                    })
                                        ->leftJoin('mo_reservation_detail_benefit_card', function ($join) {
                                            $join->on('mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_benefit_card.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_event', function ($join) {
                                            $join->on('mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_event.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_package', function ($join) {
                                            $join->on('mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_package.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_park', function ($join) {
                                            $join->on('mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_park.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_restaurant', function ($join) {
                                            $join->on('mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_restaurant.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_tour', function ($join) {
                                            $join->on('mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_tour.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_transportation', function ($join) {
                                            $join->on('mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_transportation.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_photo', function ($join) {
                                            $join->on('mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_photo.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_food', function ($join) {
                                            $join->on('mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_food.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_day_more', function ($join) {
                                            $join->on('mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_day_more.deleted_at');
                                        })
                                        ->leftJoin('mo_reservation_detail_hotel', function ($join) {
                                            $join->on('mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                                                ->whereNull('mo_reservation_detail_hotel.deleted_at');
                                        });
    
                                    if (count($promotion_enjoy) > 0) {
    
    
    
                                        foreach ($promotion_enjoy as $enjoy) {
                                            $sql_reservation_enjoy = clone $sql_reservation_clone_clone;
                                            if (is_null($datos_reservation_client_type)) {
    
                                                $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                                    ->mergeBindings($sql_reservation_enjoy)
                                                    ->where(function ($query) use ($enjoy) {
                                                        $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                            ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                                    });
    
                                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                                    $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                        ->where('date_aux','<=', $fecha_fin);
                                                }
    
                                                $products_enjoy = $sql_reservation_enjoy->get();
    
                                                foreach ($products_enjoy as $product_enjoy) {
                                                    $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                                    $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                                    if ($promotion_enjoy) {
                                                        $datos_reservation_client_type = $product_enjoy;
                                                    }
                                                }
                                            }
                                        }
    
                                    } else {
    
                                        $sql_reservation_clone_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone_clone->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_clone_clone);
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_clone_clone->where('date_aux','>=', $fecha_inicio)
                                                ->where('date_aux','<=', $fecha_fin);
                                        }
    
                                        $datos_reservation_client_type = $sql_reservation_clone_clone->first();
                                    }
    
                                    if (!$datos_reservation_client_type) {
                                        $repetir = 0;
                                    } else {
                                        if ($auto_accumulate == 0) {
                                            $repetir = 0;
                                        }
                                        $array_detalles_repetir[] = $datos_reservation_client_type->id;
                                    }
                                }
                            }
                        } else {
    
                            $sql_reservation_clone = clone $sql_reservation_detail;
    
                            $sql_reservation_clone->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                ->where('mo_reservation_detail.service_id', $service->service_id)
                                ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir)
                                ->groupBy('mo_reservation_detail.id')
                                ->select(
                                    'mo_reservation_detail.id'
                                );
    
                            if ($promotion->apply_country_filter == 1) {
                                $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                            }
    
                            if($promotion->apply_state_filter == 1){
                                $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                $sql_reservation_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                            }
    
    
                            $datos_reservation_detail = null;
    
                            //Promociones por cantidad de pax por tipo de producto
                            if (!is_null($service->min_quantity) && is_null($service->max_quantity)) {
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity));
    
                            }
    
                            if (!is_null($service->max_quantity) & is_null($service->min_quantity)) {
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity));
                            }
    
    
                            if (!is_null($service->max_quantity) && !is_null($service->min_quantity)) {
    
                                $sql_reservation_clone
                                    ->select(
                                        'mo_reservation_detail.id',
                                        DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as total_client_types')
                                    )
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) <=' . $service->max_quantity))
                                    ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) >=' . $service->min_quantity));
                            }
    
                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                            $sql_reservation_clone->addSelect(
                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                                IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                                IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                                IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                                IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                                IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                                IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                                IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                                IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                                IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                                IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                                mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                            );
    
                            if (count($promotion_enjoy) > 0) {
    
    
    
    
                                foreach ($promotion_enjoy as $enjoy) {
                                    $sql_reservation_enjoy = clone $sql_reservation_clone;
                                    if (is_null($datos_reservation_detail)) {
    
                                        $sql_reservation_enjoy = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_enjoy->toSql()}) as sub"))
                                            ->mergeBindings($sql_reservation_enjoy)
                                            ->where(function ($query) use ($enjoy) {
                                                $query->where('date_aux', '>=', $enjoy->enjoy_date_start)
                                                    ->where('date_aux', '<=', $enjoy->enjoy_date_end);
                                            });
    
                                        if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
                                            $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                            $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                            $sql_reservation_enjoy->where('date_aux','>=', $fecha_inicio)
                                                ->where('date_aux','<=', $fecha_fin);
                                        }
    
                                        $products_enjoy = $sql_reservation_enjoy->get();
    
                                        foreach ($products_enjoy as $product_enjoy) {
                                            $fecha_disfrute = Carbon::parse($product_enjoy->date_aux)->format('N');
    
                                            $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $enjoy->id)->where('day_id', $fecha_disfrute)->first();
    
                                            if ($promotion_enjoy) {
                                                $datos_reservation_detail = $product_enjoy;
                                            }
                                        }
                                    }
                                }
    
                            } else {
    
                                $sql_reservation_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_clone->toSql()}) as sub"))
                                    ->mergeBindings($sql_reservation_clone);
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $fecha_inicio = Carbon::now()->addDays($promotion->anticipation_start)->format('Y-m-d');
                                    $fecha_fin = Carbon::now()->addDays($promotion->anticipation_end)->format('Y-m-d');
    
                                    $sql_reservation_clone->where('date_aux','>=', $fecha_inicio)
                                        ->where('date_aux','<=', $fecha_fin);
                                }
    
                                $datos_reservation_detail = $sql_reservation_clone->first();
                            }
    
                            if (!$datos_reservation_detail) {
                                $repetir = 0;
                            } else {
                                if ($auto_accumulate == 0) {
                                    $repetir = 0;
                                }
                                $array_detalles_repetir[] = $datos_reservation_detail->id;
                            }
                        }
                    }
                    $count++;
    
                }
            }
    
            if (!is_null($promotion->promotion_amount) || $promotion->promotion_type_id == 4 || $promotion->promotion_type_id == 5) {
                if($promotion->apply_product_filter == 1 || $promotion->apply_product_type_filter == 1 || $promotion->apply_service_filter == 1){
    
                    $repetir_general = 1;
    
                    $array_detalles_repetir_general = array();
                    $count = 0;
                    while($repetir_general) {
    
                        $count++;
                        $repetir_general = 1;
    
                        $datos_suma = 0;
    
                        $promotion_product_filter = PromotionProduct::where('promotion_id',$promotion->id)->get();
    
                        $promotion_service_filter = PromotionService::where('promotion_id', $promotion->id)->get();
    
                        $promotion_product_type_filter = PromotionProductType::where('promotion_id', $promotion->id)->get();
    
                        $sql_reservation_promotion_product = clone $sql_reservation_detail;
    
                        $sql_reservation_promotion_product->join('mo_reservation_detail_client_type','mo_reservation_detail_client_type.reservation_detail_id','mo_reservation_detail.id')
                            ->whereNull('mo_reservation_detail_client_type.deleted_at')
                            ->join('mo_product','mo_product.id','mo_reservation_detail.product_id')
                            ->whereNull('mo_product.deleted_at')
                            ->select(
                                'mo_reservation.id as reservation_id',
                                'mo_reservation_detail.id',
                                'mo_reservation_detail_client_type.sale_price',
                                'mo_reservation_detail_client_type.client_type_id',
                                'mo_reservation_detail_client_type.id as detail_client_type',
                                DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as count_client_types'),
                                DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                    IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                    IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                    IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                    IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                    IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                    IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                    IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                    IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                    IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                    IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                    mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                            )
                            ->groupBy('mo_reservation_detail.id');
    
                        $sql_reservation_promotion_product_client_type = clone $sql_reservation_promotion_product;
    
                        if($promotion->apply_product_filter == 1 && count($promotion_product_filter) > 0) {
    
                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                            $array_fechas = array();
                            foreach ($promotion_enjoy as $range) {
                                $rango = \Carbon\CarbonPeriod::create($range->enjoy_date_start, $range->enjoy_date_end);
    
                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $range->id)->get();
    
                                foreach ($rango as $fecha) {
                                    foreach($promotion_enjoy as $enjoy) {
    
                                        if($fecha->format('N') == $enjoy->day_id) {
    
                                            $array_fechas[] = $fecha->format('Y-m-d');
                                        }
                                    }
                                }
                            }
    
                            foreach ($promotion_product_filter as $promotion_product){
                                $sql_reservation_promotion_product_clone = clone $sql_reservation_promotion_product;
    
                                $sql_reservation_promotion_product_clone
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir_general)
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->where('mo_reservation_detail.product_id', $promotion_product->product_id);
    
                                if(!is_null($promotion_product->location_id)) {
                                    $sql_reservation_promotion_product_clone->where('mo_reservation_detail.location_id',$promotion_product->location_id);
                                }
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if($promotion_product->apply_client_type_filter == 1) {
    
                                    $promotion_product_client_type = PromotionProductClientType::where('promotion_product_id', $promotion_product->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_product_client_type);
    
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if(!is_null($promotion_product->min_quantity) || !is_null($promotion_product->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion_product->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '>=', $promotion_product->min_quantity);
                                    }
    
                                    if(!is_null($promotion_product->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion_product->max_quantity);
                                    }
    
                                } else {
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
                                }
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    $sql_reservation_promotion_product_clone->where('date_aux', '>=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_start))
                                        ->where('date_aux','<=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_end));
                                }
    
                                if(count($promotion_enjoy) > 0) {
                                    $sql_reservation_promotion_product_clone->whereIn('date_aux', $array_fechas);
                                }
    
                                $sql_primer_detalle = clone $sql_reservation_promotion_product_clone;
    
                                $dato_primer_detalle = $sql_primer_detalle->groupBy('id')->first();
    
                                $datos_producto = $sql_reservation_promotion_product_clone->where('id', $dato_primer_detalle->id)->groupBy('id')->get();
                                
                                $suma_detalle = 0;
                                foreach($datos_producto as $producto) {
    
                                    $sql_client_type = DB::connection('tenant')->table('mo_reservation_detail_client_type')
                                        ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                        ->where('mo_reservation_detail_client_type.reservation_detail_id', $producto->id);
    
                                    if ($promotion->apply_country_filter == 1) {
                                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                    }
    
                                    if($promotion->apply_state_filter == 1){
                                        $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                    }
    
                                    if($promotion_product->apply_client_type_filter == 1) {
    
                                        $promotion_product_client_type = PromotionProductClientType::where('promotion_product_id', $promotion_product->id)->pluck('client_type_id')->toArray();
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_product_client_type);
    
                                    }
    
                                    if ($promotion->apply_client_type_filter == 1){
                                        $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                    }
    
                                    $datos_client_type = $sql_client_type->get();
    
                                    $suma_detalle_client_types = 0;
    
                                    foreach ($datos_client_type as $client_type) {
                                        $promotion_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_promotion')
                                            ->where('reservation_id', $producto->reservation_id)
                                            ->where('reservation_detail_id', $producto->id)
                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                            ->where('reservation_detail_client_type_id', $client_type->id)
                                            ->orderBy('id')
                                            ->get();
    
                                        $suma_subtotal_client_type = $client_type->sale_price;
    
                                        foreach ($promotion_reservation_detail_client_type as $promotion_detail_client_type) {
    
                                            if ($promotion_detail_client_type->promotion_type_id == 1 || $promotion_detail_client_type->promotion_type_id == 2) {
                                                $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                                            }
    
                                            if ($promotion_detail_client_type->promotion_type_id == 3) {
                                                $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                                            }
    
                                            $suma_subtotal_client_type = number_format($suma_subtotal_client_type, $settings_decimales, '.', '');
                                        }
                                        $suma_detalle_client_types+=$suma_subtotal_client_type;
                                    }
    
                                    $promotion_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $producto->reservation_id)
                                        ->whereNull('mo_reservation_promotion.deleted_at')
                                        ->where('reservation_detail_id', $producto->id)
                                        ->whereNull('reservation_detail_client_type_id')
                                        ->orderBy('id')
                                        ->get();
    
                                    $suma_detalle = $suma_detalle_client_types;
    
                                    foreach ($promotion_reservation_detail_client_type as $promotion_detail_client_type) {
    
                                        if ($promotion_detail_client_type->promotion_type_id == 1 || $promotion_detail_client_type->promotion_type_id == 2) {
                                            $suma_detalle -= $promotion_detail_client_type->promotion_amount_total;
                                        }
    
                                        if ($promotion_detail_client_type->promotion_type_id == 3) {
                                            $suma_detalle -= $promotion_detail_client_type->promotion_amount_total;
                                        }
    
                                        $suma_detalle = number_format($suma_detalle, $settings_decimales, '.', '');
                                    }
                                }
    
                                $suma_detalle = number_format($suma_detalle, $settings_decimales, '.', '');
    
                                $datos_suma += $suma_detalle;
    
                                $array_detalles_repetir_general[] = $dato_primer_detalle->id;
                            }
                        }
    
                        if($promotion->apply_product_type_filter == 1 && count($promotion_product_type_filter) > 0) {
    
                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                            $array_fechas = array();
                            foreach ($promotion_enjoy as $range) {
                                $rango = \Carbon\CarbonPeriod::create($range->enjoy_date_start, $range->enjoy_date_end);
    
                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $range->id)->get();
    
                                foreach ($rango as $fecha) {
                                    foreach($promotion_enjoy as $enjoy) {
    
                                        if($fecha->format('N') == $enjoy->day_id) {
    
                                            $array_fechas[] = $fecha->format('Y-m-d');
                                        }
                                    }
                                }
                            }
    
                            foreach ($promotion_product_type_filter as $promotion_product){
    
                                $sql_reservation_promotion_product_clone = clone $sql_reservation_promotion_product;
    
                                $sql_reservation_promotion_product_clone
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir_general)
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->where('mo_product.type_id', $promotion_product->product_type_id);
    
                                $sql_reservation_promotion_product_clone->groupBy('mo_reservation_detail_client_type.id');
    
                                if($promotion_product->apply_client_type_filter == 1) {
    
                                    $promotion_product_client_type = PromotionProductTypeClientType::where('promotion_product_type_id', $promotion_product->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_product_client_type);
    
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
    
                                if(!is_null($promotion_product->min_quantity) || !is_null($promotion_product->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion_product->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '>=', $promotion_product->min_quantity);
                                    }
    
                                    if(!is_null($promotion_product->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion_product->max_quantity);
                                    }
    
                                } else {
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
                                }
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    $sql_reservation_promotion_product_clone->where('date_aux', '>=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_start))
                                        ->where('date_aux','<=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_end));
                                }
    
                                if(count($promotion_enjoy) > 0) {
                                    $sql_reservation_promotion_product_clone->whereIn('date_aux', $array_fechas);
                                }
    
                                $sql_primer_detalle = clone $sql_reservation_promotion_product_clone;
                                $dato_primer_detalle = $sql_primer_detalle->groupBy('id')->first();
    
                                $datos_producto = $sql_reservation_promotion_product_clone->where('id', $dato_primer_detalle->id)->groupBy('id')->get();
    
                                $suma_detalle = 0;
    
                                foreach($datos_producto as $producto) {
    
                                    $sql_client_type = DB::connection('tenant')->table('mo_reservation_detail_client_type')
                                        ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                        ->where('mo_reservation_detail_client_type.reservation_detail_id', $producto->id);
    
                                    if($promotion_product->apply_client_type_filter == 1) {
                                        $promotion_product_client_type = PromotionProductTypeClientType::where('promotion_product_type_id', $promotion_product->id)->pluck('client_type_id')->toArray();
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_product_client_type);
                                    }
    
                                    if ($promotion->apply_client_type_filter == 1){
                                        $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                    }
    
                                    if ($promotion->apply_country_filter == 1) {
                                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                    }
    
                                    if($promotion->apply_state_filter == 1){
                                        $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                    }
    
                                    $datos_client_type = $sql_client_type->get();
    
                                    $suma_detalle_client_types = 0;
    
                                    foreach ($datos_client_type as $client_type) {
                                        $promotion_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_promotion')
                                            ->where('reservation_id', $producto->reservation_id)
                                            ->where('reservation_detail_id', $producto->id)
                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                            ->where('reservation_detail_client_type_id', $client_type->id)
                                            ->orderBy('id')
                                            ->get();
    
                                        $suma_subtotal_client_type = $client_type->sale_price;
    
                                        foreach ($promotion_reservation_detail_client_type as $promotion_detail_client_type) {
    
                                            if ($promotion_detail_client_type->promotion_type_id == 1 || $promotion_detail_client_type->promotion_type_id == 2) {
                                                $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                                            }
    
                                            if ($promotion_detail_client_type->promotion_type_id == 3) {
                                                $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                                            }
    
                                            $suma_subtotal_client_type = number_format($suma_subtotal_client_type, $settings_decimales, '.', '');
                                        }
                                        $suma_detalle_client_types+=$suma_subtotal_client_type;
                                    }
    
                                    $promotion_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $producto->reservation_id)
                                        ->whereNull('mo_reservation_promotion.deleted_at')
                                        ->where('reservation_detail_id', $producto->id)
                                        ->whereNull('reservation_detail_client_type_id')
                                        ->orderBy('id')
                                        ->get();
    
                                    $suma_detalle = $suma_detalle_client_types;
    
                                    foreach ($promotion_reservation_detail_client_type as $promotion_detail_client_type) {
    
                                        if ($promotion_detail_client_type->promotion_type_id == 1 || $promotion_detail_client_type->promotion_type_id == 2) {
                                            $suma_detalle -= $promotion_detail_client_type->promotion_amount_total;
                                        }
    
                                        if ($promotion_detail_client_type->promotion_type_id == 3) {
                                            $suma_detalle -= $promotion_detail_client_type->promotion_amount_total;
                                        }
    
                                        $suma_detalle = number_format($suma_detalle, $settings_decimales, '.', '');
                                    }
                                }
    
                                $suma_detalle = number_format($suma_detalle, $settings_decimales, '.', '');
    
                                $datos_suma += $suma_detalle;
    
                                $array_detalles_repetir_general[] = $dato_primer_detalle->id;
    
                            }
    
                        }
    
                        if($promotion->apply_service_filter == 1 && count($promotion_service_filter) > 0) {
    
                            $promotion_enjoy = PromotionEnjoy::where('promotion_id', $promotion->id)->get();
    
                            $array_fechas = array();
                            foreach ($promotion_enjoy as $range) {
                                $rango = \Carbon\CarbonPeriod::create($range->enjoy_date_start, $range->enjoy_date_end);
    
                                $promotion_enjoy = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $range->id)->get();
    
                                foreach ($rango as $fecha) {
                                    foreach($promotion_enjoy as $enjoy) {
    
                                        if($fecha->format('N') == $enjoy->day_id) {
    
                                            $array_fechas[] = $fecha->format('Y-m-d');
                                        }
                                    }
                                }
                            }
    
                            foreach ($promotion_service_filter as $promotion_service){
                                $sql_reservation_promotion_service_clone = clone $sql_reservation_promotion_product;
    
                                $sql_reservation_promotion_service_clone
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir_general)
                                    ->where('mo_reservation_detail.service_id', $promotion_service->service_id);
    
                                $sql_reservation_promotion_service_clone->groupBy('mo_reservation_detail_client_type.id');
    
                                if($promotion_service->apply_client_type_filter == 1) {
    
                                    $promotion_service_client_type = PromotionServiceClientType::where('promotion_service_id', $promotion_service->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_service_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_service_client_type);
    
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_service_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_promotion_service_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_promotion_service_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
    
                                if(!is_null($promotion_service->min_quantity) || !is_null($promotion_service->max_quantity)){
    
                                    $sql_reservation_promotion_service_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_service_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_service_clone);
    
                                    if(!is_null($promotion_service->min_quantity)) {
                                        $sql_reservation_promotion_service_clone
                                            ->where('count_client_types', '>=', $promotion_service->min_quantity);
                                    }
    
                                    if(!is_null($promotion_service->max_quantity)) {
                                        $sql_reservation_promotion_service_clone
                                            ->where('count_client_types', '<=', $promotion_service->max_quantity);
                                    }
    
                                } else {
                                    $sql_reservation_promotion_service_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_service_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_service_clone);
                                }
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $sql_reservation_promotion_service_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_service_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_service_clone);
    
                                    $sql_reservation_promotion_service_clone->where('date_aux', '>=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_start))
                                        ->where('date_aux','<=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_end));
                                }
    
                                if(count($promotion_enjoy) > 0) {
                                    $sql_reservation_promotion_service_clone->whereIn('date_aux', $array_fechas);
                                }
    
                                $sql_primer_detalle = clone $sql_reservation_promotion_service_clone;
    
                                $dato_primer_detalle = $sql_primer_detalle->groupBy('id')->first();
    
                                $datos_servicio = $sql_reservation_promotion_service_clone->where('id', $dato_primer_detalle->id)->groupBy('id')->get();
    
                                $suma_detalle = 0;
    
                                foreach($datos_servicio as $servicio) {
                                    $sql_client_type = DB::connection('tenant')->table('mo_reservation_detail_client_type')
                                        ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                        ->where('mo_reservation_detail_client_type.reservation_detail_id', $servicio->id);
    
                                    if ($promotion->apply_country_filter == 1) {
                                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                    }
    
                                    if($promotion->apply_state_filter == 1){
                                        $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                    }
    
                                    if ($promotion->apply_client_type_filter == 1){
                                        $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                    }
    
                                    if ($promotion->apply_country_filter == 1) {
                                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                        $sql_client_type->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                    }
    
                                    $datos_client_type = $sql_client_type->get();
    
                                    $suma_detalle_client_types = 0;
    
                                    foreach ($datos_client_type as $client_type) {
                                        $promotion_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_promotion')
                                            ->where('reservation_id', $servicio->reservation_id)
                                            ->where('reservation_detail_id', $servicio->id)
                                            ->whereNull('mo_reservation_promotion.deleted_at')
                                            ->where('reservation_detail_client_type_id', $servicio->detail_client_type)
                                            ->orderBy('id')
                                            ->get();
    
                                        $suma_subtotal_client_type = $client_type->sale_price;
    
                                        foreach ($promotion_reservation_detail_client_type as $promotion_detail_client_type) {
    
                                            if ($promotion_detail_client_type->promotion_type_id == 1 || $promotion_detail_client_type->promotion_type_id == 2) {
                                                $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                                            }
    
                                            if ($promotion_detail_client_type->promotion_type_id == 3) {
                                                $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                                            }
    
                                            $suma_subtotal_client_type = number_format($suma_subtotal_client_type, $settings_decimales, '.', '');
                                        }
                                        $suma_detalle_client_types+=$suma_subtotal_client_type;
                                    }
    
                                    $promotion_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $servicio->reservation_id)
                                        ->where('reservation_detail_id', $servicio->id)
                                        ->whereNull('mo_reservation_promotion.deleted_at')
                                        ->whereNull('reservation_detail_client_type_id')
                                        ->orderBy('id')
                                        ->get();
    
                                    $suma_detalle = $suma_detalle_client_types;
    
                                    foreach ($promotion_reservation_detail_client_type as $promotion_detail_client_type) {
    
                                        if ($promotion_detail_client_type->promotion_type_id == 1 || $promotion_detail_client_type->promotion_type_id == 2) {
                                            $suma_detalle -= $promotion_detail_client_type->promotion_amount_total;
                                        }
    
                                        if ($promotion_detail_client_type->promotion_type_id == 3) {
                                            $suma_detalle -= $promotion_detail_client_type->promotion_amount_total;
                                        }
    
                                        $suma_detalle = number_format($suma_detalle, $settings_decimales, '.', '');
                                    }
                                }
    
                                $suma_detalle = number_format($suma_detalle, $settings_decimales, '.', '');
    
                                $datos_suma += $suma_detalle;
    
                                $array_detalles_repetir_general[] = $dato_primer_detalle->id;
                            }
    
                        }
    
                        if ($promotion->promotion_type_id == 1) {
    
                            $promotion_amount_total = $datos_suma * ($promotion->promotion_amount / 100);
                        }
    
                        if ($promotion->promotion_type_id == 2) {
                            $sql_reservation_currency = clone $sql_reservation_detail;
    
                            $datos_reservation_currency = $sql_reservation_currency
                                ->select(
                                    'mo_reservation_detail.currency_id'
                                )->first();
    
                            if ($promotion->currency_id != $datos_reservation_currency->currency_id) {
                                $promotion_amount_total = conversor($promotion->promotion_amount, $promotion->currency_id, $datos_reservation_currency->currency_id);
                            } else {
                                $promotion_amount_total = $promotion->promotion_amount;
                            }
                        }
    
                        if ($promotion->promotion_type_id == 3) {
    
                            $sql_reservation_currency = clone $sql_reservation_detail;
    
                            $datos_reservation_currency = $sql_reservation_currency
                                ->select(
                                    'mo_reservation_detail.currency_id'
                                )->first();
    
                            if ($promotion->currency_id != $datos_reservation_currency->currency_id) {
                                $descuento = $datos_suma - conversor($promotion->promotion_amount, $promotion->currency_id, $datos_reservation_currency->currency_id);
                                $promotion_amount_total = $descuento;
                            } else {
                                $descuento = $datos_suma - $promotion->promotion_amount;
                                $promotion_amount_total = $descuento;
                            }
                        }
    
    
                        $amount = null;
    
                        if (!is_null($promotion->promotion_amount)) {
                            $amount = $promotion->promotion_amount;
                        }
    
    
                        $promotion_type_id = null;
                        $promotion_type_translation = null;
                        $currency_id = null;
    
                        if (!is_null($promotion->currency_id)) {
                            $currency_id = $promotion->currency_id;
                        }
    
                        if (!is_null($promotion->promotion_type_id)) {
                            $promotion_type_id = $promotion->promotion_type_id;
                            $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                                ->where('language_id', $idioma->id)
                                ->first()->name;
                        }
    
                        if($promotion->promotion_type_id == 4 || $promotion->promotion_type_id == 5) {
                            $promotion_amount_total = null;
                            $currency_id = null;
                            $amount = null;
                        }
    
                        ReservationPromotion::create([
                            'reservation_id' => $request->get('id'),
                            'promotion_id' => $promotion->id,
                            'promotion_amount' => $amount,
                            'promotion_amount_total' => $promotion_amount_total,
                            'promotion_name' => $promotion_translate->name,
                            'promotion_short_description' => $promotion_translate->short_description,
                            'promotion_order' => $promotion->order,
                            'promotion_type_id' => $promotion_type_id,
                            'promotion_type_name' => $promotion_type_translation,
                            'coupon_code' => $coupon_code,
                            'benefit_card_id' => $benefit_card_id,
                            'benefit_card_number' => $benefit_card_number,
                            'benefit_card_name' => $benefit_card_name,
                            'code' => $promotion->code,
                            'currency_id' => $currency_id,
                        ]);
    
                        if($auto_accumulate == 1){
                            $sql_reservation_promotion_product = clone $sql_reservation_detail;
    
                            $sql_reservation_promotion_product->join('mo_reservation_detail_client_type','mo_reservation_detail_client_type.reservation_detail_id','mo_reservation_detail.id')
                                ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                ->join('mo_product','mo_product.id','mo_reservation_detail.product_id')
                                ->whereNull('mo_product.deleted_at')
                                ->select(
                                    'mo_reservation.id as reservation_id',
                                    'mo_reservation_detail.id',
                                    'mo_reservation_detail_client_type.sale_price',
                                    'mo_reservation_detail_client_type.client_type_id',
                                    'mo_reservation_detail_client_type.id as detail_client_type',
                                    DB::connection('tenant')->raw('COUNT(mo_reservation_detail_client_type.id) as count_client_types'),
                                    DB::connection('tenant')->raw('IF(mo_reservation_detail_activity.id IS NOT NULL, mo_reservation_detail_activity.date, 
                                    IF(mo_reservation_detail_benefit_card.id IS NOT NULL, mo_reservation_detail_benefit_card.date,
                                    IF(mo_reservation_detail_event.id IS NOT NULL, mo_reservation_detail_event.date,
                                    IF(mo_reservation_detail_package.id IS NOT NULL, mo_reservation_detail_package.date,
                                    IF(mo_reservation_detail_park.id IS NOT NULL, mo_reservation_detail_park.date,
                                    IF(mo_reservation_detail_restaurant.id IS NOT NULL, mo_reservation_detail_restaurant.date,
                                    IF(mo_reservation_detail_tour.id IS NOT NULL, mo_reservation_detail_tour.date,
                                    IF(mo_reservation_detail_transportation.id IS NOT NULL, mo_reservation_detail_transportation.date,
                                    IF(mo_reservation_detail_photo.id IS NOT NULL, mo_reservation_detail_photo.date,
                                    IF(mo_reservation_detail_food.id IS NOT NULL, mo_reservation_detail_food.date,
                                    IF(mo_reservation_detail_day_more.id IS NOT NULL, mo_reservation_detail_day_more.date,
                                    mo_reservation_detail_hotel.date_start))))))))))) AS date_aux')
                                );
    
                            foreach ($promotion_product_filter as $promotion_product){
    
                                $sql_reservation_promotion_product_clone = clone $sql_reservation_promotion_product;
    
                                $sql_reservation_promotion_product_clone
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir_general)
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->where('mo_reservation_detail.product_id', $promotion_product->product_id);
    
                                $sql_reservation_promotion_product_clone->groupBy('mo_reservation_detail_client_type.id');
    
                                if(!is_null($promotion_product->location_id)) {
                                    $sql_reservation_promotion_product_clone->where('mo_reservation_detail.location_id',$promotion_product->location_id);
                                }
    
                                if($promotion_product->apply_client_type_filter == 1) {
    
                                    $promotion_product_client_type = PromotionProductClientType::where('promotion_product_id', $promotion_product->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_product_client_type);
    
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if(!is_null($promotion_product->min_quantity) || !is_null($promotion_product->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion_product->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '>=', $promotion_product->min_quantity);
                                    }
    
                                    if(!is_null($promotion_product->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion_product->max_quantity);
                                    }
    
                                    $sql_reservation_promotion_product_clone->groupBy('detail_client_type');
                                } elseif (!is_null($promotion->min_quantity) || !is_null($promotion->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '>=', $promotion->min_quantity);
                                    }
    
                                    if(!is_null($promotion->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion->max_quantity);
                                    }
    
                                    $sql_reservation_promotion_product_clone->groupBy('detail_client_type');
                                } else {
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
                                }
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    $sql_reservation_promotion_product_clone->where('date_aux', '>=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_start))
                                        ->where('date_aux','<=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_end));
                                }
    
                                if(count($promotion_enjoy) > 0) {
                                    $sql_reservation_promotion_product_clone->whereIn('date_aux', $array_fechas);
                                }
    
                                $datos_producto = $sql_reservation_promotion_product_clone->get();
    
                                if(count($datos_producto) <= 0 || count($datos_producto) < count($promotion_product_filter)) {
                                    $repetir_general = 0;
                                }
                            }
    
                            foreach ($promotion_service_filter as $promotion_service){
                                $sql_reservation_promotion_product_clone = clone $sql_reservation_promotion_product;
    
                                $sql_reservation_promotion_product_clone
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir_general)
                                    ->where('mo_reservation_detail.service_id', $promotion_service->service_id);
    
                                $sql_reservation_promotion_product_clone->groupBy('mo_reservation_detail_client_type.id');
    
                                if($promotion_service->apply_client_type_filter == 1) {
    
                                    $promotion_service_client_type = PromotionServiceClientType::where('promotion_service_id', $promotion_service->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_service_client_type);
    
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if(!is_null($promotion_service->min_quantity) || !is_null($promotion_service->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion_service->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '>=', $promotion_service->min_quantity);
                                    }
    
                                    if(!is_null($promotion_service->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion_service->max_quantity);
                                    }
    
                                    $sql_reservation_promotion_product_clone->groupBy('detail_client_type');
                                } elseif (!is_null($promotion->min_quantity) || !is_null($promotion->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types','>=', $promotion->min_quantity);
                                    }
    
                                    if(!is_null($promotion->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion->max_quantity);
                                    }
    
                                    $sql_reservation_promotion_product_clone->groupBy('detail_client_type');
                                } else {
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
                                }
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    $sql_reservation_promotion_product_clone->where('date_aux', '>=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_start))
                                        ->where('date_aux','<=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_end));
                                }
    
    
                                if(count($promotion_enjoy) > 0) {
                                    $sql_reservation_promotion_product_clone->whereIn('date_aux', $array_fechas);
                                }
    
                                $datos_producto = $sql_reservation_promotion_product_clone->get();
    
    
                                if(count($datos_producto) <= 0 || count($datos_producto) < count($promotion_service_filter)) {
                                    $repetir_general = 0;
                                }
                            }
    
                            $count = 0;
                            foreach ($promotion_product_type_filter as $promotion_product){
    
                                $sql_reservation_promotion_product_clone = clone $sql_reservation_promotion_product;
    
                                $sql_reservation_promotion_product_clone
                                    ->whereNotIn('mo_reservation_detail.id', $array_detalles_repetir_general)
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->where('mo_product.type_id', $promotion_product->product_type_id);
    
                                $sql_reservation_promotion_product_clone->groupBy('mo_reservation_detail_client_type.id');
    
                                if($promotion_product->apply_client_type_filter == 1) {
    
                                    $promotion_product_client_type = PromotionProductTypeClientType::where('promotion_product_type_id', $promotion_product->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_product_client_type);
    
                                }
    
                                if ($promotion->apply_client_type_filter == 1){
                                    $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                                }
    
                                if ($promotion->apply_country_filter == 1) {
                                    $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                                }
    
                                if($promotion->apply_state_filter == 1){
                                    $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                                    $sql_reservation_promotion_product_clone->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                                }
    
                                if(!is_null($promotion_product->min_quantity) || !is_null($promotion_product->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion_product->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '>=', $promotion_product->min_quantity);
                                    }
    
                                    if(!is_null($promotion_product->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion_product->max_quantity);
                                    }
    
                                    $sql_reservation_promotion_product_clone->groupBy('detail_client_type');
                                } elseif (!is_null($promotion->min_quantity) || !is_null($promotion->max_quantity)){
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    if(!is_null($promotion->min_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '>=', $promotion->min_quantity);
                                    }
    
                                    if(!is_null($promotion->max_quantity)) {
                                        $sql_reservation_promotion_product_clone
                                            ->where('count_client_types', '<=', $promotion->max_quantity);
                                    }
    
                                    $sql_reservation_promotion_product_clone->groupBy('detail_client_type');
                                } else {
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
                                }
    
                                if (!is_null($promotion->anticipation_start) || !is_null($promotion->anticipation_end)) {
    
                                    $sql_reservation_promotion_product_clone = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_reservation_promotion_product_clone->toSql()}) as sub"))
                                        ->mergeBindings($sql_reservation_promotion_product_clone);
    
                                    $sql_reservation_promotion_product_clone->where('date_aux', '>=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_start))
                                        ->where('date_aux','<=', Carbon::now()->startOfDay()->addDays($promotion->anticipation_end));
                                }
    
                                if(count($promotion_enjoy) > 0) {
                                    $sql_reservation_promotion_product_clone->whereIn('date_aux', $array_fechas);
                                }
    
                                $datos_producto = $sql_reservation_promotion_product_clone->get();
    
                                if(count($datos_producto) <= 0 || count($datos_producto) < count($promotion_product_type_filter)) {
                                    $repetir_general = 0;
                                }
                            }
    
                        } else {
                            $repetir_general = 0;
                        }
                    }
                } else {
    
                    $sql_reservation_detail_clone = clone $sql_reservation;
    
                    $suma_subtotal = $sql_reservation_detail_clone
                        ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                        ->whereNull('mo_reservation_detail_client_type.deleted_at');
    
                    if ($promotion->apply_country_filter == 1) {
                        $promotion_country = PromotionCountry::where('promotion_id', $promotion->id)->get(['country_id']);
    
                        $suma_subtotal->whereIn('mo_reservation_detail_client_type.client_country_id',array_column($promotion_country->toArray(),'country_id'));
                    }
    
                    if($promotion->apply_state_filter == 1){
                        $promotion_state = PromotionState::where('promotion_id', $promotion->id)->get(['state_id']);
    
                        $suma_subtotal->whereIn('mo_reservation_detail_client_type.client_state_id',array_column($promotion_state->toArray(),'state_id'));
                    }
    
                    if ($promotion->apply_client_type_filter == 1){
                        $promotion_client_type = PromotionClientType::where('promotion_id', $promotion->id)->pluck('client_type_id')->toArray();
    
                        $suma_subtotal->whereIn('mo_reservation_detail_client_type.client_type_id', $promotion_client_type);
                    }
    
                    $suma_subtotal = $suma_subtotal->sum('mo_reservation_detail_client_type.sale_price');
    
                    $total = $suma_subtotal;
    
                    $promotion_amount_total = 0;
    
                    if ($promotion->promotion_type_id == 1) {
    
                        $datos_promocion_generales_detalle = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                            ->whereNull('mo_reservation_promotion.deleted_at')
                            ->get();
    
                        foreach ($datos_promocion_generales_detalle as $dato_promocion) {
                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                $total -= $dato_promocion->promotion_amount_total;
                            }
    
                            if ($dato_promocion->promotion_type_id == 3) {
                                $total -= $dato_promocion->promotion_amount_total;
                            }
                            $total = number_format($total, $settings_decimales, '.', '');
                        }
    
                        $promotion_amount_total = $total * ($promotion->promotion_amount / 100);
                    }
    
                    if ($promotion->promotion_type_id == 2) {
                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                        $datos_reservation_currency = $sql_reservation_currency
                            ->select(
                                'mo_reservation_detail.currency_id'
                            )->first();
    
                        if ($promotion->currency_id != $datos_reservation_currency->currency_id) {
                            $promotion_amount_total = conversor($promotion->promotion_amount, $promotion->currency_id, $datos_reservation_currency->currency_id);
                        } else {
                            $promotion_amount_total = $promotion->promotion_amount;
                        }
                    }
    
                    if ($promotion->promotion_type_id == 3) {
    
                        $datos_promocion_generales_detalle = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $request->get('id'))
                            ->whereNull('mo_reservation_promotion.deleted_at')
                            ->get();
    
                        foreach ($datos_promocion_generales_detalle as $dato_promocion) {
                            if ($dato_promocion->promotion_type_id == 1 || $dato_promocion->promotion_type_id == 2) {
                                $total -= $dato_promocion->promotion_amount_total;
                            }
    
                            if ($dato_promocion->promotion_type_id == 3) {
                                $total -= $dato_promocion->promotion_amount_total;
                            }
                            $total = number_format($total, $settings_decimales, '.', '');
                        }
    
                        $sql_reservation_currency = clone $sql_reservation_detail;
    
                        $datos_reservation_currency = $sql_reservation_currency
                            ->select(
                                'mo_reservation_detail.currency_id'
                            )->first();
    
                        if ($promotion->currency_id != $datos_reservation_currency->currency_id) {
                            $descuento = $total - conversor($promotion->promotion_amount, $promotion->currency_id, $datos_reservation_currency->currency_id);
                            $promotion_amount_total = $descuento;
                        } else {
                            $descuento = $total - $promotion->promotion_amount;
                            $promotion_amount_total = $descuento;
                        }
                    }
    
                    //
                    $amount = null;
    
                    if (!is_null($promotion->promotion_amount)) {
                        $amount = $promotion->promotion_amount;
                    }
    
    
                    $promotion_type_id = null;
                    $promotion_type_translation = null;
                    $currency_id = null;
    
                    if (!is_null($promotion->currency_id)) {
                        $currency_id = $promotion->currency_id;
                    }
    
                    if($promotion->promotion_type_id == 4 || $promotion->promotion_type_id == 5){
                        $amount = null;
                        $promotion_amount_total = null;
                        $currency_id = null;
    
                    }
    
                    if (!is_null($promotion->promotion_type_id)) {
                        $promotion_type_id = $promotion->promotion_type_id;
                        $promotion_type_translation = PromotionTypeTranslation::where('promotion_type_id', $promotion->promotion_type_id)
                            ->where('language_id', $idioma->id)
                            ->first()->name;
                    }
    
                    ReservationPromotion::create([
                        'reservation_id' => $request->get('id'),
                        'promotion_id' => $promotion->id,
                        'promotion_amount' => $amount,
                        'promotion_amount_total' => $promotion_amount_total,
                        'promotion_name' => $promotion_translate->name,
                        'promotion_short_description' => $promotion_translate->short_description,
                        'promotion_order' => $promotion->order,
                        'promotion_type_id' => $promotion_type_id,
                        'promotion_type_name' => $promotion_type_translation,
                        'coupon_code' => $coupon_code,
                        'benefit_card_id' => $benefit_card_id,
                        'benefit_card_number' => $benefit_card_number,
                        'benefit_card_name' => $benefit_card_name,
                        'code' => $promotion->code,
                        'currency_id' => $currency_id,
                    ]);
                }
            }
                //
    
        }
    }

    function reservationTotalToPay($id) {
        $sql_suma_subtotal = DB::connection('tenant')->table('mo_reservation')
            ->select(
                'mo_reservation.id',
                'mo_reservation.client_id',
                'mo_reservation.client_session',
                'mo_reservation.subchannel_id'
            )
            ->whereNull('mo_reservation.deleted_at')
            ->where('mo_reservation.id', $id)
            ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
            ->whereNull('mo_reservation_detail.deleted_at')
            ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
            ->whereNull('mo_reservation_detail_client_type.deleted_at');
    
    
        $settings_decimales = Settings::where('name', 'numero_decimales')->first()->value;
    
        $total = 0;
    
        $reservation_details = ReservationDetail::where('reservation_id', $id)
            ->where('operation_status_id','!=',5)
            ->where('operation_status_id','!=',6)
            ->get();
    
        foreach ($reservation_details as $detail_reservation) {
    
            $reservation_detail_client_types = ReservationDetailClientType::where('reservation_detail_id', $detail_reservation->id)->get();
    
            $suma_subtotal = 0;
            foreach ($reservation_detail_client_types as $detail_client_type) {
                $promotion_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $id)
                    ->whereNull('mo_reservation_promotion.deleted_at')
                    ->where('reservation_detail_id', $detail_reservation->id)
                    ->where('reservation_detail_client_type_id', $detail_client_type->id)
                    ->orderBy('id')
                    ->get();
    
                $suma_subtotal_client_type = $detail_client_type->sale_price;
    
                foreach ($promotion_reservation_detail_client_type as $promotion_detail_client_type) {
    
                    if ($promotion_detail_client_type->promotion_type_id == 1 || $promotion_detail_client_type->promotion_type_id == 2) {
                        $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                    }
    
                    if ($promotion_detail_client_type->promotion_type_id == 3) {
                        $suma_subtotal_client_type -= $promotion_detail_client_type->promotion_amount_total;
                    }
                    $suma_subtotal_client_type = number_format($suma_subtotal_client_type, $settings_decimales, '.', '');
                }
    
                $suma_subtotal += $suma_subtotal_client_type;
    
            }
    
    
            $promotion_reservation_detail = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $id)
                ->where('reservation_detail_id', $detail_reservation->id)
                ->whereNull('mo_reservation_promotion.deleted_at')
                ->whereNull('reservation_detail_client_type_id')
                ->orderBy('id')
                ->get();
    
            foreach ($promotion_reservation_detail as $promotion_detail) {
    
                if ($promotion_detail->promotion_type_id == 1 || $promotion_detail->promotion_type_id == 2) {
                    $suma_subtotal -= $promotion_detail->promotion_amount_total;
                }
    
                if ($promotion_detail->promotion_type_id == 3) {
                    $suma_subtotal -= $promotion_detail->promotion_amount_total;
                }
                $suma_subtotal = number_format($suma_subtotal, $settings_decimales, '.', '');
    
            }
    
            $total += $suma_subtotal;
        }
    
        $promociones_generales = DB::connection('tenant')->table('mo_reservation_promotion')->where('reservation_id', $id)
            ->whereNull('mo_reservation_promotion.deleted_at')
            ->whereNull('reservation_detail_id')->get();
    
    
        foreach ($promociones_generales as $promocion) {
            if ($promocion->promotion_type_id == 1 || $promocion->promotion_type_id == 2) {
                $total -= $promocion->promotion_amount_total;
            }
    
            if ($promocion->promotion_type_id == 3) {
                $total -= $promocion->promotion_amount_total;
            }
    
            $total = number_format($total, $settings_decimales, '.', '');
        }
    
        $datos_reservation_insurance = DB::connection('tenant')->table('mo_reservation_insurance')
            ->select(
                'mo_reservation_insurance.total_price'
            )
            ->whereNull('mo_reservation_insurance.deleted_at')
            ->where('mo_reservation_insurance.reservation_id', $id)
            ->first();
    
        if ($datos_reservation_insurance) {
            $total += $datos_reservation_insurance->total_price;
        }
    
        return $total;
    }

    function reservationTotalPaid($id) {
        $settings_decimales = Settings::where('name', 'numero_decimales')->first()->value;
    
        $moneda = DB::connection('tenant')->table('mo_reservation_detail')
            ->select('mo_reservation_detail.currency_id')
            ->whereNull('mo_reservation_detail.deleted_at')
            ->where('mo_reservation_detail.reservation_id',$id)
            ->first();
    
        $datos_reservation_payment = DB::connection('tenant')->table('mo_reservation_payment')
            ->select(
                'mo_reservation_payment.amount',
                'mo_reservation_payment.currency_id',
                'mo_reservation_payment.currency_exchange_id'
            )
            ->whereNull('mo_reservation_payment.deleted_at')
            ->where('mo_reservation_payment.reservation_id', $id)
            ->where('mo_reservation_payment.status_ok', 1)
            ->get();
    
        $total_pagado = 0;
    
        foreach($datos_reservation_payment as $reservation_payment) {
            if($reservation_payment->currency_id == $moneda->currency_id){
                $total_pagado += $reservation_payment->amount;
            } else {
                $total_pagado = $total_pagado + conversorCustomReservation($reservation_payment->amount, $reservation_payment->currency_id, $moneda->currency_id, $reservation_payment->currency_exchange_id);
            }
            $total_pagado = round($total_pagado, $settings_decimales);
        }
    
    
        $total_pagado = round($total_pagado,$settings_decimales);
    
        return $total_pagado;
    }

    /**
    *desencriptar
    *
    * @param string $cadena
    * @return string
    */
    function desencriptar($cadena)
    {
        //$resultado = convert_uudecode($cadena);
        $resultado = str_rot13($cadena);
        $resultado = strrev($resultado);
        $resultado = base64_decode($resultado);

        return ($resultado);
    }

    function encriptar($cadena)
    {
        $resultado = base64_encode($cadena);
        $resultado = strrev($resultado);
        $resultado = str_rot13($resultado);
        //$resultado = convert_uuencode($resultado);

        return ($resultado);
    }

/**
 * Funcion para generar PDF de reserva
 *
 * @param string $name
 * @return null
 */
function generatePDF($data)
{

    $pdf = PDF::loadView($data['template'], $data['content']);

    return $pdf->download($data['file_name']);

}

function encriptar_parametros_url($data)
{
    $resultado = base64_encode($data);
    $resultado = strrev($resultado);
    $resultado = strtr($resultado, '+/=', '-_,');
    return $resultado;
}

function desencriptar_parametros_url($data)
{
    $resultado = strtr($data, '-_,', '+/=');
    $resultado = strrev($resultado);
    $resultado = base64_decode($resultado);
    return $resultado;
}

function insurance($request, $emulated_subchannel_id, $reserva_agent_id)
{

    //Elementos de la reserva
    $sql_reservation_detail = DB::connection('tenant')->table('mo_reservation_detail')
        ->select('mo_reservation_detail.id', 'mo_reservation_detail.currency_id',
            'mo_reservation_detail.service_id', 'mo_reservation_detail.product_id')
        ->whereNull('mo_reservation_detail.deleted_at')
        ->join('mo_reservation', 'mo_reservation.id', 'mo_reservation_detail.reservation_id')
        ->whereNull('mo_reservation.deleted_at')
        ->where(function ($query) {
            $query->where('mo_reservation_detail.operation_status_id', 1)
                ->orWhere('mo_reservation_detail.operation_status_id', 2)
                ->orWhere('mo_reservation_detail.operation_status_id', 7);
        })
        ->where('mo_reservation.id', $request->get('id'));

    //Clientes de la reserva
    $sql_reservation_detail_client_type = DB::connection('tenant')->table('mo_reservation_detail_client_type')
        ->select(
            'mo_reservation_detail_client_type.id',
            'mo_reservation_detail_client_type.original_net_price',
            'mo_reservation_detail_client_type.original_sale_price',
            'mo_reservation_detail_client_type.original_markup',
            'mo_reservation_detail_client_type.net_price',
            'mo_reservation_detail_client_type.sale_price',
            'mo_reservation_detail_client_type.markup'
        )
        ->whereNull('mo_reservation_detail_client_type.deleted_at');

    //Tipos de producto
    $sql_product_type = DB::connection('tenant')->table('mo_product_type')
        ->select('mo_product_type.id')
        ->whereNull('mo_product_type.deleted_at')
        ->join('mo_product', 'mo_product.type_id', 'mo_product_type.id')
        ->whereNull('mo_product.deleted_at');

    $quantity = 0;
    $quantity_aux = 0;
    $date_init = null;
    $date_final = null;
    $moneda = null;

    $sql_reservation_detail_clone = clone $sql_reservation_detail;
    $datos_reservation_detail = $sql_reservation_detail_clone->get();

    foreach ($datos_reservation_detail as $reserva_detalle) {

        //Moneda de la reserva (común para todos los elementos de reserva)
        $moneda = $reserva_detalle->currency_id;

        //Cantidad (número máximo de visitantes en un elemento de reserva)
        $sql_reservation_detail_client_type_clone = clone $sql_reservation_detail_client_type;
        $quantity_aux = $sql_reservation_detail_client_type_clone->where('mo_reservation_detail_client_type.reservation_detail_id', $reserva_detalle->id)->count();
        if ($quantity_aux > $quantity) {
            $quantity = $quantity_aux;
        }

        //Fechas de seguro
        $sql_reservation_detail_clone = clone $sql_reservation_detail;
        $sql_reservation_detail_clone->where('mo_reservation_detail.id', $reserva_detalle->id);

        //Producto
        if ($reserva_detalle->service_id == null) {

            $sql_product_type_clone = clone $sql_product_type;
            $datos_product_type = $sql_product_type_clone->where('mo_product.id', $reserva_detalle->product_id)
                ->first();

            if ($datos_product_type && $datos_product_type->id == 1) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_package', 'mo_reservation_detail_package.reservation_detail_id', 'mo_reservation_detail.id')
                    ->whereNull('mo_reservation_detail_package.deleted_at')
                    ->orderBy('mo_reservation_detail_package.date', 'desc')
                    ->addSelect('mo_reservation_detail_package.date')
                    ->first();

                $date_aux = $datos->date;
            }

            if ($datos_product_type && $datos_product_type->id == 2) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_hotel', 'mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_hotel.date_end')
                    ->whereNull('mo_reservation_detail_hotel.deleted_at')->first();
                $date_aux = $datos->date_end;
            }

            if ($datos_product_type && $datos_product_type->id == 3) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_activity', 'mo_reservation_detail_activity.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_activity.date')
                    ->whereNull('mo_reservation_detail_activity.deleted_at')->first();
                $date_aux = $datos->date;
            }

            if ($datos_product_type && $datos_product_type->id == 4) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_event', 'mo_reservation_detail_event.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_event.date')
                    ->whereNull('mo_reservation_detail_event.deleted_at')->first();
                $date_aux = $datos->date;
            }

            if ($datos_product_type && $datos_product_type->id == 5) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_tour', 'mo_reservation_detail_tour.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_tour.date')
                    ->whereNull('mo_reservation_detail_tour.deleted_at')->first();
                $date_aux = $datos->date;
            }

            if ($datos_product_type && $datos_product_type->id == 6) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_park', 'mo_reservation_detail_park.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_park.date')
                    ->whereNull('mo_reservation_detail_park.deleted_at')->first();
                $date_aux = $datos->date;
            }

            if ($datos_product_type && $datos_product_type->id == 7) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_restaurant', 'mo_reservation_detail_restaurant.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_restaurant.date')
                    ->whereNull('mo_reservation_detail_restaurant.deleted_at')->first();
                $date_aux = $datos->date;
            }


            if ($datos_product_type && $datos_product_type->id == 8) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_benefit_card', 'mo_reservation_detail_benefit_card.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_benefit_card.date')
                    ->whereNull('mo_reservation_detail_benefit_card.deleted_at')->first();
                $date_aux = $datos->date;
            }

            //Servicio
        } else {
            if ($reserva_detalle->service_id == 1) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_transportation', 'mo_reservation_detail_transportation.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_transportation.date')
                    ->whereNull('mo_reservation_detail_transportation.deleted_at')->first();
                $date_aux = $datos->date;
            }

            if ($reserva_detalle->service_id == 2) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_food', 'mo_reservation_detail_food.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_food.date')
                    ->whereNull('mo_reservation_detail_food.deleted_at')->first();
                $date_aux = $datos->date;
            }

            if ($reserva_detalle->service_id == 3) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_photo', 'mo_reservation_detail_photo.reservation_detail_id', 'mo_reservation_detail.id')
                    ->addSelect('mo_reservation_detail_photo.date')
                    ->whereNull('mo_reservation_detail_photo.deleted_at')->first();
                $date_aux = $datos->date;
            }

            if ($reserva_detalle->service_id == 4) {
                $datos = $sql_reservation_detail_clone->join('mo_reservation_detail_day_more', 'mo_reservation_detail_day_more.reservation_detail_id', 'mo_reservation_detail_day_more.id')
                    ->addSelect('mo_reservation_detail_day_more.date')
                    ->whereNull('mo_reservation_detail_day_more.deleted_at')->first();
                $date_aux = $datos->date;
            }
        }

        //Extracción de rango de fechas
        if ($datos) {
            if ($date_init) {
                if ($date_aux < $date_init) {
                    $date_init = $date_aux;
                }
            } else {
                $date_init = $date_aux;
            }
            if ($date_final) {
                if ($date_aux > $date_final) {
                    $date_final = $date_aux;
                }
            } else {
                $date_final = $date_aux;
            }
        }
    }

    $fecha_hora = Carbon::now()->format('Y-m-d H:i:s');
    $datos_currency_exchange = CurrencyExchange::where('currency_id', $moneda)
        ->where('date_time_start', '<=', $fecha_hora)
        ->orderBy('date_time_start', 'desc')
        ->first();

    //Precio del seguro
    $insurance = DB::connection('tenant')->table('mo_insurance')
        ->select('mo_insurance.id','mo_insurance.sale_price','mo_insurance.currency_id','mo_insurance.iva_id','mo_iva.value as iva')
        ->whereNull('mo_insurance.deleted_at')
        ->where('mo_insurance.id', $request->get('insurance_id'))
        ->join('mo_iva', 'mo_iva.id', 'mo_insurance.iva_id')
        ->whereNull('mo_iva.deleted_at')
        ->first();
                    
    $unity_price = $insurance->sale_price;
    $total_price = $quantity * $unity_price;
    $moneda_base = $insurance->currency_id;

    $datos_reservation_insurance = DB::connection('tenant')->table('mo_reservation_insurance')
        ->where('reservation_id', $request->get('id'))
        ->where('insurance_id', $request->get('insurance_id'))
        ->whereNull('mo_reservation_insurance.deleted_at')
        ->first();

    //Actualización del registro existente para la reserva y seguro especificados
    if ($datos_reservation_insurance) {
        ReservationInsurance::where('reservation_id', $request->get('id'))
            ->where('insurance_id', $request->get('insurance_id'))
            ->update([
                'subchannel_id' => $emulated_subchannel_id,
                'quantity' => $quantity,
                'unity_price' => conversor($unity_price, $moneda_base, $moneda),
                'original_unity_price' => conversor($unity_price, $moneda_base, $moneda),
                'total_price' => conversor($total_price, $moneda_base, $moneda),
                'original_total_price' => conversor($total_price, $moneda_base, $moneda),
                'purchase_date' => $date_init,
                'visit_date' => $date_final,
                'currency_id' => $moneda,
                'currency_exchange_id' => $datos_currency_exchange->id,
                'iva' => (!$insurance->iva || $insurance->iva == '') ? 0 : $insurance->iva,
                'operation_status_id' => 1,
                'date_time_start' => Carbon::now()->format('Y-m-d H:i:s'),
                'payment_status_id' => 1,
                'agent_id' => $reserva_agent_id,
            ]);

        //Creación de registro de seguro para la reserva
    } else {
        $reserva = ReservationInsurance::create([
            'reservation_id' => $request->get('id'),
            'insurance_id' => $request->get('insurance_id'),
            'subchannel_id' => $emulated_subchannel_id,
            'quantity' => $quantity,
            'unity_price' => conversor($unity_price, $moneda_base, $moneda),
            'original_unity_price' => conversor($unity_price, $moneda_base, $moneda),
            'total_price' => conversor($total_price, $moneda_base, $moneda),
            'original_total_price' => conversor($total_price, $moneda_base, $moneda),
            'purchase_date' => $date_init,
            'visit_date' => $date_final,
            'currency_id' => $moneda,
            'currency_exchange_id' => $datos_currency_exchange->id,
            'iva' => (!$insurance->iva || $insurance->iva == '') ? 0 : $insurance->iva,
            'operation_status_id' => 1,
            'date_time_start' => Carbon::now()->format('Y-m-d H:i:s'),
            'payment_status_id' => 1,
            'agent_id' => $reserva_agent_id,
        ]);
    }
}


/**
    * Función que crea la conexión con el servicio externo
    * @param $external_connection_url
    * @param $external_service_user
    * @param $external_service_password
    * @return SoapCLient
*/
function iniciarClienteSoap($external_connection_url, $external_service_user, $external_service_password)
{


    $wsdl = $external_connection_url;


    $cliente = new SoapClient($wsdl);


    $headerbody = ['From' => ['Credential' => ['userName' => $external_service_user,
        'password' => $external_service_password]]];

    $ns = 'http://htng.org/1.1/Header/';

    $header = new SOAPHeader($ns, 'HTNGHeader', $headerbody);

    $cliente->__setSoapHeaders($header);


    return ($cliente);
}

/**
 * Función que genera la estructura básica obligatoria para las peticiones soap
 * @param $peticion
 */
function generarPOS($peticion)
{
    $peticion->addChild('POS');
    $peticion->POS->addChild('Source');
    $peticion->POS->Source->addChild('RequestorID');
    $peticion->POS->Source->RequestorID->addAttribute('ID', '10');
    $peticion->POS->Source->RequestorID->addAttribute('ID_Context', 'Synxis');
    $peticion->POS->Source->RequestorID->addChild('CompanyName');
    $peticion->POS->Source->RequestorID->CompanyName->addAttribute('Code', 'WSBE');
}

/**
    * Función que realiza la llamada al servicio externo para consultar la disponibilidad de habitaciones
    *
    * @param $rooms
    * @return Objeto XML
*/
function getExternalRoomData($rooms, $language, $currency)
{

    $cliente = iniciarClienteSoap($rooms['external_connection_url'], $rooms['external_service_user'], $rooms['external_service_password']);


//        Se cre un elemento SimpleXML donde se añaden los datos que necesitamos para realizar la petición
//        evitando enviar un fichero de texto XML y permitiendo el paso de parámetros al servicio

    $peticion = new SimpleXMLElement('<OTA_HotelAvailRQ MaxResponses="0" BestOnly="false" SummaryOnly="true" HotelStayOnly="false" xmlns="http://www.opentravel.org/OTA/2003/05"></OTA_HotelAvailRQ>');

    $peticion->addAttribute('PrimaryLangID', $language);
    $peticion->addAttribute('RequestedCurrency', $currency);
    $peticion->addAttribute('EchoToken', '[token]');
    $peticion->addAttribute('ExactMatchOnly', 'false');


//        Todas las peticiones requieren un campo POS con datos relativos al cliente
//        que realiza la petición, por lo que lo llevamos a una función externa generarPos

    generarPOS($peticion);


//        Se continúa creando el fichero XML


    $peticion->addChild('AvailRequestSegments');
    $peticion->AvailRequestSegments->addChild('AvailRequestSegment');
    $peticion->AvailRequestSegments->AvailRequestSegment->addChild('StayDateRange');
    $peticion->AvailRequestSegments->AvailRequestSegment->StayDateRange->addAttribute('Start', $rooms['date_start']);
    $peticion->AvailRequestSegments->AvailRequestSegment->StayDateRange->addAttribute('End', $rooms['date_end']);
    $peticion->AvailRequestSegments->AvailRequestSegment->addChild('RoomStayCandidates');
    $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->addChild('RoomStayCandidate');
    $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addAttribute('Quantity', $rooms['number_room']);

    if (isset($rooms['code'])) {
        $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addAttribute('RoomTypeCode', $rooms['code']);

    }


    $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addChild('GuestCounts');

    if(isset($rooms['ages']['adult']['age'])) {
        foreach($rooms['ages']['adult']['age'] as $edad) {

            $tipo_cliente = null;

            $tipo_cliente = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');

            $tipo_cliente->addAttribute('AgeQualifyingCode', '10');

            $tipo_cliente->addAttribute('Count', '1');
            $tipo_cliente->addAttribute('Age', $edad);
        }

        if(count($rooms['ages']['adult']['age']) < $rooms['adult']) {
            for($i=0; $i <= count($rooms['ages']['adult']['age']) - $rooms['adult']; $i++) {
                $tipo_cliente = null;

                $tipo_cliente = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');

                $tipo_cliente->addAttribute('AgeQualifyingCode', '10');
                $tipo_cliente->addAttribute('Count', 1);
            }

        }

    } else {
        if(isset($rooms['adult'])) {
            $tipo_cliente = null;

            $tipo_cliente = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');

            $tipo_cliente->addAttribute('AgeQualifyingCode', '10');
            $tipo_cliente->addAttribute('Count', $rooms['adult']);
        }
    }

    if(isset($rooms['ages']['child']['age'])) {
        foreach($rooms['ages']['child']['age'] as $edad) {

            $tipo_cliente = null;

            $tipo_cliente = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');

            $tipo_cliente->addAttribute('AgeQualifyingCode', '8');

            $tipo_cliente->addAttribute('Count', '1');
            $tipo_cliente->addAttribute('Age', $edad);
        }

        if(count($rooms['ages']['child']['age']) < $rooms['child']) {

            for($i = 0; $i <= count($rooms['ages']['child']['age']) - $rooms['child']; $i++) {
                $tipo_cliente = null;

                $tipo_cliente = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');

                $tipo_cliente->addAttribute('AgeQualifyingCode', '8');
                $tipo_cliente->addAttribute('Count', 1);
            }
        }
    } else {
        if(isset($rooms['child'])) {
            $guest_2 = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');
            $guest_2->addAttribute('AgeQualifyingCode', '8');
            $guest_2->addAttribute('Count', $rooms['child']);
        }
    }

    $peticion->AvailRequestSegments->AvailRequestSegment->addChild('HotelSearchCriteria');
    $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->addChild('Criterion');
    $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->addChild('HotelRef');
    $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->HotelRef->addAttribute('HotelCode', $rooms['external_code']);


    //Terminada la petición, se da formato XML

    $xml = $peticion->asXML();
    $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml);
    $param = new SoapVar($xml, XSD_ANYXML);

//        Se llama al servicio externo con la petición y se devuelve el resultado de la petición
//        para ser procesado por el método showRoom o showRoomType

    $respuesta = $cliente->CheckAvailability($param);

    if (isset($respuesta->Errors)) {
        Throw new \Exception('Ocurrió un error durante en la conexión con el servicio de hoteles ' . json_encode($respuesta->Errors));
    }

    return $respuesta;

}

function getExternalAvailability($rooms, $language, $currency)
    {

        $cliente = iniciarClienteSoap($rooms['external_connection_url'], $rooms['external_service_user'], $rooms['external_service_password']);


//        Se cre un elemento SimpleXML donde se añaden los datos que necesitamos para realizar la petición
//        evitando enviar un fichero de texto XML y permitiendo el paso de parámetros al servicio

        $peticion = new SimpleXMLElement('<OTA_HotelAvailRQ MaxResponses="0" BestOnly="false" SummaryOnly="false" HotelStayOnly="true" xmlns="http://www.opentravel.org/OTA/2003/05"></OTA_HotelAvailRQ>');

        $peticion->addAttribute('PrimaryLangID', $language);
        $peticion->addAttribute('RequestedCurrency', $currency);
        $peticion->addAttribute('EchoToken', '[token]');
        $peticion->addAttribute('ExactMatchOnly', 'false');


//        Todas las peticiones requieren un campo POS con datos relativos al cliente
//        que realiza la petición, por lo que lo llevamos a una función externa generarPos

        generarPOS($peticion);


//        Se continúa creando el fichero XML


        $peticion->addChild('AvailRequestSegments');
        $peticion->AvailRequestSegments->addChild('AvailRequestSegment');
        $peticion->AvailRequestSegments->AvailRequestSegment->addChild('StayDateRange');
        $peticion->AvailRequestSegments->AvailRequestSegment->StayDateRange->addAttribute('Start', $rooms['date_start']);
        $peticion->AvailRequestSegments->AvailRequestSegment->StayDateRange->addAttribute('End', $rooms['date_end']);
        $peticion->AvailRequestSegments->AvailRequestSegment->addChild('RoomStayCandidates');
        $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->addChild('RoomStayCandidate');
        $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addAttribute('Quantity', $rooms['number_room']);

        if (isset($rooms['code'])) {
            $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addAttribute('RoomTypeCode', $rooms['code']);
        }


        $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addChild('GuestCounts');

        $guest_1 = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');
        $guest_1->addAttribute('AgeQualifyingCode', '10');
        $guest_1->addAttribute('Count', $rooms['adult']);

        $guest_2 = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');
        $guest_2->addAttribute('AgeQualifyingCode', '8');
        $guest_2->addAttribute('Count', $rooms['child']);

        $peticion->AvailRequestSegments->AvailRequestSegment->addChild('HotelSearchCriteria');
        $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->addChild('Criterion');
        $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->addChild('HotelRef');
        $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->HotelRef->addAttribute('HotelCode', $rooms['external_code']);


        //Terminada la petición, se da formato XML


        $xml = $peticion->asXML();
        $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml);
        $param = new SoapVar($xml, XSD_ANYXML);

//        Se llama al servicio externo con la petición y se devuelve el resultado de la petición
//        para ser procesado por el método showRoom o showRoomType

        $respuesta = $cliente->CheckAvailability($param);

        if (isset($respuesta->Errors)) {
            Throw new \Exception('Ocurrió un error durante en la conexión con el servicio de hoteles ' . json_encode($respuesta->Errors));
        }

        return $respuesta;

    }

    function ReservationgetExternalRoomData($rooms, $language, $currency)
    {

        $sql_client_types = DB::connection('tenant')->table('mo_client_type')
            ->whereNull('mo_client_type.deleted_at');

        $cliente = iniciarClienteSoap($rooms['external_connection_url'], $rooms['external_service_user'], $rooms['external_service_password']);


//        Se cre un elemento SimpleXML donde se añaden los datos que necesitamos para realizar la petición
//        evitando enviar un fichero de texto XML y permitiendo el paso de parámetros al servicio

        $peticion = new SimpleXMLElement('<OTA_HotelAvailRQ MaxResponses="0" BestOnly="false" SummaryOnly="true" HotelStayOnly="false" xmlns="http://www.opentravel.org/OTA/2003/05"></OTA_HotelAvailRQ>');

        $peticion->addAttribute('PrimaryLangID', $language->abbreviation);
        $peticion->addAttribute('RequestedCurrency', $currency->iso_code);
        $peticion->addAttribute('EchoToken', '[token]');
        $peticion->addAttribute('ExactMatchOnly', 'false');


//        Todas las peticiones requieren un campo POS con datos relativos al cliente
//        que realiza la petición, por lo que lo llevamos a una función externa generarPos

        generarPOS($peticion);


//        Se continúa creando el fichero XML


        $peticion->addChild('AvailRequestSegments');
        $peticion->AvailRequestSegments->addChild('AvailRequestSegment');
        $peticion->AvailRequestSegments->AvailRequestSegment->addChild('StayDateRange');
        $peticion->AvailRequestSegments->AvailRequestSegment->StayDateRange->addAttribute('Start', $rooms['date_start']);
        $peticion->AvailRequestSegments->AvailRequestSegment->StayDateRange->addAttribute('End', $rooms['date_end']);
        $peticion->AvailRequestSegments->AvailRequestSegment->addChild('RoomStayCandidates');
        $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->addChild('RoomStayCandidate');
        $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addAttribute('Quantity', $rooms['number_room']);

        if (isset($rooms['code'])) {
            $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addAttribute('RoomTypeCode', $rooms['code']);

        }


        $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->addChild('GuestCounts');

        $datos_client_types = $sql_client_types->groupBy('hotel_id')->get();

        foreach($rooms['client_types'] as $tipo_cliente) {
            $guest_1 = null;
            $guest_1 = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');
            $guest_1->addAttribute('AgeQualifyingCode', $tipo_cliente['hotel_id']);
            if(isset($tipo_cliente['age'])) {
                $guest_1->addAttribute('Age', $tipo_cliente['age']);
            }
            $guest_1->addAttribute('Count', $tipo_cliente['quantity']);
        }

//        dd($peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts);
//        foreach ($datos_client_types as $client_type){
//            if(isset($rooms[$client_type->hotel_code])){
//                $guest_1 = null;
//                $guest_1 = $peticion->AvailRequestSegments->AvailRequestSegment->RoomStayCandidates->RoomStayCandidate->GuestCounts->addChild('GuestCount');
//                $guest_1->addAttribute('AgeQualifyingCode', $client_type->hotel_id);
//                $guest_1->addAttribute('Count', $rooms[$client_type->hotel_code]);
//            }
//        }

        $peticion->AvailRequestSegments->AvailRequestSegment->addChild('HotelSearchCriteria');
        $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->addChild('Criterion');
        $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->addChild('HotelRef');
        $peticion->AvailRequestSegments->AvailRequestSegment->HotelSearchCriteria->Criterion->HotelRef->addAttribute('HotelCode', $rooms['external_code']);

        //Terminada la petición, se da formato XML


        $xml = $peticion->asXML();

        $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml);
        $param = new SoapVar($xml, XSD_ANYXML);

//        Se llama al servicio externo con la petición y se devuelve el resultado de la petición
//        para ser procesado por el método showRoom o showRoomType

        $respuesta = $cliente->CheckAvailability($param);

        if (isset($respuesta->Errors)) {
            Throw new \Exception('Ocurrió un error durante en la conexión con el servicio de hoteles ' . json_encode($respuesta->Errors));
        }

        return $respuesta;

    }

    /**
 * @param $reservation_ids
 */
function cancelarReservaHotel($reservation_ids)
{

    $sql_reservation_detail = DB::connection('tenant')->table('mo_reservation_detail')
        ->select(
            'mo_reservation_detail.id',
            'mo_product.id',
            'mo_product.type_id',
            'mo_product.external_connection',
            'mo_product.external_connection_url',
            'mo_product.external_code',
            'mo_product.external_service_user',
            'mo_product.external_service_password'
        )
        ->whereNull('mo_reservation_detail.deleted_at')
        ->where('mo_reservation_detail.operation_status_id', 5)
        ->whereNull('mo_reservation_detail.service_id')
        ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
        ->whereNull('mo_product.deleted_at');

    $sql_reservation_hotel_detail = DB::connection('tenant')->table('mo_reservation_detail_hotel_room')
        ->select(
            'mo_reservation_detail_hotel_room.confirmation_code'
        )
        ->whereNull('mo_reservation_detail_hotel_room.deleted_at')
        ->join('mo_reservation_detail_hotel', 'mo_reservation_detail_hotel.id', 'mo_reservation_detail_hotel_room.reservation_hotel_id')
        ->whereNull('mo_reservation_detail_hotel.deleted_at');

    foreach ($reservation_ids as $reservation_id) {

        $sql_reservation_detail_clone = clone $sql_reservation_detail;

        $datos_reservation_detail = $sql_reservation_detail_clone
            ->where('mo_reservation_detail.id', $reservation_id)->first();

        if ($datos_reservation_detail && $datos_reservation_detail->external_connection == 1) {

            $sql_reservation_hotel_detail_clone = clone $sql_reservation_hotel_detail;

            $datos_reservation_hotel_detail = $sql_reservation_hotel_detail_clone
                ->where('mo_reservation_detail_hotel.reservation_detail_id', $reservation_id)
                ->first();

            if ($datos_reservation_hotel_detail) {

                $cliente = iniciarClienteSoap($datos_reservation_detail->external_connection_url, $datos_reservation_detail->external_service_user, $datos_reservation_detail->external_service_password);

                $peticion = new SimpleXMLElement(' <OTA_CancelRQ xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" EchoToken="[token]"></OTA_CancelRQ>');
//
                generarPOS($peticion);

                $peticion->addChild('UniqueID');
                $peticion->UniqueID->addAttribute('Type', 14);
                $peticion->UniqueID->addAttribute('ID', $datos_reservation_hotel_detail->confirmation_code);
                $peticion->UniqueID->addAttribute('ID_Context', 'CrsConfirmNumber');

                $peticion->addChild('Verification');
                $peticion->Verification->addChild('TPA_Extensions');
                $peticion->Verification->TPA_Extensions->addChild('BasicPropertyInfo');
                $peticion->Verification->TPA_Extensions->BasicPropertyInfo->addAttribute('HotelCode', $datos_reservation_detail->external_code);

                $xml = $peticion->asXML();
                $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml);
                $param = new SoapVar($xml, XSD_ANYXML);

                $respuesta = $cliente->CancelReservations($param);
            }

        }
    }
}



function visibleSubchannelProducts($subchannel_id, $product_id, $language_id){

    //Consulta productos visibles por canal
    $subchannel = Subchannel::where('id', $subchannel_id)->first();    
    $fecha = Carbon::now()->format('Y-m-d');

    $sql_productos = DB::connection('tenant')->table('mo_product')
        ->select(
            'mo_product.id as product_id',
            'mo_product_translation.name as product_name',
            'mo_tag.id as tag_id',
            'mo_tag_translation.name as tag_name',
            'mo_view_product.id as view_id',
            'mo_view_product_translation.name as view_name'
        )
        ->where('mo_product.id', '=', $product_id)
        ->whereNull('mo_product.deleted_at')
        ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
        ->whereNull('mo_product_translation.deleted_at')
        ->where('mo_product_translation.language_id', '=', $language_id)
        ->join('mo_price_product', 'mo_product.id', '=', 'mo_price_product.product_id')
        ->whereNull('mo_price_product.deleted_at')
        ->whereNull('mo_price_product.service_id')
        ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
        ->whereNull('mo_price.deleted_at')
        ->where('mo_price.date_start', '<=', $fecha)
        ->where('mo_price.date_end', '>=', $fecha)
        ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
        ->whereNull('mo_contract_model.deleted_at')
        ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
        ->whereNull('mo_subchannel.deleted_at')
        ->where('mo_subchannel.id', '=', $subchannel_id)
        ->leftJoin('mo_product_tag', function ($join) {
            $join->on('mo_product_tag.product_id', '=', 'mo_product.id')
                ->whereNull('mo_product_tag.deleted_at');
        })
        ->leftJoin('mo_tag', function ($join) {
            $join->on('mo_product_tag.tag_id', '=', 'mo_tag.id')
                ->whereNull('mo_tag.deleted_at');
        })
        ->leftJoin('mo_tag_translation', function ($join) use ($language_id) {
            $join->on('mo_tag_translation.tag_id', '=', 'mo_tag.id')
                ->whereNull('mo_tag_translation.deleted_at')
                ->where('mo_tag_translation.language_id', '=', $language_id);
        });

    if ($subchannel->view_blocked == 0) {
        $sql_productos->leftJoin('mo_view_product', function ($join) use ($subchannel_id) {
            $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                ->where('mo_view_product.subchannel_id', '=', $subchannel_id)
                ->whereNull('mo_view_product.deleted_at');
        })
        ->leftJoin('mo_view_product_translation', function ($join) use ($language_id) {
            $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                ->where('mo_view_product_translation.language_id', '=', $language_id)
                ->whereNull('mo_view_product_translation.deleted_at');
        })->Where(function ($query) {
            $query->where('mo_view_product.published', '=', '1');

            $query->orWhere(function ($query) {
                $query->WhereNull('mo_view_product.published')
                    ->Where('mo_subchannel.view_blocked', '=', '0');
            });
        });

    } else {
        $sql_productos->join('mo_view_product', 'mo_view_product.product_id', '=', 'mo_product.id')
            ->where('mo_view_product.subchannel_id', '=', $subchannel_id)
            ->whereNull('mo_view_product.deleted_at')
            ->join('mo_view_product_translation', 'mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
            ->where('mo_view_product_translation.language_id', '=', $language_id)
            ->whereNull('mo_view_product_translation.deleted_at');
    }

    $consulta_productos = clone $sql_productos;
    $datos_producto = $consulta_productos->groupBy('mo_product.id')->orderBy('product_name')->first();

    return $datos_producto;
}