<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_category_translation";

    public $fillable = [

        'language_id',
        'category_id',
        'name',
        'description',
        'short_description',
        'friendly_url',
        'title_seo',
        'description_seo',
        'keywords_seo'
    ];

    /**
     * Define relación de la tabla mo_category_translation con la tabla mo_category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo("\App\Models\tenant\Category", 'category_id');
    }

    /**
     * Define relación de la tabla mo_category_translation con la tabla mo_language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo("App\Models\tenant\Language", 'language_id');
    }

}