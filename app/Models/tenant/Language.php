<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Language
 *
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Language onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Language withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Language withoutTrashed()
 * @mixin \Eloquent
 */
class Language extends Model
{
    protected $table = 'mo_language';

    protected $fillable = ['id', 'name_lang', 'abbreviation', 'flag','front','back'];


    use SoftDeletes, UsesTenantConnection;

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Define relación de la table mo_language con mo_product_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productTranslation()
    {
        return $this->hasMany("\App\Models\tenant\ProductTranslation", 'language_id');
    }

    /**
     * Define relación de la table mo_language con mo_type_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function typeTranslation()
    {
        return $this->hasMany("\App\Models\tenant\TypeTranslation", 'language_id');
    }

}
