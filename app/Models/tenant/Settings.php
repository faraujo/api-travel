<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use  UsesTenantConnection;

    protected $table = "mo_settings";

    protected $fillable = ['name', 'value'];

}