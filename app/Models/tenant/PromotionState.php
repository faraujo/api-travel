<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionState extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_state';

    public $fillable = [
        'promotion_id',
        'state_id',
    ];

}