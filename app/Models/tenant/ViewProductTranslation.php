<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViewProductTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_view_product_translation";

    public $fillable = [

        'language_id',
        'view_product_id',
        'name',
        'description',
        'features',
        'recommendations',
        'short_description',
        'friendly_url',
        'discover_url',
        'title_seo',
        'description_seo',
        'keywords_seo',
        'legal',

        'index',
        'breadcrumb',
        'rel',
        'og_title',
        'og_description',
        'og_image',
        'twitter_title',
        'twitter_description',
        'twitter_image',
        'canonical_url',
        'script_head',
        'script_body',
        'script_footer',
    ];

}