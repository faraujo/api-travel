<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_location";

    public $fillable = [
        'name',
        'depends_on'
    ];


}