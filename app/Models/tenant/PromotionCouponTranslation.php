<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 12/02/2019
 * Time: 18:03
 */

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionCouponTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_promotion_coupon_translation';

    protected $fillable = [
        'language_id',
        'coupon_id',
        'file_id',
        'description',
        'legal',
        'notes',
        'booking'
    ];

}