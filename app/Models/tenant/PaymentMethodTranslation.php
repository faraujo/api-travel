<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethodTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_payment_method_translation";

    protected $fillable = ['payment_method_id','language_id','name'];


}