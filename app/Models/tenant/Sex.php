<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Sex
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Sex whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Sex extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_sex";

    protected $fillable = [];


}