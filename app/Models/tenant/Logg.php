<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;


class Logg extends Model
{
    use UsesTenantConnection;

    protected $table = "mo_email_log";

    protected $fillable = ['date','from','to','cc','bcc','subject','body','headers','attachments','description_error','received_email'];

}

