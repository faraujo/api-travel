<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_service_translation";

    public $fillable = [
        'service_id',
        'language_id',
        'name',
        'description',
    ];


    /**
     * Define relación de la tabla mo_product_translation con la tabla mo_language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo("App\Models\tenant\Language", 'language_id');
    }

}