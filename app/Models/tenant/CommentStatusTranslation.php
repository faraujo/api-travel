<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentStatusTranslation extends Model
{
    protected $table = "mo_comment_status_translation";

    use SoftDeletes, UsesTenantConnection;
    
    public $fillable = [

        'comment_status_id',
        'language_id',
        'name',
    ];


    /**
     * Define relación de la tabla mo_comment_status_translation con la tabla mo_comment_status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commentStatus()
    {
        return $this->belongsTo("\App\Models\tenant\CommentStatus", 'comment_status_id');
    }


}