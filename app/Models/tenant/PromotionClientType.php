<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionClientType extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_promotion_client_type';

    protected $fillable = ['client_type_id','promotion_id'];
    
}