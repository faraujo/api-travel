<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;


//class User extends Model implements AuthenticatableContract, AuthorizableContract
class User extends Model implements AuthenticatableContract
{
    use Authenticatable, HasApiTokens, SoftDeletes, UsesTenantConnection;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'email2', 
        'password',
        'document_type_id', 
        'number_document',
        'sex_id', 
        'birthdate', 
        'language_id', 
        'telephone1', 
        'telephone2', 
        'telephone3', 
        'address', 
        'postal_code',
        'city',
        'state_id',
        'country_id',
        'business',
        'telephone_business',
        'observations',
        'bloqued_login',
        'worker',
        'worker_type_id',
        'bloqued_to',
        'image_id',
        'faults_login',
        'facebook_id',
        'google_id',
        'social_register'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $table = 'mo_user';

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->hasMany('App\Models\tenant\UserRole', 'user_id');
    }
    
    /**
     * Define relación de la tabla mo_price con la tabla mo_user_identification
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userIdentification()
    {
        return $this->hasMany('App\Models\tenant\UserIdentification', 'user_id');
    }

    /**
     * Define la relación de la tabla mo_user y mo_user_remember_token
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userRemeberToken()
    {
        return $this->hasMany('App\Models\tenant\UserRememberToken', 'user_id');
    }

}
