<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViewProduct extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'product_id',
        'subchannel_id',
        'published',
        'order',

    ];

    protected $table = 'mo_view_product';


    /**
     * Define relación de la tabla mo_view_product con la tabla mo_view_product_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function viewProductTranslation()
    {
        return $this->hasMany('App\Models\tenant\ViewProductTranslation', 'view_product_id');
    }


    /**
     * Define relación de la tabla mo_view_product con la tabla mo_view_product_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function viewProductFile()
    {
        return $this->hasMany('App\Models\tenant\ViewProductFile','view_product_id');
    }


}