<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationLog extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_log';

    protected $fillable = [
        'date',
        'client_id',
        'agent_id',
        'action_id',
        'reservation_id',
        'reservation_detail_id',
        'subchannel_id',
    ];


}