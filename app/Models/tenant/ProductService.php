<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductService extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_product_service";

    public $fillable = [
        'product_id',
        'service_id',
    ];


    /**
     * Define relación de la tabla mo_product_service con la tabla mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo("\App\Models\tenant\Product", 'product_id');
    }

    /**
     * Define relación de la tabla mo_product_service con la tabla mo_service
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo("\App\Models\tenant\Service", 'service_id');
    }

}