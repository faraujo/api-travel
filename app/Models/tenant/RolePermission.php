<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RolePermission extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_role_permission';

    protected $fillable = ['permission_id', 'role_id','subchannel_id'];


}