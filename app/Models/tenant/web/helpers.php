<?php
    
    use App\Models\tenant\Language;
    use App\Models\tenant\Currency;
    use App\Models\tenant\Settings;
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;
    use GuzzleHttp\Client;

    function settings($idioma, $subchannel, $moneda, $request){
        $fecha = Carbon::now();

        $fecha_actual = $fecha->format('Y-m-d H:i:s');

        $week_day = $fecha->format('N');

        $sql_product_type = DB::connection('tenant')->table('mo_product_type')
            ->select(
                'mo_product_type.id',
                'mo_product_type_translation.name'
            )->whereNull('mo_product_type.deleted_at')
            ->join('mo_product_type_translation','mo_product_type_translation.type_id','mo_product_type.id')
            ->whereNull('mo_product_type_translation.deleted_at')
            ->where('mo_product_type_translation.language_id', $idioma->id);

        $sql_category = DB::connection('tenant')->table('mo_category')
            ->select(
                'mo_category.id',
                'mo_category_translation.name'
            )
            ->whereNull('mo_category.deleted_at')
            ->join('mo_category_translation','mo_category_translation.category_id','mo_category.id')
            ->whereNull('mo_category_translation.deleted_at')
            ->where('mo_category_translation.language_id', $idioma->id)
            ->orderBy('mo_category.order','desc');

        $sql_currencies = DB::connection('tenant')->table('mo_currency')
            ->select(
                'mo_currency.id',
                'mo_currency.name',
                'mo_currency.simbol',
                'mo_currency.iso_number',
                'mo_currency.iso_code'
            )
            ->whereNull('mo_currency.deleted_at')
            ->where('mo_currency.front', 1);

        $sql_settings = DB::connection('tenant')->table('mo_settings')
            ->select(
                'mo_settings.name',
                'mo_settings.value'
            )
            ->where('mo_settings.name','nombre_cliente')            
            ->orwhere('mo_settings.name','email_cliente_contacto')
            ->orWhere('mo_settings.name','idioma_defecto_id')
            ->orWhere('mo_settings.name','moneda_defecto_id')
            ->orWhere('mo_settings.name','logo_image_id');

        $sql_file = DB::connection('tenant')->table('mo_file')
            ->select(
                'mo_file.id',
                'mo_file.url_file'
            )->whereNull('mo_file.deleted_at')
            ->join('mo_file_translation','mo_file_translation.file_id','mo_file.id')
            ->whereNull('mo_file_translation.deleted_at')
            ->groupBy('mo_file.id');


        $sql_promotions = DB::connection('tenant')->table('mo_promotion')
            ->select(
                'mo_promotion.id',
                'mo_promotion.code',
                'mo_promotion.order',
                'mo_promotion.visibility',
                'mo_promotion.accumulate',
                'mo_promotion.promotion_amount',
                'mo_promotion.currency_id',
                'mo_promotion.promotion_type_id',
                'mo_promotion.anticipation_start',
                'mo_promotion.anticipation_end',
                'mo_promotion.coupon_code',
                'mo_promotion.coupon_type',
                'mo_promotion.benefit_card',
                'mo_promotion.benefit_card_id',
                'mo_promotion_translation.name',
                'mo_promotion_translation.description',
                'mo_promotion_translation.short_description',
                'mo_promotion_translation.friendly_url',
                'mo_promotion_translation.external_url',
                'mo_promotion_translation.observations',
                'mo_promotion_translation.legal',
                'mo_promotion_translation.notes',
                'mo_promotion_translation.gift',
                'mo_promotion_type_translation.name as type_name'
            )
            ->whereNull('mo_promotion.deleted_at')
            ->join('mo_promotion_type', 'mo_promotion_type.id', 'mo_promotion.promotion_type_id')
            ->whereNull('mo_promotion_type.deleted_at')
            ->join('mo_promotion_type_translation', 'mo_promotion_type_translation.promotion_type_id', 'mo_promotion_type.id')
            ->whereNull('mo_promotion_type_translation.deleted_at')
            ->join('mo_promotion_translation', 'mo_promotion_translation.promotion_id', 'mo_promotion.id')
            ->whereNull('mo_promotion_translation.deleted_at')
            ->where('mo_promotion_translation.language_id', $idioma->id)
            ->join('mo_campaign', 'mo_campaign.id', 'mo_promotion.campaign_id')
            ->whereNull('mo_campaign.deleted_at')
            ->where('mo_campaign.date_start', '<=', $fecha->format('Y-m-d'))
            ->where('mo_campaign.date_end', '>=', $fecha->format('Y-m-d'))
            ->join('mo_campaign_translation', 'mo_campaign_translation.campaign_id', 'mo_campaign.id')
            ->whereNull('mo_campaign_translation.deleted_at')
            ->where('mo_campaign_translation.language_id', $idioma->id)
            ->where('mo_promotion.visibility', 1)
            ->join('mo_promotion_range_sale_day', 'mo_promotion_range_sale_day.promotion_id', 'mo_promotion.id')
            ->whereNull('mo_promotion_range_sale_day.deleted_at')
            ->where('mo_promotion_range_sale_day.sale_date_start', '<=', $fecha_actual)
            ->where('mo_promotion_range_sale_day.sale_date_end', '>=', $fecha_actual)
            ->join('mo_promotion_sale_day', 'mo_promotion_sale_day.promotion_range_sale_day_id', 'mo_promotion_range_sale_day.id')
            ->whereNull('mo_promotion_sale_day.deleted_at')
            ->where('mo_promotion_sale_day.day_id', $week_day)
            ->leftJoin('mo_promotion_client_type', function ($join) {
                $join->on('mo_promotion_client_type.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_client_type.deleted_at');
            })
            ->leftJoin('mo_promotion_country', function ($join) {
                $join->on('mo_promotion_country.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_country.deleted_at');
            })->leftJoin('mo_promotion_currency', function ($join) {
                $join->on('mo_promotion_currency.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_currency.deleted_at');
            })->leftJoin('mo_promotion_device', function ($join) {
                $join->on('mo_promotion_device.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_device.deleted_at');
            })->leftJoin('mo_promotion_language', function ($join) {
                $join->on('mo_promotion_language.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_language.deleted_at');
            })
            ->leftJoin('mo_promotion_payment_method', function ($join) {
                $join->on('mo_promotion_payment_method.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_payment_method.deleted_at');
            })
            ->leftJoin('mo_promotion_pickup', function ($join) {
                $join->on('mo_promotion_pickup.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_pickup.deleted_at');
            })->leftJoin('mo_promotion_product', function ($join) {
                $join->on('mo_promotion_product.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_product.deleted_at');
            })->leftJoin('mo_promotion_product_type', function ($join) {
                $join->on('mo_promotion_product_type.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_product_type.deleted_at');
            })->leftJoin('mo_promotion_range_enjoy_day', function ($join) {
                $join->on('mo_promotion_range_enjoy_day.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_range_enjoy_day.deleted_at');
            })->leftJoin('mo_promotion_state', function ($join) {
                $join->on('mo_promotion_state.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_state.deleted_at');
            })->leftJoin('mo_promotion_subchannel', function ($join) {
                $join->on('mo_promotion_subchannel.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_subchannel.deleted_at');
            })->leftJoin('mo_promotion_service', function ($join) {
                $join->on('mo_promotion_service.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_service.deleted_at');
            })
            ->where(function ($query) use ($subchannel) {
                $query->where('mo_promotion.apply_subchannel_filter', 0)
                    ->orWhere(function ($query) use ($subchannel) {
                        $query->where('mo_promotion.apply_subchannel_filter', 1)
                            ->where('mo_promotion_subchannel.subchannel_id', $subchannel);
                    });
            })->where(function ($query) use ($moneda) {
                $query->where('mo_promotion.apply_currency_filter', 0)
                    ->orWhere(function ($query) use ($moneda) {
                        $query->where('mo_promotion.apply_currency_filter', 1)
                            ->where('mo_promotion_currency.currency_id', $moneda->id);
                    });
            })
            ->where(function ($query) use ($idioma) {
                $query->where('mo_promotion.apply_language_filter', 0)
                    ->orWhere(function ($query) use ($idioma) {
                        $query->where('mo_promotion.apply_language_filter', 1)
                            ->where('mo_promotion_language.language_id', $idioma->id);
                    });
            })
            ->orderby('mo_promotion.order','desc')
            ->groupBy('mo_promotion.id');

        $sql_legals = DB::connection('tenant')->table('mo_web_legal')
            ->select(
                'mo_web_legal.id',
                'mo_web_legal.type',
                'mo_web_legal_translation.legal'
            )
            ->where('mo_web_legal.deleted_at')
            ->leftJoin('mo_web_legal_translation', function ($join) use ($idioma) {
                $join->on('mo_web_legal_translation.legal_id','mo_web_legal.id')
                    ->whereNull('mo_web_legal_translation.deleted_at')
                    ->where('mo_web_legal_translation.language_id', $idioma->id);
            })->groupBy('mo_web_legal.id');

        $sql_literals = DB::connection('tenant')->table('mo_web_literal')
            ->select(
                'mo_web_literal.id',
                'mo_web_literal.name',
                'mo_web_literal_translation.translation'
            )->whereNull('mo_web_literal.deleted_at')
            ->leftJoin('mo_web_literal_translation', function ($join) use ($idioma) {
                $join->on('mo_web_literal_translation.literal_id', 'mo_web_literal.id')
                    ->whereNull('mo_web_literal_translation.deleted_at')
                    ->where('mo_web_literal_translation.language_id', $idioma->id);
            })->groupBy('mo_web_literal.id');

        $sql_social_network = DB::connection('tenant')->table('mo_web_social_network')
            ->select(
                'mo_web_social_network.social_network',
                'mo_web_social_network.external_url'
            )->whereNull('mo_web_social_network.deleted_at');

        $sql_customer_support = DB::connection('tenant')->table('mo_web_customer_support')
            ->select(
                'mo_web_customer_support.id',
                'mo_web_customer_support_translation.language_id',
                'mo_web_customer_support_translation.name',
                'mo_web_customer_support_translation.contact'
            )
            ->whereNull('mo_web_customer_support.deleted_at')
            ->join('mo_web_customer_support_translation', 'mo_web_customer_support_translation.customer_support_id', 'mo_web_customer_support.id')
            ->whereNull('mo_web_customer_support_translation.deleted_at')
            ->where('mo_web_customer_support_translation.language_id', $idioma->id);

        $sql_page = DB::connection('tenant')->table('mo_web_page')
            ->select(
                'mo_web_page.id',
                'mo_web_page.type',
                'mo_web_page_translation.content'
            )->whereNull('mo_web_page.deleted_at')
            ->leftJoin('mo_web_page_translation', function ($join) use ($idioma) {
                $join->on('mo_web_page_translation.page_id','mo_web_page.id')
                    ->whereNull('mo_web_page_translation.deleted_at')
                    ->where('mo_web_page_translation.language_id', $idioma->id);
            })
            ->groupBy('mo_web_page.id');


        $array = array();

        //Tipos de productos
        $datos_product_type = $sql_product_type->get();
        
        $array['product_type'] = array();
        foreach($datos_product_type as $product){
            $tipo_producto = array();

            $tipo_producto['id'] = $product->id;
            $tipo_producto['name'] = $product->name;

            $array['product_type'][] = $tipo_producto;

        }
        //FIN tipo de productos

        //Categorias de productos
        $datos_categories = $sql_category->get();

        $array['category'] = array();
        foreach($datos_categories as $category) {
            $array_category = array();

            $array_category['id'] = $category->id;
            $array_category['name'] = $category->name;

            $array['category'][] = $array_category;
        }
        //FIN categoria de productos

        //Promociones
        $datos_promotions = $sql_promotions->get();

        $array['promotion'] = array();
        foreach($datos_promotions as $promotion){
            $array_promotion = array();

            $array_promotion['id'] = $promotion->id;

            $array_promotion['name'] = $promotion->name;

            $array['promotion'][] = $promotion;
        }
        //FIN Promociones

        //Idiomas

        $array['language'] = languages();

        //FIN idiomas

        //Monedas 
        $datos_currency = $sql_currencies->get();

        $array['currency'] = array();

        foreach($datos_currency as $currency){
            $arary_currency = array();

            $array_currency['id'] = $currency->id;
            $array_currency['name'] = $currency->name;
            $array_currency['simbol'] = $currency->simbol;
            $array_currency['iso_number'] = $currency->iso_number;
            $array_currency['iso_code'] = $currency->iso_code;

            $array['currency'][] = $currency;

        }
        //FIN monedas

        //Settings
        $datos_settings = $sql_settings->get();

        $array['settings'] = array();

        $array_settings = array();

        foreach($datos_settings as $setting){

            if($setting->name == 'logo_image_id') {
                $datos_file = $sql_file->where('mo_file.id', $setting->value)->first();

                if($datos_file){
                    $array_settings['logo_image'] = Storage::url($datos_file->url_file);
                } else {
                    $array_settings['logo_image'] = null;
                }

            } else {
                $array_settings[$setting->name] = $setting->value;
            }
        }

        $array['settings'][] = $array_settings;
        //FIN Settings

        //Legales
        $array['legal'] = array();
        $datos_legals = $sql_legals->get();

        foreach($datos_legals as $legal){
            $array_legals = array();

            $array_legals['type'] = $legal->type;

            $array_legals['legal'] = $legal->legal != null ? $legal->legal : '';

            $array['legal'][$legal->type] = $array_legals;
        }

        //FIN legales

        //Literales
        $array['literal'] = array();

        $datos_literals = $sql_literals->get();

        foreach($datos_literals as $literal){

            $array_literal = array();

            $array_literal['name'] = $literal->name;

            $array_literal['translation'] = $literal->translation != null ? $literal->translation : '';

            $array['literal'][$literal->name] = $array_literal;

        }

        //FIN literales

        //Atencion al cliente
        $datos_customer_support = $sql_customer_support->get();

        $array['customer_support'] = array();
        foreach($datos_customer_support as $customer_support){
            $array_customer = array();

            $array_customer['id'] = $customer_support->id;
            $array_customer['name'] = $customer_support->name;
            $array_customer['contact'] = $customer_support->contact;

            $array['customer_support'][] = $array_customer;
        }
        //FIN atencion al cliente

        //Redes sociales
        $array['social_network'] = array();

        $datos_social_network = $sql_social_network->get();

        foreach($datos_social_network as $socialnetwork) {

            $array_social_network = array();

            $array_social_network['social_network'] = $socialnetwork->social_network;
            $array_social_network['external_url'] = $socialnetwork->external_url ? $socialnetwork->external_url : '';

            $array['social_network'][] = $array_social_network;
        }

        //FIN redes sociales

        //Páginas
        $datos_pages = $sql_page->get();

        $array['page'] = array();
        foreach($datos_pages as $page){
            $array_page = array();

            $array_page['id'] = $page->id;
            $array_page['type'] = $page->type;
            $array_page['content'] = $page->content ? 1 : 0;

            $array['page'][$page->type] = $array_page;
        }

        //FIN páginas

        //Petición a reserva  
        $array['shoppingcart'] = null; 
        if($request->header('Authorization') || $request->get('client_session')){
                     
            $ruta_base = str_replace($request->path(), '', $request->url());
            $ruta_final = $ruta_base.'api/v1/reservation/shoppingcart/search';
            $filters = [
                'subchannel_id' => $request->get('subchannel_id'),
                'lang' => $request->get('lang'),
                'device' => $request->get('device'),
            ];
            if($request->get('client_session')){
                $filters['client_session'] = $request->get('client_session');
            }
            if($request->get('reservation_id')){
                $filters['id'] = $request->get('reservation_id');
            }    
            
            $client = new Client();
            $res = $client->request('GET', $ruta_final, ['headers' => ['Authorization' => $request->header('Authorization'), 'SubchannelToken' => $request->header('SubchannelToken')], 'json' => $filters, 'http_errors' => false]);
            //$respuesta = response()->json(\GuzzleHttp\json_decode($res->getBody()->getContents(), true), $res->getStatusCode());

            if($res->getStatusCode() === 500){
            } else{
                $respuesta = \GuzzleHttp\json_decode($res->getBody()->getContents(), true);
                if ($res->getStatusCode() === 200) {
                    if(sizeof($respuesta['data']) > 0){
                        $array['shoppingcart']['id'] = $respuesta['data'][0]['reservation'][0]['id'];
                        $array['shoppingcart']['number'] = $respuesta['data'][0]['reservation'][0]['number'];
                    }
                } else  {                  
                } 
            }
        }

        return $array;
    }

    function languages(){
        $sql_languages = DB::connection('tenant')->table('mo_language')
            ->select(
                'mo_language.id',
                'mo_language.abbreviation',
                'mo_language.name_lang'
            )
            ->whereNull('mo_language.deleted_at')
            ->where('mo_language.front', 1);

        $datos_language = $sql_languages->get();

        $array_languages = array();

        foreach($datos_language as $language){
            $array_language = array();

            $array_language['id'] = $language->id;
            $array_language['abbreviation'] = $language->abbreviation;
            $array_language['name_lang'] = $language->name_lang;

            $array_languages[] = $array_language;
        }

        return $array_languages;
    }

    function currencies(){
        $sql_currencies = DB::connection('tenant')->table('mo_currency')
            ->select(
                'mo_currency.id',
                'mo_currency.iso_code',
                'mo_currency.name'
            )
            ->whereNull('mo_currency.deleted_at')
            ->where('mo_currency.front', 1);

        $datos_currencies = $sql_currencies->get();

        $array_currencies = array();

        foreach($datos_currencies as $currency){
            $array_currency = array();

            $array_currency['id'] = $currency->id;
            $array_currency['iso_code'] = $currency->iso_code;
            $array_currency['name'] = $currency->name;

            $array_currencies[] = $array_currency;
        }

        return $array_currencies;
    }
?>