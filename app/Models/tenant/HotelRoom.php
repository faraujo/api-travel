<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelRoom extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_hotel_room";

    protected $fillable = [
        'hotel_id',
        'category_id',
        'code',
        'uom_id',
        'order',
        'longitude',
        'latitude',
        'address',
        'pax',
    ];


    /**
     * Define relación de la tabla mo_hotel_room con la tabla mo_room_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomFile()
    {
        return $this->hasMany('App\Models\tenant\HotelRoomFile','room_id');
    }

    /**
     * Define relación de la tabla mo_hotel_room con la tabla mo_hotel_room_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomTranslation()
    {
        return $this->hasMany('App\Models\tenant\HotelRoomTranslation', 'room_id');
    }

}