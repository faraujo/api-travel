<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CostCenter extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_cost_center';

    protected $fillable = [
        'department_id',
        'name',
        'code',
        'accounting_cost_center',
        'accounting_code',
    ];

}