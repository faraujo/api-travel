<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebLiteralTranslation extends Model
{
    //
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_literal_translation";

    protected $fillable = [
        'literal_id',
        'language_id',
        'translation'
    ];

}
