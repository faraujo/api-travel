<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationConfirmedLegal extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_confirmed_legal';

    protected $fillable = [
        'id',
        'name',
    ];

}