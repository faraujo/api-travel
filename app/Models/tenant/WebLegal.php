<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebLegal extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_legal";

    protected $fillable = [
        'type'
    ];

    /**
     * Define relación de la tabla mo_web_legal con la tabla mo_web_legal_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function legalTranslation()
    {
        return $this->hasMany('App\Models\tenant\WebLegalTranslation', 'legal_id');
    }

}