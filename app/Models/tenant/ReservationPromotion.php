<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationPromotion extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_promotion';

    protected $fillable = [
        'reservation_id',
        'reservation_detail_id',
        'reservation_detail_client_type_id',
        'promotion_id',
        'promotion_order',
        'promotion_type_id',
        'currency_id',
        'promotion_type_name',
        'code',
        'coupon_code',
        'promotion_amount',
        'promotion_name',
        'promotion_short_description',
        'benefit_card_id',
        'benefit_card_name',
        'benefit_card_number',
        'promotion_amount_total',
    ];


}