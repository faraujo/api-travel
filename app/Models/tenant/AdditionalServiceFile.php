<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalServiceFile extends Model
{

    use SoftDeletes, UsesTenantConnection;

    protected $fillable = ['service_id','file_id'];

    protected $table = 'mo_additional_service_file';

}