<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationOperationStatusTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_reservation_operation_status_translation';

    protected $fillable = [
        'operation_status_id',
        'language_id',
        'name'
    ];


}