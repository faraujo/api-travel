<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordToken extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_user_recover_token';

    protected $fillable = ['user_id', 'token', 'expired_at'];

}