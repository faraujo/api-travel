<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_product_translation";

    public $fillable = [

        'language_id',
        'product_id',
        'name',
        'description',
        'features',
        'recommendations',
        'short_description',
        'friendly_url',
        'discover_url',
        'title_seo',
        'description_seo',
        'keywords_seo',
        'legal',

        'index',
        'breadcrumb',
        'rel',
        'og_title',
        'og_description',
        'og_image',
        'twitter_title',
        'twitter_description',
        'twitter_image',
        'canonical_url',
        'script_head',
        'script_body',
        'script_footer',
    ];


    /**
     * Define relación de la tabla mo_product_translation con la tabla mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo("\App\Models\tenant\Product", 'product_id');
    }

    /**
     * Define relación de la tabla mo_product_translation con la tabla mo_language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo("App\Models\tenant\Language", 'language_id');
    }

}