<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionAccumulate extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_promotion_accumulate';

    protected $fillable = [
        'promotion_id',
        'promotion_accumulate_id',
    ];


}