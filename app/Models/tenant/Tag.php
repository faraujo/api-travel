<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'order'
    ];

    protected $table = 'mo_tag';


    /**
     * Define relación de la tabla mo_tag con la tabla mo_tag_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tagTranslation()
    {
        return $this->hasMany('App\Models\tenant\TagTranslation', 'tag_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_tag, a través de tag_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productTag()
    {
        return $this->hasMany('App\Models\tenant\ProductTag', 'tag_id');
    }

}