<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TagTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_tag_translation";

    public $fillable = [

        'language_id',
        'tag_id',
        'name',
        'description',
        'short_description',
        'friendly_url',
        'title_seo',
        'description_seo',
        'keywords_seo'
    ];

    /**
     * Define relación de la tabla mo_tag_translation con la tabla mo_tag
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo("\App\Models\tenant\Tag", 'tag_id');
    }

    /**
     * Define relación de la tabla mo_tag_translation con la tabla mo_language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo("App\Models\tenant\Language", 'language_id');
    }

}