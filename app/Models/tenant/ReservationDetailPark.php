<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDetailPark extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_detail_park';

    protected $fillable = [
        'reservation_detail_id',
        'date',
        'date_visit_limit',
        'session',
    ];

}