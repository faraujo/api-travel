<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_currency";

    protected $fillable = ['name', 'simbol', 'iso_number', 'iso_code', 'presentation_simbol', 'position', 'service_exchange',
        'decimal_separator', 'thousand_separator','front','automatic_conversion', 'automatic_service', 'automatic_environment', 'automatic_user', 'automatic_password',
        'automatic_token'];

}