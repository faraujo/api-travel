<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionProductType extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_product_type';

    public $fillable = [
        'id',
        'promotion_id',
        'product_type_id',
        'quantity',
        'min_quantity',
        'max_quantity',
        'series_one',
        'series_two',
        'promotion_amount',
        'promotion_type_id',
        'currency_id',
        'apply_client_type_filter',
    ];

}