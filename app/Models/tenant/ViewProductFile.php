<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViewProductFile extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_view_product_file';
    protected $fillable = [
        'view_product_id',
        'file_id',
    ];

}