<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionServiceClientType extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_promotion_service_client_type';

    protected $fillable = ['promotion_service_id','client_type_id'];


}