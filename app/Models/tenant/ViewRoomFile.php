<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViewRoomFile extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_view_room_file';

    protected $fillable = [
        'view_room_id',
        'subchannel_id',
        'order',
        'file_id',
    ];
    

}