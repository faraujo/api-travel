<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelRatePlan extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_hotel_rate_plan';

}