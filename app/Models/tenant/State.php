<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class State extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'id',
        'country_id',
        'iso2',
        'iso3',
        'name',
    ];

    protected $table = 'mo_state';

}