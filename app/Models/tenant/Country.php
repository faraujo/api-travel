<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Country extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_country";

    protected $fillable = [
        'id',
        'continent_id',
        'iso2',
        'iso3',
        'order'
    ];

}