<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 01/08/2018
 * Time: 14:02
 */

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductType extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_product_type';


}