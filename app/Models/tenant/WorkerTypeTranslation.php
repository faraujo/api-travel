<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkerTypeTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_worker_type_translation';

    protected $fillable = ['worker_type_id', 'language_id', 'name', 'description'];

}