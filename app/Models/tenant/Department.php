<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_department';

    protected $fillable = [
        'name',
        'code',
        'accounting_code',
    ];

}