<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViewRoom extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_view_room';
    protected $fillable = [
        'room_id',
        'subchannel_id',
        'order',
        'published',
    ];


    /**
     * Define relación de la tabla mo_view_room con la tabla mo_view_room_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function viewRoomTranslation()
    {
        return $this->hasMany('App\Models\tenant\ViewRoomTranslation', 'view_room_id');
    }

    /**
     * Define relación de la tabla mo_view_room con la tabla mo_view_room_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function viewRoomFile()
    {
        return $this->hasMany('App\Models\tenant\ViewRoomFile','view_room_id');
    }

}