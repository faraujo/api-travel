<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationCommissionAgentLog extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_commission_agent_log';

    protected $fillable = [
        'reservation_id',
        'supervisor_reassignment_id',
        'old_agent_id',
        'new_agent_id'
    ];

}