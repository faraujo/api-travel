<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductLocation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_product_location";

    public $fillable = [
        'product_id',
        'location_id',
    ];

}