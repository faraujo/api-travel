<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionPaymentMethod extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_payment_method';

    public $fillable = [
        'promotion_id',
        'payment_method_id',
    ];

}