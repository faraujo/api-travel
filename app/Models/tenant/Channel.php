<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Channel extends Model
{
    use softDeletes, UsesTenantConnection;
    
    protected $table = 'mo_channel';

    protected $fillable = ['id'];


        /**
     * Define relación de la tabla mo_channel con la tabla mo_channel_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function channelTranslation()
    {
        return $this->hasMany('App\Models\tenant\ChannelTranslation', 'channel_id');
    }

}