<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_role';

    protected $fillable = ['name', 'description', 'deleted_at'];


    /**
     * Define relación de la tabla mo_role con la tabla mo_role_permission
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissionRole()
    {
        return $this->hasMany('App\Models\tenant\RolePermission', 'role_id');
    }

    /**
     * Define relación de la tabla mo_role con la tabla mo_user_role
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roleUser()
    {
        return $this->hasMany('App\Models\tenant\UserRole', 'role_id');
    }

}