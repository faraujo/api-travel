<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_email_template";

    protected $fillable = ['name','description','file_name'];


}