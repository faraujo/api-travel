<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelRoomFile extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_hotel_room_file';

    protected $fillable = [
        'room_id',
        'file_id',
    ];

}