<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class campaignSubchannel extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_campaign_subchannel';
    public $fillable = [
        'campaign_id',
        'subchannel_id',
    ];

}