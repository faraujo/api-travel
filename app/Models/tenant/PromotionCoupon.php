<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionCoupon extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_coupon';

    protected $fillable = [
        'promotion_id',
    ];


}