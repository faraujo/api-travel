<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 05/03/2019
 * Time: 17:34
 */

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IdentificationUser extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_user_identification';


}