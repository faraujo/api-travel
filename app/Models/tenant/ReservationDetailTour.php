<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

class ReservationDetailTour extends Model
{
    use UsesTenantConnection;

    protected $table = 'mo_reservation_detail_tour';

    protected $fillable = [
        'reservation_detail_id',
        'date',
        'session',
    ];

}