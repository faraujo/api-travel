<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Settings
 *
 * @mixin \Eloquent
 */
class FileResize extends Model
{
    use UsesTenantConnection;

    protected $table = "mo_file_resize";

    protected $fillable = ['prefix', 'value'];

}