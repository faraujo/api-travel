<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CurrencyExchange extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_currency_exchange';

    protected $fillable = ['currency_id','user_id','date_time_start','exchange'];

}