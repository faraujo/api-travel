<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion';

    protected $fillable = [
        'code',
        'order',
        'visibility',
        'accumulate',
        'min_quantity',
        'max_quantity',
        'quantity',
        'promotion_amount',
        'currency_id',
        'promotion_type_id',
        'campaign_id',
        'department_id',
        'cost_center_id',
        'apply_product_filter',
        'apply_product_type_filter',
        'apply_service_filter',
        'apply_subchannel_filter',
        'apply_country_filter',
        'apply_state_filter',
        'apply_currency_filter',
        'apply_day_filter',
        'apply_language_filter',
        'apply_client_type_filter',
        'apply_pickup_filter',
        'apply_location_filter',
        'apply_payment_method_filter',
        'apply_device_filter',
        'lockers_validation',
        'supervisor_validation',
        'allow_date_change',
        'allow_upgrade',
        'allow_product_change',
        'pay_difference',
        'anticipation_start',
        'anticipation_end',
        'sale_date_start',
        'sale_date_end',
        'enjoy_date_start',
        'enjoy_date_end',
        'validity_date_start',
        'validity_date_end',
        'coupon_code',
        'coupon_type',
        'coupon_sheet_start',
        'coupon_sheet_end',
        'coupon_total_uses',
        'user_id',
        'benefit_card',
        'benefit_card_total_uses',
        'benefit_card_id'
    ];
    

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionTranslation()
    {
        return $this->hasMany('App\Models\tenant\PromotionTranslation', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_subchannel, a través de subchannel_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionChannel()
    {
        return $this->hasMany('App\Models\tenant\PromotionSubchannel', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_country, a través de country_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionCountry()
    {
        return $this->hasMany('App\Models\tenant\PromotionCountry', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_state, a través de state_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionState()
    {
        return $this->hasMany('App\Models\tenant\PromotionState', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_product, a través de product_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionProduct()
    {
        return $this->hasMany('App\Models\tenant\PromotionProduct', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_product_type, a través de product_type_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionProductType()
    {
        return $this->hasMany('App\Models\tenant\PromotionProductType', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_service, a través de service_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionService()
    {
        return $this->hasMany('App\Models\tenant\PromotionService', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_currency, a través de currency_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionCurrency()
    {
        return $this->hasMany('App\Models\tenant\PromotionCurrency', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_payment_method, a través de payment_method_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionPaymentMethod()
    {
        return $this->hasMany('App\Models\tenant\PromotionPaymentMethod', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_language, a través de language_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionLanguage()
    {
        return $this->hasMany('App\Models\tenant\PromotionLanguage', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_pickup, a través de promotion_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionPickup()
    {
        return $this->hasMany('App\Models\tenant\PromotionPickup', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionFile()
    {
        return $this->hasMany('App\Models\tenant\PromotionFile', 'promotion_id');
    }

    /**
     * Define la relación de la tabla mo_promotion con la tabla mo_promotion_client_type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionClientType()
    {
        return $this->hasMany('App\Models\tenant\PromotionClientType', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_range_sale_day
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionRangeSaleDay()
    {
        return $this->hasMany('App\Models\tenant\PromotionRangeSaleDay', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_range_enjoy_day
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionRangeEnjoyDay()
    {
        return $this->hasMany('App\Models\tenant\PromotionRangeEnjoyDay', 'promotion_id');
    }

    /**
     * Define relación de la tabla mo_promotion con la tabla mo_promotion_coupon
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionCoupon()
    {
        return $this->hasMany('App\Models\tenant\PromotionCoupon', 'promotion_id');
    }

    /**
     * Define la relación de la tabla mo_promotion con la tabla mo_promotion_device
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionDevice()
    {
        return $this->hasMany('App\Models\tenant\PromotionDevice', 'promotion_id');
    }

    /**
     * Define la relación de la tabla mo_promotion con la tabla mo_promotion_accumulate
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotionAccumulate()
    {
        return $this->hasMany('App\Models\tenant\PromotionAccumulate', 'promotion_id');
    }

}