<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $fillable = ['text', 'rating', 'user_id', 'language_id', 'product_id', 'subchannel_id', 'updated_by', 'updated_when','comment_status_id'];

    protected $table = 'mo_comment';


    /**
     * Define la relación de mo_comment con mo_language
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Models\tenant\Language', 'language_id');
    }

    /**
     * Define la relación de mo_comment con mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function product()
    {
        return $this->belongsTo('\App\Models\tenant\Product', 'product_id');
    }

    /**
     * Define la relación de mo_comment con mo_user
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo('\App\Models\tenant\User', 'user_id');
    }

}