<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PriceProduct extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_price_product';

    protected $fillable = [
        'price_id',
        'session',
        'product_id',
        'service_id',
        'net_price',
        'sale_price',
        'markup',
        'price_before',
        'pickup_id',
        'client_type_id',
        'individual_sale',
        'location_id',
        'tip_amount',
        'tip_percent',
        'commission_amount',
        'commission_percent',
    ];

}