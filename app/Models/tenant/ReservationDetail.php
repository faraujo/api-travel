<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDetail extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_reservation_detail';

    protected $fillable = [
        'reservation_id',
        'subchannel_id',
        'product_id',
        'product_name',
        'product_code',
        'service_id',
        'service_name',
        'location_id',
        'location_name',
        'contract_id',
        'price_id',
        'currency_id',
        'currency_exchange_id',
        'iva',
        'date_time_start',
        'date_time_expiration',
        'operation_status_id',
        'payment_status_id',
        'agent_id',
        'ticketing_send',
        'ticketing_response',
        'ticketing_number',
        'ticketing_cashier_name',
        'ticketing_agent_name',
        'ticketing_number_operation',
        'ticketing_date_time_notification',
        'date_time_operation',
        'date_time_cancellation',
        'notes'
    ];


    /**
     * Define relación de la tabla mo_reservation_detail con la tabla mo_reservation_detail_client_type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetailClientType()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetailClientType', 'reservation_detail_id');
    }

    /**
     * Define relación de la tabla mo_reservation_detail con la tabla mo_reservation_detail_hotel
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetailHotel()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetailHotel', 'reservation_detail_id');
    }

    /**
     * Define relación de la tabla mo_reservation_detail con la tabla mo_reservation_detail_activity
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetailActivity()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetailActivity', 'reservation_detail_id');
    }

    /**
     * Define relación de la tabla mo_reservation_detail con la tabla mo_reservation_detail_tour
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetailTour()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetailTour', 'reservation_detail_id');
    }

    /**
     * Define relación de la tabla mo_reservation_detail con la tabla mo_reservation_detail_park
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetailPark()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetailPark', 'reservation_detail_id');
    }


    /**
     * Define relación de la tabla mo_reservation_detail con la tabla mo_reservation_detail_restaurant
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetailRestaurant()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetailRestaurant', 'reservation_detail_id');
    }

    /**
     * Define relación de la tabla mo_reservation_detail con la tabla mo_reservation_detail_restaurant
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetailTransportation()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetailTransportation', 'reservation_detail_id');
    }


}