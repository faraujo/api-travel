<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Iva extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_iva';

    public $fillable = ['value'];

    /**
     * Define relación de la tabla mo_iva con la tabla mo_iva_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ivaTranslation()
    {
        return $this->hasMany('App\Models\tenant\IvaTranslation', 'iva_id');
    }

}