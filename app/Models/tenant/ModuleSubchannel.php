<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleSubchannel extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_module_subchannel";

    public $fillable = [

        'module_id',
        'subchannel_id',
    ];

}