<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebSliderFile extends Model
{
    //
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_slider_file";

    protected $fillable = [
        'file_id',
        'slider_id'
    ];
}
