<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 01/02/2019
 * Time: 10:54
 */

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationAntifraudLevel extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_anti_fraud_level';

    protected $fillable = [
        'id',
        'code',
        'name',
    ];
}