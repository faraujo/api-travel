<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionCurrency extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_currency';

    public $fillable = [
        'promotion_id',
        'currency_id',
    ];

}