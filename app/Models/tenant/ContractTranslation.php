<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_contract_model_translation';

    public $fillable = [
        'language_id',
        'contract_model_id',
        'name',

    ];

}