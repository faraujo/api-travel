<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractModelProduct extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_contract_model_product';

    protected $fillable = [
        'contract_model_id',
        'product_id',
        'service_id',
        'time_expiration',
        'time_limit_enjoyment',
        'apply_cancel_penalization',
        'individual_sale',
        'required_service',
    ];

}