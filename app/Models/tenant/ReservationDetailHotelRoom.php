<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDetailHotelRoom extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_detail_hotel_room';

    protected $fillable = [
        'reservation_hotel_id',
        'room_id',
        'confirmation_code',
        'room_type_name',
        'rate_plan_code',
        'external_request',
        'external_response'
    ];

}