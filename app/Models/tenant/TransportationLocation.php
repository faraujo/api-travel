<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportationLocation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_transportation_location';

    public $fillable = [
        'name',
        'api_code'
    ];

}