<?php

namespace App\Models\tenant;



use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRememberToken extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_user_remember_token';

    protected $fillable = ['user_id','ip','token'];


}