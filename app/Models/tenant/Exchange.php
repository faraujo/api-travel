<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exchange extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_currency_exchange";

    protected $fillable = ['currency_id','date_time_start','exchange','user_id'];

}