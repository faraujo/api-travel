<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'type_id',
        'brand_id',
        'company_id',
        'uom_id',
        'code',
        'order',
        'billable',
        'height_from',
        'height_to',
        'weight_from',
        'weight_to',
        'longitude',
        'latitude',
        'address',
        'pax',
        'cost_center',
        'accounting_account',
        'salable',
        'iva_id',
        'age_free_to',
        'age_child_to',
        'age_from',
        'age_to',
        'depends_on',
        'home_app_hotel',
        'app_hotel_service',
        'app_hotel_without_reservation',
        'app_hotel_afi_park',
        'app_hotel_afi_tour',
        'app_hotel_menu_service',
        'app_hotel_with_service',
        'app_hotel_with_service_direct',
    ];

    protected $table = 'mo_product';

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productTranslation()
    {
        return $this->hasMany('App\Models\tenant\ProductTranslation', 'product_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productType()
    {
        return $this->belongsTo('App\Models\tenant\Type', 'type_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_package
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productPackagePro()
    {
        return $this->hasMany('App\Models\tenant\ProductPackage', 'product_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_package
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productPackagePack()
    {
        return $this->hasMany('App\Models\tenant\ProductPackage', 'package_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_service
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productService()
    {
        return $this->hasMany('App\Models\tenant\ProductService', 'product_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productCategory()
    {
        return $this->hasMany('App\Models\tenant\ProductCategory', 'product_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_tag
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productTag()
    {
        return $this->hasMany('App\Models\tenant\ProductTag', 'product_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productFile()
    {
        return $this->hasMany('App\Models\tenant\ProductFile','product_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_product_location
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productLocation()
    {
        return $this->hasMany('App\Models\tenant\ProductLocation','product_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_availability
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productAvailability()
    {
        return $this->hasMany('App\Models\tenant\Availability', 'product_id');
    }

}