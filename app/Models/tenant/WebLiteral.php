<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebLiteral extends Model
{
    //
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_literal";

    protected $fillable = [
        'name'
    ];

    /**
     * Define relación de la tabla mo_web_literal con la tabla mo_web_literal_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function literalTranslation()
    {
        return $this->hasMany('App\Models\tenant\WebLiteralTranslation', 'literal_id');
    }
}
