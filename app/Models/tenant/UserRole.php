<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_user_role";

    protected $fillable = ['user_id', 'role_id'];


}