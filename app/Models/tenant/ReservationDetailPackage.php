<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDetailPackage extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_detail_package';

    protected $fillable = [
        'date',
        'date_visit_limit',
        'session',
        'reservation_detail_id',
        'product_id',
        'product_name',
        'location_name',
        'location_id',
    ];


}