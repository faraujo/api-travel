<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentTypeTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_document_type_translation';

    protected $fillable = ['document_type_id','language_id','name'];

}