<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDetailClientType extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_reservation_detail_client_type';

    protected $fillable = [
        'reservation_detail_id',
        'client_type_id',
        'age',
        'client_type_name',
        'client_id',
        'client_name',
        'client_surname',
        'client_country_id',
        'client_state_id',
        'client_telephone1',
        'client_email',
        'original_net_price',
        'original_sale_price',
        'original_markup',
        'net_price',
        'sale_price',
        'markup',
    ];


}