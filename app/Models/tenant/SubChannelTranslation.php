<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubChannelTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_subchannel_translation';

    public $fillable = [

        'language_id',
        'subchannel_id',
        'name',

    ];

}