<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_product_category";

    public $fillable = [
        'category_id',
        'product_id',
    ];

    /**
     * Define relación de la tabla mo_product_category con la tabla mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo("\App\Models\tenant\Product", 'product_id');
    }

    /**
     * Define relación de la tabla mo_product_category con la tabla mo_category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo("\App\Models\tenant\Category", 'category_id');
    }

}