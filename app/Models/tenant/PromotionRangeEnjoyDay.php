<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionRangeEnjoyDay extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_range_enjoy_day';

    public $fillable = [
        'promotion_id',
        'enjoy_date_start',
        'enjoy_date_end'
    ];

}