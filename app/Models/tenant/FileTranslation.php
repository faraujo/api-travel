<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileTranslation extends Model
{

    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_file_translation';

    protected $fillable = [
        'file_id',
        'name',
        'description',
        'language_id',
        'alternative_text',
    ];

}