<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebSocialNetwork extends Model
{
    //

    protected $table = 'mo_web_social_network';

    protected $fillable = [
        'social_network',
        'external_url'
    ];

    use SoftDeletes, UsesTenantConnection;
}
