<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_campaign_translation';

    public $fillable = [
        'language_id',
        'campaign_id',
        'name',
        'description',
    ];

}