<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationPayment extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_reservation_payment';

    protected $fillable = [
        'reservation_id',
        'reservation_detail_id',
        'payment_method_id',
        'subchannel_id',
        'client_id',
        'agent_id',
        'client_session',
        'currency_id',
        'amount',
        'currency_exchange_id',
        'date_time',
        'status_ok',
        'code',
        'response',
        'response_code',
        'request_paypal',
        'response_paypal',
        //'response_company_code',
        //'response_company_name',
        //'compropago_place',
        //'response_mop',
        //'response_code_mop',
        'credit_card',
        'name',
        'surname',
        'bank',
        'bank_name',
        'months_no_interest'
    ];

}