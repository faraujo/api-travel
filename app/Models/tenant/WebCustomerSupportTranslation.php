<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebCustomerSupportTranslation extends Model
{

    use SoftDeletes, UsesTenantConnection;
    //

    protected $table = "mo_web_customer_support_translation";

    protected $fillable = [
        'language_id',
        'customer_support_id',
        'name',
        'contact'
    ];
    
}
