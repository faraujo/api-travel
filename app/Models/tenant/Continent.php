<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Continent extends Model
{

    use SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'id',
        'iso2',
    ];

    protected $table = 'mo_continent';

}