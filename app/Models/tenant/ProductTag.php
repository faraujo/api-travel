<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTag extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_product_tag";

    public $fillable = [
        'tag_id',
        'product_id',
    ];


    /**
     * Define relación de la tabla mo_product_tag con la tabla mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo("\App\Models\tenant\Product", 'product_id');
    }

    /**
     * Define relación de la tabla mo_product_tag con la tabla mo_tag
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo("\App\Models\tenant\Tag", 'tag_id');
    }

}