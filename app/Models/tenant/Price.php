<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_price';

    protected $fillable = ['contract_model_id', 'currency_id', 'promotions', 'date_end', 'date_start', 'user_id'];

    /**
     * Define relación de la tabla mo_price con la tabla mo_price_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function priceProduct()
    {
        return $this->hasMany('App\Models\tenant\PriceProduct', 'price_id');
    }
}