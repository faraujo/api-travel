<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPackage extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_product_package";

    public $fillable = [
        'product_id',
        'package_id',
        'location_id',
    ];

    /**
     * Define relación de la tabla mo_product_package con la tabla mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo("\App\Models\tenant\Product", 'product_id');
    }

    /**
     * Define relación de la tabla mo_product_package con la tabla mo_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package()
    {
        return $this->belongsTo("\App\Models\tenant\Product", 'product_id');
    }



}