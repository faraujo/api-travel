<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UomTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_uom_translation';

    protected $fillable = ['language_id', 'uom_id', 'name','description'];

}