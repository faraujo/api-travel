<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationLegal extends Model
{
    use SoftDeletes, UsesTenantConnection;
    protected $table = 'mo_reservation_legal';

    protected $fillable = [
        'id',
        'name',
    ];

    
}