<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_reservation';

    protected $fillable = [
        'code',
        'notes',
        'subchannel_id',
        'language_id',
        'client_id',
        'client_name',
        'client_surname',
        'client_country_id',
        'client_state_id',
        'client_telephone1',
        'client_email',
        'client_session',
        'commission_agent_id',
        'anti_fraud_code',
        'credit_card',
        'credit_card_number',
        'credit_card_cvv',
        'credit_expiry_date',
        'credit_card_type',
        'credit_card_name',
        'credit_card_surname',
        'salesforce_id',
    ];


    /**
     * Define relación de la tabla mo_reservation con la tabla mo_reservation_detail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservationDetail()
    {
        return $this->hasMany('App\Models\tenant\ReservationDetail', 'reservation_id');
    }

}