<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PromotionSubchannel extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_promotion_subchannel';
    public $fillable = [
        'promotion_id',
        'subchannel_id',
    ];

}