<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductFile extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_product_file';
    protected $fillable = [
        'product_id',
        'file_id',
    ];

}