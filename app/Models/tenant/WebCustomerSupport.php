<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebCustomerSupport extends Model
{
    //
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_customer_support";

    protected $fillable = [
        'name',
        'contact'
    ];

    /**
     * Define relación de la tabla mo_web_customer_support con la tabla mo_web_customer_support_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customerSupportTranslation()
    {
        return $this->hasMany('App\Models\tenant\WebCustomerSupportTranslation', 'customer_support_id');
    }
}
