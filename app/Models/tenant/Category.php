<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'order'
    ];

    protected $table = 'mo_category';

    /**
     * Define relación de la tabla mo_category con la tabla mo_category_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryTranslation()
    {
        return $this->hasMany('App\Models\tenant\CategoryTranslation', 'category_id');
    }

    /**
     * Define relación de la tabla mo_product con la tabla mo_category, a través de category_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productCategoryCat()
    {
        return $this->hasMany('App\Models\tenant\ProductCategory', 'category_id');
    }

    /**
     * Define relación de la tabla mo_category con la tabla mo_category_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryFile()
    {
        return $this->hasMany('App\Models\tenant\CategoryFile','category_id');
    }

}