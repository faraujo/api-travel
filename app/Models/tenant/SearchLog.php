<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SearchLog extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $fillable = [
        'subchannel_id','language_id','tag_id','category_id','product_id','user_id','product_type_id',
        'currency_id','money','availability','date','depends_on','category_id_depends',
        'searched_word','total_first_search_results','total_second_search_results','result','hotel_room_id','hotel_room_category_id','hotel_date_start',
        'hotel_date_end','hotel_adult','hotel_child','hotel_number_room','hotel_info_only'
    ];

    protected $table = 'mo_search_log';


}