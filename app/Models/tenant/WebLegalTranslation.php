<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebLegalTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_legal_translation";

    protected $fillable = [
        'legal_id',
        'language_id',
        'legal'
    ];

}