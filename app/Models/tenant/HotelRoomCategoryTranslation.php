<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelRoomCategoryTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_hotel_room_category_translation';

    protected $fillable = [
        'language_id',
        'category_id',
        'name',
        'description'
    ];


}