<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebSlider extends Model
{
    //
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_slider";

    protected $fillable = [
        'name',
    ];

    /**
     * Define relación de la tabla mo_web_slider con la tabla mo_web_slider_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sliderFiles()
    {
        return $this->hasMany('App\Models\tenant\WebSliderFile', 'slider_id');
    }
}
