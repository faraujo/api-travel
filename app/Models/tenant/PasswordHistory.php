<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordHistory extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_password_history';

    protected $fillable = ['user_id', 'password'];


}