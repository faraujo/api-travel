<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HotelRoomTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_hotel_room_translation';

    protected $fillable = [
        'language_id',
        'room_id',
        'name',
        'description',
        'short_description',
        'location',
        'views',
        'size',
        'capacity',
        'url_360',
        'friendly_url',
        'title_seo',
        'description_seo',
        'keywords_seo',
        'legal',
        'services_included',
        'services_not_included',

        'index',
        'breadcrumb',
        'rel',
        'og_title',
        'og_description',
        'og_image',
        'twitter_title',
        'twitter_description',
        'twitter_image',
        'canonical_url',
        'script_head',
        'script_body',
        'script_footer',
    ];

}