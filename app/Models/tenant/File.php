<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class File extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_file";

    protected $fillable = ['type_id', 'order', 'url_file','private', 'file_name','file_dimensions','mimetype', 'file_size',
        'device_id','key_file'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileTranslation()
    {
        return $this->hasMany('App\Models\tenant\FileTranslation', 'file_id');
    }
}