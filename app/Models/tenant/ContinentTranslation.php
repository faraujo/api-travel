<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContinentTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_continent_translation";

    public $fillable = [
        'continent_id',
        'language_id',
        'name',
    ];

}