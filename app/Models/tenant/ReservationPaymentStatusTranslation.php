<?php
/**
 * Created by PhpStorm.
 * User: Viavox
 * Date: 25/10/2018
 * Time: 8:20
 */

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationPaymentStatusTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_reservation_payment_status_translation';

    protected $fillable = [
        'payment_status_id',
        'language_id',
        'name',
    ];

}