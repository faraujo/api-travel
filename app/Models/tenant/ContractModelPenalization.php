<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractModelPenalization extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_contract_model_penalization';

    protected $fillable = [
        'contract_model_product_id',
        'days',
        'penalization_amount',
    ];

}