<?php

namespace App\Models\tenant;

//use App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_product_type_translation';

    protected $fillable = ['language_id', 'type_id', 'name'];


    /**
     * Define la relación de la tabla mo_type_translation con la tabla mo_type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Models\tenant\Type', 'type_id');
    }

    /**
     * Define la relación de la tabla mo_type_translation con la tabla mo_language
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo("App\Models\tenant\Language", 'language_id');
    }

}