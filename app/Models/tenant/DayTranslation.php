<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

class DayTranslation extends Model
{
    use UsesTenantConnection;

    protected $table = 'mo_day_translation';

}