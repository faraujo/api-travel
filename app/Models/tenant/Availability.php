<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Availability extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $fillable = ['date','session','product_id','service_id','location_id','quota','avail','overbooking','overbooking_unit', 'closed','unlimited'];

    protected $table = "mo_availability";

}