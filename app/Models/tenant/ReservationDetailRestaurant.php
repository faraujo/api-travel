<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDetailRestaurant extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_detail_restaurant';

    protected $fillable = [
        'reservation_detail_id',
        'date',
        'session',
    ];

}