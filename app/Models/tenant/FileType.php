<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FileType extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_file_type";


}