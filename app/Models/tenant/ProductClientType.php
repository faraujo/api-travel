<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductClientType extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_product_client_type';

    protected $fillable = ['product_id', 'client_type_id'];


}