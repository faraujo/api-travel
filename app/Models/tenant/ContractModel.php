<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractModel extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_contract_model';

    protected $fillable = ['time_lock_avail'];

    /**
     * Define relación de la tabla mo_contract_model con la tabla mo_contract_model_product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contractModelProduct()
    {
        return $this->hasMany('App\Models\tenant\ContractModelProduct', 'contract_model_id');
    }

    /**
     * Define relación de la tabla mo_contract con la tabla mo_contract_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contractModelTranslation()
    {
        return $this->hasMany('App\Models\tenant\ContractTranslation', 'contract_model_id');
    }

}