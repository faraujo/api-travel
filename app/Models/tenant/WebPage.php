<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebPage extends Model
{
    //
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_page";

    protected $fillable = [
        'type',
    ];

    /**
     * Define relación de la tabla mo_web_page con la tabla mo_web_page_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pageTranslation()
    {
        return $this->hasMany('App\Models\tenant\WebPageTranslation', 'page_id');
    }
}
