<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChannelTranslation extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_channel_translation';

    public $fillable = [

        'language_id',
        'channel_id',
        'name',

    ];


}