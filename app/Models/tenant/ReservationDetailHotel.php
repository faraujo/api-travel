<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationDetailHotel extends Model
{
    use SoftDeletes, UsesTenantConnection;

    protected $table = 'mo_reservation_detail_hotel';

    protected $fillable = [
        'reservation_detail_id',
        'date_start',
        'date_end',
        'arrival_airline',
        'arrival_session',
        'arrival_flight_number',
        'arrival_terminal',
        'departure_airline',
        'departure_session',
        'departure_flight_number',
        'departure_terminal',
    ];

}