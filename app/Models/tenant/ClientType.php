<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientType extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_client_type';

    protected $fillable = ['id','from','to','code', 'api_id', 'hotel_id', 'hotel_code', 'hotel_age_required'];


    /**
     * Define relación de la tabla mo_client_type con la tabla mo_client_type_translation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientTypeTranslation()
    {
        return $this->hasMany('App\Models\tenant\ClientTypeTranslation', 'client_type_id');
    }



    /**
     * Define relación de la tabla mo_client_type con la tabla mo_promotion_product_client_type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PromotionClientType()
    {
        return $this->hasMany('\App\Models\tenant\PromotionClientType','client_type_id');
    }

    /**
     * Define relación de la tabla mo_client_type con la tabla mo_promotion_product_client_type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PromotionProductClientType()
    {
        return $this->hasMany('\App\Models\tenant\PromotionProductClientType','client_type_id');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PromotionProductTypeClientType()
    {
        return $this->hasMany('\App\Models\tenant\PromotionProductTypeClientType','client_type_id');
    }
}