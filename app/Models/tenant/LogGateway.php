<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogGateway extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = "mo_gateway_log";

    protected $fillable = ['user_id', 'start_petition','end_petition','input_data', 'output_data','petition_ip'];

}