<?php

namespace App\Models\tenant;


use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportationLocationDetail extends Model
{
    use SoftDeletes, UsesTenantConnection;
    
    protected $table = 'mo_transportation_location_detail';

    public $fillable = [
        'transportation_location_id',
        'product_id',
        'subchannel_id',
        'name',
        'code',
        'date',
        'session',
    ];

}