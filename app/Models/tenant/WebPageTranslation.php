<?php

namespace App\Models\tenant;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebPageTranslation extends Model
{
    //
    use SoftDeletes, UsesTenantConnection;

    protected $table = "mo_web_page_translation";

    protected $fillable = [
        'language_id',
        'page_id',
        'content'
    ];
}
