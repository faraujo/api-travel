<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasPaymentMethod extends Model
{
    //
    use SoftDeletes;

    protected $table = 'saas_payment_method';

    protected $fillable = [
    ];
}
