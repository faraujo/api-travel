<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\SaasUserValidateEmailToken;
use App\Models\SaasSettings;
use App\Models\SaasUser;
use Illuminate\Http\Request;

function websitePlanRules($web) {
    $now = Carbon::now()->format('Y-m-d H:i:s');
    $plans = DB::table('saas_website_plan_rule')->where('website_id', $web->id)
                ->select('saas_website_plan_rule.code', 'saas_website_plan_rule.date_start', 'saas_website_plan_rule.date_end', 'saas_website_plan_rule.status_id',
                    'saas_website_plan_rule.plan_id', 'saas_website_plan_rule.rule_id', 'saas_website_plan_rule.value', 'saas_website_plan_rule.unlimited', 'saas_website_plan_rule.sale_price', 'saas_rule.code as rule_code',
                    'saas_website_plan_rule.id as plan_rule_id', 'saas_website_plan_rule.cancelled')
                ->whereNull('saas_website_plan_rule.deleted_at')
                ->leftJoin('saas_rule', function ($join) {
                    $join->on('saas_rule.id','saas_website_plan_rule.rule_id')
                        ->whereNull('saas_rule.deleted_at');
                })
                ->where(function ($query) use ($now) {
                    $query->where('saas_website_plan_rule.date_start', '<=', $now)
                        ->where('saas_website_plan_rule.date_end', '>=', $now);
                })
                ->whereNull('saas_website_plan_rule.date_replace')
                ->where('saas_website_plan_rule.status_id', '=', 2)
                ->get();

    return $plans;
}

function websiteRules($plan) {
    $plan_rules = DB::table('saas_plan_rule')->where('plan_id', $plan->plan_id)
    ->select('saas_plan_rule.id', 'saas_plan_rule.plan_id', 'saas_plan_rule.rule_id',
        'saas_plan_rule.value', 'saas_plan_rule.unlimited','saas_rule.code')
    ->whereNull('saas_plan_rule.deleted_at')
    ->join('saas_rule', 'saas_rule.id','saas_plan_rule.rule_id')
    ->whereNull('saas_rule.deleted_at')
    ->get();

    return $plan_rules;

}
 
function validateCif ($cif /*$check*/) {
    $cif_codes = 'JABCDEFGHI';

    // $cif = array_pop ($check);

    $sum = (string) getCifSum ($cif);
    $n = (10 - substr ($sum, -1)) % 10;

    if (preg_match ('/^[ABCDEFGHJNPQRSUVW]{1}/', $cif)) {
        if (in_array ($cif[0], array ('A', 'B', 'E', 'H'))) {
            // Numerico
            return ($cif[8] == $n);
        } elseif (in_array ($cif[0], array ('K', 'P', 'Q', 'S'))) {
            // Letras
            return ($cif[8] == $cif_codes[$n]);
        } else {
            // Alfanumérico
            if (is_numeric ($cif[8])) {
                return ($cif[8] == $n);
            } else {
                return ($cif[8] == $cif_codes[$n]);
            }
        }
    }

    return false;
}
function getCifSum($cif) {
    $sum = $cif[2] + $cif[4] + $cif[6];

    for ($i = 1; $i<8; $i += 2) {
      $tmp = (string) (2 * $cif[$i]);

      $tmp = $tmp[0] + ((strlen ($tmp) == 2) ?  $tmp[1] : 0);

      $sum += $tmp;
    }

    return $sum;
  }

  function sendValidateEmail($user_id) {
        $user = SaasUser::where('id','=',$user_id)->first();
        
       //Generación del token para el email de verificación
       $verify_email_token = $user->createToken('MotorOnline')->accessToken;
       
       SaasUserValidateEmailToken::create([
           'user_id' => $user->id,
           'token' => $verify_email_token
       ]);

       $ruta_verify_email = SaasSettings::where('name', '=', 'validate_email_route_front')->first()->value;
     
       $emails = [
           '0' => [
               'email' => $user->email,
               'subject_email' => 'Verificar email',
           ]
       ];
       
       $controller = app(\App\Http\Controllers\SaasEmailController::class);

       $peticion = new Request();

       $peticion->setMethod('POST');

       $peticion->request->add(['title' => 'Verificar email']);

       $peticion->request->add(['body' => $ruta_verify_email . '?token=' . $verify_email_token]);

       $peticion->request->add(['template' => 'verify']);

       $peticion->request->add(['emails' => $emails]);
       
       $controller->send($peticion);
      
       //

  }

  function paymentEmail($id, $concepto = null, $amount = 0) {

    $datos_compra = DB::table('saas_user')->select(
        'saas_user.name',
        'saas_user.surname',
        'saas_user.email',
        'saas_website.name as website_name',
        'saas_website.domain',
        'saas_website.status_id',
        'saas_website_plan_rule.code',
        'saas_website_plan_rule.plan_id',
        'saas_website_plan_rule.rule_id',
        'saas_website_plan_rule.date_end',
        'saas_website_plan_rule.cancelled',
        'saas_website_plan_rule.payment_method_id',
        'saas_website_plan_rule.subscription_code'
    )->whereNull('saas_user.deleted_at')
    ->join('saas_website','saas_website.user_id','saas_user.id')
    ->whereNull('saas_website.deleted_at')
    ->join('saas_website_plan_rule','saas_website_plan_rule.website_id','saas_website.id')
    ->whereNull('saas_website_plan_rule.deleted_at')
    ->where('saas_website_plan_rule.id',$id)
    ->first();

    $ruta_api = SaasSettings::where('saas_settings.name', 'base_path')
        ->first()
        ->value;

    $protocol = SaasSettings::where('name', 'protocol')
        ->first()
        ->value;

    $ruta = $protocol . $ruta_api . '/api/saas/email';

    $email_info = SaasSettings::where('name', 'email_info')->first()->value;

    $mensaje = '';
    $asunto = '';
    if($datos_compra->cancelled == '0') {
        if($concepto == 'new') {
            $payment_method = DB::table('saas_payment_method_translation')
            ->select(
                'saas_payment_method_translation.name'
            )
            ->whereNull('saas_payment_method_translation.deleted_at')
            ->join('saas_payment_method', 'saas_payment_method.id','saas_payment_method_translation.payment_method_id')
            ->whereNull('saas_payment_method.deleted_at')
            ->where('saas_payment_method.id',$datos_compra->payment_method_id)
            ->where('saas_payment_method_translation.language_id', 1)
            ->first();

            $asunto = 'Nueva subscripción de pago';
            $mensaje = '<p>Se ha dado de alta una nueva subscripción de pago en Viavox Experience con los siguientes datos:</p>';

            if($datos_compra->plan_id) {
                if($datos_compra->status_id == 3 || $datos_compra->status_id == 4) {
                    $mensaje .= '<p><b>Nombre de site:</b> ' . $datos_compra->website_name . '</p>';
                    $mensaje .= '<p><b>Dominio:</b> ' . $datos_compra->domain . '</p>';
                } else {
                    $mensaje .= '<p><b>Nuevo sitio<b>';
                }
            }

            $mensaje .= '<p><b>Nombre de usuario: </b> ' . $datos_compra->name . ' ' . $datos_compra->surname . '</p>';
            $mensaje .= '<p><b>Email: </b> ' . $datos_compra->email . '</p>';

            $mensaje .= '<p><b>Fecha de caducidad: </b>' . Carbon::parse($datos_compra->date_end)->format('d-m-Y H:i:s') . '</p>';
            $mensaje .= '<p><b>Tipo de compra: </b>';

            if($datos_compra->plan_id) {
                $mensaje .= 'Plan ' . $datos_compra->code;
            } else {
                $mensaje .= 'Regla ' . $datos_compra->code;
            }
            $mensaje .= '</p>';

            $mensaje .= '<p><b>Importe: </b>' . number_format($amount, 2) . '€</p>';

            if($payment_method) {
                $mensaje .= '<p><b>Método de pago:</b> ' . $payment_method->name . '</p>';
            }

            if($datos_compra->subscription_code){
                $mensaje .= '<p><b>Código de subscripción:</b> ' . $datos_compra->subscription_code . '</p>';
            }
        } else {
            $payment_method = DB::table('saas_payment_method_translation')
            ->select(
                'saas_payment_method_translation.name'
            )
            ->whereNull('saas_payment_method_translation.deleted_at')
            ->join('saas_payment_method', 'saas_payment_method.id','saas_payment_method_translation.payment_method_id')
            ->whereNull('saas_payment_method.deleted_at')
            ->where('saas_payment_method.id',$datos_compra->payment_method_id)
            ->where('saas_payment_method_translation.language_id', 1)
            ->first();

            $asunto = 'Nuevo pago realizado';
            $mensaje = '<p>Se ha realizado el pago de una subscripción en Viavox Experience con los siguientes datos:</p>';

            if($datos_compra->plan_id) {
                if($datos_compra->status_id == 3 || $datos_compra->status_id == 4) {
                    $mensaje .= '<p><b>Nombre de site:</b> ' . $datos_compra->website_name . '</p>';
                    $mensaje .= '<p><b>Dominio:</b> ' . $datos_compra->domain . '</p>';
                } else {
                    $mensaje .= '<p><b>Nuevo sitio<b>';
                }
            }
            

            $mensaje .= '<p><b>Nombre de usuario: </b> ' . $datos_compra->name . ' ' . $datos_compra->surname . '</p>';
            $mensaje .= '<p><b>Email: </b> ' . $datos_compra->email . '</p>';

            $mensaje .= '<p><b>Fecha de caducidad: </b>' . Carbon::parse($datos_compra->date_end)->format('d-m-Y H:i:s') . '</p>';
            $mensaje .= '<p><b>Tipo de compra: </b>';

            if($datos_compra->plan_id) {
                $mensaje .= 'Plan ' . $datos_compra->code;
            } else {
                $mensaje .= 'Regla ' . $datos_compra->code;
            }
            $mensaje .= '</p>';

            $mensaje .= '<p><b>Importe: </b>' . number_format($amount, 2) . '€</p>';

            if($payment_method) {
                $mensaje .= '<p><b>Método de pago:</b> ' . $payment_method->name . '</p>';
            }

            if($datos_compra->subscription_code){
                $mensaje .= '<p><b>Código de subscripción:</b> ' . $datos_compra->subscription_code . '</p>';
            }
            
        }
        
    } else {
        $asunto = 'Cancelación de subscripción';
        $mensaje = '<p>Se ha cancelado una subscripción de pago en Viavox Experience con los siguientes datos:</p>';
    
        if($datos_compra->status_id == 3 || $datos_compra->status_id == 4) {
            $mensaje .= '<p><b>Nombre de site:</b> ' . $datos_compra->website_name . '</p>';
            $mensaje .= '<p><b>Dominio:</b> ' . $datos_compra->domain . '</p>';
        }

        $mensaje .= '<p><b>Nombre de usuario: </b> ' . $datos_compra->name . ' ' . $datos_compra->surname . '</p>';
        $mensaje .= '<p><b>Email: </b> ' . $datos_compra->email . '</p>';

        $mensaje .= '<p><b>Fecha de caducidad: </b>' . Carbon::parse($datos_compra->date_end)->format('d-m-Y H:i:s') . '</p>';
        $mensaje .= '<p><b>Tipo de compra: </b>';

        if($datos_compra->plan_id) {
            $mensaje .= 'Plan ' . $datos_compra->code;
        } else {
            $mensaje .= 'Regla ' . $datos_compra->code;
        }

        $mensaje .= '</p>';

        if($datos_compra->subscription_code){
            $mensaje .= '<p><b>Código de subscripción:</b> ' . $datos_compra->subscription_code . '</p>';
        }

    }

    $client = new \GuzzleHttp\Client();
    $res = $client->request('POST', $ruta, ['json' => ['title' => $asunto,
        'body' => $mensaje,
        'template' => 'information',
        'emails' => [
            '0' => [
                'email' => $email_info,
                'subject_email' => $asunto,
            ]]], 'http_errors' => false]);
  }
