<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasWebsiteLanguage extends Model
{
    use SoftDeletes;

    protected $table = 'saas_website_language';

    protected $fillable = [
        'website_id',
        'language_id'
    ];

}
