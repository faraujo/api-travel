<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class SaasPayment extends Model
{

    use SoftDeletes;

    protected $table = "saas_payment";

    protected $fillable = [
        'payment_method_id',
        'website_plan_rule_id',
        'code',
        'currency_id',
        'amount',
        'status_ok',
        'request_paypal',
        'response_paypal',
        'request_suscription_paypal',
        'response_suscription_paypal',
        'request_redsys',
    ];

}

