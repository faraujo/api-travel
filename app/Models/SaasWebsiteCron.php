<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hyn\Tenancy\Traits\UsesSystemConnection;

class SaasWebsiteCron extends Model
{
    use SoftDeletes, UsesSystemConnection;

    protected $table = 'saas_website_cron';

    protected $fillable = [ 
        'date_start',
        'date_end',
        'log'       
    ];

}
