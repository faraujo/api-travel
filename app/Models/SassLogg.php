<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SaasLogg extends Model
{

    protected $table = "saas_email_log";

    protected $fillable = ['date','from','to','cc','bcc','subject','body','headers','attachments','description_error','received_email'];

}

