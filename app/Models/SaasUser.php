<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;


//class SaasUser extends Model implements AuthenticatableContract, AuthorizableContract
class SaasUser extends Model implements AuthenticatableContract
{
    use Authenticatable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'business_name',
        'business_number_document',
        'business_address'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $table = 'saas_user';

    /**
     * Define la relación de la tabla saas_user y saas_user_remember_token
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function saasUserRemeberToken()
    {
        return $this->hasMany('App\Models\SaasUserRememberToken', 'user_id');
    }

}
