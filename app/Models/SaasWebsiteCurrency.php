<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasWebsiteCurrency extends Model
{
    use SoftDeletes;

    protected $table = 'saas_website_currency';

    protected $fillable = [
        'website_id',
        'currency_id'
    ];

}
