<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasWebsitePlan extends Model
{
    use SoftDeletes;

    protected $table = 'saas_website_plan_rule';

    protected $fillable = [
        'website_id',
        'plan_id',
        'rule_id',
        'value',
        'unlimited',
        'sale_price',
        'code',
        'date_start',
        'date_end',
        'status_id',
        'subscription_code',
        'payment_method_id',
    ];

}
