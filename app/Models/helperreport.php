<?php
use App\Models\SaasSettings;
use Illuminate\Http\Request;

function reportSaasService(Exception $exception, String $service = null)
    {

        
        $envio_correo = SaasSettings::where('name', '=', 'send_email_error')->first()->value;

        $environment = SaasSettings::where('name','=','nombre_cliente')->first()->value;
        //if ($exception instanceof \ErrorException) { Condicional si es un tipo de excepcion concreta
            if ($envio_correo && $service !== 'SaasEmails') {

                $mensaje = 'Microservicio de '.$service.': File' . $exception->getFile() . ' , Error: ' . $exception->getMessage() . ', Line: ' . $exception->getLine() . '';

                $controller = app(\App\Http\Controllers\SaasEmailController::class);

                $peticion = new Request();

                $peticion->setMethod('POST');

                $peticion->request->add(['title' => 'Error']);

                $peticion->request->add(['body' => $mensaje]);

                $peticion->request->add(['template' => 'error']);

                $peticion->request->add(['emails' => [
                    '0' => [
                        'email' => SaasSettings::where('name', '=', 'email_error_sistema')->first()->value,
                        'subject_email' => $environment . ' - Error',
                    ]]]);

                
                $controller->send($peticion);

            }

            $envio_slack = SaasSettings::where('name', '=', 'send_slack_error')->first()->value;
            if ($envio_slack) {

                $mensaje = 'File' . $exception->getFile() . ' , Error: ' . $exception->getMessage() . ', Line: ' . $exception->getLine() . '';

                $controller = app(\App\Http\Controllers\SaasSlackController::class);

                $peticion = new Request();

                $peticion->setMethod('POST');

                $peticion->request->add(['from' => 'Microservicio de '.$service]);

                $peticion->request->add(['content' => 'Excepción o error de sistema']);

                $peticion->request->add(['type' => 'error']);

                $peticion->request->add(['attachment' => [
                    'title' => $environment . ' - Microservicio de '.$service.':',
                    'content' => $mensaje
                ]]);

                $peticion->request->add(['space' => 'error_sistema']);

                $peticion->request->add(['channel' => [
                    '0' => SaasSettings::where('name', '=', 'slack_channel_error_sistema')->first()->value
                ]]);
                
                $controller->send($peticion);
            }
        //}
    }