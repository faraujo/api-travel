<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasPasswordToken extends Model
{
    use SoftDeletes;

    protected $table = 'saas_user_recover_token';

    protected $fillable = ['user_id', 'token', 'expired_at'];

}