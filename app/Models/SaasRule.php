<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasRule extends Model
{
    use SoftDeletes;

    protected $table = 'saas_rule';

    protected $fillable = [
    ];

}
