<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaasCurrency extends Model
{
    use SoftDeletes;

    protected $table = 'saas_currency';

    protected $fillable = [        
    ];

}
