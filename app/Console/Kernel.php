<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\reservationCommand::class,
        Commands\createTenant::class,
        Commands\RedsysRecurrentPayments::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Tarea programada encargada de expirar reservas activas fuera de tiempo de reserva
        $cron_interval = DB::table('saas_settings')->where('name', 'cron_expire_reservation')->first();
        
        $schedule->command('reservation:expired')->cron('*/' . $cron_interval->value . ' * * * *');

        $cron_tenant_interval = DB::table('saas_settings')->where('name', 'cron_create_tenant')->first();

        $schedule->command('tenant:create')->cron('*/' . $cron_tenant_interval->value . ' * * * *');

        $schedule->command('redsys:recurrent')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
