<?php

namespace App\Console\Commands;

use Illuminate\Support\Carbon;
use App\Models\tenant\ReservationDetail;
use App\Models\tenant\ReservationDetailTransportation;
use App\Models\tenant\ReservationDetailFood;
use App\Models\tenant\ReservationDetailPhoto;
use App\Models\tenant\ReservationDetailDayMore;
use App\Models\tenant\ReservationDetailPackage;
use App\Models\tenant\ReservationDetailHotel;
use App\Models\tenant\ReservationDetailActivity;
use App\Models\tenant\ReservationDetailEvent;
use App\Models\tenant\ReservationDetailTour;
use App\Models\tenant\ReservationDetailPark;
use App\Models\tenant\ReservationDetailRestaurant;
use App\Models\tenant\ReservationDetailBenefitCard;
use App\Models\tenant\Availability;
use App\Models\tenant\Language;
use App\Models\tenant\ReservationPromotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Hyn\Tenancy\Environment;
use App\Models\SaasReservationCron;

use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Models\Website;

use Illuminate\Console\Command;

class reservationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reservation:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Método para la expiración de reservas pendientes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        try {

            $cron_in_execution = DB::table('saas_reservation_cron')
                ->whereNull('saas_reservation_cron.deleted_at')
                ->whereNull('saas_reservation_cron.date_end')
                ->whereNotNull('saas_reservation_cron.date_start')
                ->first();

            if(!$cron_in_execution){

                $log = SaasReservationCron::create([
                    'date_start' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                $websites = DB::connection('system')->table('websites')->whereNull('websites.deleted_at')->get();

                foreach($websites as $webs) {
                    $tenancy = app(Environment::class);

                    $website = app(WebsiteRepository::class)->findById($webs->id);

                    $tenancy->tenant($website);

                    DB::connection('tenant')->beginTransaction();

                    //Consultas preparadas
                    $sql_reservations = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id as reservation_id',
                            'mo_reservation.language_id',
                            'mo_reservation_detail.id',
                            'mo_reservation_detail.product_id',
                            'mo_reservation_detail.service_id',
                            'mo_reservation_detail.subchannel_id',
                            'mo_reservation_detail.contract_id',
                            'mo_reservation_detail.location_id',
                            'mo_reservation_detail.date_time_start',
                            'mo_reservation_detail.date_time_quotation',
                            'mo_product.type_id',
                            'mo_product.external_connection',
                            'mo_product.external_connection_url',
                            'mo_product.external_code',
                            'mo_product.external_service_user',
                            'mo_product.external_service_password',
                            'mo_contract_model.time_lock_avail'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                        ->whereNull('mo_reservation_detail.deleted_at')
                        ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_contract_model', 'mo_contract_model.id', 'mo_reservation_detail.contract_id')
                        ->whereNull('mo_contract_model.deleted_at');

                    $sql_reservation_settings = DB::connection('tenant')->table('mo_contract_model_product')
                        ->select(
                            'individual_sale',
                            'time_limit_enjoyment',
                            'time_expiration'
                        )
                        ->whereNull('mo_contract_model_product.deleted_at')
                        ->join('mo_contract_model', 'mo_contract_model.id', 'mo_contract_model_product.contract_model_id')
                        ->whereNull('mo_contract_model.deleted_at');

                    $sql_reservation_client_type = DB::connection('tenant')->table('mo_reservation_detail_client_type')
                        ->whereNull('mo_reservation_detail_client_type.deleted_at');

                    //Fin consultas preparadas

                    //Bloqueo de disponibilidad en reservas iniciadas

                    $sql_reservations_clone = clone $sql_reservations;

                    $sql_reservations_clone->where('mo_reservation_detail.operation_status_id', 1);

                    $datos_reserva = $sql_reservations_clone->get();

                    foreach ($datos_reserva as $dato) {

                        if ($dato->time_lock_avail != null) {

                            $fecha_reserva = Carbon::parse($dato->date_time_start);

                            if ($fecha_reserva->addMinutes($dato->time_lock_avail)->format('Y-m-d H:i:s') < Carbon::now()->format('Y-m-d H:i:s')) {

                                $sql_reservation_client_type_clone = clone $sql_reservation_client_type;

                                $datos_client_type = $sql_reservation_client_type_clone->where('mo_reservation_detail_client_type.reservation_detail_id', $dato->id)->count();

                                ReservationDetail::where('id', $dato->id)->update([
                                    'operation_status_id' => 6,
                                    'date_time_expiration' => Carbon::now()->format('Y-m-d H:i:s')
                                ]);

                                $datos = null;

                                if ($dato->service_id != '') {

                                    if ($dato->service_id == 1) {
                                        $datos = ReservationDetailTransportation::where('reservation_detail_id', $dato->id)->first();
                                    }

                                    if ($dato->service_id == 2) {
                                        $datos = ReservationDetailFood::where('reservation_detail_id', $dato->id)->first();
                                    }

                                    if ($dato->service_id == 3) {
                                        $datos = ReservationDetailPhoto::where('reservation_detail_id', $dato->id)->first();
                                    }

                                    if ($dato->service_id == 4) {
                                        $datos = ReservationDetailDayMore::where('reservation_detail_id', $dato->id)->first();
                                    }

                                } else {

                                    if ($dato->type_id == 1) {
                                        $datos = ReservationDetailPackage::where('reservation_detail_id', $dato->id)->get();
                                    }

                                    if ($dato->type_id == 2) {
                                        $datos = ReservationDetailHotel::where('reservation_detail_id', $dato->id)->first();
                                    }

                                    if ($dato->type_id == 3) {
                                        $datos = ReservationDetailActivity::where('reservation_detail_id', $dato->id)->first();
                                    }

                                    if ($dato->type_id == 4) {
                                        $datos = ReservationDetailEvent::where('reservation_detail_id', $dato->id)->first();
                                    }

                                    if ($dato->type_id == 5) {
                                        $datos = ReservationDetailTour::where('reservation_detail_id', $dato->id)->first();
                                    }

                                    if ($dato->type_id == 6) {
                                        $datos = ReservationDetailPark::where('reservation_detail_id', $dato->id)->first();

                                    }

                                    if ($dato->type_id == 7) {
                                        $datos = ReservationDetailRestaurant::where('reservation_detail_id', $dato->id)->first();

                                    }

                                    if ($dato->type_id == 8) {
                                        $datos = ReservationDetailBenefitCard::where('reservation_detail_id', $dato->id)->first();

                                    }
                                }

                                // Si productos "normales" o servicios
                                if ($dato->type_id != 1 || $dato->service_id != '') {
                                    $datos_auxiliares = array();
                                    if($dato->type_id != 2 || !is_null($dato->service_id)){
                                        if ($datos->date != '' && $datos->session != '') {
                                                $datos_auxiliares['date'] = $datos->date;
                                                $datos_auxiliares['session'] = $datos->session;

                                        } else {
                                            $datos_auxiliares['date'] = null;
                                            $datos_auxiliares['session'] = null;
                                        }
                                    } else {
                                        if ($datos->date_start != '') {
                                            $datos_auxiliares['date'] = $datos->date_start;
                                            $datos_auxiliares['session'] = null;
                                        }
                                    }

                                    $datos_avail = Availability::where('mo_availability.date', $datos_auxiliares['date'])
                                        ->where('mo_availability.session', $datos_auxiliares['session'])
                                        ->where('product_id', $dato->product_id)
                                        ->where('service_id', $dato->service_id)
                                        ->where('location_id', $dato->location_id)
                                        ->where('closed', '!=', 1)
                                        ->first();

                                    if($datos_avail){
                                        $datos_avail->update([
                                            'avail' => $datos_avail->avail + $datos_client_type
                                        ]);
                                    }

                                } else {

                                    foreach ($datos as $dato_product) {

                                        $datos_auxiliares = array();
                                        if ($dato_product->date != '' && $dato_product->session != '') {
                                            $datos_auxiliares['date'] = $dato_product->date;
                                            $datos_auxiliares['session'] = $dato_product->session;
                                        } else {
                                            $datos_auxiliares['date'] = null;
                                            $datos_auxiliares['session'] = null;
                                        }

                                        $datos_avail = Availability::where('mo_availability.date', $datos_auxiliares['date'])
                                            ->where('mo_availability.session', $datos_auxiliares['session'])
                                            ->where('product_id', $dato_product->product_id)
                                            ->whereNull('service_id')
                                            ->where('location_id', $dato_product->location_id)
                                            ->where('closed', '!=', 1)
                                            ->first();
                                        if($datos_avail){
                                            $datos_avail->update([
                                                'avail' => $datos_avail->avail + $datos_client_type
                                            ]);
                                        }

                                    }
                                }

                                $idioma = Language::where('id', $dato->language_id)->first();

                                $promotion_coupon = ReservationPromotion::whereNotNull('coupon_code')->where('reservation_id', $dato->reservation_id)->get();

                                $promotion_benefit_card = ReservationPromotion::whereNotNull('benefit_card_id')->where('reservation_id', $dato->reservation_id)->get();

                                ReservationPromotion::where('reservation_id', $dato->reservation_id)->delete();

                                foreach($promotion_benefit_card as $benefit) {

                                    $request_benefit = new Request();

                                    $request_benefit->request->add(['id' => $dato->reservation_id]);

                                    $request_benefit->request->add(['benefit_card_id' => $benefit->benefit_card_id]);
                                    $request_benefit->request->add(['benefit_card_number' => $benefit->benefit_card_number]);

                                    $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_benefit, $dato->subchannel_id, $idioma);
                                }

                                foreach($promotion_coupon as $coupon) {

                                    $request_coupon = new Request();

                                    $request_coupon->request->add(['id' => $dato->reservation_id]);

                                    $request_coupon->request->add(['coupon_code' => $coupon->coupon_code]);

                                    $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_coupon, $dato->subchannel_id, $idioma);
                                }

                                $new_request = new Request();

                                $new_request->request->add(['id' => $dato->reservation_id]);

                                $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($new_request, $dato->subchannel_id, $idioma);
                            }
                        }
                    }


                    //Bloqueo de disponibilidad en reservas cotizadas

                    $sql_reservations_clone = clone $sql_reservations;

                    $sql_reservations_clone->where('mo_reservation_detail.operation_status_id', 2);

                    $datos_reserva = $sql_reservations_clone->get();

                    foreach ($datos_reserva as $dato) {

                        $sql_reservation_settings_clone = clone $sql_reservation_settings;

                        $datos_settings = $sql_reservation_settings_clone
                            ->where('mo_contract_model_product.product_id', $dato->product_id)
                            ->whereNull('mo_contract_model_product.service_id')
                            ->where('mo_contract_model.id', $dato->contract_id)
                            ->first();


                        if ($datos_settings) {

                            if ($datos_settings->time_expiration != null) {

                                $fecha_reserva = Carbon::parse($dato->date_time_quotation);


                                if ($fecha_reserva->addHour($datos_settings->time_expiration)->format('Y-m-d H:i:s') < Carbon::now()->format('Y-m-d H:i:s')) {

                                    $sql_reservation_client_type_clone = clone $sql_reservation_client_type;

                                    $datos_client_type = $sql_reservation_client_type_clone->where('mo_reservation_detail_client_type.reservation_detail_id', $dato->id)->count();

                                    ReservationDetail::where('id', $dato->id)->update([
                                        'operation_status_id' => 6,
                                        'date_time_expiration' => Carbon::now()->format('Y-m-d H:i:s')
                                    ]);

                                    $datos = null;

                                    if ($dato->service_id != '') {
                                        if ($dato->service_id == 1) {
                                            $datos = ReservationDetailTransportation::where('reservation_detail_id', $dato->id)->first();
                                        }

                                        if ($dato->service_id == 2) {
                                            $datos = ReservationDetailFood::where('reservation_detail_id', $dato->id)->first();
                                        }

                                        if ($dato->service_id == 3) {
                                            $datos = ReservationDetailPhoto::where('reservation_detail_id', $dato->id)->first();
                                        }

                                        if ($dato->service_id == 4) {
                                            $datos = ReservationDetailDayMore::where('reservation_detail_id', $dato->id)->first();
                                        }

                                    } else {

                                        if ($dato->type_id == 1) {
                                            $datos = ReservationDetailPackage::where('reservation_detail_id', $dato->id)->get();
                                        }

                                        if($dato->type_id == 2) {
                                            $datos = ReservationDetailHotel::where('reservation_detail_id', $dato->id)->get();
                                        }

                                        if ($dato->type_id == 3) {
                                            $datos = ReservationDetailActivity::where('reservation_detail_id', $dato->id)->first();
                                        }

                                        if ($dato->type_id == 4) {
                                            $datos = ReservationDetailEvent::where('reservation_detail_id', $dato->id)->first();
                                        }

                                        if ($dato->type_id == 5) {
                                            $datos = ReservationDetailTour::where('reservation_detail_id', $dato->id)->first();
                                        }

                                        if ($dato->type_id == 6) {
                                            $datos = ReservationDetailPark::where('reservation_detail_id', $dato->id)->first();

                                        }

                                        if ($dato->type_id == 7) {
                                            $datos = ReservationDetailRestaurant::where('reservation_detail_id', $dato->id)->first();

                                        }

                                        if ($dato->type_id == 8) {
                                            $datos = ReservationDetailBenefitCard::where('reservation_detail_id', $dato->id)->first();

                                        }
                                    }


                                    // Si productos "normales" o servicios
                                    if ($dato->type_id != 1 || $dato->service_id != '') {
                                        $datos_auxiliares = array();

                                        if($dato->type_id != 2 || !is_null($dato->service_id)){
                                            if ($datos->date != '' && $datos->session != '') {
                                                $datos_auxiliares['date'] = $datos->date;
                                                $datos_auxiliares['session'] = $datos->session;

                                            } else {
                                                $datos_auxiliares['date'] = null;
                                                $datos_auxiliares['session'] = null;
                                            }
                                        } else {

                                            if ($datos->date_start != '') {
                                                $datos_auxiliares['date'] = $datos->date_start;
                                                $datos_auxiliares['session'] = null;
                                            }
                                        }

                                        $datos_avail = Availability::where('mo_availability.date', $datos_auxiliares['date'])
                                            ->where('mo_availability.session', $datos_auxiliares['session'])
                                            ->where('product_id', $dato->product_id)
                                            ->where('service_id', $dato->service_id)
                                            ->where('location_id', $dato->location_id)
                                            ->where('closed', '!=', 1)
                                            ->first();

                                        if($datos_avail){
                                            $datos_avail->update([
                                                'avail' => $datos_avail->avail + $datos_client_type
                                            ]);
                                        }

                                    } else {

                                        foreach ($datos as $dato_product) {

                                            $datos_auxiliares = array();
                                            if ($dato_product->date != '' && $dato_product->session != '') {
                                                $datos_auxiliares['date'] = $dato_product->date;
                                                $datos_auxiliares['session'] = $dato_product->session;
                                            } else {
                                                $datos_auxiliares['date'] = null;
                                                $datos_auxiliares['session'] = null;
                                            }

                                            $datos_avail = Availability::where('mo_availability.date', $datos_auxiliares['date'])
                                                ->where('mo_availability.session', $datos_auxiliares['session'])
                                                ->where('product_id', $dato_product->product_id)
                                                ->whereNull('service_id')
                                                ->where('location_id', $dato_product->location_id)
                                                ->where('closed', '!=', 1)
                                                ->first();
                                            if($datos_avail){
                                                $datos_avail->update([
                                                    'avail' => $datos_avail->avail + $datos_client_type
                                                ]);
                                            }
                                        }
                                    }

                                    $idioma = Language::where('id', $dato->language_id)->first();

                                    $promotion_coupon = ReservationPromotion::whereNotNull('coupon_code')->where('reservation_id', $dato->reservation_id)->get();

                                    $promotion_benefit_card = ReservationPromotion::whereNotNull('benefit_card_id')->where('reservation_id', $dato->reservation_id)->get();

                                    ReservationPromotion::where('reservation_id', $dato->reservation_id)->delete();

                                    foreach($promotion_benefit_card as $benefit) {

                                        $request_benefit = new Request();

                                        $request_benefit->request->add(['id' => $dato->reservation_id]);

                                        $request_benefit->request->add(['benefit_card_id' => $benefit->benefit_card_id]);
                                        $request_benefit->request->add(['benefit_card_number' => $benefit->benefit_card_number]);

                                        $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_benefit, $dato->subchannel_id, $idioma);
                                    }

                                    foreach($promotion_coupon as $coupon) {

                                        $request_coupon = new Request();

                                        $request_coupon->request->add(['id' => $dato->reservation_id]);

                                        $request_coupon->request->add(['coupon_code' => $coupon->coupon_code]);

                                        $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_coupon, $dato->subchannel_id, $idioma);
                                    }

                                    $new_request = new Request();

                                    $new_request->request->add(['id' => $dato->reservation_id]);

                                    $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($new_request, $dato->subchannel_id, $idioma);
                                }
                            }
                        }
                    }

                    //Bloqueo de reservas con fechas pasadas
                    $sql_reservations_clone = clone $sql_reservations;

                    $sql_reservations_clone->where(function ($query){
                        $query->where('mo_reservation_detail.operation_status_id', 1)
                            ->orWhere('mo_reservation_detail.operation_status_id', 2);
                    });

                    $datos_reserva = $sql_reservations_clone->get();

                    foreach ($datos_reserva as $dato) {

                        $sql_reservation_client_type_clone = clone $sql_reservation_client_type;

                        $datos_client_type = $sql_reservation_client_type_clone->where('mo_reservation_detail_client_type.reservation_detail_id', $dato->id)->count();

                        $datos = null;


                        if ($dato->service_id != '') {
                            if ($dato->service_id == 1) {
                                $datos = ReservationDetailTransportation::where('reservation_detail_id', $dato->id)->first();
                            }

                            if ($dato->service_id == 2) {
                                $datos = ReservationDetailFood::where('reservation_detail_id', $dato->id)->first();
                            }

                            if ($dato->service_id == 3) {
                                $datos = ReservationDetailPhoto::where('reservation_detail_id', $dato->id)->first();
                            }

                            if ($dato->service_id == 4) {
                                $datos = ReservationDetailDayMore::where('reservation_detail_id', $dato->id)->first();
                            }

                        } else {
                            if ($dato->type_id == 1) {
                                $datos = ReservationDetailPackage::where('reservation_detail_id', $dato->id)->orderBy('date','asc')->first();
                            }

                            if($dato->type_id == 2) {
                                $datos = ReservationDetailHotel::where('reservation_detail_id', $dato->id)->first();
                            }

                            if ($dato->type_id == 3) {
                                $datos = ReservationDetailActivity::where('reservation_detail_id', $dato->id)->first();
                            }

                            if ($dato->type_id == 4) {
                                $datos = ReservationDetailEvent::where('reservation_detail_id', $dato->id)->first();
                            }

                            if ($dato->type_id == 5) {
                                $datos = ReservationDetailTour::where('reservation_detail_id', $dato->id)->first();
                            }

                            if ($dato->type_id == 6) {
                                $datos = ReservationDetailPark::where('reservation_detail_id', $dato->id)->first();

                            }

                            if ($dato->type_id == 7) {
                                $datos = ReservationDetailRestaurant::where('reservation_detail_id', $dato->id)->first();

                            }

                            if ($dato->type_id == 8) {
                                $datos = ReservationDetailBenefitCard::where('reservation_detail_id', $dato->id)->first();
                            }
                        }

                        if($dato->type_id != 2 || !is_null($dato->service_id)){
                            if(!is_null($datos->session)) {
                                $fecha_reserva = Carbon::parse($datos->date . ' ' . $datos->session)->format('Y-m-d H:i:s');
                            } else {
                                $fecha_reserva = Carbon::parse($datos->date)->endOfDay()->format('Y-m-d H:i:s');
                            }
                        } else {
                            if ($datos->date_start != '') {
                                $fecha_reserva = Carbon::parse($datos->date_start)->format('Y-m-d H:i:s');
                            }
                        }

                        
                        if($fecha_reserva < Carbon::now()->format('Y-m-d H:i:s')){
                            ReservationDetail::where('id', $dato->id)->update([
                                'operation_status_id' => 6,
                                'date_time_expiration' => Carbon::now()->format('Y-m-d H:i:s')
                            ]);

                            // Si productos "normales" o servicios
                            if ($dato->type_id != 1 || $dato->service_id != '') {
                                $datos_auxiliares = array();

                                if($dato->type_id != 2 || !is_null($dato->service_id)){
                                    if ($datos->date != '' && $datos->session != '') {
                                        $datos_auxiliares['date'] = $datos->date;
                                        $datos_auxiliares['session'] = $datos->session;

                                    } else {
                                        $datos_auxiliares['date'] = null;
                                        $datos_auxiliares['session'] = null;
                                    }
                                } else {

                                    if ($datos->date_start != '') {
                                        $datos_auxiliares['date'] = $datos->date_start;
                                        $datos_auxiliares['session'] = null;
                                    }
                                }

                                $datos_avail = Availability::where('mo_availability.date', $datos_auxiliares['date'])
                                    ->where('mo_availability.session', $datos_auxiliares['session'])
                                    ->where('product_id', $dato->product_id)
                                    ->where('service_id', $dato->service_id)
                                    ->where('location_id', $dato->location_id)
                                    ->where('closed', '!=', 1)
                                    ->first();

                                if($datos_avail){
                                    $datos_avail->update([
                                        'avail' => $datos_avail->avail + $datos_client_type
                                    ]);
                                }

                            } else {

                                $datos = ReservationDetailPackage::where('reservation_detail_id', $dato->id)->get();
                                
                                foreach ($datos as $dato_product) {

                                    $datos_auxiliares = array();
                                    if ($dato_product->date != '' && $dato_product->session != '') {
                                        $datos_auxiliares['date'] = $dato_product->date;
                                        $datos_auxiliares['session'] = $dato_product->session;
                                    } else {
                                        $datos_auxiliares['date'] = null;
                                        $datos_auxiliares['session'] = null;
                                    }

                                    $datos_avail = Availability::where('mo_availability.date', $datos_auxiliares['date'])
                                        ->where('mo_availability.session', $datos_auxiliares['session'])
                                        ->where('product_id', $dato_product->product_id)
                                        ->whereNull('service_id')
                                        ->where('location_id', $dato_product->location_id)
                                        ->where('closed', '!=', 1)
                                        ->first();
                                    if($datos_avail){
                                        $datos_avail->update([
                                            'avail' => $datos_avail->avail + $datos_client_type
                                        ]);
                                    }
                                }
                            }

                            $idioma = Language::where('id', $dato->language_id)->first();

                            $promotion_coupon = ReservationPromotion::whereNotNull('coupon_code')->where('reservation_id', $dato->reservation_id)->get();

                            $promotion_benefit_card = ReservationPromotion::whereNotNull('benefit_card_id')->where('reservation_id', $dato->reservation_id)->get();

                            ReservationPromotion::where('reservation_id', $dato->reservation_id)->delete();

                            foreach($promotion_benefit_card as $benefit) {

                                $request_benefit = new Request();

                                $request_benefit->request->add(['id' => $dato->reservation_id]);

                                $request_benefit->request->add(['benefit_card_id' => $benefit->benefit_card_id]);
                                $request_benefit->request->add(['benefit_card_number' => $benefit->benefit_card_number]);

                                $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_benefit, $dato->subchannel_id, $idioma);
                            }

                            foreach($promotion_coupon as $coupon) {

                                $request_coupon = new Request();

                                $request_coupon->request->add(['id' => $dato->reservation_id]);

                                $request_coupon->request->add(['coupon_code' => $coupon->coupon_code]);

                                $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_coupon, $dato->subchannel_id, $idioma);
                            }

                            $new_request = new Request();

                            $new_request->request->add(['id' => $dato->reservation_id]);

                            $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($new_request, $dato->subchannel_id, $idioma);
                        }
                    }

                    DB::connection('tenant')->commit();
                    
                }

                $log->date_end = Carbon::now()->format('Y-m-d H:i:s');
                $log->save();
            }

            
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Ocurrio un error durante la actualización de los estados de la reservas' . ' ' . $dato->id);
            reportService($e, 'Cron reservas');
        }
    }
}
