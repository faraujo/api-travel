<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SaasPayment;
use App\Models\SaasPaymentMethod;
use App\Models\SaasCurrency;
use App\Models\SaasWebsitePlan;
use App\Models\SaasWebsite;
use App\Models\SaasSettings;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Ssheduardo\Redsys\Facades\Redsys;

class RedsysRecurrentPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redsys:recurrent';

    /**
     * Ejecución de cobro de todos los pagos recurrentes creados a través de redsys.
     *
     * @var string
     */
    protected $description = 'Ejecución de cobro de todos los pagos recurrentes creados a través de redsys';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $array['error'] = 200;
        try{
            
            DB::beginTransaction();

            $websites_redsys = DB::table('saas_website_plan_rule')
                ->whereNull('saas_website_plan_rule.deleted_at')
                ->where('saas_website_plan_rule.payment_method_id', 2)
                ->whereNotNull('saas_website_plan_rule.subscription_code')
                ->where('saas_website_plan_rule.date_end', '<=', Carbon::now()->endofDay()->addDay())
                ->where('saas_website_plan_rule.date_end','>=', Carbon::now()->startOfDay())
                ->where('saas_website_plan_rule.cancelled', 0)
                ->get();

            $metodo_pago = SaasPaymentMethod::where('id', 2)->first();

            $key = $metodo_pago->redsys_merchant_secret;
            $code = $metodo_pago->redsys_merchant_code;
            $terminal = $metodo_pago->redsys_merchant_terminal;
            $enviroment = $metodo_pago->redsys_enviroment;
            $redsysendpoint = $metodo_pago->redsys_recurrent_endpoint;
            
            $protocol = SaasSettings::where('name', 'protocol')->first()->value;
            $base_path = SaasSettings::where('name','base_path')->first()->value;
            
            config([
                'redsys.key' => $key,
                'redsys.merchantcode' => $code,
                'redsys.terminal' => $terminal,
                'redsys.enviroment' => $enviroment,
            ]);

            foreach($websites_redsys as $website){
                $parametros = array();

                $currency = SaasCurrency::where('id', 3)->first();

                $pago = SaasPayment::create([
                    'payment_method_id' => 2,
                    'website_plan_rule_id' => $website->id,
                    'code' => 'redsys',
                    'currency_id' => $currency->id,
                    'amount' => $website->sale_price,
                    'status_ok' => 0,
                ]);

                Redsys::setAmount(round($website->sale_price, 2));
                Redsys::setOrder(str_pad($pago->id, 12, "0", STR_PAD_LEFT));
                Redsys::setMerchantcode($code);
                Redsys::setCurrency($currency->iso_number);
                Redsys::setNotification($protocol.$base_path.'/api/saas/payment/redsys/notify'); //Url de notificacion
                Redsys::setTransactiontype('0');
                Redsys::setTerminal($terminal);
                Redsys::setIdentifier($website->subscription_code);
                Redsys::setMerchantDirectPayment(true);

                $signature = Redsys::generateMerchantSignature($key);
                
                Redsys::setMerchantSignature($signature);

                $encode_parameters['Ds_MerchantParameters'] = Redsys::generateMerchantParameters();
                $encode_parameters['Ds_Signature'] = Redsys::getMerchantSignature();
                $encode_parameters['Ds_SignatureVersion'] = Redsys::getVersion();

                $client = new Client();
                $res = $client->request('POST', $redsysendpoint, ['json' => $encode_parameters, 'http_errors' => false]);
                
                $respuesta = \GuzzleHttp\json_decode($res->getBody()->getContents(), true);
            }

            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            $this->info($e->getMessage());
            reportSaasService($e, 'SaasPayment');
        }

    }
}
