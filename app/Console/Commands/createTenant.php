<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SaasWebsiteCron;
use App\Models\SaasWebsite;

use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class createTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea estructura de base de datos del tenant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        try {


            $cron_in_execution = DB::table('saas_website_cron')
                ->whereNull('saas_website_cron.deleted_at')
                ->whereNull('saas_website_cron.date_end')
                ->whereNotNull('saas_website_cron.date_start')
                ->first();

            if(!$cron_in_execution){
                $log = SaasWebsiteCron::create([
                    'date_start' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                $datos_website = DB::table('saas_website')
                    ->whereNull('saas_website.deleted_at')
                    ->whereNull('saas_website.website_id')
                    ->where('saas_website.status_id',3)
                    ->get();

                $database_prefix = DB::table('saas_settings')
                    ->where('name', 'tenant_database_prefix')
                    ->first()
                    ->value;

                $control_panel_route = DB::table('saas_settings')
                    ->where('name', 'base_path_control_panel')
                    ->first()
                    ->value;

                $base_path_saas = DB::table('saas_settings')
                    ->where('name', 'base_path_saas')
                    ->first()
                    ->value;

                $base_path_system = DB::table('saas_settings')
                    ->where('name', 'base_path_system')
                    ->first()
                    ->value;

                $protocol = DB::table('saas_settings')
                    ->where('name', 'protocol')
                    ->first()
                    ->value;

                $ruta_api = DB::table('saas_settings')
                                ->where('saas_settings.name', 'base_path')
                                ->first()
                                ->value;

                foreach($datos_website as $saaswebsite) {

                    $datos_user = DB::connection('system')->table('saas_user')
                        ->whereNull('saas_user.deleted_at')
                        ->where('saas_user.id', $saaswebsite->user_id)
                        ->first();

                    $sql_language = DB::connection('system')->table('saas_website_language')
                        ->whereNull('saas_website_language.deleted_at')
                        ->where('saas_website_language.website_id',$saaswebsite->id);

                    $sql_currency = DB::connection('system')->table('saas_website_currency')
                        ->whereNull('saas_website_currency.deleted_at')
                        ->where('saas_website_currency.website_id',$saaswebsite->id);

                    $website_name = $database_prefix.$saaswebsite->domain; // saas_vxt_test
                    $hostname_name = $saaswebsite->domain.'.'.$ruta_api; // test.mtr.viavoxexperience.loc

                    // dd($hostname_name);
                    
                    $this->info('[' .  Carbon::now()->format('Y-m-d H:i:s') . '] Empieza la creación de website con migraciones');

                    $website = new Website;
                    $website->uuid = $website_name;
                    app(WebsiteRepository::class)->create($website);

                    $this->info("Creación de website " . $website_name);
                    // dd($website->uuid); // Unique id
                    $hostname = new Hostname;
                    $hostname->fqdn = $hostname_name;
                    $hostname = app(HostnameRepository::class)->create($hostname);
                    app(HostnameRepository::class)->attach($hostname, $website);

                    $this->info("Creación de hostname " . $hostname_name);

                    DB::connection('tenant')->table('mo_user')
                        ->whereNull('mo_user.deleted_at')
                        ->where('mo_user.id', 1)
                        ->update([
                            'system_user_id' => $saaswebsite->user_id,
                            'name' => $datos_user->name,
                            'surname' => $datos_user->surname,
                            'email' => $datos_user->email,
                            'email2' => null,
                            'password' => $datos_user->password,
                            'language_id' => $datos_user->language_id,
                        ]);

                    $datos_user_tenant = DB::connection('tenant')->table('mo_user')
                            ->whereNull('mo_user.deleted_at')
                            ->where('mo_user.email', $datos_user->email)
                            ->first();

                    $this->info("Modificación de primer usuario");

                    DB::connection('tenant')->table('mo_settings')
                            ->where('name', 'moneda_defecto_id')
                            ->update([
                                'value' => $saaswebsite->default_currency_id
                            ]);

                    $this->info('Modificación de idioma por defecto');

                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'nombre_cliente')
                        ->update([
                            'value' => $saaswebsite->name
                        ]);

                    DB::connection('tenant')->table('mo_settings')
                    ->where('name', 'nif_cliente')
                    ->update([
                        'value' => $saaswebsite->nif                        
                    ]);

                    $this->info('Modificación de email de contacto');

                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'email_cliente_contacto')
                        ->update([
                            'value' => $datos_user->email
                        ]);

                    $this->info('Modificación de email from name default por defecto');
                    
                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'email_fromname_default')
                        ->update([
                            'value' => $saaswebsite->name
                        ]);
                   

                    $this->info('Modificación de ruta de recuperacion de contraseña del control panel');

                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'recover_route_back')
                        ->update([
                            'value' => $protocol.$saaswebsite->domain.'.'.$control_panel_route.'/recover'
                        ]);

                    $this->info('Modificación de ruta de recuperacion de contraseña del portal de venta');

                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'recover_route_front')
                        ->update([
                            'value' => $protocol.$saaswebsite->domain.'.'.$base_path_system.'/recover'
                        ]);

                    $this->info('Modificación de ruta para pdf de reserva');

                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'url_reservation_pdf')
                        ->update([
                            'value' => $protocol.$saaswebsite->domain.'.'.$control_panel_route.'/ticketpdf'
                        ]);

                    $this->info('Modificación de ruta base para imagenes');

                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'storage_files')
                        ->update([
                            'value' => $protocol.$saaswebsite->domain.'.'.$ruta_api.'/storage/'
                        ]);

                    $this->info('Modificación de ruta para enlace email cotización');

                    DB::connection('tenant')->table('mo_settings')
                    ->where('name', 'url_quotation')
                    ->update([
                        'value' => $protocol.$saaswebsite->domain.'.'.$base_path_system.'/%lang%/cartstep3email'
                    ]);

                    $this->info('Modificación de condicionante para heimdall');

                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'heimdall_middleware')
                        ->update([
                            'value' => 1
                        ]);

                    $this->info('Modificación de nombre de cliente en settings');
                    
                    DB::connection('tenant')->table('mo_settings')
                        ->where('name', 'idioma_defecto_id')
                        ->update([
                            'value' => $saaswebsite->default_language_id
                        ]);
                    $this->info('Modificación de moneda por defecto');

                    DB::connection('tenant')->table('mo_password_history')
                        ->whereNull('mo_password_history.deleted_at')
                        ->where('mo_password_history.id', 1)
                        ->update([
                            'password' =>  $datos_user->password,
                        ]);
                    
                    $this->info("Modificación de historial de contraseñas del usuario");

                    DB::connection('tenant')->table('mo_language')
                        ->whereNull('mo_language.deleted_at')
                        ->whereIn('mo_language.id', $sql_language->get(['language_id'])->map(function ($x) {
                            return (array)$x;
                        })->toArray())
                        ->update([
                            'front' => 1
                        ]);

                    DB::connection('tenant')->table('mo_language')
                        ->whereNull('mo_language.deleted_at')
                        ->whereNotIn('mo_language.id', $sql_language->get(['language_id'])->map(function ($x) {
                            return (array)$x;
                        })->toArray())
                        ->update([
                            'front' => 0
                        ]);

                    $this->info("Modificación de idiomas del sistema");

                    DB::connection('tenant')->table('mo_currency')
                    ->whereNull('mo_currency.deleted_at')
                    ->whereIn('mo_currency.id', $sql_currency->get(['currency_id'])->map(function ($x) {
                        return (array)$x;
                    })->toArray())
                    ->update([
                        'front' => 1
                    ]);
                    

                    DB::connection('tenant')->table('mo_currency')
                        ->whereNull('mo_currency.deleted_at')
                        ->whereNotIn('mo_currency.id', $sql_currency->get(['currency_id'])->map(function ($x) {
                            return (array)$x;
                        })->toArray())
                        ->update([
                            'front' => 0
                        ]);

                    DB::connection('system')->table('saas_website')
                        ->where('id', $saaswebsite->id)
                        ->update([
                            'website_id' => $website->id,
                            'status_id' => 4,
                        ]);

                    $this->info("Modificación de monedas del sistema");

                    DB::connection('system')->table('saas_user')
                        ->where('saas_user.email', $datos_user->email)
                        ->whereNull('saas_user.deleted_at')
                        ->update([
                            'tenant_user_id' => $datos_user_tenant->id,
                        ]);

                    $this->info("Modificación de usuario del sistema");

                    $language = DB::connection('system')->table('saas_language')
                        ->whereNull('saas_language.deleted_at')
                        ->where('saas_language.id', $saaswebsite->default_language_id)
                        ->first();

                    app('translator')->setLocale(strtolower($language->abbreviation));

                    $mensaje = __('website.create.email.welcome');
                    $mensaje .= __('website.create.email.access', ['url' => $protocol . $saaswebsite->domain.'.'.$control_panel_route]);

                    $ruta = $protocol . $ruta_api . '/api/saas/email';

                    $client = new \GuzzleHttp\Client();
                    $res = $client->request('POST', $ruta, ['json' => ['title' => 'Bienvenido',
                        'body' => $mensaje,
                        'template' => 'welcome',
                        'emails' => [
                            '0' => [
                                'email' => $datos_user->email,
                                'subject_email' => 'Bienvenido a Viavox Experience',
                            ]]], 'http_errors' => false]);

                    $this->info($res->getBody()->getContents());
                            
                    $this->info("Envio del email de alta");

                    $this->info('[' .  Carbon::now()->format('Y-m-d H:i:s') . '] Fin de creación de website');

                }

                $log->date_end = Carbon::now()->format('Y-m-d H:i:s');
                $log->save();
            }

        }catch(\Exception $e){
            $this->info($e->getMessage());
        }
    }
}
