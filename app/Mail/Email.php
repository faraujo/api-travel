<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Email extends Mailable
{
    use Queueable, SerializesModels;

    protected $from_email;
    protected $from_name;
    protected $subject_email;
    protected $template;
    protected $title;
    protected $adjuntos;
    protected $body;
    protected $storage_files;
    protected $client_logo;
    protected $social_media;
    protected $front_url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($array_datos)
    {
        //
        //
        $this->from_email = $array_datos['from_email'];
        $this->from_name = $array_datos['from_name'];
        $this->subject_email = $array_datos['subject_email'];
        $this->template = $array_datos['template'];
        $this->title = $array_datos['title'];
        $this->adjuntos = $array_datos['adjuntos'];
        $this->body = $array_datos['body'];
        if(isset($array_datos['storage_files'])){
            $this->storage_files = $array_datos['storage_files'];
        }else{
            $this->storage_files = '';
        }
        $this->client_logo = isset($array_datos['client_logo']) ? $array_datos['client_logo'] : null;
        $this->social_media = isset($array_datos['social_media']) ? $array_datos['social_media'] : null;
        $this->front_url = isset($array_datos['front_url']) ? $array_datos['front_url'] : null;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->markdown($this->template)
            ->with(['title' => $this->title, 'body' => $this->body, 'storage_files' => $this->storage_files, 
                'client_logo' => $this->client_logo, 'social_media' => $this->social_media, 'front_url' => $this->front_url])
            ->from($this->from_email, $this->from_name)
            ->subject($this->subject_email);
        if($this->adjuntos != ''){
            foreach($this->adjuntos as $adjunto){
                $email->attach($adjunto);
            }
        }

        return $email;
    }
}
