<?php

namespace App\Http\Middleware;


use App\Models\tenant\Settings;
use App\Models\tenant\Subchannel;
use App\Models\tenant\User;
use Closure;
use App\Models\tenant\UserToken;
use Carbon\Carbon;

class AutorizacionSubchannel
{

    public function handle($request, Closure $next)
    {
        $error = 0;
        if ($request->header('SubchannelToken')) {

            $token = explode(' ', $request->header('SubchannelToken'));
            if (isset($token[1])) {
                $subchannel_token = Subchannel::where('authorization_token', '=', $token[1])->where('id', $request->get('subchannel_id'))->first();
                if ($subchannel_token != '') {
                    $error = 0;
                } else {
                    $error = 1;
                }
            } else {
                $error = 1;
            }
        } else {
            $error = 1;
        }

        if ($error == 0) {
            $respuesta = $next($request);
        } else {
            $respuesta = \response()->json(['error' => 401, 'error_description' => 'Unauthorized'], 401);
        }

        return $respuesta;

    }
}