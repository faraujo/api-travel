<?php

namespace App\Http\Middleware;

use App\Models\tenant\User;
use App\Models\tenant\UserRole;
use App\Models\tenant\UserToken;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\DB;

class RolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  $todos
     * @param  $permisos_param
     * @return mixed
     */
    public function handle($request, Closure $next, $todos = null, $permisos_param = null, $admin = 0, $subchannel = 0)
    {
        $error = 0;
        if ($permisos_param != null) {

            $barra = '/';
            $token = explode(' ', $request->header('Authorization'));
            $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->where('deleted_at', '=', null)->first();
            $user = User::where('id', '=', $user_token->user_id)->first();
            $roles = UserRole::where('user_id', '=', $user->id)->get();
            $permisos = explode($barra, $permisos_param);

            if ($subchannel == 1) {
                $subcanal = $request->get('subchannel_id');

                $sql_permisos = DB::connection('tenant')->table('mo_permission')
                    ->select('mo_permission.id')
                    ->whereNull('mo_permission.deleted_at')
                    ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                    ->whereNull('mo_role_permission.deleted_at')
                    ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                    ->whereNull('mo_module.deleted_at')
                    ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                    ->whereNull('mo_module_subchannel.deleted_at')
                    ->where('mo_module_subchannel.subchannel_id', '=', $subcanal)
                    ->where('mo_permission.subchannel', '=', 1)
                    ->where('mo_role_permission.subchannel_id', $subcanal)
                    ->groupBy('mo_permission.id');

                $sql_permiso_global_canal = clone $sql_permisos;
                $sql_permiso_global_canal->where('mo_permission.id', '=', 109);

                $array_permisos_acceso_canal = array();
                foreach ($roles as $rol) {
                    $sql_permisos_clone = clone $sql_permiso_global_canal;
                    $datos_permisos_clone = $sql_permisos_clone->where('mo_role_permission.role_id', '=', $rol->role_id)->get();
                    
                    foreach ($datos_permisos_clone as $permission) {
                        $array_permisos_acceso_canal[] = $permission->id;
                    }
                }
                if (count($array_permisos_acceso_canal) > 0) {
                    $sql_permisos->whereIn('mo_permission.id', $permisos);

                    $array_permisos = array();
                    foreach ($roles as $rol) {
                        $sql_permisos_clone = clone $sql_permisos;
                        $datos_permisos_clone = $sql_permisos_clone->where('mo_role_permission.role_id', '=', $rol->role_id)->get();
                        foreach ($datos_permisos_clone as $permission) {
                            $array_permisos[] = $permission->id;
                        }
                    }
                    if($todos == 1){
                        $error = 0;
                        foreach ($permisos as $permiso) {
                            if (!in_array($permiso, $array_permisos)) {
                                $error = 1;
                            }
                        }
                    }else{
                        $error = 1;
                        foreach ($permisos as $permiso) {
                            if (in_array($permiso, $array_permisos)) {
                                $error = 0;
                            }
                        }
                    }
                } else {
                    $error = 1;
                }

            } else {

                $sql_permiso_global_canal = DB::connection('tenant')->table('mo_permission')
                    ->select('mo_permission.id')
                    ->whereNull('mo_permission.deleted_at')
                    ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                    ->whereNull('mo_role_permission.deleted_at')
                    ->where('mo_permission.admin', '=', 1)
                    ->where('mo_permission.id', '=', 108)
                    ->groupBy('mo_permission.id');

                $array_permisos_acceso_canal = array();
                foreach ($roles as $rol) {
                    $sql_permisos_clone = clone $sql_permiso_global_canal;
                    $datos_permisos_clone = $sql_permisos_clone->where('mo_role_permission.role_id', '=', $rol->role_id)->get();
                    foreach ($datos_permisos_clone as $permission) {
                        $array_permisos_acceso_canal[] = $permission->id;
                    }
                }

                $permisocanal = false;
                foreach ($permisos as $permiso) {
                    if($permiso == 109) {
                        $permisocanal = true;
                    }
                }
//
                $array_permisos = array();

                if($permisocanal){
                    $sql_permisos_canal = DB::connection('tenant')->table('mo_user_role')
                        ->select('mo_permission.id')
                        ->join('mo_role','mo_role.id','=','mo_user_role.role_id')
                        ->whereNull('mo_role.deleted_at')
                        ->join('mo_role_permission','mo_role_permission.role_id','mo_role.id')
                        ->whereNull('mo_role_permission.deleted_at')
                        ->join('mo_permission','mo_permission.id','mo_role_permission.permission_id')
                        ->whereNull('mo_permission.deleted_at')
                        ->where('mo_permission.subchannel', '=', 1)
                        ->where('mo_permission.id','=',109)
                        ->groupBy('mo_permission.id');
                    foreach ($roles as $rol) {
                        $sql_permisos_canal_clone = clone $sql_permisos_canal;
                        $datos_permisos_clone = $sql_permisos_canal_clone->where('mo_role_permission.role_id', '=', $rol->role_id)->first();
                        if($datos_permisos_clone){
                            $array_permisos[] = $datos_permisos_clone->id;
                        }
                    }
                }

                if (count($array_permisos_acceso_canal) > 0 || count($array_permisos) > 0) {

                    $sql_permisos = DB::connection('tenant')->table('mo_permission')
                        ->select('mo_permission.id')
                        ->whereNull('mo_permission.deleted_at')
                        ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                        ->whereNull('mo_role_permission.deleted_at')
                        ->where('mo_permission.admin', '=', 1)
                        ->whereIn('mo_permission.id', $permisos)
                        ->groupBy('mo_permission.id');


                    foreach ($roles as $rol) {
                        $sql_permisos_clone = clone $sql_permisos;
                        $datos_permisos_clone = $sql_permisos_clone->where('mo_role_permission.role_id', '=', $rol->role_id)->get();
                        foreach ($datos_permisos_clone as $permission) {
                            $array_permisos[] = $permission->id;
                        }
                    }
                    if($todos == 1){
                        $error = 0;
                        foreach ($permisos as $permiso) {
                            if (!in_array($permiso, $array_permisos)) {
                                $error = 1;
                            }
                        }
                    }else{
                        $error = 1;
                        foreach ($permisos as $permiso) {
                            if (in_array($permiso, $array_permisos)) {
                                $error = 0;
                            }
                        }
                    }
                } else {
                    $error = 1;
                }
            }
        }

        if($error == 0){
            return $next($request);
        }else{
            return response()->json(['error' => 401, 'error_description' => 'Unauthorized roles'], 401);
        }

    }
}