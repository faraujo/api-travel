<?php

namespace App\Http\Middleware;


use App\Models\SaasSettings;
use App\Models\SaasUser;
use Closure;
use App\Models\SaasUserToken;
use Carbon\Carbon;

class SaasAutorizacionSubchannel
{

    public function handle($request, Closure $next)
    {
        $error = 0;
        if ($request->header('SubchannelToken')) {

            $token = explode(' ', $request->header('SubchannelToken'));            
            if (isset($token[1])) {
                $subchannel_token = SaasSettings::where('name', '=', 'subchannel_token')->first();
                if ($subchannel_token != '' && $subchannel_token->value == $token[1]) {
                    $error = 0;
                } else {
                    $error = 1;
                }
            } else {
                $error = 1;
            }
        } else {
            $error = 1;
        }

        if ($error == 0) {
            $respuesta = $next($request);
        } else {
            $respuesta = \response()->json(['error' => 401, 'error_description' => 'Unauthorized'], 401);
        }

        return $respuesta;

    }
}