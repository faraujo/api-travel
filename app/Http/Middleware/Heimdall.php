<?php

namespace App\Http\Middleware;
use Hyn\Tenancy\Models\Hostname;
use Illuminate\Support\Facades\DB;

use Closure;

class Heimdall
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $respuesta = null;

        

        $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
            ->where('mo_settings.name', 'heimdall_middleware')
            ->first()
            ->value;
        
        if($heimdall_middleware == '1') {

            $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();
    
            $website = DB::table('saas_website')
                            ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                            'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                            'saas_website.default_language_id', 'saas_website.default_currency_id')
                            ->whereNull('saas_website.deleted_at')
                            ->where('saas_website.website_id', $website->id)
                            ->first();
    
            $plans = websitePlanRules($website)->where('plan_id', '!=', null);

            if(count($plans) <= 0) {
                 //Heimdall - datos para redirección
                 $array['data'] = array();
                 $array['data'][0]['website_id'] = $website->id;

                 $protocol = DB::table('saas_settings')
                 ->where('saas_settings.name', 'protocol')->first()->value;
                 $base_path_saas = DB::table('saas_settings')
                 ->where('saas_settings.name', 'base_path_saas')->first()->value;
                 $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                $respuesta = response()->json(['error' => 419, 'error_description' => 'You do not have an active plan', 'data' => $array['data']], 419);
            } else {
                $respuesta = $next($request);
            }
        } else {
            $respuesta = $next($request);
        }

        return $respuesta;
    }
}
