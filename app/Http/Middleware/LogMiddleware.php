<?php

namespace App\Http\Middleware;


use App\Models\tenant\LogGateway;
use App\Models\tenant\User;
use App\Models\tenant\UserToken;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class LogMiddleware
{

    public function handle($request, Closure $next, $start_petition, $input_data)
    {
        $response = $next($request);
        $user = null;

        if($request->header('Authorization')){
            $token = explode(' ', $request->header('Authorization'));
            $user_token = UserToken::where('token','=', $token[1])->where('expired_at', '>=', Carbon::now())->where('deleted_at', '=', null)->first();
            $user = $user_token ? User::where('id', '=', $user_token->user_id)->first() : null;
        }

        LogGateway::create([
            'user_id' => $user != '' ? $user->id : null,
            'input_data' => $input_data,
            'output_data' => $response,
            'start_petition' => $start_petition,
            'end_petition' => Carbon::now(),
            'petition_ip' => $request->ip(),
        ]);


        return $response;
    }


}