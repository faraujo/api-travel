<?php

namespace App\Http\Middleware;

use App\Models\SaasSettings;
use App\Models\SaasUser;
use App\Models\SaasUserToken;
use Carbon\Carbon;
use Closure;

use Symfony\Component\HttpFoundation\Response;

class SaasAutorizacion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $respuesta = null;
        $respuesta = \response()->json(['error' => 401, 'error_description' => 'Unauthorized'], 401);
        if ($request->header('Authorization')) {
            $token = explode(' ', $request->header('Authorization'));

            if (isset($token[1])) {
                $user_token = SaasUserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();
                if ($user_token != '') {
                    $user = SaasUser::find($user_token->user_id);
                    //se comprueba en cada petición que el usuario no esté bloqueado y que tenga token sin espirar
                    if ($user && $user->bloqued_login != 1 && ($user->bloqued_to == null || $user->bloqued_to < Carbon::now())) {
                        $user_token->expired_at = Carbon::now()->addMinutes(SaasSettings::where('name', '=', 'token_validate')->first()->value);
                        $user_token->save();
                        $respuesta = $next($request);
                    }
                }
            }
        }

        return $respuesta;

    }
}