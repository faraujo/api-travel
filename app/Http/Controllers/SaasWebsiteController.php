<?php

namespace App\Http\Controllers;

use App\Models\SaasWebsite;
use App\Models\SaasWebsitePlan;
use App\Models\SaasPlan;
use App\Models\SaasRule;
use App\Models\SaasSettings;
use App\Models\SaasCurrency;
use App\Models\SaasLanguage;
use App\Models\SaasWebsiteCurrency;
use App\Models\SaasWebsiteLanguage;
use App\Models\SaasUserToken;
use App\Models\SaasUser;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class SaasWebsiteController extends Controller
{
    /**
     * Función para la creación de un saas website
     *
     * Para la creación de un saas website se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createWebsite(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $user_id = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }   
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $array['data'] = array();

                // Crear el saas website
                $website = SaasWebsite::create([
                    'user_id' => $user_id,
                    'status_id' => 1 //Se inicializa con estado 'Pendiente de Pago'
                ]);
                // FIN el saas website

                $array['data'][] = ['id' => $website->id];

            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la creación de un saas website plan
     *
     * Para la creación de un saas website plan se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createWebsitePlan(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'website_id' => 'required|integer|min:0|exists:saas_website,id,deleted_at,NULL',
                'plan_id' => 'required_without:rule_code|integer|min:0|exists:saas_plan,id,deleted_at,NULL',
                'rule_code' => 'required_without:plan_id|exists:saas_rule,code,deleted_at,NULL',
                'value' => 'required_with:rule_code|integer|min:0',
                'unlimited' => 'required_with:rule_code|boolean',
                // 'date_start' => 'date|required|date_format:"Y-m-d H:i:s"',
                // 'date_end' => 'date|required|date_format:"Y-m-d H:i:s"|after_or_equal:date_start',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }           

            if ($request->get('plan_id') != '' && $request->get('rule_code') != '') {               
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['planrule' => ['It is only possible to specify a plan or a rule']]);
            }
            //Fin de validación

            $user_id = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }


            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $datos_website = SaasWebsite::where('id',$request->get('website_id'))
                    ->first();

                $error_aux = 0;

                if($datos_website) {
                    if ($user_id != '') {
                        if ($datos_website->user_id != $user_id) {
                            $error_aux = 1;
                        }
                    }
                } else {
                    $error_aux = 1;
                }

                if ($error_aux == 1) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The website plan can not be created']]);
                }

                if ($error == 1) {
                    $array['error'] = 400;
                    $array['error_description'] = 'The fields are not the required format';
                    $array['error_inputs'][] = $mensaje_validador;
                } else {
                    // $fecha_inicio = Carbon::createFromFormat('Y-m-d H:i:s', $request->get('date_start'));
                    // $fecha_fin = Carbon::createFromFormat('Y-m-d H:i:s', $request->get('date_end'));

                    $fecha_inicio = Carbon::now()->startOfDay();
                    
                    $fecha_fin = Carbon::now()->addMonth()->endOfDay();

                    $sale_price = null;
                    if($request->get('plan_id') != ''){
                        $plan = SaasPlan::where('id', '=', $request->get('plan_id'))->first();
                        $sale_price = $plan->sale_price;
                    }
                    if($request->get('rule_code') != ''){
                        $rule = SaasRule::where('code', '=', $request->get('rule_code'))->first();
                        $sale_price = $rule->sale_price;
                    }

                    // Crear el saas website plan
                    $websiteplan = SaasWebsitePlan::create([
                        'website_id' => $request->get('website_id'),
                        'plan_id' => $request->get('plan_id') != '' ? $request->get('plan_id') : null,
                        'rule_id' => $request->get('rule_code') != '' ? $rule->id : null,
                        'value' => $request->get('rule_code') != '' && $request->get('value') != '' ? $request->get('value') : null,
                        'unlimited' => $request->get('rule_code') != '' && $request->get('unlimited') != '' ? $request->get('unlimited') : 0,
                        'sale_price' => $sale_price,
                        'code' => $request->get('plan_id') != '' ? $plan->code : $rule->code,
                        'status_id' => $request->get('plan_id') == 1 ? 2 : 1, //Se inicializa con estado 'Pendiente de Pago' salvo en plan gratuito
                        'date_start' => $fecha_inicio->format('Y-m-d H:i:s'),
                        'date_end' => $request->get('plan_id') == 1 ? Carbon::createFromDate('2099')->endOfYear()->format('Y-m-d H:i:s') : $fecha_fin->format('Y-m-d H:i:s'),
                    ]);
                    // FIN el saas website plan

                    //Caso de plan gratuito
                    if($request->get('plan_id') == 1){

                        //Actualización estado website
                        $website = SaasWebsite::where('id',$request->get('website_id'))->first();
                        if($website->status_id == 1){
                            $website->update([
                                'status_id' => 2, //Estado 'Asistente'
                            ]);
                        }

                        //Sustitución como plan vigente
                        $fecha_hora_operacion = Carbon::now()->format('Y-m-d H:i:s');
                        SaasWebsitePlan::where('website_id', $request->get('website_id'))
                        ->whereNotNull('plan_id')
                        ->whereNotIn('id', [$websiteplan->id])
                        ->whereNull('date_replace')
                        ->update([
                            'date_replace' => $fecha_hora_operacion,
                        ]);

                        $email = paymentEmail($websiteplan->id, 'new');
                    }

                    $array['data'][] = ['id' => $websiteplan->id];
                }                
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }
        return response()->json($array, $array['error']);
    }
   
    /**
     * Función para la actualización de un saas website
     *
     * Para la actualización de un saas website se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateWebsite(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
     
            //Validación idioma por defecto
            $check_default_language = 0;
            if ($request->get('default_language_id') != '') {        
                if($request->get('languages') && is_array($request->get('languages'))){
                    if(!in_array($request->get('default_language_id'), $request->get('languages'))){
                        $check_default_language = 1;
                    }
                }else{
                    $language = SaasWebsiteLanguage::where('website_id', '=', $request->get('id'))
                        ->where('language_id', '=', $request->get('default_language_id'))->first();

                    if (!$language) {
                        $check_default_language = 1;
                    } 
                }
                if($check_default_language == 1){
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['default_language_id' => ['The language default selected is not valid']]);
                }
            }

            //Validación moneda por defecto
            $check_default_currency = 0;
            if ($request->get('default_currency_id') != '') {        
                if($request->get('currencies') && is_array($request->get('currencies'))){
                    if(!in_array($request->get('default_currency_id'), $request->get('currencies'))){
                        $check_default_currency = 1;
                    }
                }else{
                    $currency = SaasWebsiteCurrency::where('website_id', '=', $request->get('id'))
                        ->where('currency_id', '=', $request->get('default_currency_id'))->first();
                    if (!$currency) {
                        $check_default_currency = 1;
                    } 
                }
                if($check_default_currency == 1){
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['default_currency_id' => ['The currency default selected is not valid']]);
                }
            }
            //Fin de validación



            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'step' => 'required|integer|min:0',
                'name' => 'required_if:step,1',
                'domain' => 'required_if:step,1|regex:/^[a-z0-9ñÑ]+$/|unique:saas_website,domain,'. $request->get('id').',id,deleted_at,NULL',
                'nif' => 'required_if:step,1|unique:saas_website,nif,'. $request->get('id').',id,deleted_at,NULL',
                'website_id' => 'integer|min:0|exists:websites,id,deleted_at,NULL',
                'finished' => 'required_if:step,2|boolean',                
                'languages' => 'required_if:step,2|array',
                'languages.*' => 'required|integer|min:0|exists:saas_language,id,deleted_at,NULL',
                'currencies' => 'required_if:step,2|array',
                'currencies.*' => 'required|integer|min:0|exists:saas_currency,id,deleted_at,NULL',
                'default_language_id' => 'required_if:step,2|integer|min:0|exists:saas_language,id,deleted_at,NULL',
                'default_currency_id' => 'required_if:step,2|integer|min:0|exists:saas_currency,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
              

            $user_id = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $website = SaasWebsite::where('id', $request->get('id'))->first();
                if (!$website) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    $error_aux = 0;

                    if($website) {
                        if ($user_id != '') {
                            if ($website->user_id != $user_id) {
                                $error_aux = 1;
                            }
                        }
                    } else {
                        $error_aux = 1;
                    }

                    if ($error_aux == 1) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The website can not be updated']]);
                    }

                    if ($error == 1) {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = $mensaje_validador;
                    } else {
                        //Actualizar website
                        if($request->get('name') != ''){
                            $website->name = $request->get('name');
                        }
                        if($request->get('domain') != '' && $website->status_id == 2){
                            $website->domain = STR::lower($request->get('domain'));
                        }
                        if($request->get('nif') != ''){
                            $website->nif = $request->get('nif');
                        }
                        if($request->get('default_language_id') != ''){
                            $website->default_language_id = $request->get('default_language_id');
                        }
                        if($request->get('default_currency_id') != ''){
                            $website->default_currency_id = $request->get('default_currency_id');
                        }
                        $website->save();

                        //Crear relaciones con idiomas
                        if ($request->get('languages') != '') {

                            $website->saasWebsiteLanguage()->delete();

                            foreach ($request->get('languages') as $language) {
                                SaasWebsiteLanguage::create([
                                    'website_id' => $website->id,
                                    'language_id' => $language,
                                ]);
                            }
                        }

                        //Crear relaciones con monedas
                        if ($request->get('currencies') != '') {

                            $website->saasWebsiteCurrency()->delete();

                            foreach ($request->get('currencies') as $currency) {
                                SaasWebsiteCurrency::create([
                                    'website_id' => $website->id,
                                    'currency_id' => $currency,
                                ]);
                            }
                        }

                        //Actualización estado website al finalizar el asistente
                        if($request->get('finished')){
                            if($website->status_id == 2){
                                $website->update([
                                    'status_id' => 3, //Estado 'Pendiente de desplegar'
                                ]);
                            }
                            /*if($website->status_id == 2 && $website->website_id != null){
                                $website->update([
                                    'status_id' => 4, //Estado 'Desplegado'
                                ]);
                            }*/
                        }

                        //Actualización de datos de usuario en caso de estar ambos (nombre y cif de empresa) vacíos
                        $user = SaasUser::where('id', $user_id)->first();
                        if($request->get('name') != '' && $request->get('nif') != ''){
                            if(($user->business_name == null || $user->business_name == '') && ($user->business_number_document == null || $user->business_number_document == '')){
                                $user->business_name = $request->get('name');
                                $user->business_number_document = $request->get('nif');
                                $user->save();
                            }
                        }
                    }
                                        
                }
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todos los websites
     *
     * Para la consulta de websites se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showWebsites(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
                //'user_id' => 'integer|min:0'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN Validación de campos

            $user_id = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Se preparan las consultas
                $websites = DB::table('saas_website')
                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                    ->whereNull('saas_website.deleted_at')
                    ->where('saas_website.user_id', $user_id);
                    
                $sub = DB::table('saas_website')
                    ->select('saas_website.id')
                    ->whereNull('saas_website.deleted_at')
                    ->where('saas_website.user_id', $user_id);  
                    
                $user = SaasUser::where('id', $user_id)->first();
                
                //Lectura parámetros de ruta CP
                $protocol = SaasSettings::where('name', 'protocol')->first()->value;
                $base_path_control_panel = SaasSettings::where('name', 'base_path_control_panel')->first()->value;

                // Recogida de datos enviados por el usuario y establecimiento de filtros
                if ($request->get('name') != '') {
                    $websites->Where(function ($query) use ($request) {
                        $query->where('saas_website.name', 'Like', '%' . $request->get('name') . '%');
                    });
                    
                    $sub->Where(function ($query) use ($request) {
                        $query->where('saas_website.name', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                // Order
                $orden = 'saas_website.user_id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'saas_website.name';
                            break;
                        case 'user_id':
                            $orden = 'saas_website.user_id';
                            break;
                        case 'id':
                            $orden = 'saas_website.id';
                            break;
                        default:
                            $orden = 'saas_website.user_id';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $websites->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = SaasSettings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_websites = $websites->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_websites = $websites->get();

                }
                //Fin de paginación

                $websites_count = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $websites_count->count();

                $array['data'] = array();
                
                $array['data'][0]['user'][] = [
                    'id' => $user_id,
                    'verified_email' => $user['verified_email'],
                ];

                foreach ($datos_websites as $web) {

                                       
                    $web->plan = array();

                    //plan y reglas vigentes asociados al website
                    $plans = websitePlanRules($web);

                    foreach ($plans as $plan) {

                        //Planes
                        if($plan->plan_id != null){
                            //reglas asociadas al plan
                            $plan_rules = websiteRules($plan);

                            foreach ($plan_rules as $plan_rule){
                                $plan->rules[] = [
                                    'id' => $plan_rule->id,
                                    'plan_id' => $plan_rule->plan_id,
                                    'rule_id' => $plan_rule->rule_id,
                                    'value' => $plan_rule->value,
                                    'unlimited' => $plan_rule->unlimited,
                                ];
                            }

                            $web->plan[] = [
                                'id' => $plan->plan_id,
                                'code' => $plan->code,
                                'date_start' => $plan->date_start,
                                'date_end' => $plan->date_end,
                                'rules' => $plan->rules
                            ];
                        } 
                    }
                    
                    //Ruta CP
                    $cp_route = $web->domain && $web->domain != '' ?  $protocol.$web->domain.'.'.$base_path_control_panel : null;
                    $cp_name = $web->domain && $web->domain != '' ?  $web->domain.'.'.$base_path_control_panel : null;

                    $array['data'][0]['websites'][] = [
                        'id' => $web->id,
                        'name' => $web->name,
                        'domain' => $web->domain,
                        'cp_route' => $cp_route,
                        'cp_name' => $cp_name,
                        'status_id' => $web->status_id,
                        'plan'=> $web->plan,
                    ];
                }
                $array['total_results'] = $totales;
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra un website buscado por su id
     *
     * Para la consulta de un website se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showWebsiteId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::beginTransaction();
            
            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $user_id = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                if ($user_token) {
                    $user_id = $user_token->user_id;
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $now = Carbon::now()->startOfDay()->format('Y-m-d H:i:s');

                $website = DB::table('saas_website')
                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                    ->whereNull('saas_website.deleted_at')
                    ->where('saas_website.user_id', $user_id);                   

                $website->where('saas_website.id', '=', $id);

                //Lectura parámetros de ruta CP
                $protocol = SaasSettings::where('name', 'protocol')->first()->value;
                $base_path_control_panel = SaasSettings::where('name', 'base_path_control_panel')->first()->value;

                $website = $website->get();

                $array['data'] = array();

                foreach ($website as $web) {       
                    
                    //Ruta CP
                    $cp_route = $web->domain && $web->domain != '' ?  $protocol.$web->domain.'.'.$base_path_control_panel : null;
                    $cp_name = $web->domain && $web->domain != '' ?  $web->domain.'.'.$base_path_control_panel : null;
                    $web->cp_route = $cp_route;
                    $web->cp_name = $cp_name;

                    $web->languages = array();
                    $web->currencies = array();
                    $web->plan = array();
                    $web->rules = array();

                    //idiomas asociados al website
                    $languages = DB::table('saas_website_language')->where('website_id', $web->id)
                        ->select('language_id as id')
                        ->join('saas_language', 'saas_website_language.language_id', 'saas_language.id')
                        ->whereNull('saas_language.deleted_at')
                        ->whereNull('saas_website_language.deleted_at')
                        ->get();
                    foreach ($languages as $language) {
                        $web->languages[] = $language;
                    }

                    //monedas asociadas al website
                    $currencies = DB::table('saas_website_currency')->where('website_id', $web->id)
                        ->select('currency_id as id')
                        ->join('saas_currency', 'saas_website_currency.currency_id', 'saas_currency.id')
                        ->whereNull('saas_currency.deleted_at')
                        ->whereNull('saas_website_currency.deleted_at')
                        ->get();
                    foreach ($currencies as $currency) {
                        $web->currencies[] = $currency;
                    }

                    //plan y reglas vigentes asociados al website
                    $plans = websitePlanRules($web);
                    
                    foreach ($plans as $plan) {

                        //Planes
                        if($plan->plan_id != null){
                            //reglas asociadas al plan
                            $plan_rules = websiteRules($plan);

                            foreach ($plan_rules as $plan_rule){
                                $plan->rules[] = [
                                    'id' => $plan_rule->id,
                                    'plan_id' => $plan_rule->plan_id,
                                    'rule_id' => $plan_rule->rule_id,
                                    'code' => $plan_rule->code,
                                    'value' => $plan_rule->value,
                                    'unlimited' => $plan_rule->unlimited,
                                ];
                            }

                            //$web->plans[] = $plan;
                            $web->plan[] = [
                                'id' => $plan->plan_id,
                                'plan_rule_id' => $plan->plan_rule_id,
                                'code' => $plan->code,
                                'date_start' => $plan->date_start,
                                'date_end' => $plan->date_end,
                                'sale_price' => $plan->sale_price,
                                'rules' => $plan->rules,
                                'cancelled' => $plan->cancelled,
                            ];

                        //Reglas sueltas
                        } else{
                            $web->rules[] = [
                                'id' => $plan->rule_id,
                                'plan_rule_id' => $plan->plan_rule_id,
                                'code' => $plan->code,
                                'rule_code' => $plan->rule_code,
                                'value' => $plan->value,
                                'unlimited' => $plan->unlimited,
                                'date_start' => $plan->date_start,
                                'date_end' => $plan->date_end,
                                'sale_price' => $plan->sale_price,
                                'cancelled' => $plan->cancelled,
                            ];
                        }
                    }
                    
                    $array['data'][0]['website'][] = $web;
                    
                }
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }

        return response()->json($array, $array['error']);

    }

    /**
     * Función que muestra una regla buscado por su code
     *
     * Para la consulta de un website se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showRule(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::beginTransaction();
            
            //Validación
            $error = 0;
            $mensaje_validador = collect();

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_rule = DB::table('saas_rule')
                    ->select(
                        'saas_rule.id', 
                        'saas_rule.code',
                        'saas_rule.sale_price'
                    )
                    ->whereNull('saas_rule.deleted_at');   

                $sql_rule->where('saas_rule.code', '=', $request->get('rule_code'));

                $rules = $sql_rule->get();

                $array['data'] = array();

                foreach ($rules as $rule) {      
                    
                    $rule_array = array();

                    $rule_array['id'] = $rule->id;
                    $rule_array['code'] = $rule->code; 
                    $rule_array['sale_price'] = $rule->sale_price;              
                    
                    $array['data'][0]['rule'][] = $rule_array;
                    
                }
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }

        return response()->json($array, $array['error']);

    }

    /**
     * Función para mostrar las monedas.
     *
     * Para la consulta de monedas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCurrencies(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::beginTransaction();

            //Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'limit' => 'integer|min:0',
                'page' => 'integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $monedas = DB::table('saas_currency')
                    ->select('saas_currency.id', 'saas_currency.name', 'saas_currency.simbol', 'saas_currency.iso_number', 'saas_currency.iso_code', 'saas_currency.presentation_simbol', 'saas_currency.position', 'saas_currency.decimal_separator', 'saas_currency.thousand_separator')
                    ->where('saas_currency.deleted_at', '=', null);

                $sub = DB::table('saas_currency')
                    ->select('saas_currency.id')
                    ->where('saas_currency.deleted_at', '=', null);

                //$id_moneda_principal = SaasSettings::where('name', '=', 'moneda_defecto_id')->first();

                /*$moneda_principal = SaasCurrency::where('id', '=', $id_moneda_principal->value)->first(['id', 'name', 'simbol', 'iso_number', 'iso_code', 'presentation_simbol',
                    'position', 'decimal_separator', 'thousand_separator']);*/


                // Order
                $orden = 'name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'id';
                            break;
                        case 'name':
                            $orden = 'name';
                            break;
                        case 'order':
                                $orden = 'order';
                                break;
                        default:
                            $orden = 'name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $monedas = $monedas->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = SaasSettings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $monedas = $monedas->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $monedas = $monedas->get();

                }

                //Fin de paginación


                $monedas_count = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $monedas_count->count();

                $array['data'] = array();

                if ($totales > 0) {
                    $array['data'][0]['currency'] = $monedas;
                    //$array['data'][0]['main_currency'][] = $moneda_principal;
                }

                $array['total_results'] = $totales;

            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todos los idiomas
     *
     * Para la consulta de idiomas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showLanguages(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::beginTransaction();
            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'front' => 'boolean',
                'back' => 'boolean',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Preparación de las consultas
                $idiomas = SaasLanguage::all();

                if ($request->get('front') != '') {
                    $idiomas = $idiomas->where('front', '=', $request->get('front'));
                }

                if ($request->get('back') != '') {
                    $idiomas = $idiomas->where('back', '=', $request->get('back'));
                }

                $array['data'] = array();
                foreach ($idiomas as $idioma) {
                    $array['data'][0]['language'][] = $idioma;
                }
                $array['total_results'] = count($idiomas);

            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }

        return response()->json($array, $array['error']);
    }

    public function showWebsitePlanId ($id, Request $request) {
        $array['error'] = 200;

        try {

            DB::beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();        
            //Fin de validación

            $user_id = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $websitePlan = DB::table('saas_website_plan_rule')
                    ->select(
                        'saas_website_plan_rule.id',
                        'saas_website_plan_rule.website_id',
                        'saas_website_plan_rule.plan_id',
                        'saas_website_plan_rule.rule_id',
                        'saas_website_plan_rule.value',
                        'saas_website_plan_rule.unlimited',
                        'saas_website_plan_rule.sale_price',
                        'saas_website_plan_rule.code',
                        'saas_website_plan_rule.date_start',
                        'saas_website_plan_rule.date_end',
                        'saas_website_plan_rule.date_replace',
                        'saas_website_plan_rule.status_id'
                    )
                    ->whereNull('saas_website_plan_rule.deleted_at')
                    ->join('saas_website','saas_website.id','saas_website_plan_rule.website_id')
                    ->whereNull('saas_website.deleted_at')
                    ->whereNull('saas_website_plan_rule.date_replace')
                    ->where('saas_website_plan_rule.status_id', 1)
                    ->where('saas_website.user_id',$user_id)
                    ->where('saas_website_plan_rule.id', $id)
                    ->get();


                $array['data'] = array();

                foreach($websitePlan as $plan) {

                    $array_website = array();

                    $array_website['id'] = $plan->id;
                    $array_website['plan_id'] = $plan->plan_id;
                    $array_website['rule_id'] = $plan->rule_id;
                    $array_website['value'] = $plan->value;
                    $array_website['unlimited'] = $plan->unlimited;
                    $array_website['sale_price'] = $plan->sale_price;
                    $array_website['code'] = $plan->code;
                    $array_website['date_start'] = $plan->date_start;
                    $array_website['date_end'] = $plan->date_end;
                    $array_website['date_replace'] = $plan->date_replace;
                    $array_website['status_id'] = $plan->status_id;

                    $array['data'][0]['website_plan'][] = $array_website;
                }

                $protocol = DB::table('saas_settings')
                ->where('saas_settings.name', 'protocol')->first()->value;

                $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                $array['data'][0]['saas_url'] = $protocol . $base_path_saas;
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasWebsite');
        }
        return response()->json($array, $array['error']);
    }

}
