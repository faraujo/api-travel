<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Models\SaasLogg;
use App\Mail\Email;
use App\Models\SaasSettings;
use App\Models\Template;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use function PHPSTORM_META\elementType;

class SaasEmailController extends Controller
{


    /**
     * Método para el envío de emails
     *
     * Son requeridos dirección de email donde se envía, título de la página html que tendrá el email, cuerpo del mensaje, nombre de la plantilla que se envía "información" o "error" y mailbox
     * que contiene el nombre del buzón desde donde se envía el email, dirección de email desde donde se envía, nombre de quien envía el email, y su configuración de host, username, password, encryption, driver y port.
     * No requerido es el parámetro archivos adjuntos que contiene la url donde está el archivo adjuntado.
     * Para el envío de emails se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        $array['error'] = 200;
        try {

            $host = '';
            $username = '';
            $password = '';
            $encryption = '';
            $driver = '';
            $port = '';
            $from_email = '';
            $from_name = '';

            DB::beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'emails' => 'required|array',
                'emails.*.email' => 'required|email',
                'emails.*.subject_email' => 'required',
                'title' => 'required',
                'attachments' => 'array',
                'template' => 'required|in:recover,information,error,welcome,verify', // TODO afinar

            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('template') != '' && !\File::exists(resource_path('views/emails/' . $request->get('template') . '.blade.php'))) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['template' => ['Template not exists']]);
            }

            if ($request->get('mailbox') != '') {
                $host = SaasSettings::where('name', '=', 'email_host_' . $request->get('mailbox'))->first();
                $username = SaasSettings::where('name', '=', 'email_username_' . $request->get('mailbox'))->first();
                $password = SaasSettings::where('name', '=', 'email_password_' . $request->get('mailbox'))->first();
                $encryption = SaasSettings::where('name', '=', 'email_encryption_' . $request->get('mailbox'))->first();
                $driver = SaasSettings::where('name', '=', 'email_driver_' . $request->get('mailbox'))->first();
                $port = SaasSettings::where('name', '=', 'email_port_' . $request->get('mailbox'))->first();
                $from_email = SaasSettings::where('name', 'email_from_' . $request->get('mailbox'))->first();
                $from_name = SaasSettings::where('name', 'email_fromname_' . $request->get('mailbox'))->first();
            } else {
                $host = SaasSettings::where('name', '=', 'email_host_default')->first();
                $username = SaasSettings::where('name', '=', 'email_username_default')->first();
                $password = SaasSettings::where('name', '=', 'email_password_default')->first();
                $encryption = SaasSettings::where('name', '=', 'email_encryption_default')->first();
                $driver = SaasSettings::where('name', '=', 'email_driver_default')->first();
                $port = SaasSettings::where('name', '=', 'email_port_default')->first();
                $from_email = SaasSettings::where('name', 'email_from_default')->first();
                $from_name = SaasSettings::where('name', 'email_fromname_default')->first();
            }

            if (($host == '') || ($username == '') || ($password == '') || ($encryption == '') || ($driver == '') || ($port == '') || ($from_email == '') || ($from_name == '')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['mailbox' => ['Mailbox not available']]);
            } else {
                $host = $host->value;
                $username = $username->value;
                $password = $password->value;
                $encryption = $encryption->value;
                $driver = $driver->value;
                $port = $port->value;
                $from_email = $from_email->value;
                $from_name = $from_name->value;
            }

            //Fin validacion

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                config([
                    'mail.driver' => $driver,
                    'mail.host' => $host,
                    'mail.port' => $port,
                    'mail.username' => $username,
                    'mail.password' => $password,
                    'mail.encryption' => $encryption,
                ]);

/*                 $template = '';
                if ($request->get('template') != '') { */
                    $template = $request->get('template');
/*                 } else {
                    $template = Template::where('name', '=', SaasSettings::where('name', '=', 'email_template_default')->first()->value)->first()->file_name;
                } */


                // Recogida de url de archivos del email
                // TODO configurar posibles imagenes
                //! $file_storage_url = SaasSettings::where('name', 'storage_files')->first()->value . 'mtr/public/email/common/';
                foreach ($request->get('emails') as $email) {

                    $array_datos = [
                        'from_email' => $from_email,
                        'from_name' => $from_name,
                        'subject_email' => $email['subject_email'],
                        'template' => 'emails.' . $template,
                        'title' => $request->get('title'),
                        'body' => $request->get('body'),
                        //! 'storage_files' => $file_storage_url,
                        'adjuntos' => $request->get('attachments')
                    ];

                    Mail::to($email['email'])
                        ->bcc(SaasSettings::where('name', 'email_copia_sistema')->first()->value)
                        ->queue(new Email($array_datos));

                }

                //log
                if (isset($array_datos['adjuntos'])) {
                    $adjuntos = '';
                    for ($i = 0; $i < count($array_datos['adjuntos']); $i++) {
                        $adjuntos .= $array_datos['adjuntos'][$i] . '   ';
                    }
                }
                //guarda un registro por cada dirección a la que envía
                foreach ($request->get('emails') as $email) {
                    SaasLogg::create([
                        'date' => date('Y-m-d H:i:s'),
                        'from' => $from_email,
                        'to' => $email['email'],
                        'cc' => null,
                        'bcc' => null,
                        'subject' => $email['subject_email'],
                        'body' => $request->get('body'),
                        'headers' => null,
                        'attachments' => (isset($adjuntos)) ? $adjuntos : null,
                        'description_error' => null,
                        'received_email' => 1,
                    ]);
                }
                //fin log
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage() . $e->getLine();
            //log
            if (isset($array_datos['adjuntos'])) {
                $adjuntos = '';
                for ($i = 0; $i < count($array_datos['adjuntos']); $i++) {
                    $adjuntos .= $array_datos['adjuntos'][$i] . '   ';
                }
            }
            //guarda un registro por cada dirección a la que envía
            foreach ($request->get('emails') as $email) {
                SaasLogg::create([
                    'date' => date('Y-m-d H:i:s'),
                    'from' => $from_email,
                    'to' => $email['email'],
                    'cc' => null,
                    'bcc' => null,
                    'subject' => $email['subject_email'],
                    'body' => $request->get('body'),
                    'headers' => null,
                    'attachments' => (isset($adjuntos)) ? $adjuntos : null,
                    'description_error' => utf8_encode($e->getMessage() . ' ' . $e->getLine()),
                    'received_email' => 0,
                ]);
            }
            //fin log
            
            reportSaasService($e, 'SaasEmails');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función que lista las plantillas de email disponibles
     * Para el listado de plantillas se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTemplate()
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();

            $templates = Template::all(['id', 'name', 'description', 'file_name']);
            $totales = count($templates);

            $array['data'] = array();

            $array['data'][0]['email'][0]['template'] = $templates;
            $array['total_results'] = $totales;

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            
            reportSaasService($e, 'SaasEmails');
        }

        return response()->json($array, $array['error']);
    }

}