<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

//use App\Country;
//use App\State;
//use App\EncryptCustom;
//use App\Language;
use App\Models\SaasPayment;
use App\Models\SaasPaymentMethod;
use App\Models\SaasCurrency;
use App\Models\SaasWebsitePlan;
use App\Models\SaasWebsite;
use App\Models\SaasSettings;
use App\Models\SaasUserToken;
// use Illuminate\Support\Facades\Storage;
use Storage;
// use App\PaymentMethod;
// use App\PaymentMethodTranslation;
// use App\Reservation;
// use App\ReservationAntifraudLevel;
// use App\ReservationDetailActivity;
// use App\ReservationDetailBenefitCard;
// use App\ReservationDetailClientType;
// use App\ReservationDetailDayMore;
// use App\ReservationDetailEvent;
// use App\ReservationDetailFood;
// use App\ReservationDetailHotel;
// use App\ReservationDetailHotelRoom;
// use App\ReservationDetailPackage;
// use App\ReservationDetailPark;
// use App\ReservationDetailPhoto;
// use App\ReservationDetailRestaurant;
// use App\ReservationDetailTour;
// use App\ReservationDetailTransportation;
// use App\ReservationPromotion;
// use App\Settings;
// use App\Currency;
// use App\CurrencyExchange;
// use App\ReservationDetail;
// use App\ReservationInsurance;
// use App\ReservationLog;
// use App\TestWebHookCompropago;
// use App\User;
// use App\UserToken;
// use App\ReservationPayment;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Hamcrest\Core\Set;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Exceptions\Handler;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Integer;
use Validator;
use Illuminate\Http\Request;
use SoapCLient;
use SoapVar;
use SoapHeader;
use SimpleXMLElement;
use App\Subchannel;
use Srmklive\PayPal\Services\ExpressCheckout;
use Illuminate\Support\Facades\Log;
use Ssheduardo\Redsys\Facades\Redsys;


class SaasPaymentController extends Controller
{

    private function updateWebsite($id){
        $websiteplan = SaasWebsitePlan::where('id',$id)->first();

        $status_id = 2;
        // Actualizar website plan
        $websiteplan->status_id = 2;

        $fecha_fin = Carbon::now()->endofday()->addMonth()->format('Y-m-d H:i:s');

        $websiteplan->date_end = $fecha_fin;

        $websiteplan->save();

        $website = SaasWebsite::where('id',$websiteplan->website_id)
                        ->first();

        if($websiteplan->plan_id && $status_id == 2){

            //Actualización estado website de 'Pendiente de pago' a 'Asistente' en caso de plan pagado
            if($website->status_id == 1){
                $website->update([
                    'status_id' => 2, //Estado 'Asistente'
                ]);
            }

            //Sustitución como plan vigente
            $fecha_hora_operacion = Carbon::now()->format('Y-m-d H:i:s');

            $cancelar_subscripcion = DB::table('saas_website_plan_rule')
                ->whereNull('saas_website_plan_rule.deleted_at')
                ->whereNotNull('saas_website_plan_rule.subscription_code')
                ->where('saas_website_plan_rule.cancelled', '0')
                ->where('saas_website_plan_rule.website_id', $websiteplan->website_id)
                ->whereNotIn('saas_website_plan_rule.id',[$id])
                ->whereNotNull('saas_website_plan_rule.plan_id')
                ->get();

            foreach($cancelar_subscripcion as $subscripcion){
                $cancel = $this->cancelGatewaySubscription ($subscripcion->id);
            }

            SaasWebsitePlan::where('website_id', $websiteplan->website_id)
            ->whereNotNull('plan_id')
            ->whereNotIn('id', [$id])
            ->whereNull('date_replace')
            ->update([
                'date_replace' => $fecha_hora_operacion,
            ]);

        }
    }

    private function cancelGatewaySubscription ($id) {

        $status_ok = 0;

        $subscripcion = SaasWebsitePlan::where('id', $id)->first();

        $payment_method = SaasPaymentMethod::where('id', $subscripcion->payment_method_id)->first();

        if($payment_method) {
            $payment_method_id = $payment_method->id;
            $website_plan_rule_id = $subscripcion->id;
            
            $paypal_request = null;
            $paypal_response = null;
    
            if ($payment_method->type == 'paypal') {
                $website_plan_rule_id = $subscripcion->id;
    
                $datos_entorno_paypal = SaasSettings::where('name', 'paypal_environment')->first()->value;
    
                if ($datos_entorno_paypal == 'live') {
                    config([
                        'paypal.live.username' => $payment_method->paypal_username,
                        'paypal.live.password' => $payment_method->paypal_password,
                        'paypal.live.secret' => $payment_method->paypal_secret,
                        'paypal.mode' => 'live'
                    ]);
                } else {
                    config([
                        'paypal.sandbox.username' => $payment_method->paypal_username,
                        'paypal.sandbox.password' => $payment_method->paypal_password,
                        'paypal.sandbox.secret' => $payment_method->paypal_secret,
                        'paypal.mode' => 'sandbox'
                    ]);
                }
    
                $provider = new ExpressCheckout();
    
                $response = $provider->suspendRecurringPaymentsProfile($subscripcion->subscription_code);
    
                if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                    DB::table('saas_website_plan_rule')
                    ->where('saas_website_plan_rule.id', $subscripcion->id)
                    ->update([
                        'cancelled' => 1,
                    ]);
                    $status_ok = 1;

                    $datos_compra = DB::table('saas_user')->select(
                        'saas_user.name',
                        'saas_user.surname',
                        'saas_user.email',
                        'saas_website.name as website_name',
                        'saas_website.domain',
                        'saas_website.status_id',
                        'saas_website_plan_rule.code',
                        'saas_website_plan_rule.plan_id',
                        'saas_website_plan_rule.rule_id',
                        'saas_website_plan_rule.date_end',
                        'saas_website_plan_rule.subscription_code'
                    )->whereNull('saas_user.deleted_at')
                    ->join('saas_website', 'saas_website.user_id', 'saas_user.id')
                    ->whereNull('saas_website.deleted_at')
                    ->join('saas_website_plan_rule', 'saas_website_plan_rule.website_id', 'saas_website.id')
                    ->whereNull('saas_website_plan_rule.deleted_at')
                    ->where('saas_website_plan_rule.id', $id)
                    ->first();

                    $email = paymentEmail($id);
                } else {
                    $status_ok = 0;
                }
    
                $paypal_request = $subscripcion->subscription_code;
    
                $paypal_response = json_encode($response);
          
            } elseif($payment_method->type == 'tarjeta'){
                $status_ok = 1;
            }
    
            DB::table('saas_payment')->insert([
                'code' => 'cancel',
                'payment_method_id' => $payment_method_id,
                'website_plan_rule_id' => $website_plan_rule_id,
                'status_ok' => $status_ok,
                'request_paypal' => $paypal_request,
                'response_paypal' => $paypal_response,
            ]);
        }

        return $status_ok;
    }

    /**
     * Función para realizar un pago
     *
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payment(Request $request) {

        $array['error'] = 200;
        
        try {
            DB::beginTransaction();

            //Validacion
            $error = 0;

            $mensaje_validador = collect();
            $validator = \Validator::make($request->all(), [
                'currency_id' => 'required|exists:saas_currency,id,deleted_at,NULL',
                'plan_rule_id' => 'required|exists:saas_website_plan_rule,id,deleted_at,NULL',
                'name' => 'required',
                'payment_method_id' => 'required|exists:saas_payment_method,id,deleted_at,NULL',
                'return_url' => 'required',
                'cancel_url' => 'required'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $metodo_pago = SaasPaymentMethod::where('id', $request->get('payment_method_id'))->first();


                // Según la forma de pago necesitará más o menos datos y distintas validaciones
                $request_paypal = null;
                $response_paypal = null;
                $request_suscription_paypal = null;
                $response_suscription_paypal = null;
                $status_ok = 0;

                // Obtener URL de redirección para realizar el pago en funcion del metodo de pago
                
                if ($metodo_pago->type == 'tarjeta') {
                    // ...
                    
                    $key = $metodo_pago->redsys_merchant_secret;
                    $code = $metodo_pago->redsys_merchant_code;
                    $terminal = $metodo_pago->redsys_merchant_terminal;
                    $enviroment = $metodo_pago->redsys_enviroment;
                    $name = SaasSettings::where('name', 'nombre_cliente')->first()->value;

                    $prefix_invoice = SaasSettings::where('name', 'prefix_invoice')->first();

                    $protocol = SaasSettings::where('name', 'protocol')->first()->value;
                    $base_path = SaasSettings::where('name','base_path')->first()->value;
                    
                    
                    $currency_iso_number = SaasCurrency::where('id', $request->get('currency_id'))->first()->iso_number;

                    $datoswebsite = SaasWebsitePlan::where('id', $request->get('plan_rule_id'))->first();

                    $pago = SaasPayment::create([
                        'payment_method_id' => $metodo_pago->id,
                        'website_plan_rule_id' => $request->get('plan_rule_id'),
                        'code' => 'redsys',
                        'currency_id' => $request->get('currency_id'),
                        'amount' => $datoswebsite->sale_price,
                        'status_ok' => 0,
                    ]);

                    config([
                        'redsys.key' => $key,
                        'redsys.merchantcode' => $code,
                        'redsys.terminal' => $terminal,
                        'redsys.enviroment' => $enviroment,
                    ]);

                    $invoice_id = str_pad($pago->id, 12, "0", STR_PAD_LEFT);

                    Redsys::setAmount(round($datoswebsite->sale_price, 2));
                    Redsys::setOrder($invoice_id);
                    Redsys::setMerchantcode($code); //Reemplazar por el código que proporciona el banco
                    Redsys::setCurrency($currency_iso_number);
                    Redsys::setTransactiontype('0');
                    Redsys::setTerminal($terminal);
                    Redsys::setMethod('C'); //Solo pago con tarjeta, no mostramos iupay
                    Redsys::setNotification($protocol.$base_path.'/api/saas/payment/redsys/notify'); //Url de notificacion
                    Redsys::setUrlOk($request->get('return_url')); //Url OK
                    Redsys::setUrlKo($request->get('cancel_url')); //Url KO
                    Redsys::setVersion('HMAC_SHA256_V1');
                    Redsys::setTradeName($name);
                    Redsys::setProductDescription($request->get('name'));
                    Redsys::setEnviroment($enviroment); //Entorno test
                    // Redsys::setPan('4548812049400004'); //Número de la tarjeta
                    // Redsys::setExpiryDate('2012'); //AAMM (año y mes)
                    // Redsys::setCVV2('123');
                    Redsys::setIdentifier();

                    $signature = Redsys::generateMerchantSignature($key);
                    Redsys::setMerchantSignature($signature);

                    $form = Redsys::createForm();

                    $form .= "<script>document.getElementById('redsys_form').submit();</script>";

                    $array['data'][0]['redsys_form'] = $form;

                } elseif ($metodo_pago->type == 'cortesia') {
                    // ...
                } elseif ($metodo_pago->type == 'paypal') {
                    
                    $datoswebsite = SaasWebsitePlan::where('id', $request->get('plan_rule_id'))->first();

                    $prefix_invoice = SaasSettings::where('name', 'prefix_invoice')->first();

                    $pago = SaasPayment::create([
                        'payment_method_id' => $metodo_pago->id,
                        'website_plan_rule_id' => $request->get('plan_rule_id'),
                        'code' => 'pay',
                        'currency_id' => $request->get('currency_id'),
                        'amount' => $datoswebsite->sale_price,
                        'status_ok' => $status_ok,
                    ]);
                    
                    $invoice_id = $prefix_invoice->value.$pago->id;

                    $currency = SaasCurrency::where('id', $request->get('currency_id'))->first()->iso_code;

                    $datos_entorno_paypal = SaasSettings::where('name','paypal_environment')->first()->value;

                    $cart = [];
                    $cart['items'] = [
                        [
                            'name'  => $request->get('name'),
                            'price' => round($datoswebsite->sale_price, 2),
                            'qty'   => 1,
                            'desc' => $request->get('name')
                        ]
                    ];
                    $cart['invoice_id'] = $invoice_id;
                    $cart['invoice_description'] = $request->get('name');
                    $cart['return_url'] = $request->get('return_url');
                    $cart['cancel_url'] = $request->get('cancel_url');

                    $cart['total'] = round($datoswebsite->sale_price, 2);
                   
                    if($datos_entorno_paypal == 'live') {
                        config([
                            'paypal.live.username' => $metodo_pago->paypal_username,
                            'paypal.live.password' => $metodo_pago->paypal_password,
                            'paypal.live.secret' => $metodo_pago->paypal_secret,
                            'paypal.currency' => $currency,
                            'paypal.mode' => 'live'
                        ]);
                    } else {
                        config([
                            'paypal.sandbox.username' => $metodo_pago->paypal_username,
                            'paypal.sandbox.password' => $metodo_pago->paypal_password,
                            'paypal.sandbox.secret' => $metodo_pago->paypal_secret,
                            'paypal.currency' => $currency,
                            'paypal.mode' => 'sandbox'
                        ]);
                    }

                    $options = [
                        'BRANDNAME' => 'Viavox Experience',
                        'LOGOIMG' => 'https://www.viavoxexperience.com/images/logo_viavox_experience.png',
                        'CHANNELTYPE' => 'Merchant'
                    ];

                    $provider = new ExpressCheckout();

                    $request_paypal = $cart;
                    $response_paypal = $provider->setExpressCheckout($cart, true);
                    // if there is no link redirect back with error message
                    if (!$response_paypal['paypal_link']) {
                        $array['error'] = 400;
                        $array['error_description'] = 'Something went wrong with PayPal';
                        $array['error_inputs'][0] = ['paypal' => ['Paypal connection error']];
                    } else {
                        $array['data'][0]['paypal_link'] = $response_paypal['paypal_link'];
                    }

                    $pago->update([
                        'request_paypal' => json_encode($request_paypal),
                        'response_paypal' => json_encode($response_paypal),
                    ]);
                }

            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasPayment');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para validar una transacción
     *
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paypalSubscription(Request $request) {

        $array['error'] = 200;

        try {

            DB::beginTransaction();

            //Validacion
            $error = 0;

            $mensaje_validador = collect();
            $validator = \Validator::make($request->all(), [
                'token' => 'required'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $metodo_pago = SaasPaymentMethod::where('id', 1)->first();

                $datos_entorno_paypal = SaasSettings::where('name','paypal_environment')->first()->value;

                $currency = SaasCurrency::where('id', 3)->first()->iso_code;

                if($datos_entorno_paypal == 'live') {
                    config([
                        'paypal.live.username' => $metodo_pago->paypal_username,
                        'paypal.live.password' => $metodo_pago->paypal_password,
                        'paypal.live.secret' => $metodo_pago->paypal_secret,
                        'paypal.currency' => $currency,
                        'paypal.mode' => 'live'
                    ]);
                } else {
                    config([
                        'paypal.sandbox.username' => $metodo_pago->paypal_username,
                        'paypal.sandbox.password' => $metodo_pago->paypal_password,
                        'paypal.sandbox.secret' => $metodo_pago->paypal_secret,
                        'paypal.currency' => $currency,
                        'paypal.mode' => 'sandbox'
                    ]);
                }
                
                $provider = new ExpressCheckout();

                $response = $provider->getExpressCheckoutDetails($request->get('token'));

                if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                    $array['error'] = 400;
                    $array['error_description'] = 'Error processing PayPal payment';
                    $array['error_inputs'][0] = ['paypal' => ['Paypal connection error']];
                } else {
                    $invoice_id = explode('-', $response['INVNUM'])[1];

                    $request_suscription_paypal = [
                        'token' => $response['TOKEN'],
                        'amt' => $response['AMT'],
                        'desc' => $response['DESC']
                    ];

                    $provider = new ExpressCheckout();
                    $response_suscription_paypal = $provider->createMonthlySubscription($response['TOKEN'], $response['AMT'], $response['DESC']);

                    if (!empty($response_suscription_paypal['PROFILESTATUS']) && in_array($response_suscription_paypal['PROFILESTATUS'], ['ActiveProfile', 'PendingProfile'])) {
                        $status_ok = 1;
                    } else {
                        $status_ok = 0;
                    }

                    if($status_ok == '1') {
                        $payment = SaasPayment::where('id', '=', $invoice_id)->first();

                        $payment->status_ok = 1;

                        $payment->request_suscription_paypal = json_encode($request_suscription_paypal);
                        $payment->response_suscription_paypal = json_encode($response_suscription_paypal);

                        $payment->save();
                        $update = $this->updateWebsite($payment->website_plan_rule_id);

                        $websiteplan = SaasWebsitePlan::where('id', $payment->website_plan_rule_id)->first();

                        if($websiteplan) {
                            $websiteplan->subscription_code = $response_suscription_paypal['PROFILEID'];

                            $websiteplan->payment_method_id = $metodo_pago->id;

                            $websiteplan->save();

                            $array_website ['id'] = $websiteplan->id;
                            $array_website ['status_id'] = $websiteplan->status_id;

                            $array['data']['0']['website'] = $array_website;
                        }

                        $email = paymentEmail($websiteplan->id, 'new', $payment->amount);
                    }

                    $array['data'][0]['status_ok'] = $status_ok; 
                }
            }

            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasPayment');
        }
        return response()->json($array, $array['error']);
    }

    public function cancelSubscription (Request $request) {
        $array['error'] = 200;

        try {

            DB::beginTransaction();

            //Validacion
            $error = 0;

            $mensaje_validador = collect();
            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0|exists:saas_website,id,deleted_at,NULL',
                'plan_rule_id' => 'required|integer|min:0|exists:saas_website_plan_rule,id,deleted_at,NULL'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Fin de validación

            $user_id = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $datos_website = SaasWebsite::where('id',$request->get('id'))
                    ->first();

                $error_aux = 0;

                if($datos_website) {
                    if ($user_id != '') {
                        if ($datos_website->user_id != $user_id) {
                            $error_aux = 1;
                        }
                    }
                } else {
                    $error_aux = 1;
                }

                if ($error_aux == 1) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The website plan can not be canceled']]);
                }

                if ($error == 1) {
                    $array['error'] = 400;
                    $array['error_description'] = 'The fields are not the required format';
                    $array['error_inputs'][] = $mensaje_validador;
                } else {

                    $cancel = $this->cancelGatewaySubscription($request->get('plan_rule_id'));
                }
            }

            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasPayment');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para recivir la petición de paypal al producirse una acción de pago
     * 
     */
    public function paypalIpn(Request $request) {
        
        $error = 200;

        $payment_method = SaasPaymentMethod::where('id', 1)->first();

        $datos_entorno_paypal = SaasSettings::where('name','paypal_environment')->first()->value;

        if($datos_entorno_paypal == 'live') {
            config([
                'paypal.live.username' => $payment_method->paypal_username,
                'paypal.live.password' => $payment_method->paypal_password,
                'paypal.live.secret' => $payment_method->paypal_secret,
                'paypal.mode' => 'live'
            ]);
        } else {
            config([
                'paypal.sandbox.username' => $payment_method->paypal_username,
                'paypal.sandbox.password' => $payment_method->paypal_password,
                'paypal.sandbox.secret' => $payment_method->paypal_secret,
                'paypal.mode' => 'sandbox'
            ]);
        }

        $response = '';
        if($request->get('txn_type') != '') {
            $provider = new ExpressCheckout;

            $post = $request->all();  
            $post['cmd']  = '_notify-validate';

            $response = (string) $provider->verifyIPN($post);

            
            if ($response === 'VERIFIED') {   
                switch($request->get('txn_type')) {
                    case 'recurring_payment':
                        $website_plan_rule_id = SaasWebsitePlan::where('subscription_code', $request->get('recurring_payment_id'))
                            ->first()->id;

                        $currency = SaasCurrency::where('iso_code', $request->get('currency_code'))->first();

                        $update = $this->updateWebsite($website_plan_rule_id);

                        $pago = SaasPayment::create([
                            'payment_method_id' => 1,
                            'website_plan_rule_id' => $website_plan_rule_id,
                            'code' => 'ipn',
                            'currency_id' => $currency->id,
                            'amount' => $request->get('amount'),
                            'status_ok' => 1,
                            'request_paypal' => addslashes(serialize($post)),
                            'response_paypal' => addslashes(serialize($request->all())),
                        ]);
                        $email = paymentEmail($website_plan_rule_id, 'payment', $request->get('amount'));

                        break;
                    case 'recurring_payment_suspended':
                    case 'recurring_payment_profile_cancel':
                        $website_plan_rule_id = SaasWebsitePlan::where('subscription_code', $request->get('recurring_payment_id'))
                            ->first()->id;

                        DB::table('saas_website_plan_rule')
                            ->where('subscription_code', $request->get('recurring_payment_id'))
                            ->update([
                                'cancelled' => 1,
                            ]);
                        
                        DB::table('saas_payment')->insert([
                            'code' => 'ipn-cancel',
                            'payment_method_id' => 1,
                            'website_plan_rule_id' => $website_plan_rule_id,
                            'status_ok' => 1,
                            'request_paypal' => addslashes(serialize($post)),
                            'response_paypal' => addslashes(serialize($request->all())),
                        ]);

                        $email = paymentEmail($website_plan_rule_id);
                        break;
                    case 'recurring_payment_expired':
    
                        break;
                    case 'recurring_payment_failed':
    
                        break;
                    case 'recurring_payment_profile_created':
    
                        break;
                    case 'recurring_payment_skipped':
    
                        break;
                    case 'recurring_payment_suspended_due_to_max_failed_payment':
    
                        break;
    
                    default:
                            
                }
            }
        }else {
            $error = 500;
        }

        $tabla_ipn = DB::table('saas_paypal_ipn')->insert([
            'paypal_request' => addslashes(serialize($request->all())),
            'paypal_response' => $response
        ]);
    }

    
    public function redsysSubscription(Request $request) {
        $array['error'] = 200;

        try {

            DB::beginTransaction();

            $metodo_pago = SaasPaymentMethod::where('id', 2)->first();
            $key = $metodo_pago->redsys_merchant_secret;

            $parameters = Redsys::getMerchantParameters($request->input('Ds_MerchantParameters'));
            $DsResponse = $parameters["Ds_Response"];
            $DsResponse += 0;

            $status_ok = 0;

            $payment = SaasPayment::where('id',intval($parameters['Ds_Order']))->first();
            
            if (Redsys::check($key, $request->input()) && $DsResponse <= 99) {

                $status_ok = 1;
                // lo que quieras que haya si es positiva la confirmación de redsys

                if($payment) {

                    $payment->request_redsys = addslashes(serialize($parameters));
                    $payment->status_ok = 1;
                    
                    $payment->save();

                    $metodo_pago = SaasPaymentMethod::where('id', $payment->payment_method_id)->first();

                    $update = $this->updateWebsite($payment->website_plan_rule_id);

                    $websiteplan = SaasWebsitePlan::where('id', $payment->website_plan_rule_id)->first();

                    if($websiteplan) {

                        $websiteplan->payment_method_id = $metodo_pago->id;
                        $websiteplan->subscription_code = $parameters['Ds_Merchant_Identifier'];

                        $websiteplan->save();

                        $array_website ['id'] = $websiteplan->id;
                        $array_website ['status_id'] = $websiteplan->status_id;

                        $array['data']['0']['website'] = $array_website;
                    }

                    $email = paymentEmail($websiteplan->id, 'new', $payment->amount);
                }   
            } else {
                if($payment) {
                    $payment->request_redsys = addslashes(serialize($parameters));
                    $payment->save();
                }
            }

            $array['data'][0]['status_ok'] = $status_ok; 

            $tabla_ipn = DB::table('saas_redsys_notify')->insert([
                'redsys_request' => addslashes(serialize($request->all()))
            ]);

            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasPayment');
        }

        return response()->json($array, $array['error']);
    }

}