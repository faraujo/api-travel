<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\SaasPasswordHistory;
use App\Models\SaasPasswordToken;
use App\Models\SaasSettings;
use App\Models\SaasUser;
use App\Models\SaasUserRememberToken;
use App\Models\SaasUserToken;
use App\Models\SaasUserValidateEmailToken;
use App\Models\SaasWebsite;
use Hyn\Tenancy\Environment;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\Handler;
use App\Models\tenant\Settings;
use App\Models\tenant\User;
use App\Models\tenant\UserToken;

/* use App\Models\tenant\SaasUserRole;
use App\Models\tenant\SaasUserIdentification; */

class SaasUserController extends Controller
{
    /**
     * Función para validación de token a través del middleware de Autorization
     *
     * @return \Illuminate\Http\Response
     */
    public function valid_token()
    {
        return response()->json(['error' => 200]);
    }

    /**
     * Función para la creación/modificación de un usuario.
     *
     * Para la creación de un usuario se realiza una petición POST.
     * Para la modificación de un usuario se realiza una petición PUT.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::beginTransaction();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                'c_password' => 'required_with:password|same:password'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //controla que no introduzcan espacios en blanco en el password
            if ($request->get('password')) {
                if (preg_match('/\s/', $request->get('password')) || preg_match('/\s/', $request->get('c_password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('c_password'))) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
                }
            }

            //Validación específica para creación de usuario

            if ($request->isMethod('post')) {
                $validator = \Validator::make($request->all(), [
                    'email' => 'unique:saas_user,email,NULL,id,deleted_at,NULL|unique:saas_user',
                    'password' => 'required',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                //Validación específica para actualización de usuario
            } else {
                $validator = \Validator::make($request->all(), [
                    'id' => 'integer|min:0|required',
                    'password' => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                    'c_password' => 'required_with:password|same:password'
                ]);
                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                } else {
                    $usuario = SaasUser::find($request->get('id'));
                    if (!$usuario) {
                        $error = 2;
                    } else {
                        if ($error == 0) {
                            $usada = false;

                            $guardadas = SaasPasswordHistory::where('user_id', $request->get('id'))->orderBy('id', 'desc')->limit(4)->get();

                            foreach ($guardadas as $guardada) {
                                if (Hash::check($request->get('password'), $guardada->password)) {
                                    $usada = true;
                                }
                            }

                            if ($usada) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['password' => ['The password entered must be different from the last four.']]);
                            }
                        }
                    }
                }
            }
            // FIN Validación

            if ($error == 0) {

                //Generación array de datos de usuario
                $user = [
                    'email' => $request->get('email'),
                ];


                if ($request->get('password')) {
                    $user['password'] = Hash::make($request->get('password'));
                }

                //Creación o actualización del registro de usuario en BBDD
                if ($request->isMethod('put')) {
                    $usuario->update($user);
                } else {
                    $usuario = SaasUser::create($user);
                }

                //Almacenamiento de contraseña en el histórico
                if ($request->get('password')) {
                    SaasPasswordHistory::create([
                        'user_id' => $usuario->id,
                        'password' => $usuario->password,
                    ]);
                }
            } elseif ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } elseif ($error == 2) {
                $array['error'] = 404;
                $array['error_description'] = 'Data not found';
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'Saas_Usuarios');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la consulta de un perfil de usuario
     *
     * Para la consulta de un perfil de usuario se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProfile(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();
            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {
                    $token = SaasUserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {

                        $datos_usuario = DB::table('saas_user')->select(
                            'saas_user.id',
                            'saas_user.name',
                            'saas_user.surname',
                            'saas_user.document_type_id',
                            'saas_user.number_document',
                            'saas_user.sex_id',
                            'saas_user.birthdate',
                            'saas_user.email',
                            'saas_user.email2',
                            'saas_user.faults_login',
                            'saas_user.bloqued_login',
                            'saas_user.bloqued_to',
                            'saas_user.language_id',
                            'saas_user.telephone1',
                            'saas_user.telephone2',
                            'saas_user.telephone3',
                            'saas_user.address',
                            'saas_user.postal_code',
                            'saas_user.city',
                            'saas_user.state_id',
                            'saas_user.country_id',
                            'saas_user.business',
                            'saas_user.business_name',
                            'saas_user.business_number_document',
                            'saas_user.business_address',
                            'saas_user.telephone_business',
                            'saas_user.worker',
                            'saas_user.worker_type_id',
                            'saas_user.observations',
                            'saas_user.image_id',
                            'verified_email'
                        )
                        ->where('saas_user.deleted_at', '=', null)
                        ->where('saas_user.id', '=', $token->user_id)
                        ->first();

                        $array['data'] = array();
                        if ($datos_usuario) {
                            $array['data'][0]['user'][] = $datos_usuario;
                        }
                        
                    } else {
                        $array['error'] = 404;
                        $array['error_description'] = 'Data not found';
                    }
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = ['token' => ['Authorization header is required.']];
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    public function updateProfile(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            if (!$request->header('Authorization')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['token' => ['Authorization header is required.']]);
            }

            $token_api = explode(' ', $request->header('Authorization'));
            $user_id = SaasUserToken::where('token', '=', $token_api[1])->first()->user_id;

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',                
                'password' => 'min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                'c_password' => 'required_with:password|same:password',
                'actual_password' => 'required_with:password',
                'business_number_document' => 'unique:saas_user,business_number_document,'. $user_id.',id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Comprueba que se ha enviado correctamente el password del usuario
           
            $user = SaasUser::where('id', $user_id)->where('email', $request->get('email'))->first();
            if (!$user) {
                $error = 2;
            }else{
                
                // Validación de password actual
                $password_match = false;
                if ($request->get('actual_password') != '') {
                    if (Hash::check($request->get('actual_password'), $user->password)) {
                        $password_match = true;
                    }

                    if (!$password_match) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['actual_password' => ['The actual password is not correct.']]);
                    }
                    //Validación de nuevo password
                    else {
                        if ($request->get('password') != '') {
                            $token_api = explode(' ', $request->header('Authorization'));
                            $user_id = SaasUserToken::where('token', '=', $token_api[1])->first()->user_id;
            
                            $usada = false;
            
                            $guardadas = SaasPasswordHistory::where('user_id', $user_id)->orderBy('id', 'desc')->limit(4)->get();
            
                            foreach ($guardadas as $guardada) {
                                if (Hash::check($request->get('password'), $guardada->password)) {
                                    $usada = true;
                                }
                            }
            
                            if ($usada) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['password' => ['The password entered must be different from the last four.']]);
                            }
                        }
                    }
                }                  
            }
            // FIN Validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } elseif ($error == 2) {
                $array['error'] = 404;
                $array['error_description'] = 'Data not found';

            } else {
                $token_api = explode(' ', $request->header('Authorization'));
                $token = SaasUserToken::where('token', '=', $token_api[1])->first();
                if ($token) {
                    $user_id = SaasUserToken::where('token', '=', $token_api[1])->first()->user_id;
                    if (!$user_id) {
                        $array['error'] = 404;
                        $array['error_description'] = 'SaasUser not found';
                    } else {

                        //Asignacion de campos
                        $user = [];

                        if ($request->get('name') != '') {
                            $user['name'] = $request->get('name');
                        }
                        if ($request->get('surname') != '') {
                            $user['surname'] = $request->get('surname');
                        }
                        if ($request->get('business_name') != '') {
                            $user['business_name'] = $request->get('business_name');
                        }
                        if ($request->get('business_number_document') != '') {
                            $user['business_number_document'] = $request->get('business_number_document');
                        }
                        if ($request->get('business_address') != '') {
                            $user['business_address'] = $request->get('business_address');
                        }
                        if ($request->get('password') != '') {
                            $user['password'] = Hash::make($request->get('password'));

                            SaasPasswordHistory::create([
                                'user_id' => $user_id,
                                'password' => Hash::make($request->get('password')),
                            ]);
                        }

                        $usuario = SaasUser::where('id', '=', $user_id)->first();
                        $usuario->update($user);

                    }
                } else {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Login
     *
     * Para la realización de login se realizara una peticion POST. Son requeridos los parámetros: email y password. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” además del token generado en la variable “data”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
                'remember' => 'boolean',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $user = SaasUser::where('email', '=', $request->get('email'))->first();

                $remember = 0;
                if ($request->get('remember') != '') {
                    $remember = $request->get('remember');
                }

                if (!$user) {
                    $array = ['error' => 401, 'error_description' => 'Unauthorized'];
                } elseif ($user->bloqued_login == 1) {
                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued']];
                } elseif ($user->bloqued_to >= Carbon::now()) {
                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued temporarily']];
                } elseif (Auth::guard('saas')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
                    $error_permisos = 0;

                    $user = Auth::guard('saas')->user();

                    $array['data'] = array();
                    $token = $user->createToken('MotorOnline')->accessToken;

                    $remember_token = null;

                    $time_expired = SaasSettings::where('name', 'token_validate')->first()->value;
                    $expired_at = Carbon::now()->addMinutes($time_expired);

                    SaasUserToken::create([
                        'user_id' => $user->id,
                        'token' => $token,
                        'expired_at' => $expired_at,
                    ]);

                    if ($remember == 1) {
                        $remember_token = $user->createToken('MotorOnline')->accessToken;

                        SaasUserRememberToken::create([
                            'user_id' => $user->id,
                            'token' => $remember_token,
                            'ip' => $request->ip(),
                        ]);
                    }


                    $user->faults_login = 0;
                    $user->save();
                    $array['data'][] = ['user_id' => $user->id, 'token' => 'Bearer ' . $token, 'remember_token' => $remember_token];
                } else {
                    $array = ['error' => 401, 'error_description' => 'Unauthorized'];

                    //Actualización de parámetros de fallo de acceso y bloqueo del usuario

                    $errores_maximos = SaasSettings::where('name', '=', 'maximo_accesos_failed')->first()->value;
                    $user->faults_login = $user->faults_login < $errores_maximos ? $user->faults_login + 1 : $user->faults_login;

                    if ($user->faults_login >= $errores_maximos) {
                        $tiempo_bloqueo = SaasSettings::where('name', 'time_bloqued_password')->first()->value;
                        $user->bloqued_to = Carbon::now()->addMinutes($tiempo_bloqueo);
                        $user->faults_login = 0;
                    }
                    $user->save();
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }
        

        return response()->json($array, $array['error']);
    }


    /**
     * Registro de usuario
     *
     * Para el registro de un usuario se realizara una peticion POST. Son requeridos los parámetros: Email, password, c_password además de number_document en caso de existir document_type_id. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” además del token generado en la variable “data”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'name' => 'required',
                'surname' => 'required',
                'email' => 'required|email|unique:saas_user,email,NULL,id,deleted_at,NULL|unique:saas_user,email2,NULL,id,deleted_at,NULL',
                'password' => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //controla que no introduzcan espacios en blanco en el password ni caracteres acentuados
            if (preg_match('/\s/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password'))) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $user = SaasUser::create([
                    'name' => $request->get('name'),
                    'surname' => $request->get('surname'),
                    'email' => $request->get('email'),
                    'password' => Hash::make($request->get('password')),
                ]);

                $array['data'] = array();
                $token = $user->createToken('MotorOnline')->accessToken;
                $time_expired = SaasSettings::where('name', 'token_validate')->first()->value;
                $expired_at = Carbon::now()->addMinutes($time_expired);
                SaasUserToken::create([
                    'user_id' => $user->id,
                    'token' => $token,
                    'expired_at' => $expired_at,
                ]);

                $remember_token = $user->createToken('MotorOnline')->accessToken;

                SaasUserRememberToken::create([
                    'user_id' => $user->id,
                    'token' => $remember_token,
                    'ip' => $request->ip(),
                ]);
                // Llamada a helper de envío de email de verificación
               $sendValidateEmail = sendValidateEmail($user->id);

                $array['data'][] = ['token' => 'Bearer ' . $token, 'remember_token' => $remember_token, 'id' => $user->id];

                SaasPasswordHistory::create([
                    'user_id' => $user->id,
                    'password' => $user->password,
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función solicitar password
     *
     * Para solicitar un nuevo password se realizará una peticion POST. Es requerido el parámetro 'email' y 'environment'. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();
            $validator = \Validator::make($request->all(), [
                'email' => 'required|email|exists:saas_user,email,deleted_at,NULL',
                'environment' => 'required|in:front,back,hotel',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //fin validación


            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $user = SaasUser::where('email', '=', $request->get('email'))->first();

                if ($user->bloqued_login || $user->bloqued_to >= Carbon::now()) {
                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued']];
                } else {
                    //comprueba si pidió un cambio de contraseña y ya se le envió un email para restablecerla
                    $token_user = SaasPasswordToken::where('user_id', '=', $user->id)->where('expired_at', '>=', Carbon::now())->first();

                    if ($token_user) {
                        $array['error'] = 409;
                        $array['error_description'] = 'Conflict';
                        $array['error_inputs'][0] = ['email' => [0 => 'You have already been sent an email to reset your password']];
                    } else {

                        //borra token que pudiera haber sin borrar porque no se haya llevado a cabo el cambio de contraseña y quedaran expirados pero no borrados
                        $token_no_borrados = SaasPasswordToken::where('user_id', '=', $user->id)->get();
                        if (count($token_no_borrados) > 0) {
                            foreach ($token_no_borrados as $token) {
                                $token->delete();
                            }
                        }
                        //crea token y envía email para restablecer contraseña
                        $token_forgot = $user->createToken('MotorOnline')->accessToken;

                        $time_expired = SaasSettings::where('name', 'recover_token_validate')->first()->value;
                        $expired_at = Carbon::now()->addMinutes($time_expired);

                        //trae link para cambio de password en función del entorno desde el que se solicita
                        // $ruta_api = SaasSettings::where('name', '=', 'api_gateway_route')->first()->value;

                        $ruta_recover_entorno = SaasSettings::where('name', '=', 'recover_route_' . $request->get('environment'))->first()->value;


                        $ruta_recover = $ruta_recover_entorno;
                        /*$params = ['title' => 'Recuperar password',
                            'body' => $ruta_recover . '?token=' . $token_forgot,
                            'template' => 'recover',
                            'emails' => [
                                '0' => [
                                    'email' => $request->get('email'),
                                    'subject_email' => 'Restore password',
                                ]
                            ]];

                        $ruta_final = $ruta_api . '/v1/email_without_token';
                        $client = new Client();
                        $client->request('POST', $ruta_final, ['form_params' => $params, 'http_errors' => false]);*/

                        $emails = [
                            '0' => [
                                'email' => $request->get('email'),
                                'subject_email' => 'Restore password',
                            ]
                        ];
                        
                        $controller = app(\App\Http\Controllers\SaasEmailController::class);

                        $peticion = new Request();

                        $peticion->setMethod('POST');

                        $peticion->request->add(['title' => 'Recuperar password']);

                        $peticion->request->add(['body' => $ruta_recover . '?token=' . $token_forgot]);

                        $peticion->request->add(['template' => 'recover']);

                        $peticion->request->add(['emails' => $emails]);
                        
                        $controller->send($peticion);

                        SaasPasswordToken::create([
                            'user_id' => $user->id,
                            'token' => $token_forgot,
                            'expired_at' => $expired_at,
                        ]);
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }


        return response()->json($array, $array['error']);
    }


    /**
     * Función para cambiar password
     *
     * Para cambiar el password se realizará una peticion POST. Es requerido el nuevo password, la repetición del mismo y el token que se le ha generado al pedir el nuevo password. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recover(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::beginTransaction();

            $passToken = SaasPasswordToken::where('token', '=', $request->get('token'))->where('expired_at', '>=', Carbon::now())->first();

            if ($passToken) {
                //Validacion
                $error = 0;
                $mensaje_validador = collect();

                $validator = \Validator::make($request->all(), [
                    'password' => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                    'c_password' => 'required|same:password',
                    'token' => 'required',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                //controla que no introduzcan espacios en blanco en el password ni caracteres acentuados
                if (preg_match('/\s/', $request->get('password')) || preg_match('/\s/', $request->get('c_password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('c_password'))) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
                }
                if ($error == 1) {
                    $array['error'] = 400;
                    $array['error_description'] = 'The fields are not the required format';
                    $array['error_inputs'][] = $mensaje_validador;
                } else {
                    $usada = false;

                    $guardadas = SaasPasswordHistory::where('user_id', $passToken->user_id)->orderBy('id', 'desc')->limit(4)->get();

                    foreach ($guardadas as $guardada) {
                        if (Hash::check($request->get('password'), $guardada->password)) {
                            $usada = true;
                        }
                    }

                    if (!$usada) {
                        $user = SaasUser::find($passToken->user_id);

                        $user->update([
                            'password' => Hash::make($request->get('password')),
                        ]);

                        SaasPasswordHistory::create([
                            'user_id' => $user->id,
                            'password' => $user->password,
                        ]);

                        $passToken->delete();
                        $userToken = SaasUserToken::where('user_id', '=', $passToken->user_id)->get();
                        if ($userToken) {
                            foreach ($userToken as $userT) {
                                $userT->delete();
                            }
                        }
                    } else {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = ['password' => ['The password entered must be different from the last four.']];
                    }
                }
            } else {
                $array['error'] = 401;
                $array['error_description'] = 'Unauthorized';
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para verificar el email tras el registro
     *
     * Para verificar el email se realizará una petición post. Es requerido un token que se crea en el momento del registro del usuario. Si la operación no produce errores se devuelve, en la variable "error" el valor "200"
     *
     * @param Request $request
     * @return \Iluminate\Http\JsonResponse
     */
    public function verifyEmail(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::beginTransaction();

            

           
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                    'token' => 'required',
                ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

               
            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $emailToken = SaasUserValidateEmailToken::where('token', '=', $request->get('token'))->whereNull('deleted_at')->first();
                if (!$emailToken) {
                    $array['error'] = 401;
                    $array['error_description'] = 'Unauthorized';
                } else {
                    //Lógica de actualización de usuario
                    $user_id = $emailToken['user_id'];
                                
                    SaasUser::where('id', '=', $user_id)->update(
                                    [
                                        'verified_email' => 1
                                    ]
                                );
                }
            }
             

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para validar el email del usuario desde el portal
     *
     * Para validar el email se realizará una peticion POST. Es requerido el token para comprobar la identidad del usuario. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendVerifyEmail(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();
            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {
                    $token = SaasUserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {
                        sendValidateEmail($token->user_id);
                    } else {
                        $array['error'] = 404;
                        $array['error_description'] = 'Data not found';
                    }

                   
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = ['token' => ['Authorization header is required.']];
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para hacer logout
     *
     * Para hacer logout se realizará una peticion POST. Es requerido el token para eliminarlo de base de datos. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();
            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {
                    $token = SaasUserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {
                        $token->delete();
                    } else {
                        $array['error'] = 404;
                        $array['error_description'] = 'Data not found';
                    }

                    // Se borra el remember token de este modo para eliminar únicamente el recuérdame del equipo en el que se está operando
                    if ($request->get('remember_token') != '') {
                        SaasUserRememberToken::where('token', '=', $request->get('remember_token'))->delete();
                    }
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = ['token' => ['Authorization header is required.']];
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función para hacer refresh del token
     *
     * Para hacer refresh se realizará una peticion POST. Es requerido el remember token para generar un nuevo token para el usuario.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'remember_token' => 'required'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $remember_token = $request->get('remember_token');

                $user = SaasUser::whereHas('saasUserRemeberToken', function ($query) use ($remember_token) {
                    $query->where('token', $remember_token);
                })->where('bloqued_login', '!=', 1)->where(function ($query) {
                    $query->whereNull('bloqued_to')->orWhere('bloqued_to', '<', Carbon::now());
                })->first();

                if ($user) {
                    $token = $user->createToken('MotorOnline')->accessToken;
                    $remember_token = $user->createToken('MotorOnline')->accessToken;

                    SaasUserRememberToken::where('token', '=', $request->get('remember_token'))->update([
                        'token' => $remember_token,
                        'ip' => $request->ip(),
                    ]);

                    SaasUserToken::create([
                        'user_id' => $user->id,
                        'token' => $token,
                        'expired_at' => Carbon::now()->addMinutes(SaasSettings::where('name', 'token_validate')->first()->value),
                    ]);
                    $array['data'][] = ['token' => 'Bearer ' . $token, 'remember_token' => $remember_token];
                } else {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    public function loginsocial(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'social_id' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'method' => 'required'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $method = $request->get('method');
            if ($method != 'facebook' && $method != 'google') {
                $error = 1;
                $mensaje_validador = 'Method not implemented';
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $user = SaasUser::where('email', '=', $request->get('email'))->first();

                //If no exist, create new user
                if (!$user) {

                    //Generate Password

                    //$password = null;
                    
                    $length_pass = 16;
                    $password = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!$#%', ceil($length_pass / strlen($x)))), 1, $length_pass);
                    


                    //Search Country
                    $country_id = null;
                    if ($request->get('country_code') != null && $request->get('country_code') != '') {
                        $country = Country::where('iso3', $request->get('country_code'))->first();
                        if (isset($country->id)) {
                            $country_id = $country->id;
                        }
                    }

                    //Default language
                    $language_id = null;
                    if ($request->get('lang_code') != null && $request->get('lang_code') != '') {
                        $lang = DB::table('saas_language')->where('abbreviation', $request->get('lang_code'))->whereNull('deleted_at')->first();
                        if (isset($lang->id)) {
                            $language_id = $lang->id;
                        }
                    }

                    if (is_null($country_id)) {
                        $country_id = Country::orderBy('order', 'desc')->first()->id;
                    }


                    $user = SaasUser::create([
                        'name' => ($request->get('first_name') != '') ? $request->get('first_name') : null,
                        'surname' => ($request->get('last_name') != '') ? $request->get('last_name') : null,
                        'document_type_id' => null,
                        'number_document' => null,
                        'sex_id' => null,
                        'birthdate' => ($request->get('birthdate') != '') ? $request->get('birthdate') : null,
                        'email' => $request->get('email'),
                        'email2' => ($request->get('email2') != '') ? $request->get('email2') : null,
                        'password' => Hash::make($password),
                        'language_id' => $language_id,
                        'worker' => 0,
                        'worker_type_id' => null,
                        'observations' => null,
                        'country_id' => $country_id,
                        'social_register' => 0
                    ]);

                    $user->save();

                    if ($method == 'facebook') {
                        $user->update([
                            'facebook_id' => $request->get('social_id'),
                        ]);
                        $user->save();
                    } elseif ($method == 'google') {
                        $user->update([
                            'google_id' => $request->get('social_id'),
                        ]);
                        $user->save();
                    }

                    $array['data'] = array();
                    $token = $user->createToken('MotorOnline')->accessToken;
                    $remember_token = $user->createToken('MotorOnline')->accessToken;
                    $time_expired = SaasSettings::where('name', 'token_validate')->first()->value;
                    $expired_at = Carbon::now()->addMinutes($time_expired);

                    SaasUserToken::create([
                        'user_id' => $user->id,
                        'token' => $token,
                        'expired_at' => $expired_at,
                    ]);

                    SaasUserRememberToken::create([
                        'user_id' => $user->id,
                        'token' => $remember_token,
                        'ip' => $request->ip(),
                    ]);

                    SaasPasswordHistory::create([
                        'user_id' => $user->id,
                        'password' => $user->password,
                    ]);

                    $remember_token = $user->createToken('MotorOnline')->accessToken;

                    SaasUserRememberToken::create([
                        'user_id' => $user->id,
                        'token' => $remember_token,
                        'ip' => $request->ip(),
                    ]);

                    $array['data'][] = ['user_id' => $user->id, 'token' => 'Bearer ' . $token, 'remember_token' => $remember_token];
                } elseif ($user->bloqued_login == 1) {
                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'SaasUser bloqued']];
                } elseif ($user->bloqued_to >= Carbon::now()) {
                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'SaasUser bloqued temporarily']];
                } elseif (Auth::loginUsingId($user->id)) {
                    $error_social = 0;

                    if ($method == 'facebook') {
//                        if (isset($user->facebook_id) && intval($user->facebook_id) > 0) {
//
//                            if (intval($user->facebook_id) != intval($request->get('social_id'))) {
//                                $error_social = 1;
//                            }
//
//                        } else {
                        $user->update([
                                'facebook_id' => $request->get('social_id'),
                            ]);
                        $user->save();
//                        }
                    } elseif ($method == 'google') {
//                        if (isset($user->google_id) && intval($user->google_id) > 0) {
//
//                            if (intval($user->google_id) != intval($request->get('social_id'))) {
//                                $error_social = 1;
//                            }
//
//                        } else {
                        $user->update([
                                'google_id' => $request->get('social_id'),
                            ]);
                        $user->save();
//                        }
                    } else {
                        $error_social = 1;
                    }


                    if ($error_social == 0) {
                        $array['data'] = array();
                        $token = $user->createToken('MotorOnline')->accessToken;

                        $remember_token = null;

                        $time_expired = SaasSettings::where('name', 'token_validate')->first()->value;
                        $expired_at = Carbon::now()->addMinutes($time_expired);

                        SaasUserToken::create([
                            'user_id' => $user->id,
                            'token' => $token,
                            'expired_at' => $expired_at,
                        ]);

                        $remember_token = $user->createToken('MotorOnline')->accessToken;

                        SaasUserRememberToken::create([
                            'user_id' => $user->id,
                            'token' => $remember_token,
                            'ip' => $request->ip(),
                        ]);


                        $user->faults_login = 0;
                        $user->save();
                        $array['data'][] = ['user_id' => $user->id, 'token' => 'Bearer ' . $token, 'remember_token' => $remember_token];
                    } else {
                        $array['error'] = 403;
                        $array['error_description'] = 'Forbidden';
                        $array['error_inputs'][0] = ['social_id' => [0 => 'Facebook Token error.']];
                    }
                } else {
                    $array = ['error' => 401, 'error_description' => 'Unauthorized'];
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para cambiar password unicamente si el usuario tiene social_register a 0
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changepassword(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::beginTransaction();

            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {
                    $token = SaasUserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {
                        $error = 0;
                        $mensaje_validador = collect();

                        $validator = \Validator::make($request->all(), [
                            'password' => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                            'c_password' => 'required|same:password'
                        ]);

                        if ($validator->fails()) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge($validator->errors());
                        }

                        //controla que no introduzcan espacios en blanco en el password ni caracteres acentuados
                        if (preg_match('/\s/', $request->get('password')) || preg_match('/\s/', $request->get('c_password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('c_password'))) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
                        }
                        if ($error == 1) {
                            $array['error'] = 400;
                            $array['error_description'] = 'The fields are not the required format';
                            $array['error_inputs'][] = $mensaje_validador;
                        } else {
                            $user = SaasUser::find($token->user_id);
                            
                            // El usuario se ha registrado con sociallogin y ademas tiene social_register a 0
                            if (($user->facebook_id || $user->google_id) && $user->social_register === 0) {
                                $user->update([
                                    'password' => Hash::make($request->get('password')),
                                    'social_register' => 1
                                ]);

                                SaasPasswordHistory::create([
                                    'user_id' => $user->id,
                                    'password' => $user->password
                                ]);
                            } else {
                                $array['error'] = 400;
                                $array['error_description'] = 'The fields are not the required format';
                                $array['error_inputs'][] = ['password' => ['The password entered must be different from the last four.']];
                            }
                        }
                    } else {
                        $array['error'] = 401;
                        $array['error_description'] = 'Unauthorized';
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportSaasService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }


    public function engineLogin(Request $request) {
        $array['error'] = 200;
        try{

            DB::beginTransaction();
            
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'website_id' => 'required|integer|min:0|exists:saas_website,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = SaasUserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $datos_website = SaasWebsite::where('id',$request->get('website_id'))
                    ->first();

                $error_aux = 0;

                if($datos_website) {
                    if ($user_id != '') {
                        if ($datos_website->user_id != $user_id) {
                            $error_aux = 1;
                        }
                    }
                } else {
                    $error_aux = 1;
                }

                if ($error_aux == 1) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['You cannot log in to the selected website']]);
                }

                if ($error == 1) {
                    $array['error'] = 400;
                    $array['error_description'] = 'The fields are not the required format';
                    $array['error_inputs'][] = $mensaje_validador;
                } else {
                    $token_api = explode(' ', $request->header('Authorization'));
                    $token = SaasUserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {        
                        $protocol = SaasSettings::where('name', 'protocol')->first()->value;
                        $base_path_control_panel = SaasSettings::where('name', 'base_path_control_panel')->first()->value;

                        $tenancy = app(Environment::class);

                        $website = app(WebsiteRepository::class)->findById($datos_website->website_id);
    
                        $tenancy->tenant($website);

                        $user = User::where('system_user_id', $user_id)->first();

                        if($user) {

                            $token = $user->createToken('MotorOnline')->accessToken;

                            UserToken::create([
                                'user_id' => $user->id,
                                'token' => $token,
                                'expired_at' => Carbon::now()->addMinutes(Settings::where('name', 'token_validate')->first()->value),
                            ]);
    
                            $cp_route = $datos_website->domain && $datos_website->domain != '' ?  $protocol.$datos_website->domain.'.'.$base_path_control_panel : null;

                            $array['data'][] = ['token' => encriptar($token), 'cproute' => $cp_route];
                            
                        } else {
                            $array['error'] = 404;
                            $array['error_description'] = 'Data not found';
                        }
                    } else {
                        $array['error'] = 404;
                        $array['error_description'] = 'Data not found';
                    }
                }
                
            }

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'SaasUsuarios');
        }

        return response()->json($array, $array['error']);
    }
    
}
