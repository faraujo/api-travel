<?php

namespace App\Http\Controllers\tenant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use Illuminate\Support\Str;
use App\Models\tenant\WebCustomerSupportTranslation;
use App\Models\tenant\WebCustomerSupport;
use App\Models\tenant\Settings;

class WebCustomerSupportController extends Controller
{

    public function create(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $array_traducciones = array();

            $error = 0;
            $mensaje_validador = collect();

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('contact')[$idioma->abbreviation])) {
                    $validator = \Validator::make($request->all(), [
                        'name.' . $idioma->abbreviation => 'required',
                        'contact.' . $idioma->abbreviation => 'required'
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $customer_support = WebCustomerSupport::create([]);


                foreach ($array_traducciones as $idioma) {
                    WebCustomerSupportTranslation::create([
                        'customer_support_id' => $customer_support->id,
                        'language_id' => $idioma->id,
                        'name' => $request->get('name')[$idioma->abbreviation],
                        'contact' => $request->get('contact')[$idioma->abbreviation]
                    ]);
                }
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Customer Support');

        }

        return response()->json($array, $array['error']);
    }

    //
    public function show(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();
            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_customer_support = DB::connection('tenant')->table('mo_web_customer_support')
                    ->select(
                        'mo_web_customer_support.id'
                    )
                    ->whereNull('mo_web_customer_support.deleted_at')
                    ->join('mo_web_customer_support_translation','mo_web_customer_support_translation.customer_support_id','mo_web_customer_support.id')
                    ->whereNull('mo_web_customer_support_translation.deleted_at');

                $sub = DB::connection('tenant')->table('mo_web_customer_support')
                    ->select(
                        'mo_web_customer_support.id'
                    )
                    ->whereNull('mo_web_customer_support.deleted_at')
                    ->join('mo_web_customer_support_translation','mo_web_customer_support_translation.customer_support_id','mo_web_customer_support.id')
                    ->whereNull('mo_web_customer_support_translation.deleted_at');

                if ($request->get('lang') != '') {
                    $sql_customer_support->where('mo_web_customer_support_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_web_customer_support_translation.language_id', '=', $idioma->id);
                }

                if($request->get('name') != '') {
                    $sql_customer_support->where('mo_web_customer_support_translation.name', 'like', '%'.$request->get('name').'%');
                    $sub->where('mo_web_customer_support_translation.name', 'like', '%'.$request->get('name').'%');
                }

                if($request->get('contact') != '') {
                    $sql_customer_support->where('mo_web_customer_support_translation.contact', 'like', '%'.$request->get('contact').'%');
                    $sub->where('mo_web_customer_support_translation.contact', 'like', '%'.$request->get('contact').'%');
                }

                $orden = 'mo_web_customer_support_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_web_customer_support.id';
                            break;
                        case 'name':
                            $orden = 'mo_web_customer_support_translation.name';
                            break;
                        case 'contact':
                            $orden = 'mo_web_customer_support_translation.contact';
                            break;
                        default:
                            $orden = 'mo_web_customer_support_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_customer_support = $sql_customer_support->groupBy('mo_web_customer_support.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_customer_support = $sql_customer_support->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_customer_support = $sql_customer_support->get();

                }
                //Fin de paginación

                $sub->groupBy('mo_web_customer_support.id');

                $customer_support_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $customer_support_count->count();

                $array['data'] = array();
                foreach ($datos_customer_support as $customer_support) {
                    $array_customer_support = array();

                    $array_customer_support['id'] = $customer_support->id;

                    foreach($idiomas as $idioma) {
                        $traducciones = WebCustomerSupportTranslation::where('customer_support_id', $customer_support->id)
                            ->select(
                                'id',
                                'language_id',
                                'name',
                                'contact'
                            )
                            ->where('language_id', $idioma->id)
                            ->get();

                        foreach($traducciones as $traduccion) {
                            $array_traduccion = array();

                            $array_traduccion['id'] = $traduccion->id;
                            $array_traduccion['language_id'] = $traduccion->language_id;
                            $array_traduccion['name'] = $traduccion->name;
                            $array_traduccion['contact'] = $traduccion->contact;

                            $array_customer_support['lang'][][$idioma->abbreviation] = $array_traduccion;
                        }
                    }

                    $array['data'][0]['customersupport'][] = $array_customer_support;

                }
                
                $array['total_results'] = $totales;
                
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Customer Support');

        }

        return response()->json($array, $array['error']);
    }

    public function showId($id) {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $sql_customer_support = DB::connection('tenant')->table('mo_web_customer_support')
                ->select(
                    'mo_web_customer_support.id'
                )
                ->whereNull('mo_web_customer_support.deleted_at')
                ->join('mo_web_customer_support_translation','mo_web_customer_support_translation.customer_support_id','mo_web_customer_support.id')
                ->whereNull('mo_web_customer_support_translation.deleted_at')
                ->where('mo_web_customer_support.id', $id);

            $datos_customer_support = $sql_customer_support->get();
            $array['data'] = array();
            foreach($datos_customer_support as $customer_support) {
                $array_customer_support = array();

                $array_customer_support['id'] = $customer_support->id;

                foreach ($idiomas as $idioma) {
                    $traducciones = WebCustomerSupportTranslation::where('customer_support_id', $customer_support->id)
                        ->select(
                            'id',
                            'language_id',
                            'name',
                            'contact'
                        )->where('language_id', $idioma->id)
                        ->get();
                    
                    foreach($traducciones as $traduccion) {
                        $array_traduccion = array();

                        $array_traduccion['id'] = $traduccion->id;
                        $array_traduccion['language_id'] = $traduccion->language_id;
                        $array_traduccion['name'] = $traduccion->name;
                        $array_traduccion['contact'] = $traduccion->contact;

                        $array_customer_support['lang'][][$idioma->abbreviation] = $array_traduccion;
                    }

                }

                $array['data'][0]['customersupport'][] = $array_customer_support;
            }

            

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Customer Support');
        }

        return response()->json($array, $array['error']);
    }

    public function update(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $array_traducciones = array();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('contact')[$idioma->abbreviation])) {
                    $validator = \Validator::make($request->all(), [
                        'name.' . $idioma->abbreviation => 'required',
                        'contact.' . $idioma->abbreviation => 'required'
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $customer_support = WebCustomerSupport::where('id', $request->get('id'))->first();

                if(!$customer_support) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
    
                    $customer_support->customerSupportTranslation()->delete();

                    foreach ($array_traducciones as $idioma) {
                        WebCustomerSupportTranslation::create([
                            'customer_support_id' => $customer_support->id,
                            'language_id' => $idioma->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'contact' => $request->get('contact')[$idioma->abbreviation]
                        ]);
                    }
                }
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Customer Support');

        }

        return response()->json($array, $array['error']);
    }

    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = \Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_web_customer_support,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {

                    foreach ($request->get('ids') as $customer_support) {

                        $producto = WebCustomerSupport::where('id', $customer_support)->first();
                        $producto->customerSupportTranslation()->delete();
                        $producto->delete();
                    }

                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Productos');
        }

        return response()->json($array, $array['error']);
    }

    
}
