<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\UomTranslation;
use Illuminate\Support\Str;
use Validator;
//use \Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use Illuminate\Support\Facades\DB;


class UomController extends Controller
{

    //use DatabaseTransactions;

    /**
     * Función que muestra las unidades de medida disponibles
     *
     * Para la consulta de las unidades de medida se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUom(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $uoms = DB::connection('tenant')->table('mo_uom')
                    ->select('mo_uom.id')
                    ->where('mo_uom.deleted_at', '=', null)
                    ->join('mo_uom_translation', 'mo_uom.id', 'mo_uom_translation.uom_id')
                    ->where('mo_uom_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_uom')
                    ->select('mo_uom.id')
                    ->where('mo_uom.deleted_at', '=', null)
                    ->join('mo_uom_translation', 'mo_uom.id', 'mo_uom_translation.uom_id')
                    ->where('mo_uom_translation.deleted_at', '=', null);
                //Fin precompiladas

                //Filtros de busqueda
                if ($request->get('lang') != '') {
                    $uoms->where('mo_uom_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_uom_translation.language_id', '=', $idioma->id);
                }
                //Fin de filtros

                // Order
                $orden = 'mo_uom.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_uom.id';
                            break;
                        case 'name':
                            $orden = 'mo_uom_translation.name';
                            break;
                        default:
                            $orden = 'mo_uom.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $uoms = $uoms->groupBy('mo_uom.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $uoms = $uoms->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $uoms = $uoms->get();

                }

                //Fin de paginación



                $sub->groupBy('mo_uom_translation.uom_id');
                $uoms_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $uoms_count->count();

                $array['data'] = array();


                //Se recorren las unidades de medida
                foreach ($uoms as $uom) {

                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de las unidades de medida
                        $traduccion = UomTranslation::where('uom_id', '=', $uom->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name','description']);
                        foreach ($traduccion as $trad) {
                            $uom->lang[][$idi->abbreviation] = $trad;
                        }

                    }

                    $array['data'][0]['uom'][] = $uom;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Unidades de medida');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Devuelve la unidad de medida solicitada
     *
     * Para la consulta de una unidad de medida se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUomId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Consulta BBDD
            $uom = DB::connection('tenant')->table('mo_uom')
                ->select('mo_uom.id')
                ->join('mo_uom_translation', 'mo_uom.id', '=', 'mo_uom_translation.uom_id')
                ->where('mo_uom_translation.deleted_at', '=', null)
                ->where('mo_uom.deleted_at', '=', null)
                ->where('mo_uom.id', '=', $id);


            $uom = $uom->groupBy('mo_uom.id');
            $uom = $uom->get();
            //Fin consulta BBDD

            $array['data'] = array();
            foreach ($uom as $uom_aux) {
                //Traducciones de la unidad de medida
                foreach ($idiomas as $idi) {
                    $traducciones = UomTranslation::where('language_id', '=', $idi->id)->where('uom_id', '=', $uom_aux->id)
                        ->select('id', 'language_id', 'name','description')
                        ->get();
                    foreach ($traducciones as $trad) {
                        $uom_aux->lang[][$idi->abbreviation] = $trad;
                    }
                }
                $array['data'][0]['uom'][] = $uom_aux;
            }

            //Fin traducciones

            DB::connection('tenant')->commit();


        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Unidades de medida');
        }
        return response()->json($array, $array['error']);
    }

}