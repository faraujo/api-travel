<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Language;
use App\Models\tenant\WorkerTypeTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use App\Exceptions\Handler;

class WorkerTypeController extends Controller
{

    /**
     * Muestra los tipos de trabajador
     *
     * Para la consulta de tipos de trabajador se realiza una petición GET. Si la operación no produce errores se devuelve,
     * en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showWorkerTypes(Request $request)
    {
        $array['error'] = 200;
        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $worker_types = DB::connection('tenant')->table('mo_worker_type')
                    ->select('mo_worker_type.id')
                    ->where('mo_worker_type.deleted_at', '=', null)
                    ->join('mo_worker_type_translation', 'mo_worker_type_translation.worker_type_id', '=', 'mo_worker_type.id')
                    ->where('mo_worker_type_translation.deleted_at', '=', null)
                    ->join('mo_language', 'mo_language.id', '=', 'mo_worker_type_translation.language_id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_worker_type_translation.worker_type_id');


                $sub = DB::connection('tenant')->table('mo_worker_type')
                    ->select('mo_worker_type.id')
                    ->where('mo_worker_type.deleted_at', '=', null)
                    ->join('mo_worker_type_translation', 'mo_worker_type_translation.worker_type_id', '=', 'mo_worker_type.id')
                    ->where('mo_worker_type_translation.deleted_at', '=', null)
                    ->join('mo_language', 'mo_language.id', '=', 'mo_worker_type_translation.language_id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_worker_type_translation.worker_type_id');

                if ($request->get('lang') != '') {
                    $worker_types->where('mo_worker_type_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_worker_type_translation.language_id', '=', $idioma->id);
                }

                $worker_types_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $worker_types_count->count();

                $worker_types = $worker_types->get();

                $array['data'] = array();
                foreach ($worker_types as $worker_type) {

                    foreach ($idiomas as $idi) {
                        $traduccion = WorkerTypeTranslation::where('worker_type_id', '=', $worker_type->id)
                            ->select('language_id', 'name', 'description')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $worker_type->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['worker'][] = $worker_type;
                }

                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de trabajadores');
        }

        return response()->json($array, $array['error']);
    }
}