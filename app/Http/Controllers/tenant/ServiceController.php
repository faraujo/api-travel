<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Service;
use App\Models\tenant\Settings;
use App\Models\tenant\ProductService;
use App\Models\tenant\ServiceTranslation;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Product;
use App\Models\tenant\Language;
use Illuminate\Http\Request;
use App\Exceptions\Handler;

class ServiceController extends Controller
{


    /**
     * Función para la creacion de un servicio
     *
     * Para la creación de un servicio se realiza una petición POST.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $idiomas = Language::where('front',1)->get();
            $array_traducciones = array();

            //VALIDACION

            $mensaje_validador = collect();

            foreach ($idiomas as $idioma) {
                if (isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }

            }
            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            //FIN DE VALIDACION

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                //creación de Service
                $servicio = Service::create([]);
                //fin creación de Service

                //creación de traducciones
                foreach ($array_traducciones as $idioma) {
                    ServiceTranslation::create(
                        [
                            'service_id' => $servicio->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'language_id' => $idioma->id,
                            'description' => $request->get('description')[$idioma->abbreviation],
                        ]
                    );
                }
                //fin creación de traducciones
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Servicios');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Funcion para la eliminacion de servicios
     *
     * Para la eliminación de un servicio se realiza una petición DELETE.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_service,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $service_id) {
                        $servicio = Service::where('id', $service_id)->first();
                        // borrado de servicio, relaciones en tabla intermedia con productos y servicio
                        $servicio->productService()->delete();
                        $servicio->serviceTranslation()->delete();
                        $servicio->delete();
                        //fin borrado

                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Servicios');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para modificar un servicio.
     *
     * Para la modificación de servicios se realiza una petición PUT.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $idiomas = Language::where('front',1)->get();
            $array_traducciones = array();

            //Validación

            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'id' => 'integer|required',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }
            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //FIN VALIDACION

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $servicio = Service::find($request->get('id'));
                if (!$servicio) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    //actualiza traducciones
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        $traduccion = $servicio->serviceTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {

                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                            ]);
                        } else {
                            //si no existe traducción la crea
                            ServiceTranslation::create([
                                'service_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                            ]);
                            //FIN CREACION DE TRADUCCIONES
                        }
                    }
                    $servicio->serviceTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    //FIN ACTUALIZACION DE TRADUCCIONES

                }
            }
            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Servicios');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Funcion que lista todos los servicios
     *
     * Para la consulta de servicios se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'product_id' => 'integer|min:0',
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $servicios = DB::connection('tenant')->table('mo_service')
                    ->select('mo_service.id')
                    ->join('mo_service_translation', 'mo_service.id', '=', 'mo_service_translation.service_id')
                    ->where('mo_service_translation.deleted_at', '=', null)
                    ->where('mo_service.deleted_at', '=', null)
                    ->leftJoin('mo_product_service', function ($join) {
                        $join->on('mo_service.id', '=', 'mo_product_service.service_id')->where('mo_product_service.deleted_at', null);
                    })
                    ->leftjoin('mo_product', function ($join) {
                        $join->on('mo_product_service.product_id', '=', 'mo_product.id')->where('mo_product.deleted_at', null);
                    });

                $sub = DB::connection('tenant')->table('mo_service')
                    ->select('mo_service.id')
                    ->join('mo_service_translation', 'mo_service.id', '=', 'mo_service_translation.service_id')
                    ->where('mo_service_translation.deleted_at', '=', null)
                    ->where('mo_service.deleted_at', '=', null)
                    ->leftJoin('mo_product_service', function ($join) {
                        $join->on('mo_service.id', '=', 'mo_product_service.service_id')->where('mo_product_service.deleted_at', null);
                    })
                    ->leftjoin('mo_product', function ($join) {
                        $join->on('mo_product_service.product_id', '=', 'mo_product.id')->where('mo_product.deleted_at', null);
                    });

                //Filtros
                if ($request->get('lang') != '') {
                    $servicios->where('language_id', '=', $idioma->id);
                    $sub->where('language_id', '=', $idioma->id);
                }

                if ($request->get('product_id') != '') {
                    $servicios->where('mo_product.id', '=', $request->get('product_id'));
                    $sub->where('mo_product.id', '=', $request->get('product_id'));
                }

                if ($request->get('name') != '') {
                    $servicios->Where(function ($query) use ($request) {
                        $query->where('mo_service_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_service_translation.description', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_service_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_service_translation.description', 'Like', '%' . $request->get('name') . '%');
                    });
                }
                //FIN Filtros

                // Order
                //por defecto por name
                $orden = 'mo_service_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_service_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_service_translation.description';
                            break;
                        case 'product_id':
                            $orden = 'mo_service.product_id';
                            break;
                        case 'id':
                            $orden = 'mo_product_service.id';
                            break;
                        default:
                            $orden = 'mo_service_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $servicios = $servicios->groupBy('mo_service.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $servicios = $servicios->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $servicios = $servicios->get();

                }

                //Fin de paginación



                //Total de resultados

                $sub->groupBy('mo_service.id');
                $servicios_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $servicios_count->count();

                //Generación de salida

                $array['data'] = array();
                foreach ($servicios as $servicio) {
                    if ($request->get('lang') != '') {
                        $traducciones = ServiceTranslation::where('language_id', '=', $idioma->id)->where('service_id', '=', $servicio->id)
                            ->select('id', 'service_id', 'language_id', 'name', 'description')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $servicio->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = ServiceTranslation::where('service_id', '=', $servicio->id)
                                ->select('id', 'service_id', 'language_id', 'name', 'description')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $servicio->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['service'][] = $servicio;
                }
                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Servicios');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función que muestra un producto buscado por su id
     *
     * Para la consulta de un servicio se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // FIN Validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $servicios = DB::connection('tenant')->table('mo_service')
                    ->select('mo_service.id')
                    ->join('mo_service_translation', 'mo_service.id', '=', 'mo_service_translation.service_id')
                    ->where('mo_service_translation.deleted_at', '=', null)
                    ->where('mo_service.deleted_at', '=', null);
                //Filtros
                if ($request->get('lang') != '') {
                    $servicios->where('language_id', '=', $idioma->id);
                }

                $servicios->where('mo_service.id', '=', $id);
                //FIN Filtros

                $servicios = $servicios->groupBy('mo_service.id');
                $servicios = $servicios->get();


                $array['data'] = array();

                foreach ($servicios as $servicio) {
                    if ($request->get('lang') != '') {
                        $traducciones = ServiceTranslation::where('language_id', '=', $idioma->id)->where('service_id', '=', $servicio->id)
                            ->select('id', 'service_id', 'language_id', 'name', 'description')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $servicio->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = ServiceTranslation::where('service_id', '=', $servicio->id)
                                ->select('id', 'service_id', 'language_id', 'name', 'description')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $servicio->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['service'][] = $servicio;
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Servicios');
        }

        return response()->json($array, $array['error']);

    }


    /**
     * Función que asigna servicios a productos
     *
     * Para asignar servicios a un producto se realiza un petición POST.
     * Borra todas las relaciones existentes del producto con los servicios y crea las nuevas relaciones segun los servicios que se le especifica.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'product_id' => 'required|numeric',
                'services' => 'array',
                'services.*' => 'required|numeric|exists:tenant.mo_service,id,deleted_at,NULL|distinct',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin de validación

            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'product_id' => 'exists:tenant.mo_product,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    //Busca y borra todas los servicios asociados al producto
                    ProductService::where('product_id', '=', $request->get('product_id'))->delete();

                    //Si recibe parámetro servicios recorre el array y crea la relación con el producto
                    if ($request->get('services')) {
                        foreach ($request->get('services') as $service) {
                            ProductService::create([
                                'product_id' => $request->get('product_id'),
                                'service_id' => $service,
                            ]);
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Servicios');
        }

        return response()->json($array, $array['error']);
    }

}