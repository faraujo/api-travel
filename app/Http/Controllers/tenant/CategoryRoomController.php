<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\HotelRoomCategoryTranslation;
use Illuminate\Support\Str;
use Validator;
//use \Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileTypeTranslation;




class CategoryRoomController extends Controller
{

    //use DatabaseTransactions;

    /**
     * Función que muestra las categorías de habitaciones de hotel
     *
     * Para la consulta de las categorías se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCategories(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $categories = DB::connection('tenant')->table('mo_hotel_room_category')
                    ->select('mo_hotel_room_category.id')
                    ->where('mo_hotel_room_category.deleted_at', '=', null)
                    ->join('mo_hotel_room_category_translation', 'mo_hotel_room_category.id', 'mo_hotel_room_category_translation.category_id')
                    ->where('mo_hotel_room_category_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_hotel_room_category')
                    ->select('mo_hotel_room_category.id')
                    ->where('mo_hotel_room_category.deleted_at', '=', null)
                    ->join('mo_hotel_room_category_translation', 'mo_hotel_room_category.id', 'mo_hotel_room_category_translation.category_id')
                    ->where('mo_hotel_room_category_translation.deleted_at', '=', null);
                //Fin precompiladas

                //Filtros de busqueda
                if ($request->get('lang') != '') {
                    $categories->where('mo_hotel_room_category_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_hotel_room_category_translation.language_id', '=', $idioma->id);
                }
                //Fin de filtros


                // Order
                $orden = 'mo_hotel_room_category.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_hotel_room_category.id';
                            break;
                        case 'name':
                            $orden = 'mo_hotel_room_category_translation.name';
                            break;
                        default:
                            $orden = 'mo_hotel_room_category.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $categories = $categories->groupBy('mo_hotel_room_category.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $categories = $categories->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $categories = $categories->get();

                }


                // Totales resultado
                $sub->groupBy('mo_hotel_room_category_translation.category_id');
                $categories_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $categories_count->count();


                //ruta de storage de settings para concatenar a ruta de archivo de categoria
                $storage = Settings::where('name', 'storage_files')->first()->value;



                //generacion de salida

                $array['data'] = array();


                //Se recorren los tipos
                foreach ($categories as $category) {

                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de los tipos
                        $traduccion = HotelRoomCategoryTranslation::where('category_id', '=', $category->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name', 'description']);
                        foreach ($traduccion as $trad) {
                            $category->lang[][$idi->abbreviation] = $trad;
                        }

                    }

                    $files = DB::connection('tenant')->table('mo_hotel_room_category_file')
                        ->select('mo_file.id', 'mo_file.file_name','mo_file.file_size','mo_file.file_dimensions','mo_file.mimetype','mo_file.type_id', 'mo_file.order', 'mo_file.url_file','mo_file.device_id','mo_file.key_file')
                        ->where('mo_hotel_room_category_file.hotel_room_category_id', '=', $category->id)
                        ->where('mo_hotel_room_category_file.deleted_at', '=', null)
                        ->join('mo_file', 'mo_file.id', '=', 'mo_hotel_room_category_file.file_id')
                        ->where('mo_file.deleted_at', '=', null)
                        ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                        ->where('mo_file_translation.deleted_at', null)
                        ->orderBy('mo_file.order','desc')
                        ->groupBy('mo_file.id')->get();

                    $category->files = array();
                    $array_file = array();
                    foreach ($files as $file) {
                        //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                        $tipo = FileType::find($file->type_id);


                        $array_file = ['id' => $file->id, 'file_name' => $file->file_name,'file_size' => convertExtension($file->file_size),
                            'file_dimensions' => $file->file_dimensions,'mimetype' => $file->mimetype,'order' => $file->order, 'url_file' => $storage . $file->url_file,
                            'device_id' => $file->device_id, 'key_file' => $file->key_file, 'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];


                        foreach ($idiomas as $idi) {

                            //si el tipo no existe o está borrado no busca sus traducciones
                            if ($tipo) {
                                $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                    ->select('id', 'language_id', 'name')
                                    ->where('language_id', '=', $idi->id)
                                    ->groupBy('mo_file_type_translation.type_id')
                                    ->get();

                                foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                    $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                                }
                            }

                            $traducciones_files = FileTranslation::where('file_id', $file->id)
                                ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_translation.file_id')
                                ->get();

                            foreach ($traducciones_files as $traduccion_file) {
                                $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                            }
                        }
                        $category->files[] = $array_file;
                    }



                    $array['data'][0]['category'][] = $category;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Categorías de habitaciones');
        }

        return response()->json($array, $array['error']);
    }
}