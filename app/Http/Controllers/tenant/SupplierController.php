<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Settings;
use App\Models\tenant\Supplier;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SupplierController extends Controller
{

    /**
     * Función que lista todos los proveedores
     *
     * Para la consulta de proveedores se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();


            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                //Precompiladas
                $suppliers = DB::connection('tenant')->table('mo_supplier')
                    ->select('mo_supplier.id', 'mo_supplier.name')
                    ->where('mo_supplier.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_supplier')
                    ->select('mo_supplier.id')
                    ->where('mo_supplier.deleted_at', '=', null);
                //Fin precompiladas


                //Fin de filtros

                // Order
                $orden = 'mo_supplier.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_supplier.id';
                            break;
                        case 'name':
                            $orden = 'mo_supplier.name';
                            break;
                        default:
                            $orden = 'mo_supplier.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $suppliers = $suppliers->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $suppliers = $suppliers->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $suppliers = $suppliers->get();

                }

                //Fin de paginación



                $suppliers_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);



                $totales = $suppliers_count->count();


                // Generación de salida


                $array['data'] = array();


                //Se recorren los proveedores
                foreach ($suppliers as $supplier) {

                    $array['data'][0]['supplier'][] = $supplier;
                }
                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Proveedores');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para la creación de proveedores
     *
     * Para la creación de proveedores se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                // Crear proveedor
                $supplier = Supplier::create([
                    'name' => $request->get('name') != '' ? $request->get('name') : null,
                ]);
                // FIN crear proveedor

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Proveedores');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de proveedores.
     *
     * Para la eliminación de proveedores se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_supplier,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $supplier_id) {
                        $supplier = Supplier::where('id', $supplier_id)->first();
                        // borrado de proveedor
                        $supplier->delete();
                        //fin borrado

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Proveedores');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar un proveedor.
     *
     * Para la modificación de un proveedor se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $supplier = Supplier::find($request->get('id'));
                if (!$supplier) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar proveedor
                    $supplier->name = $request->get('name') != '' ? $request->get('name') : null;

                    $supplier->save();
                    //FIN actualizar proveedor

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Proveedores');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra un proveedor buscada por su id
     *
     * Para la consulta de un proveedor se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $datos_supplier = DB::connection('tenant')->table('mo_supplier')
            ->select(
                'mo_supplier.id',
                'mo_supplier.name'
            )
            ->where('mo_supplier.deleted_at', '=', null)
            ->where('mo_supplier.id', '=', $id);

            $datos_supplier = $datos_supplier->get();

            $array['data'] = array();
            foreach ($datos_supplier as $supplier) {
                
                $array['data'][0]['supplier'][] = $supplier;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Proveedores');
        }

        return response()->json($array, $array['error']);

    }
   
}