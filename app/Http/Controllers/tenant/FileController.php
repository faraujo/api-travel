<?php

namespace App\Http\Controllers\tenant;

use App\Http\Controllers\Controller;

use App\Models\tenant\File;
use App\Models\tenant\FileExtension;
use App\Models\tenant\FileResize;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Exceptions\Handler;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;

class FileController extends Controller
{

    /**
     * Método para la subida de archivos
     *
     * Para la creación de archivos se realiza una petición POST. Son requeridos los parámetros: files y type.
     * Opcionalmente se le puede indicar el parametro order del archivo que se desea subir y private para almacenarlo de forma pública o privada.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y una variable “data” con un objeto file con la información del archivo creado.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        set_time_limit(0);
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $idiomas = Language::where('front', 1)->get();

            $disco = Config::get('filesystems.default');

            // Cálculo de Tenant
            $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();
            if ($website) {
                $route_tenant=$website->uuid."/";
            } else {
                $route_tenant="";
            }

            //Validación

            //Validación de extensión del archivo
            $extensiones = '';
            if ($request->get('type') != '') {
                $datos_extensiones = FileExtension::where('name', '=', STR::lower($request->get('type')))->first();

                if ($datos_extensiones != '') {
                    $extensiones = $datos_extensiones->extensions;
                }
            }

            $upload_max_size = Settings::where('name', '=', 'upload_max_size')->first()->value;

            $validator = \Validator::make($request->all(), [
                'files' => 'array|required',
                'files.*' => 'array',
                'files.*.file' => 'required|mimetypes:' . $extensiones,
                'files.*.file' => 'max:'.$upload_max_size,
                'files.*.order' => 'integer|min:0',
                'files.*.key_file' => 'integer|min:0',
                'files.*.device_id' => 'integer|exists:tenant.mo_device,id,deleted_at,NULL',
                'type' => 'required|exists:tenant.mo_file_extension,name,deleted_at,NULL',
                'private' => 'boolean',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Fin de validación de extensión


            // FIN validación
            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $fecha = Carbon::now()->format('Y').'/'.Carbon::now()->format('m');

                if ($request->allFiles()['files'] != '') {
                    $posicion = 0;
                    $array_file = array();

                    foreach ($request->allFiles()['files'] as $key => $archivo) {
                        $privado = 0;
                        if ($request->get('private') != '' && $request->get('private') == 1) {
                            $privado = 1;
                            $path = Storage::put($route_tenant.'mtr/private/' . $fecha, $archivo['file'], 'private');
                        } else {
                            $privado = 0;
                            $path = Storage::put($route_tenant.'mtr/public/' . $fecha, $archivo['file'], 'public');
                        }

                        $filename =  isset($request->get('files')[$posicion]['original_name']) ? $request->get('files')[$posicion]['original_name'] : $archivo['file']->getClientOriginalName();
                        $url_file = isset($archivo['file']) ? $path : $archivo['file']['file'];
                        $dimesiones = getimagesize($archivo['file']);
                        $mimetype = $archivo['file']->getMimeType();
                        $key_file_random = rand(0, 499900) + 100;
                        
                        $guarda_archivo = File::create([
                            'order' => isset($request->get('files')[$posicion]['order']) ? $request->get('files')[$posicion]['order'] : 0,
                            'private' => $privado,
                            'url_file' => $url_file,
                            'mimetype' => $mimetype,
                            'file_dimensions' => $dimesiones[0] ? $dimesiones[0] . ' x ' . $dimesiones[1] : null,
                            'file_name' => $filename,
                            'file_size' => $archivo['file']->getClientSize(),
                            'key_file' => isset($request->get('files')[$posicion]['key_file']) ? $request->get('files')[$posicion]['key_file'] : $key_file_random,
                            'device_id' => isset($request->get('files')[$posicion]['device_id']) ? $request->get('files')[$posicion]['device_id'] : 1,
                            'type_id' => strpos($mimetype, 'image') !== false ? 3 : 2
                        ]);

                        // Redimensionar si es tipo imagen
                        if (strpos($mimetype, 'image') !== false) {
                            $url_resize = explode('/', $url_file);
                            $file_name = $url_resize[count($url_resize)-1];
                            $extension = image_type_to_extension($dimesiones[2]);

                            //NEW SIZES
                            $sizes = FileResize::get();

                            if (isset($sizes)) {
                                foreach ($sizes as $size) {
                                    $value = $size->value;

                                    $image_resize = Image::make($archivo['file'])->resize($value, null, function ($constraint) {
                                        $constraint->aspectRatio();
                                    })->encode($extension == '.png' ? 'png' : 'jpeg', 80);

                                    if ($privado == 1) {
                                        $path_resize = Storage::put($route_tenant.'mtr/private/' . $fecha . '/' . $value . '_' . $file_name, $image_resize->__toString(), 'private');
                                    } else {
                                        $path_resize = Storage::put($route_tenant.'mtr/public/' . $fecha . '/' . $value . '_' . $file_name, $image_resize->__toString(), 'public');
                                    }
                                }
                            }
                        }

                        $array_file = [
                            'id' => $guarda_archivo->id, 
                            'file_name' => $guarda_archivo->file_name, 
                            'file_size' => convertExtension($guarda_archivo->file_size), 
                            'file_dimensions' => $guarda_archivo->file_dimensions, 
                            'mimetype' => $guarda_archivo->mimetype, 
                            'order' => $guarda_archivo->order, 
                            'url_file' => Storage::url($guarda_archivo->url_file), 
                            'type' => null,
                            'device_id' => $guarda_archivo->device_id,
                            'key_file' => $guarda_archivo->key_file,
                            'type_id' => $guarda_archivo->type_id,
                        ];

                        if ($privado == 1 && $disco == 's3') {
                            $setting_expired = Settings::where('name', '=', 'minutes_expired_file')->first();
                            $expired = $setting_expired->value;
                            $array_file['url_file'] = Storage::temporaryUrl(
                                $guarda_archivo->url_file,
                                now()->addMinutes($expired)
                            );
                        }
                        $name = isset($request->get('files')[$posicion]['original_name']) ? $request->get('files')[$posicion]['original_name'] : $archivo['file']->getClientOriginalName();

                        foreach ($idiomas as $idioma) {
                            $traduccion_file = FileTranslation::create([
                                'file_id' => $guarda_archivo->id,
                                'language_id' => $idioma->id,
                                'name' => $name,
                                'description' => null,
                                'alternative_text' => $name,
                            ]);

                            $array_file['lang'][][$idioma->abbreviation] = ['id' => $traduccion_file->id, 'language_id' => $traduccion_file->language_id,
                                'name' => $traduccion_file->name, 'description' => $traduccion_file->description, 'alternative_text' => $traduccion_file->alternative_text];
                        }
                        $posicion++;
                        $array['data'][0]['file'][] = $array_file;
                    }
                }
            }
            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
            reportService($e, 'Archivos');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Consulta de archivos subidos
     *
     * Para la consulta de archivos se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function show(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'integer',
                'limit' => 'integer|min:0',
                'type_id' => 'integer',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación envia mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $archivos = DB::connection('tenant')->table('mo_file')
                    ->select('mo_file.id', 'mo_file.type_id', 'mo_file.order', 'mo_file.private', 'mo_file.url_file', 'mo_file_translation.id', 'mo_file_translation.language_id', 'mo_file_translation.name', 'mo_file_translation.description', 'mo_file_translation.alternative_text')
                    ->join('mo_file_translation', 'mo_file_translation.file_id', 'mo_file.id')
                    ->leftJoin('mo_file_type', 'mo_file_type.id', 'mo_file.type_id')
                    ->where('mo_file_type.deleted_at', '=', null)
                    ->where('mo_file.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_file')
                    ->select('mo_file.id')
                    ->join('mo_file_translation', 'mo_file_translation.file_id', 'mo_file.id')
                    ->leftJoin('mo_file_type', 'mo_file_type.id', 'mo_file.type_id')
                    ->where('mo_file_type.deleted_at', '=', null)
                    ->where('mo_file.deleted_at', '=', null);


                if ($request->get('lang') != '') {
                    $archivos->where('mo_file_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_file_translation.language_id', '=', $idioma->id);
                }

                if ($request->get('type_id') != '') {
                    $archivos->where('mo_file.type_id', '=', $request->get('type_id'));
                    $sub->where('mo_file.type_id', '=', $request->get('type_id'));
                }

                if ($request->get('name') != '') {
                    $archivos->Where(function ($query) use ($request) {
                        $query->where('mo_file_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_file_translation.description', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_file_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_file_translation.description', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('private') != '') {
                    $archivos->where('mo_file.private', '=', $request->get('private'));
                    $sub->where('mo_file.private', '=', $request->get('private'));
                }

                // Order
                $orden = 'mo_file.order';
                $request_order = $request->get('order');
                switch ($request_order) {
                    case 'id':
                        $orden = 'mo_file.id';
                        break;
                    case 'user_id':
                        $orden = 'mo_file.user_id';
                        break;
                    case 'order':
                        $orden = 'mo_file.order';
                        break;
                    case 'type_id':
                        $orden = 'mo_file.type_id';
                        break;
                    default:
                        $orden = 'mo_file.order';
                        break;
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $archivos = $archivos->groupBy('mo_file.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {
                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $archivos = $archivos->forPage($inicio, $limite)->get();

                //si filtro limit = 0 se obtienen todos los resultados
                } else {
                    $archivos = $archivos->get();
                }

                //Fin de paginación


                $sub->groupBy('mo_file.id');
                $achivos_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $achivos_count->count();


                $array['data'] = array();
                $disco = Config::get('filesystems.default');

                $traducciones = DB::connection('tenant')->table('mo_file_translation')
                    ->select('mo_file_translation.id', 'mo_file_translation.language_id', 'mo_file_translation.name', 'mo_file_translation.description', 'mo_file_translation.alternative_text', 'mo_language.id', 'mo_language.abbreviation')
                    ->join('mo_language', 'mo_language.id', 'mo_file_translation.language_id');

                foreach ($archivos as $archivo) {
                    $sql_traducciones = clone $traducciones;
                    $sql_traducciones->where('mo_file_translation.file_id', $archivo->id);
                    if ($request->get('lang') != '') {
                        $sql_traducciones->where('mo_language.abbreviation', $request->get('lang'));
                    }
                    $datos_traducciones = $sql_traducciones->get();
                    $translate = array();
                    foreach ($datos_traducciones as $traduccion) {
                        $translate[][$traduccion->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name, 'description' => $traduccion->description, 'alternative_text' => $traduccion->alternative_text];
                    }

                    //Generación url de archivo

                    if ($disco == 's3') {
                        if ($archivo->type_id != 6 && $archivo->type_id != 7) {
                            if ($archivo->private == 1) {
                                $setting_expired = Settings::where('name', '=', 'minutes_expired_file')->first();
                                $expired = $setting_expired->value;

                                $url = Storage::temporaryUrl(
                                    $archivo->url_file,
                                    now()->addMinutes($expired)
                                );
                                $archivo->url = $url;
                            } else {
                                $archivo->url = Storage::url($archivo->url_file);
                            }
                        } else {
                            $archivo->url = $archivo->url_file;
                        }
                    } else {
                        if ($archivo->type_id != 6 && $archivo->type_id != 7) {
                            $archivo->url = Storage::url($archivo->url_file);
                        } else {
                            $archivo->url = $archivo->url_file;
                        }
                    }

                    $array['data'][0]['file'][] = ['id' => $archivo->id, 'type_id' => $archivo->type_id,
                        'order' => $archivo->order, 'private' => $archivo->private, 'url_file' => $archivo->url_file, 'url' => $archivo->url, 'lang' => $translate];
                }

                $array['total_results'] = $totales;
            }
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
            reportService($e, 'Archivos');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Consulta de un archivo subido buscado por su id
     *
     * Para la consulta de un archivo se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();
            $idiomas = Language::where('front', 1)->get();

            $archivos = DB::connection('tenant')->table('mo_file')
                ->select(
                    'mo_file.id',
                    'mo_file.type_id',
                    'mo_file.order',
                    'mo_file.private',
                    'mo_file.url_file',
                    'mo_file.file_name',
                    'mo_file.file_size',
                    'mo_file.file_dimensions',
                    'mo_file.mimetype',
                    'mo_file.created_at',
                    'mo_file.device_id',
                    'mo_file.key_file'
                )
                ->where('mo_file.deleted_at', '=', null)
                ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                ->where('mo_file_translation.deleted_at', '=', null);


            $archivos->where('mo_file.id', '=', $id);

            $archivos = $archivos->groupBy('mo_file.id');
            $archivos = $archivos->get();

            $disco = Config::get('filesystems.default');

            $array['data'] = array();
            foreach ($archivos as $archivo) {
                if ($disco == 's3' && $archivo->private != 0) {
                    $setting_expired = Settings::where('name', '=', 'minutes_expired_file')->first();
                    $expired = $setting_expired->value;

                    $archivo->url = Storage::temporaryUrl(
                        $archivo->url_file,
                        now()->addMinutes($expired)
                    );
                } else {
                    $archivo->url = Storage::url($archivo->url_file);
                }

                foreach ($idiomas as $idi) {
                    $traduccion = FileTranslation::where('file_id', '=', $archivo->id)
                        ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                        ->where('language_id', '=', $idi->id)
                        ->get();
                    foreach ($traduccion as $trad) {
                        $archivo->lang[][$idi->abbreviation] = $trad;
                    }
                }

                $array['data'][0]['file'][] = ['id' => $archivo->id, 'type_id' => $archivo->type_id, 'order' => $archivo->order, 'private' => $archivo->private,
                    'url_file' => $archivo->url_file, 'file_name' => $archivo->file_name, 'file_size' => convertExtension($archivo->file_size),
                    'file_dimensions' => $archivo->file_dimensions, 'mimetype' => $archivo->mimetype, 'created_at' => $archivo->created_at, 'url' => $archivo->url,
                    'device_id' => $archivo->device_id,'key_file' => $archivo->key_file,'lang' => $archivo->lang];
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
            reportService($e, 'Archivos');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función para la eliminación de uno o varios archivos.
     *
     * Para la eliminación de archivos se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            //Validación


            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_file,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $file_id) {
                        $file = File::where('id', $file_id)->first();
                        $file->fileTranslation()->delete();
                        $file->delete();
                    }
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Archivos');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Método para la actualización de archivos
     *
     * Para la modificación de archivos se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y una variable “data” con un objeto file con la información del archivo modificado.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = [];

            $idiomas = Language::where('front', 1)->get();


            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'type_id' => 'integer|exists:tenant.mo_file_type,id,deleted_at,NULL',
                'order' => 'integer|min:0',
                'name' => 'array|required',
                'name.*' => 'required',
                'alternative_text' => 'array|required',
                'alternative_text.*' => 'required',
                'description' => 'array',
                'private' => 'boolean',
                'device_id' => 'integer|exists:tenant.mo_device,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
 
            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) || isset($request->get('alternative_text')[$idioma->abbreviation])) {
                    $validator = \Validator::make($request->all(), [
                        'name.' . $idioma->abbreviation => 'required',
                        'alternative_text.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }


            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $file = File::where('id', '=', $request->get('id'))->first();

                if (!$file) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    $disco = Config::get('filesystems.default');

                    $file->update([
                        'order' => $request->get('order') != '' ? $request->get('order') : $file->order,
                        'private' => $request->get('private') != '' ? $request->get('private') : $file->private,
                        'type_id' => $request->get('type_id') != '' ? $request->get('type_id') : $file->type_id,
                        'device_id' => $request->get('device_id') != '' ? $request->get('device_id') : $file->device_id,
                        'key_file' => $request->get('key_file') != '' ? $request->get('key_file') : $file->key_file,
                    ]);

                    $traduccionesBorrar = $file->fileTranslation();

                    $array_file = array();


                    $array_file = ['id' => $file->id, 'file_name' => $file->file_name, 'file_size' => convertExtension($file->file_size), 'file_dimensions' => $file->file_dimensions, 'mimetype' => $file->mimetype, 'order' => $file->order,
                        'url_file' => Storage::url($file->url_file),'device_id' => $file->device_id,'key_file' => $file->key_file,
                         'type' => ($file->type_id != null) ? ['id' => $file->type_id] : null];

                    if ($request->get('private') == 1 && $disco == 's3') {
                        $setting_expired = Settings::where('name', '=', 'minutes_expired_file')->first();
                        $expired = $setting_expired->value;
                        $array_file['url_file'] = Storage::temporaryUrl(
                            $file->url_file,
                            now()->addMinutes($expired)
                        );
                    }

                    foreach ($array_traducciones as $idioma) {
                        $traduccion = $file->fileTranslation()->where('language_id', '=', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {
                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => isset($request->get('description')[$idioma->abbreviation]) ? $request->get('description')[$idioma->abbreviation] : null,
                                'alternative_text' => $request->get('alternative_text')[$idioma->abbreviation],
                            ]);
                        } else {
                            FileTranslation::create([
                                'file_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => isset($request->get('description')[$idioma->abbreviation]) ? $request->get('description')[$idioma->abbreviation] : null,
                                'alternative_text' => $request->get('alternative_text')[$idioma->abbreviation],
                            ]);
                        }
                    }

                    $traduccionesBorrar->whereNotIn('language_id', $array_borrar)->delete();

                    //borra traducciones que no se hayan enviado si existen y prepara salida con traducciones del archivo actualizado
                    foreach ($idiomas as $idioma) {
                        $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                            ->select('id', 'language_id', 'name')
                            ->where('language_id', '=', $idioma->id)
                            ->groupBy('mo_file_type_translation.type_id')
                            ->get();

                        foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                            $array_file['type']['lang'][][$idioma->abbreviation] = $traduccion_tipo_file;
                        }

                        $traduccion = $file->fileTranslation()->where('language_id', '=', $idioma->id)
                            ->where('deleted_at', '=', null)->first();

                        if ($traduccion) {
                            $array_file['lang'][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id,
                                'name' => $traduccion->name, 'description' => $traduccion->description, 'alternative_text' => $traduccion->alternative_text];
                        }
                    }

                    $array['data'][0]['file'][] = $array_file;
                }
            }


            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
            reportService($e, 'Archivos');
        }

        return response()->json($array, $array['error']);
    }
}
