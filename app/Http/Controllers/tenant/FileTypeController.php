<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Exceptions\Handler;

class FileTypeController extends Controller
{

    /**
     * Muestra los tipos de archivos que se pueden subir
     *
     * Para la consulta de tipos de archivos se realiza una petición GET. Si la operación no produce errores se devuelve,
     * en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTypes(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                $tipos = DB::connection('tenant')->table('mo_file_type')
                    ->select('mo_file_type.id')
                    ->join('mo_file_type_translation', 'mo_file_type.id', 'mo_file_type_translation.type_id')
                    ->where('mo_file_type_translation.deleted_at', '=', null)
                    ->where('mo_file_type.deleted_at', '=', null)
                    ->join('mo_language', 'mo_file_type_translation.language_id', 'mo_language.id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_file_type.id')->get();

                $sub = DB::connection('tenant')->table('mo_file_type')
                    ->select('mo_file_type.id')
                    ->join('mo_file_type_translation', 'mo_file_type.id', 'mo_file_type_translation.type_id')
                    ->where('mo_file_type_translation.deleted_at', '=', null)
                    ->where('mo_file_type.deleted_at', '=', null)
                    ->join('mo_language', 'mo_file_type_translation.language_id', 'mo_language.id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_file_type.id')->get();


                foreach ($tipos as $tipo) {
                    if ($request->get('lang') != '') {
                        $traducciones = FileTypeTranslation::where('language_id', '=', $idioma->id)->where('type_id', '=', $tipo->id)
                            ->select('name', 'description')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $tipo->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = FileTypeTranslation::where('type_id', '=', $tipo->id)
                                ->select('language_id', 'name', 'description')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $tipo->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['type'][] = $tipo;
                }

                $totales = $sub->count();
                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
            reportService($e, 'Tipos de archivos');
        }

        return response()->json($array, $array['error']);
    }



    /**
     * Muestra un tipo de archivo buscado por su id
     *
     * Para la consulta de tipos de archivos se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTypeId($id, Request $request)
    {
        $array['error'] = 200;


        try {

            $error = 0;

            if ($request->get('lang') != '') {

                $idioma_request = Str::upper($request->get('lang'));

                $idioma = Language::where('abbreviation', $idioma_request)->where('front',1)->first();
                if ($idioma) {
                    $id_idioma = $idioma->id;
                } else {
                    $error = 1;
                }
            }

            if ($error == 0) {
                $idiomas = Language::where('front',1)->get();

                $tipos = DB::connection('tenant')->table('mo_file_type')
                    ->select('mo_file_type.id')
                    ->join('mo_file_type_translation', 'mo_file_type.id', 'mo_file_type_translation.type_id')
                    ->where('mo_file_type_translation.deleted_at', '=', null)
                    ->where('mo_file_type.deleted_at', '=', null)
                    ->where('mo_file_type.id', '=', $id);

                $tipos = $tipos->groupBy('mo_file_type_translation.type_id')->first();

                $array['data'][0]['type'] = array();

                if ($tipos) {
                    if ($request->get('lang') != '') {
                        $traducciones = FileTypeTranslation::where('language_id', '=', $idioma->id)->where('type_id', '=', $tipos->id)
                            ->select('name', 'description')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $tipos->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = FileTypeTranslation::where('type_id', '=', $tipos->id)
                                ->select('name', 'description')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $tipos->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['type'][] = $tipos;
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][]['lang'][] = 'The lang is not exists';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de archivos');
        }

        return response()->json($array, $array['error']);
    }

}