<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;

use App\Models\tenant\File;
use App\Models\tenant\Settings;
use App\Models\tenant\Contact;
use App\Models\tenant\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class ContactController extends Controller
{


    /**
     * Función para la creación/modificación de un contacto.
     *
     * Para la creación de un contacto se realiza una petición POST.
     * Para la modificación de un contacto se realiza una petición PUT.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'email2' => 'email',
                'birthdate' => 'date|date_format:"Y-m-d"|before_or_equal:today',
                'language_id' => 'exists:tenant.mo_language,id,deleted_at,NULL|integer|min:0',
                'state_id' => 'integer|min:0|exists:tenant.mo_state,id,deleted_at,NULL',
                'country_id' => 'integer|min:0|required_with:state_id|exists:tenant.mo_country,id,deleted_at,NULL',
                'document_type_id' => 'required_with:number_document|exists:tenant.mo_document_type,id,deleted_at,NULL|integer|min:0',
                'number_document' => 'required_with:document_type_id',
                'sex_id' => 'exists:tenant.mo_sex,id,deleted_at,NULL|integer|min:0',
                'bloqued_login' => 'boolean',
                'worker' => 'boolean',
                'worker_type_id' => 'exists:tenant.mo_worker_type,id,deleted_at,NULL|integer|min:0',
                'roles' => 'array',
                'roles.*' => 'required|integer|exists:tenant.mo_role,id,deleted_at,NULL|min:0',
                'image_id' => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Método que valida que un estado pertenezca al país enviado

            if ($request->get('state_id') != '') {
                $state = $request->get('state_id');
                $country = $request->get('country_id');
                $sql_country = DB::connection('tenant')->Table('mo_state')
                    ->where('id', $state)
                    ->select('country_id');
                $datos_country = $sql_country->first();

                if ($datos_country) {
                    if ($datos_country->country_id != $country) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['state_id' => ['state_id not match with country_id']]);
                    }
                }


            }

            $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                ->where('mo_settings.name', 'heimdall_middleware')
                ->first()
                ->value;
            
            $rule_codes = [];

            if($heimdall_middleware == '1') {
                $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                $plans = websitePlanRules($website);

                $cantidad = 0;

                foreach($plans as $plan) {
                    if($plan->plan_id != null){
                        $rules = websiteRules($plan);
                        
                        $rules_contract = $rules->where('code', 'contract');

                        if($rules_contract) {
                            foreach($rules_contract as $rule) {
                                if($rule->unlimited == 1){
                                    $cantidad = -1;
                                } else {
                                    if($cantidad >= 0 && $cantidad < $rule->value) {
                                        $cantidad = $rule->value;
                                    }
                                }
                            }
                        }
                    } else {
                        if($plan->rule_code == 'contract') {
                            if($plan->unlimited == 1){
                                $cantidad = -1;
                            }else{
                                if($cantidad >= 0 && $cantidad < $plan->value){
                                    $cantidad = $plan->value;
                                }
                            }
                        }
                    }
                }

                if($cantidad == 0) {
                    $error = 1;
                    $rule_codes[] = 'contract';
                } else if ($cantidad > 0) {
                    
                    $total_contracts = DB::connection('tenant')->table('mo_contract')
                        ->whereNull('mo_contract.deleted_at');

                    if ($request->isMethod('put')) {
                        $total_contracts = $total_contracts->where('mo_contract.id', '!=', $request->get('id'));
                    }

                    if(count($total_contracts) >= $cantidad) {
                        $error = 1;
                        $rule_codes[] = 'contract';
                    }
                }
            }


            //Validación específica para creación de contacto

            if ($request->isMethod('post')) {

                $validator = \Validator::make($request->all(), [
                    'email' => 'unique:tenant.mo_contact,email,NULL,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                //Validación específica para actualización de contacto

            } else {
                $validator = \Validator::make($request->all(), [
                    'id' => 'integer|min:0|required',
                    'email' => 'unique:tenant.mo_contact,email,' . $request->get('id') . ',id,deleted_at,NULL',
                ]);
                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }else {
                    $contacto = Contact::find($request->get('id'));
                    if (!$contacto) {
                        $error = 2;
                    }
                }
            }
            // FIN Validación

            if ($error == 0) {

                if($request->get('country_id') != '') {
                    $country = Country::where('id', $request->get('country_id'))->first();
                } else {
                    $country = Country::orderBy('order','desc')->first();
                }
                //Generación array de datos de contacto
                $contact = [
                    'name' => ($request->get('name') != '') ? $request->get('name') : null,
                    'surname' => ($request->get('surname') != '') ? $request->get('surname') : null,
                    'document_type_id' => ($request->get('document_type_id') != '') ? $request->get('document_type_id') : null,
                    'number_document' => ($request->get('number_document') != '') ? $request->get('number_document') : null,
                    'sex_id' => ($request->get('sex_id') != '') ? $request->get('sex_id') : null,
                    'birthdate' => ($request->get('birthdate') != '') ? $request->get('birthdate') : null,
                    'email' => $request->get('email'),
                    'email2' => ($request->get('email2') != '') ? $request->get('email2') : null,
                    'bloqued_login' => ($request->get('bloqued_login') != '') ? $request->get('bloqued_login') : 0,
                    'language_id' => ($request->get('language_id') != '') ? $request->get('language_id') : null,
                    'telephone1' => ($request->get('telephone1') != '') ? $request->get('telephone1') : null,
                    'telephone2' => ($request->get('telephone2') != '') ? $request->get('telephone2') : null,
                    'telephone3' => ($request->get('telephone3') != '') ? $request->get('telephone3') : null,
                    'address' => ($request->get('address') != '') ? $request->get('address') : null,
                    'postal_code' => ($request->get('postal_code') != '') ? $request->get('postal_code') : null,
                    'city' => ($request->get('city') != '') ? $request->get('city') : null,
                    'state_id' => ($request->get('state_id') != '') ? $request->get('state_id') : null,
                    'country_id' => $country->id,
                    'business' => ($request->get('business') != '') ? $request->get('business') : null,
                    'telephone_business' => ($request->get('telephone_business') != '') ? $request->get('telephone_business') : null,
                    'worker' => ($request->get('worker') != '') ? $request->get('worker') : 0,
                    'worker_type_id' => ($request->get('worker_type_id') != '') ? $request->get('worker_type_id') : null,
                    'observations' => ($request->get('observations') != '') ? $request->get('observations') : null,
                    'image_id' => ($request->get('image_id') != '') ? $request->get('image_id') : null,
                ];


                //Creación o actualización del registro de contacto en BBDD
                if ($request->isMethod('put')) {

                    $contacto->update($contact);

                    //Borrado de identificadores pertenecientes al contacto

                } else {
                    $contacto = Contact::create($contact);

                }



            } elseif ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } elseif ($error == 2) {
                $array['error'] = 404;
                $array['error_description'] = 'Data not found';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contactos');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para mostrar los contactos.
     *
     * Para la consulta de contactos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'limit' => 'integer|min:0',
                'page' => 'integer|min:0',
                'state_id' => 'integer|min:0|exists:tenant.mo_state,id,deleted_at,NULL',
                'country_id' => 'integer|min:0|required_with:state_id|exists:tenant.mo_country,id,deleted_at,NULL',
                'language_id' => 'integer|min:0',
                'sex_id' => 'integer|min:0',
                'worker_type_id' => 'integer|min:0',
                'role_id' => 'integer|min:0',
                'bloqued_login' => 'boolean',
                'worker' => 'boolean',
                'document_type_id' => 'integer|min:0',
                'date_start' => 'date|required_with:date_end|date_format:"Y-m-d"',
                'date_end' => 'date|required_with:date_start|after_or_equal:date_start|date_format:"Y-m-d"',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Método que valida que un estado pertenezca al país enviado

            if ($request->get('state_id') != '') {
                $state = $request->get('state_id');
                $country = $request->get('country_id');
                $sql_country = DB::connection('tenant')->Table('mo_state')
                    ->where('id', $state)
                    ->select('country_id');
                $datos_country = $sql_country->first();

                if ($datos_country) {
                    if ($datos_country->country_id != $country) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['state_id' => ['state_id not match with country_id']]);
                    }
                }


            }

            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $contactos = DB::connection('tenant')->table('mo_contact')
                    ->select('mo_contact.id', 'mo_contact.name', 'mo_contact.surname', 'mo_contact.document_type_id', 'mo_contact.number_document', 'mo_contact.sex_id', 'mo_contact.birthdate', 'mo_contact.email', 'mo_contact.email2',
                        'mo_contact.faults_login', 'mo_contact.bloqued_login', 'mo_contact.bloqued_to', 'mo_contact.language_id', 'mo_contact.telephone1', 'mo_contact.telephone2', 'mo_contact.telephone3', 'mo_contact.address', 'mo_contact.postal_code',
                        'mo_contact.city', 'mo_contact.state_id', 'mo_contact.country_id', 'mo_contact.business', 'mo_contact.telephone_business', 'mo_contact.worker', 'mo_contact.worker_type_id',
                        'mo_contact.observations', 'mo_contact.image_id')
                    ->where('mo_contact.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_contact')
                    ->select('mo_contact.id')
                    ->where('mo_contact.deleted_at', '=', null);


                if ($request->get('country_id') != '') {
                    $contactos->where('mo_contact.country_id', $request->get('country_id'));
                    $sub->where('mo_contact.country_id', $request->get('country_id'));
                }

                if ($request->get('state_id') != '') {
                    $contactos->where('mo_contact.state_id', $request->get('state_id'));
                    $sub->where('mo_contact.state_id', $request->get('state_id'));
                }

                if ($request->get('language_id') != '') {
                    $contactos->where('mo_contact.language_id', $request->get('language_id'));
                    $sub->where('mo_contact.language_id', $request->get('language_id'));
                }

                if ($request->get('sex_id') != '') {
                    $contactos->where('mo_contact.sex_id', $request->get('sex_id'));
                    $sub->where('mo_contact.sex_id', $request->get('sex_id'));
                }

                if ($request->get('worker_type_id') != '') {
                    $contactos->where('mo_contact.worker_type_id', $request->get('worker_type_id'));
                    $sub->where('mo_contact.worker_type_id', $request->get('worker_type_id'));
                }

                if ($request->get('date_start') != '') {
                    $contactos->where('mo_contact.birthdate', '>=', $request->get('date_start'))
                        ->where('mo_contact.birthdate', '<=', $request->get('date_end'));
                    $sub->where('mo_contact.birthdate', '>=', $request->get('date_start'))
                        ->where('mo_contact.birthdate', '<=', $request->get('date_end'));
                }

                if ($request->get('role_id') != '') {
                    $contactos->where('mo_role.id', $request->get('role_id'));
                    $sub->where('mo_role.id', $request->get('role_id'));
                }

                if ($request->get('name') != '') {
                    $contactos->where(function ($query) use ($request) {
                        $query->where('mo_contact.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_contact.surname', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_contact.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_contact.surname', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('bloqued_login') != '') {
                    $contactos->where('mo_contact.bloqued_login', $request->get('bloqued_login'));
                    $sub->where('mo_contact.bloqued_login', $request->get('bloqued_login'));
                }

                if ($request->get('worker') != '') {
                    $contactos->where('mo_contact.worker', $request->get('worker'));
                    $sub->where('mo_contact.worker', $request->get('worker'));
                }

                if ($request->get('document_type_id') != '') {
                    $contactos->where('mo_contact.document_type_id', $request->get('document_type_id'));
                    $sub->where('mo_contact.document_type_id', $request->get('document_type_id'));
                }

                if ($request->get('number_document') != '') {
                    $contactos->where('mo_contact.number_document', $request->get('number_document'));
                    $sub->where('mo_contact.number_document', $request->get('number_document'));
                }

                if ($request->get('email') != '') {
                    $contactos->where(function ($query) use ($request) {
                        $query->where('mo_contact.email', 'Like', '%' . $request->get('email') . '%')->orWhere('mo_contact.email2', 'Like', '%' . $request->get('email') . '%');
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_contact.email', 'Like', '%' . $request->get('email') . '%')->orWhere('mo_contact.email2', 'Like', '%' . $request->get('email') . '%');
                    });
                }

                if ($request->get('postal_code') != '') {
                    $contactos->where('mo_contact.postal_code', $request->get('postal_code'));
                    $sub->where('mo_contact.postal_code', $request->get('postal_code'));
                }

                if ($request->get('business') != '') {
                    $contactos->where('mo_contact.business', 'Like', '%' . $request->get('business') . '%');
                    $sub->where('mo_contact.business', 'Like', '%' . $request->get('business') . '%');
                }

                if ($request->get('address') != '') {
                    $contactos->where(function ($query) use ($request) {
                        $query->where('mo_contact.address', 'Like', '%' . $request->get('address') . '%')
                            ->orWhere('mo_contact.city', 'Like', '%' . $request->get('address') . '%');
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_contact.address', 'Like', '%' . $request->get('address') . '%')
                            ->orWhere('mo_contact.city', 'Like', '%' . $request->get('address') . '%');
                    });
                }

                if ($request->get('telephone') != '') {
                    $contactos->where(function ($query) use ($request) {
                        $query->where('mo_contact.telephone1', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_contact.telephone2', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_contact.telephone3', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_contact.telephone_business', 'Like', '%' . $request->get('telephone') . '%');

                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_contact.telephone1', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_contact.telephone2', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_contact.telephone3', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_contact.telephone_business', 'Like', '%' . $request->get('telephone') . '%');

                    });
                }

                // Order
                $orden = 'name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'id';
                            break;
                        case 'name':
                            $orden = 'name';
                            break;
                        case 'surname':
                            $orden = 'surname';
                            break;
                        case 'birthdate':
                            $orden = 'birthdate';
                            break;
                        case 'email':
                            $orden = 'email';
                            break;
                        case 'number_document':
                            $orden = 'number_document';
                            break;
                        case 'postal_code':
                            $orden = 'postal_code';
                            break;
                        case 'business':
                            $orden = 'business';
                            break;
                        case 'worker_type_id':
                            $orden = 'worker_type_id';
                            break;
                        default:
                            $orden = 'name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $contactos = $contactos->groupBy('mo_contact.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $contactos = $contactos->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $contactos = $contactos->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_contact.id');
                $contactos_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $contactos_count->count();

                $array['data'] = array();

                if ($totales > 0) {

                    $array['data'][0]['contact'] = $contactos;

                }

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contactos');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Eliminación de un contacto
     *
     * Para la eliminación de contactos se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $validator = \Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_contact,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $contact) {
                        $contacto = Contact::where('id', $contact)->first();
                        $contacto->delete();
                    }

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contactos');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para la consulta de un contacto
     *
     * Para la consulta de un contacto se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $datos_contacto = DB::connection('tenant')->table('mo_contact')
                ->select('mo_contact.id', 'mo_contact.name', 'mo_contact.surname', 'mo_contact.document_type_id', 'mo_contact.number_document', 'mo_contact.sex_id', 'mo_contact.birthdate', 'mo_contact.email', 'mo_contact.email2',
                    'mo_contact.faults_login', 'mo_contact.bloqued_login', 'mo_contact.bloqued_to', 'mo_contact.language_id', 'mo_contact.telephone1', 'mo_contact.telephone2', 'mo_contact.telephone3', 'mo_contact.address', 'mo_contact.postal_code',
                    'mo_contact.city', 'mo_contact.state_id', 'mo_contact.country_id', 'mo_contact.business', 'mo_contact.telephone_business', 'mo_contact.worker', 'mo_contact.worker_type_id',
                    'mo_contact.observations', 'mo_contact.image_id')
                ->where('mo_contact.deleted_at', '=', null)
                ->where('mo_contact.id', '=', $id)
                ->first();


            $array['data'] = array();

            if ($datos_contacto) {

                $array['data'][0]['contact'][] = $datos_contacto;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contactos');
        }
        return response()->json($array, $array['error']);
    }

    public function showContactsPos(Request $request){
        $array['error'] = 200;
        try{
            DB::connection('tenant')->beginTransaction();

            $sql_contacts = DB::connection('tenant')->table('mo_contact')
                ->select(
                    DB::connection('tenant')->raw('null as id'),
                    'mo_contact.name',
                    'mo_contact.surname',
                    'mo_contact.document_type_id',
                    'mo_contact.number_document',
                    'mo_contact.sex_id',
                    'mo_contact.birthdate',
                    'mo_contact.email',
                    'mo_contact.email2',
                    'mo_contact.faults_login',
                    'mo_contact.bloqued_login',
                    'mo_contact.bloqued_to',
                    'mo_contact.language_id',
                    'mo_contact.telephone1',
                    'mo_contact.telephone2',
                    'mo_contact.telephone3',
                    'mo_contact.address',
                    'mo_contact.postal_code',
                    'mo_contact.city',
                    'mo_contact.state_id',
                    'mo_contact.country_id',
                    'mo_contact.business',
                    'mo_contact.telephone_business',
                    'mo_contact.worker',
                    'mo_contact.worker_type_id',
                    'mo_contact.observations',
                    'mo_contact.image_id'
                )->whereNull('mo_contact.deleted_at');

            $sql_users = DB::connection('tenant')->table('mo_user')
                ->select(
                'mo_user.id',
                'mo_user.name',
                'mo_user.surname',
                'mo_user.document_type_id',
                'mo_user.number_document',
                'mo_user.sex_id',
                'mo_user.birthdate',
                'mo_user.email',
                'mo_user.email2',
                'mo_user.faults_login',
                'mo_user.bloqued_login',
                'mo_user.bloqued_to',
                'mo_user.language_id',
                'mo_user.telephone1',
                'mo_user.telephone2',
                'mo_user.telephone3',
                'mo_user.address',
                'mo_user.postal_code',
                'mo_user.city',
                'mo_user.state_id',
                'mo_user.country_id',
                'mo_user.business',
                'mo_user.telephone_business',
                'mo_user.worker',
                'mo_user.worker_type_id',
                'mo_user.observations',
                'mo_user.image_id'
                )
                ->whereNull('mo_user.deleted_at');

            if ($request->get('email') != '') {
                $sql_contacts->where(function ($query) use ($request) {
                    $query->where('mo_contact.email', 'Like', '%' . $request->get('email') . '%')
                        ->orWhere('mo_contact.email2', 'Like', '%' . $request->get('email') . '%')
                        ->orWhere(DB::connection('tenant')->raw('concat(mo_contact.name, " ", mo_contact.surname)'),'Like', '%' . $request->get('email') . '%');
                });

                $sql_users->where(function ($query) use ($request) {
                    $query->where('mo_user.email', 'Like', '%' . $request->get('email') . '%')
                        ->orWhere('mo_user.email2', 'Like', '%' . $request->get('email') . '%')
                        ->orWhere(DB::connection('tenant')->raw('concat(mo_user.name, " ", mo_user.surname)'),'Like', '%' . $request->get('email') . '%');
                });
            }

            $array['data'] = array();

            $sql_contacts = $sql_contacts->union($sql_users);

            $datos = $sql_contacts->get();

            $array_contacts = array();
            foreach ($datos as $dato) {
                if(array_search($dato->email, array_column($array_contacts, 'email')) === false) {
                    $array_contacts[] = $dato;
                }
            }

            $array['data'][0]['user'] = $array_contacts;
            DB::connection('tenant')->commit();
        }catch (\Exception $e){
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            reportService($e, 'Contactos');
        }

        return response()->json($array, $array['error']);
    }


}