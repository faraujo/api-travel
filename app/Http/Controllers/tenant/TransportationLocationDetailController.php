<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use App\Models\tenant\TransportationLocationDetail;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransportationLocationDetailController extends Controller
{


    /**
     * Función para la creación de paradas para el servicio transporte
     *
     * Para la creación de paradas se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'transportation_location_id' => 'required|integer|min:0|exists:tenant.mo_transportation_location,id,deleted_at,NULL',
                'product_id' => 'integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
                'subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'name' => 'required',
                'code' => 'required',
                'date' => 'date|date_format:"Y-m-d"',
                'session' => 'date_format:H:i|required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                // Crear la parada
                $parada = TransportationLocationDetail::create([
                    'transportation_location_id' => $request->get('transportation_location_id') != '' ? $request->get('transportation_location_id') : null,
                    'product_id' => $request->get('product_id') != '' ? $request->get('product_id') : null,
                    'subchannel_id' => $request->get('subchannel_id') != '' ? $request->get('subchannel_id') : null,
                    'name' => $request->get('name') != '' ? $request->get('name') : null,
                    'code' => $request->get('code') != '' ? $request->get('code') : null,
                    'date' => $request->get('date') != '' ? $request->get('date') : null,
                    'session' => $request->get('session') != '' ? $request->get('session') : null,
                ]);
                // FIN crear paradas transporte

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Paradas Transporte');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de una parada.
     *
     * Para la eliminación de paradas se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_transportation_location_detail,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $transportation_location_detail_id) {
                        $transportation_location_detail = TransportationLocationDetail::where('id', $transportation_location_detail_id)->first();
                        // borrado de parada
                        $transportation_location_detail->delete();
                        //fin borrado

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Paradas Transporte');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una parada.
     *
     * Para la modificación de paradas se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'transportation_location_id' => 'required|integer|min:0|exists:tenant.mo_transportation_location,id,deleted_at,NULL',
                'product_id' => 'integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
                'subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'name' => 'required',
                'code' => 'required',
                'date' => 'date|date_format:"Y-m-d"',
                'session' => 'date_format:H:i|required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $parada = TransportationLocationDetail::find($request->get('id'));
                if (!$parada) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar parada
                    $parada->transportation_location_id = $request->get('transportation_location_id') != '' ? $request->get('transportation_location_id') : null;
                    $parada->product_id = $request->get('product_id') != '' ? $request->get('product_id') : null;
                    $parada->subchannel_id = $request->get('subchannel_id') != '' ? $request->get('subchannel_id') : null;
                    $parada->name = $request->get('name') != '' ? $request->get('name') : null;
                    $parada->code = $request->get('code') != '' ? $request->get('code') : null;
                    $parada->date = $request->get('date') != '' ? $request->get('date') : null;
                    $parada->session = $request->get('session') != '' ? $request->get('session') : null;

                    $parada->save();
                    //FIN actualizar la parada

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Paradas Transporte');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todas las paradas para el trasporte
     *
     * Para la consulta de tags se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
                'product_id' => 'integer|min:0',
                'subchannel_id' => 'integer|min:0',
                'transportation_location_id' => 'integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Se preparan las consultas
                $transportations = DB::connection('tenant')->table('mo_transportation_location_detail')
                    ->select('mo_transportation_location_detail.id', 'mo_transportation_location_detail.transportation_location_id',
                    'mo_transportation_location_detail.product_id', 'mo_transportation_location_detail.subchannel_id',
                    'mo_transportation_location_detail.name', 'mo_transportation_location_detail.code',
                    'mo_transportation_location_detail.date', 'mo_transportation_location_detail.session')
                    ->where('mo_transportation_location_detail.deleted_at', '=', null)
                    ->leftJoin('mo_transportation_location', function ($join) {
                        $join->on('mo_transportation_location.id', '=', 'mo_transportation_location_detail.transportation_location_id')->where('mo_transportation_location.deleted_at', null);
                    })
                    ->leftJoin('mo_product', function ($join) {
                        $join->on('mo_product.id', '=', 'mo_transportation_location_detail.product_id')->where('mo_product.deleted_at', null);
                    })
                    ->leftjoin('mo_subchannel', function ($join) {
                        $join->on('mo_transportation_location_detail.subchannel_id', '=', 'mo_subchannel.id')->where('mo_subchannel.deleted_at', null);
                    });


                $sub = DB::connection('tenant')->table('mo_transportation_location_detail')
                    ->select('mo_transportation_location_detail.id')
                    ->where('mo_transportation_location_detail.deleted_at', '=', null)
                    ->leftJoin('mo_transportation_location', function ($join) {
                        $join->on('mo_transportation_location.id', '=', 'mo_transportation_location_detail.transportation_location_id')->where('mo_transportation_location.deleted_at', null);
                    })
                    ->leftJoin('mo_product', function ($join) {
                        $join->on('mo_product.id', '=', 'mo_transportation_location_detail.product_id')->where('mo_product.deleted_at', null);
                    })
                    ->leftjoin('mo_subchannel', function ($join) {
                        $join->on('mo_transportation_location_detail.subchannel_id', '=', 'mo_subchannel.id')->where('mo_subchannel.deleted_at', null);
                    });

                // Recogida de datos enviados por el usuario y establecimiento de filtros
                if ($request->get('name') != '') {

                    $transportations->Where(function ($query) use ($request) {
                        $query->where('mo_transportation_location_detail.name', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_transportation_location_detail.name', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('product_id') != '') {
                    $transportations->where('mo_transportation_location_detail.product_id', '=', $request->get('product_id'))->where('mo_product.id', $request->get('product_id'));
                    $sub->where('mo_transportation_location_detail.product_id', '=', $request->get('product_id'))->where('mo_product.id', $request->get('product_id'));
                }

                if ($request->get('subchannel_id') != '') {
                    $transportations->where('mo_transportation_location_detail.subchannel_id', '=', $request->get('subchannel_id'))->where('mo_subchannel.id', $request->get('subchannel_id'));
                    $sub->where('mo_transportation_location_detail.subchannel_id', '=', $request->get('subchannel_id'))->where('mo_subchannel.id', $request->get('subchannel_id'));
                }

                if ($request->get('transportation_location_id') != '') {
                    $transportations->where('mo_transportation_location_detail.transportation_location_id', '=', $request->get('transportation_location_id'))->where('mo_transportation_location.id', $request->get('transportation_location_id'));
                    $sub->where('mo_transportation_location_detail.transportation_location_id', '=', $request->get('transportation_location_id'))->where('mo_transportation_location.id', $request->get('transportation_location_id'));
                }

                // Order
                $orden = 'mo_transportation_location_detail.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_transportation_location_detail.name';
                            break;
                        case 'code':
                            $orden = 'mo_transportation_location_detail.code';
                            break;
                        case 'id':
                            $orden = 'mo_transportation_location_detail.id';
                            break;
                        default:
                            $orden = 'mo_transportation_location_detail.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $transportations->groupBy('mo_transportation_location_detail.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $transportations = $transportations->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $transportations = $transportations->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_transportation_location_detail.id');
                $transportations_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $transportations_count->count();

                $array['data'] = array();
                foreach ($transportations as $transportation) {
                    $array['data'][0]['transportationlocationstop'][] = $transportation;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Paradas Transporte');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra una parada buscada por su id
     *
     * Para la consulta de una parada se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $datos_transportation = DB::connection('tenant')->table('mo_transportation_location_detail')
            ->select(
                'mo_transportation_location_detail.id',
                'mo_transportation_location_detail.transportation_location_id',
                'mo_transportation_location_detail.product_id',
                'mo_transportation_location_detail.subchannel_id',
                'mo_transportation_location_detail.name',
                'mo_transportation_location_detail.code',
                'mo_transportation_location_detail.date',
                'mo_transportation_location_detail.session'
            )
            ->where('mo_transportation_location_detail.deleted_at', '=', null)
            ->where('mo_transportation_location_detail.id', '=', $id);

            $datos_transportation = $datos_transportation->get();

            $array['data'] = array();
            foreach ($datos_transportation as $transportation) {
                $arraytransportation = array();
                $arraytransportation['id'] = $transportation->id;
                $arraytransportation['transportation_location_id'] = $transportation->transportation_location_id;
                $arraytransportation['product_id'] = $transportation->product_id;
                $arraytransportation['subchannel_id'] = $transportation->subchannel_id;
                $arraytransportation['name'] = $transportation->name;
                $arraytransportation['code'] = $transportation->code;
                $arraytransportation['date'] = $transportation->date;
                $arraytransportation['session'] = Carbon::parse($transportation->session)->format('H:i');

                $array['data'][0]['transportationlocationstop'][] = $arraytransportation;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Paradas Transporte');
        }

        return response()->json($array, $array['error']);

    }
   
}