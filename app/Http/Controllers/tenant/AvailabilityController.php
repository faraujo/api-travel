<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Availability;
use App\Exceptions\Handler;
use App\Models\tenant\Language;
use App\Models\tenant\Product;
use App\Models\tenant\ProductTranslation;
use App\Models\tenant\ServiceTranslation;
use App\Models\tenant\Settings;
use Carbon\Carbon;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AvailabilityController extends Controller
{

    /**
     * Función para la creación de un inventario
     *
     * Para la creación de un inventario se realizara una petición POST.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {

        set_time_limit(0);

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [

                'product_id' => 'numeric|min:0|exists:tenant.mo_product,id,deleted_at,NULL|required',
                'date_start' => 'date|required|after_or_equal:today|date_format:"Y-m-d"',
                'date_end' => 'date|required|after_or_equal:date_start|date_format:"Y-m-d"',
                'locations' => 'array|required',

                'locations.*.location_id' => 'integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL|required',
                'locations.*.quota' => 'integer|min:0|required_without_all:locations.*.avail,locations.*.closed,locations.*.unlimited,locations.*.availabilities,locations.*.services',
                'locations.*.avail' => 'integer|required_without_all:locations.*.quota,locations.*.closed,locations.*.unlimited,locations.*.availabilities,locations.*.services',
                'locations.*.overbooking' => 'integer|min:0',
                'locations.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.overbooking',
                'locations.*.closed' => 'boolean|required_without_all:locations.*.quota,locations.*.avail,locations.*.unlimited,locations.*.availabilities,locations.*.services',
                'locations.*.unlimited' => 'boolean|required_without_all:locations.*.quota,locations.*.avail,locations.*.closed,locations.*.availabilities,locations.*.services',

                'locations.*.availabilities' => 'array|required_without_all:locations.*.quota,locations.*.avail,locations.*.closed,locations.*.unlimited,locations.*.services',

                'locations.*.availabilities.*.session' => 'date_format:H:i|required',
                'locations.*.availabilities.*.quota' => 'integer|min:0|required_without_all:locations.*.availabilities.*.avail,locations.*.availabilities.*.closed,locations.*.availabilities.*.unlimited',
                'locations.*.availabilities.*.avail' => 'integer|required_without_all:locations.*.availabilities.*.quota,locations.*.availabilities.*.closed,locations.*.availabilities.*.unlimited',
                'locations.*.availabilities.*.overbooking' => 'integer|min:0',
                'locations.*.availabilities.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.availabilities.*.overbooking',
                'locations.*.availabilities.*.closed' => 'boolean|required_without_all:locations.*.availabilities.*.quota,locations.*.availabilities.*.avail,locations.*.availabilities.*.unlimited',
                'locations.*.availabilities.*.unlimited' => 'boolean|required_without_all:locations.*.availabilities.*.quota,locations.*.availabilities.*.avail,locations.*.availabilities.*.closed',


                'locations.*.services' => 'array|required_without_all:locations.*.quota,locations.*.avail,locations.*.closed,locations.*.unlimited,locations.*.availabilities',

                'locations.*.services.*.service_id' => 'integer|min:0|exists:tenant.mo_service,id,deleted_at,NULL|required_with:locations.*.services',
                'locations.*.services.*.quota' => 'integer|min:0|required_without_all:locations.*.services.*.avail,locations.*.services.*.closed,locations.*.services.*.unlimited,locations.*.services.*.availabilities',
                'locations.*.services.*.avail' => 'integer|required_without_all:locations.*.services.*.quota,locations.*.services.*.closed,locations.*.services.*.unlimited,locations.*.services.*.availabilities',
                'locations.*.services.*.overbooking' => 'integer|min:0',
                'locations.*.services.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.services.*.overbooking',
                'locations.*.services.*.closed' => 'boolean|required_without_all:locations.*.services.*.quota,locations.*.services.*.avail,locations.*.services.*.unlimited,locations.*.services.*.availabilities',
                'locations.*.services.*.unlimited' => 'boolean|required_without_all:locations.*.services.*.quota,locations.*.services.*.avail,locations.*.services.*.closed,locations.*.services.*.availabilities',

                'locations.*.services.*.availabilities' => 'array|required_without_all:locations.*.services.*.quota,locations.*.services.*.avail,locations.*.services.*.closed,locations.*.services.*.unlimited',

                'locations.*.services.*.availabilities.*.session' => 'date_format:H:i|required',
                'locations.*.services.*.availabilities.*.quota' => 'integer|min:0|required_without_all:locations.*.services.*.availabilities.*.avail,locations.*.services.*.availabilities.*.closed,locations.*.services.*.availabilities.*.unlimited',
                'locations.*.services.*.availabilities.*.avail' => 'integer|required_without_all:locations.*.services.*.availabilities.*.quota,locations.*.services.*.availabilities.*.closed,locations.*.services.*.availabilities.*.unlimited',
                'locations.*.services.*.availabilities.*.overbooking' => 'integer|min:0',
                'locations.*.services.*.availabilities.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.services.*.availabilities.*.overbooking',
                'locations.*.services.*.availabilities.*.closed' => 'boolean|required_without_all:locations.*.services.*.availabilities.*.quota,locations.*.services.*.availabilities.*.avail,locations.*.services.*.availabilities.*.unlimited',
                'locations.*.services.*.availabilities.*.unlimited' => 'boolean|required_without_all:locations.*.services.*.availabilities.*.quota,locations.*.services.*.availabilities.*.avail,locations.*.services.*.availabilities.*.closed',

            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin de validación

            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {  //Si no se a producido ningun error continua

                foreach ($request->get('locations') as $location) {
                    //Inicio de consulta con filtros de búsqueda
                    $availabilities = Availability::where('product_id', '=', $request->get('product_id'))
                        ->where('location_id', '=', $location['location_id']);

                    // Crea o actualiza el cupo general
                    $availabilities_clone = clone $availabilities;
                    $avail = $availabilities_clone->where('service_id', '=', null)->where('date', '=', null)->where('session', '=', null)->first();


                    if ($avail) {  //Si ya existe cupo general lo actualiza
                        //Actualización con los campos enviados
                        if (isset($location['quota'])) {
                            if (!isset($location['avail'])) {
                                $disponible = $location['quota'] - $avail->quota + $avail->avail;
                            } else {
                                $disponible = $location['avail'];
                            }
                            $avail->quota = $location['quota'];
                            $avail->avail = $disponible;
                        }

                        if (isset($location['overbooking'])) {
                            $avail->overbooking = $location['overbooking'];
                            $avail->overbooking_unit = $location['overbooking_unit'];
                        }

                        if (isset($location['closed'])) {
                            $avail->closed = $location['closed'];
                        }

                        if (isset($location['unlimited'])) {
                            $avail->unlimited = $location['unlimited'];
                        }
                        $avail->save();

                    } elseif (isset($location['avail']) || isset($location['quota']) || isset($location['closed']) || isset($location['unlimited'])) { // si no existe cupo general lo crea con lo campos enviados

                        if (isset($location['avail'])) {
                            $disponible = $location['avail'];
                        } elseif (isset($location['quota'])) {
                            $disponible = $location['quota'];
                        } else {
                            $disponible = 0;
                        }

                        Availability::create([
                            'date' => null,
                            'session' => null,
                            'product_id' => $request->get('product_id'),
                            'service_id' => null,
                            'location_id' => $location['location_id'],
                            'quota' => isset($location['quota']) ? $location['quota'] : 0,
                            'avail' => $disponible,
                            'overbooking' => isset($location['overbooking']) ? $location['overbooking'] : 0,
                            'overbooking_unit' => isset($location['overbooking_unit']) ? $location['overbooking_unit'] : 'u',
                            'unlimited' => isset($location['unlimited']) ? $location['unlimited'] : 0,
                            'closed' => isset($location['closed']) ? $location['closed'] : 0,
                        ]);

                    }


                    if (isset($location['availabilities'])) { //Crea o actualiza cupos para producto

                        foreach ($location['availabilities'] as $availability) {

                            $availabilities_clone = clone $availabilities;
                            $availabilities_clone = $availabilities_clone->where('service_id', '=', null)->where('session', '=', $availability['session']);

                            //Formateo de fechas para poder manipularlas
                            $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'));
                            $fecha = Carbon::createFromFormat('Y-m-d', $request->get('date_start'));

                            //Recorre los dias comprendidos entre la fecha de inicio y la de fin
                            while ($fecha <= $fecha_fin) {

                                $availabilities_clone2 = clone $availabilities_clone;
                                $avail = $availabilities_clone2->where('date', '=', $fecha->format('Y-m-d'))->first();


                                if ($avail) {
                                    //Si existen registros los actualiza
                                    //Actualización de los campos enviados
                                    if (isset($availability['quota'])) {
                                        if (!isset($availability['avail'])) {
                                            $disponible = $availability['quota'] - $avail->quota + $avail->avail;
                                        } else {
                                            $disponible = $availability['avail'];
                                        }
                                        $avail->quota = $availability['quota'];
                                        $avail->avail = $disponible;
                                    }

                                    if (isset($availability['overbooking'])) {
                                        $avail->overbooking = $availability['overbooking'];
                                        $avail->overbooking_unit = $availability['overbooking_unit'];
                                    }

                                    if (isset($availability['closed'])) {
                                        $avail->closed = $availability['closed'];
                                    }

                                    if (isset($availability['unlimited'])) {
                                        $avail->unlimited = $availability['unlimited'];
                                    }

                                    $avail->save();


                                } else {
                                    //Si no existen registros se crean
                                    $disponible = 0;
                                    if (isset($availability['avail'])) {
                                        $disponible = $availability['avail'];
                                    } elseif (isset($availability['quota'])) {
                                        $disponible = $availability['quota'];
                                    } else {
                                        $disponible = 0;
                                    }

                                    Availability::create([
                                        'date' => $fecha->format('Y-m-d'),
                                        'session' => $availability['session'],
                                        'product_id' => $request->get('product_id'),
                                        'service_id' => null,
                                        'location_id' => $location['location_id'],
                                        'quota' => isset($availability['quota']) ? $availability['quota'] : 0,
                                        'avail' => $disponible,
                                        'overbooking' => isset($availability['overbooking']) ? $availability['overbooking'] : 0,
                                        'overbooking_unit' => isset($availability['overbooking_unit']) ? $availability['overbooking_unit'] : 'u',
                                        'unlimited' => isset($availability['unlimited']) ? $availability['unlimited'] : 0,
                                        'closed' => isset($availability['closed']) ? $availability['closed'] : 0,
                                    ]);
                                }
                                //Se añade 1 dia a la fecha
                                $fecha = $fecha->addDay(1);

                            }
                        }
                    }

                    if (isset($location['services'])) { // Crea o actualiza cupos para servicios

                        foreach ($location['services'] as $service) {

                            $availabilities_clone = clone $availabilities;

                            $avail = $availabilities_clone->where('service_id', '=', $service['service_id'])->where('date', '=', null)->where('session', '=', null)->first();;


                            if ($avail) {  //Si ya existe cupo general lo actualiza

                                //Actualización con los campos enviados
                                if (isset($service['quota'])) {
                                    if (!isset($service['avail'])) {
                                        $disponible = $service['quota'] - $avail->quota + $avail->avail;
                                    } else {
                                        $disponible = $service['avail'];
                                    }
                                    $avail->quota = $service['quota'];
                                    $avail->avail = $disponible;
                                }

                                if (isset($service['overbooking'])) {
                                    $avail->overbooking = $service['overbooking'];
                                    $avail->overbooking_unit = $service['overbooking_unit'];
                                }

                                if (isset($service['closed'])) {
                                    $avail->closed = $service['closed'];
                                }

                                if (isset($service['unlimited'])) {
                                    $avail->unlimited = $service['unlimited'];
                                }

                                $avail->save();


                            } elseif (isset($service['avail']) || isset($service['quota']) || isset($service['closed']) || isset($service['unlimited'])) { // si no existe cupo general lo crea con los campos enviados

                                //Si no existen registros se crean
                                $disponible = 0;
                                if (isset($service['avail'])) {
                                    $disponible = $service['avail'];
                                } elseif (isset($service['quota'])) {
                                    $disponible = $service['quota'];
                                } else {
                                    $disponible = 0;
                                }

                                Availability::create([
                                    'date' => null,
                                    'session' => null,
                                    'product_id' => $request->get('product_id'),
                                    'service_id' => $service['service_id'],
                                    'location_id' => $location['location_id'],
                                    'quota' => isset($service['quota']) ? $service['quota'] : 0,
                                    'avail' => $disponible,
                                    'overbooking' => isset($service['overbooking']) ? $service['overbooking'] : 0,
                                    'overbooking_unit' => isset($service['overbooking_unit']) ? $service['overbooking_unit'] : 'u',
                                    'unlimited' => isset($service['unlimited']) ? $service['unlimited'] : 0,
                                    'closed' => isset($service['closed']) ? $service['closed'] : 0,
                                ]);

                            }


                            if (isset($service['availabilities'])) { //Crea o actualiza cupos para sesiones de servicios

                                foreach ($service['availabilities'] as $availability) {

                                    $availabilities_clone = clone $availabilities;
                                    $availabilities_clone2 = $availabilities_clone->where('service_id', '=', $service['service_id'])->where('session', '=', $availability['session']);

                                    //Formateo de fechas para poder manipularlas
                                    $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'));
                                    $fecha = Carbon::createFromFormat('Y-m-d', $request->get('date_start'));

                                    //Recorre los días comprendidos entre la fecha de inicio y la de fin
                                    while ($fecha <= $fecha_fin) {

                                        $availabilities_clone3 = clone $availabilities_clone2;
                                        $avail = $availabilities_clone3->where('date', '=', $fecha->format('Y-m-d'))->first();


                                        if ($avail) {
                                            //Si existen registros los actualiza
                                            //Actualización de los campos enviados

                                            if (isset($availability['quota'])) {
                                                if (!isset($availability['avail'])) {
                                                    $disponible = $availability['quota'] - $avail->quota + $avail->avail;
                                                } else {
                                                    $disponible = $availability['avail'];
                                                }
                                                $avail->quota = $availability['quota'];
                                                $avail->avail = $disponible;
                                            }

                                            if (isset($availability['overbooking'])) {
                                                $avail->overbooking = $availability['overbooking'];
                                                $avail->overbooking_unit = $availability['overbooking_unit'];
                                            }

                                            if (isset($availability['closed'])) {
                                                $avail->closed = $availability['closed'];
                                            }

                                            if (isset($availability['unlimited'])) {
                                                $avail->unlimited = $availability['unlimited'];
                                            }

                                            $avail->save();


                                        } else {

                                            //Si no existen registros se crean
                                            $disponible = 0;
                                            if (isset($availability['avail'])) {
                                                $disponible = $availability['avail'];
                                            } elseif (isset($availability['quota'])) {
                                                $disponible = $availability['quota'];
                                            } else {
                                                $disponible = 0;
                                            }

                                            Availability::create([
                                                'date' => $fecha->format('Y-m-d'),
                                                'session' => $availability['session'],
                                                'product_id' => $request->get('product_id'),
                                                'service_id' => $service['service_id'],
                                                'location_id' => $location['location_id'],
                                                'quota' => isset($availability['quota']) ? $availability['quota'] : 0,
                                                'avail' => $disponible,
                                                'overbooking' => isset($availability['overbooking']) ? $availability['overbooking'] : 0,
                                                'overbooking_unit' => isset($availability['overbooking_unit']) ? $availability['overbooking_unit'] : 'u',
                                                'unlimited' => isset($availability['unlimited']) ? $availability['unlimited'] : 0,
                                                'closed' => isset($availability['closed']) ? $availability['closed'] : 0,
                                            ]);


                                        }
                                        //Se añade 1 dia a la fecha
                                        $fecha = $fecha->addDay(1);

                                    }
                                }
                            }
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            //Lanza mensaje de error en caso de producirse alguna excepcion
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Disponibilidad');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para borrado de inventarios, cupos y cierres
     *
     * Para la eliminación de una disponibilidad de un producto se realiza una petición DELETE.
     * Si la operación no produce error se devuelve la variable “error” con valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'date_start' => 'date|required_with:date_end,session|date_format:"Y-m-d"',
                'date_end' => 'date|required_with:date_start,session|after_or_equal:date_start|date_format:"Y-m-d"',
                'product_id' => 'numeric|required',
                'service_id' => 'numeric',
                'session' => 'date_format:H:i',
                'location_id' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                // Disponibilidad que se quiere eliminar
                $validator = Validator::make($request->all(), [
                    'product_id' => 'exists:tenant.mo_product,id,deleted_at,NULL',
                    'service_id' => 'exists:tenant.mo_service,id,deleted_at,NULL',
                ]);
                if ($validator->fails()) {
                    $error = 1;
                }
                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    $avail = Availability::where('product_id', $request->get('product_id'));
                    if ($request->get('service_id') != '') {
                        $avail->where('service_id', $request->get('service_id'));
                    }

                    // Si el producto o servicio existe, se verifica si se enviaron rangos de fechas o sesiones o location
                    if ($request->get('date_start') != '') {
                        $avail->where('date', '>=', $request->get('date_start'))
                            ->where('date', '<=', $request->get('date_end'));
                    }
                    if ($request->get('session') != '') {
                        $avail->where('session', $request->get('session'));
                    }

                    if ($request->get('location_id') != '') {
                        $avail->where('location_id', $request->get('location_id'));
                    }

                    if ($error == 0) {
                        $avail->delete();
                    }
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Disponibilidad');
        }

        return response()->json($array, $array['error']);

    }


    /**
     * Función que muestra los inventarios, cupos y cierres para un rango de un mes
     *
     * Para la consulta del inventario productos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $idiomas = Language::where('front',1)->get();

            $validator = Validator::make($request->all(), [
                'month' => 'required|integer|min:0,max:12',
                'year' => 'required|integer|min:0',
                'type_id' => 'integer|min:0',
                'page' => 'numeric',
                'limit' => 'numeric',
                'location_id' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }


            //Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;
                    $rule_codes = [];

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_availability = $rules->where('code', 'product_availability');

                            if($rules_availability) {
                                foreach($rules_availability as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'product_availability') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'product_availability';
                    } else if ($cantidad > 0) {
                        $total_availabilties = Availability::get();

                        if(count($total_availabilties) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'product_availability';
                        }
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    $sql_productos = DB::connection('tenant')->table('mo_product')
                        ->select(['mo_product.id', 'mo_product.code'])
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation', 'mo_product.id', 'mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->join('mo_product_type', 'mo_product.type_id', 'mo_product_type.id')
                        ->whereNull('mo_product_type.deleted_at')
                        ->join('mo_product_location', 'mo_product.id', 'mo_product_location.product_id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->join('mo_location', 'mo_product_location.location_id', 'mo_location.id')
                        ->where('mo_product.type_id', '!=', 1);

                    $sub = DB::connection('tenant')->table('mo_product')
                        ->select(['mo_product.id'])
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation', 'mo_product.id', 'mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->join('mo_product_type', 'mo_product.type_id', 'mo_product_type.id')
                        ->whereNull('mo_product_type.deleted_at')
                        ->join('mo_product_location', 'mo_product.id', 'mo_product_location.product_id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->join('mo_location', 'mo_product_location.location_id', 'mo_location.id')
                        ->where('mo_product.type_id', '!=', 1);

                    //Consultas de disponibilidades por fecha y sesión con closed 1
                    $sql_disponibilidad = DB::connection('tenant')->table('mo_availability')
                        ->select(DB::connection('tenant')->raw('SUM(mo_availability.avail) as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                        ->whereNull('mo_availability.deleted_at')
                        ->orderBy('mo_availability.date')
                        ->groupBy('mo_availability.date')
                        ->whereNull('mo_availability.service_id')
                        ->where('mo_availability.closed', '=', 0);

                    //Consultas de disponibildades por fecha y sesión con closed 0
                    $sql_disponibilidad_cerrado = DB::connection('tenant')->table('mo_availability')
                        ->select(DB::connection('tenant')->raw('0 as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                        ->whereNull('mo_availability.deleted_at')
                        ->orderBy('mo_availability.date')
                        ->groupBy('mo_availability.date')
                        ->whereNull('mo_availability.service_id')
                        ->where('mo_availability.closed', '=', 1);

                    //Consultas de disponibildades de cupo general con closed 0
                    $sql_disponibilidad_general = DB::connection('tenant')->table('mo_availability')
                        ->select(DB::connection('tenant')->raw('SUM(mo_availability.avail) as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                        ->whereNull('mo_availability.deleted_at')
                        ->whereNull('mo_availability.date')
                        ->whereNull('mo_availability.service_id')
                        ->where('mo_availability.closed', '=', 0)
                        ->groupBy('date');

                    //Consultas de disponibildades de cupo general con closed 1
                    $sql_disponibilidad_general_cerrado = DB::connection('tenant')->table('mo_availability')
                        ->select(DB::connection('tenant')->raw('SUM(mo_availability.avail) as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                        ->whereNull('mo_availability.deleted_at')
                        ->whereNull('mo_availability.date')
                        ->whereNull('mo_availability.service_id')
                        ->where('mo_availability.closed', '=', 1)
                        ->groupBy('date');


                    if ($request->get('name') != '') {
                        $sql_productos->where('mo_product_translation.name', 'Like', '%' . $request->get('name') . '%');
                        $sub->where('mo_product_translation.name', 'Like', '%' . $request->get('name') . '%');
                    }

                    if ($request->get('type_id') != '') {
                        $sql_productos->where('mo_product.type_id', '=', $request->get('type_id'));
                        $sub->where('mo_product.type_id', '=', $request->get('type_id'));
                    }

                    if ($request->get('location_id') != '') {
                        $sql_productos->where('mo_location.id', '=', $request->get('location_id'));
                        $sub->where('mo_location.id', '=', $request->get('location_id'));
                        $sql_disponibilidad->where('mo_availability.location_id', '=', $request->get('location_id'));
                        $sql_disponibilidad_cerrado->where('mo_availability.location_id', '=', $request->get('location_id'));
                        $sql_disponibilidad_general->where('mo_availability.location_id', '=', $request->get('location_id'));
                        $sql_disponibilidad_general_cerrado->where('mo_availability.location_id', '=', $request->get('location_id'));
                    }

                    //Se establece manualmente el día para evitar problemas al crear fechas de meses con 30 o menos días ya que se crea el día actual del mes indicado
                    $fecha_inicio = Carbon::create($request->get('year'), $request->get('month'), 1)->firstOfMonth();
                    $fecha_fin = Carbon::create($request->get('year'), $request->get('month'), 1)->lastOfMonth();


                    $sql_disponibilidad->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                            $query->where('mo_availability.date', '>=', $fecha_inicio->format('Y-m-d'))
                                ->where('mo_availability.date', '<=', $fecha_fin->format('Y-m-d'));
                        });
                    })->whereNotNull('mo_availability.date');

                    $sql_disponibilidad_cerrado->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                            $query->where('mo_availability.date', '>=', $fecha_inicio->format('Y-m-d'))
                                ->where('mo_availability.date', '<=', $fecha_fin->format('Y-m-d'));
                        });
                    })->whereNotNull('mo_availability.date');


                    // Order
                    $orden = 'mo_product_translation.name';
                    $request_order = $request->get('order');
                    if ($request_order != '') {
                        switch ($request->get('order')) {
                            case 'name':
                                $orden = 'mo_product_translation.name';
                                break;
                            default:
                                $orden = 'mo_product_translation.name';
                                break;
                        }
                    }

                    // FIN Order

                    // Order_way
                    $sentido = 'asc';
                    $request_order_way = $request->get('order_way');
                    if ($request_order_way != '') {
                        switch ($request->get('order_way')) {
                            case 'asc':
                                $sentido = 'asc';
                                break;
                            case 'desc':
                                $sentido = 'desc';
                                break;
                            default:
                                $sentido = 'asc';
                                break;
                        }
                    }
                    // FIN Order_way


                    $sql_productos->groupBy('mo_product.id')->orderBy($orden, $sentido);
                    // Paginación según filtros y ejecución de la consulta
                    if ($request->get('limit') != '0') {

                        $settings = Settings::where('name', '=', 'limit_registers')->first();
                        $limite = $settings->value;

                        if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                            $limite = $request->get('limit');
                        }


                        $inicio = 0;
                        if ($request->get('page') != '') {
                            $inicio = $request->get('page');
                        }

                        $datos_productos = $sql_productos->forPage($inicio, $limite)->get();

                        //si filtro limit = 0 se obtienen todos los resultados
                    } else {

                        $datos_productos = $sql_productos->get();

                    }

                    //Fin de paginación

                    $sub->groupBy('mo_product.id');
                    $avail_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                        ->mergeBindings($sub);


                    $totales = $avail_count->count();

                    $array['data'] = array();
                    foreach ($datos_productos as $producto) {


                        //Clonación de consultas de disponibilidades por fecha y sesión
                        $sql_disponibilidad_clone = clone $sql_disponibilidad;
                        $sql_disponibilidad_cerrado_clone = clone $sql_disponibilidad_cerrado;

                        //Condición de consultas de disponibilades de por fecha y sesión de product_id
                        $sql_disponibilidad_clone->where('mo_availability.product_id', '=', $producto->id);
                        $sql_disponibilidad_cerrado_clone->where('mo_availability.product_id', '=', $producto->id);

                        //Clonación de consultas de disponibilades de cupos generales
                        $sql_disponibilidad_general_clone = clone $sql_disponibilidad_general;
                        $sql_disponibilidad_general_cerrado_clone = clone $sql_disponibilidad_general_cerrado;

                        //Condición de consultas de disponibilades de cupos generales de product_id
                        $sql_disponibilidad_general_clone->where('mo_availability.product_id', '=', $producto->id);
                        $sql_disponibilidad_general_cerrado_clone->where('mo_availability.product_id', '=', $producto->id);

                        //Al realizar los union toma como preferencia los resultados de la query que comprueba los cupos NO cerrados
                        //Se mantiene dado que se considera como preferente estos frente a los cupos con estado cerrado
                        //Tanto en los cupos por fecha y sesión como los cupos generales

                        //Unión de consultas de disponiblades por fecha y sesión
                        $sql_disponibilidad_completa = $sql_disponibilidad_clone->union($sql_disponibilidad_cerrado_clone);

                        //Unión de consultas de disponibilidades de cupo general
                        $sql_disponibilidad_completa_general = $sql_disponibilidad_general_clone->union($sql_disponibilidad_general_cerrado_clone);


                        //Subconsulta de disponibilidades por fecha y sesión
                        $datos_disponibilidad = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_disponibilidad_completa->toSql()}) as sub"))
                            ->select('*')
                            ->groupBy('date')
                            ->mergeBindings($sql_disponibilidad_completa)->get();

                        //Subconsulta de disponibilidades de cupo general
                        $datos_disponibilidad_general = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_disponibilidad_completa_general->toSql()}) as sub"))
                            ->select('*')
                            ->groupBy('date')
                            ->mergeBindings($sql_disponibilidad_completa_general)->first();

                        $array_disponibilidades = array();

                        $fecha = '';
                        $fecha = Carbon::createFromFormat('Y-m-d', $fecha_inicio->format('Y-m-d'))->startOfDay();
                        while ($fecha <= $fecha_fin) {
                            foreach ($datos_disponibilidad as $avail) {
                                if ($avail->date == $fecha->format('Y-m-d')) {
                                    //Si en array de cupos por fecha y sesión existe registro para la fecha recorrida se añade al array de devolución
                                    //Se castea a entero para conservar formato de datos devueltos
                                    $array_disponibilidades[] = ['date' => $avail->date, 'avail' => (int)$avail->total_avail, 'closed' => $avail->closed, 'unlimited' => $avail->unlimited];
                                }
                            }
                            
                            if ($datos_disponibilidad_general && !in_array($fecha->format('Y-m-d'), array_column($array_disponibilidades, 'date'))) {
                                //Si no existe registro por fecha y sesión para la fecha recorrida se comprueba el cupo general del producto
                                $dato_avail = 0;
                                if ($datos_disponibilidad_general->closed != 1) {
                                    $dato_avail = $datos_disponibilidad_general->total_avail != null ? $datos_disponibilidad_general->total_avail : 0;
                                }
                                //Se castea a entero para conservar formato de datos devueltos
                                $array_disponibilidades[] = [
                                    'date' => $fecha->format('Y-m-d'),
                                    'avail' => (int)$dato_avail,
                                    'closed' => $datos_disponibilidad_general->closed,
                                    'unlimited' => $datos_disponibilidad_general->unlimited,
                                ];
                            } elseif (!in_array($fecha->format('Y-m-d'), array_column($array_disponibilidades, 'date')) && !$datos_disponibilidad_general) {
                                //Si para dia expecificado no existe en array cupo por fecha y hora ni general se establece a null
                                $array_disponibilidades[] = [
                                    'date' => $fecha->format('Y-m-d'),
                                    'avail' => null,
                                    'closed' => null,
                                    'unlimited' => null,
                                ];
                            }
                            $fecha->addDay();
                        }

                        foreach ($idiomas as $idi) {
                            $traduccion = ProductTranslation::where('product_id', '=', $producto->id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $producto->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                        $array['data'][0]['availability'][] = [
                            'id' => $producto->id,
                            'lang' => $producto->lang,
                            'availability' => $array_disponibilidades,
                        ];
                    }
                    $array['total_results'] = $totales;
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Disponibilidad');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra los inventarios, cupos y cierres para un array de productos, para un rango de fechas, incluidos los productos que forman parte de un paquete
     *
     * Para la consulta del inventario productos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function showCalendar(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $idiomas = Language::where('front',1)->get();

            $validator = Validator::make($request->all(), [
                'date_start' => 'date|required|date_format:"Y-m-d"',
                'date_end' => 'date|required|after_or_equal:date_start|date_format:"Y-m-d"',
                'products' => 'required|array',
                'products.*' => 'required|distinct|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->first();

            }

            //Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_productos = DB::connection('tenant')->table('mo_product')
                    ->select(
                        'mo_product.id',
                        'mo_product.code',
                        'mo_product.order',
                        'mo_product_translation.language_id',
                        'mo_product_translation.name',
                        'mo_product_type.id as product_type_id'
                    )
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_translation', 'mo_product.id', 'mo_product_translation.product_id')
                    ->whereNull('mo_product_translation.deleted_at')
                    ->where('mo_product_translation.language_id', '=', $idioma->id)
                    ->join('mo_product_type', 'mo_product.type_id', 'mo_product_type.id')
                    ->whereNull('mo_product_type.deleted_at')
                    ->join('mo_product_location', 'mo_product.id', 'mo_product_location.product_id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->join('mo_location', 'mo_product_location.location_id', 'mo_location.id')
                    ->whereNull('mo_location.deleted_at');

                $sql_locaciones = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id', 'mo_location.name', 'mo_location.depends_on')
                    ->whereNull('mo_location.deleted_at')
                    ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->groupBy('mo_location.id');

                $sql_product_package = DB::connection('tenant')->table('mo_product')
                    ->select(
                        'mo_product.id',
                        'mo_product.order',
                        'mo_product.code',
                        'mo_product_translation.language_id',
                        'mo_product_translation.name',
                        'mo_product_type.id as product_type_id',
                        'mo_product_package.location_id',
                        'mo_location.name as location_name',
                        'mo_location.depends_on as location_depends_on'
                    )
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_translation', 'mo_product_translation.product_id', 'mo_product.id')
                    ->whereNull('mo_product_translation.deleted_at')
                    ->where('mo_product_translation.language_id', '=', $idioma->id)
                    ->join('mo_product_type', 'mo_product_type.id', 'mo_product.type_id')
                    ->whereNull('mo_product_type.deleted_at')
                    ->join('mo_product_package', 'mo_product_package.product_id', 'mo_product.id')
                    ->whereNull('mo_product_package.deleted_at')
                    ->join('mo_product_location', 'mo_product_location.product_id', 'mo_product.id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->join('mo_location', 'mo_location.id', 'mo_product_package.location_id')
                    ->whereNull('mo_location.deleted_at')
                    ->groupBy('mo_product.id');

                //Consultas de disponibilidades por fecha y sesión con closed 1
                $sql_disponibilidad = DB::connection('tenant')->table('mo_availability')
                    ->select(DB::connection('tenant')->raw('SUM(mo_availability.avail) as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                    ->whereNull('mo_availability.deleted_at')
                    ->orderBy('mo_availability.date')
                    ->groupBy('mo_availability.date')
                    ->whereNull('mo_availability.service_id')
                    ->where('mo_availability.closed', '=', 0);

                //Consultas de disponibildades por fecha y sesión con closed 0
                $sql_disponibilidad_cerrado = DB::connection('tenant')->table('mo_availability')
                    ->select(DB::connection('tenant')->raw('0 as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                    ->whereNull('mo_availability.deleted_at')
                    ->orderBy('mo_availability.date')
                    ->groupBy('mo_availability.date')
                    ->whereNull('mo_availability.service_id')
                    ->where('mo_availability.closed', '=', 1);

                //Consultas de disponibildades de cupo general con closed 0
                $sql_disponibilidad_general = DB::connection('tenant')->table('mo_availability')
                    ->select(DB::connection('tenant')->raw('SUM(mo_availability.avail) as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                    ->whereNull('mo_availability.deleted_at')
                    ->whereNull('mo_availability.date')
                    ->whereNull('mo_availability.service_id')
                    ->where('mo_availability.closed', '=', 0)
                    ->groupBy('date');

                //Consultas de disponibildades de cupo general con closed 1
                $sql_disponibilidad_general_cerrado = DB::connection('tenant')->table('mo_availability')
                    ->select(DB::connection('tenant')->raw('SUM(mo_availability.avail) as total_avail'), 'mo_availability.date', 'mo_availability.session', DB::connection('tenant')->raw('MIN(mo_availability.closed) as closed'), DB::connection('tenant')->raw('MAX(mo_availability.unlimited) as unlimited'))
                    ->whereNull('mo_availability.deleted_at')
                    ->whereNull('mo_availability.date')
                    ->whereNull('mo_availability.service_id')
                    ->where('mo_availability.closed', '=', 1)
                    ->groupBy('date');

                $fecha_inicio = $request->get('date_start');
                $fecha_fin = $request->get('date_end');

                $sql_disponibilidad->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where('mo_availability.date', '>=', $fecha_inicio)
                            ->where('mo_availability.date', '<=', $fecha_fin);
                    });
                })->whereNotNull('mo_availability.date');

                $sql_disponibilidad_cerrado->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                    $query->where(function ($query) use ($fecha_inicio, $fecha_fin) {
                        $query->where('mo_availability.date', '>=', $fecha_inicio)
                            ->where('mo_availability.date', '<=', $fecha_fin);
                    });
                })->whereNotNull('mo_availability.date');


                // Order
                $orden = 'mo_product_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_product_translation.name';
                            break;
                        default:
                            $orden = 'mo_product_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way


                $sql_productos->groupBy('mo_product.id')->orderBy($orden, $sentido);
                $array['data'] = array();

                foreach ($request->get('products') as $product_id) {
                    $sql_resultados_productos_clone = clone $sql_productos;

                    $producto = $sql_resultados_productos_clone->where('mo_product.id', $product_id)->first();

                    if($producto) {

                        $array_salida_producto = array();

                        $array_salida_producto['id'] = $producto->id;

                        $sql_locaciones_clone = clone $sql_locaciones;
                        $datos_locaciones = $sql_locaciones_clone->where('mo_product_location.product_id', $producto->id)->get();

                        $array_salida_producto['lang'][][$idioma->abbreviation] = [
                            'language_id' => $producto->language_id,
                            'name' => $producto->name,
                        ];

                        // Tipo de producto
                        $array_tipo_producto = array();
                        $array_tipo_producto['id'] = $producto->product_type_id;
                        $array_salida_producto['type'][] = $array_tipo_producto;
                        // FIN Tipo de producto

                        $array_traduccion = array();

                        $array_salida_producto['location'] = array();
                        foreach ($datos_locaciones as $locacion) {

                            $array_location = [
                                'id' => $locacion->id,
                                'name' => $locacion->name,
                            ];

                            //Producto tipo paquete
                            if ($producto->product_type_id == 1) {
                                $array_location['product_package'] = array();
                                $sql_product_package_clone = clone $sql_product_package;

                                $datos_product_package = $sql_product_package_clone->where('mo_product_package.package_id', $producto->id)->get();

                                foreach ($datos_product_package as $product_package) {
                                    $array_product_package = array();

                                    $array_product_package['id'] = $product_package->id;

                                    $array_product_package['lang'][][$idioma->abbreviation] = [
                                        'language_id' => $product_package->language_id,
                                        'name' => $product_package->name,
                                    ];

                                    $array_location_package = array();

                                    $array_location_package = [
                                        'id' => $product_package->location_id,
                                        'name' => $product_package->location_name,
                                    ];

                                    ////Disponibilidades del producto
                                    //Clonación de consultas de disponibilidades por fecha y sesión
                                    $sql_disponibilidad_clone = clone $sql_disponibilidad;
                                    $sql_disponibilidad_cerrado_clone = clone $sql_disponibilidad_cerrado;

                                    $sql_disponibilidad_clone->where('mo_availability.product_id', '=', $product_package->id);
                                    $sql_disponibilidad_cerrado_clone->where('mo_availability.product_id', '=', $product_package->id);

                                    $sql_disponibilidad_clone->where('mo_availability.location_id', '=', $product_package->location_id);
                                    $sql_disponibilidad_cerrado_clone->where('mo_availability.location_id', '=', $product_package->location_id);

                                    //Clonación de consultas de disponibilades de cupos generales
                                    $sql_disponibilidad_general_clone = clone $sql_disponibilidad_general;
                                    $sql_disponibilidad_general_cerrado_clone = clone $sql_disponibilidad_general_cerrado;

                                    $sql_disponibilidad_general_clone->where('mo_availability.product_id', '=', $product_package->id);
                                    $sql_disponibilidad_general_cerrado_clone->where('mo_availability.product_id', '=', $product_package->id);

                                    $sql_disponibilidad_general_clone->where('mo_availability.location_id', '=', $product_package->location_id);
                                    $sql_disponibilidad_general_cerrado_clone->where('mo_availability.location_id', '=', $product_package->location_id);

                                    //Al realizar los union toma como preferencia los resultados de la query que comprueba los cupos NO cerrados
                                    //Se mantiene dado que se considera como preferente estos frente a los cupos con estado cerrado
                                    //Tanto en los cupos por fecha y sesión como los cupos generales

                                    //Unión de consultas de disponiblades por fecha y sesión
                                    $sql_disponibilidad_completa = $sql_disponibilidad_clone->union($sql_disponibilidad_cerrado_clone);

                                    //Unión de consultas de disponibilidades de cupo general
                                    $sql_disponibilidad_completa_general = $sql_disponibilidad_general_clone->union($sql_disponibilidad_general_cerrado_clone);


                                    //Subconsulta de disponibilidades por fecha y sesión
                                    $datos_disponibilidad = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_disponibilidad_completa->toSql()}) as sub"))
                                        ->select('*')
                                        ->groupBy('date')
                                        ->mergeBindings($sql_disponibilidad_completa)->get();

                                    //Subconsulta de disponibilidades de cupo general
                                    $datos_disponibilidad_general = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_disponibilidad_completa_general->toSql()}) as sub"))
                                        ->select('*')
                                        ->groupBy('date')
                                        ->mergeBindings($sql_disponibilidad_completa_general)->first();

                                    $array_disponibilidades = array();

                                    $fecha = '';
                                    $fecha = Carbon::createFromFormat('Y-m-d', $fecha_inicio)->startOfDay();
                                    $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'))->endOfDay();
                                    while ($fecha <= $fecha_fin) {
                                        if($fecha >= Carbon::now()->startOfDay()->format('Y-m-d')){
                                            foreach ($datos_disponibilidad as $avail) {
                                                if ($avail->date == $fecha->format('Y-m-d')) {
                                                    //Si en array de cupos por fecha y sesión existe registro para la fecha recorrida se añade al array de devolución
                                                    //Se castea a entero para conservar formato de datos devueltos
                                                    $array_disponibilidades[] = ['date' => $avail->date, 'avail' => (int)$avail->total_avail, 'closed' => $avail->closed, 'unlimited' => $avail->unlimited];
                                                }
                                            }
                                            if ($datos_disponibilidad_general && !array_search($fecha->format('Y-m-d'), array_column($array_disponibilidades, 'date'))) {
                                                //Si no existe registro por fecha y sesión para la fecha recorrida se comprueba el cupo general del producto
                                                $dato_avail = 0;
                                                if ($datos_disponibilidad_general->closed != 1) {
                                                    $dato_avail = $datos_disponibilidad_general->total_avail != null ? $datos_disponibilidad_general->total_avail : 0;
                                                }
                                                //Se castea a entero para conservar formato de datos devueltos
                                                $array_disponibilidades[] = [
                                                    'date' => $fecha->format('Y-m-d'),
                                                    'avail' => (int)$dato_avail,
                                                    'closed' => $datos_disponibilidad_general->closed,
                                                    'unlimited' => $datos_disponibilidad_general->unlimited,
                                                ];
                                            } elseif (!array_search($fecha->format('Y-m-d'), array_column($array_disponibilidades, 'date')) && !$datos_disponibilidad_general) {
                                                //Si para dia expecificado no existe en array cupo por fecha y hora ni general se establece a null
                                                $array_disponibilidades[] = [
                                                    'date' => $fecha->format('Y-m-d'),
                                                    'avail' => null,
                                                    'closed' => null,
                                                    'unlimited' => null,
                                                ];
                                            }

                                            //Se bloquean fechas anteriores a la actual
                                        } else {
                                            $array_disponibilidades[] = [
                                                'date' => $fecha->format('Y-m-d'),
                                                'avail' => null,
                                                'closed' => null,
                                                'unlimited' => null,
                                            ];
                                        }

                                        $fecha->addDay();
                                    }

                                    //Fin disponibilidades del producto

                                    $array_location_package['availabilities'] = $array_disponibilidades;

                                    $array_product_package['location'][] = $array_location_package;

                                    $array_location['product_package'][] = $array_product_package;
                                }

                                //Resto de productos
                            } else {
                                ////Disponibilidades del producto
                                //Clonación de consultas de disponibilidades por fecha y sesión
                                $sql_disponibilidad_clone = clone $sql_disponibilidad;
                                $sql_disponibilidad_cerrado_clone = clone $sql_disponibilidad_cerrado;

                                $sql_disponibilidad_clone->where('mo_availability.product_id', '=', $producto->id);
                                $sql_disponibilidad_cerrado_clone->where('mo_availability.product_id', '=', $producto->id);

                                $sql_disponibilidad_clone->where('mo_availability.location_id', '=', $locacion->id);
                                $sql_disponibilidad_cerrado_clone->where('mo_availability.location_id', '=', $locacion->id);

                                //Clonación de consultas de disponibilades de cupos generales
                                $sql_disponibilidad_general_clone = clone $sql_disponibilidad_general;
                                $sql_disponibilidad_general_cerrado_clone = clone $sql_disponibilidad_general_cerrado;

                                $sql_disponibilidad_general_clone->where('mo_availability.product_id', '=', $producto->id);
                                $sql_disponibilidad_general_cerrado_clone->where('mo_availability.product_id', '=', $producto->id);

                                $sql_disponibilidad_general_clone->where('mo_availability.location_id', '=', $locacion->id);
                                $sql_disponibilidad_general_cerrado_clone->where('mo_availability.location_id', '=', $locacion->id);

                                //Al realizar los union toma como preferencia los resultados de la query que comprueba los cupos NO cerrados
                                //Se mantiene dado que se considera como preferente estos frente a los cupos con estado cerrado
                                //Tanto en los cupos por fecha y sesión como los cupos generales

                                //Unión de consultas de disponiblades por fecha y sesión
                                $sql_disponibilidad_completa = $sql_disponibilidad_clone->union($sql_disponibilidad_cerrado_clone);

                                //Unión de consultas de disponibilidades de cupo general
                                $sql_disponibilidad_completa_general = $sql_disponibilidad_general_clone->union($sql_disponibilidad_general_cerrado_clone);

                                //Subconsulta de disponibilidades por fecha y sesión
                                $datos_disponibilidad = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_disponibilidad_completa->toSql()}) as sub"))
                                    ->select('*')
                                    ->groupBy('date')
                                    ->mergeBindings($sql_disponibilidad_completa)->get();

                                //Subconsulta de disponibilidades de cupo general
                                $datos_disponibilidad_general = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_disponibilidad_completa_general->toSql()}) as sub"))
                                    ->select('*')
                                    ->groupBy('date')
                                    ->mergeBindings($sql_disponibilidad_completa_general)->first();

                                $array_disponibilidades = array();

                                $fecha = '';
                                $fecha = Carbon::createFromFormat('Y-m-d', $fecha_inicio)->startOfDay();

                                $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'))->endOfDay();
                                while ($fecha <= $fecha_fin) {
                                    if ($fecha >= Carbon::now()->startOfDay()->format('Y-m-d')) {
                                        foreach ($datos_disponibilidad as $avail) {
                                            if ($avail->date == $fecha->format('Y-m-d')) {
                                                //Si en array de cupos por fecha y sesión existe registro para la fecha recorrida se añade al array de devolución
                                                //Se castea a entero para conservar formato de datos devueltos
                                                $array_disponibilidades[] = ['date' => $avail->date, 'avail' => (int)$avail->total_avail, 'closed' => $avail->closed, 'unlimited' => $avail->unlimited];
                                            }
                                        }
                                        if ($datos_disponibilidad_general && !array_search($fecha->format('Y-m-d'), array_column($array_disponibilidades, 'date'))) {
                                            //Si no existe registro por fecha y sesión para la fecha recorrida se comprueba el cupo general del producto
                                            $dato_avail = 0;
                                            if ($datos_disponibilidad_general->closed != 1) {
                                                $dato_avail = $datos_disponibilidad_general->total_avail != null ? $datos_disponibilidad_general->total_avail : 0;
                                            }
                                            //Se castea a entero para conservar formato de datos devueltos
                                            $array_disponibilidades[] = [
                                                'date' => $fecha->format('Y-m-d'),
                                                'avail' => (int)$dato_avail,
                                                'closed' => $datos_disponibilidad_general->closed,
                                                'unlimited' => $datos_disponibilidad_general->unlimited,
                                            ];
                                        } elseif (!array_search($fecha->format('Y-m-d'), array_column($array_disponibilidades, 'date')) && !$datos_disponibilidad_general) {
                                            //Si para dia expecificado no existe en array cupo por fecha y hora ni general se establece a null
                                            $array_disponibilidades[] = [
                                                'date' => $fecha->format('Y-m-d'),
                                                'avail' => null,
                                                'closed' => null,
                                                'unlimited' => null,
                                            ];
                                        }

                                        //Se bloquean fechas anteriores a la actual
                                    } else {
                                        $array_disponibilidades[] = [
                                            'date' => $fecha->format('Y-m-d'),
                                            'avail' => null,
                                            'closed' => null,
                                            'unlimited' => null,
                                        ];
                                    }
                                    $fecha->addDay();

                                }

                                //Fin disponibilidades del producto

                                $array_location['availabilities'] = $array_disponibilidades;

                            }

                            $array_salida_producto['location'][] = $array_location;
                        }
                        $array['data'][0]['product'][] = $array_salida_producto;
                    }
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Disponibilidad');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra la disponibilidad de un producto a partir de su id
     *
     * Para la consulta del inventario de un producto y sus servicios asociados si los tiene se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'date' => 'date|required|date_format:"Y-m-d"',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                //datos de producto enviado
                $productos = DB::connection('tenant')->table('mo_product')
                    ->select('mo_product.id')
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_translation', 'mo_product_translation.product_id', 'mo_product.id')
                    ->whereNull('mo_product_translation.deleted_at')
                    ->where('mo_product.id', $id)->groupBy('mo_product.id')->get();

                //datos de servicios que están en product service con el producto enviado
                $servicios = DB::connection('tenant')->table('mo_service')
                    ->select('mo_service.id')
                    ->whereNull('mo_service.deleted_at')
                    ->join('mo_service_translation', 'mo_service_translation.service_id', 'mo_service.id')
                    ->whereNull('mo_service_translation.deleted_at')
                    ->join('mo_product_service', 'mo_product_service.service_id', 'mo_service.id')
                    ->whereNull('mo_product_service.deleted_at')
                    ->where('mo_product_service.product_id', $id)
                    ->groupBy('mo_service.id')->get();

                //locaciones que comparten registro con el producto en product location
                $locaciones = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id', 'mo_location.name')
                    ->whereNull('mo_location.deleted_at')
                    ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->where('mo_product_location.product_id', $id)
                    ->groupBy('mo_location.id')->get();

                $sql_avail = DB::connection('tenant')->table('mo_availability')
                    ->select('mo_availability.id', 'mo_availability.date', 'mo_availability.session', 'mo_availability.product_id', 'mo_availability.service_id', 'mo_availability.location_id', 'mo_availability.quota', 'mo_availability.avail', 'mo_availability.overbooking',
                        'mo_availability.overbooking_unit', 'mo_availability.closed', 'mo_availability.unlimited')
                    ->whereNull('mo_availability.deleted_at')
                    ->where('mo_availability.product_id', '=', $id)
                    ->where(function ($query) use ($request) {
                        $query->where('mo_availability.date', '=', null)
                            ->orWhere('mo_availability.date', '=', $request->get('date'));
                    });

                $array['data'] = array();
                foreach ($productos as $producto) {
                    foreach ($idiomas as $idi) {
                        $traducciones_product = ProductTranslation::where('language_id', '=', $idi->id)->where('product_id', '=', $producto->id)
                            ->select('id', 'language_id', 'name')
                            ->get();
                        foreach ($traducciones_product as $trad) {
                            $producto->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $product_type_id = Product::where('id', $producto->id)->first()->type_id;

                    foreach ($locaciones as $locacion) {

                        $sql_avail_clone = clone $sql_avail;

                        $avail_producto = $sql_avail_clone->where('mo_availability.service_id', '=', null)->where('mo_availability.location_id', '=', $locacion->id)
                            ->orderBy('mo_availability.session')->get();

                        $array_location = ['id' => $locacion->id, 'name' => $locacion->name];

                        if($product_type_id != 2){
                            $array_location['availability'] = $avail_producto;
                        }

                        $array_location['service'] = array();


                        if ($servicios) {
                            foreach ($servicios as $servicio) {

                                $array_service = array();

                                $sql_avail_clone_service = clone $sql_avail;
                                $array_service['id'] = $servicio->id;

                                foreach ($idiomas as $idi) {

                                    $traducciones_service = ServiceTranslation::where('language_id', '=', $idi->id)->where('service_id', '=', $servicio->id)
                                        ->select('id', 'language_id', 'name')
                                        ->get();
                                    foreach ($traducciones_service as $trad) {
                                        $array_service['lang'][][$idi->abbreviation] = $trad;
                                    }

                                }

                                $datos_avail_service = $sql_avail_clone_service->where('mo_availability.service_id', '=', $servicio->id)->where('mo_availability.location_id', '=', $locacion->id)
                                    ->orderBy('mo_availability.session')->get();

                                $array_service['availability'] = $datos_avail_service;

                                $array_location['service'][] = $array_service;
                            }
                        }
                        $producto->location[] = $array_location;
                    }

                    $array['data'][0]['availability'][0]['product'][0] = $producto;
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Disponibilidad');
        }

        return response()->json($array, $array['error']);

    }

    /**
     * Función para la actualización de un inventario
     *
     * Para actualizar la disponibilidad de un inventario se realiza una petición PUT.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function update(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'date_start' => 'date|required_with:date_end,session|after_or_equal:today|date_format:"Y-m-d',
                'date_end' => 'date|required_with:date_start,session|after_or_equal:date_start|date_format:"Y-m-d',
                'product_id' => 'required|numeric',
                'service_id' => 'numeric',
                'location_id' => 'required|numeric',
                'session' => 'date_format:H:i:s',
                'avail' => 'numeric|required',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // Fin de validación

            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                //Valida la existencia en base de datos y no esten borrados de forma lógica
                $validator = Validator::make($request->all(), [
                    'product_id' => 'exists:tenant.mo_product,id,deleted_at,NULL',
                    'service_id' => 'exists:tenant.mo_service,id,deleted_at,NULL',
                    'location_id' => 'exists:tenant.mo_location,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    //Inicializa la consulta con filtros
                    $sql_availability = Availability::where('product_id', '=', $request->get('product_id'))
                        ->where('location_id', '=', $request->get('location_id'));

                    // Si se trata de un servicio
                    if ($request->get('service_id') != '') {
                        $sql_availability->where('service_id', '=', $request->get('service_id'));
                    } else {
                        $sql_availability->where('service_id', '=', null);
                    }

                    // Si se envía rango de fechas
                    if ($request->get('date_start') != '') {

                        $fecha = Carbon::createFromFormat('Y-m-d', $request->get('date_start'));
                        $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'));
                        $sql_availability->where('date', '>=', $fecha->format('Y-m-d'))->where('date', '<=', $fecha_fin->format('Y-m-d'));


                        //Si se especifica sessión establece el filtro
                        if ($request->get('session') != '') {
                            $sql_availability->where('session', '=', $request->get('session'));
                        } else {
                            $sql_availability->where('session', '=', null);
                        }

                        $datos_availabilities = $sql_availability->get();


                        foreach ($datos_availabilities as $avail) {
                            $avail->avail = $avail->avail + $request->get('avail');
                            $avail->save();
                        }

                    } else {
                        //En el caso de no especificar fecha establece el filtro de búsqueda
                        $avail = $sql_availability->where('date', '=', null)->first();
                        if ($avail) {
                            //Si encuenrta algun registro lo actualiza
                            $avail->avail = $avail->avail + $request->get('avail');
                            $avail->save();
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            //Lanza mensaje de error en caso de producirse alguna excepcion
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Disponibilidad');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para la modificación del inventario de un producto en una fecha determinada.
     *
     * Para la actualización de un inventario en un dia determinado se realizara una petición PUT.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDay(Request $request)
    {

        set_time_limit(0);

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'product_id' => 'required|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
                'date' => 'date|required|after_or_equal:today|date_format:"Y-m-d"',
                'locations' => 'array|required',

                'locations.*.location_id' => 'integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL|required',
                'locations.*.quota' => 'integer|min:0|required_without_all:locations.*.avail,locations.*.closed,locations.*.unlimited,locations.*.availabilities,locations.*.services',
                'locations.*.avail' => 'integer|required_without_all:locations.*.quota,locations.*.closed,locations.*.unlimited,locations.*.availabilities,locations.*.services',
                'locations.*.overbooking' => 'integer|min:0|required_with:locations.*.overbooking_unit',
                'locations.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.overbooking',
                'locations.*.closed' => 'boolean|required_without_all:locations.*.quota,locations.*.avail,locations.*.unlimited,locations.*.availabilities,locations.*.services',
                'locations.*.unlimited' => 'boolean|required_without_all:locations.*.quota,locations.*.avail,locations.*.closed,locations.*.availabilities,locations.*.services',

                'locations.*.availabilities' => 'array|required_without_all:locations.*.quota,locations.*.avail,locations.*.closed,locations.*.unlimited,locations.*.services',

                'locations.*.availabilities.*.session' => 'date_format:H:i|required',
                'locations.*.availabilities.*.quota' => 'integer|min:0|required_without_all:locations.*.availabilities.*.avail,locations.*.availabilities.*.closed,locations.*.availabilities.*.unlimited',
                'locations.*.availabilities.*.avail' => 'integer|required_without_all:locations.*.availabilities.*.quota,locations.*.availabilities.*.closed,locations.*.availabilities.*.unlimited',
                'locations.*.availabilities.*.overbooking' => 'integer|min:0|required_with:locations.*.availabilities.*.overbooking_unit',
                'locations.*.availabilities.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.availabilities.*.overbooking',
                'locations.*.availabilities.*.closed' => 'boolean|required_without_all:locations.*.availabilities.*.quota,locations.*.availabilities.*.avail,locations.*.availabilities.*.unlimited',
                'locations.*.availabilities.*.unlimited' => 'boolean|required_without_all:locations.*.availabilities.*.quota,locations.*.availabilities.*.avail,locations.*.availabilities.*.closed',


                'locations.*.services' => 'array|required_without_all:locations.*.quota,locations.*.avail,locations.*.closed,locations.*.unlimited,locations.*.availabilities',

                'locations.*.services.*.service_id' => 'integer|min:0|exists:tenant.mo_service,id,deleted_at,NULL|required_with:locations.*.services',
                'locations.*.services.*.quota' => 'integer|min:0|required_without_all:locations.*.services.*.avail,locations.*.services.*.closed,locations.*.services.*.unlimited,locations.*.services.*.availabilities',
                'locations.*.services.*.avail' => 'integer|required_without_all:locations.*.services.*.quota,locations.*.services.*.closed,locations.*.services.*.unlimited,locations.*.services.*.availabilities',
                'locations.*.services.*.overbooking' => 'integer|min:0|required_with:locations.*.services.*.overbooking_unit',
                'locations.*.services.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.services.*.overbooking',
                'locations.*.services.*.closed' => 'boolean|required_without_all:locations.*.services.*.quota,locations.*.services.*.avail,locations.*.services.*.unlimited,locations.*.services.*.availabilities',
                'locations.*.services.*.unlimited' => 'boolean|required_without_all:locations.*.services.*.quota,locations.*.services.*.avail,locations.*.services.*.closed,locations.*.services.*.availabilities',

                'locations.*.services.*.availabilities' => 'array|required_without_all:locations.*.services.*.quota,locations.*.services.*.avail,locations.*.services.*.closed,locations.*.services.*.unlimited',

                'locations.*.services.*.availabilities.*.session' => 'date_format:H:i|required',
                'locations.*.services.*.availabilities.*.quota' => 'integer|min:0|required_without_all:locations.*.services.*.availabilities.*.avail,locations.*.services.*.availabilities.*.closed,locations.*.services.*.availabilities.*.unlimited',
                'locations.*.services.*.availabilities.*.avail' => 'integer|required_without_all:locations.*.services.*.availabilities.*.quota,locations.*.services.*.availabilities.*.closed,locations.*.services.*.availabilities.*.unlimited',
                'locations.*.services.*.availabilities.*.overbooking' => 'integer|min:0|required_with:locations.*.servoces.*.availabilities.*.overbooking_unit',
                'locations.*.services.*.availabilities.*.overbooking_unit' => 'in:"u", "%"|required_with:locations.*.services.*.availabilities.*.overbooking',
                'locations.*.services.*.availabilities.*.closed' => 'boolean|required_without_all:locations.*.services.*.availabilities.*.quota,locations.*.services.*.availabilities.*.avail,locations.*.services.*.availabilities.*.unlimited',
                'locations.*.services.*.availabilities.*.unlimited' => 'boolean|required_without_all:locations.*.services.*.availabilities.*.quota,locations.*.services.*.availabilities.*.avail,locations.*.services.*.availabilities.*.closed',

            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin de validación

            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {  //Si no se ha producido ningún error continua

                //Consulta base para reutilizar
                $sql_availability = Availability::where('product_id', '=', $request->get('product_id'));


                $array_locaciones = array();
                foreach ($request->get('locations') as $location) {
                    $array_locaciones[] = $location['location_id'];
                    //Clon de consulta base y añadir where location_id de cupo por fecha y hora
                    $sql_availability_clone = clone $sql_availability;
                    $sql_availability_clone->where('location_id', '=', $location['location_id']);

                    //Clon de consulta base de cupo general
                    $sql_availability_general = clone $sql_availability_clone;
                    $sql_availability_general->whereNull('date')
                        ->whereNull('session')
                        ->whereNull('service_id');

                    //Actualización o borrado de cupo general sin fecha ni sesión
                    if (isset($location['quota']) || isset($location['avail']) || isset($location['closed']) || isset($location['unlimited'])) {

                        //Consulta de cupo general
                        $datos_availability_general = $sql_availability_general->first();

                        if ($datos_availability_general) {
                            if (isset($location['quota'])) {
                                if (!isset($location['avail'])) {
                                    $disponible = $location['quota'] - $datos_availability_general->quota + $datos_availability_general->avail;
                                } else {
                                    $disponible = $location['avail'];
                                }
                                $datos_availability_general->quota = $location['quota'];
                                $datos_availability_general->avail = $disponible;
                            }

                            if (isset($location['overbooking'])) {
                                $datos_availability_general->overbooking = $location['overbooking'];
                                $datos_availability_general->overbooking_unit = $location['overbooking_unit'];
                            }

                            if (isset($location['closed'])) {
                                $datos_availability_general->closed = $location['closed'];
                            }

                            if (isset($location['unlimited'])) {
                                $datos_availability_general->unlimited = $location['unlimited'];
                            }

                            $datos_availability_general->save();
                        } else {

                            if (isset($location['avail'])) {
                                $disponible = $location['avail'];
                            } elseif (isset($location['quota'])) {
                                $disponible = $location['quota'];
                            } else {
                                $disponible = 0;
                            }

                            $datos_availability = Availability::create([
                                'product_id' => $request->get('product_id'),
                                'location_id' => $location['location_id'],
                                'quota' => isset($location['quota']) ? $location['quota'] : 0,
                                'avail' => $disponible,
                                'overbooking' => isset($location['overbooking']) ? $location['overbooking'] : 0,
                                'overbooking_unit' => isset($location['overbooking_unit']) ? $location['overbooking_unit'] : 'u',
                                'closed' => isset($location['closed']) ? $location['closed'] : 0,
                                'unlimited' => isset($location['unlimited']) ? $location['unlimited'] : 0,
                            ]);
                        }
                    } else {
                        $sql_availability_general->delete();
                    }

                    //Actualización o borrado de los cupos con fecha y sesión

                    $array_sessiones = array();
                    if (isset($location['availabilities'])) {
                        foreach ($location['availabilities'] as $disponible) {
                            $sql_availability_session = clone $sql_availability_clone;
                            $datos_availability_session = $sql_availability_session->where('session', '=', $disponible['session'])
                                ->whereNull('service_id')->where('date', '=', $request->get('date'))->first();

                            if ($datos_availability_session) {
                                if (isset($disponible['quota'])) {
                                    if (!isset($disponible['avail'])) {
                                        $disponibilidad = $disponible['quota'] - $datos_availability_session->quota + $datos_availability_session->avail;
                                    } else {
                                        $disponibilidad = $disponible['avail'];
                                    }
                                    $datos_availability_session->quota = $disponible['quota'];
                                    $datos_availability_session->avail = $disponibilidad;
                                }

                                if (isset($disponible['overbooking'])) {
                                    $datos_availability_session->overbooking = $disponible['overbooking'];
                                    $datos_availability_session->overbooking_unit = $disponible['overbooking_unit'];
                                }

                                if (isset($disponible['closed'])) {
                                    $datos_availability_session->closed = $disponible['closed'];
                                }

                                if (isset($disponible['unlimited'])) {
                                    $datos_availability_session->unlimited = $disponible['unlimited'];
                                }

                                $datos_availability_session->save();

                            } else {
                                if (isset($disponible['avail'])) {
                                    $disponibles = $disponible['avail'];
                                } elseif (isset($disponible['quota'])) {
                                    $disponibles = $disponible['quota'];
                                } else {
                                    $disponibles = 0;
                                }

                                $datos_availability = Availability::create([
                                    'date' => $request->get('date'),
                                    'session' => $disponible['session'],
                                    'product_id' => $request->get('product_id'),
                                    'location_id' => $location['location_id'],
                                    'quota' => isset($disponible['quota']) ? $disponible['quota'] : 0,
                                    'avail' => $disponibles,
                                    'overbooking' => isset($disponible['overbooking']) ? $disponible['overbooking'] : 0,
                                    'overbooking_unit' => isset($disponible['overbooking_unit']) ? $disponible['overbooking_unit'] : 'u',
                                    'closed' => isset($disponible['closed']) ? $disponible['closed'] : 0,
                                    'unlimited' => isset($disponible['unlimited']) ? $disponible['unlimited'] : 0,
                                ]);
                            }

                            $array_sessiones[] = $disponible['session'];
                        }


                    }
                    $sql_borrados = clone $sql_availability_clone;
                    $sql_borrados->whereNull('service_id')->where('date', $request->get('date'))->whereNotNull('session')->whereNotIn('session', $array_sessiones)->delete();

                    $array_servicios = array();
                    // Actualización o borrado de los cupos de servicio asociado al producto
                    if (isset($location['services'])) {

                        foreach ($location['services'] as $servicios) {

                            $sql_availability_servicios_general = clone $sql_availability_clone;
                            $sql_availability_servicios_general->where('service_id', '=', $servicios['service_id'])->whereNull('date')
                                ->whereNull('session')->where('service_id', '=', $servicios['service_id']);

                            if (isset($servicios['quota']) || isset($servicios['avail']) || isset($servicios['closed']) || isset($servicios['unlimited'])) {

                                $datos_availability_servicios_general = $sql_availability_servicios_general->first();

                                if ($datos_availability_servicios_general) {

                                    if (isset($servicios['quota'])) {
                                        if (!isset($servicios['avail'])) {
                                            $disponible = $servicios['quota'] - $datos_availability_servicios_general->quota + $datos_availability_servicios_general->avail;
                                        } else {
                                            $disponible = $servicios['avail'];
                                        }
                                        $datos_availability_servicios_general->quota = $servicios['quota'];
                                        $datos_availability_servicios_general->avail = $disponible;
                                    }

                                    if (isset($location['overbooking'])) {
                                        $datos_availability_servicios_general->overbooking = $servicios['overbooking'];
                                        $datos_availability_servicios_general->overbooking_unit = $servicios['overbooking_unit'];
                                    }

                                    if (isset($location['closed'])) {
                                        $datos_availability_servicios_general->closed = $servicios['closed'];
                                    }

                                    if (isset($location['unlimited'])) {
                                        $datos_availability_servicios_general->unlimited = $servicios['unlimited'];
                                    }

                                    $datos_availability_servicios_general->save();
                                } else {

                                    if (isset($servicios['avail'])) {
                                        $disponible = $servicios['avail'];
                                    } elseif (isset($servicios['quota'])) {
                                        $disponible = $servicios['quota'];
                                    } else {
                                        $disponible = 0;
                                    }

                                    $datos_availability = Availability::create([
                                        'service_id' => $servicios['service_id'],
                                        'product_id' => $request->get('product_id'),
                                        'location_id' => $location['location_id'],
                                        'quota' => isset($servicios['quota']) ? $servicios['quota'] : 0,
                                        'avail' => $disponible,
                                        'overbooking' => isset($servicios['overbooking']) ? $servicios['overbooking'] : 0,
                                        'overbooking_unit' => isset($servicios['overbooking_unit']) ? $servicios['overbooking_unit'] : 'u',
                                        'closed' => isset($servicios['closed']) ? $servicios['closed'] : 0,
                                        'unlimited' => isset($servicios['unlimited']) ? $servicios['unlimited'] : 0,
                                    ]);
                                }
                            } else {
                                $sql_availability_servicios_general->delete();
                            }

                            //Actualización o borrado de los cupos con fecha y sesión
                            if (isset($servicios['availabilities'])) {
                                $array_sessiones = array();
                                foreach ($servicios['availabilities'] as $disponible) {
                                    $sql_availability_session = clone $sql_availability_clone;
                                    $datos_availability_session = $sql_availability_session->where('session', '=', $disponible['session'])
                                        ->where('service_id', '=', $servicios['service_id'])->where('date', '=', $request->get('date'))->first();

                                    if ($datos_availability_session) {
                                        if (isset($disponible['quota'])) {
                                            if (!isset($disponible['avail'])) {
                                                $disponibilidad = $disponible['quota'] - $datos_availability_session->quota + $datos_availability_session->avail;
                                            } else {
                                                $disponibilidad = $disponible['avail'];
                                            }
                                            $datos_availability_session->quota = $disponible['quota'];
                                            $datos_availability_session->avail = $disponibilidad;
                                        }

                                        if (isset($disponible['overbooking'])) {
                                            $datos_availability_session->overbooking = $disponible['overbooking'];
                                            $datos_availability_session->overbooking_unit = $disponible['overbooking_unit'];
                                        }

                                        if (isset($disponible['closed'])) {
                                            $datos_availability_session->closed = $disponible['closed'];
                                        }

                                        if (isset($disponible['unlimited'])) {
                                            $datos_availability_session->unlimited = $disponible['unlimited'];
                                        }

                                        $datos_availability_session->save();

                                    } else {
                                        if (isset($disponible['avail'])) {
                                            $disponibles = $disponible['avail'];
                                        } elseif (isset($disponible['quota'])) {
                                            $disponibles = $disponible['quota'];
                                        } else {
                                            $disponibles = 0;
                                        }

                                        $datos_availability = Availability::create([
                                            'date' => $request->get('date'),
                                            'session' => $disponible['session'],
                                            'product_id' => $request->get('product_id'),
                                            'location_id' => $location['location_id'],
                                            'service_id' => $servicios['service_id'],
                                            'quota' => isset($disponible['quota']) ? $disponible['quota'] : 0,
                                            'avail' => $disponibles,
                                            'overbooking' => isset($disponible['overbooking']) ? $disponible['overbooking'] : 0,
                                            'overbooking_unit' => isset($disponible['overbooking_unit']) ? $disponible['overbooking_unit'] : 'u',
                                            'closed' => isset($disponible['closed']) ? $disponible['closed'] : 0,
                                            'unlimited' => isset($disponible['unlimited']) ? $disponible['unlimited'] : 0,
                                        ]);
                                    }
                                    $array_sessiones[] = $disponible['session'];
                                }
                                //Borra todos los cupos que tienen sesion pero no han sido enviados
                                $sql_availability_borrado_session = clone $sql_availability_clone;
                                $sql_availability_borrado_session->where('date', '=', $request->get('date'))->whereNotNull('session')->where('service_id', '=', $servicios['service_id'])->whereNotIn('session', $array_sessiones)->delete();
                            } else {
                                //Elimina los cupos de los servicios con fecha y hora no enviados
                                $sql_availability_servicios_session = clone $sql_availability_clone;
                                $sql_availability_servicios_session->where('date', '=', $request->get('date'))->where('service_id', '=', $servicios['service_id'])->whereNotNull('service_id')->whereNotNull('date')->whereNotNull('session')->delete();
                            }

                            $array_servicios[] = $servicios['service_id'];
                        }

                        $sql_borrados = clone $sql_availability_clone;
                        $sql_borrados_general = clone $sql_availability_clone;

                        $sql_borrados->where('date', '=', $request->get('date'))->whereNotNull('service_id')->whereNotIn('service_id', $array_servicios)->delete();
                        $sql_borrados_general->whereNull('date')->whereNotNull('service_id')->whereNotIn('service_id', $array_servicios)->delete();
                    } else {
                        $sql_borrados = clone $sql_availability_clone;

                        $sql_borrados->whereNotNull('service_id')->delete();
                    }
                }
                $sql_availability_location = clone $sql_availability;
                $sql_availability_location_general = clone $sql_availability;

                $sql_availability_location->where('date', '=', $request->get('date'))->whereNotIn('location_id', $array_locaciones)->delete();
                $sql_availability_location_general->whereNull('date')->whereNull('session')->whereNotIn('location_id', $array_locaciones)->delete();
            }
            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Disponibilidad');
        }

        return response()->json($array, $array['error']);
    }
}