<?php

namespace App\Http\Controllers\tenant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use Illuminate\Support\Str;
use App\Models\tenant\Settings;
use App\Models\tenant\WebPage;
use App\Models\tenant\WebPageTranslation;

class WebPageController extends Controller
{
    //
    public function show(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();
            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_page = DB::connection('tenant')->table('mo_web_page')
                    ->select(
                        'mo_web_page.id',
                        'mo_web_page.type'
                    )
                    ->whereNull('mo_web_page.deleted_at')
                    ->leftJoin('mo_web_page_translation', function ($join){
                        $join->on('mo_web_page_translation.page_id','mo_web_page.id')
                            ->whereNull('mo_web_page_translation.deleted_at');
                    });

                $sub = DB::connection('tenant')->table('mo_web_page')
                    ->select(
                        'mo_web_page.id'
                    )
                    ->whereNull('mo_web_page.deleted_at')
                    ->leftJoin('mo_web_page_translation', function ($join){
                        $join->on('mo_web_page_translation.page_id','mo_web_page.id')
                            ->whereNull('mo_web_page_translation.deleted_at');
                    });

                if ($request->get('lang') != '') {
                    $sql_page->where('mo_web_page_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_web_page_translation.language_id', '=', $idioma->id);
                }

                if($request->get('type') != '') {
                    $sql_page->where('mo_web_page.type', 'like', '%'.$request->get('type').'%');
                    $sub->where('mo_web_page.type', 'like', '%'.$request->get('type').'%');
                }

                if($request->get('content') != '') {
                    $sql_page->where('mo_web_page_translation.content', 'like', '%'.$request->get('content').'%');
                    $sub->where('mo_web_page_translation.content', 'like', '%'.$request->get('content').'%');
                }

                $orden = 'mo_web_page.type';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_web_page.id';
                            break;
                        case 'type':
                            $orden = 'mo_web_page.type';
                            break;
                        case 'content':
                            $orden = 'mo_web_page_translation.content';
                            break;
                        default:
                            $orden = 'mo_web_page.type';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_page = $sql_page->groupBy('mo_web_page.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_page = $sql_page->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_page = $sql_page->get();

                }
                //Fin de paginación

                $sub->groupBy('mo_web_page.id');

                $page_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $page_count->count();

                $array['data'] = array();
                foreach ($datos_page as $page) {
                    $array_page = array();

                    $array_page['id'] = $page->id;
                    $array_page['type'] = $page->type;

                    foreach($idiomas as $idioma) {
                        $traducciones = WebPageTranslation::where('page_id', $page->id)
                            ->select(
                                'id',
                                'language_id',
                                'content'
                            )
                            ->where('language_id', $idioma->id)
                            ->get();

                        foreach($traducciones as $traduccion) {
                            $array_traduccion = array();

                            $array_traduccion['id'] = $traduccion->id;
                            $array_traduccion['language_id'] = $traduccion->language_id;
                            $array_traduccion['content'] = $traduccion->content;

                            $array_page['lang'][][$idioma->abbreviation] = $array_traduccion;
                        }
                    }

                    $array['data'][0]['webpage'][] = $array_page;

                }
                
                $array['total_results'] = $totales;
                
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Páginas');

        }

        return response()->json($array, $array['error']);
    }

    public function showId($id) {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $sql_page = DB::connection('tenant')->table('mo_web_page')
                ->select(
                    'mo_web_page.id',
                    'mo_web_page.type'
                )
                ->whereNull('mo_web_page.deleted_at')
                ->leftJoin('mo_web_page_translation', function ($join){
                    $join->on('mo_web_page_translation.page_id','mo_web_page.id')
                        ->whereNull('mo_web_page_translation.deleted_at');
                })
                ->where('mo_web_page.id', $id);

            $datos_page = $sql_page->get();
            $array['data'] = array();
            foreach($datos_page as $page) {
                $array_page = array();

                $array_page['id'] = $page->id;
                $array_page['type'] = $page->type;

                foreach ($idiomas as $idioma) {
                    $traducciones = WebPageTranslation::where('page_id', $page->id)
                        ->select(
                            'id',
                            'language_id',
                            'content'
                        )->where('language_id', $idioma->id)
                        ->get();
                    
                    foreach($traducciones as $traduccion) {
                        $array_traduccion = array();

                        $array_traduccion['id'] = $traduccion->id;
                        $array_traduccion['language_id'] = $traduccion->language_id;
                        $array_traduccion['content'] = $traduccion->content;

                        $array_page['lang'][][$idioma->abbreviation] = $array_traduccion;
                    }

                }

                $array['data'][0]['webpage'][] = $array_page;


            }

            

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Páginas');
        }

        return response()->json($array, $array['error']);
    }

    public function update(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $array_traducciones = array();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'type' => 'unique:tenant.mo_web_page,type,'. $request->get('id') . ',id,deleted_at,NULL'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            

            foreach ($idiomas as $idioma) {
                if (isset($request->get('content')[$idioma->abbreviation])) {
                    
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $page = WebPage::where('id', $request->get('id'))->first();

                if(!$page) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
    
                    $page->pageTranslation()->delete();

                    foreach ($array_traducciones as $idioma) {
                        WebPageTranslation::create([
                            'page_id' => $page->id,
                            'language_id' => $idioma->id,
                            'content' => $request->get('content')[$idioma->abbreviation]
                        ]);
                    }
                }
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Páginas');

        }

        return response()->json($array, $array['error']);
    }

    public function showType($type, Request $request) {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $idiomas = Language::where('front', 1)->get();
            
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->where('front', 1)->first();

            }

            if($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            }else{
                $sql_page = DB::connection('tenant')->table('mo_web_page')
                ->select(
                    'mo_web_page.id',
                    'mo_web_page.type',
                    'mo_web_page_translation.content'
                )
                ->whereNull('mo_web_page.deleted_at')
                ->leftJoin('mo_web_page_translation', function ($join){
                    $join->on('mo_web_page_translation.page_id','mo_web_page.id')
                        ->whereNull('mo_web_page_translation.deleted_at');
                })
                ->where('mo_web_page.type', $type)
                ->where('mo_web_page_translation.language_id', '=', $idioma->id)
                ->groupBy('mo_web_page.id');

                $datos_page = $sql_page->get();
                $array['data'] = array();
                foreach($datos_page as $page) {
                    $array_page = array();

                    $array_page['id'] = $page->id;
                    $array_page['type'] = $page->type;

                    $array_page['content'] = $page->content;

                    $array['data'][0]['webpage'][] = $array_page;
                }
            }

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Páginas');
        }

        return response()->json($array, $array['error']);
    }
}
