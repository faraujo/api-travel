<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Language;
use App\Models\tenant\SubChannelTranslation;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{

    /**
     * Consulta de permisos
     *
     * Para la consulta de permisos se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable
     * “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPermissions()
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            //Precompiladas
            $sql_modules_subchannel = DB::connection('tenant')->table('mo_module')
                ->select('mo_module.id','mo_module.name')
                ->where('mo_module.deleted_at', '=', null)
                ->join('mo_module_subchannel','mo_module_subchannel.module_id','=','mo_module.id')
                ->whereNull('mo_module_subchannel.deleted_at')
                ->join('mo_permission','mo_permission.module_id','mo_module.id')
                ->whereNull('mo_permission.deleted_at')
                ->groupBy('mo_permission.module_id')
                ->orderBy('mo_module.order','desc');

            $datos_subchannels = DB::connection('tenant')->table('mo_subchannel')
                ->select('mo_subchannel.id')
                ->whereNull('mo_subchannel.deleted_at')
                ->join('mo_subchannel_translation','mo_subchannel_translation.subchannel_id','=', 'mo_subchannel.id')
                ->whereNull('mo_subchannel_translation.deleted_at')
                ->groupBy('mo_subchannel.id')
                ->get();

            $sql_permissions_module = DB::connection('tenant')->table('mo_permission')
                ->select('mo_permission.id', 'mo_permission.name', 'mo_permission.description')
                ->where('mo_permission.deleted_at', '=', null)
                ->orderBy('order','desc');

            $sql_modules = DB::connection('tenant')->table('mo_module')
                ->select('mo_module.id','mo_module.name')
                ->where('mo_module.deleted_at', '=', null)
                ->join('mo_permission','mo_permission.module_id','mo_module.id')
                ->whereNull('mo_permission.deleted_at')
                ->groupBy('mo_permission.module_id')
                ->orderBy('mo_module.order','desc');

            $datos_idiomas = Language::where('front',1)->get();

            //Fin Compiladas

            //Obtiene los módulos generales
            $sql_modules_general_clone = clone $sql_modules;
            $datos_modules_general = $sql_modules_general_clone->where('mo_permission.subchannel','=', 0)
                                    ->where('mo_permission.admin','=',0)
                                    ->groupBy('mo_permission.module_id')
                                    ->get();
            $array['data'] = array();
            $array_permissions = array();
            $array_general = array();
            foreach($datos_modules_general as $module_general){
                //Obtiene permisos de modulo con admin a 0
                $sql_permissions_module_clone = clone $sql_permissions_module;
                $datos_permissions_module = $sql_permissions_module_clone
                    ->where('mo_permission.module_id', '=', $module_general->id)
                    ->where('mo_permission.admin','=',0)
                    ->where('mo_permission.subchannel','=',0)
                    ->get();
                $array_general['permission'][][$module_general->name] = $datos_permissions_module;
            }




            //Obtiene los modulos de administracion global - permiso con campo admin 1
            $sql_modules_clone= clone $sql_modules;
            $datos_modules = $sql_modules_clone->where('mo_permission.admin','=','1')->get();

            $array_modules = array();
            //Se recorren todos los modulos para acceso global modulo
            foreach ($datos_modules as $module){

                //Obtiene permisos de modulo con admin a 1
                $sql_permissions_module_clone = clone $sql_permissions_module;
                $datos_permissions_module = $sql_permissions_module_clone
                                                ->where('mo_permission.module_id', '=', $module->id)
                                                ->where('mo_permission.admin','=',1)
                                                ->get();

                $array_modules['permission'][][$module->name] = $datos_permissions_module;
            }





            //Se recorren los canales disponibles para obtener los permisos por canal
            $array_subcanales = array();
            foreach($datos_subchannels as $subchannel){

                $array_subchannel = array();
                $array_subchannel['id'] = $subchannel->id;

                $array_subchannel['lang'] = array();

                //Obtiene traducciones del canal
                foreach ($datos_idiomas as $idioma) {
                    $traduccion = SubChannelTranslation::where('mo_subchannel_translation.language_id', '=', $idioma->id)
                        ->where('mo_subchannel_translation.subchannel_id', '=', $subchannel->id)
                        ->first();
                    if ($traduccion) {
                        $array_subchannel['lang'][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                    }


                }

                $sql_modules_subchannel_clone = clone $sql_modules_subchannel;

                //Se obtienen los modulos asociados al canal
                $datos_modules_subchannel = $sql_modules_subchannel_clone->where('mo_module_subchannel.subchannel_id','=', $subchannel->id)->where('mo_permission.subchannel','=',1)->get();


                $array_subchannel['permission'] = array();
                //Se obtienen los modulos con permisos por canal
                foreach ($datos_modules_subchannel as $module){

                    //Se obtienen los permisos asociados al modulo
                    $sql_permissions_module_clone = clone $sql_permissions_module;
                    $datos_permissions_module = $sql_permissions_module_clone
                                                    ->where('mo_permission.module_id', '=', $module->id)
                                                    ->where('mo_permission.subchannel','=',1)
                                                    ->get();

                    $array_subchannel['permission'][][$module->name] = $datos_permissions_module;
                }

                $array_subcanales[] = $array_subchannel;



            }
            $array_permissions[]['general'][] = $array_general;
            $array_permissions[]['globalaccess'][] = $array_modules;
            $array_permissions[]['subchannel'] = $array_subcanales;
            $array['data']= $array_permissions;

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
            reportService($e, 'Permisos');
        }

        return response()->json($array, $array['error']);
    }

}