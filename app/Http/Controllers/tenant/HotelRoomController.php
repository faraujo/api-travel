<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\FileTranslation;
use App\Exceptions\Handler;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\HotelRoom;
use App\Models\tenant\HotelRoomFile;
use App\Models\tenant\HotelRoomTranslation;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Support\Facades\DB;

class HotelRoomController extends Controller
{

    /**
     * Función para la creación de tipos de habitación
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'code' => 'required',
                'order' => 'integer|min:0',
                'longitude' => 'numeric',
                'latitude' => 'numeric',
                'pax' => 'required|boolean',
                'hotel_id' => 'integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL|required',
                'category_id' => 'required|integer|min:0|exists:tenant.mo_hotel_room_category,id,deleted_at,NULL',
                'uom_id' => 'required|integer|min:0|exists:tenant.mo_uom,id,deleted_at,NULL',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('location')[$idioma->abbreviation]) ||
                    isset($request->get('views')[$idioma->abbreviation]) || isset($request->get('size')[$idioma->abbreviation]) ||
                    isset($request->get('capacity')[$idioma->abbreviation]) || isset($request->get('url_360')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation]) ||
                    isset($request->get('title_seo')[$idioma->abbreviation]) || isset($request->get('description_seo')[$idioma->abbreviation]) ||
                    isset($request->get('keywords_seo')[$idioma->abbreviation]) || isset($request->get('legal')[$idioma->abbreviation]) ||
                    isset($request->get('services_included')[$idioma->abbreviation]) || isset($request->get('services_not_included')[$idioma->abbreviation]) ||

                    isset($request->get('breadcrumb')[$idioma->abbreviation]) || isset($request->get('rel')[$idioma->abbreviation]) ||
                    isset($request->get('og_title')[$idioma->abbreviation]) || isset($request->get('og_description')[$idioma->abbreviation]) ||
                    isset($request->get('og_image')[$idioma->abbreviation]) || isset($request->get('twitter_title')[$idioma->abbreviation]) ||
                    isset($request->get('twitter_description')[$idioma->abbreviation]) || isset($request->get('twitter_image')[$idioma->abbreviation]) ||
                    isset($request->get('script_head')[$idioma->abbreviation]) || isset($request->get('canonical_url')[$idioma->abbreviation]) ||
                    isset($request->get('script_body')[$idioma->abbreviation]) || isset($request->get('script_footer')[$idioma->abbreviation]) ||
                    isset($request->get('index')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',

                        'index.' . $idioma->abbreviation => 'boolean',
                        'og_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                        'twitter_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $room = HotelRoom::create([
                    'hotel_id' => $request->get('hotel_id'),
                    'category_id' => $request->get('category_id'),
                    'code' => $request->get('code'),
                    'uom_id' => $request->get('uom_id'),
                    'order' => ($request->get('order') != '') ? $request->get('order') : 0,
                    'longitude' => ($request->get('longitude') != '') ? $request->get('longitude') : null,
                    'latitude' => ($request->get('latitude') != '') ? $request->get('latitude') : null,
                    'address' => $request->get('address') ? $request->get('address') : null,
                    'pax' => $request->get('pax'),
                ]);

                // Crear las traducciones de la habitacion
                foreach ($array_traducciones as $idioma) {

                    //Preparación
                    $friendly = null;
                    if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                        $friendly = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                    } else {
                        if ($request->get('name')[$idioma->abbreviation] != '') {
                            $friendly = Str::slug($request->get('name')[$idioma->abbreviation]);
                        }
                    }
                    //

                    HotelRoomTranslation::create(
                        [
                            'room_id' => $room->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'language_id' => $idioma->id,
                            'description' => $request->get('description')[$idioma->abbreviation],
                            'short_description' => $request->get('short_description')[$idioma->abbreviation],
                            'location' => isset($request->get('location')[$idioma->abbreviation]) ? $request->get('location')[$idioma->abbreviation] : null,
                            'views' => isset($request->get('views')[$idioma->abbreviation]) ? $request->get('views')[$idioma->abbreviation] : null,
                            'size' => isset($request->get('size')[$idioma->abbreviation]) ? $request->get('size')[$idioma->abbreviation] : null,
                            'capacity' => isset($request->get('capacity')[$idioma->abbreviation]) ? $request->get('capacity')[$idioma->abbreviation] : null,
                            'url_360' => isset($request->get('url_360')[$idioma->abbreviation]) ? $request->get('url_360')[$idioma->abbreviation] : null,
                            'friendly_url' => $friendly,
                            'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                            'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                            'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,
                            'services_included' => isset($request->get('services_included')[$idioma->abbreviation]) ? $request->get('services_included')[$idioma->abbreviation] : null,
                            'services_not_included' => isset($request->get('services_not_included')[$idioma->abbreviation]) ? $request->get('services_not_included')[$idioma->abbreviation] : null,

                            'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                            'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                            'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                            'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                            'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                            'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                            'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                            'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                            'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                            'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                            'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                            'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                            'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                        ]
                    );
                    if ($request->get('files') != '') {
                        foreach ($request->get('files') as $file) {
                            HotelRoomFile::create([
                                'room_id' => $room->id,
                                'file_id' => $file,
                            ]);
                        }
                    }


                }
                // FIN crear las traducciones del producto

            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Habitaciones de hotel');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para el borrado de tipos de habitación
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_hotel_room,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {

                    foreach ($request->get('ids') as $room_id) {

                        $room = HotelRoom::where('id', $room_id)->first();
                        $room->roomTranslation()->delete();
                        $room->delete();
                    }

                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Habitaciones de hotel');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una habitación.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
                'category_id' => 'required|integer|min:0|exists:tenant.mo_hotel_room_category,id,deleted_at,NULL',
                'code' => 'required',
                'order' => 'integer|min:0',
                'longitude' => 'numeric',
                'latitude' => 'numeric',
                'pax' => 'required|boolean',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                'uom_id' => 'required|integer|min:0|exists:tenant.mo_uom,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }


            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('location')[$idioma->abbreviation]) ||
                    isset($request->get('views')[$idioma->abbreviation]) || isset($request->get('size')[$idioma->abbreviation]) ||
                    isset($request->get('capacity')[$idioma->abbreviation]) || isset($request->get('url_360')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation]) ||
                    isset($request->get('title_seo')[$idioma->abbreviation]) || isset($request->get('description_seo')[$idioma->abbreviation]) ||
                    isset($request->get('keywords_seo')[$idioma->abbreviation]) || isset($request->get('legal')[$idioma->abbreviation]) ||
                    isset($request->get('services_included')[$idioma->abbreviation]) || isset($request->get('services_not_included')[$idioma->abbreviation]) ||

                    isset($request->get('breadcrumb')[$idioma->abbreviation]) || isset($request->get('rel')[$idioma->abbreviation]) ||
                    isset($request->get('og_title')[$idioma->abbreviation]) || isset($request->get('og_description')[$idioma->abbreviation]) ||
                    isset($request->get('og_image')[$idioma->abbreviation]) || isset($request->get('twitter_title')[$idioma->abbreviation]) ||
                    isset($request->get('twitter_description')[$idioma->abbreviation]) || isset($request->get('twitter_image')[$idioma->abbreviation]) ||
                    isset($request->get('script_head')[$idioma->abbreviation]) || isset($request->get('canonical_url')[$idioma->abbreviation]) ||
                    isset($request->get('script_body')[$idioma->abbreviation]) || isset($request->get('script_footer')[$idioma->abbreviation]) ||
                    isset($request->get('index')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',

                        'index.' . $idioma->abbreviation => 'boolean',
                        'og_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                        'twitter_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación


            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $room = HotelRoom::find($request->get('id'));
                if (!$room) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    $room->roomFile()->delete();
                    // Actualizar producto
                    $room->code = $request->get('code');
                    $room->uom_id = $request->get('uom_id');
                    $room->category_id = $request->get('category_id');
                    $room->order = $request->get('order') != '' ? $request->get('order') : 0;
                    $room->longitude = $request->get('longitude') != '' ? $request->get('longitude') : null;
                    $room->latitude = $request->get('latitude') != '' ? $request->get('latitude') : null;
                    $room->address = $request->get('address') != '' ? $request->get('address') : null;
                    $room->pax = $request->get('pax');


                    $room->save();

                    // Actualizar traducciones del producto
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        // Preparación de datos
                        $friendly_url = null;

                        if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                            $friendly_url = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                        } else {
                            $friendly_url = Str::slug($request->get('name')[$idioma->abbreviation]);
                        }
                        //

                        $traduccion = $room->roomTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {

                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'location' =>  isset($request->get('location')[$idioma->abbreviation]) ? $request->get('location')[$idioma->abbreviation] : null,
                                'views' =>  isset($request->get('views')[$idioma->abbreviation]) ? $request->get('views')[$idioma->abbreviation] : null,
                                'size' =>  isset($request->get('size')[$idioma->abbreviation]) ? $request->get('size')[$idioma->abbreviation] : null,
                                'capacity' =>  isset($request->get('capacity')[$idioma->abbreviation]) ? $request->get('capacity')[$idioma->abbreviation] : null,
                                'url_360' =>  isset($request->get('url_360')[$idioma->abbreviation]) ? $request->get('url_360')[$idioma->abbreviation] : null,
                                'friendly_url' => $friendly_url,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                                'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,
                                'services_included' => isset($request->get('services_included')[$idioma->abbreviation]) ? $request->get('services_included')[$idioma->abbreviation] : null,
                                'services_not_included' => isset($request->get('services_not_included')[$idioma->abbreviation]) ? $request->get('services_not_included')[$idioma->abbreviation] : null,

                                'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                                'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                                'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                                'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                                'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                                'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                                'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                                'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                                'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                                'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                                'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                                'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                                'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                            ]);
                        } else {

                            HotelRoomTranslation::create([
                                'room_id' => $room->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'language_id' => $idioma->id,
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'location' => isset($request->get('location')[$idioma->abbreviation]) ? $request->get('location')[$idioma->abbreviation] : null,
                                'views' => isset($request->get('views')[$idioma->abbreviation]) ? $request->get('views')[$idioma->abbreviation] : null,
                                'size' => isset($request->get('size')[$idioma->abbreviation]) ? $request->get('size')[$idioma->abbreviation] : null,
                                'capacity' => isset($request->get('capacity')[$idioma->abbreviation]) ? $request->get('capacity')[$idioma->abbreviation] : null,
                                'url_360' =>  isset($request->get('url_360')[$idioma->abbreviation]) ? $request->get('url_360')[$idioma->abbreviation] : null,
                                'friendly_url' => $friendly_url,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                                'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,
                                'services_included' => isset($request->get('services_included')[$idioma->abbreviation]) ? $request->get('services_included')[$idioma->abbreviation] : null,
                                'services_not_included' => isset($request->get('services_not_included')[$idioma->abbreviation]) ? $request->get('services_not_included')[$idioma->abbreviation] : null,

                                'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                                'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                                'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                                'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                                'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                                'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                                'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                                'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                                'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                                'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                                'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                                'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                                'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                            ]);
                        }
                    }
                    $room->roomTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de producto

                    //Crear relación con archivo
                    if ($request->get('files') != '') {
                        foreach ($request->get('files') as $file) {
                            HotelRoomFile::create([
                                'room_id' => $room->id,
                                'file_id' => $file,
                            ]);
                        }
                    }
                    //FIN crear relación con archivo

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Habitaciones de hotel');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar todas las habitaciones
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'hotel_id' => 'integer|min:0',
                'category_id' => 'integer|min:0',
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $sql_rooms = DB::connection('tenant')->table('mo_hotel_room')
                    ->select(
                        'mo_hotel_room.id',
                        'mo_hotel_room.hotel_id',
                        'mo_hotel_room.category_id',
                        'mo_hotel_room.code',
                        'mo_hotel_room.uom_id',
                        'mo_hotel_room.order',
                        'mo_hotel_room.longitude',
                        'mo_hotel_room.latitude',
                        'mo_hotel_room.address',
                        'mo_hotel_room.pax')
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->join('mo_hotel_room_translation', 'mo_hotel_room_translation.room_id', 'mo_hotel_room.id')
                    ->whereNull('mo_hotel_room_translation.deleted_at');

                $sub = DB::connection('tenant')->table('mo_hotel_room')
                    ->select('mo_hotel_room.id')
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->join('mo_hotel_room_translation', 'mo_hotel_room_translation.room_id', 'mo_hotel_room.id')
                    ->whereNull('mo_hotel_room_translation.deleted_at');

                if ($request->get('lang') != '') {
                    $sql_rooms->where('mo_hotel_room_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_hotel_room_translation.language_id', '=', $idioma->id);
                }

                if ($request->get('hotel_id') != '') {
                    $sql_rooms->where('mo_hotel_room.hotel_id', $request->get('hotel_id'));
                    $sub->where('mo_hotel_room.hotel_id', $request->get('hotel_id'));
                }

                if ($request->get('category_id') != '') {
                    $sql_rooms->where('mo_hotel_room.category_id', $request->get('category_id'));
                    $sub->where('mo_hotel_room.category_id', $request->get('category_id'));
                }

                if ($request->get('code') != '') {
                    $sql_rooms->where('mo_hotel_room.code', 'Like', '%' . $request->get('code') . '%');
                    $sub->where('mo_hotel_room.code', 'Like', '%' . $request->get('code') . '%');
                }

                if ($request->get('name') != '') {
                    $sql_rooms->where('mo_hotel_room_translation.name', 'Like', '%' . $request->get('name') . '%');
                    $sub->where('mo_hotel_room_translation.name', 'Like', '%' . $request->get('name') . '%');
                }

                // Order
                $orden = 'mo_hotel_room_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_hotel_room_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_hotel_room_translation.description';
                            break;
                        case 'short_description':
                            $orden = 'mo_hotel_room_translation.short_description';
                            break;
                        case 'hotel_id':
                            $orden = 'mo_hotel_room.hotel_id';
                            break;
                        case 'category_id':
                            $orden = 'mo_hotel_room.category_id';
                            break;
                        case 'id':
                            $orden = 'mo_hotel_room.id';
                            break;
                        case 'order':
                            $orden = 'mo_hotel_room.order';
                            break;
                        default:
                            $orden = 'mo_hotel_room_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_rooms = $sql_rooms->groupBy('mo_hotel_room.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_rooms = $sql_rooms->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_rooms = $sql_rooms->get();

                }

                //Fin de paginación

                $sub->groupBy('mo_hotel_room_translation.room_id');
                $rooms_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $rooms_count->count();

                $array['data'] = array();
                foreach ($datos_rooms as $room) {
                    foreach ($idiomas as $idi) {
                        $traduccion = HotelRoomTranslation::where('room_id', '=', $room->id)
                            ->select(
                                'id',
                                'language_id',
                                'name',
                                'description',
                                'short_description',
                                'location',
                                'views',
                                'size',
                                'capacity',
                                'url_360',
                                'friendly_url',
                                'title_seo',
                                'description_seo',
                                'keywords_seo',
                                'legal',
                                'services_included',
                                'services_not_included',
                            'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $room->lang[][$idi->abbreviation] = $trad;
                        }
                    }
                    $array['data'][0]['room'][] = $room;
                }

                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Habitaciones de hotel');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Funcion para mostrar una habitación
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            $idiomas = Language::where('front',1)->get();

            $sql_rooms = DB::connection('tenant')->table('mo_hotel_room')
                ->select(
                    'mo_hotel_room.id',
                    'mo_hotel_room.hotel_id',
                    'mo_hotel_room.category_id',
                    'mo_hotel_room.code',
                    'mo_hotel_room.uom_id',
                    'mo_hotel_room.order',
                    'mo_hotel_room.longitude',
                    'mo_hotel_room.latitude',
                    'mo_hotel_room.address',
                    'mo_hotel_room.pax')
                ->whereNull('mo_hotel_room.deleted_at')
                ->join('mo_hotel_room_translation', 'mo_hotel_room_translation.room_id', 'mo_hotel_room.id')
                ->whereNull('mo_hotel_room_translation.deleted_at');


            $sql_rooms->where('mo_hotel_room.id', $id);
            $sql_rooms->groupBy('mo_hotel_room.id');
            $datos_rooms = $sql_rooms->get();

            $storage = Settings::where('name', 'storage_files')->first()->value;

            $array['data'] = array();
            foreach ($datos_rooms as $room){

                $files = DB::connection('tenant')->table('mo_hotel_room_file')
                    ->select('mo_file.id', 'mo_file.file_name','mo_file.file_size','mo_file.file_dimensions','mo_file.mimetype','mo_file.type_id', 'mo_file.order', 'mo_file.url_file',
                        'mo_file.device_id','mo_file.key_file')
                    ->where('mo_hotel_room_file.room_id', '=', $room->id)
                    ->where('mo_hotel_room_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_hotel_room_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->groupBy('mo_file.id')->get();

                $room->files = array();
                $array_file = array();
                foreach ($files as $file) {

                    //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                    $tipo = FileType::find($file->type_id);

                    $array_file = ['id' => $file->id, 'file_name' => $file->file_name,'file_size' => convertExtension($file->file_size),
                        'file_dimensions' => $file->file_dimensions,'mimetype' => $file->mimetype,'order' => $file->order, 'url_file' => $storage . $file->url_file,
                        'device_id' => $file->device_id,'key_file' => $file->key_file, 'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];
                    foreach ($idiomas as $idi) {

                        //si el tipo no existe o está borrado no busca sus traducciones
                        if ($tipo) {
                            $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_type_translation.type_id')
                                ->get();

                            foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                            }
                        }

                        $traducciones_files = FileTranslation::where('file_id', $file->id)
                            ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                            ->where('language_id', '=', $idi->id)
                            ->groupBy('mo_file_translation.file_id')
                            ->get();

                        foreach ($traducciones_files as $traduccion_file) {
                            $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                        }
                    }
                    $room->files[] = $array_file;
                }

                foreach ($idiomas as $idi) {
                    $traduccion = HotelRoomTranslation::where('room_id', '=', $room->id)
                        ->select('id', 'language_id', 'name', 'description', 'short_description', 'location', 'views', 'size', 'capacity','url_360', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal', 'services_included', 'services_not_included',
                            'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                        ->where('language_id', '=', $idi->id)
                        ->get();
                    foreach ($traduccion as $trad) {
                        if ($trad->og_image && $trad->og_image != ''){
                            $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->og_image)->first()->url_file;
                            $trad->og_image_route = $storage . $sql_file_seo;
                        }else{
                            $trad->og_image_route = null;
                        }
                        if ($trad->twitter_image && $trad->twitter_image != ''){
                            $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->twitter_image)->first()->url_file;
                            $trad->twitter_image_route = $storage . $sql_file_seo;
                        }else{
                            $trad->twitter_image_route = null;
                        }
                        $room->lang[][$idi->abbreviation] = $trad;
                    }
                }

                $array['data'][0]['room'][] = $room;
            }


            DB::connection('tenant')->commit();



        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Habitaciones de hotel');
        }

        return response()->json($array, $array['error']);
    }

}