<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Models\tenant\SubChannel;
use App\Models\tenant\SubChannelTranslation;
use App\Models\tenant\Channel;
use App\Models\tenant\ChannelTranslation;
use App\Models\tenant\ModuleSubchannel;
use App\Models\tenant\RolePermission;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SubChannelController extends Controller
{


    /**
     * Función para mostrar los subcanales existentes.
     *
     * Para la consulta de subcanales se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'channel_id' => 'integer|min:0|exists:tenant.mo_channel,id,deleted_at,NULL',
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',

            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = Language::where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $subchannels = DB::connection('tenant')->table('mo_subchannel')
                    ->select('mo_subchannel.id', 'mo_subchannel.channel_id', 'mo_subchannel.contract_model_id', 'mo_subchannel.reservation_code', 'mo_subchannel.view_blocked', 'mo_subchannel.prefix')
                    ->join('mo_subchannel_translation', 'mo_subchannel.id', '=', 'mo_subchannel_translation.subchannel_id')
                    ->where('mo_subchannel_translation.deleted_at', '=', null)
                    ->where('mo_subchannel.deleted_at', '=', null)
                    ->leftJoin('mo_channel', function ($join) {
                        $join->on('mo_channel.id', '=', 'mo_subchannel.channel_id')->where('mo_channel.deleted_at', null);
                    })
                    ->leftjoin('mo_channel_translation', function ($join) {
                        $join->on('mo_channel_translation.channel_id', '=', 'mo_subchannel.channel_id')->where('mo_channel_translation.deleted_at', null);
                    });

                $sub = DB::connection('tenant')->table('mo_subchannel')
                    ->select('mo_subchannel.id')
                    ->join('mo_subchannel_translation', 'mo_subchannel.id', '=', 'mo_subchannel_translation.subchannel_id')
                    ->where('mo_subchannel_translation.deleted_at', '=', null)
                    ->where('mo_subchannel.deleted_at', '=', null)
                    ->leftJoin('mo_channel', function ($join) {
                        $join->on('mo_channel.id', '=', 'mo_subchannel.channel_id')->where('mo_channel.deleted_at', null);
                    })
                    ->leftjoin('mo_channel_translation', function ($join) {
                        $join->on('mo_channel_translation.channel_id', '=', 'mo_subchannel.channel_id')->where('mo_channel_translation.deleted_at', null);
                    });

                if ($request->get('channel_id') != '') {
                    $subchannels->where('mo_subchannel.channel_id', $request->get('channel_id'));
                    $sub->where('mo_subchannel.channel_id', $request->get('channel_id'));
                }

                if ($request->get('lang') != '') {
                    $subchannels->where('mo_subchannel_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_subchannel_translation.language_id', '=', $idioma->id);
                }

                if ($request->get('name') != '') {
                    $subchannels->where('mo_subchannel_translation.name', 'Like', '%' . $request->get('name') . '%');
                    $sub->where('mo_subchannel_translation.name', 'Like', '%' . $request->get('name') . '%');
                }


                // Order
                $orden = 'mo_subchannel_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_subchannel.id';
                            break;
                        case 'name':
                            $orden = 'mo_subchannel_translation.name';
                            break;
                        default:
                            $orden = 'mo_subchannel_translation.name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $subchannels->groupBy('mo_subchannel.id')->orderBy($orden, $sentido);



                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $subchannels = $subchannels->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $subchannels = $subchannels->get();

                }

                //Fin de paginación

                $sub->groupBy('mo_subchannel.id');
                $subchannels_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $subchannels_count->count();


                $array['data'] = array();

                foreach ($subchannels as $subchannel) {

                    $subchannel->channel = null;

                    foreach ($idiomas as $idioma) {

                        $datos_channel_traduccion = DB::connection('tenant')->table('mo_channel_translation')
                            ->select('mo_channel_translation.language_id', 'mo_channel_translation.name')
                            ->where('mo_channel_translation.channel_id', '=', $subchannel->channel_id)
                            ->where('mo_channel_translation.language_id', '=', $idioma->id)
                            ->where('mo_channel_translation.deleted_at', '=', null)
                            ->get();

                        foreach ($datos_channel_traduccion as $channel_traduccion) {
                            $subchannel->channel['id'] = $subchannel->channel_id;
                            $subchannel->channel['lang'][][$idioma->abbreviation] = $channel_traduccion;
                        }

                        $traduccion = SubChannelTranslation::where('subchannel_id', '=', $subchannel->id)
                            ->where('language_id', '=', $idioma->id)
                            ->select('id', 'language_id', 'name')
                            ->get();

                        foreach ($traduccion as $trad) {
                            $subchannel->lang[][$idioma->abbreviation] = $trad;
                        }

                    }

                    $array['data'][0]['subchannel'][] = ['id' => $subchannel->id, 'contract_model_id' => $subchannel->contract_model_id,
                        'channel' => $subchannel->channel, 'view_blocked' => $subchannel->view_blocked, 'reservation_code' => $subchannel->reservation_code, 'prefix' => $subchannel->prefix, 'lang' => $subchannel->lang];

                }

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Subcanales');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra un subcanal buscado por su id
     *
     * Para la consulta de un subchannel se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // FIN Validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $subchannels = DB::connection('tenant')->table('mo_subchannel')
                    ->select('mo_subchannel.id','mo_subchannel.channel_id','mo_subchannel.contract_model_id', 'mo_subchannel.prefix', 'mo_subchannel.reservation_code', 'mo_subchannel.view_blocked', 'mo_subchannel.authorization_token', 'mo_subchannel.api_code', 'mo_subchannel.cashier', 'mo_subchannel.sendconfirmedemail')
                    ->join('mo_subchannel_translation', 'mo_subchannel.id', '=', 'mo_subchannel_translation.subchannel_id')
                    ->where('mo_subchannel_translation.deleted_at', '=', null)
                    ->where('mo_subchannel.deleted_at', '=', null);

                if ($request->get('lang') != '') {
                    $subchannels->where('language_id', '=', $idioma->id);
                }

                $subchannels->where('mo_subchannel.id', '=', $id);

                $subchannels->groupBy('mo_subchannel.id');
                $subchannels = $subchannels->get();

                $array['data'] = array();
                foreach ($subchannels as $subchannel) {
                    if ($request->get('lang') != '') {
                        $traducciones = SubChannelTranslation::where('language_id', '=', $idioma->id)->where('subchannel_id', '=', $subchannel->id)
                            ->select('name')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $subchannel->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = SubChannelTranslation::where('subchannel_id', '=', $subchannel->id)
                                ->select('name')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $subchannel->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }

                    $modules = ModuleSubchannel::where('subchannel_id', '=', $subchannel->id)
                    ->select('module_id')
                    ->where('mo_module_subchannel.deleted_at', '=', null)
                    ->get();
                    
                    $module_data =  [];
                    foreach ($modules as $module) {
                        $module_data[]['id'] = $module->module_id;
                    }
                    $subchannel->module = $module_data;
                    $array['data'][0]['subchannel'][] = $subchannel;
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Subcanales');
        }

        return response()->json($array, $array['error']);

    }

    /**
     * Función para crear subcanales
     *
     * Para crear subcanales se realiza una petición POST.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'channel_id' => 'required|integer|min:0|exists:tenant.mo_channel,id,deleted_at,NULL',
                'contract_model_id' => 'required|integer|min:0|exists:tenant.mo_contract_model,id,deleted_at,NULL',
                'reservation_code' => 'integer|min:0',
                'view_blocked' => 'required|boolean',
                'sendconfirmedemail' => 'boolean',
                'modules' => 'array',
                'modules.*.id' => 'required|integer|min:0|exists:tenant.mo_module,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_subchannel = $rules->where('code', 'subchannel');

                            if($rules_subchannel) {
                                foreach($rules_subchannel as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'subchannel') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                    } else if ($cantidad > 0) {
                        $total_subchannels = Subchannel::get();

                        if(count($total_subchannels) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'subchannel';
                        }
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    // Crear el subcanal.
                    $subchannel = Subchannel::create([
                        'channel_id' => $request->get('channel_id'),
                        'contract_model_id' => $request->get('contract_model_id'),
                        'prefix' => $request->get('prefix') != '' ? $request->get('prefix') : null,
                        'api_code' => $request->get('api_code') != '' ? $request->get('api_code') : null,
                        'reservation_code' => $request->get('reservation_code') != '' ? $request->get('reservation_code') : 0,
                        'view_blocked' => $request->get('view_blocked'),
                        'authorization_token'=> Str::random(16),
                        'sendconfirmedemail' => $request->get('sendconfirmedemail') != '' ? $request->get('sendconfirmedemail') : 0,
                    ]);
                    // FIN crear el subcanal

                    // Crear las traducciones para el subcanal
                    foreach ($array_traducciones as $idioma) {

                        SubChannelTranslation::create(
                            [
                                'subchannel_id' => $subchannel->id,
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]
                        );
                    }
                    // FIN crear las traducciones del subcanal


                    // Crear la relación módulo-subcanal
                    if ($request->get('modules') != '') {
                        foreach ($request->get('modules') as $module) {
                            ModuleSubchannel::create([
                                'module_id' => $module['id'],
                                'subchannel_id' => $subchannel->id,
                            ]);
                        }
                    }
                    // FIN crear la relación módulo-subcanal

                    // Crear la relación rol administrador - permisos por subcanal
                    $subchannel_permissions = DB::connection('tenant')->table('mo_permission')
                        ->where('mo_permission.subchannel', 1)
                        ->whereNull('deleted_at')
                        ->get();

                    foreach ($subchannel_permissions as $permission) {
                        RolePermission::create([
                            'role_id' => 1,
                            'permission_id' => $permission->id,
                            'subchannel_id' => $subchannel->id,
                        ]);
                    }                    
                    // FIN crear la relación rol administrador - permisos por subcanal
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Subcanales');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para modificar los subcanales existentes.
     *
     * Para la modificación de subcanales se realiza una petición PUT.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();


            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'channel_id' => 'required|integer|min:0|exists:tenant.mo_channel,id,deleted_at,NULL',
                'contract_model_id' => 'required|integer|min:0|exists:tenant.mo_contract_model,id,deleted_at,NULL',
                'reservation_code' => 'integer|min:0',
                'view_blocked' => 'required|boolean',
                'sendconfirmedemail' => 'boolean',
                'modules' => 'array',
                'modules.*.id' => 'required|integer|min:0|exists:tenant.mo_module,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $subchannel = SubChannel::where('id', '=', $request->get('id'))->first();

                if (!$subchannel) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar subcanal
                    $subchannel->channel_id = $request->get('channel_id');
                    $subchannel->contract_model_id = $request->get('contract_model_id');
                    $subchannel->prefix = $request->get('prefix') != '' ? $request->get('prefix') : null;
                    $subchannel->api_code = $request->get('api_code') != '' ? $request->get('api_code') : null;
                    $subchannel->reservation_code = $request->get('reservation_code') != '' ? $request->get('reservation_code') : 0;
                    $subchannel->view_blocked = $request->get('view_blocked');
                    $subchannel->sendconfirmedemail = $request->get('sendconfirmedemail') != '' ? $request->get('sendconfirmedemail') : 0;
                    $subchannel->save();
                    //FIN actualizar subcanal

                    // Actualizar traducciones del subcanal
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        $traduccion = $subchannel->subchannelTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {
                            //Si existe traducción lo actualiza
                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);
                        } else {
                            //Si no existe traducción la crea
                            SubChannelTranslation::create([
                                'subchannel_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);
                        }
                    }
                    $subchannel->subchannelTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de subcanal

                    $subchannel->moduleSubchannel()->delete();

                    // Crear la relación módulo-subcanal
                    if ($request->get('modules') != '') {
                        foreach ($request->get('modules') as $module) {
                            ModuleSubchannel::create([
                                'module_id' => $module['id'],
                                'subchannel_id' => $subchannel->id,
                            ]);
                        }
                    }
                    // FIN crear la relación módulo-subcanal                   

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Subcanales');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de uno o más subcanales.
     *
     * Para la eliminación de subcanales se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Subchannel 1 no se puede eliminar
            if($error == 0 && in_array(1, $request->get('ids'))){
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['web' => ['This channel cannot be removed']]);
            }
            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_subchannel,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {

                    foreach ($request->get('ids') as $subchannel_id) {

                        $subcanal = Subchannel::where('id', $subchannel_id)->first();
                        $subcanal->subchannelTranslation()->delete();
                        $subcanal->moduleSubchannel()->delete();
                        $subcanal->rolePermissionSubchannel()->delete();
                        $subcanal->delete();
                    }

                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Subcanales');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar los canales existentes.
     *
     * Para la consulta de canales se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showChannels(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',

            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = Language::where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $channels = DB::connection('tenant')->table('mo_channel')
                    ->select('mo_channel.id')
                    ->join('mo_channel_translation', 'mo_channel.id', '=', 'mo_channel_translation.channel_id')
                    ->where('mo_channel.deleted_at', '=', null)
                    ->where('mo_channel_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_channel')
                    ->select('mo_channel.id')
                    ->join('mo_channel_translation', 'mo_channel.id', '=', 'mo_channel_translation.channel_id')
                    ->where('mo_channel.deleted_at', '=', null)
                    ->where('mo_channel_translation.deleted_at', '=', null);

                if ($request->get('lang') != '') {
                    $channels->where('mo_channel_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_channel_translation.language_id', '=', $idioma->id);
                }

                if ($request->get('name') != '') {
                    $channels->where('mo_channel_translation.name', 'Like', '%' . $request->get('name') . '%');
                    $sub->where('mo_channel_translation.name', 'Like', '%' . $request->get('name') . '%');
                }


                // Order
                $orden = 'name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'id';
                            break;
                        default:
                            $orden = 'name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $channels->groupBy('mo_channel.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $channels = $channels->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $channels = $channels->get();

                }

                //Fin de paginación

                $sub->groupBy('mo_channel.id');
                $channels_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $channels_count->count();


                $array['data'] = array();

                foreach ($channels as $channel) {


                    foreach ($idiomas as $idioma) {

                        $traduccion = ChannelTranslation::where('channel_id', '=', $channel->id)
                            ->where('language_id', '=', $idioma->id)
                            ->select('id', 'language_id', 'name')
                            ->get();

                        foreach ($traduccion as $trad) {
                            $channel->lang[][$idioma->abbreviation] = $trad;
                        }

                    }

                    $array['data'][0]['channel'][] = ['id' => $channel->id, 'lang' => $channel->lang];

                }

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Canales');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función que muestra el canal buscado por su id
     *
     * Para la consulta de un canal se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showIdChannel($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // FIN Validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $channels = DB::connection('tenant')->table('mo_channel')
                    ->select('mo_channel.id')
                    ->join('mo_channel_translation', 'mo_channel.id', '=', 'mo_channel_translation.channel_id')
                    ->where('mo_channel_translation.deleted_at', '=', null)
                    ->where('mo_channel.deleted_at', '=', null);

                if ($request->get('lang') != '') {
                    $channels->where('language_id', '=', $idioma->id);
                }

                $channels->where('mo_channel.id', '=', $id);

                $channels->groupBy('mo_channel.id');
                $channels = $channels->get();

                $array['data'] = array();
                foreach ($channels as $channel) {
                    if ($request->get('lang') != '') {
                        $traducciones = ChannelTranslation::where('language_id', '=', $idioma->id)->where('channel_id', '=', $channel->id)
                            ->select('id', 'language_id', 'name')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $channel->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = ChannelTranslation::where('channel_id', '=', $channel->id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $channel->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['channel'][] = $channel;
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Canales');
        }

        return response()->json($array, $array['error']);

    }


    /**
     * Función para la creación de un canal y sus traducciones
     *
     * Para la creación de canales se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createChannel(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                // Crear el canal.
                $canal = Channel::create();
                // FIN crear el canal

                // Crear las traducciones para el canal
                foreach ($array_traducciones as $idioma) {

                    ChannelTranslation::create(
                        [
                            'channel_id' => $canal->id,
                            'language_id' => $idioma->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                        ]
                    );
                }
                // FIN crear las traducciones del canal
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Canales');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para modificar un canal.
     *
     * Para la modificación de canales se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function updateChannel(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $canal = Channel::find($request->get('id'));
                if (!$canal) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar traducciones del canal
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        $traduccion = $canal->channelTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {

                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);
                        } else {

                            ChannelTranslation::create([
                                'channel_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);
                        }
                    }
                    $canal->channelTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de canal

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Canales');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para la eliminación de un canal.
     *
     * Para la eliminación de canales se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteChannel(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Channel 1 no se puede eliminar
            if($error == 0 && in_array(1, $request->get('ids'))){
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['web' => ['This channel cannot be removed']]);
            }
            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_channel,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $channel_id) {
                        $channel = Channel::where('id', $channel_id)->first();
                        $channel->channelTranslation()->delete();
                        $channel->delete();

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Canales');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Consulta de módulos aplicables al subcanal
     *
     * Para la consulta de módulos se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable
     * “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showSubchannelModules()
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            //Precompiladas
            $sql_modules_subchannel = DB::connection('tenant')->table('mo_module')
                ->select('mo_module.id','mo_module.name')
                ->where('mo_module.deleted_at', '=', null)
                ->join('mo_permission','mo_permission.module_id','mo_module.id')
                ->whereNull('mo_permission.deleted_at')
                ->where('mo_permission.subchannel', '=', 1);

            $sub = DB::connection('tenant')->table('mo_module')
                ->select('mo_module.id','mo_module.name')
                ->where('mo_module.deleted_at', '=', null)
                ->join('mo_permission','mo_permission.module_id','mo_module.id')
                ->whereNull('mo_permission.deleted_at')
                ->where('mo_permission.subchannel', '=', 1);

            //Se obtienen los modulos asociados a canal
            $datos_modules_subchannel = $sql_modules_subchannel->groupBy('mo_permission.module_id')->orderBy('mo_module.order','desc')->get();

            $sub->groupBy('mo_permission.module_id');
            $modules_subchannel_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                ->mergeBindings($sub);

            $totales = $modules_subchannel_count->count();

            $array['data'][0]['module'] = $datos_modules_subchannel;
            
            $array['total_results'] = $totales;

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
            reportService($e, 'Subcanales');
        }

        return response()->json($array, $array['error']);
    }

}