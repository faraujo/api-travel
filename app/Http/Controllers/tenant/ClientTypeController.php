<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\ClientType;
use App\Models\tenant\ClientTypeTranslation;
use App\Exceptions\Handler;

use App\Models\tenant\Language;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\tenant\Settings;

class ClientTypeController extends Controller
{

    /**
     * Función para la creación de un tipo de cliente y sus traducciones
     *
     * Para la creación de tipos de cliente se realiza una petición POST. Si la operación no produce errores se devuelve,
     * en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $error = 0;

            // Validación
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'code' => 'required',
                //'name' => 'required|array',
                //'name.*' => 'required',
                'from' => 'integer|min:0|required',
                'to' => 'integer|min:0|required',
                'api_id' => 'integer|min:0',
                'hotel_age_required' => 'boolean',
            ]);


            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if (($request->get('from') != '' && $request->get('to') != '') && ($request->get('from') > $request->get('to'))) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['from' => ['0' => 'The field from must be less than to.']]);
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //FIN de validación
            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                //Crea el tipo de cliente
                $client_type = ClientType::create([
                    'code' => $request->get('code'),
                    'from' => $request->get('from'),
                    'to' => $request->get('to'),
                    'api_id' => $request->get('api_id') != '' ? $request->get('api_id') : null,  
                    'hotel_id' => $request->get('hotel_id') != '' ? $request->get('hotel_id') : null,
                    'hotel_code' => $request->get('hotel_code') != '' ? $request->get('hotel_code') : null,
                    'hotel_age_required' => $request->get('hotel_age_required') != '' ? $request->get('hotel_age_required') : 0, 
                                     
                ]);

                //Crea las traducciones del tipo de cliente
                foreach ($array_traducciones as $idioma) {
                    ClientTypeTranslation::create([
                        'client_type_id' => $client_type->id,
                        'language_id' => $idioma->id,
                        'name' => $request->get('name')[$idioma->abbreviation],
                    ]);
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de cliente');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar un tipo de cliente.
     *
     * Para la modificación de tipos de cliente se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'code' => 'required',
                //'name' => 'required|array',
                //'name.*' => 'required',
                'from' => 'integer|min:0|required',
                'to' => 'integer|min:0|required',
                'api_id' => 'integer|min:0',
                'hotel_age_required' => 'boolean',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if (($request->get('from') != '' && $request->get('to') != '') && ($request->get('from') > $request->get('to'))) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['from' => ['0' => 'The field from must be less than to.']]);
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }

            }
            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $client_type = ClientType::where('id', '=', $request->get('id'))->first();
                if (!$client_type) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    //Actualiza datos tipo de cliente
                    $client_type->from = $request->get('from');
                    $client_type->to = $request->get('to');
                    $client_type->code = $request->get('code');
                    $client_type->api_id = $request->get('api_id') != '' ? $request->get('api_id') : null;  
                    $client_type->hotel_id = $request->get('hotel_id') != '' ? $request->get('hotel_id') : null;
                    $client_type->hotel_code = $request->get('hotel_code') != '' ? $request->get('hotel_code') : null;
                    $client_type->hotel_age_required = $request->get('hotel_age_required') != '' ? $request->get('hotel_age_required') : 0; 

                    $client_type->save();

                    // Actualizar traducciones del tipo de cliente
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        $traduccion = $client_type->clientTypeTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {
                            //Si existe traducción lo actualiza
                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);

                        } else {
                            //Si no existe traducción la crea
                            ClientTypeTranslation::create([
                                'client_type_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],

                            ]);
                        }
                    }
                    $client_type->clientTypeTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de tipos de cliente
                }
            }
            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de cliente');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todos los tipos de cliente
     *
     * Para la consulta de tipos de cliente se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $error = 0;

            // Validación
            $mensaje_validador = collect();


            $validator = Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $sql_client_type = DB::connection('tenant')->table('mo_client_type')
                    ->select('mo_client_type.id', 'mo_client_type.code', 'mo_client_type.from', 'mo_client_type.to',
                            'mo_client_type.api_id', 'mo_client_type.hotel_id', 'mo_client_type.hotel_code', 'mo_client_type.hotel_age_required')
                    ->where('mo_client_type.deleted_at', '=', null)
                    ->join('mo_client_type_translation', 'mo_client_type_translation.client_type_id', 'mo_client_type.id')
                    ->where('mo_client_type_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_client_type')
                    ->select('mo_client_type.id')
                    ->where('mo_client_type.deleted_at', '=', null)
                    ->join('mo_client_type_translation', 'mo_client_type_translation.client_type_id', 'mo_client_type.id')
                    ->where('mo_client_type_translation.deleted_at', '=', null);

                // Recogida de datos enviados por el usuario y establecimiento de filtros
                
                if ($request->get('lang') != '') {
                    $sql_client_type->where('mo_client_type_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_client_type_translation.language_id', '=', $idioma->id);
                }

                if ($request->get('name') != '') {

                    $sql_client_type->Where(function ($query) use ($request) {
                        $query->where('mo_client_type_translation.name', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_client_type_translation.name', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                // Order
                $orden = 'mo_client_type_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_client_type_translation.name';
                            break;
                        case 'code':
                            $orden = 'mo_client_type.code';
                            break;
                        case 'from':
                            $orden = 'mo_client_type.from';
                            break;
                        case 'to':
                            $orden = 'mo_client_type.to';
                            break;
                        case 'id':
                            $orden = 'mo_client_type.id';
                            break;
                        default:
                            $orden = 'mo_client_type_translation.name';
                            break;
                    }
                }

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $client_type = $sql_client_type->groupBy('mo_client_type.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $client_type = $client_type->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $client_type = $client_type->get();

                }

                //Fin de paginación



                $sub->groupBy('mo_client_type_translation.client_type_id');
                $client_type_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $client_type_count->count();


                $array['data'] = array();
                foreach ($client_type as $client) {

                    foreach ($idiomas as $idi) {
                        $traduccion = ClientTypeTranslation::where('client_type_id', '=', $client->id)
                            ->select('id', 'language_id', 'name')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $client->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['client_type'][] = $client;
                }
                $array['total_results'] = $totales;


            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de cliente');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra un tipo de cliente buscado por su id
     *
     * Para la consulta de un tipo de cliente se realizará una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $sql_client_type = DB::connection('tenant')->table('mo_client_type')
                ->select('mo_client_type.id', 'mo_client_type.code', 'mo_client_type.from', 'mo_client_type.to',
                        'mo_client_type.api_id', 'mo_client_type.hotel_id', 'mo_client_type.hotel_code', 'mo_client_type.hotel_age_required')
                ->where('mo_client_type.deleted_at', '=', null)
                ->join('mo_client_type_translation', 'mo_client_type_translation.client_type_id', '=', 'mo_client_type.id')
                ->where('mo_client_type_translation.deleted_at', '=', null);

            $sql_client_type->where('mo_client_type.id', '=', $id);

            $sql_client_type = $sql_client_type->groupBy('mo_client_type.id');
            $client_type = $sql_client_type->get();

            $array['data'] = array();
            foreach ($client_type as $client) {

                foreach ($idiomas as $idi) {
                    $traduccion = ClientTypeTranslation::where('client_type_id', '=', $client->id)
                        ->select('id', 'language_id', 'name')
                        ->where('language_id', '=', $idi->id)
                        ->get();
                    foreach ($traduccion as $trad) {
                        $client->lang[][$idi->abbreviation] = $trad;
                    }
                }

                $array['data'][0]['client_type'][] = $client;

            }


            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de cliente');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de un tipo de cliente.
     *
     * Para la eliminación de tipos de cliente se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_client_type,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $client_tag_id_id) {
                        $client_type = ClientType::where('id', $client_tag_id_id)->first();
                        $client_type->clientTypeTranslation()->delete();
                        $client_type->promotionClientType()->delete();
                        $client_type->promotionProductClientType()->delete();
                        $client_type->promotionProductTypeClientType()->delete();
                        $client_type->delete();
                    }
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de cliente');
        }
        return response()->json($array, $array['error']);
    }

}