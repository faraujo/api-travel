<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\DocumentTypeTranslation;
use App\Models\tenant\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use App\Exceptions\Handler;

class DocumentTypeController extends Controller
{

    /**
     * Muestra los tipos de documento
     *
     * Para la consulta de tipos de documento se realiza una petición GET. Si la operación no produce errores se devuelve,
     * en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDocumentTypes(Request $request)
    {
        $array['error'] = 200;
        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $document_types = DB::connection('tenant')->table('mo_document_type')
                    ->select('mo_document_type.id')
                    ->where('mo_document_type.deleted_at', '=', null)
                    ->join('mo_document_type_translation', 'mo_document_type_translation.document_type_id', '=', 'mo_document_type.id')
                    ->where('mo_document_type_translation.deleted_at', '=', null)
                    ->join('mo_language', 'mo_language.id', '=', 'mo_document_type_translation.language_id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_document_type_translation.document_type_id');


                $sub = DB::connection('tenant')->table('mo_document_type')
                    ->select('mo_document_type.id')
                    ->where('mo_document_type.deleted_at', '=', null)
                    ->join('mo_document_type_translation', 'mo_document_type_translation.document_type_id', '=', 'mo_document_type.id')
                    ->where('mo_document_type_translation.deleted_at', '=', null)
                    ->join('mo_language', 'mo_language.id', '=', 'mo_document_type_translation.language_id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_document_type_translation.document_type_id');

                if ($request->get('lang') != '') {
                    $document_types->where('mo_document_type_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_document_type_translation.language_id', '=', $idioma->id);
                }

                $document_types_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $document_types_count->count();

                $document_types = $document_types->get();

                $array['data'] = array();
                foreach ($document_types as $document_type) {

                    foreach ($idiomas as $idi) {
                        $traduccion = DocumentTypeTranslation::where('document_type_id', '=', $document_type->id)
                            ->select('language_id', 'name')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $document_type->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['document'][] = $document_type;
                }

                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de documentos');
        }

        return response()->json($array, $array['error']);
    }
}