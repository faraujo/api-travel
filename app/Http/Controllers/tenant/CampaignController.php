<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Campaign;
use App\Models\tenant\CampaignSubchannel;
use App\Models\tenant\CampaignTranslation;
use App\Models\tenant\Exceptions\Handler;
use App\Models\tenant\Promotion;
use App\Models\tenant\PromotionAccumulate;
use App\Models\tenant\PromotionClientType;
use App\Models\tenant\PromotionCountry;
use App\Models\tenant\PromotionCoupon;
use App\Models\tenant\PromotionCouponTranslation;
use App\Models\tenant\PromotionCurrency;
use App\Models\tenant\PromotionDevice;
use App\Models\tenant\PromotionEnjoyDay;
use App\Models\tenant\PromotionFile;
use App\Models\tenant\PromotionLanguage;
use App\Models\tenant\PromotionPaymentMethod;
use App\Models\tenant\PromotionPickup;
use App\Models\tenant\PromotionProduct;
use App\Models\tenant\PromotionProductClientType;
use App\Models\tenant\PromotionProductType;
use App\Models\tenant\PromotionProductTypeClientType;
use App\Models\tenant\PromotionRangeEnjoyDay;
use App\Models\tenant\PromotionRangeSaleDay;
use App\Models\tenant\PromotionSaleDay;
use App\Models\tenant\PromotionService;
use App\Models\tenant\PromotionServiceClientType;
use App\Models\tenant\PromotionState;
use App\Models\tenant\PromotionSubchannel;
use App\Models\tenant\PromotionTranslation;
use App\Models\tenant\Settings;
use Carbon\Carbon;
use validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;


class CampaignController extends Controller
{


    /**
     * Función para la eliminación de una campaña.
     *
     * Para la eliminación de campañas se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCampaign(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = \Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_campaign,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $campana_id) {
                        $campana = Campaign::where('id', $campana_id)->first();

                        $campana->campaignTranslation()->delete();
                        $campana->delete();
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Campañas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que crea o modifica una campaña
     *
     * Para la creación o modificación de campañas se realiza una petición POST para la creación o PUT para la modificación. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCampaign(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = \Validator::make($request->all(), [
                'active' => 'boolean',
                'date_start' => 'date|required|date_format:"Y-m-d"',
                'date_end' => 'date|required|after_or_equal:date_start|date_format:"Y-m-d"',
                'apply_filter_subchannel' => 'boolean|required',
                'subchannels' => 'array',
                'subchannels.*.id' => 'distinct|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            
            $array_subchannels = $request->get('subchannels');
            if ($request->get('apply_filter_subchannel') == 1 && !isset($array_subchannels)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['subchannels' => ['If apply_filter_subchannel is 1, the subchannels field must arrive, null or informed']]);
            }

            if (($request->get('apply_filter_subchannel') == "0" || is_null($request->get('apply_filter_subchannel'))) && isset($array_subchannels)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['subchannels' => ['If apply_filter_subchannel is 0, can not send subchannels']]);
            }


            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation])) {
                    $validator = \Validator::make($request->all(), [
                        'name.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',

                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($request->isMethod('put')) {
                $validator = \Validator::make($request->all(), [
                    'id' => 'integer|min:0|required',
                ]);
                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                } else {
                    $campana = Campaign::find($request->get('id'));
                    if (!$campana) {
                        $error = 2;
                    }
                }

                $sql_promotion = DB::connection('tenant')->table('mo_promotion')
                    ->whereNull('mo_promotion.deleted_at')
                    ->join('mo_campaign','mo_campaign.id','mo_promotion.campaign_id')
                    ->whereNull('mo_campaign.deleted_at')
                    ->where('mo_campaign.id',$request->get('id'));

                if($request->get('date_start') != '' && $request->get('date_end') != ''){
                    $sql_promotion_sale_clone = clone $sql_promotion;
                    $sql_promotion_enjoy_clone = clone $sql_promotion;

                    $datos_promotion_sale = $sql_promotion_sale_clone->join('mo_promotion_range_sale_day', function ($join) use ($request) {
                        $join->on('mo_promotion_range_sale_day.promotion_id','mo_promotion.id')
                            ->whereNull('mo_promotion_range_sale_day.deleted_at')
                            ->where(function ($query) use ($request){
                                $query->where('mo_promotion_range_sale_day.sale_date_start','<', Carbon::parse($request->get('date_start'))->startOfDay())
                                    ->orWhere('mo_promotion_range_sale_day.sale_date_end','>', Carbon::parse($request->get('date_end'))->endOfDay());
                            });
                    })->first();
//                    $datos_promotion_enjoy = $sql_promotion_enjoy_clone->join('mo_promotion_range_enjoy_day', function ($join) use ($request) {
//                        $join->on('mo_promotion_range_enjoy_day.promotion_id','mo_promotion.id')
//                            ->whereNull('mo_promotion_range_enjoy_day.deleted_at')
//                            ->where(function ($query) use ($request){
//                                $query->where('mo_promotion_range_enjoy_day.enjoy_date_start','<', Carbon::parse($request->get('date_start'))->startOfDay())
//                                    ->orWhere('mo_promotion_range_enjoy_day.enjoy_date_end','>', Carbon::parse($request->get('date_end'))->endOfDay());
//                            });
//                    })->first();

//                    if($datos_promotion_sale || $datos_promotion_enjoy){
                    if($datos_promotion_sale){
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['dates' => ['0' => 'There are promotions outside the indicated range']]);
                    }
                }
            }
            //Fin de validación
            $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                ->where('mo_settings.name', 'heimdall_middleware')
                ->first()
                ->value;

            $rule_codes = [];

            if($heimdall_middleware == '1') {

                $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                $website = DB::table('saas_website')
                                ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                'saas_website.default_language_id', 'saas_website.default_currency_id')
                                ->whereNull('saas_website.deleted_at')
                                ->where('saas_website.website_id', $website->id)
                                ->first();

                $plans = websitePlanRules($website);

                $cantidad = 0;

                foreach($plans as $plan) {
                    if($plan->plan_id != null){
                        $rules = websiteRules($plan);
                        
                        $rules_campaign = $rules->where('code', 'promo_campaign');

                        if($rules_campaign) {
                            foreach($rules_campaign as $rule) {
                                if($rule->unlimited == '1'){
                                    $cantidad = -1;
                                } else {
                                    if($cantidad >= 0 && $cantidad < $rule->value) {
                                        $cantidad = $rule->value;
                                    }
                                }
                            }
                        }
                    } else {
                        if($plan->rule_code == 'promo_campaign') {
                            if($plan->unlimited == '1'){
                                $cantidad = -1;
                            }else{
                                if($cantidad >= 0 && $cantidad < $plan->value){
                                    $cantidad = $plan->value;
                                }
                            }
                        }
                    }
                }

                if($cantidad == 0) {
                    $error = 3;
                    $rule_codes[] = 'promo_campaign';
                } else if ($cantidad > 0) {

                    if($request->isMethod('post')){
                        $total_campaigns = Campaign::get();
                    } else {
                        $total_campaigns = Campaign::where('id','!=', $request->get('id'))->get();
                    }
                    

                    if(count($total_campaigns) >= $cantidad) {
                        $error = 3;
                        $rule_codes[] = 'promo_campaign';
                    }
                }
            }

            if ($error == 0) {

                if ($request->isMethod('post')) {
                    $campana = Campaign::create([
                        'active' => $request->get('active') != '' ? $request->get('active') : 0,
                        'date_start' => $request->get('date_start'),
                        'date_end' => $request->get('date_end'),
                        'apply_filter_subchannel' => $request->get('apply_filter_subchannel'),
                    ]);

                    // Asociacion de canales
                    if ($request->get('subchannels') != '') {
                        foreach ($request->get('subchannels') as $subchannel) {
                            if (isset($subchannel['id']) && $subchannel['id'] != '') {
                                CampaignSubchannel::create([
                                    'campaign_id' => $campana->id,
                                    'subchannel_id' => $subchannel['id'],
                                ]);
                            }
                        }
                    }
                }

                if ($request->isMethod('put')) {
                    $campana->campaignChannel()->delete();
                    $campana->update([
                        'active' => $request->get('active') != '' ? $request->get('active') : 0,
                        'date_start' => $request->get('date_start'),
                        'date_end' => $request->get('date_end'),
                        'apply_filter_subchannel' => $request->get('apply_filter_subchannel'),
                    ]);

                    // Asociacion de canales
                    if ($request->get('subchannels') != '') {
                        foreach ($request->get('subchannels') as $subchannel) {
                            if (isset($subchannel['id']) && $subchannel['id'] != '') {
                                CampaignSubchannel::create([
                                    'campaign_id' => $campana->id,
                                    'subchannel_id' => $subchannel['id'],
                                ]);
                            }
                        }
                    }
                }

                // Actualizar traducciones de campaña
                $array_borrar = [];
                foreach ($array_traducciones as $idioma) {

                    $traduccion = $campana->campaignTranslation()->where('language_id', $idioma->id)->first();

                    $array_borrar[] = $idioma->id;

                    if ($traduccion) {
                        //Si existe traducción lo actualiza
                        $traduccion->update([
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                        ]);

                    } else {
                        //Si no existe traducción la crea
                        CampaignTranslation::create([
                            'campaign_id' => $campana->id,
                            'language_id' => $idioma->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                        ]);
                    }
                }
                $campana->campaignTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                // FIN actualizar traducciones de campaña


            } elseif ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } elseif ($error == 2) {
                $array['error'] = 404;
                $array['error_description'] = 'Data not found';
            } elseif ($error == 3) {
                $array['error'] = 419;
                $array['error_description'] = 'You do not have a valid plan';
                $array['error_rule_code'] = $rule_codes;

                //Heimdall - datos para redirección
                $array['data'] = array();
                $array['data'][0]['website_id'] = $website->id;

                $protocol = DB::table('saas_settings')
                ->where('saas_settings.name', 'protocol')->first()->value;
                $base_path_saas = DB::table('saas_settings')
                ->where('saas_settings.name', 'base_path_saas')->first()->value;
                $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Campañas');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función que lista todas las campañas
     *
     * Para la consulta de campañas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCampaigns(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
                'active' => 'boolean',
                'subchannel_id' => 'integer|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'date_start' => 'date|date_format:"Y-m-d"',
                'date_end' => 'date|date_format:"Y-m-d"',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $campanas = DB::connection('tenant')->table('mo_campaign')
                    ->select('mo_campaign.id', 'mo_campaign.active', 'mo_campaign.apply_filter_subchannel', 'date_start', 'date_end')
                    ->where('mo_campaign.deleted_at', '=', null)
                    ->join('mo_campaign_translation', 'mo_campaign.id', 'mo_campaign_translation.campaign_id')
                    ->where('mo_campaign_translation.deleted_at', '=', null)
                    ->leftJoin('mo_campaign_subchannel', function ($join) {
                        $join->on('mo_campaign_subchannel.campaign_id', '=', 'mo_campaign.id')
                            ->whereNull('mo_campaign_subchannel.deleted_at');
                    });

                $sub = DB::connection('tenant')->table('mo_campaign')
                    ->select('mo_campaign.id', 'mo_campaign.active', 'mo_campaign.apply_filter_subchannel', 'date_start', 'date_end')
                    ->where('mo_campaign.deleted_at', '=', null)
                    ->join('mo_campaign_translation', 'mo_campaign.id', 'mo_campaign_translation.campaign_id')
                    ->where('mo_campaign_translation.deleted_at', '=', null)
                    ->leftJoin('mo_campaign_subchannel', function ($join) {
                        $join->on('mo_campaign_subchannel.campaign_id', '=', 'mo_campaign.id')
                            ->whereNull('mo_campaign_subchannel.deleted_at');
                    });

                //Fin precompiladas

                //Filtros de busqueda
                if ($request->get('lang') != '') {
                    $campanas->where('mo_campaign_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_campaign_translation.language_id', '=', $idioma->id);
                }

                if ($request->get('description') != '') {
                    $campanas->where('mo_campaign_translation.description', 'LIKE', '%' . $request->get('description') . '%');
                    $sub->where('mo_campaign_translation.description', 'LIKE', '%' . $request->get('description') . '%');
                }

                if (
                    $request->get('code') != '' ||
                    $request->get('user') != ''
                ) {
                    $campanas->join('mo_promotion', 'mo_promotion.campaign_id', 'mo_campaign.id');
                    $sub->join('mo_promotion', 'mo_promotion.campaign_id', 'mo_campaign.id');
                }

                if ($request->get('code') != '') {
                    $campanas->where('mo_promotion.code', 'LIKE', '%' . $request->get('code') . '%');
                    $sub->where('mo_promotion.code', 'LIKE', '%' . $request->get('code') . '%');
                }

                if ($request->get('user') != '') {
                    $campanas->join('mo_user', 'mo_user.id', 'mo_promotion.user_id')
                        ->where('mo_user.name', 'LIKE', '%' . $request->get('user') . '%')
                        ->orWhere('mo_user.surname', 'LIKE', '%' . $request->get('user') . '%');
                    $sub->join('mo_user', 'mo_user.id', 'mo_promotion.user_id')
                        ->where('mo_user.name', 'LIKE', '%' . $request->get('user') . '%')
                        ->orWhere('mo_user.surname', 'LIKE', '%' . $request->get('user') . '%');
                }

                if ($request->get('active') != '') {
                    $campanas->where('mo_campaign.active', $request->get('active'));
                    $sub->where('mo_campaign.active', $request->get('active'));
                }

                if ($request->get('date_start') != '') {
                    $campanas->where('mo_campaign.date_start', '>=', $request->get('date_start'));
                    $sub->where('mo_campaign.date_start', '>=', $request->get('date_start'));
                }

                if ($request->get('date_end') != '') {
                    $campanas->where('mo_campaign.date_end', '<=', $request->get('date_end'));
                    $sub->where('mo_campaign.date_end', '<=', $request->get('date_end'));
                }

                if ($request->get('apply_filter_subchannel') != '') {
                    $campanas->where('mo_campaign.apply_filter_subchannel', $request->get('apply_filter_subchannel'));
                    $sub->where('mo_campaign.apply_filter_subchannel', $request->get('apply_filter_subchannel'));
                }

                if ($request->get('subchannel_id') != '') {
                    $campanas->where('mo_campaign_subchannel.subchannel_id', $request->get('subchannel_id'));
                    $sub->where('mo_campaign_subchannel.subchannel_id', $request->get('subchannel_id'));
                }


                //Fin de filtros

                // Order
                $orden = 'mo_campaign.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_campaign.id';
                            break;
                        case 'name':
                            $orden = 'mo_campaign_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_campaign_translation.description';
                            break;
                        default:
                            $orden = 'mo_campaign.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $campanas = $campanas->groupBy('mo_campaign.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $campanas = $campanas->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $campanas = $campanas->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_campaign_translation.campaign_id');
                $campanas_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $campanas_count->count();


                $array['data'] = array();


                //Se recorren las campañas
                foreach ($campanas as $campana) {

                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de la campaña
                        $traduccion = CampaignTranslation::where('campaign_id', '=', $campana->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name', 'description']);
                        foreach ($traduccion as $trad) {
                            $campana->lang[][$idi->abbreviation] = $trad;
                        }

                    }

                    $array['data'][0]['campaign'][] = $campana;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Campañas');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función que muestra una campaña buscada por su id
     *
     * Para la consulta de una campaña se realizará una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCampaignId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            //Consulta BBDD
            $campana = DB::connection('tenant')->table('mo_campaign')
                ->select('mo_campaign.id', 'mo_campaign.active', 'mo_campaign.date_start', 'mo_campaign.date_end', 'mo_campaign.apply_filter_subchannel')
                ->join('mo_campaign_translation', 'mo_campaign.id', '=', 'mo_campaign_translation.campaign_id')
                ->where('mo_campaign_translation.deleted_at', '=', null)
                ->where('mo_campaign.deleted_at', '=', null);

            $campana->where('mo_campaign.id', '=', $id);

            $campana = $campana->groupBy('mo_campaign.id');
            $campana = $campana->get();
            //Fin consulta BBDD

            $array['data'] = array();
            foreach ($campana as $campa) {
                //Traducciones de la campaña.
                foreach ($idiomas as $idi) {
                    $traducciones = CampaignTranslation::where('language_id', '=', $idi->id)->where('campaign_id', '=', $campa->id)
                        ->select('id', 'language_id', 'campaign_id', 'name', 'description')
                        ->get();
                    foreach ($traducciones as $trad) {
                        $campa->lang[][$idi->abbreviation] = $trad;
                    }
                }

                $campa->subchannels = array();
                if ($campa->apply_filter_subchannel) {
                    $datos_subchannels = CampaignSubchannel::where('campaign_id', $campa->id)
                        ->select('subchannel_id')
                        ->join('mo_subchannel', 'mo_campaign_subchannel.subchannel_id', 'mo_subchannel.id')
                        ->whereNull('mo_subchannel.deleted_at')
                        ->whereNull('mo_campaign_subchannel.deleted_at')
                        ->get();

                    foreach ($datos_subchannels as $channel) {
                        $campa->subchannels[] = $channel;
                    }
                }

                $array['data'][0]['campaign'][] = $campa;
            }


            //Fin traducciones

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Campañas');
        }

        return response()->json($array, $array['error']);

    }

    public function duplicateCampaignId(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0|exists:tenant.mo_campaign,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                $rule_codes = [];

                if($heimdall_middleware == '1') {
                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();
    
                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();
    
                    $plans = websitePlanRules($website);
    
                    $cantidad = 0;
    
                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_campaign = $rules->where('code', 'promo_campaign');
    
                            if($rules_campaign) {
                                foreach($rules_campaign as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'promo_campaign') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    
    
                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'promo_campaign';
                    } else if ($cantidad > 0) {
                        $total_campaigns = Campaign::get();
    
                        if(count($total_campaigns) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'promo_campaign';
                        }
                    }
                }

                

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {
                    $datos_campaign = Campaign::where('id', $request->get('id'))->first();

                    $new_campaign = Campaign::create([
                        'active' => $datos_campaign->active,
                        'apply_filter_subchannel' => $datos_campaign->apply_filter_subchannel,
                        'date_start' => $datos_campaign->date_start,
                        'date_end' => $datos_campaign->date_end
                    ]);

                    $datos_campaign_tranlsation = CampaignTranslation::where('campaign_id', $request->get('id'))->get();

                    foreach ($datos_campaign_tranlsation as $translation) {
                        CampaignTranslation::create([
                            'campaign_id' => $new_campaign->id,
                            'language_id' => $translation->language_id,
                            'name' => $translation->name.'-copy',
                            'description' => $translation->description,
                        ]);
                    }

                    $datos_campaign_subchannel = CampaignSubchannel::where('campaign_id', $request->get('id'))->get();

                    foreach ($datos_campaign_subchannel as $subchannel) {
                        CampaignSubchannel::create([
                            'campaign_id' => $new_campaign->id,
                            'subchannel_id' => $subchannel->subchannel_id
                        ]);
                    }

                    $promociones = Promotion::where('campaign_id', $request->get('id'))->get();

                    foreach ($promociones as $promocion) {
                        $promocion_new = Promotion::create([
                            'code' => $promocion->code.'-copy',
                            'order' => $promocion->order,
                            'visibility' => $promocion->visibility,
                            'accumulate' => $promocion->accumulate,
                            'min_quantity' => $promocion->min_quantity,
                            'max_quantity' => $promocion->max_quantity,
                            'promotion_amount' => $promocion->promotion_amount,
                            'currency_id' => $promocion->currency_id,
                            'promotion_type_id' => $promocion->promotion_type_id,
                            'campaign_id' => $new_campaign->id,
                            'department_id' => $promocion->department_id,
                            'cost_center_id' => $promocion->cost_center_id,
                            'apply_payment_method_filter' => $promocion->apply_payment_method_filter,
                            'apply_language_filter' => $promocion->apply_language_filter,
                            'apply_currency_filter' => $promocion->apply_currency_filter,
                            'apply_pickup_filter' => $promocion->apply_pickup_filter,
                            'apply_product_filter' => $promocion->apply_product_filter,
                            'apply_product_type_filter' => $promocion->apply_product_type_filter,
                            'apply_service_filter' => $promocion->apply_service_filter,
                            'apply_subchannel_filter' => $promocion->apply_subchannel_filter,
                            'apply_client_type_filter' => $promocion->apply_client_type_filter,
                            'apply_country_filter' => $promocion->apply_country_filter,
                            'apply_state_filter' => $promocion->apply_state_filter,
                            'apply_device_filter' => $promocion->apply_device_filter,
                            'lockers_validation' => $promocion->lockers_validation,
                            'supervisor_validation' => $promocion->supervisor_validation,
                            'allow_date_change' => $promocion->allow_date_change,
                            'allow_upgrade' => $promocion->allow_upgrade,
                            'allow_product_change' => $promocion->allow_product_change,
                            'pay_difference' => $promocion->pay_difference,
                            'anticipation_start' => $promocion->anticipation_start,
                            'anticipation_end' => $promocion->anticipation_end,
                            'coupon_code' => $promocion->coupon_code,
                            'coupon_type' => $promocion->coupon_type,
                            'coupon_sheet_start' => $promocion->coupon_sheet_start,
                            'coupon_sheet_end' => $promocion->coupon_sheet_end,
                            'coupon_total_uses' => $promocion->coupon_total_uses,
                            'benefit_card' => $promocion->benefit_card,
                            'benefit_card_total_uses' => $promocion->benefit_card_total_uses,
                            'benefit_card_id' => $promocion->benefit_card_id,
                            'user_id' => $promocion->user_id
                        ]);

                        $promotion_translations = PromotionTranslation::where('promotion_id', $promocion->id)->get();

                        foreach ($promotion_translations as $translation) {
                            PromotionTranslation::create([
                                'promotion_id' => $promocion_new->id,
                                'language_id' => $translation->language_id,
                                'gift' => $translation->gift,
                                'name' => $translation->name.'-copy',
                                'description' => $translation->description,
                                'short_description' => $translation->short_description,
                                'friendly_url' => $translation->friendly_url,
                                'external_url' => $translation->external_url,
                                'title_seo' => $translation->title_seo,
                                'description_seo' => $translation->description_seo,
                                'keywords_seo' => $translation->keywords_seo,
                                'observations' => $translation->observations,
                                'legal' => $translation->legal,
                                'notes' => $translation->notes,
                            ]);
                        }

                        $promotion_accumulate = PromotionAccumulate::where('promotion_id', $promocion->id)->get();

                        foreach ($promotion_accumulate as $accumulate) {
                            PromotionAccumulate::create([
                                'promotion_id' => $promocion_new->id,
                                'promotion_accumulate_id' => $accumulate->promotion_accumulate_id,
                            ]);
                        }

                        $client_types = PromotionClientType::where('promotion_id', $promocion->id)->get();

                        foreach ($client_types as $client_type) {
                            PromotionClientType::create([
                                'promotion_id' => $promocion_new->id,
                                'client_type_id' => $client_type->client_type_id,
                            ]);
                        }

                        $countries = PromotionCountry::where('promotion_id', $promocion->id)->get();
                        foreach ($countries as $country) {
                            PromotionCountry::create([
                                'promotion_id' => $promocion_new->id,
                                'country_id' => $country->country_id,
                            ]);
                        }

                        $cupones = PromotionCoupon::where('promotion_id', $promocion->id)->get();
                        foreach ($cupones as $cupon) {
                            $cupon_new = PromotionCoupon::create([
                                'promotion_id' => $promocion_new->id,
                            ]);

                            $coupon_translate = PromotionCouponTranslation::where('coupon_id', $cupon->id)->get();

                            foreach ($coupon_translate as $coupon) {
                                PromotionCouponTranslation::create([
                                    'coupon_id' => $cupon_new->id,
                                    'language_id' => $coupon->language_id,
                                    'file_id' => $coupon->file_id,
                                    'legal' => $coupon->legal,
                                ]);
                            }
                        }

                        $currencies = PromotionCurrency::where('promotion_id', $promocion->id)->get();

                        foreach ($currencies as $currency) {
                            PromotionCurrency::create([
                                'promotion_id' => $promocion_new->id,
                                'currency_id' => $currency->currency_id,
                            ]);
                        }

                        $range_enjoy_days = PromotionRangeEnjoyDay::where('promotion_id', $promocion->id)->get();

                        foreach ($range_enjoy_days as $range) {
                            $range_new = PromotionRangeEnjoyDay::create([
                                'promotion_id' => $promocion_new->id,
                                'enjoy_date_start' => $range->enjoy_date_start,
                                'enjoy_date_end' => $range->enjoy_date_end
                            ]);

                            $enjoy_days = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $range->id)->get();

                            foreach ($enjoy_days as $day) {
                                PromotionEnjoyDay::create([
                                    'promotion_range_enjoy_day_id' => $range_new->id,
                                    'day_id' => $day->day_id
                                ]);
                            }
                        }

                        $files = PromotionFile::where('promotion_id', $promocion->id)->get();

                        foreach ($files as $file) {
                            PromotionFile::create([
                                'promotion_id' => $promocion_new->id,
                                'file_id' => $file->file_id
                            ]);
                        }

                        $languages = PromotionLanguage::where('promotion_id', $promocion->id)->get();

                        foreach ($languages as $language) {
                            PromotionLanguage::create([
                                'promotion_id' => $promocion_new->id,
                                'language_id' => $language->language_id,
                            ]);
                        }

                        $payment_methods = PromotionPaymentMethod::where('promotion_id', $promocion->id)->get();

                        foreach ($payment_methods as $payment_method) {
                            PromotionPaymentMethod::create([
                                'promotion_id' => $promocion_new->id,
                                'payment_method_id' => $payment_method->payment_method_id,
                            ]);
                        }

                        $pickups = PromotionPickup::where('promotion_id', $promocion->id)->get();
                        foreach ($pickups as $pickup) {
                            PromotionPickup::create([
                                'promotion_id' => $promocion_new->id,
                                'pickup_id' => $pickup->pickup_id,
                            ]);
                        }

                        $products = PromotionProduct::where('promotion_id', $promocion->id)->get();

                        foreach ($products as $product) {

                            $product_new = PromotionProduct::create([
                                'promotion_id' => $promocion_new->id,
                                'product_id' => $product->product_id,
                                'location_id' => $product->location_id,
                                'min_quantity' => $product->min_quantity,
                                'max_quantity' => $product->max_quantity,
                                'series_one' => $product->series_one,
                                'series_two' => $product->series_two,
                                'promotion_amount' => $product->promotion_amount,
                                'currency_id' => $product->currency_id,
                                'promotion_type_id' => $product->promotion_type_id,
                                'apply_client_type_filter' => $product->apply_client_type_filter,
                            ]);


                            $product_client_types = PromotionProductClientType::where('promotion_product_id', $product->id)->get();

                            foreach ($product_client_types as $product_client_type) {
                                PromotionProductClientType::create([
                                    'promotion_product_id' => $product_new->id,
                                    'client_type_id' => $product_client_type->client_type_id
                                ]);
                            }
                        }


                        $product_types = PromotionProductType::where('promotion_id', $promocion->id)->get();
                        foreach ($product_types as $product_type) {
                            $product_type_new = PromotionProductType::create([
                                'promotion_id' => $promocion_new->id,
                                'product_type_id' => $product_type->product_type_id,
                                'min_quantity' => $product_type->min_quantity,
                                'max_quantity' => $product_type->max_quantity,
                                'series_one' => $product_type->series_one,
                                'series_two' => $product_type->series_two,
                                'promotion_amount' => $product_type->promotion_amount,
                                'currency_id' => $product_type->currency_id,
                                'promotion_type_id' => $product_type->promotion_type_id,
                                'apply_client_type_filter' => $product_type->apply_client_type_filter,
                            ]);

                            $product_type_client_types = PromotionProductTypeClientType::where('promotion_product_type_id', $product_type->id)->get();

                            foreach ($product_type_client_types as $product_type_client_type) {
                                PromotionProductTypeClientType::create([
                                    'promotion_product_type_id' => $product_type_new->id,
                                    'client_type_id' => $product_type_client_type->client_type_id
                                ]);
                            }
                        }


                        $ranges_sale = PromotionRangeSaleDay::where('promotion_id', $promocion->id)->get();

                        foreach ($ranges_sale as $range_sale) {
                            $range_sale_new = PromotionRangeSaleDay::create([
                                'promotion_id' => $promocion_new->id,
                                'sale_date_start' => $range_sale->sale_date_start,
                                'sale_date_end' => $range_sale->sale_date_end,
                            ]);

                            $sale_days = PromotionSaleDay::where('promotion_range_sale_day_id', $range_sale->id)->get();

                            foreach ($sale_days as $sale_day) {
                                PromotionSaleDay::create([
                                    'promotion_range_sale_day_id' => $range_sale_new->id,
                                    'day_id' => $sale_day->day_id
                                ]);
                            }
                        }

                        $services = PromotionService::where('promotion_id', $promocion->id)->get();

                        foreach ($services as $service) {
                            $service_new = PromotionService::create([
                                'promotion_id' => $promocion_new->id,
                                'service_id' => $service->service_id,
                                'min_quantity' => $service->min_quantity,
                                'max_quantity' => $service->max_quantity,
                                'series_one' => $service->series_one,
                                'series_two' => $service->series_two,
                                'promotion_amount' => $service->promotion_amount,
                                'currency_id' => $service->currency_id,
                                'promotion_type_id' => $service->promotion_type_id,
                                'apply_client_type_filter' => $service->apply_client_type_filter,
                            ]);

                            $service_client_types = PromotionServiceClientType::where('promotion_service_id', $service->id)->get();

                            foreach ($service_client_types as $service_client_type) {
                                PromotionServiceClientType::create([
                                    'promotion_service_id' => $service_new->id,
                                    'client_type_id' => $service_client_type->client_type_id
                                ]);
                            }
                        }

                        $states = PromotionState::where('promotion_id', $promocion->id)->get();

                        foreach ($states as $state) {
                            PromotionState::create([
                                'promotion_id' => $promocion_new->id,
                                'state_id' => $state->state_id,
                            ]);
                        }

                        $subchannels = PromotionSubchannel::where('promotion_id', $promocion->id)->get();

                        foreach ($subchannels as $subchannel) {
                            PromotionSubchannel::create([
                                'promotion_id' => $promocion_new->id,
                                'subchannel_id' => $subchannel->subchannel_id
                            ]);
                        }



                        $promotion_device = PromotionDevice::where('promotion_id', $promocion->id)->get();

                        foreach ($promotion_device as $device){
                            PromotionDevice::create([
                                'promotion_id' => $promocion_new->id,
                                'device_id' => $device->device_id,
                            ]);
                        }


                    }
                    $array['data'][] = ['campaign' => [['id' => $new_campaign->id]]];
                }
            }


            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Campañas');
        }

        return response()->json($array, $array['error']);
    }

}