<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use Validator;
//use \Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Models\tenant\CommentStatus;
use App\Models\tenant\CommentStatusTranslation;
use App\Exceptions\Handler;


class CommentStatusController extends Controller
{

    //use DatabaseTransactions;

    /**
     * Función que muestra los estados de opiniones
     *
     * Para la consulta de los estados de opiniones se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showStatus(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Se preparan las consultas
                $comment_status = DB::connection('tenant')->table('mo_comment_status')
                    ->select('mo_comment_status.id')
                    ->join('mo_comment_status_translation', 'mo_comment_status.id', '=', 'mo_comment_status_translation.comment_status_id')
                    ->where('mo_comment_status_translation.deleted_at', '=', null)
                    ->where('mo_comment_status.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_comment_status')
                    ->select('mo_comment_status.id')
                    ->join('mo_comment_status_translation', 'mo_comment_status.id', '=', 'mo_comment_status_translation.comment_status_id')
                    ->where('mo_comment_status_translation.deleted_at', '=', null)
                    ->where('mo_comment_status.deleted_at', '=', null);

                // Recogida de datos enviados por el usuario y establecimiento de filtros
                if ($request->get('lang') != '') {
                    $comment_status->where('language_id', '=', $idioma->id);
                    $sub->where('language_id', '=', $idioma->id);
                }


                $sub->groupBy('mo_comment_status_translation.comment_status_id');
                $status_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $comment_status->groupBy('mo_comment_status.id');
                $totales = $status_count->count();


                $comment_stados = $comment_status->get();

                $array['data'] = array();

                foreach ($comment_stados as $status) {

                    foreach ($idiomas as $idi) {
                        $traduccion = CommentStatusTranslation::where('comment_status_id', '=', $status->id)
                            ->select('language_id', 'name')
                            ->where('language_id', '=', $idi->id)
                            ->get();

                        foreach ($traduccion as $trad) {
                            $status->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['status'][] = $status;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {

            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Estado de comentarios');
        }

        return response()->json($array, $array['error']);
    }


}