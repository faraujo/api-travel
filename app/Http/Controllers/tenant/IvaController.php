<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Iva;
use App\Models\tenant\IvaTranslation;
use Illuminate\Support\Str;
use Validator;
//use \Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use Illuminate\Support\Facades\DB;


class IvaController extends Controller
{

    //use DatabaseTransactions;

    /**
     * Función que muestra las tasas de iva disponibles
     *
     * Para la consulta de las tasas de iva se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showIva(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $ivas = DB::connection('tenant')->table('mo_iva')
                    ->select('mo_iva.id', 'mo_iva.value')
                    ->where('mo_iva.deleted_at', '=', null)
                    ->join('mo_iva_translation', 'mo_iva.id', 'mo_iva_translation.iva_id')
                    ->where('mo_iva_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_iva')
                    ->select('mo_iva.id')
                    ->where('mo_iva.deleted_at', '=', null)
                    ->join('mo_iva_translation', 'mo_iva.id', 'mo_iva_translation.iva_id')
                    ->where('mo_iva_translation.deleted_at', '=', null);
                //Fin precompiladas

                //Filtros de busqueda
                if ($request->get('name') != '') {
                    $ivas->where('mo_iva_translation.name', 'Like', '%' . $request->get('name') . '%');
                    $sub->where('mo_iva_translation.name', 'Like', '%' . $request->get('name') . '%');
                }
                if ($request->get('lang') != '') {
                    $ivas->where('mo_iva_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_iva_translation.language_id', '=', $idioma->id);
                }
                //Fin de filtros

                // Order
                $orden = 'mo_iva.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_iva.id';
                            break;
                        case 'name':
                            $orden = 'mo_iva_translation.name';
                            break;
                        case 'value':
                                $orden = 'mo_iva.value';
                                break;
                        default:
                            $orden = 'mo_iva.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $ivas = $ivas->groupBy('mo_iva.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $ivas = $ivas->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $ivas = $ivas->get();

                }

                //Fin de paginación



                $sub->groupBy('mo_iva_translation.iva_id');
                $ivas_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $ivas_count->count();

                $array['data'] = array();


                //Se recorren las tasas de iva
                foreach ($ivas as $iva) {

                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de las tasas de iva
                        $traduccion = IvaTranslation::where('iva_id', '=', $iva->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name']);
                        foreach ($traduccion as $trad) {
                            $iva->lang[][$idi->abbreviation] = $trad;
                        }

                    }

                    $array['data'][0]['iva'][] = $iva;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Iva');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Devuelve la tasa de iva solicitada
     *
     * Para la consulta de una tasa de iva se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showIvaId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Consulta BBDD
            $iva = DB::connection('tenant')->table('mo_iva')
                ->select('mo_iva.id', 'mo_iva.value')
                ->join('mo_iva_translation', 'mo_iva.id', '=', 'mo_iva_translation.iva_id')
                ->where('mo_iva_translation.deleted_at', '=', null)
                ->where('mo_iva.deleted_at', '=', null)
                ->where('mo_iva.id', '=', $id);


            $iva = $iva->groupBy('mo_iva.id');
            $iva = $iva->get();
            //Fin consulta BBDD

            $array['data'] = array();
            foreach ($iva as $iva_aux) {
                //Traducciones de la tasa de iva
                foreach ($idiomas as $idi) {
                    $traducciones = IvaTranslation::where('language_id', '=', $idi->id)->where('iva_id', '=', $iva_aux->id)
                        ->select('id', 'language_id', 'name')
                        ->get();
                    foreach ($traducciones as $trad) {
                        $iva_aux->lang[][$idi->abbreviation] = $trad;
                    }
                }
                $array['data'][0]['iva'][] = $iva_aux;
            }

            //Fin traducciones

            DB::connection('tenant')->commit();


        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Iva');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la creación de una tasa de IVA y sus traducciones
     *
     * Para la creación de tasas de iva se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createIva(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'value' => 'required|numeric|min:0'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                    
                
                // Crear tasa de iva.
                    $tasaIva = Iva::create([
                        'value' => $request->get('value') != '' ? $request->get('value') : 0,
                    ]);
                    // FIN crear la tasa de iva

                    // Crear las traducciones para la tasa de iva
                    foreach ($array_traducciones as $idioma) {

                        IvaTranslation::create(
                            [
                                'iva_id' => $tasaIva->id,
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]
                        );
                    }
                    // FIN crear las traducciones de la tasa de iva
                }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Iva');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una tasa de iva.
     *
     * Para la modificación de tasas de iva se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function updateIva(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
                'value' => 'required|numeric|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $tasaIva = Iva::find($request->get('id'));
                if (!$tasaIva) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar tasa de Iva
                    $tasaIva->value = $request->get('value') != '' ? $request->get('value') : 0;

                    $tasaIva->save();
                    //FIN actualizar la tasa de Iva

                    // Actualizar traducciones de la tasa de Iva
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        $traduccion = $tasaIva->ivaTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {

                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);
                        } else {

                            IvaTranslation::create([
                                'iva_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);
                        }
                    }
                    $tasaIva->ivaTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de tasa de iva

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Iva');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de una tasa de Iva.
     *
     * Para la eliminación de tasas de Iva se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteIva(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_iva,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $iva_id) {
                        $iva = Iva::where('id', $iva_id)->first();
                        // borrado de tasas de Iva y sus traducciones
                        $iva->ivaTranslation()->delete();
                        $iva->delete();
                        //fin borrado

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Iva');
        }
        return response()->json($array, $array['error']);
    }

}