<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Settings;
use App\Models\tenant\Language;
use App\Models\tenant\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;


class SettingsController extends Controller
{

    /**
     * Función que lista algunos parámetros de ajuste de la tabla settings
     *
     * Para la consulta de parámetros de ajuste se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function show()
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Preparación de las consultas
            $parametros = Settings::where('name', 'email_host_default')
                ->orWhere('name', 'email_username_default')
                ->orWhere('name', 'email_from_default')
                ->orWhere('name', 'email_fromname_default')
                ->orWhere('name', 'email_driver_default')
                ->orWhere('name', 'email_port_default')
                ->orWhere('name', 'email_encryption_default')
                ->orWhere('name', 'idioma_defecto_id')
                ->orWhere('name', 'moneda_defecto_id')
                ->orWhere('name', 'limit_registers')
                ->orWhere('name', 'height_unit')
                ->orWhere('name', 'weight_unit')
                ->orWhere('name', 'token_validate')
                ->orWhere('name', 'recover_token_validate')
                ->orWhere('name', 'time_bloqued_password')
                ->orWhere('name', 'url_reservation_pdf')
                ->orWhere('name','nombre_cliente')
                ->orWhere('name','email_cliente_contacto')
                ->orWhere('name', 'reservation_code_length')
                ->orWhere('name', 'discount_max')
                ->orWhere('name', 'cron_expire_reservation')
                ->orWhere('name', 'logo_image_id')
                ->orWhere('name', 'nif_cliente')
                ->orWhere('name', 'direccion_facturacion')
                ->orWhere('name', 'paypal_environment')
                ->orWhere('name', 'paypal_username')
                ->orWhere('name', 'paypal_password')
                ->orWhere('name', 'paypal_secret')
                ->get();

            $array['data'] = array();

            $array_settings = array();
            foreach ($parametros as $parametro) {
                $array_settings[$parametro->name] = $parametro->value;
            }

            //Idiomas
            $idiomas = Language::all();
            $monedas = Currency::get(['id','name','simbol','iso_number','iso_code','iso2','presentation_simbol','decimal_separator','thousand_separator','front']);

            $array['data'][0]['settings'][] = $array_settings;
            $array['data'][0]['settings'][0]['languages'] = $idiomas;
            $array['data'][0]['settings'][0]['currencies'] = $monedas;

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Settings');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar un parámetro de configuración.
     *
     * Para la modificación de un parámetro de configuración se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();


            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'email_port_default' => 'required|integer|min:0',
                'idioma_defecto_id' => 'required|integer|min:0|exists:tenant.mo_language,id,deleted_at,NULL',
                'moneda_defecto_id' => 'required|integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',
                //'limit_registers' => 'required|integer|min:0',
                'token_validate' => 'required|integer|min:0',
                'recover_token_validate' => 'required|integer|min:0',
                'time_bloqued_password' => 'required|integer|min:0',
                'email_host_default' => 'required',
                'email_username_default' => 'required',
                'email_from_default' => 'required',
                'email_fromname_default' => 'required',
                'height_unit' => 'required',
                'weight_unit' => 'required',
                'nombre_cliente' => 'required',
                'email_cliente_contacto' => 'required|email',
                'nif_cliente' => 'required',
                'reservation_code_length' => 'required|integer',
                'discount_max' => 'required|numeric',
                'cron_expire_reservation' => 'required|numeric',
                'languages' => 'required|array',
                "languages.*"  => "required|integer|distinct|min:0|exists:tenant.mo_language,id,deleted_at,NULL",
                'currencies' => 'required|array',
                "currencies.*"  => "required|integer|distinct|min:0|exists:tenant.mo_currency,id,deleted_at,NULL",
                'logo_image_id' => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Validación idioma por defecto
            $check_default_language = 0;
            if ($request->get('idioma_defecto_id') != '') {        
                if($request->get('languages') && is_array($request->get('languages'))){
                    if(!in_array($request->get('idioma_defecto_id'), $request->get('languages'))){
                        $check_default_language = 1;
                    }
                }else{
                    $language = Settings::where('name', '=', 'idioma_defecto_id')->first()->value;

                    if (!$language) {
                        $check_default_language = 1;
                    } 
                }
                if($check_default_language == 1){
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['idioma_defecto_id' => ['The language default selected is not valid']]);
                }
            }
             //Validación moneda por defecto
             $check_default_currency = 0;
             if ($request->get('moneda_defecto_id') != '') {        
                 if($request->get('currencies') && is_array($request->get('currencies'))){
                     if(!in_array($request->get('moneda_defecto_id'), $request->get('currencies'))){
                         $check_default_currency = 1;
                     }
                 }else{
                     $currency = Settings::where('name', '=', 'moneda_defecto_id')->first()->value;
                     if (!$currency) {
                         $check_default_currency = 1;
                     } 
                 }
                 if($check_default_currency == 1){
                     $error = 1;
                     $mensaje_validador = $mensaje_validador->merge(['moneda_defecto_id' => ['The currency default selected is not valid']]);
                 }
             }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } 
            else {
                

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                $rule_codes = [];
                
                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();
            
                    $plans = websitePlanRules($website);

                    $cantidad = 0;
                    $cantidad_currency = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);

                            if($request->get('languages')){
                                $rules_language = $rules->where('code', 'conf_language');
                                
                                $language_rule_code = 'conf_language';

                                if($rules_language) {
                                    foreach($rules_language as $rule) {
                                        if($rule->unlimited == 1) {
                                            $cantidad = -1;
                                        } else {
                                            if($cantidad >= 0 && $cantidad < $rule->value){
                                                $cantidad = $rule->value;
                                            }
                                        }
                                    }
                                }
                            }
                            //
                            if($request->get('currencies')){
                                $rules_currency = $rules->where('code', 'conf_currencie');
                                
                                $currency_rule_code = 'conf_currencie';

                                if($rules_currency) {
                                    foreach($rules_currency as $rule) {

                                        if($rule->unlimited == 1) {
                                            $cantidad_currency = -1;
                                        } else {
        
                                            if($cantidad_currency >= 0 && $cantidad_currency < $rule->value){
                                                $cantidad_currency = $rule->value;
                                            }
                                        }
                                    }
                                }
                            }
                            //
                        } else {
                            if($plan->rule_code == 'conf_language') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
    
                            if($plan->rule_code == 'conf_currencie') {
                                if($plan->unlimited == 1){
                                    $cantidad_currency = -1;
                                }else{
                                    if($cantidad_currency >= 0 && $cantidad_currency < $plan->value){
                                        $cantidad_currency = $plan->value;
                                    }
                                }
                            }
                        }
                    }
                     
                    if($request->get('languages') != '' && count($request->get('languages')) > 0) {
                        if($cantidad == 0) {
                            $error = 1;
                            $rule_codes[] = 'conf_language';
                        } else if ($cantidad > 0 && count($request->get('languages')) > $cantidad){
                            $error = 1;
                            $rule_codes[] = 'conf_language';
                        }
                    }
                    //
                    if($request->get('currencies') != '' && count($request->get('currencies')) > 0) {
                        if($cantidad_currency == 0) {
                            $error = 1;
                            $rule_codes[] = 'conf_currencie';
                        } else if ($cantidad_currency > 0 && count($request->get('currencies')) > $cantidad_currency){
                            $error = 1;
                            $rule_codes[] = 'conf_currencie';
                        }
                    }
                    //

                     
                }
                
                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {
                        // Actualizar parámetros de configuración

                if ($request->get('email_password_default')) {
                    Settings::where('name', 'email_password_default')->update(['value' => $request->get('email_password_default')]);
                }
                Settings::where('name', 'email_host_default')->update(['value' => $request->get('email_host_default')]);

                Settings::where('name', 'email_username_default')->update(['value' => $request->get('email_username_default')]);

                Settings::where('name', 'email_from_default')->update(['value' => $request->get('email_from_default')]);

                Settings::where('name', 'email_fromname_default')->update(['value' => $request->get('email_fromname_default')]);

                Settings::where('name', 'email_port_default')->update(['value' => $request->get('email_port_default')]);

                if ($request->get('email_encryption_default') == '') {
                    Settings::where('name', 'email_encryption_default')->update(['value' => '']);
                }else{
                    Settings::where('name', 'email_encryption_default')->update(['value' => $request->get('email_encryption_default')]);
                }

                Settings::where('name', 'idioma_defecto_id')->update(['value' => $request->get('idioma_defecto_id')]);

                Settings::where('name', 'moneda_defecto_id')->update(['value' => $request->get('moneda_defecto_id')]);

                //Settings::where('name', 'limit_registers')->update(['value' => $request->get('limit_registers')]);

                Settings::where('name', 'height_unit')->update(['value' => $request->get('height_unit')]);

                Settings::where('name', 'weight_unit')->update(['value' => $request->get('weight_unit')]);

                Settings::where('name', 'token_validate')->update(['value' => $request->get('token_validate')]);

                Settings::where('name', 'recover_token_validate')->update(['value' => $request->get('recover_token_validate')]);

                Settings::where('name', 'time_bloqued_password')->update(['value' => $request->get('time_bloqued_password')]);

                Settings::where('name', 'nombre_cliente')->update(['value' => $request->get('nombre_cliente')]);

                Settings::where('name', 'email_cliente_contacto')->update(['value' => $request->get('email_cliente_contacto')]);

                Settings::where('name', 'nif_cliente')->update(['value' => $request->get('nif_cliente')]);

                if ($request->get('direccion_facturacion') == '') {
                    Settings::where('name', 'direccion_facturacion')->update(['value' => '']);
                }else{
                    Settings::where('name', 'direccion_facturacion')->update(['value' => $request->get('direccion_facturacion')]);
                }

                Settings::where('name', 'reservation_code_length')->update(['value' => $request->get('reservation_code_length')]);

                Settings::where('name', 'discount_max')->update(['value' => $request->get('discount_max')]);

                Settings::where('name', 'cron_expire_reservation')->update(['value' => $request->get('cron_expire_reservation')]);

                Settings::where('name', 'logo_image_id')->update(['value' => $request->get('logo_image_id')]);

                Settings::where('name', 'paypal_environment')->update(['value' => $request->get('paypal_environment')]);

                Settings::where('name', 'paypal_username')->update(['value' => $request->get('paypal_username')]);

                Settings::where('name', 'paypal_password')->update(['value' => $request->get('paypal_password')]);
                
                Settings::where('name', 'paypal_secret')->update(['value' => $request->get('paypal_secret')]);
                
                //FIN actualizar parámetros de configuración

                //Actualización monedas
                
                
                if ($request->get('currencies')) {

                    Currency::query()->update(['front' => 0]);
                    

                    foreach ($request->get('currencies') as $currency) {
                        Currency::where('id', $currency)
                            ->update([
                                'front' => 1
                            ]);
                    }

                    
                }

                //FIN actualización monedas

                //Actualización Idiomas
                
                
                if ($request->get('languages')) {

                    Language::query()->update(['front' => 0]);
                    

                    foreach ($request->get('languages') as $language) {
                        Language::where('id', $language)
                            ->update([
                                'front' => 1
                            ]);
                    }

                    
                }

                //FIN actualización monedas
                }
                
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Settings');
        }
        return response()->json($array, $array['error']);
    }

}
