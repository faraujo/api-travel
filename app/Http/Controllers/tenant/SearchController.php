<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\AdditionalServiceTranslation;
use App\Models\tenant\AdditionalServiceTypeTranslation;
use App\Models\tenant\AdditionalServiceFile;
use App\Models\tenant\CategoryTranslation;
use App\Models\tenant\ClientType;
use App\Models\tenant\ClientTypeTranslation;
use App\Models\tenant\ContractModelProduct;
use App\Models\tenant\Country;
use App\Models\tenant\Currency;
use App\Models\tenant\CurrencyExchange;
use App\Exceptions\Handler;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\Product;
use App\Models\tenant\ProductCategory;
use App\Models\tenant\ProductTranslation;
use App\Models\tenant\ProductTypeTranslation;
use App\Models\tenant\PromotionClientType;
use App\Models\tenant\PromotionProduct;
use App\Models\tenant\PromotionProductClientType;
use App\Models\tenant\PromotionProductType;
use App\Models\tenant\PromotionProductTypeClientType;
use App\Models\tenant\PromotionService;
use App\Models\tenant\PromotionServiceClientType;
use App\Models\tenant\SearchLog;
use App\Models\tenant\ServiceTranslation;
use App\Models\tenant\Subchannel;
use App\Models\tenant\ViewProductTranslation;
use App\Models\tenant\ViewRoomTranslation;
use App\Models\tenant\UserToken;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Validator;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SoapCLient;
use SoapVar;
use SoapHeader;
use SimpleXMLElement;
use App\Models\tenant\Device;


class SearchController extends Controller
{
    /**
     * Función para guardar un log de búsquedas
     *
     * Guarda cada búsqueda en BBDD.
     * Se utiliza en cada búsqueda por canal, almacenando los filtros utilizados en la búsqueda (contenidos en el $request o enviados como parámetros independientes) y los resultados obtenidos.
     * En este caso el primer parámetro ($product_id) tendrá que enviarse a valor null.
     * También se utiliza cada vez que se muestra la ficha de producto, enviando en este caso como primer parámetro el product_id del producto a mostrar, y un único resultado de búsqueda.
     * La fecha, idioma y tipo de moneda a utilizar en la búsqueda, siempre serán enviados, y se tratan como parámetros independientes del $request ya que en los métodos si no se envían estos filtros se definirán unos valores por defecto.
     *
     * @param $product_id
     * @param $request
     * @param $id_idioma
     * @param $moneda_convertir
     * @param $fecha
     * @param $total_resultados_directos
     * @param $total_resultados_indirectos
     * @param $total_resultados
     * @param $hotel_room_id
     * @param $is_hotel
     */
    private function createLog($product_id, $request, $id_idioma, $moneda_convertir, $fecha, $total_resultados_directos, $total_resultados_indirectos, $total_resultados, $hotel_room_id, $is_hotel)
    {
        SearchLog::create([

            'product_id' => $product_id ? $product_id : null,
            'subchannel_id' => $request->get('subchannel_id'),
            'searched_word' => ($request->get('search') != '') ? $request->get('search') : null,
            'product_type_id' => ($request->get('type_id') != '') ? $request->get('type_id') : null,
            'depends_on' => ($request->get('depends_on') != '') ? $request->get('depends_on') : null,
            'category_id' => ($request->get('category_id') != '' && $is_hotel == false) ? $request->get('category_id') : null,
            'tag_id' => ($request->get('tag_id') != '') ? $request->get('tag_id') : null,
            'user_id' => ($request->get('user_id') != '') ? $request->get('user_id') : null,
            'money' => ($request->get('money') != '') ? $request->get('money') : null,
            'availability' => ($request->get('avail') != '') ? $request->get('avail') : null,
            'date' => $fecha,
            'currency_id' => $moneda_convertir,
            'language_id' => $id_idioma,
            'hotel_room_id' => $hotel_room_id,
            'hotel_room_category_id' => ($request->get('category_id') != '' && $is_hotel == true) ? $request->get('category_id') : null,
            'hotel_info_only' => ($request->get('info_only') != '') ? $request->get('info_only') : 0,
            'hotel_number_room' => ($request->get('number_room') != '') ? $request->get('number_room') : null,
            'hotel_date_start' => ($request->get('date_start') != '') ? $request->get('date_start') : null,
            'hotel_date_end' => ($request->get('date_end') != '') ? $request->get('date_end') : null,
            'hotel_adult' => ($request->get('adult') != '') ? $request->get('adult') : null,
            'hotel_child' => ($request->get('child') != '') ? $request->get('child') : 0,
            'total_first_search_results' => $total_resultados_directos,
            'total_second_search_results' => $total_resultados_indirectos ? $total_resultados_indirectos : 0,
            'result' => ($total_resultados == 0) ? 0 : 1,
        ]);
    }

    /**
     * Búsqueda de productos y tags disponibles por canal
     *
     * Para la consulta de los productos y tags disponibles por subcanal se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPredictive(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'date' => 'date|date_format:"Y-m-d"|after_or_equal:today',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang')) {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->first();
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                $subchannel = Subchannel::where('id', $request->get('subchannel_id'))->first();
                $fecha = ($request->get('date') != '') ? Carbon::createFromFormat('Y-m-d', $request->get('date'))->format('Y-m-d') : Carbon::now()->format('Y-m-d');

                $id_idioma = $idioma->id;

                //Consulta productos visibles por canal

                $sql_productos = DB::connection('tenant')->table('mo_product')
                    ->select(
                        'mo_product.id as product_id',
                        'mo_product_translation.name as product_name',
                        'mo_tag.id as tag_id',
                        'mo_tag_translation.name as tag_name',
                        'mo_view_product.id as view_id',
                        'mo_view_product_translation.name as view_name'
                    )
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                    ->whereNull('mo_product_translation.deleted_at')
                    ->where('mo_product_translation.language_id', '=', $id_idioma)
                    ->join('mo_price_product', 'mo_product.id', '=', 'mo_price_product.product_id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->whereNull('mo_price_product.service_id')
                    ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
                    ->whereNull('mo_price.deleted_at')
                    ->where('mo_price.date_start', '<=', $fecha)
                    ->where('mo_price.date_end', '>=', $fecha)
                    ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_subchannel.id', '=', $request->get('subchannel_id'))
                    ->leftJoin('mo_product_tag', function ($join) {
                        $join->on('mo_product_tag.product_id', '=', 'mo_product.id')
                            ->whereNull('mo_product_tag.deleted_at');
                    })
                    ->leftJoin('mo_tag', function ($join) {
                        $join->on('mo_product_tag.tag_id', '=', 'mo_tag.id')
                            ->whereNull('mo_tag.deleted_at');
                    })
                    ->leftJoin('mo_tag_translation', function ($join) use ($id_idioma) {
                        $join->on('mo_tag_translation.tag_id', '=', 'mo_tag.id')
                            ->whereNull('mo_tag_translation.deleted_at')
                            ->where('mo_tag_translation.language_id', '=', $id_idioma);
                    });

                if ($subchannel->view_blocked == 0) {
                    $sql_productos->leftJoin('mo_view_product', function ($join) use ($request) {
                        $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                            ->where('mo_view_product.subchannel_id', '=', $request->get('subchannel_id'))
                            ->whereNull('mo_view_product.deleted_at');
                    })
                        ->leftJoin('mo_view_product_translation', function ($join) use ($idioma) {
                            $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                                ->where('mo_view_product_translation.language_id', '=', $idioma->id)
                                ->whereNull('mo_view_product_translation.deleted_at');
                        })->Where(function ($query) {
                            $query->where('mo_view_product.published', '=', '1');

                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_product.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '0');
                            });
                        });

                } else {
                    $sql_productos->join('mo_view_product', 'mo_view_product.product_id', '=', 'mo_product.id')
                        ->where('mo_view_product.subchannel_id', '=', $request->get('subchannel_id'))
                        ->whereNull('mo_view_product.deleted_at')
                        ->join('mo_view_product_translation', 'mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                        ->where('mo_view_product_translation.language_id', '=', $idioma->id)
                        ->whereNull('mo_view_product_translation.deleted_at');
                }


                //Ejecución de consultas y generación de array de datos de salida
                $array['data'] = array();

                $consulta_productos = clone $sql_productos;
                $datos_productos = $consulta_productos->groupBy('mo_product.id')->orderBy('product_name')->get();

                foreach ($datos_productos as $producto) {

                    //Producto (por canal)
                    if ($producto->view_id != null) {

                        $array['data'][0]['product'][] = [
                            'id' => $producto->product_id,
                            'lang' => array([
                                $idioma->abbreviation => [
                                    'name' => $producto->view_name
                                ]
                            ])
                        ];

                        //Producto (general)
                    } else {
                        $array['data'][0]['product'][] = [
                            'id' => $producto->product_id,
                            'lang' => array([
                                $idioma->abbreviation => [
                                    'name' => $producto->product_name
                                ]
                            ])
                        ];
                    }
                }

                $consulta_etiquetas = clone $sql_productos;
                //Si producto no tiene tags, evita que se muestre una tag nula
                $consulta_etiquetas->whereNotNull('mo_tag_translation.name');
                $datos_etiquetas = $consulta_etiquetas->groupBy('mo_tag.id')->orderBy('tag_name')->get();

                foreach ($datos_etiquetas as $etiqueta) {
                    $array['data'][0]['tag'][] = [
                        'id' => $etiqueta->tag_id,
                        'lang' => array([
                            $idioma->abbreviation => [
                                'name' => $etiqueta->tag_name
                            ]
                        ])
                    ];
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');
        }

        return response()->json($array, $array['error']);

    }

    /**
     * Búsqueda de productos disponibles por canal
     *
     * Búsqueda de productos por canal pudiendo filtrar por canal, moneda, tipo de producto, categoría, disponibilidad, precio, productos dependientes
     * con coste añadido y sin coste añadido, etiqueta y categoría del producto dependiente
     * Para la consulta de los productos disponibles por canal se realiza una petición GET. Si la operación no produce
     * errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showSubchannel($id = null, Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::all();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
                'emulated_subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'types' => 'array',
                'types.*' => 'min:0|exists:tenant.mo_product_type,id,deleted_at,NULL|integer|distinct',
                'type_id' => 'exists:tenant.mo_product_type,id,deleted_at,NULL|integer|min:0',
                'category_id' => 'exists:tenant.mo_category,id,deleted_at,NULL|integer|min:0',
                'categories' => 'array',
                'categories.*' => 'min:0|exists:tenant.mo_category,id,deleted_at,NULL|integer|distinct',
                'avail' => 'integer|min:0',
                'money' => 'integer|min:0',
                'date' => 'date|date_format:"Y-m-d"|after_or_equal:today',
                'page' => 'integer',
                'limit' => 'integer|min:0',
                'depends_on' => 'exists:tenant.mo_product,id,deleted_at,NULL|integer|min:0',
                'tag_id' => 'integer|min:0|exists:tenant.mo_tag,id,deleted_at,NULL',
                'location_id' => 'exists:tenant.mo_location,id,deleted_at,NULL|integer|min:0',
                'file_types' => 'array',
                'file_types.*' => 'distinct|exists:tenant.mo_file_type,id,deleted_at,NULL|integer|min:0',
                'salable' => 'boolean',
                'promotions' => 'boolean',
                'home_app_hotel' => 'boolean',
                'device' => 'exists:tenant.mo_device,code,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }


            $subchannel_id = '';
            $emulated_subchannel_id = '';
            // Si la petición es pública a través de un canal de venta
            if (strpos($request->path(), 'search/pos') === false) {

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }


            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->where('front', 1)->first();

            }

            //Obtención moneda a la que se quiere convertir
            if ($request->get('currency') != '') {

                $currency = Currency::where('iso_code', Str::upper($request->get('currency')))->first();
                if (!$currency) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['currency' => ['The currency is not exists']]);
                } else {
                    $id_moneda_convertir = $currency->id;
                    $datos_moneda = $currency;
                }
            } else {
                $moneda_settings = Settings::where('name', 'moneda_defecto_id')->first();
                $id_moneda_convertir = $moneda_settings->value;
                $datos_moneda = Currency::find($id_moneda_convertir);
            }

            //fin obtención moneda

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_search = $rules->where('code', 'search');

                            if($rules_search) {
                                foreach($rules_search as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'search') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'search';
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    $fecha = ($request->get('date') != '') ? Carbon::createFromFormat('Y-m-d', $request->get('date'))->format('Y-m-d') : Carbon::now()->format('Y-m-d');

                    $fecha_inicio = $request->get('date') != '' ? Carbon::parse($request->get('date')) : Carbon::now();
                    $week_day = $fecha_inicio->format('N');

                    $subchannel = Subchannel::where('id', $emulated_subchannel_id)->first();

                    $settings_decimales = Settings::where('name', 'numero_decimales')->first()->value;

                    $sql_productos_directo = DB::connection('tenant')->table('mo_product')
                        ->select(
                            'mo_product.id',
                            'mo_product.code',
                            'mo_product.company_id',
                            'mo_product.order',
                            'mo_product.billable',
                            'mo_product.height_from',
                            'mo_product.height_to',
                            'mo_product.weight_from',
                            'mo_product.weight_to',
                            'mo_product.longitude',
                            'mo_product.latitude',
                            'mo_product.salable',
                            'mo_product.app_hotel_service',
                            'mo_product.app_hotel_without_reservation',
                            'mo_product.app_hotel_afi_park',
                            'mo_product.app_hotel_afi_tour',
                            'mo_product.app_hotel_menu_service',
                            'mo_product.app_hotel_with_service',
                            'mo_product.app_hotel_with_service_direct',

                            'mo_product_translation.language_id',
                            'mo_product_translation.name',
                            'mo_product_translation.description',
                            'mo_product_translation.features',
                            'mo_product_translation.recommendations',
                            'mo_product_translation.short_description',
                            'mo_product_translation.friendly_url',
                            'mo_product_translation.discover_url',
                            'mo_product_translation.title_seo',
                            'mo_product_translation.description_seo',
                            'mo_product_translation.keywords_seo',
                            'mo_product_translation.legal',

                            'mo_product_translation.breadcrumb',
                            'mo_product_translation.rel',
                            'mo_product_translation.index',
                            'mo_product_translation.og_title',
                            'mo_product_translation.og_description',
                            'mo_product_translation.og_image',
                            'mo_product_translation.twitter_title',
                            'mo_product_translation.twitter_description',
                            'mo_product_translation.twitter_image',
                            'mo_product_translation.canonical_url',
                            'mo_product_translation.script_head',
                            'mo_product_translation.script_body',
                            'mo_product_translation.script_footer',

                            'mo_product_type_translation.type_id as product_type_id',
                            'mo_product_type_translation.name as product_type_name',
                            'mo_price.currency_id',
                            'mo_price.promotions',
                            'mo_price_product.price_id',
                            'mo_view_product.id as view_id',
                            'mo_view_product_translation.language_id as view_language_id',
                            'mo_view_product_translation.name as view_name',
                            'mo_view_product_translation.description as view_description',
                            'mo_view_product_translation.features as view_features',
                            'mo_view_product_translation.recommendations as view_recommendations',
                            'mo_view_product_translation.short_description as view_short_description',
                            'mo_view_product_translation.friendly_url as view_friendly_url',
                            'mo_view_product_translation.discover_url as view_discover_url',
                            'mo_view_product_translation.title_seo as view_title_seo',
                            'mo_view_product_translation.description_seo as view_description_seo',
                            'mo_view_product_translation.keywords_seo as view_keywords_seo',
                            'mo_view_product_translation.legal as view_legal',

                            'mo_view_product_translation.breadcrumb as view_breadcrumb',
                            'mo_view_product_translation.rel as view_rel',
                            'mo_view_product_translation.index as view_index',
                            'mo_view_product_translation.og_title as view_og_title',
                            'mo_view_product_translation.og_description as view_og_description',
                            'mo_view_product_translation.og_image as view_og_image',
                            'mo_view_product_translation.twitter_title as view_twitter_title',
                            'mo_view_product_translation.twitter_description as view_twitter_description',
                            'mo_view_product_translation.twitter_image as view_twitter_image',
                            'mo_view_product_translation.canonical_url as view_canonical_url',
                            'mo_view_product_translation.script_head as view_script_head',
                            'mo_view_product_translation.script_body as view_script_body',
                            'mo_view_product_translation.script_footer as view_script_footer',

                            'mo_contract_model.id as contrac_model_id',

                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product.order, mo_view_product.order) AS aux_order'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.friendly_url, mo_view_product_translation.friendly_url) AS aux_friendly_url'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product.home_app_hotel, mo_view_product.home_app_hotel) AS aux_home_app_hotel'),

                            'mo_contract_model_product.required_service',
                            'mo_contract_model_product.individual_sale'

                        )
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->where('mo_product_translation.language_id', '=', $idioma->id)
                        ->join('mo_product_type', 'mo_product.type_id', 'mo_product_type.id')
                        ->whereNull('mo_product_type.deleted_at')
                        ->join('mo_product_type_translation', 'mo_product_type.id', 'mo_product_type_translation.type_id')
                        ->whereNull('mo_product_type_translation.deleted_at')
                        ->join('mo_price_product', 'mo_product.id', '=', 'mo_price_product.product_id')
                        ->whereNull('mo_price_product.deleted_at')
                        ->whereNull('mo_price_product.service_id')
                        ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
                        ->whereNull('mo_price.deleted_at')
                        ->where('mo_price.date_start', '<=', $fecha)
                        ->where('mo_price.date_end', '>=', $fecha)
                        ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                        ->whereNull('mo_contract_model.deleted_at')
                        ->leftJoin('mo_contract_model_product',function ($join){
                            $join->on('mo_contract_model_product.contract_model_id','mo_contract_model.id')
                                ->whereNull('mo_contract_model_product.deleted_at')
                                ->whereNull('mo_contract_model_product.service_id');
                            $join->on('mo_contract_model_product.product_id','mo_product.id')
                                ->whereNull('mo_contract_model_product.deleted_at')
                                ->whereNull('mo_contract_model_product.service_id');
                        })
                        ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                        ->whereNull('mo_subchannel.deleted_at')
                        ->where('mo_subchannel.id', '=', $emulated_subchannel_id)
                        ->join('mo_product_location', 'mo_product_location.product_id', 'mo_product.id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->join('mo_location', 'mo_location.id', 'mo_product_location.location_id')
                        ->whereNull('mo_location.deleted_at')
                        ->leftJoin('mo_product_tag', function ($join) {
                            $join->on('mo_product_tag.product_id', '=', 'mo_product.id')
                                ->whereNull('mo_product_tag.deleted_at');
                        })
                        ->leftJoin('mo_tag', function ($join) {
                            $join->on('mo_product_tag.tag_id', '=', 'mo_tag.id')
                                ->whereNull('mo_tag.deleted_at');
                        })
                        ->leftJoin('mo_tag_translation', function ($join) use ($idioma) {
                            $join->on('mo_tag_translation.tag_id', '=', 'mo_tag.id')
                                ->whereNull('mo_tag_translation.deleted_at')
                                ->where('mo_tag_translation.language_id', '=', $idioma->id);
                        })
                        ->leftJoin('mo_product_category', function ($join) {
                            $join->on('mo_product_category.product_id', '=', 'mo_product.id')
                                ->whereNull('mo_product_category.deleted_at');
                        })
                        ->leftJoin('mo_category', function ($join) {
                            $join->on('mo_product_category.category_id', '=', 'mo_category.id')
                                ->whereNull('mo_category.deleted_at');
                        })
                        ->leftJoin('mo_category_translation', function ($join) use ($idioma) {
                            $join->on('mo_category_translation.category_id', '=', 'mo_category.id')
                                ->whereNull('mo_category_translation.deleted_at')
                                ->where('mo_category_translation.language_id', '=', $idioma->id);
                        })->leftjoin('mo_availability', function ($join) use ($fecha) {
                            $join->on('mo_availability.product_id', '=', 'mo_product.id')
                                ->where('mo_availability.deleted_at', '=', null)
                                ->where('mo_availability.service_id', '=', null)
                                ->where('mo_availability.closed', '!=', 1)
                                ->where(function ($query) use ($fecha) {
                                    $query->where('mo_availability.date', '=', $fecha)
                                        ->orWhere('mo_availability.date', '=', null);
                                });
                        });

                    $sql_count_productos_directo = DB::connection('tenant')->table('mo_product')
                        ->select('mo_product.id',
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.friendly_url, mo_view_product_translation.friendly_url) AS aux_friendly_url'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product.home_app_hotel, mo_view_product.home_app_hotel) AS aux_home_app_hotel')
                        )
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->where('mo_product_translation.language_id', '=', $idioma->id)
                        ->join('mo_product_type', 'mo_product.type_id', 'mo_product_type.id')
                        ->whereNull('mo_product_type.deleted_at')
                        ->join('mo_product_type_translation', 'mo_product_type.id', 'mo_product_type_translation.type_id')
                        ->whereNull('mo_product_type_translation.deleted_at')
                        ->join('mo_price_product', 'mo_product.id', '=', 'mo_price_product.product_id')
                        ->whereNull('mo_price_product.deleted_at')
                        ->whereNull('mo_price_product.service_id')
                        ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
                        ->whereNull('mo_price.deleted_at')
                        ->where('mo_price.date_start', '<=', $fecha)
                        ->where('mo_price.date_end', '>=', $fecha)
                        ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                        ->whereNull('mo_contract_model.deleted_at')
                        ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                        ->whereNull('mo_subchannel.deleted_at')
                        ->where('mo_subchannel.id', '=', $emulated_subchannel_id)
                        ->join('mo_product_location', 'mo_product_location.product_id', 'mo_product.id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->join('mo_location', 'mo_location.id', 'mo_product_location.location_id')
                        ->whereNull('mo_location.deleted_at')
                        ->leftJoin('mo_product_tag', function ($join) {
                            $join->on('mo_product_tag.product_id', '=', 'mo_product.id')
                                ->whereNull('mo_product_tag.deleted_at');
                        })
                        ->leftJoin('mo_tag', function ($join) {
                            $join->on('mo_product_tag.tag_id', '=', 'mo_tag.id')
                                ->whereNull('mo_tag.deleted_at');
                        })
                        ->leftJoin('mo_tag_translation', function ($join) use ($idioma) {
                            $join->on('mo_tag_translation.tag_id', '=', 'mo_tag.id')
                                ->whereNull('mo_tag_translation.deleted_at')
                                ->where('mo_tag_translation.language_id', '=', $idioma->id);
                        })
                        ->leftJoin('mo_product_category', function ($join) {
                            $join->on('mo_product_category.product_id', '=', 'mo_product.id')
                                ->whereNull('mo_product_category.deleted_at');
                        })
                        ->leftJoin('mo_category', function ($join) {
                            $join->on('mo_product_category.category_id', '=', 'mo_category.id')
                                ->whereNull('mo_category.deleted_at');
                        })
                        ->leftJoin('mo_category_translation', function ($join) use ($idioma) {
                            $join->on('mo_category_translation.category_id', '=', 'mo_category.id')
                                ->whereNull('mo_category_translation.deleted_at')
                                ->where('mo_category_translation.language_id', '=', $idioma->id);
                        })
                        ->leftjoin('mo_availability', function ($join) use ($fecha) {
                            $join->on('mo_availability.product_id', '=', 'mo_product.id')
                                ->where('mo_availability.deleted_at', '=', null)
                                ->where('mo_availability.service_id', '=', null)
                                ->where('mo_availability.closed', '!=', 1)
                                ->where(function ($query) use ($fecha) {
                                    $query->where('mo_availability.date', '=', $fecha)
                                        ->orWhere('mo_availability.date', '=', null);
                                });
                        });

                    $sql_product_type_translation = DB::connection('tenant')->table('mo_product_type_translation')
                        ->whereNull('mo_product_type_translation.deleted_at');

                    $sql_product_category = DB::connection('tenant')->table('mo_product_category')
                        ->select(
                            'mo_product_category.category_id',
                            'mo_category_translation.name'
                        )
                        ->whereNull('mo_product_category.deleted_at')
                        ->join('mo_category_translation','mo_category_translation.category_id','mo_product_category.category_id')
                        ->whereNull('mo_category_translation.deleted_at');

                    //Servicios asociados al producto
                    $sql_serviciosAsociados = DB::connection('tenant')->table('mo_service')
                        ->select('mo_service.id',
                            'mo_service_translation.language_id',
                            'mo_service_translation.name',
                            'mo_service_translation.description')
                        ->whereNull('mo_service.deleted_at')
                        ->join('mo_service_translation', 'mo_service.id', '=', 'mo_service_translation.service_id')
                        ->whereNull('mo_service_translation.deleted_at')
                        ->where('mo_service_translation.language_id', $idioma->id)
                        ->join('mo_product_service', 'mo_product_service.service_id', 'mo_service.id')
                        ->whereNull('mo_product_service.deleted_at')
                        ->join('mo_price_product', 'mo_price_product.service_id', 'mo_service.id')
                        ->whereNull('mo_price_product.deleted_at')
                        ->join('mo_price', 'mo_price.id', 'mo_price_product.price_id')
                        ->whereNull('mo_price.deleted_at')
                        ->where('mo_price.date_start', '<=', $fecha)
                        ->where('mo_price.date_end', '>=', $fecha)
                        ->join('mo_contract_model', 'mo_contract_model.id', 'mo_price.contract_model_id')
                        ->whereNull('mo_contract_model.deleted_at')
                        ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                        ->whereNull('mo_subchannel.deleted_at')
                        ->where('mo_subchannel.id', '=', $emulated_subchannel_id)
                        ->groupBy('mo_service.id');

                    $sql_locaciones = DB::connection('tenant')->table('mo_location')
                        ->select('mo_location.id', 'mo_location.name', 'mo_location.depends_on')
                        ->whereNull('mo_location.deleted_at')
                        ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->groupBy('mo_location.id');
                    //fin archivos

                    //disponibilidades
                    $sql_availabilities = DB::connection('tenant')->table('mo_availability')
                        ->select('mo_availability.id',
                            'mo_availability.date',
                            'mo_availability.session',
                            'mo_availability.quota',
                            'mo_availability.avail',
                            'mo_availability.overbooking',
                            'mo_availability.overbooking_unit',
                            'mo_availability.closed',
                            'mo_availability.unlimited')
                        ->whereNull('mo_availability.deleted_at')
                        ->whereNull('mo_availability.service_id')
                        ->orderBy('mo_availability.date')
                        ->orderBy('mo_availability.session', 'asc')
                        ->groupBy('mo_availability.session');

                    $sql_availability_servicios = DB::connection('tenant')->table('mo_availability')
                        ->select('mo_availability.id',
                            'mo_availability.date',
                            'mo_availability.session',
                            'mo_availability.quota',
                            'mo_availability.avail',
                            'mo_availability.overbooking',
                            'mo_availability.overbooking_unit',
                            'mo_availability.closed',
                            'mo_availability.unlimited')
                        ->whereNull('mo_availability.deleted_at')
                        ->orderBy('mo_availability.date')
                        ->orderBy('mo_availability.session', 'asc')
                        ->groupBy('mo_availability.session');
                    //fin disponibilidades

                    $sql_tags = DB::connection('tenant')->table('mo_tag')
                        ->select('mo_tag.id',
                            'mo_tag_translation.language_id',
                            'mo_tag_translation.name',
                            'mo_tag_translation.short_description')
                        ->whereNull('mo_tag.deleted_at')
                        ->join('mo_tag_translation', 'mo_tag.id', '=', 'mo_tag_translation.tag_id')
                        ->where('mo_tag_translation.language_id', '=', $idioma->id)
                        ->whereNull('mo_tag_translation.deleted_at')
                        ->join('mo_product_tag', 'mo_tag.id', '=', 'mo_product_tag.tag_id')
                        ->whereNull('mo_product_tag.deleted_at');

                    //datos tipo de cliente y tarifa producto
                    $sql_client_type = DB::connection('tenant')->table('mo_client_type')
                        ->select('mo_client_type.id',
                            'mo_client_type.code',
                            'mo_client_type.from',
                            'mo_client_type.to',
                            'mo_client_type.hotel_age_required',
                            'mo_client_type_translation.language_id',
                            'mo_client_type_translation.name')
                        ->whereNull('mo_client_type.deleted_at')
                        ->join('mo_client_type_translation', 'mo_client_type_translation.client_type_id', 'mo_client_type.id')
                        ->whereNull('mo_client_type_translation.deleted_at')
                        ->where('mo_client_type_translation.language_id', '=', $idioma->id)
                        ->join('mo_price_product', 'mo_price_product.client_type_id', '=', 'mo_client_type.id')
                        ->whereNull('mo_price_product.service_id')
                        ->whereNull('mo_price_product.deleted_at')
                        ->groupBy('mo_client_type.id');

                    $sql_tarifa_client_type = DB::connection('tenant')->table('mo_price_product')
                        ->select('mo_price_product.net_price',
                            'mo_price_product.sale_price',
                            'mo_price_product.markup',
                            'mo_price_product.price_before')
                        ->where('mo_price_product.deleted_at', '=', null);

                    $sql_pickups = DB::connection('tenant')->table('mo_transportation_location')
                        ->select('mo_transportation_location.id',
                            'mo_transportation_location.name',
                            'mo_transportation_location.api_code'
                        )
                        ->whereNull('mo_transportation_location.deleted_at')
                        ->join('mo_price_product', 'mo_price_product.pickup_id', 'mo_transportation_location.id')
                        ->where('mo_price_product.deleted_at', '=', null)
                        ->groupBy('mo_transportation_location.id');

                    $sql_archivos_view = DB::connection('tenant')->table('mo_view_product_file')
                        ->select('mo_file.id as file_id',
                            'mo_file.file_name',
                            'mo_file.file_size',
                            'mo_file.file_dimensions',
                            'mo_file.mimetype',
                            'mo_file.type_id',
                            'mo_file.order',
                            'mo_file.url_file',
                            'mo_file.device_id',
                            'mo_file.key_file',
                            'mo_file_translation.id',
                            'mo_file_translation.language_id',
                            'mo_file_translation.name',
                            'mo_file_translation.alternative_text',
                            'mo_file_type.id as file_type')
                        ->whereNull('mo_view_product_file.deleted_at')
                        ->join('mo_file', 'mo_file.id', '=', 'mo_view_product_file.file_id')
                        ->whereNull('mo_file.deleted_at')
                        ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                        ->whereNull('mo_file_translation.deleted_at')
                        ->leftJoin('mo_file_type', function ($join) {
                            $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                                ->whereNull('mo_file_type.deleted_at')
                                ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                        })
                        ->where('mo_file_translation.language_id', $idioma->id)
                        ->orderBy('mo_file.order', 'desc');

                    //Archivos si tiene el de mayor orden
                    $sql_archivos = DB::connection('tenant')->table('mo_product_file')
                        ->select(
                            'mo_file.id as file_id',
                            'mo_file.file_name',
                            'mo_file.file_size',
                            'mo_file_translation.id',
                            'mo_file_translation.name',
                            'mo_file_translation.alternative_text',
                            'mo_file_translation.language_id',
                            'mo_file.file_dimensions',
                            'mo_file.mimetype',
                            'mo_file.type_id',
                            'mo_file.order',
                            'mo_file.url_file',
                            'mo_file.device_id',
                            'mo_file.key_file',
                            'mo_file_type.id as file_type')
                        ->where('mo_product_file.deleted_at', '=', null)
                        ->join('mo_file', 'mo_file.id', '=', 'mo_product_file.file_id')
                        ->where('mo_file.deleted_at', '=', null)
                        ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                        ->where('mo_file_translation.deleted_at', null)
                        ->where('mo_file_translation.language_id', $idioma->id)
                        ->leftJoin('mo_file_type', function ($join) {
                            $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                                ->where('mo_file_type.deleted_at', null)
                                ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id')
                                ->whereNull('mo_file_type_translation.deleted_at');
                        })
                        ->groupBy('mo_file.id')
                        ->orderBy('mo_file.order', 'desc');

                    $sql_product_package = DB::connection('tenant')->table('mo_product')
                        ->select(
                            'mo_product.id',
                            'mo_product.code',
                            'mo_product.company_id',
                            'mo_product.order',
                            'mo_product.billable',
                            'mo_product.height_from',
                            'mo_product.height_to',
                            'mo_product.weight_from',
                            'mo_product.weight_to',
                            'mo_product.longitude',
                            'mo_product.latitude',
                            'mo_product.salable',
                            'mo_product.app_hotel_service',
                            'mo_product.app_hotel_without_reservation',
                            'mo_product.app_hotel_afi_park',
                            'mo_product.app_hotel_afi_tour',
                            'mo_product.app_hotel_menu_service',
                            'mo_product.app_hotel_with_service',
                            'mo_product.app_hotel_with_service_direct',

                            'mo_product_translation.language_id',
                            'mo_product_translation.name',
                            'mo_product_translation.description',
                            'mo_product_translation.features',
                            'mo_product_translation.recommendations',
                            'mo_product_translation.short_description',
                            'mo_product_translation.friendly_url',
                            'mo_product_translation.discover_url',
                            'mo_product_translation.title_seo',
                            'mo_product_translation.description_seo',
                            'mo_product_translation.keywords_seo',

                            'mo_product_translation.breadcrumb',
                            'mo_product_translation.rel',
                            'mo_product_translation.index',
                            'mo_product_translation.og_title',
                            'mo_product_translation.og_description',
                            'mo_product_translation.og_image',
                            'mo_product_translation.twitter_title',
                            'mo_product_translation.twitter_description',
                            'mo_product_translation.twitter_image',
                            'mo_product_translation.canonical_url',
                            'mo_product_translation.script_head',
                            'mo_product_translation.script_body',
                            'mo_product_translation.script_footer',

                            'mo_product_translation.legal',
                            'mo_product_type_translation.type_id as product_type_id',
                            'mo_product_type_translation.name as product_type_name',
                            'mo_product_package.location_id',
                            'mo_location.name as location_name',
                            'mo_location.depends_on as location_depends_on',

                            'mo_view_product.id as view_id',

                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.name, mo_view_product_translation.name) AS name'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.description, mo_view_product_translation.description) AS description'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.features, mo_view_product_translation.features) AS features'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.recommendations, mo_view_product_translation.recommendations) AS recommendations'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.short_description, mo_view_product_translation.short_description) AS short_description'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.friendly_url, mo_view_product_translation.friendly_url) AS friendly_url'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.discover_url, mo_view_product_translation.discover_url) AS discover_url'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.title_seo, mo_view_product_translation.title_seo) AS title_seo'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.description_seo, mo_view_product_translation.description_seo) AS description_seo'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.keywords_seo, mo_view_product_translation.keywords_seo) AS keywords_seo'),

                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.breadcrumb, mo_view_product_translation.breadcrumb) AS breadcrumb'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.rel, mo_view_product_translation.rel) AS rel'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.index, mo_view_product_translation.index) AS product_index'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.og_title, mo_view_product_translation.og_title) AS og_title'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.og_description, mo_view_product_translation.og_description) AS og_description'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.og_image, mo_view_product_translation.og_image) AS og_image'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.twitter_title, mo_view_product_translation.twitter_title) AS twitter_title'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.twitter_description, mo_view_product_translation.twitter_description) AS twitter_description'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.twitter_image, mo_view_product_translation.twitter_image) AS twitter_image'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.canonical_url, mo_view_product_translation.canonical_url) AS canonical_url'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.script_head, mo_view_product_translation.script_head) AS script_head'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.script_body, mo_view_product_translation.script_body) AS script_body'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.script_footer, mo_view_product_translation.script_footer) AS script_footer'),

                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.legal, mo_view_product_translation.legal) AS legal'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.language_id, mo_view_product_translation.language_id) AS language_id'),

                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product.home_app_hotel, mo_view_product.home_app_hotel) AS aux_home_app_hotel'),

                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product.order, mo_view_product.order) AS aux_order')
                        )
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation', 'mo_product_translation.product_id', 'mo_product.id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->where('mo_product_translation.language_id', '=', $idioma->id)
                        ->join('mo_product_type', 'mo_product_type.id', 'mo_product.type_id')
                        ->whereNull('mo_product_type.deleted_at')
                        ->join('mo_product_type_translation', 'mo_product_type_translation.type_id', 'mo_product_type.id')
                        ->whereNull('mo_product_type_translation.deleted_at')
                        ->join('mo_product_package', 'mo_product_package.product_id', 'mo_product.id')
                        ->whereNull('mo_product_package.deleted_at')
                        ->join('mo_product_location', 'mo_product_location.product_id', 'mo_product.id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->join('mo_location', 'mo_location.id', 'mo_product_package.location_id')
                        ->whereNull('mo_location.deleted_at')
                        ->leftJoin('mo_view_product', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                                ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                                ->where('mo_view_product.published', 1)
                                ->whereNull('mo_view_product.deleted_at');
                        })
                        ->leftJoin('mo_view_product_translation', function ($join) use ($idioma) {
                            $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                                ->where('mo_view_product_translation.language_id', '=', $idioma->id)
                                ->whereNull('mo_view_product_translation.deleted_at');
                        })->leftJoin('mo_subchannel', function ($join) {
                            $join->on('mo_subchannel.id', 'mo_view_product.subchannel_id')
                                ->whereNull('mo_subchannel.deleted_at');
                        })
                        ->groupBy('mo_product.id');


                    //consulta preparada para hreflang de seo
                    $sql_hreflang = DB::connection('tenant')->table('mo_product')
                        ->select('mo_language.abbreviation',
                            'mo_product_translation.language_id',
                            'mo_product_translation.friendly_url',
                            'mo_view_product_translation.language_id',
                            'mo_view_product_translation.friendly_url',

                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.language_id, mo_view_product_translation.language_id) AS language_id'),
                            DB::connection('tenant')->raw('IF(mo_view_product.id IS NULL, mo_product_translation.friendly_url, mo_view_product_translation.friendly_url) AS friendly_url')

                        )
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation','mo_product.id','mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->leftJoin('mo_view_product', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                                ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                                ->where('mo_view_product.published', 1)
                                ->whereNull('mo_view_product.deleted_at');
                        })
                        ->leftJoin('mo_view_product_translation', function ($join) use ($idioma) {
                            $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                                ->whereNull('mo_view_product_translation.deleted_at');
                        })
                        ->groupBy('mo_language.id');

                    $sql_currency_exchange = DB::connection('tenant')->table('mo_currency_exchange')
                        ->whereNull('mo_currency_exchange.deleted_at');


                    if($request->get('promotions') != '' && $request->get('promotions') == 1){
                        $sql_promotions = DB::connection('tenant')->table('mo_promotion')
                            ->select(
                                'mo_promotion.*'
                            )
                            ->whereNull('mo_promotion.deleted_at')
                            ->join('mo_promotion_type', 'mo_promotion_type.id', 'mo_promotion.promotion_type_id')
                            ->whereNull('mo_promotion_type.deleted_at')
                            ->join('mo_promotion_type_translation', 'mo_promotion_type_translation.promotion_type_id', 'mo_promotion_type.id')
                            ->whereNull('mo_promotion_type_translation.deleted_at')
                            ->join('mo_promotion_translation', 'mo_promotion_translation.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_translation.deleted_at')
                            ->join('mo_campaign', 'mo_campaign.id', 'mo_promotion.campaign_id')
                            ->whereNull('mo_campaign.deleted_at')
                            ->where('mo_campaign.active', 1)
                            ->where('mo_campaign.date_start', '<=', $fecha)
                            ->where('mo_campaign.date_end', '>=', $fecha)
                            ->join('mo_campaign_translation', 'mo_campaign_translation.campaign_id', 'mo_campaign.id')
                            ->whereNull('mo_campaign_translation.deleted_at')
                            ->leftJoin('mo_campaign_subchannel', function ($join) {
                                $join->on('mo_campaign_subchannel.campaign_id', 'mo_campaign.id')
                                    ->whereNull('mo_campaign_subchannel.deleted_at');
                            })
                            ->where(function ($query) use ($emulated_subchannel_id) {
                                $query->where('mo_campaign.apply_filter_subchannel', 0)
                                    ->orWhere('mo_campaign_subchannel.subchannel_id', $emulated_subchannel_id);
                            })
                            ->join('mo_promotion_range_sale_day', 'mo_promotion_range_sale_day.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_sale_day.deleted_at')
                            ->where('mo_promotion_range_sale_day.sale_date_start', '<=', $fecha)
                            ->where('mo_promotion_range_sale_day.sale_date_end', '>=', $fecha)
                            ->join('mo_promotion_sale_day', 'mo_promotion_sale_day.promotion_range_sale_day_id', 'mo_promotion_range_sale_day.id')
                            ->whereNull('mo_promotion_sale_day.deleted_at')
                            ->where('mo_promotion_sale_day.day_id', $week_day)
                            ->leftJoin('mo_promotion_subchannel', function ($join) {
                                $join->on('mo_promotion_subchannel.promotion_id', '=', 'mo_promotion.id')
                                    ->whereNull('mo_promotion_subchannel.deleted_at');
                            })
                            ->where(function ($query) use ($emulated_subchannel_id) {
                                $query->where('mo_promotion.apply_subchannel_filter', 0)
                                    ->orWhere('mo_promotion_subchannel.subchannel_id', $emulated_subchannel_id);
                            })
                            ->leftJoin('mo_promotion_range_enjoy_day', function ($join) use ($fecha) {
                                $join->on('mo_promotion_range_enjoy_day.promotion_id', 'mo_promotion.id')
                                    ->whereNull('mo_promotion_range_enjoy_day.deleted_at');
                            })
                            ->where(function ($query) use ($fecha,$week_day) {
                                $query->where(function ($query) use ($fecha,$week_day) {
                                    $query->where('mo_promotion_range_enjoy_day.enjoy_date_start', '<=', $fecha)
                                        ->where('mo_promotion_range_enjoy_day.enjoy_date_end', '>=', $fecha)
                                        ->where('mo_promotion_enjoy_day.day_id', $week_day);
                                })->orWhereNull('mo_promotion_range_enjoy_day.enjoy_date_start');
                            })
                            ->leftJoin('mo_promotion_enjoy_day', function ($join) use ($week_day) {
                                $join->on('mo_promotion_enjoy_day.promotion_range_enjoy_day_id', 'mo_promotion_range_enjoy_day.id')
                                    ->whereNull('mo_promotion_enjoy_day.deleted_at');
                            })
                            ->leftJoin('mo_promotion_currency', function ($join){
                                $join->on('mo_promotion_currency.promotion_id','mo_promotion.id')
                                    ->whereNull('mo_promotion.deleted_at');
                            })
                            ->leftJoin('mo_promotion_device', function ($join){
                                $join->on('mo_promotion_device.promotion_id','mo_promotion.id')
                                    ->whereNull('mo_promotion_device.deleted_at');
                            })
                            ->where(function ($query) use ($id_moneda_convertir) {
                                $query->where('mo_promotion.apply_currency_filter', '0')
                                    ->orWhere('mo_promotion_currency.currency_id', $id_moneda_convertir);
                            })
                            ->where('mo_promotion.benefit_card', 0)
                            ->orderBy('mo_promotion.order', 'desc');

                        if ($request->get('coupon_code') != '') {
                            $sql_promotions->where(function ($query) use ($request) {
                                $query->where(function ($query) use ($request) {
                                    $query->where('mo_promotion.coupon_type', 1)
                                        ->where('mo_promotion.coupon_code', $request->get('coupon_code'));
                                })->orWhere('mo_promotion.coupon_type', 0);
                            });
                        } else {
                            $sql_promotions->where('mo_promotion.coupon_type', 0);
                        }

                        if ($request->get('device') != '') {

                            $promotion_device = Device::where('code', Str::upper($request->get('device')))->where('active', 1)->first();

                            $sql_promotions->where(function ($query) use ($promotion_device) {
                                $query->where(function ($query) use ($promotion_device) {
                                    $query->where('mo_promotion.apply_device_filter', 1)
                                        ->where('mo_promotion_device.device_id', $promotion_device->id);
                                })->orWhere('mo_promotion.apply_device_filter', 0);
                            });
                        } else {
                            $sql_promotions->where('mo_promotion.apply_device_filter', 0);
                        }

                        $sql_promotion_product = clone $sql_promotions;

                        $count_productos = DB::connection('tenant')->table('mo_promotion_product')
                            ->whereNull('mo_promotion_product.deleted_at')
                            ->groupBy('mo_promotion_product.promotion_id');


                        $sql_promotion_product->join('mo_promotion_product', 'mo_promotion_product.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product.deleted_at')
                            ->where('mo_promotion.apply_product_type_filter', 0)
                            ->where('mo_promotion.apply_product_filter', 1)
                            ->where('mo_promotion.apply_service_filter', 0)
                            ->whereNull('mo_promotion_product.series_one')
                            ->whereNull('mo_promotion_product.series_two')
                            ->whereIn('mo_promotion.id', collect(DB::connection('tenant')->table(DB::connection('tenant')->raw("({$count_productos->select('mo_promotion_product.promotion_id', DB::connection('tenant')->raw('count(mo_promotion_product.id) as rule'))->toSql()}) as sub"))
                                ->mergeBindings($count_productos)->where('rule', 1)->get(['promotion_id']))->map(function ($x) {
                                return (array)$x;
                            })->toArray())
                            ->groupBy('mo_promotion_product.id');

                        $sql_promotion_product_type = clone $sql_promotions;

                        $count_product_types = DB::connection('tenant')->table('mo_promotion_product_type')
                            ->whereNull('mo_promotion_product_type.deleted_at')
                            ->groupBy('mo_promotion_product_type.promotion_id');

                        $sql_promotion_product_type->join('mo_promotion_product_type', 'mo_promotion_product_type.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product_type.deleted_at')
                            ->where('mo_promotion.apply_product_type_filter', 1)
                            ->where('mo_promotion.apply_product_filter', 0)
                            ->where('mo_promotion.apply_service_filter', 0)
                            ->whereNull('mo_promotion_product_type.series_one')
                            ->whereNull('mo_promotion_product_type.series_two')
                            ->whereIn('mo_promotion.id', collect(DB::connection('tenant')->table(DB::connection('tenant')->raw("({$count_product_types->select('mo_promotion_product_type.promotion_id', DB::connection('tenant')->raw('count(mo_promotion_product_type.id) as rule'))->toSql()}) as sub"))
                                ->mergeBindings($count_product_types)->where('rule', 1)->get(['promotion_id']))->map(function ($x) {
                                return (array)$x;
                            })->toArray())
                            ->groupBy('mo_promotion_product_type.id');

                        $sql_promotion_service = clone $sql_promotions;

                        $count_services = DB::connection('tenant')->table('mo_promotion_service')
                            ->whereNull('mo_promotion_service.deleted_at')
                            ->groupBy('mo_promotion_service.promotion_id');

                        $sql_promotion_service->join('mo_promotion_service', 'mo_promotion_service.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_service.deleted_at')
                            ->where('mo_promotion.apply_product_type_filter', 0)
                            ->where('mo_promotion.apply_product_filter', 0)
                            ->where('mo_promotion.apply_service_filter', 1)
                            ->whereIn('mo_promotion.id', collect(DB::connection('tenant')->table(DB::connection('tenant')->raw("({$count_services->select('mo_promotion_service.promotion_id', DB::connection('tenant')->raw('count(mo_promotion_service.id) as rule'))->toSql()}) as sub"))
                                ->mergeBindings($count_services)->where('rule', 1)->get(['promotion_id']))->map(function ($x) {
                                return (array)$x;
                            })->toArray());

                        $sql_promotions
                            ->where('mo_promotion.apply_product_type_filter', 0)
                            ->where('mo_promotion.apply_product_filter', 0)
                            ->where('mo_promotion.apply_service_filter', 0);

                        $sql_datos_filtro_product = DB::connection('tenant')->table('mo_promotion_product')
                        ->whereNull('mo_promotion_product.deleted_at');

                        $sql_datos_filtro_product_type = DB::connection('tenant')->table('mo_promotion_product_type')
                        ->whereNull('mo_promotion_product_type.deleted_at');

                        $sql_accumulate_promotion = DB::connection('tenant')->table('mo_promotion_accumulate')
                        ->whereNull('mo_promotion_accumulate.deleted_at');

                        $sql_accumulate_promotion_service = DB::connection('tenant')->table('mo_promotion_accumulate')
                        ->whereNull('mo_promotion_accumulate.deleted_at');

                        $sql_datos_filtro_servicio = DB::connection('tenant')->table('mo_promotion_service')
                        ->whereNull('mo_promotion_service.deleted_at');

                        $sql_datos_filtro_service_client_type = DB::connection('tenant')->table('mo_promotion_service_client_type')
                            ->whereNull('mo_promotion_service_client_type.deleted_at');

                        $sql_promotion_client_type = DB::connection('tenant')->table('mo_promotion_client_type')
                            ->whereNull('mo_promotion_client_type.deleted_at');

                        $sql_promotion_product_type_client_type = DB::connection('tenant')->table('mo_promotion_product_type_client_type')
                            ->whereNull('mo_promotion_product_type_client_type.deleted_at');
                        $sql_promotion_product_client_type = DB::connection('tenant')->table('mo_promotion_product_client_type')
                            ->whereNull('mo_promotion_product_client_type.deleted_at');

                    }

                    if($request->get('ratings') == 1) {
                        $sql_rating = DB::connection('tenant')->table('mo_comment')
                            ->select(
                                DB::connection('tenant')->raw('avg(mo_comment.rating) as avg'),
                                DB::connection('tenant')->raw('count(mo_comment.id) as count')
                            )
                            ->whereNull('mo_comment.deleted_at')
                            ->where('mo_comment.language_id', $idioma->id)
                            ->where('mo_comment.comment_status_id', 2);
                    }

                    if ($id != null) {
                        $sql_productos_directo->where('mo_product.id', '=', $id);
                        $sql_count_productos_directo->where('mo_product.id', '=', $id);
                    }

                    if ($request->get('code') != '') {
                        $sql_productos_directo->where('mo_product.code', '=', $request->get('code'));
                        $sql_count_productos_directo->where('mo_product.code', '=', $request->get('code'));
                    }

                    if ($request->get('cross_products') != '') {
                        $sql_productos_directo->whereIn('mo_product.id', $request->get('cross_products'));
                        $sql_count_productos_directo->whereIn('mo_product.id', $request->get('cross_products'));

                    }

                    if ($subchannel->view_blocked == 0) {

                        $sql_productos_directo->leftJoin('mo_view_product', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                                ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                                ->whereNull('mo_view_product.deleted_at');
                        })
                            ->leftJoin('mo_view_product_translation', function ($join) use ($idioma) {
                                $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                                    ->where('mo_view_product_translation.language_id', '=', $idioma->id)
                                    ->whereNull('mo_view_product_translation.deleted_at');
                            })->Where(function ($query) {
                                $query->where('mo_view_product.published', '=', '1');

                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_product.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '0');
                                });
                            });


                        $sql_count_productos_directo->leftJoin('mo_view_product', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                                ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                                ->whereNull('mo_view_product.deleted_at');
                        })
                            ->leftJoin('mo_view_product_translation', function ($join) use ($idioma) {
                                $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                                    ->where('mo_view_product_translation.language_id', '=', $idioma->id)
                                    ->whereNull('mo_view_product_translation.deleted_at');
                            })->Where(function ($query) {
                                $query->where('mo_view_product.published', '=', '1');

                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_product.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '0');
                                });
                            });
                    } else {
                        $sql_productos_directo->join('mo_view_product', 'mo_view_product.product_id', '=', 'mo_product.id')
                            ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                            ->whereNull('mo_view_product.deleted_at')
                            ->where('mo_view_product.published', '=', 1)
                            ->join('mo_view_product_translation', 'mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                            ->where('mo_view_product_translation.language_id', '=', $idioma->id)
                            ->whereNull('mo_view_product_translation.deleted_at');

                        $sql_count_productos_directo->join('mo_view_product', 'mo_view_product.product_id', '=', 'mo_product.id')
                            ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                            ->whereNull('mo_view_product.deleted_at')
                            ->where('mo_view_product.published', '=', 1)
                            ->join('mo_view_product_translation', 'mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')
                            ->where('mo_view_product_translation.language_id', '=', $idioma->id)
                            ->whereNull('mo_view_product_translation.deleted_at');
                    }

                    //Filtro importe
                    if ($request->get('money') != '') {

                        $monedas = DB::connection('tenant')->table('mo_currency')
                            ->select(
                                'mo_currency.*')
                            ->where('mo_currency.deleted_at', '=', null)
                            ->join('mo_currency_exchange', 'mo_currency.id', '=', 'mo_currency_exchange.currency_id')
                            ->where('mo_currency_exchange.deleted_at', '=', null)->get();

                        $sql_productos_directo->where(function ($query) use ($request, $monedas, $id_moneda_convertir, $sql_productos_directo, $settings_decimales) {
                            foreach ($monedas as $moneda) {

                                $moneda_convertida = conversor($request->get('money'), $id_moneda_convertir, $moneda->id, $settings_decimales);

                                $query->orwhere(function ($query2) use ($request, $moneda, $moneda_convertida) {
                                    $query2->where('mo_price.currency_id', '=', $moneda->id);
                                    $query2->where('mo_price_product.sale_price', '<=', $moneda_convertida);
                                });
                            }
                        });

                        $sql_count_productos_directo->where(function ($query) use ($request, $monedas, $id_moneda_convertir, $sql_productos_directo, $settings_decimales) {
                            foreach ($monedas as $moneda) {
                                $moneda_convertida = conversor($request->get('money'), $id_moneda_convertir, $moneda->id, $settings_decimales);

                                $query->orwhere(function ($query2) use ($request, $moneda, $moneda_convertida) {
                                    $query2->where('mo_price.currency_id', '=', $moneda->id);
                                    $query2->where('mo_price_product.sale_price', '<=', $moneda_convertida);
                                });

                            }
                        });
                    }

                    if ($request->get('type_id') != '') {
                        $sql_productos_directo->where('mo_product_type.id', '=', $request->get('type_id'));
                        $sql_count_productos_directo->where('mo_product_type.id', '=', $request->get('type_id'));
                    }

                    if ($request->get('category_id') != '') {
                        $sql_productos_directo->where('mo_category.id', '=', $request->get('category_id'));
                        $sql_count_productos_directo->where('mo_category.id', '=', $request->get('category_id'));
                    }

                    if($request->get('types') != ''){
                        $sql_productos_directo->whereIn('mo_product_type.id', $request->get('types'));
                        $sql_count_productos_directo->whereIn('mo_product_type.id', $request->get('types'));
                    }

                    if ($request->get('categories') != '') {
                        $sql_productos_directo->whereIn('mo_category.id', $request->get('categories'));
                        $sql_count_productos_directo->whereIn('mo_category.id', $request->get('categories'));
                    }

                    if ($request->get('avail') != '') {
                        $sql_productos_directo->where(function ($query) use ($request) {
                            $query->where('mo_availability.avail', '>=', $request->get('avail'))
                                ->orWhere('mo_availability.unlimited', '=', 1);
                        });
                        $sql_count_productos_directo->where(function ($query) use ($request) {
                            $query->where('mo_availability.avail', '>=', $request->get('avail'))
                                ->orWhere('mo_availability.unlimited', '=', 1);
                        });
                    }

                    if ($request->get('depends_on') != '') {
                        $sql_productos_directo->where('mo_location.depends_on', '=', $request->get('depends_on'));
                        $sql_count_productos_directo->where('mo_location.depends_on', '=', $request->get('depends_on'));
                    }

                    if ($request->get('tag_id') != '') {
                        $sql_productos_directo->where('mo_tag.id', $request->get('tag_id'));
                        $sql_count_productos_directo->where('mo_tag.id', $request->get('tag_id'));
                    }

                    if ($request->get('location_id') != '') {
                        $sql_productos_directo->where('mo_product_location.location_id', '=', $request->get('location_id'))
                            ->where('mo_price_product.location_id', '=', $request->get('location_id'));
                        $sql_count_productos_directo->where('mo_product_location.location_id', '=', $request->get('location_id'))
                            ->where('mo_price_product.location_id', '=', $request->get('location_id'));

                        $sql_locaciones->where('mo_location.id', $request->get('location_id'));
                    }

                    if ($request->get('salable') != '') {
                        $sql_productos_directo->where('mo_product.salable', $request->get('salable'));
                        $sql_count_productos_directo->where('mo_product.salable', $request->get('salable'));
                    }

                    if($request->get('client_types') != '' && is_array($request->get('client_types'))) {

                        $sql_tipos_de_cliente_producto = DB::connection('tenant')->table('mo_product')
                            ->select(
                                'mo_product.id'
                            )->whereNull('mo_product.deleted_at')
                            ->join('mo_price_product','mo_price_product.product_id','mo_product.id')
                            ->whereNull('mo_price_product.deleted_at')
                            ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
                            ->whereNull('mo_price.deleted_at')
                            ->where('mo_price.date_start', '<=', $fecha)
                            ->where('mo_price.date_end', '>=', $fecha)
                            ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                            ->whereNull('mo_contract_model.deleted_at')
                            ->leftJoin('mo_contract_model_product',function ($join){
                                $join->on('mo_contract_model_product.contract_model_id','mo_contract_model.id')
                                    ->whereNull('mo_contract_model_product.deleted_at')
                                    ->whereNull('mo_contract_model_product.service_id');
                                $join->on('mo_contract_model_product.product_id','mo_product.id')
                                    ->whereNull('mo_contract_model_product.deleted_at')
                                    ->whereNull('mo_contract_model_product.service_id');
                            })
                            ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                            ->whereNull('mo_subchannel.deleted_at')
                            ->where('mo_subchannel.id', '=', $emulated_subchannel_id)
                            ->whereIn('mo_price_product.client_type_id', $request->get('client_types'))
                            ->groupBy('mo_product.id')
                            ->havingRaw(DB::connection('tenant')->raw('COUNT(mo_product.id) >=' . sizeof($request->get('client_types'))));

                        $array_productos = array_column(collect(DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_tipos_de_cliente_producto->select('mo_product.id')->toSql()}) as sub"))
                                ->mergeBindings($sql_tipos_de_cliente_producto)->get(['id']))->map(function ($x) {
                                return (array)$x;
                            })->toArray(), 'id');

                        $sql_productos_directo->whereIn('mo_product.id', $array_productos);
                        $sql_count_productos_directo->whereIn('mo_product.id', $array_productos);
                    }

                    //Establece la ordenación de los resultados
                    $orden = 'aux_order';
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_product_translation.name';
                            break;
                        case 'sale_price':
                            $orden = 'mo_price_product.sale_price';
                            break;
                        case 'net_price':
                            $orden = 'mo_price_product.net_price';
                            break;
                        case 'type':
                            $orden = 'mo_product.type_id';
                            break;
                        case 'code':
                            $orden = 'mo_product.code';
                            break;
                        default:
                            $orden = 'aux_order';
                            break;
                    }
                    //Establece el sentido de ordenación
                    $orden_way = 'DESC';
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $orden_way = 'ASC';
                            break;
                        case 'desc':
                            $orden_way = 'DESC';
                            break;
                        default:
                            $orden_way = 'DESC';
                            break;
                    }
                    $sql_productos_directo->orderBy($orden, $orden_way)->groupBy('mo_product.id');

                    if ($request->get('search') != '') {

                        $sql_productos_indirectos = clone $sql_productos_directo;
                        $sql_count_productos_indirecto = clone $sql_count_productos_directo;

                        $sql_productos_directo->where(function ($query) use ($request) {
                            $query->where('mo_product_translation.name', 'LIKE', '%' . $request->get('search') . '%')
                                ->orwhere('mo_tag_translation.name', 'LIKE', '%' . $request->get('search') . '%');
                        });

                        $sql_productos_indirectos->where(function ($query) use ($request) {
                            $query->orwhere('mo_product_translation.description', 'LIKE', '%' . $request->get('search') . '%')
                                ->orWhere('mo_product_translation.short_description', 'LIKE', '%' . $request->get('search') . '%')
                                ->orWhere('mo_product_translation.keywords_seo', 'LIKE', '%' . $request->get('search') . '%')
                                ->orWhere('mo_product_type_translation.name', 'LIKE', '%' . $request->get('search') . '%');
                        })
                            ->where('mo_product_translation.name', 'NOT LIKE', '%' . $request->get('search') . '%');

                        $sql_count_productos_directo->where(function ($query) use ($request) {
                            $query->where('mo_product_translation.name', 'LIKE', '%' . $request->get('search') . '%')
                                ->orwhere('mo_tag_translation.name', 'LIKE', '%' . $request->get('search') . '%');
                        });

                        $sql_count_productos_indirecto
                            ->where(function ($query) use ($request) {
                                $query->orwhere('mo_product_translation.description', 'LIKE', '%' . $request->get('search') . '%')
                                    ->orWhere('mo_product_translation.short_description', 'LIKE', '%' . $request->get('search') . '%')
                                    ->orWhere('mo_product_translation.keywords_seo', 'LIKE', '%' . $request->get('search') . '%')
                                    ->orWhere('mo_product_type_translation.name', 'LIKE', '%' . $request->get('search') . '%');
                            })
                            ->where('mo_product_translation.name', 'NOT LIKE', '%' . $request->get('search') . '%');

                        $sql_count_productos_directo->groupBy('mo_product.id');
                        $sql_count_productos_directo = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_count_productos_directo->toSql()}) as sub"))
                            ->mergeBindings($sql_count_productos_directo);
                        $sql_count_productos_indirecto->groupBy('mo_product.id');
                        $sql_count_productos_indirecto = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_count_productos_indirecto->toSql()}) as sub"))
                            ->mergeBindings($sql_count_productos_indirecto);

                        if ($request->get('friendly_url') != '') {
                            $sql_productos_directo = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_productos_directo->toSql()}) as sub"))
                                ->mergeBindings($sql_productos_directo);
                            $sql_productos_directo->where('aux_friendly_url', $request->get('friendly_url'));
                            $sql_productos_indirectos = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_productos_indirectos->toSql()}) as sub"))
                                ->mergeBindings($sql_productos_indirectos);
                            $sql_productos_indirectos->where('aux_friendly_url', $request->get('friendly_url'));

                            $sql_count_productos_directo->where('aux_friendly_url', $request->get('friendly_url'));
                            $sql_count_productos_indirecto->where('aux_friendly_url', $request->get('friendly_url'));
                        }

                        if ($request->get('home_app_hotel') != '') {
                            $sql_productos_directo = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_productos_directo->toSql()}) as sub"))
                                ->mergeBindings($sql_productos_directo);
                            $sql_productos_directo->where('aux_home_app_hotel', $request->get('home_app_hotel'));
                            $sql_productos_indirectos = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_productos_indirectos->toSql()}) as sub"))
                                ->mergeBindings($sql_productos_indirectos);
                            $sql_productos_indirectos->where('aux_home_app_hotel', $request->get('home_app_hotel'));

                            $sql_count_productos_directo->where('aux_home_app_hotel', $request->get('home_app_hotel'));
                            $sql_count_productos_indirecto->where('aux_home_app_hotel', $request->get('home_app_hotel'));
                        }

                        $datos_count_resultados_directos = $sql_count_productos_directo->count();
                        $datos_count_resultados_indirectos = $sql_count_productos_indirecto->count();

                        $sql_resultados_productos = $sql_productos_directo->union($sql_productos_indirectos);
                        $total = $datos_count_resultados_directos + $datos_count_resultados_indirectos;

                    } else {

                        $sql_count_productos_directo->groupBy('mo_product.id');

                        $datos_count_resultados_indirectos = 0;
                        $sql_count_productos_directo = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_count_productos_directo->toSql()}) as sub"))
                        ->mergeBindings($sql_count_productos_directo);

                        if ($request->get('friendly_url') != '') {
                            
                            $sql_count_productos_directo->where('aux_friendly_url', $request->get('friendly_url'));

                            $sql_productos_directo = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_productos_directo->toSql()}) as sub"))
                                ->mergeBindings($sql_productos_directo);
                            $sql_productos_directo->where('aux_friendly_url', $request->get('friendly_url'));
                        }

                        if ($request->get('home_app_hotel') != '') {
                            $sql_count_productos_directo->where('aux_home_app_hotel', $request->get('home_app_hotel'));

                            $sql_productos_directo = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_productos_directo->toSql()}) as sub"))
                                ->mergeBindings($sql_productos_directo);
                            $sql_productos_directo->where('aux_home_app_hotel', $request->get('home_app_hotel'));
                        }

                        $datos_count_resultados_directos = $sql_count_productos_directo->count();

                        $sql_resultados_productos = $sql_productos_directo;
                        $total = $datos_count_resultados_directos;
                    }


                    //Inicializa variables paginación
                    $limit = Settings::where('name', '=', 'limit_registers')->first()->value;
                    $page = 1;

                    // Paginación según filtros y ejecución de la consulta
                    if ($request->get('limit') != '0') {

                        $settings = Settings::where('name', '=', 'limit_registers')->first();
                        $limite = $settings->value;

                        if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                            $limite = $request->get('limit');
                        }


                        $inicio = 0;
                        if ($request->get('page') != '') {
                            $inicio = $request->get('page');
                        }

                        $datos_resultados_productos = $sql_resultados_productos->forPage($inicio, $limite)->get();

                        //si filtro limit = 0 se obtienen todos los resultados
                    } else {

                        $datos_resultados_productos = $sql_resultados_productos->get();

                    }


                    $storage = Settings::where('name', '=', 'storage_files')->first()->value;

                    $array['data'] = array();
                    //consulta para obtener archivos seo para facebook y twitter
                    $sql_images_og_twitter = DB::connection('tenant')->table('mo_file')
                        ->select('mo_file.id', 'mo_file.url_file');
                    //fin consulta para obtener archivos seo para facebook y twitter

                    //Recorre resultados unificados
                    $fecha_hora = Carbon::now()->format('Y-m-d H:i:s');

                    foreach ($datos_resultados_productos as $producto) {

                        $sql_cambio_actual = clone $sql_currency_exchange;
                        $sql_cambio_moneda_convertir = clone $sql_currency_exchange;

                        $cambio_actual = $sql_cambio_actual
                            ->where('currency_id', $producto->currency_id)
                            ->where('date_time_start', '<=', $fecha_hora)
                            ->orderBy('date_time_start', 'desc')
                            ->first()
                            ->exchange;

                        $cambio_moneda_convertir = $sql_cambio_moneda_convertir
                            ->where('currency_id', $id_moneda_convertir)
                            ->where('date_time_start', '<=', $fecha_hora)
                            ->orderBy('date_time_start', 'desc')
                            ->first()
                            ->exchange;
                        
                        

                        //clona consulta preparada para hreflang de seo
                        $datos_hreflang = clone $sql_hreflang;

                        $array_salida_producto = array();

                        $array_salida_producto['id'] = $producto->id;
                        $array_salida_producto['longitude'] = $producto->longitude;
                        $array_salida_producto['latitude'] = $producto->latitude;
                        $array_salida_producto['company_id'] = $producto->company_id;
                        $array_salida_producto['order'] = $producto->aux_order;
                        $array_salida_producto['billable'] = $producto->billable;
                        $array_salida_producto['code'] = $producto->code;
                        $array_salida_producto['height_from'] = $producto->height_from;
                        $array_salida_producto['height_to'] = $producto->height_to;
                        $array_salida_producto['weight_from'] = $producto->weight_from;
                        $array_salida_producto['weight_to'] = $producto->weight_to;
                        $array_salida_producto['salable'] = $producto->salable;
                        $array_salida_producto['app_hotel_service'] = $producto->app_hotel_service;
                        $array_salida_producto['app_hotel_without_reservation'] = $producto->app_hotel_without_reservation;
                        $array_salida_producto['app_hotel_afi_park'] = $producto->app_hotel_afi_park;
                        $array_salida_producto['app_hotel_afi_tour'] = $producto->app_hotel_afi_tour;
                        $array_salida_producto['app_hotel_menu_service'] = $producto->app_hotel_menu_service;
                        $array_salida_producto['app_hotel_with_service'] = $producto->app_hotel_with_service;
                        $array_salida_producto['app_hotel_with_service_direct'] = $producto->app_hotel_with_service_direct;
                        $array_salida_producto['home_app_hotel'] = $producto->aux_home_app_hotel;

                        if($producto->required_service !== null) {
                            $array_salida_producto['required_service'] = $producto->required_service;
                            $array_salida_producto['individual_sale'] = $producto->individual_sale;
                        } else {
                            $array_salida_producto['required_service'] = 0;
                            $array_salida_producto['individual_sale'] = 0;
                        }


                        $sql_locaciones_clone = clone $sql_locaciones;
                        $datos_locaciones = $sql_locaciones_clone->where('mo_product_location.product_id', $producto->id)->get();

                        // Etiquetas
                        $sql_tags_clone = clone $sql_tags;
                        $datos_tags = $sql_tags_clone->where('mo_product_tag.product_id', '=', $producto->id)->get();

                        $array_salida_producto['tag'] = array();

                        foreach ($datos_tags as $tag) {
                            $array_traduccion = array();
                            $array_traduccion[][$idioma->abbreviation] = [
                                'language_id' => $tag->language_id,
                                'name' => $tag->name,
                                'short_description' => $tag->short_description,
                            ];
                            $array_salida_producto['tag'][] = ['id' => $tag->id, 'lang' => $array_traduccion];
                        }
                        // FIN Etiquetas

                        //obtiene urls de archivos para campos seo imagen de facebook y twitter
                        $sql_images_og_twitter_clone = clone $sql_images_og_twitter;
                        $datos_images_og_twitter = $sql_images_og_twitter_clone->whereIn('mo_file.id', [$producto->view_og_image, $producto->view_twitter_image])->get();

                        $og_image = null;
                        $twitter_image = null;
                        foreach ($datos_images_og_twitter as $image) {
                            if ($image->id == $producto->view_og_image) {
                                $og_image = $storage . $image->url_file;
                            }
                            if ($image->id == $producto->view_twitter_image) {
                                $twitter_image = $storage . $image->url_file;
                            }
                        }
                        //fin obtiene urls de archivos para campos seo imagen de facebook y twitter


                        $array_salida_producto['file'] = array();

                        if ($producto->view_id != null) {

                            $array_salida_producto['lang'][][$idioma->abbreviation] = [
                                'language_id' => $producto->view_language_id,
                                'name' => $producto->view_name,
                                'description' => $producto->view_description,
                                'features' => $producto->view_features,
                                'recommendations' => $producto->view_recommendations,
                                'short_description' => $producto->view_short_description,
                                'friendly_url' => $producto->view_friendly_url,
                                'discover_url' => $producto->view_discover_url,
                                'title_seo' => $producto->view_title_seo,
                                'description_seo' => $producto->view_description_seo,
                                'keywords_seo' => $producto->view_keywords_seo,
                                'legal' => $producto->view_legal,

                                'breadcrumb' => $producto->view_breadcrumb,
                                'rel' => $producto->view_rel,
                                'index' => $producto->view_index,
                                'og_title' => $producto->view_og_title,
                                'og_description' => $producto->view_og_description,
                                'og_image' => $og_image,
                                'twitter_title' => $producto->view_twitter_title,
                                'twitter_description' => $producto->view_twitter_description,
                                'twitter_image' => $twitter_image,
                                'canonical_url' => $producto->view_canonical_url,
                                'script_head' => $producto->view_script_head,
                                'script_body' => $producto->view_script_body,
                                'script_footer' => $producto->view_script_footer,
                            ];


                            $datos_hreflang = $datos_hreflang
                                ->where('mo_view_product.product_id',$producto->id)
                                ->join('mo_language','mo_language.id','mo_view_product_translation.language_id')
                                ->where('mo_language.front',1)
                                ->whereNull('mo_language.deleted_at')
                                ->get();

                            // Si es el listado: si se envía tipo de archivo se muestran los indicados, si no se muestra uno de tipo imagen
                            // Si es el detalle: si se envía tipo de archivo se muestran los indicados, si no se muestran todos
                            $sql_archivos_view_clone = clone $sql_archivos_view;
                            $sql_archivos_view_clone->where('mo_view_product_file.view_product_id', '=', $producto->view_id);
                            if ($id == null && $request->get('code') == null && $request->get('friendly_url') == null) {
                                if ($request->get('file_types') != '') {
                                    $array_file_types = $request->get('file_types');
                                    if (isset($array_file_types) && isset($array_file_types) != '') {
                                        $sql_archivos_view_clone->whereIn('mo_file.type_id', $array_file_types);
                                    }
                                } else {
                                    $sql_archivos_view_clone->where('mo_file.type_id', 3)->limit(1);
                                }
                            } else {
                                if ($request->get('file_types') != '') {
                                    $array_file_types = $request->get('file_types');
                                    if (isset($array_file_types) && isset($array_file_types) != '') {
                                        $sql_archivos_view_clone->whereIn('mo_file.type_id', $array_file_types);
                                    }
                                } else {
                                    // No se modifica la consulta
                                }
                            }
                            $datos_archivos = $sql_archivos_view_clone->get();
                            //


                        } else {

                            $array_salida_producto['lang'][][$idioma->abbreviation] = [
                                'language_id' => $producto->language_id,
                                'name' => $producto->name,
                                'description' => $producto->description,
                                'features' => $producto->features,
                                'recommendations' => $producto->recommendations,
                                'short_description' => $producto->short_description,
                                'friendly_url' => $producto->friendly_url,
                                'discover_url' => $producto->discover_url,
                                'title_seo' => $producto->title_seo,
                                'description_seo' => $producto->description_seo,
                                'keywords_seo' => $producto->keywords_seo,
                                'legal' => $producto->legal,

                                'breadcrumb' => $producto->breadcrumb,
                                'rel' => $producto->rel,
                                'index' => $producto->index,
                                'og_title' => $producto->og_title,
                                'og_description' => $producto->og_description,
                                'og_image' => $og_image,
                                'twitter_title' => $producto->twitter_title,
                                'twitter_description' => $producto->twitter_description,
                                'twitter_image' => $twitter_image,
                                'canonical_url' => $producto->canonical_url,
                                'script_head' => $producto->script_head,
                                'script_body' => $producto->script_body,
                                'script_footer' => $producto->script_footer,
                            ];

                            $datos_hreflang = $datos_hreflang
                                ->where('mo_product.id',$producto->id)
                                ->join('mo_language','mo_language.id','mo_product_translation.language_id')
                                ->where('mo_language.front',1)
                                ->whereNull('mo_language.deleted_at')
                                ->get();

                            // Si es el listado: si se envía tipo de archivo se muestran los indicados, si no se muestra uno de tipo imagen
                            // Si es el detalle: si se envía tipo de archivo se muestran los indicados, si no se muestran todos
                            $sql_archivos_clone = clone $sql_archivos;
                            $sql_archivos_clone->where('mo_product_file.product_id', $producto->id);
                            if ($id == null && $request->get('code') == null && $request->get('friendly_url') == null) {
                                if ($request->get('file_types') != '') {
                                    $array_file_types = $request->get('file_types');
                                    if (isset($array_file_types) && isset($array_file_types) != '') {
                                        $sql_archivos_clone->whereIn('mo_file.type_id', $array_file_types);
                                    }
                                } else {
                                    $sql_archivos_clone->where('mo_file.type_id', 3)->limit(1);
                                }
                            } else {
                                if ($request->get('file_types') != '') {
                                    $array_file_types = $request->get('file_types');
                                    if (isset($array_file_types) && isset($array_file_types) != '') {
                                        $sql_archivos_clone->whereIn('mo_file_type.id', $array_file_types);
                                    }
                                } else {
                                    // No se modifica la consulta
                                }
                            }
                            $datos_archivos = $sql_archivos_clone->get();
                            //
                        }

                        $hrefs = [];
                        foreach ($datos_hreflang as $href){
                            $hrefs[] = [
                                'abbreviation' => $href->abbreviation,
                                'friendly_url' => $href->friendly_url
                            ];
                        }

                        $array_salida_producto['friendly_hreflang'] = $hrefs;

                        // Files
                        foreach ($datos_archivos as $archivo) {
                            $array_traduccion = array();
                            $array_traduccion[][$idioma->abbreviation] = [
                                'id' => $archivo->id,
                                'language_id' => $archivo->language_id,
                                'name' => $archivo->name,
                                'alternative_text' => $archivo->alternative_text,
                            ];
                            $array_salida_producto['file'][] = ['id' => $archivo->file_id,
                                'url_file' => $storage . $archivo->url_file,
                                'file_type_id' => $archivo->file_type,'order' => $archivo->order,
                                'device_id' => $archivo->device_id,'key_file' => $archivo->key_file,
                                'lang' => $array_traduccion,
                            ];
                        }
                        //


                        // Tipo de producto
                        $sql_product_type_translation_clone = clone $sql_product_type_translation;
                        $datos_product_type = $sql_product_type_translation_clone->where('language_id', $idioma->id)
                            ->where('type_id', $producto->product_type_id)
                            ->first();

                        $array_tipo_producto = array();
                        $array_tipo_producto['id'] = $producto->product_type_id;
                        if ($datos_product_type) {
                            $array_tipo_producto['lang'][][$idioma->abbreviation] = [
                                'language_id' => $idioma->id,
                                'name' => $datos_product_type->name,
                            ];
                        }
                        $array_salida_producto['type'][] = $array_tipo_producto;
                        // FIN Tipo de producto


                        // Categoria

                        $sql_product_category_clone = clone $sql_product_category;

                        $datos_categoria = $sql_product_category_clone->where('mo_product_category.product_id', $producto->id)
                            ->where('mo_category_translation.language_id', $idioma->id)
                            ->get();

                            $array_salida_producto['category'] = array();
                        foreach ($datos_categoria as $categoria) {

                            $array_categoria_producto = array();
                            $array_categoria_producto['id'] = $categoria->category_id;
                            $array_categoria_producto['lang'][][$idioma->abbreviation] = [
                                'language_id' => $idioma->id,
                                'name' => $categoria->name,
                            ];
                            $array_salida_producto['category'][] = $array_categoria_producto;
                        }

                        $price_from = 0;

                        // FIN Tipo de categoria

                        $array_traduccion = array();

                        $promociones_finales = array();

                        $array_salida_producto['location'] = array();
                        foreach ($datos_locaciones as $locacion) {

                            if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1){

                                $sql_promotions_clone = clone $sql_promotions;
                                $sql_promotion_product_clone = clone $sql_promotion_product;
                                $sql_promotion_product_type_clone = clone $sql_promotion_product_type;

                                $sql_promotion_product_clone->where('mo_promotion_product.product_id', $producto->id)
                                    ->where(function ($query) use ($locacion) {
                                        $query->where('mo_promotion_product.location_id', $locacion->id)
                                            ->orWhereNull('mo_promotion_product.location_id');
                                    });
                                $sql_promotion_product_type_clone->where('mo_promotion_product_type.product_type_id', $producto->product_type_id);

                                $sql_promotions_clone->union($sql_promotion_product_clone);
                                $sql_promotions_clone->union($sql_promotion_product_type_clone);

                                $promociones = $sql_promotions_clone->orderBy('order', 'desc')->get();

                                foreach($promociones as $promocion) {

                                    $aplica = 1;

                                    if(!is_null($promocion->anticipation_start) && !is_null($promocion->anticipation_end)) {

                                        $diferencia = Carbon::parse($fecha)->startOfDay()->diff(Carbon::now()->startOfDay())->days;

                                        if($diferencia < $promocion->anticipation_start || $diferencia > $promocion->anticipation_end){
                                            $aplica = 0;
                                        }
                                    }

                                    if($promocion->accumulate == 1 && $aplica == 1){
                                        foreach($promociones_finales as $promocion_final) {
                                            if($promocion_final->accumulate == 1){
                                                $sql_accumulate_promotion_final = clone $sql_accumulate_promotion;
                                                $accumulate_final = $sql_accumulate_promotion_final
                                                    ->where('mo_promotion_accumulate.promotion_id', $promocion_final->id)
                                                    ->where('mo_promotion_accumulate.promotion_accumulate_id', $promocion->id)
                                                    ->first();
                                                $sql_accumulate = clone $sql_accumulate_promotion;
                                                $accumulate = $sql_accumulate
                                                    ->where('mo_promotion_accumulate.promotion_id', $promocion->id)
                                                    ->where('mo_promotion_accumulate.promotion_accumulate_id', $promocion_final->id)
                                                    ->first();

                                                if(!$accumulate_final || !$accumulate) {
                                                    $aplica = 0;
                                                }
                                            } else {
                                                $aplica = 0;
                                            }
                                        }
                                    } else {
                                        if(count($promociones_finales) > 0) {
                                            $aplica = 0;
                                        }
                                    }

                                    if($aplica == 1){
                                        $promociones_finales[] = $promocion;
                                    }
                                }
                            }

                            $sql_availabilities_clone = clone $sql_availabilities;
                            $sql_availabilities_clone_clone = $sql_availabilities_clone
                                ->where('mo_availability.product_id', '=', $producto->id)
                                ->where('mo_availability.location_id', '=', $locacion->id)
                                ->where('mo_availability.date', '=', $fecha);

                            $count_sql_availabilities_clone_clone = $sql_availabilities_clone_clone->get();


                            if (count($count_sql_availabilities_clone_clone) <= '0') {
                                $sql_availabilities_clone = clone $sql_availabilities;
                                $sql_availabilities_clone_clone = $sql_availabilities_clone
                                    ->where('mo_availability.product_id', '=', $producto->id)
                                    ->where('mo_availability.location_id', '=', $locacion->id)
                                    ->where('mo_availability.date', '=', null);
                            }

                            if($request->get('ticketportal') != '' && $request->get('ticketportal') == 1 && $request->get('avail') != '') {
                                $sql_availabilities_clone_clone->where(function ($query) use ($request) {
                                    $query->where('mo_availability.avail', '>=', $request->get('avail'))
                                        ->orWhere('mo_availability.unlimited', '=', 1);
                                });
                            }

                            $array_location = [
                                'id' => $locacion->id,
                                'name' => $locacion->name,
                                'depends_on' => $locacion->depends_on,
                            ];

                            $sql_client_type_clone = clone $sql_client_type;
                            $datos_client_type = $sql_client_type_clone->where('mo_price_product.location_id', '=', $locacion->id)
                                ->where('mo_price_product.product_id', '=', $producto->id)
                                ->whereNull('mo_price_product.service_id')
                                ->where('mo_price_product.price_id', $producto->price_id)->get();
                            $array_location['client_type'] = array();

                            foreach ($datos_client_type as $tipo_cliente) {
                                // Datos del tipo de cliente
                                $array_tipo_cliente = array();
                                $array_tipo_cliente['id'] = $tipo_cliente->id;
                                $array_tipo_cliente['code'] = $tipo_cliente->code;
                                $array_tipo_cliente['from'] = $tipo_cliente->from;
                                $array_tipo_cliente['to'] = $tipo_cliente->to;
                                $array_tipo_cliente['hotel_age_required'] = $tipo_cliente->hotel_age_required;

                                $array_tipo_cliente['lang'][][$idioma->abbreviation] = [
                                    'language_id' => $tipo_cliente->language_id,
                                    'name' => $tipo_cliente->name
                                ];
                                // FIN Datos del tipo de cliente

                                $array_tipo_cliente['avail'] = array();
                                $array_tipo_cliente['service'] = array();

                                if ($producto->product_type_id != 2) {

                                    $array_disponibilidades = array();


                                    if ($producto->product_type_id != 1) {

                                        $datos_availabilities = $sql_availabilities_clone_clone->orderBy('mo_availability.session')
                                            ->where('mo_availability.closed', '!=', 1)
                                            ->get();

                                        foreach ($datos_availabilities as $availability) {

                                            $sql_tarifa_client_type_clone = clone $sql_tarifa_client_type;
                                            $datos_tarifa_client_type = $sql_tarifa_client_type_clone->where('mo_price_product.product_id', $producto->id)
                                                ->where('mo_price_product.price_id', $producto->price_id)
                                                ->whereNull('mo_price_product.service_id')
                                                ->where('mo_price_product.location_id', $locacion->id)
                                                ->where(function ($query) use ($availability) {
                                                    $query->where('mo_price_product.session', '=', $availability->session)
                                                        ->orWhere('mo_price_product.session', null);
                                                })
                                                ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                                ->orderBy('mo_price_product.session', 'desc')
                                                ->first();
                                            if ($datos_tarifa_client_type) {

                                                $final_sale_price = conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales);

                                                if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1 && count($promociones_finales) > 0){
                                                    foreach($promociones_finales as $promocion) {

                                                        $aplicar_promocion = true;
                                                        if($promocion->apply_client_type_filter == 1){
                                                            $sql_promotion_client_type_clone = clone $sql_promotion_client_type;
                                                            $promocion_client_type = $sql_promotion_client_type_clone
                                                                ->where('promotion_id', $promocion->id)
                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                ->first();

                                                            if(!$promocion_client_type){
                                                                $aplicar_promocion = false;
                                                            } else {
                                                                if($promocion->apply_product_filter == 1) {
                                                                    $sql_datos_filtro_product_clone = clone $sql_datos_filtro_product;
                                                                    $promotion_product = $sql_datos_filtro_product_clone
                                                                        ->where('promotion_id', $promocion->id)
                                                                        ->first();

                                                                    if($promotion_product) {
                                                                        if($promotion_product->apply_client_type_filter == 1){
                                                                            $sql_promotion_product_client_type_clone = clone $sql_promotion_product_client_type;
                                                                            $promotion_product_client_type = $sql_promotion_product_client_type_clone
                                                                                ->where('promotion_product_id', $promotion_product->id)
                                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                                ->first();

                                                                            if(!$promotion_product_client_type) {
                                                                                $aplicar_promocion = false;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }

                                                                if($promocion->apply_product_type_filter == 1){
                                                                    $sql_datos_filtro_product_type_clone = clone $sql_datos_filtro_product_type;
                                                                    $promotion_product_type = $sql_datos_filtro_product_type_clone
                                                                        ->where('promotion_id', $promotion->id)
                                                                        ->first();

                                                                    if($promotion_product_type) {
                                                                        if($promotion_product_type->apply_client_type_filter == 1) {
                                                                            $sql_promotion_product_type_client_type_clone = clone $sql_promotion_product_type_client_type;
                                                                            $promotion_product_type_client_type = $sql_promotion_product_type_client_type_clone
                                                                                ->where('promotion_product_type_id', $promotion_product_type->id)
                                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                                ->first();

                                                                            if(!$promotion_product_type_client_type) {
                                                                                $aplicar_promocion = false;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            if($promocion->apply_product_filter == 1) {
                                                                $sql_datos_filtro_product_clone = clone $sql_datos_filtro_product;
                                                                $promotion_product = $sql_datos_filtro_product_clone
                                                                    ->where('promotion_id', $promocion->id)
                                                                    ->first();

                                                                if($promotion_product) {
                                                                    if($promotion_product->apply_client_type_filter == 1){
                                                                        $sql_promotion_product_client_type_clone = clone $sql_promotion_product_client_type;
                                                                        $promotion_product_client_type = $sql_promotion_product_client_type_clone
                                                                            ->where('promotion_product_id', $promotion_product->id)
                                                                            ->where('client_type_id', $tipo_cliente->id)
                                                                            ->first();

                                                                        if(!$promotion_product_client_type) {
                                                                            $aplicar_promocion = false;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $aplicar_promocion = false;
                                                                }
                                                            }

                                                            if($promocion->apply_product_type_filter == 1){
                                                                $sql_datos_filtro_product_type_clone = clone $sql_datos_filtro_product_type;
                                                                $promotion_product_type = $sql_datos_filtro_product_type_clone
                                                                    ->where('promotion_id', $promocion->id)
                                                                    ->first();

                                                                if($promotion_product_type) {
                                                                    if($promotion_product_type->apply_client_type_filter == 1) {
                                                                        $sql_promotion_product_type_client_type_clone = clone $sql_promotion_product_type_client_type;
                                                                        $promotion_product_type_client_type = $sql_promotion_product_type_client_type_clone
                                                                            ->where('promotion_product_type_id', $promotion_product_type->id)
                                                                            ->where('client_type_id', $tipo_cliente->id)
                                                                            ->first();

                                                                        if(!$promotion_product_type_client_type) {
                                                                            $aplicar_promocion = false;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $aplicar_promocion = false;
                                                                }
                                                            }
                                                        }
                                                        $datos_filtro = null;

                                                        if($aplicar_promocion) {
                                                            if($promocion->apply_product_filter == 1 || $promocion->apply_product_type_filter) {
                                                                $sql_datos_filtro_product_clone = clone $sql_datos_filtro_product;
                                                                $datos_filtro = $sql_datos_filtro_product_clone
                                                                    ->where('mo_promotion_product.promotion_id',$promocion->id)
                                                                    ->first();

                                                                if(is_null($datos_filtro)) {
                                                                    $sql_datos_filtro_product_type_clone = clone $sql_datos_filtro_product_type;
                                                                    $datos_filtro = $sql_datos_filtro_product_type_clone
                                                                        ->where('mo_promotion_product_type.promotion_id', $promocion->id)
                                                                        ->first();
                                                                }

                                                                if(!is_null($datos_filtro->promotion_amount)) {
                                                                    if($datos_filtro->promotion_type_id == 1) {
                                                                        $final_sale_price = $final_sale_price - ($final_sale_price * ($datos_filtro->promotion_amount / 100));
                                                                    }

                                                                    if($datos_filtro->promotion_type_id == 2) {
                                                                        if ($datos_filtro->currency_id != $id_moneda_convertir) {
                                                                            $promotion_amount_total = conversor($datos_filtro->promotion_amount, $datos_filtro->currency_id, $id_moneda_convertir,$settings_decimales);
                                                                        } else {
                                                                            $promotion_amount_total = $datos_filtro->promotion_amount;
                                                                        }

                                                                        $final_sale_price -= $promotion_amount_total;
                                                                    }

                                                                    if($datos_filtro->promotion_type_id == 3){
                                                                        if($datos_filtro->currency_id != $id_moneda_convertir) {
                                                                            $final_sale_price = conversor($datos_filtro->promotion_amount, $datos_filtro->currency_id, $id_moneda_convertir,$settings_decimales);
                                                                        } else {
                                                                            $final_sale_price = $datos_filtro->promotion_amount;
                                                                        }
                                                                    }

                                                                    $final_sale_price = number_format($final_sale_price, $settings_decimales, '.', '');
                                                                }
                                                            }

                                                            if(!is_null($promocion->promotion_amount)) {
                                                                if($promocion->promotion_type_id == 1) {
                                                                    $final_sale_price = $final_sale_price - ($final_sale_price * ($promocion->promotion_amount / 100));
                                                                }

                                                                if($promocion->promotion_type_id == 2) {
                                                                    if ($promocion->currency_id != $id_moneda_convertir) {
                                                                        $promotion_amount_total = conversor($promocion->promotion_amount, $promocion->currency_id, $id_moneda_convertir,$settings_decimales);
                                                                    } else {
                                                                        $promotion_amount_total = $promocion->promotion_amount;
                                                                    }

                                                                    $final_sale_price -= $promotion_amount_total;
                                                                }

                                                                if($promocion->promotion_type_id == 3){
                                                                    if($promocion->currency_id != $id_moneda_convertir) {
                                                                        $final_sale_price = conversor($promocion->promotion_amount, $promocion->currency_id, $id_moneda_convertir,$settings_decimales);
                                                                    } else {
                                                                        $final_sale_price = $promocion->promotion_amount;
                                                                    }
                                                                }

                                                                $final_sale_price = number_format($final_sale_price, $settings_decimales, '.', '');
                                                            }
                                                        }
                                                    }
                                                }

                                                $availability->price = array();

                                                if($datos_tarifa_client_type) {

                                                    if($producto->currency_id != $id_moneda_convertir) {

                                                        $availability->price[] = [
                                                            'id' => $producto->price_id,
                                                            'net_price' => isset($datos_tarifa_client_type->net_price) ? conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales) : null,
                                                            'sale_price' => $final_sale_price,
                                                            'markup' => isset($datos_tarifa_client_type->markup) ? conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales) : null,
                                                            'price_before' => isset($datos_tarifa_client_type->sale_price) ? conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales) : null,
                                                            'currency_id' => $datos_moneda->id,
                                                            'currency_iso_code' => $datos_moneda->iso_code,
                                                        ];
                                                    } else {
                                                        $availability->price[] = [
                                                            'id' => $producto->price_id,
                                                            'net_price' => number_format($datos_tarifa_client_type->net_price, $settings_decimales, '.', ''),
                                                            'sale_price' => $final_sale_price,
                                                            'markup' => number_format($datos_tarifa_client_type->markup, $settings_decimales, '.', ''),
                                                            'price_before' => number_format($datos_tarifa_client_type->sale_price, $settings_decimales, '.', ''),
                                                            'currency_id' => $datos_moneda->id,
                                                            'currency_iso_code' => $datos_moneda->iso_code,
                                                        ];
                                                    }
                                                } else {
                                                    $availability->price[] = [
                                                        'id' => $producto->price_id,
                                                        'net_price' => null,
                                                        'sale_price' => $final_sale_price,
                                                        'markup' => null,
                                                        'price_before' => null,
                                                        'currency_id' => $datos_moneda->id,
                                                        'currency_iso_code' => $datos_moneda->iso_code,
                                                    ];
                                                }

                                            }
                                        }

                                        foreach ($datos_availabilities as $availability) {
                                            if (property_exists($availability, 'price')) {

                                                if($availability->price[0]['sale_price'] != 0 && $availability->price[0]['sale_price'] < $price_from || $price_from == 0) {
                                                    $price_from = $availability->price[0]['sale_price'];
                                                }

                                                $array_disponibilidades[] = $availability;
                                            }
                                        }
                                    } else {
                                        $array_disponibilidad = [
                                            'id' => 0,
                                            'date' => null,
                                            'session' => null,
                                            'quota' => null,
                                            'avail' => null,
                                            'overbooking' => null,
                                            'overbooking_unit' => null,
                                            'closed' => 0,
                                            'unlimited' => 1,
                                        ];

                                        $sql_tarifa_client_type_clone = clone $sql_tarifa_client_type;
                                        $datos_tarifa_client_type = $sql_tarifa_client_type_clone->where('mo_price_product.product_id', $producto->id)
                                            ->where('mo_price_product.price_id', $producto->price_id)
                                            ->whereNull('mo_price_product.service_id')
                                            ->where('mo_price_product.location_id', $locacion->id)
                                            ->Where('mo_price_product.session', null)
                                            ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                            ->orderBy('mo_price_product.session', 'desc')
                                            ->first();

                                        $final_sale_price = conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales);

                                        if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1 && count($promociones_finales) > 0){
                                            foreach($promociones_finales as $promocion) {
                                                $aplicar_promocion = true;

                                                if($promocion->apply_client_type_filter == 1){
                                                    $sql_promotion_client_type_clone = clone $sql_promotion_client_type;
                                                    $promocion_client_type = $sql_promotion_client_type_clone
                                                        ->where('promotion_id', $promocion->id)
                                                        ->where('client_type_id', $tipo_cliente->id)
                                                        ->first();

                                                    if(!$promocion_client_type){
                                                        $aplicar_promocion = false;
                                                    } else {
                                                        if($promocion->apply_product_filter == 1) {
                                                            $sql_datos_filtro_product_clone = clone $sql_datos_filtro_product;
                                                            $promotion_product = $sql_datos_filtro_product_clone
                                                                ->where('promotion_id', $promocion->id)
                                                                ->first();

                                                            if($promotion_product) {
                                                                if($promotion_product->apply_client_type_filter == 1){
                                                                    $sql_promotion_product_client_type_clone = clone $sql_promotion_product_client_type;
                                                                    $promotion_product_client_type = $sql_promotion_product_client_type_clone
                                                                        ->where('promotion_product_id', $promotion_product->id)
                                                                        ->where('client_type_id', $tipo_cliente->id)
                                                                        ->first();

                                                                    if(!$promotion_product_client_type) {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }
                                                            } else {
                                                                $aplicar_promocion = false;
                                                            }
                                                        }

                                                        if($promocion->apply_product_type_filter == 1){
                                                            $sql_datos_filtro_product_type_clone = clone $sql_datos_filtro_product_type;
                                                            $promotion_product_type = $sql_datos_filtro_product_type_clone
                                                                ->where('promotion_id', $promotion->id)
                                                                ->first();

                                                            if($promotion_product_type) {
                                                                if($promotion_product_type->apply_client_type_filter == 1) {
                                                                    $sql_promotion_product_type_client_type_clone = clone $sql_promotion_product_type_client_type;
                                                                    $promotion_product_type_client_type = $sql_promotion_product_type_client_type_clone
                                                                        ->where('promotion_product_type_id', $promotion_product_type->id)
                                                                        ->where('client_type_id', $tipo_cliente->id)
                                                                        ->first();

                                                                    if(!$promotion_product_type_client_type) {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }
                                                            } else {
                                                                $aplicar_promocion = false;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if($promocion->apply_product_filter == 1) {
                                                        $sql_datos_filtro_product_clone = clone $sql_datos_filtro_product;
                                                        $promotion_product = $sql_datos_filtro_product_clone
                                                            ->where('promotion_id', $promocion->id)
                                                            ->first();

                                                        if($promotion_product) {
                                                            if($promotion_product->apply_client_type_filter == 1){
                                                                $sql_promotion_product_client_type_clone = clone $sql_promotion_product_client_type;
                                                                $promotion_product_client_type = $sql_promotion_product_client_type_clone
                                                                    ->where('promotion_product_id', $promotion_product->id)
                                                                    ->where('client_type_id', $tipo_cliente->id)
                                                                    ->first();

                                                                if(!$promotion_product_client_type) {
                                                                    $aplicar_promocion = false;
                                                                }
                                                            }
                                                        } else {
                                                            $aplicar_promocion = false;
                                                        }
                                                    }

                                                    if($promocion->apply_product_type_filter == 1){
                                                        $sql_datos_filtro_product_type_clone = clone $sql_datos_filtro_product_type;
                                                        $promotion_product_type = $sql_datos_filtro_product_type_clone
                                                            ->where('promotion_id', $promocion->id)
                                                            ->first();

                                                        if($promotion_product_type) {
                                                            if($promotion_product_type->apply_client_type_filter == 1) {
                                                                $sql_promotion_product_type_client_type_clone = clone $sql_promotion_product_type_client_type;
                                                                $promotion_product_type_client_type = $sql_promotion_product_type_client_type_clone
                                                                    ->where('promotion_product_type_id', $promotion_product_type->id)
                                                                    ->where('client_type_id', $tipo_cliente->id)
                                                                    ->first();

                                                                if(!$promotion_product_type_client_type) {
                                                                    $aplicar_promocion = false;
                                                                }
                                                            }
                                                        } else {
                                                            $aplicar_promocion = false;
                                                        }
                                                    }
                                                }


                                                $datos_filtro = null;
                                                if($aplicar_promocion) {
                                                    if ($promocion->apply_product_filter == 1 || $promocion->apply_product_type_filter) {
                                                        $sql_datos_filtro_product_clone = clone $sql_datos_filtro_product;
                                                        $datos_filtro = $sql_datos_filtro_product_clone
                                                            ->where('mo_promotion_product.promotion_id', $promocion->id)
                                                            ->first();

                                                        if (is_null($datos_filtro)) {
                                                            $sql_datos_filtro_product_type_clone = clone $sql_datos_filtro_product_type;
                                                            $datos_filtro = $sql_datos_filtro_product_type_clone
                                                                ->where('mo_promotion_product_type.promotion_id', $promocion->id)
                                                                ->first();
                                                        }

                                                        if (!is_null($datos_filtro->promotion_amount)) {
                                                            if ($datos_filtro->promotion_type_id == 1) {
                                                                $final_sale_price = $final_sale_price - ($final_sale_price * ($datos_filtro->promotion_amount / 100));
                                                            }

                                                            if ($datos_filtro->promotion_type_id == 2) {
                                                                if ($datos_filtro->currency_id != $id_moneda_convertir) {
                                                                    $promotion_amount_total = conversor($datos_filtro->promotion_amount, $datos_filtro->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                } else {
                                                                    $promotion_amount_total = $product_type->promotion_amount;
                                                                }

                                                                $final_sale_price -= $promotion_amount_total;
                                                            }

                                                            if ($datos_filtro->promotion_type_id == 3) {
                                                                if ($datos_filtro->currency_id != $id_moneda_convertir) {
                                                                    $final_sale_price = conversor($datos_filtro->promotion_amount, $datos_filtro->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                } else {
                                                                    $final_sale_price = $datos_filtro->promotion_amount;
                                                                }
                                                            }

                                                            $final_sale_price = number_format($final_sale_price, $settings_decimales, '.', '');
                                                        }
                                                    }

                                                    if (!is_null($promocion->promotion_amount)) {
                                                        if ($promocion->promotion_type_id == 1) {
                                                            $final_sale_price = $final_sale_price - ($final_sale_price * ($promocion->promotion_amount / 100));
                                                        }

                                                        if ($promocion->promotion_type_id == 2) {
                                                            if ($promocion->currency_id != $id_moneda_convertir) {
                                                                $promotion_amount_total = conversor($promocion->promotion_amount, $promocion->currency_id, $id_moneda_convertir, $settings_decimales);
                                                            } else {
                                                                $promotion_amount_total = $promocion->promotion_amount;
                                                            }

                                                            $final_sale_price -= $promotion_amount_total;
                                                        }

                                                        if ($promocion->promotion_type_id == 3) {
                                                            if ($promocion->currency_id != $id_moneda_convertir) {
                                                                $final_sale_price = conversor($promocion->promotion_amount, $promocion->currency_id, $id_moneda_convertir, $settings_decimales);
                                                            } else {
                                                                $final_sale_price = $promocion->promotion_amount;
                                                            }
                                                        }

                                                        $final_sale_price = number_format($final_sale_price, $settings_decimales, '.', '');
                                                    }
                                                }
                                            }
                                        }

                                        if($datos_tarifa_client_type) {
                                            if($producto->currency_id != $id_moneda_convertir) {
                                                $array_disponibilidad['price'][] = [
                                                    'id' => $producto->price_id,
                                                    'net_price' => isset($datos_tarifa_client_type->net_price) ? conversorCustomSearch($datos_tarifa_client_type->net_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales) : null,
                                                    'sale_price' => $final_sale_price,
                                                    'markup' => isset($datos_tarifa_client_type->markup) ? conversorCustomSearch($datos_tarifa_client_type->markup, $cambio_actual, $cambio_moneda_convertir, $settings_decimales) : null,
                                                    'price_before' => isset($datos_tarifa_client_type->sale_price) ? conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales ) : null,
                                                    'currency_id' => $datos_moneda->id,
                                                    'currency_iso_code' => $datos_moneda->iso_code,
                                                ];
                                            } else {
                                                $array_disponibilidad['price'][] = [
                                                    'id' => $producto->price_id,
                                                    'net_price' => number_format($datos_tarifa_client_type->net_price, $settings_decimales, '.', ''),
                                                    'sale_price' => $final_sale_price,
                                                    'markup' =>  number_format($datos_tarifa_client_type->markup, $settings_decimales, '.', ''),
                                                    'price_before' => number_format($datos_tarifa_client_type->sale_price, $settings_decimales, '.', ''),
                                                    'currency_id' => $datos_moneda->id,
                                                    'currency_iso_code' => $datos_moneda->iso_code,
                                                ];
                                            }
                                        } else {
                                            $array_disponibilidad['price'][] = [
                                                'id' => $producto->price_id,
                                                'net_price' => null,
                                                'sale_price' => $final_sale_price,
                                                'markup' => null,
                                                'price_before' => null,
                                                'currency_id' => $datos_moneda->id,
                                                'currency_iso_code' => $datos_moneda->iso_code,
                                            ];
                                        }

                                        if($array_disponibilidad['price'][0]['sale_price'] != 0 && ($array_disponibilidad['price'][0]['sale_price'] < $price_from || $price_from == 0)) {
                                            $price_from = $array_disponibilidad['price'][0]['sale_price'];
                                        }

                                        $array_disponibilidades[] = $array_disponibilidad;
                                    }

                                    $array_tipo_cliente['avail'] = $array_disponibilidades;

                                } else {
                                    $array_disponibilidad = [
                                        'id' => 0,
                                        'date' => null,
                                        'session' => null,
                                        'quota' => null,
                                        'avail' => null,
                                        'overbooking' => null,
                                        'overbooking_unit' => null,
                                        'closed' => 0,
                                        'unlimited' => 1,
                                    ];

                                    $sql_tarifa_client_type_clone = clone $sql_tarifa_client_type;
                                        $datos_tarifa_client_type = $sql_tarifa_client_type_clone->where('mo_price_product.product_id', $producto->id)
                                            ->where('mo_price_product.price_id', $producto->price_id)
                                            ->whereNull('mo_price_product.service_id')
                                            ->where('mo_price_product.location_id', $locacion->id)
                                            ->Where('mo_price_product.session', null)
                                            ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                            ->orderBy('mo_price_product.session', 'desc')
                                            ->first();

                                    $final_sale_price = conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales);

                                    if($datos_tarifa_client_type) {
                                        if($producto->currency_id != $id_moneda_convertir) {
                                            $array_disponibilidad['price'][] = [
                                                'id' => $producto->price_id,
                                                'net_price' => isset($datos_tarifa_client_type->net_price) ? conversorCustomSearch($datos_tarifa_client_type->net_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales) : null,
                                                'sale_price' => $final_sale_price,
                                                'markup' => isset($datos_tarifa_client_type->markup) ? conversorCustomSearch($datos_tarifa_client_type->markup, $cambio_actual, $cambio_moneda_convertir, $settings_decimales) : null,
                                                'price_before' => isset($datos_tarifa_client_type->sale_price) ? conversorCustomSearch($datos_tarifa_client_type->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales ) : null,
                                                'currency_id' => $datos_moneda->id,
                                                'currency_iso_code' => $datos_moneda->iso_code,
                                            ];
                                        } else {
                                            $array_disponibilidad['price'][] = [
                                                'id' => $producto->price_id,
                                                'net_price' => number_format($datos_tarifa_client_type->net_price, $settings_decimales, '.', ''),
                                                'sale_price' => $final_sale_price,
                                                'markup' =>  number_format($datos_tarifa_client_type->markup, $settings_decimales, '.', ''),
                                                'price_before' => number_format($datos_tarifa_client_type->sale_price, $settings_decimales, '.', ''),
                                                'currency_id' => $datos_moneda->id,
                                                'currency_iso_code' => $datos_moneda->iso_code,
                                            ];
                                        }
                                    } else {
                                        $array_disponibilidad['price'][] = [
                                            'id' => $producto->price_id,
                                            'net_price' => null,
                                            'sale_price' => $final_sale_price,
                                            'markup' => null,
                                            'price_before' => null,
                                            'currency_id' => $datos_moneda->id,
                                            'currency_iso_code' => $datos_moneda->iso_code,
                                        ];
                                    }

                                    if($array_disponibilidad['price'][0]['sale_price'] != 0 && ($array_disponibilidad['price'][0]['sale_price'] < $price_from || $price_from == 0)) {
                                        $price_from = $array_disponibilidad['price'][0]['sale_price'];
                                    }

                                    $array_disponibilidades[] = $array_disponibilidad;

                                    $array_tipo_cliente['avail'] = $array_disponibilidades;

                                
                                }

                                $sql_servicios_clone = clone $sql_serviciosAsociados;

                                $datos_servicios = $sql_servicios_clone->where('mo_price_product.location_id', $locacion->id)
                                    ->where('mo_product_service.product_id', '=', $producto->id)
                                    ->where('mo_price_product.product_id', '=', $producto->id)
                                    ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                    ->get();
                                $array_tipo_cliente['service'] = array();

                                $array_servicio = array();
                                foreach ($datos_servicios as $servicio) {

                                    $promociones_finales_servicio = array();

                                    if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1){

                                        $sql_promotions_service_clone = clone $sql_promotions;
                                        $sql_promotion_service_clone = clone $sql_promotion_service;

                                        $sql_promotion_service_clone->where('mo_promotion_service.service_id', $servicio->id);

                                        $sql_promotions_service_clone->union($sql_promotion_service_clone);

                                        $promociones_servicios = $sql_promotions_service_clone->orderBy('order', 'desc')->get();

                                        foreach($promociones_servicios as $promocion_servicio) {

                                            $aplica = 1;

                                            if(!is_null($promocion_servicio->anticipation_start) && !is_null($promocion_servicio->anticipation_end)) {

                                                $diferencia = Carbon::parse($fecha)->startOfDay()->diff(Carbon::now()->startOfDay())->days;

                                                if($diferencia < $promocion_servicio->anticipation_start || $diferencia > $promocion_servicio->anticipation_end){
                                                    $aplica = 0;
                                                }
                                            }

                                            if($promocion_servicio->accumulate == 1 && $aplica == 1){
                                                foreach($promociones_finales_servicio as $promocion_final_servicio) {
                                                    if($promocion_final_servicio->accumulate == 1){
                                                        $sql_accumulate_final = clone $sql_accumulate_promotion_service;

                                                        $sql_accumulate = clone $sql_accumulate_promotion_service;

                                                        $accumulate_final = $sql_accumulate_promotion_service_clone
                                                            ->where('mo_promotion_accumulate.promotion_id', $promocion_final_servicio->id)
                                                            ->where('mo_promotion_accumulate.promotion_accumulate_id', $promocion_servicio->id)
                                                            ->first();

                                                        $accumulate = $sql_accumulate
                                                            ->where('mo_promotion_accumulate.promotion_id', $promocion_servicio->id)
                                                            ->where('mo_promotion_accumulate.promotion_accumulate_id', $promocion_final_servicio->id)
                                                            ->first();

                                                        if(!$accumulate_final || !$accumulate) {
                                                            $aplica = 0;
                                                        }
                                                    } else {
                                                        $aplica = 0;
                                                    }
                                                }
                                            } else {
                                                if(count($promociones_finales_servicio) > 0) {
                                                    $aplica = 0;
                                                }
                                            }

                                            if($aplica == 1){
                                                $promociones_finales_servicio[] = $promocion_servicio;
                                            }
                                        }
                                    }

                                    $array_servicio['id'] = $servicio->id;
                                    $array_servicio['lang'] = array();
                                    $array_servicio['lang'][][$idioma->abbreviation] = [
                                        'language_id' => $servicio->language_id,
                                        'name' => $servicio->name,
                                        'description' => $servicio->description,
                                    ];

                                    $array_servicio['avail'] = array();

                                    if ($producto->product_type_id != 1) {
                                        //Se obtienen las disponibilidades del servicio para la fecha indicada
                                        $sql_availability_clone = clone $sql_availability_servicios;
                                        $sql_availability_clone
                                            ->where('mo_availability.location_id', '=', $locacion->id)
                                            ->where('mo_availability.product_id', '=', $producto->id)
                                            ->where('mo_availability.date', '=', $fecha)
                                            ->where('mo_availability.service_id', '=', $servicio->id)
                                            ->where('mo_availability.location_id', $locacion->id);

                                        if ($sql_availability_clone->count() == '0') {
                                            $sql_availabilities_clone = clone $sql_availability_servicios;
                                            $sql_availability_clone = $sql_availabilities_clone
                                                ->where('mo_availability.product_id', '=', $producto->id)
                                                ->where('mo_availability.location_id', '=', $locacion->id)
                                                ->where('mo_availability.service_id', '=', $servicio->id)
                                                ->where('mo_availability.location_id', $locacion->id)
                                                ->where('mo_availability.date', '=', null);
                                        }
                                        $datos_availability = $sql_availability_clone->where('mo_availability.closed', '!=', 1)->get();

                                        $array_disponibilidades_servicios = array();

                                        foreach ($datos_availability as $availability) {
                                            if ($servicio->id != 1) { // Si el servicio no es transportación
                                                $sql_tarifa_servicio = clone $sql_tarifa_client_type;
                                                $datos_tarifa_servicio = $sql_tarifa_servicio->where('mo_price_product.product_id', $producto->id)
                                                    ->where('mo_price_product.price_id', $producto->price_id)
                                                    ->where('mo_price_product.service_id', '=', $servicio->id)
                                                    ->where('mo_price_product.location_id', '=', $locacion->id)
                                                    ->where(function ($query) use ($availability) {
                                                        $query->where('mo_price_product.session', '=', $availability->session)
                                                            ->orWhere('mo_price_product.session', null);
                                                    })
                                                    ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                                    ->orderBy('mo_price_product.session', 'desc')
                                                    ->first();


                                                if ($datos_tarifa_servicio) {

                                                    $final_sale_price_service = conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales);

                                                    if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1 && count($promociones_finales_servicio) > 0){
                                                        foreach($promociones_finales_servicio as $promocion_servicio) {

                                                            $aplicar_promocion = true;

                                                            if($promocion_servicio->apply_client_type_filter == 1) {
                                                                $sql_promotion_client_type_clone = clone $sql_promotion_client_type;
                                                                $promocion_client_type = $sql_promotion_client_type_clone
                                                                    ->where('promotion_id', $promocion_servicio->id)
                                                                    ->where('client_type_id', $tipo_cliente->id)
                                                                    ->first();

                                                                if(!$promocion_client_type){
                                                                    $aplicar_promocion = false;
                                                                } else {
                                                                    if($promocion_servicio->apply_service_filter == 1) {
                                                                        $sql_datos_filtro_servicio_clone = $sql_datos_filtro_servicio;
                                                                        $promotion_service = $sql_datos_filtro_servicio_clone
                                                                            ->where('promotion_id', $promocion_servicio->id)
                                                                            ->first();

                                                                        if($promotion_service) {
                                                                            if($promotion_service->apply_client_type_filter == 1){
                                                                                $sql_datos_filtro_service_client_type_clone = clone $sql_datos_filtro_service_client_type;
                                                                                $promotion_service_client_type = $sql_datos_filtro_service_client_type_clone
                                                                                    ->where('promotion_service_id', $promotion_service->id)
                                                                                    ->where('client_type_id', $tipo_cliente->id)
                                                                                    ->first();

                                                                                if(!$promotion_service_client_type) {
                                                                                    $aplicar_promocion = false;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            $aplicar_promocion = false;
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                if($promocion_servicio->apply_service_filter == 1) {
                                                                    $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                    $promotion_service = $sql_datos_filtro_servicio_clone
                                                                        ->where('promotion_id', $promocion_servicio->id)
                                                                        ->first();

                                                                    if($promotion_service) {
                                                                        if($promotion_service->apply_client_type_filter == 1){
                                                                            $sql_datos_filtro_service_client_type_clone = clone $sql_datos_filtro_service_client_type;
                                                                            $promotion_service_client_type = $sql_datos_filtro_service_client_type_clone
                                                                                ->where('promotion_service_id', $promotion_service->id)
                                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                                ->first();

                                                                            if(!$promotion_service_client_type) {
                                                                                $aplicar_promocion = false;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }
                                                            }

                                                            if($aplicar_promocion) {

                                                                $datos_filtro = null;
                                                                if ($promocion_servicio->apply_service_filter == 1) {
                                                                    $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                    $datos_filtro_servicio = $sql_datos_filtro_servicio_clone
                                                                        ->where('mo_promotion_service.promotion_id', $promocion_servicio->id)
                                                                        ->first();

                                                                    if (!is_null($datos_filtro_servicio->promotion_amount)) {
                                                                        if ($datos_filtro_servicio->promotion_type_id == 1) {
                                                                            $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($datos_filtro_servicio->promotion_amount / 100));
                                                                        }

                                                                        if ($datos_filtro_servicio->promotion_type_id == 2) {
                                                                            if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                                $promotion_amount_total = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                            } else {
                                                                                $promotion_amount_total = $datos_filtro_servicio->promotion_amount;
                                                                            }

                                                                            $final_sale_price_service -= $promotion_amount_total;
                                                                        }

                                                                        if ($datos_filtro_servicio->promotion_type_id == 3) {
                                                                            if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                                $final_sale_price_service = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                            } else {
                                                                                $final_sale_price_service = $datos_filtro_servicio->promotion_amount;
                                                                            }
                                                                        }

                                                                        $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                                    }
                                                                }

                                                                if (!is_null($promocion_servicio->promotion_amount)) {
                                                                    if ($promocion_servicio->promotion_type_id == 1) {
                                                                        $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($promocion_servicio->promotion_amount / 100));
                                                                    }

                                                                    if ($promocion_servicio->promotion_type_id == 2) {
                                                                        if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                            $promotion_amount_total = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $promotion_amount_total = $promocion_servicio->promotion_amount;
                                                                        }

                                                                        $final_sale_price_service -= $promotion_amount_total;
                                                                    }

                                                                    if ($promocion_servicio->promotion_type_id == 3) {
                                                                        if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                            $final_sale_price_service = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $final_sale_price_service = $promocion_servicio->promotion_amount;
                                                                        }
                                                                    }

                                                                    $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                                }
                                                            }
                                                        }
                                                    }

                                                    $availability->price = array();

                                                    if($datos_tarifa_servicio) {
                                                        if($producto->currency_id != $id_moneda_convertir){
                                                            $availability->price[] = [
                                                                'id' => $producto->price_id,
                                                                'net_price' => isset($datos_tarifa_servicio->net_price) ? conversorCustomSearch($datos_tarifa_servicio->net_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales) : null,
                                                                'sale_price' => $final_sale_price_service,
                                                                'markup' => isset($datos_tarifa_servicio->markup) ? conversorCustomSearch($datos_tarifa_servicio->markup, $cambio_actual, $cambio_moneda_convertir, $settings_decimales) : null,
                                                                'price_before' => isset($datos_tarifa_servicio->sale_price) ? conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales) : null,
                                                                'currency_id' => $datos_moneda->id,
                                                                'currency_iso_code' => $datos_moneda->iso_code,
                                                            ];
                                                        } else {
                                                            $availability->price[] = [
                                                                'id' => $producto->price_id,
                                                                'net_price' => number_format($datos_tarifa_servicio->net_price, $settings_decimales, '.', ''),
                                                                'sale_price' => $final_sale_price_service,
                                                                'markup' => number_format($datos_tarifa_servicio->markup, $settings_decimales, '.', ''),
                                                                'price_before' => number_format($datos_tarifa_servicio->sale_price, $settings_decimales, '.', ''),
                                                                'currency_id' => $datos_moneda->id,
                                                                'currency_iso_code' => $datos_moneda->iso_code,
                                                            ];
                                                        }
                                                    } else {
                                                        $availability->price[] = [
                                                            'id' => $producto->price_id,
                                                            'net_price' => null,
                                                            'sale_price' => $final_sale_price_service,
                                                            'markup' => null,
                                                            'price_before' => null,
                                                            'currency_id' => $datos_moneda->id,
                                                            'currency_iso_code' => $datos_moneda->iso_code,
                                                        ];
                                                    }

                                                }
                                            } else { // Si el servicio es transportación

                                                $sql_pickups_clone = clone $sql_pickups;
                                                $datos_pickups = $sql_pickups_clone
                                                    ->where('mo_price_product.price_id', $producto->price_id)
                                                    ->where('mo_price_product.product_id', $producto->id)
                                                    ->where('mo_price_product.service_id', $servicio->id)
                                                    ->where('mo_price_product.location_id', $locacion->id)
                                                    ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                                    ->get();
                                                $availability->pickup = array();
                                                foreach ($datos_pickups as $pickup) {
                                                    $pickup->price = array();

                                                    $sql_tarifa_servicio = clone $sql_tarifa_client_type;
                                                    $datos_tarifa_servicio = $sql_tarifa_servicio->where('mo_price_product.product_id', $producto->id)
                                                        ->where('mo_price_product.price_id', $producto->price_id)
                                                        ->where('mo_price_product.service_id', '=', $servicio->id)
                                                        ->where('mo_price_product.location_id', '=', $locacion->id)
                                                        ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                                        ->where('mo_price_product.pickup_id', '=', $pickup->id)
                                                        ->whereNull('mo_price_product.session')
                                                        ->first();

                                                    $final_sale_price_service = conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales);

                                                    if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1 && count($promociones_finales_servicio) > 0){
                                                        foreach($promociones_finales_servicio as $promocion_servicio) {

                                                            $aplicar_promocion = true;

                                                            if($promocion_servicio->apply_client_type_filter == 1) {
                                                                $sql_promotion_client_type_clone = clone $sql_promotion_client_type;
                                                                $promocion_client_type = $sql_promotion_client_type_clone
                                                                    ->where('promotion_id', $promocion_servicio->id)
                                                                    ->where('client_type_id', $tipo_cliente->id)
                                                                    ->first();

                                                                if(!$promocion_client_type){
                                                                    $aplicar_promocion = false;
                                                                } else {
                                                                    if($promocion_servicio->apply_service_filter == 1) {
                                                                        $sql_datos_filtro_service_clone = clone $sql_datos_filtro_service;
                                                                        $promotion_service = $sql_datos_filtro_service_clone
                                                                            ->where('promotion_id', $promocion_servicio->id)
                                                                            ->first();

                                                                        if($promotion_service) {
                                                                            if($promotion_service->apply_client_type_filter == 1) {
                                                                                $sql_datos_filtro_service_client_type_clone = clone $sql_datos_filtro_service_client_type;
                                                                                $promotion_service_client_type = $sql_datos_filtro_service_client_type_clone
                                                                                    ->where('promotion_service_id', $promotion_service->id)
                                                                                    ->where('client_type_id', $tipo_cliente->id)
                                                                                    ->first();

                                                                                if(!$promotion_service_client_type) {
                                                                                    $aplicar_promocion = false;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            $aplicar_promocion = false;
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                if($promocion_servicio->apply_service_filter == 1) {
                                                                    $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                    $promotion_service = $sql_datos_filtro_servicio_clone
                                                                        ->where('promotion_id', $promocion_servicio->id)
                                                                        ->first();

                                                                    if($promotion_service) {
                                                                        if($promotion_service->apply_client_type_filter == 1) {
                                                                            $sql_datos_filtro_service_client_type_clone = clone $sql_datos_filtro_service_client_type;
                                                                            $promotion_service_client_type = $sql_datos_filtro_service_client_type_clone
                                                                                ->where('promotion_service_id', $promotion_service->id)
                                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                                ->first();

                                                                            if(!$promotion_service_client_type) {
                                                                                $aplicar_promocion = false;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }
                                                            }

                                                            if($aplicar_promocion) {
                                                                $datos_filtro = null;
                                                                if ($promocion_servicio->apply_service_filter == 1) {
                                                                    $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                    $datos_filtro_servicio = $sql_datos_filtro_servicio_clone
                                                                        ->where('mo_promotion_service.promotion_id', $promocion_servicio->id)
                                                                        ->first();

                                                                    if (!is_null($datos_filtro_servicio->promotion_amount)) {
                                                                        if ($datos_filtro_servicio->promotion_type_id == 1) {
                                                                            $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($datos_filtro_servicio->promotion_amount / 100));
                                                                        }

                                                                        if ($datos_filtro_servicio->promotion_type_id == 2) {
                                                                            if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                                $promotion_amount_total = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                            } else {
                                                                                $promotion_amount_total = $datos_filtro_servicio->promotion_amount;
                                                                            }

                                                                            $final_sale_price_service -= $promotion_amount_total;
                                                                        }

                                                                        if ($datos_filtro_servicio->promotion_type_id == 3) {
                                                                            if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                                $final_sale_price_service = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                            } else {
                                                                                $final_sale_price_service = $datos_filtro_servicio->promotion_amount;
                                                                            }
                                                                        }

                                                                        $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                                    }
                                                                }

                                                                if (!is_null($promocion_servicio->promotion_amount)) {
                                                                    if ($promocion_servicio->promotion_type_id == 1) {
                                                                        $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($promocion_servicio->promotion_amount / 100));
                                                                    }

                                                                    if ($promocion_servicio->promotion_type_id == 2) {
                                                                        if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                            $promotion_amount_total = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $promotion_amount_total = $promocion_servicio->promotion_amount;
                                                                        }

                                                                        $final_sale_price_service -= $promotion_amount_total;
                                                                    }

                                                                    if ($promocion_servicio->promotion_type_id == 3) {
                                                                        if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                            $final_sale_price_service = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $final_sale_price_service = $promocion_servicio->promotion_amount;
                                                                        }
                                                                    }

                                                                    $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                                }
                                                            }
                                                        }
                                                    }

                                                    if($datos_tarifa_servicio) {
                                                        if($producto->currency_id != $id_moneda_convertir){
                                                            $pickup->price[] = [
                                                                'id' => $producto->price_id,
                                                                'net_price' => conversorCustomSearch($datos_tarifa_servicio->net_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales),
                                                                'sale_price' => $final_sale_price_service,
                                                                'markup' => conversorCustomSearch($datos_tarifa_servicio->markup, $cambio_actual, $cambio_moneda_convertir, $settings_decimales),
                                                                'price_before' => conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales),
                                                                'currency_id' => $datos_moneda->id,
                                                                'currency_iso_code' => $datos_moneda->iso_code,
                                                            ];
                                                        } else {
                                                            $pickup->price[] = [
                                                                'id' => $producto->price_id,
                                                                'net_price' => number_format($datos_tarifa_servicio->net_price, $settings_decimales, '.', ''),
                                                                'sale_price' => $final_sale_price_service,
                                                                'markup' => number_format($datos_tarifa_servicio->markup, $settings_decimales, '.', ''),
                                                                'price_before' => number_format($datos_tarifa_servicio->sale_price, $settings_decimales, '.', ''),
                                                                'currency_id' => $datos_moneda->id,
                                                                'currency_iso_code' => $datos_moneda->iso_code,
                                                            ];
                                                        }
                                                    } else {
                                                        if ($datos_tarifa_servicio) {
                                                            $pickup->price[] = [
                                                                'id' => $producto->price_id,
                                                                'net_price' => null,
                                                                'sale_price' => $final_sale_price_service,
                                                                'markup' => null,
                                                                'price_before' => null,
                                                                'currency_id' => $datos_moneda->id,
                                                                'currency_iso_code' => $datos_moneda->iso_code,
                                                            ];
                                                        }
                                                    }


                                                    $availability->pickup[] = $pickup;
                                                }
                                            }

                                            if ($servicio->id != 1) {
                                                //Si no existe precio la disponibilidad no debe mostrarse
                                                if (property_exists($availability, 'price')) {
                                                    $array_servicio['avail'][] = $availability;
                                                }
                                            } else {
                                                $array_servicio['avail'][] = $availability;
                                            }

                                        }
                                    } else {

                                        $availability = array();

                                        $availability = [
                                            'id' => 0,
                                            'date' => null,
                                            'session' => null,
                                            'quota' => null,
                                            'avail' => null,
                                            'overbooking' => null,
                                            'overbooking_unit' => null,
                                            'closed' => 0,
                                            'unlimited' => 1,
                                        ];


                                        if ($servicio->id != 1) { // Si el servicio no es transportación
                                            $sql_tarifa_servicio = clone $sql_tarifa_client_type;
                                            $datos_tarifa_servicio = $sql_tarifa_servicio->where('mo_price_product.product_id', $producto->id)
                                                ->where('mo_price_product.price_id', $producto->price_id)
                                                ->where('mo_price_product.service_id', '=', $servicio->id)
                                                ->where('mo_price_product.location_id', '=', $locacion->id)
                                                ->where('mo_price_product.session', null)
                                                ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                                ->orderBy('mo_price_product.session', 'desc')
                                                ->first();


                                            if ($datos_tarifa_servicio) {

                                                $final_sale_price_service = conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales);

                                                if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1 && count($promociones_finales_servicio) > 0){
                                                    foreach($promociones_finales_servicio as $promocion_servicio) {

                                                        $aplicar_promocion = true;

                                                        if($promocion_servicio->apply_client_type_filter == 1) {
                                                            $sql_promotion_client_type_clone = clone $sql_promotion_client_type;
                                                            $promocion_client_type = $sql_promotion_client_type_clone
                                                                ->where('promotion_id', $promocion_servicio->id)
                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                ->first();

                                                            if(!$promocion_client_type){
                                                                $aplicar_promocion = false;
                                                            } else {
                                                                if($promocion_servicio->apply_service_filter == 1) {
                                                                    $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                    $promotion_service = $sql_datos_filtro_servicio_clone
                                                                        ->where('promotion_id', $promocion_servicio->id)
                                                                        ->first();

                                                                    if($promotion_service) {
                                                                        if($promotion_service->apply_client_type_filter == 1){
                                                                            $sql_promotion_client_type_clone = clone $sql_promotion_client_type; 
                                                                            $promotion_service_client_type = $sql_promotion_client_type_clone
                                                                                ->where('promotion_service_id', $promotion_service->id)
                                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                                ->first();

                                                                            if(!$promotion_service_client_type) {
                                                                                $aplicar_promocion = false;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            if($promocion_servicio->apply_service_filter == 1) {
                                                                $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio; 
                                                                $promotion_service = $sql_datos_filtro_servicio_clone
                                                                    ->where('promotion_id', $promocion_servicio->id)
                                                                    ->first();

                                                                if($promotion_service) {
                                                                    if($promotion_service->apply_client_type_filter == 1){
                                                                        $sql_datos_filtro_service_client_type_clone = clone $sql_datos_filtro_service_client_type;
                                                                        $promotion_service_client_type = $sql_datos_filtro_service_client_type_clone
                                                                            ->where('promotion_service_id', $promotion_service->id)
                                                                            ->where('client_type_id', $tipo_cliente->id)
                                                                            ->first();

                                                                        if(!$promotion_service_client_type) {
                                                                            $aplicar_promocion = false;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $aplicar_promocion = false;
                                                                }
                                                            }
                                                        }

                                                        if($aplicar_promocion) {

                                                            $datos_filtro = null;
                                                            if ($promocion_servicio->apply_service_filter == 1) {
                                                                $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                $datos_filtro_servicio = $sql_datos_filtro_servicio_clone
                                                                    ->where('mo_promotion_service.promotion_id', $promocion_servicio->id)
                                                                    ->first();

                                                                if (!is_null($datos_filtro_servicio->promotion_amount)) {
                                                                    if ($datos_filtro_servicio->promotion_type_id == 1) {
                                                                        $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($datos_filtro_servicio->promotion_amount / 100));
                                                                    }

                                                                    if ($datos_filtro_servicio->promotion_type_id == 2) {
                                                                        if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                            $promotion_amount_total = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $promotion_amount_total = $datos_filtro_servicio->promotion_amount;
                                                                        }

                                                                        $final_sale_price_service -= $promotion_amount_total;
                                                                    }

                                                                    if ($datos_filtro_servicio->promotion_type_id == 3) {
                                                                        if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                            $final_sale_price_service = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $final_sale_price_service = $datos_filtro_servicio->promotion_amount;
                                                                        }
                                                                    }

                                                                    $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                                }
                                                            }

                                                            if (!is_null($promocion_servicio->promotion_amount)) {
                                                                if ($promocion_servicio->promotion_type_id == 1) {
                                                                    $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($promocion_servicio->promotion_amount / 100));
                                                                }

                                                                if ($promocion_servicio->promotion_type_id == 2) {
                                                                    if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                        $promotion_amount_total = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                    } else {
                                                                        $promotion_amount_total = $promocion_servicio->promotion_amount;
                                                                    }

                                                                    $final_sale_price_service -= $promotion_amount_total;
                                                                }

                                                                if ($promocion_servicio->promotion_type_id == 3) {
                                                                    if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                        $final_sale_price_service = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                    } else {
                                                                        $final_sale_price_service = $promocion_servicio->promotion_amount;
                                                                    }
                                                                }

                                                                $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                            }
                                                        }
                                                    }
                                                }
                                                $availability['price'] = array();
                                                if($producto->currency_id != $id_moneda_convertir) {
                                                    $availability['price'][] = [
                                                        'id' => $producto->price_id,
                                                        'net_price' => conversorCustomSearch($datos_tarifa_servicio->net_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales),
                                                        'sale_price' => $final_sale_price_service,
                                                        'markup' => conversorCustomSearch($datos_tarifa_servicio->markup, $cambio_actual, $cambio_moneda_convertir, $settings_decimales),
                                                        'price_before' => conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales),
                                                        'currency_id' => $datos_moneda->id,
                                                        'currency_iso_code' => $datos_moneda->iso_code,
                                                    ];
                                                } else {
                                                    $availability['price'][] = [
                                                        'id' => $producto->price_id,
                                                        'net_price' => number_format($datos_tarifa_servicio->net_price, $settings_decimales, '.', ''),
                                                        'sale_price' => $final_sale_price_service,
                                                        'markup' => number_format($datos_tarifa_servicio->markup, $settings_decimales, '.', ''),
                                                        'price_before' => number_format($datos_tarifa_servicio->sale_price, $settings_decimales, '.', ''),
                                                        'currency_id' => $datos_moneda->id,
                                                        'currency_iso_code' => $datos_moneda->iso_code,
                                                    ];
                                                }

                                            }
                                        } else { // Si el servicio es transportación

                                            $sql_pickups_clone = clone $sql_pickups;
                                            $datos_pickups = $sql_pickups_clone
                                                ->where('mo_price_product.price_id', $producto->price_id)
                                                ->where('mo_price_product.product_id', $producto->id)
                                                ->where('mo_price_product.service_id', $servicio->id)
                                                ->where('mo_price_product.location_id', $locacion->id)
                                                ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                                ->get();
                                            $availability['pickup'] = array();
                                            foreach ($datos_pickups as $pickup) {
                                                $pickup->price = array();

                                                $sql_tarifa_servicio = clone $sql_tarifa_client_type;
                                                $datos_tarifa_servicio = $sql_tarifa_servicio->where('mo_price_product.product_id', $producto->id)
                                                    ->where('mo_price_product.price_id', $producto->price_id)
                                                    ->where('mo_price_product.service_id', '=', $servicio->id)
                                                    ->where('mo_price_product.location_id', '=', $locacion->id)
                                                    ->where('mo_price_product.client_type_id', $tipo_cliente->id)
                                                    ->where('mo_price_product.pickup_id', '=', $pickup->id)
                                                    ->whereNull('mo_price_product.session')
                                                    ->first();

                                                $final_sale_price_service = conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir, $settings_decimales);

                                                if($request->get('promotions') != '' && $request->get('promotions') == 1 && $producto->promotions == 1 && count($promociones_finales_servicio) > 0){
                                                    foreach($promociones_finales_servicio as $promocion_servicio) {

                                                        $aplicar_promocion = true;

                                                        if($promocion_servicio->apply_client_type_filter == 1) {
                                                            $sql_promotion_client_type_clone = clone $sql_promotion_client_type;
                                                            $promocion_client_type = $sql_promotion_client_type_clone
                                                                ->where('promotion_id', $promocion_servicio->id)
                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                ->first();

                                                            if(!$promocion_client_type){
                                                                $aplicar_promocion = false;
                                                            } else {
                                                                if($promocion_servicio->apply_service_filter == 1) {
                                                                    $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                    $promotion_service = $sql_datos_filtro_servicio_clone
                                                                        ->where('promotion_id', $promocion_servicio->id)
                                                                        ->first();

                                                                    if($promotion_service) {
                                                                        if($promotion_service->apply_client_type_filter == 1) {
                                                                            $sql_datos_filtro_service_client_type_clone = clone $sql_datos_filtro_service_client_type;
                                                                            $promotion_service_client_type = $sql_datos_filtro_service_client_type_clone
                                                                                ->where('promotion_service_id', $promotion_service->id)
                                                                                ->where('client_type_id', $tipo_cliente->id)
                                                                                ->first();

                                                                            if(!$promotion_service_client_type) {
                                                                                $aplicar_promocion = false;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        $aplicar_promocion = false;
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            if($promocion_servicio->apply_service_filter == 1) {
                                                                $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                $promotion_service = $sql_datos_filtro_servicio_clone
                                                                    ->where('promotion_id', $promocion_servicio->id)
                                                                    ->first();

                                                                if($promotion_service) {
                                                                    if($promotion_service->apply_client_type_filter == 1) {
                                                                        $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                        $promotion_service_client_type = $sql_datos_filtro_servicio_clone
                                                                            ->where('promotion_service_id', $promotion_service->id)
                                                                            ->where('client_type_id', $tipo_cliente->id)
                                                                            ->first();

                                                                        if(!$promotion_service_client_type) {
                                                                            $aplicar_promocion = false;
                                                                        }
                                                                    }
                                                                } else {
                                                                    $aplicar_promocion = false;
                                                                }
                                                            }
                                                        }

                                                        if($aplicar_promocion) {
                                                            $datos_filtro = null;
                                                            if ($promocion_servicio->apply_service_filter == 1) {
                                                                $sql_datos_filtro_servicio_clone = clone $sql_datos_filtro_servicio;
                                                                $datos_filtro_servicio = $sql_datos_filtro_servicio_clone
                                                                    ->where('mo_promotion_service.promotion_id', $promocion_servicio->id)
                                                                    ->first();

                                                                if (!is_null($datos_filtro_servicio->promotion_amount)) {
                                                                    if ($datos_filtro_servicio->promotion_type_id == 1) {
                                                                        $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($datos_filtro_servicio->promotion_amount / 100));
                                                                    }

                                                                    if ($datos_filtro_servicio->promotion_type_id == 2) {
                                                                        if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                            $promotion_amount_total = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $promotion_amount_total = $datos_filtro_servicio->promotion_amount;
                                                                        }

                                                                        $final_sale_price_service -= $promotion_amount_total;
                                                                    }

                                                                    if ($datos_filtro_servicio->promotion_type_id == 3) {
                                                                        if ($datos_filtro_servicio->currency_id != $id_moneda_convertir) {
                                                                            $final_sale_price_service = conversor($datos_filtro_servicio->promotion_amount, $datos_filtro_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                        } else {
                                                                            $final_sale_price_service = $datos_filtro_servicio->promotion_amount;
                                                                        }
                                                                    }

                                                                    $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                                }
                                                            }

                                                            if (!is_null($promocion_servicio->promotion_amount)) {
                                                                if ($promocion_servicio->promotion_type_id == 1) {
                                                                    $final_sale_price_service = $final_sale_price_service - ($final_sale_price_service * ($promocion_servicio->promotion_amount / 100));
                                                                }

                                                                if ($promocion_servicio->promotion_type_id == 2) {
                                                                    if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                        $promotion_amount_total = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                    } else {
                                                                        $promotion_amount_total = $promocion_servicio->promotion_amount;
                                                                    }

                                                                    $final_sale_price_service -= $promotion_amount_total;
                                                                }

                                                                if ($promocion_servicio->promotion_type_id == 3) {
                                                                    if ($promocion_servicio->currency_id != $id_moneda_convertir) {
                                                                        $final_sale_price_service = conversor($promocion_servicio->promotion_amount, $promocion_servicio->currency_id, $id_moneda_convertir, $settings_decimales);
                                                                    } else {
                                                                        $final_sale_price_service = $promocion_servicio->promotion_amount;
                                                                    }
                                                                }

                                                                $final_sale_price_service = number_format($final_sale_price_service, $settings_decimales, '.', '');
                                                            }
                                                        }
                                                    }
                                                }

                                                if ($datos_tarifa_servicio) {
                                                    if($producto->currency_id != $id_moneda_convertir){
                                                        $pickup->price[] = [
                                                            'id' => $producto->price_id,
                                                            'net_price' => conversorCustomSearch($datos_tarifa_servicio->net_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales),
                                                            'sale_price' => $final_sale_price_service,
                                                            'markup' => conversorCustomSearch($datos_tarifa_servicio->markup, $cambio_actual, $cambio_moneda_convertir,$settings_decimales),
                                                            'price_before' => conversorCustomSearch($datos_tarifa_servicio->sale_price, $cambio_actual, $cambio_moneda_convertir,$settings_decimales),
                                                            'currency_id' => $datos_moneda->id,
                                                            'currency_iso_code' => $datos_moneda->iso_code,
                                                        ];
                                                    }else{
                                                        $pickup->price[] = [
                                                            'id' => $producto->price_id,
                                                            'net_price' => number_format($datos_tarifa_servicio->net_price, $settings_decimales, '.', ''),
                                                            'sale_price' => $final_sale_price_service,
                                                            'markup' => number_format($datos_tarifa_servicio->markup, $settings_decimales, '.', ''),
                                                            'price_before' => number_format($datos_tarifa_servicio->sale_price, $settings_decimales, '.', ''),
                                                            'currency_id' => $datos_moneda->id,
                                                            'currency_iso_code' => $datos_moneda->iso_code,
                                                        ];
                                                    }
                                                }

                                                $availability['pickup'][] = $pickup;
                                            }
                                        }
                                        $array_servicio['avail'][] = $availability;
                                    }

                                    if($request->get('ticketportal') != '' && $request->get('ticketportal') == 1 ) {
                                        if(isset($array_servicio['avail']) && count($array_servicio['avail']) > 0 ) {
                                            $array_tipo_cliente['service'][] = $array_servicio;
                                        }
                                    } else {
                                        $array_tipo_cliente['service'][] = $array_servicio;
                                    }

                                    
                                }

                                if($request->get('ticketportal') != '' && $request->get('ticketportal') == 1) {
                                    if(isset($array_tipo_cliente['avail']) && count($array_tipo_cliente['avail']) > 0 ) {
                                        $array_location['client_type'][] = $array_tipo_cliente;
                                    }
                                } else {
                                    $array_location['client_type'][] = $array_tipo_cliente;
                                }
                            }


                            if ($producto->product_type_id == 1) {
                                $array_location['product_package'] = array();

                                $sql_product_package_clone = clone $sql_product_package;

                                $datos_product_package = $sql_product_package_clone->where('mo_product_package.package_id', $producto->id)->get();


                                //consulta para obtener archivos seo para facebook y twitter
                                $sql_images_og_twitter = DB::connection('tenant')->table('mo_file')
                                    ->select('mo_file.id', 'mo_file.url_file');
                                //fin consulta para obtener archivos seo para facebook y twitter

                                foreach ($datos_product_package as $product_package) {
                                    $array_product_package = array();

                                    $array_product_package['id'] = $product_package->id;
                                    $array_product_package['longitude'] = $product_package->longitude;
                                    $array_product_package['latitude'] = $product_package->latitude;
                                    $array_product_package['company_id'] = $product_package->company_id;
                                    $array_product_package['order'] = $product_package->aux_order;
                                    $array_product_package['billable'] = $product_package->billable;
                                    $array_product_package['code'] = $product_package->code;
                                    $array_product_package['height_from'] = $product_package->height_from;
                                    $array_product_package['height_to'] = $product_package->height_to;
                                    $array_product_package['weight_from'] = $product_package->weight_from;
                                    $array_product_package['weight_to'] = $product_package->weight_to;
                                    $array_product_package['salable'] = $product_package->salable;
                                    $array_product_package['app_hotel_service'] = $product_package->app_hotel_service;
                                    $array_product_package['app_hotel_without_reservation'] = $product_package->app_hotel_without_reservation;
                                    $array_product_package['app_hotel_afi_park'] = $product_package->app_hotel_afi_park;
                                    $array_product_package['app_hotel_afi_tour'] = $product_package->app_hotel_afi_tour;
                                    $array_product_package['app_hotel_menu_service'] = $product_package->app_hotel_menu_service;
                                    $array_product_package['app_hotel_with_service'] = $product_package->app_hotel_with_service;
                                    $array_product_package['app_hotel_with_service_direct'] = $product_package->app_hotel_with_service_direct;
                                    $array_product_package['home_app_hotel'] = $product_package->aux_home_app_hotel;


                                    //obtiene urls de archivos para campos seo imagen de facebook y twitter
                                    $sql_images_og_twitter_clone = clone $sql_images_og_twitter;
                                    $datos_images_og_twitter = $sql_images_og_twitter_clone->whereIn('mo_file.id', [$product_package->og_image, $product_package->twitter_image])->get();

                                    $og_image_package = null;
                                    $twitter_image_package = null;
                                    foreach ($datos_images_og_twitter as $image) {
                                        if ($image->id == $product_package->og_image) {
                                            $og_image_package = $storage . $image->url_file;
                                        }
                                        if ($image->id == $product_package->twitter_image) {
                                            $twitter_image_package = $storage . $image->url_file;
                                        }
                                    }
                                    //fin obtiene urls de archivos para campos seo imagen de facebook y twitter

                                    $array_product_package['lang'][][$idioma->abbreviation] = [
                                        'language_id' => $product_package->language_id,
                                        'name' => $product_package->name,
                                        'description' => $product_package->description,
                                        'features' => $product_package->features,
                                        'recommendations' => $product_package->recommendations,
                                        'short_description' => $product_package->short_description,
                                        'friendly_url' => $product_package->friendly_url,
                                        'discover_url' => $product_package->discover_url,
                                        'title_seo' => $product_package->title_seo,
                                        'description_seo' => $product_package->description_seo,
                                        'keywords_seo' => $product_package->keywords_seo,
                                        'legal' => $product_package->legal,

                                        'breadcrumb' => $product_package->breadcrumb,
                                        'rel' => $product_package->rel,
                                        'index' => $product_package->product_index,
                                        'og_title' => $product_package->og_title,
                                        'og_description' => $product_package->og_description,
                                        'og_image' => $og_image_package,
                                        'twitter_title' => $product_package->twitter_title,
                                        'twitter_description' => $product_package->twitter_description,
                                        'twitter_image' => $twitter_image_package,
                                        'canonical_url' => $product_package->canonical_url,
                                        'script_head' => $product_package->script_head,
                                        'script_body' => $product_package->script_body,
                                        'script_footer' => $product_package->script_footer,
                                    ];


                                    $array_product_package['file'] = array();

                                    if ($product_package->view_id != null) {

                                        // Si es el listado: si se envía tipo de archivo se muestran los indicados, si no se muestra uno de tipo imagen
                                        // Si es el detalle: si se envía tipo de archivo se muestran los indicados, si no se muestran todos
                                        $sql_archivos_view_clone = clone $sql_archivos_view;
                                        $sql_archivos_view_clone->where('mo_view_product_file.view_product_id', '=', $product_package->view_id);
                                        if ($id == null && $request->get('code') == null && $request->get('friendly_url') == null) {
                                            if ($request->get('file_types') != '') {
                                                $array_file_types = $request->get('file_types');
                                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                                    $sql_archivos_view_clone->whereIn('mo_file.type_id', $array_file_types);
                                                }
                                            } else {
                                                $sql_archivos_view_clone->where('mo_file.type_id', 3)->limit(1);
                                            }
                                        } else {
                                            if ($request->get('file_types') != '') {
                                                $array_file_types = $request->get('file_types');
                                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                                    $sql_archivos_view_clone->whereIn('mo_file.type_id', $array_file_types);
                                                }
                                            } else {
                                                // No se modifica la consulta
                                            }
                                        }
                                        $datos_archivos = $sql_archivos_view_clone->get();
                                        //


                                    } else {

                                        // Si es el listado: si se envía tipo de archivo se muestran los indicados, si no se muestra uno de tipo imagen
                                        // Si es el detalle: si se envía tipo de archivo se muestran los indicados, si no se muestran todos
                                        $sql_archivos_clone = clone $sql_archivos;
                                        $sql_archivos_clone->where('mo_product_file.product_id', $product_package->id);
                                        if ($id == null && $request->get('code') == null && $request->get('friendly_url') == null) {
                                            if ($request->get('file_types') != '') {
                                                $array_file_types = $request->get('file_types');
                                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                                    $sql_archivos_clone->whereIn('mo_file.type_id', $array_file_types);
                                                }
                                            } else {
                                                $sql_archivos_clone->where('mo_file.type_id', 3)->limit(1);
                                            }
                                        } else {
                                            if ($request->get('file_types') != '') {
                                                $array_file_types = $request->get('file_types');
                                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                                    $sql_archivos_clone->whereIn('mo_file_type.id', $array_file_types);
                                                }
                                            } else {
                                                // No se modifica la consulta
                                            }
                                        }
                                        $datos_archivos = $sql_archivos_clone->get();

                                        //
                                    }

                                    // Files
                                    foreach ($datos_archivos as $archivo) {
                                        $array_traduccion = array();
                                        $array_traduccion[][$idioma->abbreviation] = [
                                            'id' => $archivo->id,
                                            'language_id' => $archivo->language_id,
                                            'name' => $archivo->name,
                                            'alternative_text' => $archivo->alternative_text,
                                        ];
                                        $array_product_package['file'][] = ['id' => $archivo->file_id,
                                            'url_file' => $storage . $archivo->url_file,
                                            'order' => $archivo->order,
                                            'file_type_id' => $archivo->file_type,
                                            'device_id' => $archivo->device_id,
                                            'key_file' => $archivo->key_file,
                                            'lang' => $array_traduccion,
                                        ];
                                    }

    //                              FIN ARCHIVOS

                                    $sql_tags_package_clone = clone $sql_tags;
                                    $datos_tags_package = $sql_tags_package_clone->where('mo_product_tag.product_id', '=', $product_package->id)->get();

                                    foreach ($datos_tags_package as $tag) {
                                        $array_traduccion = array();
                                        $array_traduccion[][$idioma->abbreviation] = [
                                            'language_id' => $tag->language_id,
                                            'name' => $tag->name,
                                            'short_description' => $tag->short_description,
                                        ];
                                        $array_product_package['tag'][] = ['id' => $tag->id, 'lang' => $array_traduccion];
                                    }

                                    $array_location_package = array();

                                    $array_location_package = [
                                        'id' => $product_package->location_id,
                                        'name' => $product_package->location_name,
                                        'depends_on' => $product_package->location_depends_on
                                    ];

                                    $sql_availabilities_package_clone = clone $sql_availabilities;
                                    $sql_availabilities_package_clone = $sql_availabilities_package_clone
                                        ->where('mo_availability.product_id', '=', $product_package->id)
                                        ->where('mo_availability.location_id', '=', $product_package->location_id)
                                        ->where('mo_availability.date', '=', $fecha);


                                    if ($sql_availabilities_package_clone->count() == '0') {
                                        $sql_availabilities_package_clone = clone $sql_availabilities;
                                        $sql_availabilities_package_clone = $sql_availabilities_package_clone
                                            ->where('mo_availability.product_id', '=', $product_package->id)
                                            ->where('mo_availability.location_id', '=', $product_package->location_id)
                                            ->where('mo_availability.date', '=', null);
                                    }

                                    $datos_availabilities = $sql_availabilities_package_clone->orderBy('mo_availability.session')
                                        ->where('mo_availability.closed', '!=', 1)
                                        ->get();


                                    $array_location_package['client_type'] = array();

                                    foreach ($datos_client_type as $tipo_cliente) {

                                        // Datos del tipo de cliente
                                        $array_tipo_cliente = array();
                                        $array_tipo_cliente['id'] = $tipo_cliente->id;
                                        $array_tipo_cliente['code'] = $tipo_cliente->code;
                                        $array_tipo_cliente['from'] = $tipo_cliente->from;
                                        $array_tipo_cliente['to'] = $tipo_cliente->to;

                                        $array_tipo_cliente['lang'][][$idioma->abbreviation] = [
                                            'language_id' => $tipo_cliente->language_id,
                                            'name' => $tipo_cliente->name
                                        ];
                                        // FIN Datos del tipo de cliente

                                        $array_tipo_cliente['avail'] = $datos_availabilities;

                                        $array_location_package['client_type'][] = $array_tipo_cliente;
                                    }

                                    $array_product_package['location'][] = $array_location_package;


                                    $array_location['product_package'][] = $array_product_package;
                                }

                            }
                            if($request->get('ticketportal') != '' && $request->get('ticketportal') == 1 ) {
                                if($request->get('client_types') != '' && $request->get('avail') != '') {
                                    if(count(array_intersect(array_column($array_location['client_type'], 'id'), $request->get('client_types'))) == count($request->get('client_types'))) {
                                        $array_salida_producto['location'][] = $array_location;
                                    }
                                } else {
                                    if(count($array_location['client_type']) > 0) {
                                        $array_salida_producto['location'][] = $array_location;
                                    }
                                }
                                
                            } else {
                                $array_salida_producto['location'][] = $array_location;
                            }
                        }

                        $array_salida_producto['price_from'] = $price_from;

                        $array_salida_producto['currency'] = array();

                        $array_moneda = array();

                        $array_moneda['id'] = $datos_moneda->id;
                        $array_moneda['name'] = $datos_moneda->name;
                        $array_moneda['iso_code'] = $datos_moneda->iso_code;
                        $array_moneda['simbol'] = $datos_moneda->simbol;
                        $array_moneda['presentation_simbol'] = $datos_moneda->presentation_simbol;
                        $array_moneda['position'] = $datos_moneda->position;
                        $array_moneda['decimal_separator'] = $datos_moneda->decimal_separator;
                        $array_moneda['thousand_separator'] = $datos_moneda->thousand_separator;

                        $array_salida_producto['currency'] = $array_moneda;

                        // dd($datos_moneda );

                        if($request->get('ratings') == 1) {
                            $sql_rating_clone = clone $sql_rating;

                            $datos_rating = $sql_rating_clone->where('mo_comment.product_id', $producto->id)
                                ->first();

                            $array_salida_producto['rating'] = [
                                'total_avg' => $datos_rating->avg,
                                'total_comments' => $datos_rating->count
                            ];
                        }

                        $array['data'][0]['product'][] = $array_salida_producto;
                    }

                    $array['total_results'] = $total;
                    //Log que almacena la búsqueda realizada

                    $id_producto = null;
                    if ($id != null && $total > 0) {
                        $id_producto = $id;
                    } elseif ($request->get('code') != null && $total > 0) {
                        $id_producto = $array_salida_producto['id'];
                    } elseif ($request->get('friendly_url') != null && $total > 0) {
                        $id_producto = $array_salida_producto['id'];
                    }


                    $this->createLog($id_producto, $request, $idioma->id, $id_moneda_convertir, $fecha, $datos_count_resultados_directos, $datos_count_resultados_indirectos, $array['total_results'], null, false);
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal server error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');
        }
        return response()->json($array, $array['error']);
    }

    /**
     *
     * Para la consulta por canal de la compra cruzada de un producto se realiza un peticion GET.
     * Como parámetros requeridos recibe subchannel y product
     * Puede recibir un array de ids de productos para que sean excluidos de la búsqueda
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function crosspurchase(Request $request)
    {
        $array['error'] = 200;

        $error = 0;
        $mensaje_validador = collect();

        $langCorrecta = false;
        $countryCorrecto = false;

        $validator = Validator::make($request->all(), [
            'subchannel_id' => 'exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
            'products' => 'array',
            'products.*' => 'integer|exists:tenant.mo_product,id,deleted_at,NULL|integer|min:0|distinct',
            'product_id' => 'required|exists:tenant.mo_product,id,deleted_at,NULL|integer|min:0',
            'date' => 'date|date_format:"Y-m-d"|after_or_equal:today',
            'limit' => 'integer|min:0'
        ]);

        if ($validator->fails()) {
            $error = 1;
            $mensaje_validador = $mensaje_validador->merge($validator->errors());
        }

        if ($error == 1) {
            //Si se produce error en validación se envía mensaje de error
            $array['error'] = 400;
            $array['error_description'] = 'The fields are not the required format';
            $array['error_inputs'][] = $mensaje_validador;
        } else {
            if ($request->get('lang') != '') {

                $idioma = Language::where('abbreviation', Str::upper($request->get('lang')))->first();
                if ($idioma) {
                    $langCorrecta = true;
                    $id_idioma = $idioma->id;
                    $abbreviation = Language::find($id_idioma)->abbreviation;
                } else {
                    $langCorrecta = false;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }

            } else {
                $langCorrecta = true;
                $idioma = Settings::where('name', 'idioma_defecto_id')->first();
                $id_idioma = $idioma->value;
                $idioma = Language::where('id', $id_idioma)->first();
                $abbreviation = Language::find($id_idioma)->abbreviation;
            }

            if ($request->get('country') != '') {
                $country = Country::where('iso3', $request->get('country'))->first();
                if ($country) {
                    $countryCorrecto = true;
                    $id_country = $country->id;
                } else {
                    $countryCorrecto = false;
                    $mensaje_validador = $mensaje_validador->merge(['country' => ['The country is not exists']]);
                }
            } else {
                $countryCorrecto = true;
            }

            if ($langCorrecta && $countryCorrecto) {

                //Precompilada obtener las compras en las que incluye el producto
                $sql_compras = DB::connection('tenant')->table('mo_reservation_detail')
                    ->select('mo_reservation.id')
                    ->join('mo_reservation', 'mo_reservation.id', 'mo_reservation_detail.reservation_id')
                    ->where('mo_reservation_detail.product_id', '=', $request->get('product_id'))
                    ->where('mo_reservation_detail.subchannel_id', '=', $request->get('subchannel_id'))
                    ->where('mo_reservation.deleted_at', '=', null)
                    ->where('mo_reservation_detail.deleted_at', '=', null)
                    ->whereNull('mo_reservation_detail.service_id');


                if ($request->get('country') != '') {
                    $sql_compras->where('mo_reservation.client_country_id', $id_country);
                }


                $sql_productos = DB::connection('tenant')->table('mo_reservation_detail')
                    ->select('mo_product.id')
                    ->join('mo_reservation', 'mo_reservation.id', 'mo_reservation_detail.reservation_id')
                    ->whereNull('mo_reservation.deleted_at')
                    ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
                    ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                    ->where('mo_reservation_detail.product_id', '!=', $request->get('product_id'))
                    ->where('mo_reservation_detail.deleted_at', '=', null)
                    ->where('mo_product.deleted_at', '=', null)
                    ->where(function ($query) {
                        $query->where('mo_reservation_detail.operation_status_id', 3)
                            ->orWhere('mo_reservation_detail.operation_status_id', 4);
                    })
                    ->whereIn('mo_reservation.id', collect(DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_compras->select('mo_reservation.id')->toSql()}) as sub"))
                        ->mergeBindings($sql_compras)->get(['id']))->map(function ($x) {
                        return (array)$x;
                    })->toArray())
                    ->groupBy('mo_product.id');

                $productos = collect(DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_productos->select('mo_product.id')->toSql()}) as sub"))
                    ->mergeBindings($sql_productos)->get(['id']))->map(function ($x) {
                    return (array)$x;
                })->toArray();

                $array_productos = array();
                if ($productos) {
                    $array_productos = $productos;
                }

                $request->request->add(['cross_products' => $array_productos]);

                $respuesta = $this->showSubchannel($id = null, $request);

                if (isset($respuesta->original['data'])) {
                    $array['data'] = $respuesta->original['data'];
                    $array['total_results'] = $respuesta->original['total_results'];
                } else {
                    $array['data'] = array();
                    $array['total_results'] = 0;
                }


            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            }
        }
        return response()->json($array, $array['error']);
    }

    public function addicionalServices(Request $request){

        $array['error'] = 200;

        try{

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::all();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'product_id' => 'required|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->where('front', 1)->first();

            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_additional_service = DB::connection('tenant')->table('mo_additional_service')
                    ->select(
                        'mo_additional_service.id',
                        'mo_additional_service.product_id',
                        'mo_additional_service.price',
                        'mo_additional_service.type_id',
                        'mo_additional_service.longitude',
                        'mo_additional_service.latitude',
                        'mo_additional_service_translation.name',
                        'mo_additional_service_translation.description',
                        'mo_additional_service_type_translation.name AS type_name'
                    )
                    ->whereNull('mo_additional_service.deleted_at')
                    ->where('mo_additional_service.product_id', $request->get('product_id'))
                    ->join('mo_additional_service_translation','mo_additional_service_translation.service_id','mo_additional_service.id')
                    ->whereNull('mo_additional_service_translation.deleted_at')
                    ->where('mo_additional_service_translation.language_id', $idioma->id)
                    ->join('mo_additional_service_type','mo_additional_service_type.id','mo_additional_service.type_id')
                    ->whereNull('mo_additional_service_type.deleted_at')
                    ->join('mo_additional_service_type_translation', 'mo_additional_service_type_translation.type_id','mo_additional_service_type.id')
                    ->whereNull('mo_additional_service_type_translation.deleted_at')
                    ->where('mo_additional_service_type_translation.language_id', $idioma->id)
                    ->groupBy('mo_additional_service.id');

                $sub = DB::connection('tenant')->table('mo_additional_service')
                    ->select(
                        'mo_additional_service.id'
                    )
                    ->whereNull('mo_additional_service.deleted_at')
                    ->where('mo_additional_service.product_id', $request->get('product_id'))
                    ->join('mo_additional_service_translation','mo_additional_service_translation.service_id','mo_additional_service.id')
                    ->whereNull('mo_additional_service_translation.deleted_at')
                    ->where('mo_additional_service_translation.language_id', $idioma->id)
                    ->join('mo_additional_service_type','mo_additional_service_type.id','mo_additional_service.type_id')
                    ->whereNull('mo_additional_service_type.deleted_at')
                    ->join('mo_additional_service_type_translation', 'mo_additional_service_type_translation.type_id','mo_additional_service_type.id')
                    ->whereNull('mo_additional_service_type_translation.deleted_at')
                    ->where('mo_additional_service_type_translation.language_id', $idioma->id)
                    ->groupBy('mo_additional_service.id');

                $sql_files_service = DB::connection('tenant')->table('mo_file')
                    ->select(
                        'mo_file.id',
                        'mo_file.file_name',
                        'mo_file.file_size',
                        'mo_file.file_dimensions',
                        'mo_file.file_dimensions',
                        'mo_file.mimetype',
                        'mo_file.type_id',
                        'mo_file.order',
                        'mo_file.url_file',
                        'mo_file.device_id',
                        'mo_file.key_file'
                    )
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_additional_service_file','mo_additional_service_file.file_id','mo_file.id')
                    ->whereNull('mo_additional_service_file.deleted_at')
                    ->join('mo_file_translation','mo_file_translation.file_id','mo_file.id')
                    ->whereNull('mo_file_translation.deleted_at')
                    ->where('mo_file_translation.language_id',$idioma->id)
                    ->groupBy('mo_file.id');

                $storage = Settings::where('name', 'storage_files')->first()->value;

                if($request->get('name') != '') {

                    $sql_additional_service->where(function ($query) use ($request) {
                        $query->where('mo_additional_service_translation.name','Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_additional_service_translation.description', 'Like', '%' . $request->get('name') . '%');

                    });

                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_additional_service_translation.name','Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_additional_service_translation.description', 'Like', '%' . $request->get('name') . '%');

                    });
                }

                // Order
                $orden = 'mo_additional_service.order';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_additional_service_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_additional_service_translation.description';
                            break;
                        case 'type':
                            $orden = 'mo_additional_service.type_id';
                            break;
                        case 'order':
                            $orden = 'mo_additional_service.order';
                            break;
                        case 'id':
                            $orden = 'mo_additional_service.id';
                            break;
                        default:
                            $orden = 'mo_additional_service_translation.name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'desc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_additional_service->orderBy($orden, $sentido);

                $servicios_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $total = $servicios_count->count();

                $datos_services = $sql_additional_service->get();

                foreach ($datos_services as $service) {

                    $new_service = array();

                    $new_service['id'] = $service->id;

                    $new_service['price'] = $service->price;

                    $new_service['longitude'] = $service->longitude;
                    $new_service['latitude'] = $service->latitude;

                    $new_service['lang'] = array();

                    $new_service['lang'][][$idioma->abbreviation] = [
                        'language_id' => $idioma->id,
                        'name' => $service->name,
                        'description' => $service->description,
                    ];

                    $new_service['type'][] = [
                        'id' => $service->type_id,
                        'lang' => [
                            [
                                $idioma->abbreviation => [
                                'language_id' => $idioma->id,
                                'name' => $service->type_name
                                ]
                            ]
                        ]
                    ];

                    $sql_files_service_clone = clone $sql_files_service;

                    $datos_files = $sql_files_service_clone
                        ->where('mo_additional_service_file.service_id', $service->id)
                        ->get();

                    $new_service['file'] = array();

                    foreach ($datos_files as $file) {
                        $tipo = FileType::where('id', $file->type_id)->first();

                        $array_file = array();

                        $array_file['id'] = $file->id;

                        $array_file['file_name'] = $file->file_name;

                        $array_file['file_size'] = convertExtension($file->file_size);
                        $array_file['file_dimensions'] = $file->file_dimensions;
                        $array_file['mimetype'] = $file->mimetype;
                        $array_file['order'] = $file->order;
                        $array_file['url_file'] = $storage . $file->url_file;
                        $array_file['device_id'] = $file->device_id;
                        $array_file['key_file'] = $file->key_file;
                        $array_file['type'] = ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null;

                        if ($tipo) {
                            $traduccion_tipo_file = FileTypeTranslation::where('type_id', $file->type_id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idioma->id)
                                ->first();

                            $array_file['type']['lang'][][$idioma->abbreviation] = $traduccion_tipo_file;
                        }

                        $traducciones_file = FileTranslation::where('file_id', $file->id)
                            ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                            ->where('language_id', '=', $idioma->id)
                            ->first();

                        $array_file['lang'][][$idioma->abbreviation] = $traducciones_file;

                        $new_service['file'][] = $array_file;

                    }

                    $array['data'][0]['services'][] = $new_service;
                }

                $array['total_results'] = $total;
            }

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal server error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que devuelve las promociones visibles
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPromotions(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
                'emulated_subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'device' => 'exists:tenant.mo_device,code,deleted_at,NULL'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->first();

            }

            //Obtención moneda a la que se quiere convertir
            if ($request->get('currency') != '') {

                $currency = Currency::where('iso_code', Str::upper($request->get('currency')))->first();
                if (!$currency) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['currency' => ['The currency is not exists']]);
                } else {
                    $id_moneda_convertir = $currency->id;
                    $datos_moneda = $currency;
                }
            } else {
                $moneda_settings = Settings::where('name', 'moneda_defecto_id')->first();
                $id_moneda_convertir = $moneda_settings->value;
                $datos_moneda = Currency::find($id_moneda_convertir);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $fecha = Carbon::now();

                $fecha_actual = $fecha->format('Y-m-d H:i:s');

                $week_day = $fecha->format('w');

                $settings_decimales = Settings::where('name', 'numero_decimales')->first()->value;

                $dia_semana = $week_day;
                if ($week_day == 0) {
                    $dia_semana = 7;
                }

                $sql_promotions = DB::connection('tenant')->table('mo_promotion')
                    ->select(
                        'mo_promotion.id',
                        'mo_promotion.code',
                        'mo_promotion.order',
                        'mo_promotion.visibility',
                        'mo_promotion.accumulate',
                        'mo_promotion.min_quantity',
                        'mo_promotion.max_quantity',
                        'mo_promotion.promotion_amount',
                        'mo_promotion.currency_id',
                        'mo_promotion.promotion_type_id',
                        'mo_promotion.campaign_id',
                        'mo_promotion.department_id',
                        'mo_promotion.cost_center_id',
                        'mo_promotion.apply_payment_method_filter',
                        'mo_promotion.apply_language_filter',
                        'mo_promotion.apply_currency_filter',
                        'mo_promotion.apply_pickup_filter',
                        'mo_promotion.apply_product_filter',
                        'mo_promotion.apply_product_type_filter',
                        'mo_promotion.apply_service_filter',
                        'mo_promotion.apply_subchannel_filter',
                        'mo_promotion.apply_client_type_filter',
                        'mo_promotion.apply_country_filter',
                        'mo_promotion.apply_state_filter',
                        'mo_promotion.apply_device_filter',
                        'mo_promotion.lockers_validation',
                        'mo_promotion.supervisor_validation',
                        'mo_promotion.allow_date_change',
                        'mo_promotion.allow_upgrade',
                        'mo_promotion.allow_product_change',
                        'mo_promotion.pay_difference',
                        'mo_promotion.anticipation_start',
                        'mo_promotion.anticipation_end',
                        'mo_promotion.coupon_code',
                        'mo_promotion.coupon_type',
                        'mo_promotion.coupon_sheet_start',
                        'mo_promotion.coupon_sheet_end',
                        'mo_promotion.coupon_total_uses',
                        'mo_promotion.benefit_card',
                        'mo_promotion.benefit_card_total_uses',
                        'mo_promotion.benefit_card_id',
                        'mo_promotion.user_id',
                        'mo_promotion_translation.name',
                        'mo_promotion_translation.description',
                        'mo_promotion_translation.short_description',
                        'mo_promotion_translation.friendly_url',
                        'mo_promotion_translation.external_url',
                        'mo_promotion_translation.title_seo',
                        'mo_promotion_translation.description_seo',
                        'mo_promotion_translation.keywords_seo',
                        'mo_promotion_translation.observations',
                        'mo_promotion_translation.legal',
                        'mo_promotion_translation.notes',
                        'mo_promotion_translation.gift',
                        'mo_promotion_type_translation.name as type_name',
                        'mo_campaign_translation.name as campaign_name'
                    )
                    ->whereNull('mo_promotion.deleted_at')
                    ->join('mo_promotion_type', 'mo_promotion_type.id', 'mo_promotion.promotion_type_id')
                    ->whereNull('mo_promotion_type.deleted_at')
                    ->join('mo_promotion_type_translation', 'mo_promotion_type_translation.promotion_type_id', 'mo_promotion_type.id')
                    ->whereNull('mo_promotion_type_translation.deleted_at')
                    ->join('mo_promotion_translation', 'mo_promotion_translation.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_translation.deleted_at')
                    ->where('mo_promotion_translation.language_id', $idioma->id)
                    ->join('mo_campaign', 'mo_campaign.id', 'mo_promotion.campaign_id')
                    ->whereNull('mo_campaign.deleted_at')
                    ->where('mo_campaign.date_start', '<=', $fecha->format('Y-m-d'))
                    ->where('mo_campaign.date_end', '>=', $fecha->format('Y-m-d'))
                    ->join('mo_campaign_translation', 'mo_campaign_translation.campaign_id', 'mo_campaign.id')
                    ->whereNull('mo_campaign_translation.deleted_at')
                    ->where('mo_campaign_translation.language_id', $idioma->id)
                    ->where('mo_promotion.visibility', 1)
                    ->join('mo_promotion_range_sale_day', 'mo_promotion_range_sale_day.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_range_sale_day.deleted_at')
                    ->where('mo_promotion_range_sale_day.sale_date_start', '<=', $fecha_actual)
                    ->where('mo_promotion_range_sale_day.sale_date_end', '>=', $fecha_actual)
                    ->join('mo_promotion_sale_day', 'mo_promotion_sale_day.promotion_range_sale_day_id', 'mo_promotion_range_sale_day.id')
                    ->whereNull('mo_promotion_sale_day.deleted_at')
                    ->where('mo_promotion_sale_day.day_id', $dia_semana)
                    ->leftJoin('mo_promotion_client_type', function ($join) {
                        $join->on('mo_promotion_client_type.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_client_type.deleted_at');
                    })->leftJoin('mo_promotion_country', function ($join) {
                        $join->on('mo_promotion_country.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_country.deleted_at');
                    })->leftJoin('mo_promotion_currency', function ($join) {
                        $join->on('mo_promotion_currency.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_currency.deleted_at');
                    })->leftJoin('mo_promotion_device', function ($join) {
                        $join->on('mo_promotion_device.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_device.deleted_at');
                    })->leftJoin('mo_promotion_language', function ($join) {
                        $join->on('mo_promotion_language.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_language.deleted_at');
                    })->leftJoin('mo_promotion_payment_method', function ($join) {
                        $join->on('mo_promotion_payment_method.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_payment_method.deleted_at');
                    })->leftJoin('mo_promotion_pickup', function ($join) {
                        $join->on('mo_promotion_pickup.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_pickup.deleted_at');
                    })->leftJoin('mo_promotion_product', function ($join) {
                        $join->on('mo_promotion_product.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product.deleted_at');
                    })->leftJoin('mo_promotion_product_type', function ($join) {
                        $join->on('mo_promotion_product_type.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product_type.deleted_at');
                    })->leftJoin('mo_promotion_range_enjoy_day', function ($join) {
                        $join->on('mo_promotion_range_enjoy_day.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_enjoy_day.deleted_at');
                    })->leftJoin('mo_promotion_service', function ($join) {
                        $join->on('mo_promotion_service.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_service.deleted_at');
                    })->leftJoin('mo_promotion_state', function ($join) {
                        $join->on('mo_promotion_state.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_state.deleted_at');
                    })->leftJoin('mo_promotion_subchannel', function ($join) {
                        $join->on('mo_promotion_subchannel.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_subchannel.deleted_at');
                    })->where(function ($query) use ($idioma) {
                        $query->where('mo_promotion.apply_language_filter', 0)
                            ->orWhere(function ($query) use ($idioma) {
                                $query->where('mo_promotion.apply_language_filter', 1)
                                    ->where('mo_promotion_language.language_id', $idioma->id);
                            });
                    })
                    ->groupBy('mo_promotion.id');

                $sub = DB::connection('tenant')->table('mo_promotion')
                    ->select(
                        'mo_promotion.id'
                    )
                    ->whereNull('mo_promotion.deleted_at')
                    ->join('mo_promotion_type', 'mo_promotion_type.id', 'mo_promotion.promotion_type_id')
                    ->whereNull('mo_promotion_type.deleted_at')
                    ->join('mo_promotion_type_translation', 'mo_promotion_type_translation.promotion_type_id', 'mo_promotion_type.id')
                    ->whereNull('mo_promotion_type_translation.deleted_at')
                    ->join('mo_promotion_translation', 'mo_promotion_translation.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_translation.deleted_at')
                    ->where('mo_promotion_translation.language_id', $idioma->id)
                    ->join('mo_campaign', 'mo_campaign.id', 'mo_promotion.campaign_id')
                    ->whereNull('mo_campaign.deleted_at')
                    ->where('mo_campaign.date_start', '<=', $fecha->format('Y-m-d'))
                    ->where('mo_campaign.date_end', '>=', $fecha->format('Y-m-d'))
                    ->join('mo_campaign_translation', 'mo_campaign_translation.campaign_id', 'mo_campaign.id')
                    ->whereNull('mo_campaign_translation.deleted_at')
                    ->where('mo_campaign_translation.language_id', $idioma->id)
                    ->where('mo_promotion.visibility', 1)
                    ->join('mo_promotion_range_sale_day', 'mo_promotion_range_sale_day.promotion_id', 'mo_promotion.id')
                    ->whereNull('mo_promotion_range_sale_day.deleted_at')
                    ->where('mo_promotion_range_sale_day.sale_date_start', '<=', $fecha_actual)
                    ->where('mo_promotion_range_sale_day.sale_date_end', '>=', $fecha_actual)
                    ->join('mo_promotion_sale_day', 'mo_promotion_sale_day.promotion_range_sale_day_id', 'mo_promotion_range_sale_day.id')
                    ->whereNull('mo_promotion_sale_day.deleted_at')
                    ->where('mo_promotion_sale_day.day_id', $dia_semana)
                    ->leftJoin('mo_promotion_client_type', function ($join) {
                        $join->on('mo_promotion_client_type.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_client_type.deleted_at');
                    })->leftJoin('mo_promotion_country', function ($join) {
                        $join->on('mo_promotion_country.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_country.deleted_at');
                    })->leftJoin('mo_promotion_currency', function ($join) {
                        $join->on('mo_promotion_currency.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_currency.deleted_at');
                    })->leftJoin('mo_promotion_device', function ($join) {
                        $join->on('mo_promotion_device.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_device.deleted_at');
                    })->leftJoin('mo_promotion_language', function ($join) {
                        $join->on('mo_promotion_language.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_language.deleted_at');
                    })->leftJoin('mo_promotion_payment_method', function ($join) {
                        $join->on('mo_promotion_payment_method.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_payment_method.deleted_at');
                    })->leftJoin('mo_promotion_pickup', function ($join) {
                        $join->on('mo_promotion_pickup.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_pickup.deleted_at');
                    })->leftJoin('mo_promotion_product', function ($join) {
                        $join->on('mo_promotion_product.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product.deleted_at');
                    })->leftJoin('mo_promotion_product_type', function ($join) {
                        $join->on('mo_promotion_product_type.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product_type.deleted_at');
                    })->leftJoin('mo_promotion_range_enjoy_day', function ($join) {
                        $join->on('mo_promotion_range_enjoy_day.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_enjoy_day.deleted_at');
                    })->leftJoin('mo_promotion_service', function ($join) {
                        $join->on('mo_promotion_service.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_service.deleted_at');
                    })->leftJoin('mo_promotion_state', function ($join) {
                        $join->on('mo_promotion_state.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_state.deleted_at');
                    })->leftJoin('mo_promotion_subchannel', function ($join) {
                        $join->on('mo_promotion_subchannel.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_subchannel.deleted_at');
                    })
                    ->where(function ($query) use ($idioma) {
                        $query->where('mo_promotion.apply_language_filter', 0)
                            ->orWhere(function ($query) use ($idioma) {
                                $query->where('mo_promotion.apply_language_filter', 1)
                                    ->where('mo_promotion_language.language_id', $idioma->id);
                            });
                    })
                    ->groupBy('mo_promotion.id');

                $sql_archivos = DB::connection('tenant')->table('mo_promotion_file')
                    ->select(
                        'mo_file.id as file_id',
                        'mo_file.file_name',
                        'mo_file.file_size',
                        'mo_file_translation.id',
                        'mo_file_translation.name',
                        'mo_file_translation.alternative_text',
                        'mo_file_translation.language_id',
                        'mo_file.file_dimensions',
                        'mo_file.mimetype',
                        'mo_file.type_id',
                        'mo_file.order',
                        'mo_file.url_file',
                        'mo_file.device_id',
                        'mo_file.key_file',
                        'mo_file_type.id as file_type')
                    ->whereNull('mo_promotion_file.deleted_at')
                    ->join('mo_file', 'mo_file.id', '=', 'mo_promotion_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->where('mo_file_translation.language_id', $idioma->id)
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->where('mo_file_type.deleted_at', null)
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id')
                            ->whereNull('mo_file_type_translation.deleted_at');
                    })
                    ->groupBy('mo_file.id')
                    ->orderBy('mo_file.order', 'desc');

                if ($request->get('client_type_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_client_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_client_type_filter', 1)
                                    ->where('mo_promotion_client_type.client_type_id', $request->get('client_type_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_client_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_client_type_filter', 1)
                                    ->where('mo_promotion_client_type.client_type_id', $request->get('client_type_id'));
                            });
                    });
                }

                if ($request->get('payment_method_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_payment_method_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_payment_method_filter', 1)
                                    ->where('mo_promotion_payment_method.payment_method_id', $request->get('payment_method_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_payment_method_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_payment_method_filter', 1)
                                    ->where('mo_promotion_payment_method.payment_method_id', $request->get('payment_method_id'));
                            });
                    });
                }

                if ($request->get('pickup_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_pickup_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_pickup_filter', 1)
                                    ->where('mo_promotion_pickup.pickup_id', $request->get('pickup_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_pickup_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_pickup_filter', 1)
                                    ->where('mo_promotion_pickup.pickup_id', $request->get('pickup_id'));
                            });
                    });
                }

                if ($request->get('product_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_product_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_filter', 1)
                                    ->where('mo_promotion_product.product_id', $request->get('product_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_product_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_filter', 1)
                                    ->where('mo_promotion_product.product_id', $request->get('product_id'));
                            });
                    });
                }

                if ($request->get('product_type_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_product_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_type_filter', 1)
                                    ->where('mo_promotion_product_type.product_type_id', $request->get('product_type_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_product_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_type_filter', 1)
                                    ->where('mo_promotion_product_type.product_type_id', $request->get('product_type_id'));
                            });
                    });
                }

                if ($request->get('service_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_service_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_service_filter', 1)
                                    ->where('mo_promotion_service.service_id', $request->get('service_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_service_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_service_filter', 1)
                                    ->where('mo_promotion_service.service_id', $request->get('service_id'));
                            });
                    });
                }

                if ($request->get('subchannel_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_subchannel_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_subchannel_filter', 1)
                                    ->where('mo_promotion_subchannel.subchannel_id', $request->get('subchannel_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_subchannel_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_subchannel_filter', 1)
                                    ->where('mo_promotion_subchannel.subchannel_id', $request->get('subchannel_id'));
                            });
                    });
                }

                if ($request->get('state_id') != '') {
                    $sql_promotions->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_state_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_state_filter', 1)
                                    ->where('mo_promotion_state.state_id', $request->get('state_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_promotion.apply_state_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_state_filter', 1)
                                    ->where('mo_promotion_state.state_id', $request->get('state_id'));
                            });
                    });
                }

                if ($request->get('device') != '') {

                    $promotion_device = Device::where('code', Str::upper($request->get('device')))->where('active', 1)->first();

                    $sql_promotions->where(function ($query) use ($promotion_device) {
                        $query->where('mo_promotion.apply_device_filter', 0)
                            ->orWhere(function ($query) use ($promotion_device) {
                                $query->where('mo_promotion.apply_device_filter', 1)
                                    ->where('mo_promotion_device.device_id', $promotion_device->id);
                            });
                    });
                    $sub->where(function ($query) use ($promotion_device) {
                        $query->where('mo_promotion.apply_device_filter', 0)
                            ->orWhere(function ($query) use ($promotion_device) {
                                $query->where('mo_promotion.apply_device_filter', 1)
                                    ->where('mo_promotion_device.device_id', $promotion_device->id);
                            });
                    });
                }

                if ($request->get('lockers_validation') != '') {
                    $sql_promotions->where('mo_promotion.lockers_validation', $request->get('lockers_validation'));
                    $sub->where('mo_promotion.lockers_validation', $request->get('lockers_validation'));
                }

                if ($request->get('supervisor_validation') != '') {
                    $sql_promotions->where('mo_promotion.supervisor_validation', $request->get('supervisor_validation'));
                    $sub->where('mo_promotion.supervisor_validation', $request->get('supervisor_validation'));
                }

                if ($request->get('allow_date_change') != '') {
                    $sql_promotions->where('mo_promotion.allow_date_change', $request->get('allow_date_change'));
                    $sub->where('mo_promotion.allow_date_change', $request->get('allow_date_change'));
                }

                if ($request->get('allow_date_change') != '') {
                    $sql_promotions->where('mo_promotion.allow_date_change', $request->get('allow_date_change'));
                    $sub->where('mo_promotion.allow_date_change', $request->get('allow_date_change'));
                }

                if ($request->get('allow_upgrade') != '') {
                    $sql_promotions->where('mo_promotion.allow_upgrade', $request->get('allow_upgrade'));
                    $sub->where('mo_promotion.allow_upgrade', $request->get('allow_upgrade'));
                }

                if ($request->get('allow_product_change') != '') {
                    $sql_promotions->where('mo_promotion.allow_product_change', $request->get('allow_product_change'));
                    $sub->where('mo_promotion.allow_product_change', $request->get('allow_product_change'));
                }

                if ($request->get('pay_difference') != '') {
                    $sql_promotions->where('mo_promotion.pay_difference', $request->get('pay_difference'));
                    $sub->where('mo_promotion.pay_difference', $request->get('pay_difference'));
                }

                if ($request->get('coupon_code') != '') {
                    $sql_promotions->where('mo_promotion.coupon_code', $request->get('coupon_code'));
                    $sub->where('mo_promotion.coupon_code', $request->get('coupon_code'));
                }

                if ($request->get('coupon_type') != '') {
                    $sql_promotions->where('mo_promotion.coupon_type', $request->get('coupon_type'));
                    $sub->where('mo_promotion.coupon_type', $request->get('coupon_type'));
                }


                //Establece la ordenación de los resultados
                $orden = 'mo_promotion.order';
                switch ($request->get('order')) {
                    case 'order':
                        $orden = 'mo_promotion.order';
                        break;
                    case 'id':
                        $orden = 'mo_promotion.id';
                        break;
                    case 'min_quantity':
                        $orden = 'mo_promotion.min_quantity';
                        break;
                    case 'max_quantity':
                        $orden = 'mo_promotion.max_quantity';
                        break;
                    case 'amount':
                        $orden = 'mo_promotion.amount';
                        break;
                    case 'currency':
                        $orden = 'mo_promotion.currency_id';
                        break;
                    case 'type':
                        $orden = 'mo_promotion.promotion_type_id';
                        break;
                    case 'campaign':
                        $orden = 'mo_promotion.campaign_id';
                        break;
                    case 'benefit_card':
                        $orden = 'mo_promotion.benefit_card_id';
                        break;
                    default:
                        $orden = 'mo_promotion.order';
                        break;
                }
                //Establece el sentido de ordenación
                $orden_way = 'DESC';
                switch ($request->get('order_way')) {
                    case 'asc':
                        $orden_way = 'ASC';
                        break;
                    case 'desc':
                        $orden_way = 'DESC';
                        break;
                    default:
                        $orden_way = 'DESC';
                        break;
                }

                $sql_promotions->orderBy($orden, $orden_way)->groupBy('mo_promotion.id');

                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_resultados_promotions = $sql_promotions->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {
                    $datos_resultados_promotions = $sql_promotions->get();
                }

                $promotions_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $promotions_count->count();


                $storage = Settings::where('name', '=', 'storage_files')->first()->value;

                $array['data'] = array();


                foreach ($datos_resultados_promotions as $promotion) {

                    $array_salida_promocion = array();

                    $array_salida_promocion['id'] = $promotion->id;
                    $array_salida_promocion['code'] = $promotion->code;
                    $array_salida_promocion['order'] = $promotion->order;
                    $array_salida_promocion['visibility'] = $promotion->visibility;
                    $array_salida_promocion['accumulate'] = $promotion->accumulate;
                    $array_salida_promocion['min_quantity'] = $promotion->min_quantity;
                    $array_salida_promocion['max_quantity'] = $promotion->max_quantity;

                    if ($promotion->promotion_type_id == 2 || $promotion->promotion_type_id == 3) {
                        $array_salida_promocion['promotion_amount'] = conversor($promotion->promotion_amount, $promotion->currency_id, $id_moneda_convertir, $settings_decimales);
                    } else {
                        $array_salida_promocion['promotion_amount'] = $promotion->promotion_amount;
                    }

                    $array_salida_promocion['currency'] = array();

                    if (!is_null($promotion->currency_id)) {

                        $datos_currency = Currency::where('id', $id_moneda_convertir)->first();

                        $array_salida_promocion['currency'][] = [
                            'id' => $datos_currency->id,
                            'name' => $datos_currency->name,
                            'iso_code' => $datos_currency->iso_code,
                            'presentation_simbol' => $datos_currency->presentation_simbol,
                            'position' => $datos_currency->position,
                            'position' => $datos_currency->position,
                        ];
                    }

                    $array_salida_promocion['promotion_type'] = array();
                    $array_salida_promocion['promotion_type'][] = [
                        'id' => $promotion->promotion_type_id,
                        'lang' => [
                            [
                                $idioma->abbreviation => [
                                    'name' => $promotion->type_name,
                                ],
                            ],
                        ],
                    ];


                    $array_salida_promocion['campaign'] = array();

                    $array_salida_promocion['campaign'] = [
                        'id' => $promotion->campaign_id,
                        'lang' => [
                            [
                                $idioma->abbreviation => [
                                    'name' => $promotion->campaign_name
                                ],
                            ],
                        ],
                    ];


                    $array_salida_promocion['department_id'] = $promotion->department_id;
                    $array_salida_promocion['cost_center_id'] = $promotion->cost_center_id;
                    $array_salida_promocion['apply_payment_method_filter'] = $promotion->apply_payment_method_filter;
                    $array_salida_promocion['apply_language_filter'] = $promotion->apply_language_filter;
                    $array_salida_promocion['apply_currency_filter'] = $promotion->apply_currency_filter;
                    $array_salida_promocion['apply_pickup_filter'] = $promotion->apply_pickup_filter;
                    $array_salida_promocion['apply_product_filter'] = $promotion->apply_product_filter;
                    $array_salida_promocion['apply_product_type_filter'] = $promotion->apply_product_type_filter;
                    $array_salida_promocion['apply_service_filter'] = $promotion->apply_service_filter;
                    $array_salida_promocion['apply_subchannel_filter'] = $promotion->apply_subchannel_filter;
                    $array_salida_promocion['apply_client_type_filter'] = $promotion->apply_client_type_filter;
                    $array_salida_promocion['apply_country_filter'] = $promotion->apply_country_filter;
                    $array_salida_promocion['apply_state_filter'] = $promotion->apply_state_filter;
                    $array_salida_promocion['apply_device_filter'] = $promotion->apply_device_filter;
                    $array_salida_promocion['lockers_validation'] = $promotion->lockers_validation;
                    $array_salida_promocion['supervisor_validation'] = $promotion->supervisor_validation;
                    $array_salida_promocion['allow_date_change'] = $promotion->allow_date_change;
                    $array_salida_promocion['allow_upgrade'] = $promotion->allow_upgrade;
                    $array_salida_promocion['allow_product_change'] = $promotion->allow_product_change;
                    $array_salida_promocion['pay_difference'] = $promotion->pay_difference;
                    $array_salida_promocion['anticipation_start'] = $promotion->anticipation_start;
                    $array_salida_promocion['anticipation_end'] = $promotion->anticipation_end;
                    $array_salida_promocion['coupon_code'] = $promotion->coupon_code;
                    $array_salida_promocion['coupon_type'] = $promotion->coupon_type;
                    $array_salida_promocion['coupon_sheet_start'] = $promotion->coupon_sheet_start;
                    $array_salida_promocion['coupon_sheet_end'] = $promotion->coupon_sheet_end;
                    $array_salida_promocion['coupon_total_uses'] = $promotion->coupon_total_uses;
                    $array_salida_promocion['benefit_card'] = $promotion->benefit_card;
                    $array_salida_promocion['benefit_card_total_uses'] = $promotion->benefit_card_total_uses;
                    $array_salida_promocion['benefit_card_id'] = $promotion->benefit_card_id;
                    $array_salida_promocion['user_id'] = $promotion->user_id;

                    $array_salida_promocion['lang'] = array();
                    $array_salida_promocion['lang'][][$idioma->abbreviation] = [
                        'name' => $promotion->name,
                        'description' => $promotion->description,
                        'short_description' => $promotion->short_description,
                        'friendly_url' => $promotion->friendly_url,
                        'external_url' => $promotion->external_url,
                        'title_seo' => $promotion->title_seo,
                        'description_seo' => $promotion->description_seo,
                        'keywords_seo' => $promotion->keywords_seo,
                        'observations' => $promotion->observations,
                        'legal' => $promotion->legal,
                        'notes' => $promotion->notes,
                        'gift' => $promotion->gift,
                    ];

                    $array_salida_promocion['file'] = array();

                    $sql_archivos_clone = clone $sql_archivos;

                    $sql_archivos_clone->where('mo_promotion_file.promotion_id', $promotion->id);
                    if ($request->get('file_types') != '') {
                        $array_file_types = $request->get('file_types');
                        if (isset($array_file_types) && isset($array_file_types) != '') {
                            $sql_archivos_clone->whereIn('mo_file.type_id', $array_file_types);
                        }
                    } else {
                        $sql_archivos_clone->where('mo_file.type_id', 3)->limit(1);
                    }

                    $datos_archivos = $sql_archivos_clone->get();

                    foreach ($datos_archivos as $archivo) {
                        $array_traduccion = array();
                        $array_traduccion[][$idioma->abbreviation] = [
                            'id' => $archivo->id,
                            'language_id' => $archivo->language_id,
                            'name' => $archivo->name,
                            'alternative_text' => $archivo->alternative_text,
                        ];
                        $array_salida_promocion['file'][] = ['id' => $archivo->file_id,
                            'url_file' => $storage . $archivo->url_file,
                            'order' => $archivo->order,
                            'device_id' => $archivo->device_id,
                            'key_file' => $archivo->key_file,
                            'file_type_id' => $archivo->file_type,
                            'lang' => $array_traduccion,
                        ];
                    }


                    $array['data'][0]['promotion'][] = $array_salida_promocion;

                }


                $array['total_results'] = $totales;

            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal server error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');

        }

        return response()->json($array, $array['error']);
    }

    /**
     * Para la consulta de las habitaciones se realiza una petición GET. Devuelve las habitaciones disponibles, priorizando la información disponible para el subcanal.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function ShowRoom($id = null, Request $request){
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
                'emulated_subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'hotel_id' => 'required|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
                'category_id' => 'exists:tenant.mo_hotel_room_category,id,deleted_at,NULL|integer|min:0',
                'info_only' => 'boolean',
                'number_room' => 'required_unless:info_only,1|integer|min:0',
                'date_start' => 'required_unless:info_only,1|date_format:Y-m-d|after_or_equal:today',
                'date_end' => 'required_unless:info_only,1|date_format:Y-m-d|after:date_start',
                'adult' => 'required_unless:info_only,1|integer|min:1',
                'child' => 'integer|min:0',
                'ages' => 'array',
                'ages.*' => 'required|array',
                'ages.*.age' => 'required|array',
                'ages.*.age.*' => 'required|integer|min:0',
                'page' => 'integer',
                'limit' => 'integer|min:0',
                'file_types' => 'array',
                'file_types.*' => 'distinct|exists:tenant.mo_file_type,id,deleted_at,NULL|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if($request->get('ages') != '') {
                if($request->get('child') != '') {
                    if(isset($request->get('ages')['child']['age'])){
                        if(count($request->get('ages')['child']['age']) > $request->get('child')) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['ages.child.age' => ['The number of ages must be equal to or less than the number of child']]);
                        }
                    }

                    $child = ClientType::where('id', 2)->first();

                    if($child && $child->hotel_age_required == 1) {
                        if(!isset($request->get('ages')['child']['age'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['ages.child.age' => ['The ages.child.age is required']]);
                        } else {
                            if(count($request->get('ages')['child']['age']) != $request->get('child')) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['ages.child.age' => ['The number of ages must be equal to the number of child']]);
                            }
                        }
                    }
                }

                if($request->get('adult') != '') {
                    if(isset($request->get('ages')['adult']['age'])){
                        if(count($request->get('ages')['adult']['age']) > $request->get('adult')) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['ages.adult.age' => ['The number of ages must be equal to or less than the number of adult']]);
                        }
                    }

                    $adultos = ClientType::where('id', 1)->first();

                    if($adultos && $adultos->hotel_age_required == 1) {
                        if(!isset($request->get('ages')['adult']['age'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['ages.adult.age' => ['The ages.adult.age is required']]);
                        } else {
                            if(count($request->get('ages')['adult']['age']) != $request->get('adult')) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['ages.adult.age' => ['The number of ages must be equal to the number of adults']]);
                            }
                        }
                    }
                }
            } else {
                if($request->get('child') != '') {
                    if(!isset($request->get('ages')['child']['age'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['ages.child.age' => ['The ages.child.age is required']]);
                    } else {
                        if(count($request->get('ages')['child']['age']) != $request->get('child')) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['ages.child.age' => ['The number of ages must be equal to the number of child']]);
                        }
                    }
                }
                
                if($request->get('adult') != '') {
                    $adultos = ClientType::where('id', 1)->first();

                    if($adultos && $adultos->hotel_age_required == 1) {
                        if(!isset($request->get('ages')['adult']['age'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['ages.adult.age' => ['The ages.adult.age is required']]);
                        } else {
                            if(count($request->get('ages')['adult']['age']) != $request->get('adult')) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['ages.adult.age' => ['The number of ages must be equal to the number of adults']]);
                            }
                        }
                    }
                }
            }

            $subchannel_id = '';
            $emulated_subchannel_id = '';

            // Si la petición es pública a través de un canal de venta
            if (strpos($request->path(), 'search/room/pos') === false) {

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS

                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->first();

            }

            if ($request->get('currency') != '') {

                $currency = Currency::where('iso_code', Str::upper($request->get('currency')))->first();
                if (!$currency) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['currency' => ['The currency is not exists']]);
                } else {
                    $id_moneda_convertir = $currency->id;
                    $datos_moneda = $currency;
                }
            } else {
                $moneda_settings = Settings::where('name', 'moneda_defecto_id')->first();
                $id_moneda_convertir = $moneda_settings->value;
                $datos_moneda = Currency::find($id_moneda_convertir);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $settings_decimales = Settings::where('name', 'numero_decimales')->first()->value;

                //Asignación de parámetros a variables para poder realizar la petición
                $rooms = array();
                $rooms['subchannel_id'] = $emulated_subchannel_id;
                $rooms['hotel_id'] = $request->get('hotel_id');
                $rooms['number_room'] = $request->get('number_room');
                $rooms['adult'] = $request->get('adult');
                $rooms['child'] = $request->get('child');
                $rooms['date_start'] = $request->get('date_start');
                $rooms['date_end'] = $request->get('date_end');
                $rooms['ages'] = $request->get('ages');


                //Logica
                //Asignación de fecha de entrada y fecha de salida
                if ($request->get('date_start') != '') {
                    $fecha_entrada = Carbon::createFromFormat('Y-m-d', $request->get('date_start'))->format('Y-m-d');
                } else {
                    $fecha_entrada = Carbon::now()->format('Y-m-d');
                }
                if ($request->get('date_end') != '') {
                    $fecha_salida = Carbon::createFromFormat('Y-m-d', $request->get('date_end'))->format('Y-m-d');
                } else {
                    $fecha_salida = Carbon::now()->format('Y-m-d');
                }

                $subchannel = Subchannel::where('id', $emulated_subchannel_id)->first();

                $sql_habitaciones = DB::connection('tenant')->table('mo_hotel_room')
                    ->select('mo_hotel_room.*',
                        'mo_hotel_room_translation.name',
                        'mo_hotel_room_translation.description',
                        'mo_hotel_room_translation.services_included',
                        'mo_hotel_room_translation.services_not_included',
                        'mo_hotel_room_translation.short_description',
                        'mo_hotel_room_translation.location',
                        'mo_hotel_room_translation.views',
                        'mo_hotel_room_translation.size',
                        'mo_hotel_room_translation.capacity',
                        'mo_hotel_room_translation.url_360',
                        'mo_hotel_room_translation.friendly_url',
                        'mo_hotel_room_translation.title_seo',
                        'mo_hotel_room_translation.description_seo',
                        'mo_hotel_room_translation.keywords_seo',

                        'mo_hotel_room_translation.breadcrumb',
                        'mo_hotel_room_translation.rel',
                        'mo_hotel_room_translation.index',
                        'mo_hotel_room_translation.og_title',
                        'mo_hotel_room_translation.og_description',
                        'mo_hotel_room_translation.og_image',
                        'mo_hotel_room_translation.twitter_title',
                        'mo_hotel_room_translation.twitter_description',
                        'mo_hotel_room_translation.twitter_image',
                        'mo_hotel_room_translation.canonical_url',
                        'mo_hotel_room_translation.script_head',
                        'mo_hotel_room_translation.script_body',
                        'mo_hotel_room_translation.script_footer',

                        'mo_hotel_room_translation.legal',
                        'mo_view_room.id as view_id',
                        'mo_view_room.room_id as view_room_id',
                        'mo_view_room.order as view_order',
                        'mo_view_room_translation.name as view_name',
                        'mo_view_room_translation.description as view_description',
                        'mo_view_room_translation.services_included as view_services_included',
                        'mo_view_room_translation.services_not_included as view_services_not_included',
                        'mo_view_room_translation.short_description as view_short_description',
                        'mo_view_room_translation.location as view_location',
                        'mo_view_room_translation.views as view_views',
                        'mo_view_room_translation.size as view_size',
                        'mo_view_room_translation.capacity as view_capacity',
                        'mo_view_room_translation.url_360 as view_url_360',
                        'mo_view_room_translation.friendly_url as view_friendly_url',
                        'mo_view_room_translation.title_seo as view_title_seo',
                        'mo_view_room_translation.description_seo as view_description_seo',
                        'mo_view_room_translation.keywords_seo as view_keywords_seo',

                        'mo_view_room_translation.breadcrumb as view_breadcrumb',
                        'mo_view_room_translation.rel as view_rel',
                        'mo_view_room_translation.index as view_index',
                        'mo_view_room_translation.og_title as view_og_title',
                        'mo_view_room_translation.og_description as view_og_description',
                        'mo_view_room_translation.og_image as view_og_image',
                        'mo_view_room_translation.twitter_title as view_twitter_title',
                        'mo_view_room_translation.twitter_description as view_twitter_description',
                        'mo_view_room_translation.twitter_image as view_twitter_image',
                        'mo_view_room_translation.canonical_url as view_canonical_url',
                        'mo_view_room_translation.script_head as view_script_head',
                        'mo_view_room_translation.script_body as view_script_body',
                        'mo_view_room_translation.script_footer as view_script_footer',

                        'mo_view_room_translation.legal as view_legal',
                        'mo_product_location.location_id',
                        'mo_contract_model.id as contract_mode_id',

                        DB::connection('tenant')->raw('IF(mo_view_room.id IS NULL, mo_hotel_room_translation.friendly_url, mo_view_room_translation.friendly_url) AS aux_friendly_url')
                    )
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->where('mo_hotel_room.hotel_id', '=', $request->get('hotel_id'))
                    ->join('mo_hotel_room_translation', 'mo_hotel_room.id', '=', 'mo_hotel_room_translation.room_id')
                    ->whereNull('mo_hotel_room_translation.deleted_at')
                    ->where('mo_hotel_room_translation.language_id', '=', $idioma->id)
                    ->join('mo_product', 'mo_hotel_room.hotel_id', 'mo_product.id')
                    ->where('mo_product.type_id', '=', 2)
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_location', 'mo_product_location.product_id', 'mo_product.id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->join('mo_price_product', 'mo_product.id', '=', 'mo_price_product.product_id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->whereNull('mo_price_product.service_id')
                    ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
                    ->whereNull('mo_price.deleted_at')
                    ->where('mo_price.date_start', '<=', $fecha_entrada)
                    ->where('mo_price.date_end', '>=', $fecha_salida)
                    ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_subchannel.id', '=', $emulated_subchannel_id)
                    ->leftJoin('mo_hotel_room_house', function ($join) {
                        $join->on('mo_hotel_room.id', '=', 'mo_hotel_room_house.room_id')->where('mo_hotel_room_house.deleted_at', null);
                    })
                    ->leftjoin('mo_house', function ($join) {
                        $join->on('mo_hotel_room_house.house_id', '=', 'mo_house.id')->where('mo_house.deleted_at', null);
                    })
                    ->groupBy('mo_hotel_room.id');

                $sql_count_habitaciones = DB::connection('tenant')->table('mo_hotel_room')
                    ->select('mo_hotel_room.id',
                        DB::connection('tenant')->raw('IF(mo_view_room.id IS NULL, mo_hotel_room_translation.friendly_url, mo_view_room_translation.friendly_url) AS aux_friendly_url')
                    )
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->where('mo_hotel_room.hotel_id', '=', $request->get('hotel_id'))
                    ->join('mo_hotel_room_translation', 'mo_hotel_room.id', '=', 'mo_hotel_room_translation.room_id')
                    ->whereNull('mo_hotel_room_translation.deleted_at')
                    ->where('mo_hotel_room_translation.language_id', '=', $idioma->id)
                    ->join('mo_product', 'mo_hotel_room.hotel_id', 'mo_product.id')
                    ->where('mo_product.type_id', '=', 2)
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_location', 'mo_product_location.product_id', 'mo_product.id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->join('mo_price_product', 'mo_product.id', '=', 'mo_price_product.product_id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->whereNull('mo_price_product.service_id')
                    ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
                    ->whereNull('mo_price.deleted_at')
                    ->where('mo_price.date_start', '<=', $fecha_entrada)
                    ->where('mo_price.date_end', '>=', $fecha_salida)
                    ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_subchannel.id', '=', $emulated_subchannel_id)
                    ->leftJoin('mo_hotel_room_house', function ($join) {
                        $join->on('mo_hotel_room.id', '=', 'mo_hotel_room_house.room_id')->where('mo_hotel_room_house.deleted_at', null);
                    })
                    ->leftjoin('mo_house', function ($join) {
                        $join->on('mo_hotel_room_house.house_id', '=', 'mo_house.id')->where('mo_house.deleted_at', null);
                    })
                    ->groupBy('mo_hotel_room.id');

                $datos_client_types = DB::connection('tenant')->table('mo_client_type')
                    ->select(
                        'mo_client_type.id',
                        'mo_client_type.code',
                        'mo_client_type.from',
                        'mo_client_type.to',
                        'mo_client_type_translation.language_id',
                        'mo_client_type_translation.name'
                    )
                    ->whereNull('mo_client_type.deleted_at')
                    ->join('mo_client_type_translation', 'mo_client_type_translation.client_type_id', 'mo_client_type.id')
                    ->whereNull('mo_client_type_translation.deleted_at')
                    ->where('mo_client_type_translation.language_id', '=', $idioma->id)
                    ->join('mo_price_product', 'mo_price_product.client_type_id', 'mo_client_type.id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->where('mo_price_product.product_id', '=', $request->get('hotel_id'))
                    ->join('mo_price', 'mo_price.id', 'mo_price_product.price_id')
                    ->whereNull('mo_price.deleted_at')
                    ->where('mo_price.date_start', '<=', $fecha_entrada)
                    ->where('mo_price.date_end', '>=', $fecha_salida)
                    ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_subchannel.id', '=', $emulated_subchannel_id)
                    ->groupBy('mo_client_type.id')
                    ->get();

                $sql_archivos = DB::connection('tenant')->table('mo_hotel_room_file')
                    ->select('mo_file.id as file_id',
                        'mo_file.file_name',
                        'mo_file.file_size',
                        'mo_file_translation.id',
                        'mo_file_translation.name',
                        'mo_file_translation.alternative_text',
                        'mo_file_translation.language_id',
                        'mo_file.file_dimensions',
                        'mo_file.mimetype',
                        'mo_file.type_id',
                        'mo_file.order',
                        'mo_file.url_file',
                        'mo_file.device_id',
                        'mo_file.key_file',
                        'mo_file_type.id as file_type')
                    ->whereNull('mo_hotel_room_file.deleted_at')
                    ->join('mo_file', 'mo_file.id', '=', 'mo_hotel_room_file.file_id')
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->whereNull('mo_file_translation.deleted_at')
                    ->where('mo_file_translation.language_id', $idioma->id)
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->whereNull('mo_file_type.deleted_at')
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    })
                    ->orderBy('mo_file.order', 'desc');

                $sql_archivos_view = DB::connection('tenant')->table('mo_view_room_file')
                    ->select('mo_file.id as file_id',
                        'mo_file.file_name',
                        'mo_file.file_size',
                        'mo_file.file_dimensions',
                        'mo_file.mimetype',
                        'mo_file.type_id',
                        'mo_file.order',
                        'mo_file.url_file',
                        'mo_file.device_id',
                        'mo_file.key_file',
                        'mo_file_translation.id',
                        'mo_file_translation.language_id',
                        'mo_file_translation.name',
                        'mo_file_translation.alternative_text',
                        'mo_file_type.id as file_type')
                    ->whereNull('mo_view_room_file.deleted_at')
                    ->join('mo_file', 'mo_file.id', 'mo_view_room_file.file_id')
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->whereNull('mo_file_translation.deleted_at')
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->whereNull('mo_file_type.deleted_at')
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    })
                    ->where('mo_file_translation.language_id', $idioma->id)
                    ->orderBy('mo_file.order', 'desc');

                $sql_rate_plan = DB::connection('tenant')->table('mo_hotel_rate_plan')
                    ->select(
                        'mo_hotel_rate_plan.id',
                        'mo_hotel_rate_plan_translation.name',
                        'mo_hotel_rate_plan_translation.description',
                        'mo_hotel_rate_plan_translation.cancellation_policies',
                        'mo_hotel_rate_plan_translation.guarantee'
                    )
                    ->whereNull('mo_hotel_rate_plan.deleted_at')
                    ->join('mo_hotel_rate_plan_translation','mo_hotel_rate_plan_translation.hotel_rate_plan_id','mo_hotel_rate_plan.id')
                    ->whereNull('mo_hotel_rate_plan_translation.deleted_at')
                    ->where('mo_hotel_rate_plan_translation.language_id', $idioma->id);

                $datos_hotel_language = DB::connection('tenant')->table('mo_hotel_language')
                    ->select(
                        'mo_language.abbreviation'
                    )
                    ->whereNull('mo_hotel_language.deleted_at')
                    ->where('mo_hotel_language.product_id', $request->get('hotel_id'))
                    ->where(function ($query) use ($idioma) {
                        $query->where('mo_hotel_language.language_id', $idioma->id)
                            ->orWhere('mo_hotel_language.default', 1);
                    })
                    ->join('mo_language', 'mo_language.id', 'mo_hotel_language.language_id')
                    ->whereNull('mo_language.deleted_at')
                    ->orderBy('mo_hotel_language.default', 'asc')
                    ->first();

                $datos_hotel_currency = DB::connection('tenant')->table('mo_currency')
                    ->select(
                        'mo_currency.id',
                        'mo_currency.iso_code'
                    )
                    ->whereNull('mo_currency.deleted_at')
                    ->join('mo_hotel_currency', 'mo_hotel_currency.currency_id', 'mo_currency.id')
                    ->whereNull('mo_hotel_currency.deleted_at')
                    ->where('mo_hotel_currency.product_id', $request->get('hotel_id'))
                    ->where(function ($query) use ($datos_moneda) {
                        $query->where('mo_hotel_currency.currency_id', $datos_moneda->id)
                            ->orWhere('mo_hotel_currency.default', 1);
                    })
                    ->orderBy('mo_hotel_currency.default', 'asc')
                    ->first();

                $datos_hotel_rate_plan = DB::connection('tenant')->table('mo_hotel_rate_plan')
                    ->whereNull('mo_hotel_rate_plan.deleted_at')
                    ->where('mo_hotel_rate_plan.product_id', $request->get('hotel_id'))
                    ->where('mo_hotel_rate_plan.subchannel_id', $request->get('subchannel_id'))
                    ->get();


                // Filtro para mostrar un solo tipo de habitacion
                if ($id != null) {
                    $sql_habitaciones->where('mo_hotel_room.id', '=', $id);
                    $sql_count_habitaciones->where('mo_hotel_room.id', '=', $id);
                }
                // Subcanal
                if ($subchannel->view_blocked == 1) {

                    $sql_habitaciones->join('mo_view_product', 'mo_view_product.product_id', '=', 'mo_product.id')
                        ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                        ->whereNull('mo_view_product.deleted_at')
                        ->where('mo_view_product.published', '=', 1)
                        ->where('mo_view_room.published', '=', 1)
                        ->leftJoin('mo_view_room', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')
                                ->whereNull('mo_view_room.deleted_at')
                                ->where('mo_view_room.subchannel_id', '=', $emulated_subchannel_id);
                        })
                        ->leftJoin('mo_view_room_translation', function ($join) use ($idioma) {
                            $join->on('mo_view_room_translation.view_room_id', '=', 'mo_view_room.id')
                                ->whereNull('mo_view_room_translation.deleted_at')
                                ->where('mo_view_room_translation.language_id', '=', $idioma->id);
                        });

                    $sql_count_habitaciones->join('mo_view_product', 'mo_view_product.product_id', '=', 'mo_product.id')
                        ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                        ->whereNull('mo_view_product.deleted_at')
                        ->where('mo_view_product.published', '=', 1)
                        ->where('mo_view_room.published', '=', 1)
                        ->leftJoin('mo_view_room', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')
                                ->whereNull('mo_view_room.deleted_at')
                                ->where('mo_view_room.subchannel_id', '=', $emulated_subchannel_id);
                        })
                        ->leftJoin('mo_view_room_translation', function ($join) use ($idioma) {
                            $join->on('mo_view_room_translation.view_room_id', '=', 'mo_view_room.id')
                                ->whereNull('mo_view_room_translation.deleted_at')
                                ->where('mo_view_room_translation.language_id', '=', $idioma->id);
                        });

                } else {
                    $sql_habitaciones->leftJoin('mo_view_product', function ($join) use ($emulated_subchannel_id) {
                        $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                            ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                            ->whereNull('mo_view_product.deleted_at');
                    })
                        ->Where(function ($query) {
                            $query->where('mo_view_product.published', '=', '1');

                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_product.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '0');
                            });
                        })->Where(function ($query) {
                            $query->where('mo_view_room.published', '=', '1');

                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_room.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '0');
                            });
                        })
                        ->leftJoin('mo_view_room', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')
                                ->whereNull('mo_view_room.deleted_at')
                                ->where('mo_view_room.subchannel_id', '=', $emulated_subchannel_id);
                        })
                        ->leftJoin('mo_view_room_translation', function ($join) use ($idioma) {
                            $join->on('mo_view_room_translation.view_room_id', '=', 'mo_view_room.id')
                                ->whereNull('mo_view_room_translation.deleted_at')
                                ->where('mo_view_room_translation.language_id', '=', $idioma->id);
                        });

                    $sql_count_habitaciones->leftJoin('mo_view_product', function ($join) use ($emulated_subchannel_id) {
                        $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                            ->where('mo_view_product.subchannel_id', '=', $emulated_subchannel_id)
                            ->whereNull('mo_view_product.deleted_at');
                    })
                        ->Where(function ($query) {
                            $query->where('mo_view_product.published', '=', '1');

                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_product.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '0');
                            });
                        })->Where(function ($query) {
                            $query->where('mo_view_room.published', '=', '1');

                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_room.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '0');
                            });
                        })
                        ->leftJoin('mo_view_room', function ($join) use ($emulated_subchannel_id) {
                            $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')
                                ->whereNull('mo_view_room.deleted_at')
                                ->where('mo_view_room.subchannel_id', '=', $emulated_subchannel_id);
                        })
                        ->leftJoin('mo_view_room_translation', function ($join) use ($idioma) {
                            $join->on('mo_view_room_translation.view_room_id', '=', 'mo_view_room.id')
                                ->whereNull('mo_view_room_translation.deleted_at')
                                ->where('mo_view_room_translation.language_id', '=', $idioma->id);
                        });


                }

                // Filtro categoría
                if ($request->get('category_id') != '') {
                    $sql_habitaciones->where('mo_hotel_room.category_id', '=', $request->get('category_id'));
                    $sql_count_habitaciones->where('mo_hotel_room.category_id', '=', $request->get('category_id'));
                }

                //Establece la ordenación de los resultados
                $orden = 'mo_hotel_room.order';
                switch ($request->get('order')) {
                    case 'name':
                        $orden = 'mo_hotel_room_translation.name';
                        break;
                    case 'order':
                        $orden = 'mo_view_room.order';
                        break;
                    default:
                        $orden = 'mo_hotel_room.order';
                        break;
                }
                //Establece el sentido de ordenación
                $orden_way = 'DESC';
                switch ($request->get('order_way')) {
                    case 'asc':
                        $orden_way = 'ASC';
                        break;
                    case 'desc':
                        $orden_way = 'DESC';
                        break;
                    default:
                        $orden_way = 'DESC';
                        break;
                }
                $sql_habitaciones->orderBy($orden, $orden_way);

                // Filtro friendly_url
                if ($request->get('friendly_url') != '') {
                    $sql_habitaciones = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_habitaciones->toSql()}) as sub"))
                        ->mergeBindings($sql_habitaciones);

                    $sql_count_habitaciones = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_count_habitaciones->toSql()}) as sub"))
                        ->mergeBindings($sql_count_habitaciones);

                    $sql_habitaciones->where('aux_friendly_url', $request->get('friendly_url'));

                    $sql_count_habitaciones->where('aux_friendly_url', $request->get('friendly_url'));

                }

                $sql_count_habitaciones = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sql_count_habitaciones->toSql()}) as sub"))
                    ->mergeBindings($sql_count_habitaciones);

                $datos_count_habitaciones = $sql_count_habitaciones->count();

                // Paginación

                //Inicializa variables paginación
                $limit = Settings::where('name', '=', 'limit_registers')->first()->value;
                $page = 1;

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_habitaciones = $sql_habitaciones->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_habitaciones = $sql_habitaciones->get();

                }


                if ($datos_count_habitaciones > 0 && $request->get('info_only') != 1) {
                    $sql_hoteles = DB::connection('tenant')->Table('mo_product')
                        ->where('mo_product.id', '=', $request->get('hotel_id'));
                    if ($id != null) {
                        $sql_hoteles->join('mo_hotel_room', 'mo_product.id', 'mo_hotel_room.hotel_id')
                            ->where('mo_hotel_room.id', '=', $id);
                        $datos_hoteles = $sql_hoteles->first();
                        if ($datos_hoteles->external_connection == 1) {
                            $rooms['external_code'] = $datos_hoteles->external_code;
                            $rooms['external_connection_url'] = $datos_hoteles->external_connection_url;
                            $rooms['external_service_user'] = $datos_hoteles->external_service_user;
                            $rooms['external_service_password'] = $datos_hoteles->external_service_password;
                            $rooms['code'] = $datos_hoteles->code;
                            // Se lanza petición al servicio externo
                            $datos_externos = getExternalRoomData($rooms, $datos_hotel_language->abbreviation, $datos_hotel_currency->iso_code);
                        }
                    } else {
                        $datos_hoteles = $sql_hoteles->first();
                        if ($datos_hoteles->external_connection == 1) {
                            $rooms['external_code'] = $datos_hoteles->external_code;
                            $rooms['external_connection_url'] = $datos_hoteles->external_connection_url;
                            $rooms['external_service_user'] = $datos_hoteles->external_service_user;
                            $rooms['external_service_password'] = $datos_hoteles->external_service_password;
                            // Se lanza petición al servicio externo
                            $datos_externos = getExternalRoomData($rooms, $datos_hotel_language->abbreviation, $datos_hotel_currency->iso_code);

                        }
                    }
                }
                $storage = Settings::where('name', '=', 'storage_files')->first()->value;

                // Se crea la salida
                $array['data'] = array();

                //consulta para obtener urls de imágenes seo para facebook y twitter
                $sql_images_og_twitter = DB::connection('tenant')->Table('mo_file')
                    ->select('mo_file.id','mo_file.url_file');
                //fin consulta para obtener urls de imágenes seo para facebook y twitter

                foreach ($datos_habitaciones as $habitacion) {

                    $array_salida_habitacion = array();

                    $array_salida_habitacion['id'] = $habitacion->id;
                    $array_salida_habitacion['hotel_id'] = $habitacion->hotel_id;
                    $array_salida_habitacion['location_id'] = $habitacion->location_id;
                    $array_salida_habitacion['category_id'] = $habitacion->category_id;
                    $array_salida_habitacion['code'] = $habitacion->code;
                    $array_salida_habitacion['uom_id'] = $habitacion->uom_id;
                    $array_salida_habitacion['address'] = $habitacion->address;
                    $array_salida_habitacion['longitude'] = $habitacion->longitude;
                    $array_salida_habitacion['latitude'] = $habitacion->latitude;
                    $array_salida_habitacion['pax'] = $habitacion->pax;

                    $datos_settings_contract_model = ContractModelProduct::where('contract_model_id', $habitacion->contract_mode_id)
                        ->where('product_id', $habitacion->hotel_id)
                        ->whereNull('service_id')
                        ->first();


                    if ($datos_settings_contract_model) {
                        $array_salida_habitacion['required_service'] = $datos_settings_contract_model->required_service;
                    } else {
                        $array_salida_habitacion['required_service'] = 0;
                    }

                    $array_salida_habitacion['file'] = array();

                    //obtiene urls de archivos para campos seo imagen de facebook y twitter
                    $sql_images_og_twitter_clone = clone $sql_images_og_twitter;
                    $datos_images_og_twitter = $sql_images_og_twitter_clone->whereIn('mo_file.id', [$habitacion->view_og_image, $habitacion->view_twitter_image])->get();

                    $og_image_hotel = null;
                    $twitter_image_hotel = null;
                    foreach ($datos_images_og_twitter as $image) {
                        if ($image->id == $habitacion->view_og_image) {
                            $og_image_hotel = $storage . $image->url_file;
                        }
                        if ($image->id == $habitacion->view_twitter_image) {
                            $twitter_image_hotel = $storage . $image->url_file;
                        }
                    }
                    //fin obtiene urls de archivos para campos seo imagen de facebook y twitter

                    if ($habitacion->view_id != null) {
                        $array_salida_habitacion['order'] = $habitacion->view_order;

                        $array_salida_habitacion['lang'][] = [
                            $idioma->abbreviation => [
                                'name' => $habitacion->view_name,
                                'description' => $habitacion->view_description,
                                'services_included' => $habitacion->view_services_included,
                                'services_not_included' => $habitacion->view_services_not_included,
                                'short_description' => $habitacion->view_short_description,
                                'location' => $habitacion->view_location,
                                'views' => $habitacion->view_views,
                                'size' => $habitacion->view_size,
                                'capacity' => $habitacion->view_capacity,
                                'url_360' => $habitacion->view_url_360,
                                'friendly_url' => $habitacion->view_friendly_url,
                                'title_seo' => $habitacion->view_title_seo,
                                'description_seo' => $habitacion->view_description_seo,
                                'keywords_seo' => $habitacion->view_keywords_seo,
                                'legal' => $habitacion->view_legal,

                                'breadcrumb' => $habitacion->view_breadcrumb,
                                'rel' => $habitacion->view_rel,
                                'index' => $habitacion->view_index,
                                'og_title' => $habitacion->view_og_title,
                                'og_description' => $habitacion->view_og_description,
                                'og_image' => $og_image_hotel,
                                'twitter_title' => $habitacion->view_twitter_title,
                                'twitter_description' => $habitacion->view_twitter_description,
                                'twitter_image' => $twitter_image_hotel,
                                'canonical_url' => $habitacion->view_canonical_url,
                                'script_head' => $habitacion->view_script_head,
                                'script_body' => $habitacion->view_script_body,
                                'script_footer' => $habitacion->view_script_footer,
                            ]];

                        // Si es el listado: si se envía tipo de archivo se muestran los indicados, si no se muestra uno de tipo imagen
                        // Si es el detalle: si se envía tipo de archivo se muestran los indicados, si no se muestran todos
                        $sql_archivos_view_clone = clone $sql_archivos_view;
                        $sql_archivos_view_clone->where('mo_view_room_file.view_room_id', '=', $habitacion->view_id);
                        if ($id == null) {
                            if ($request->get('file_types') != '') {
                                $array_file_types = $request->get('file_types');
                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                    $sql_archivos_view_clone->whereIn('mo_file.type_id', $array_file_types);
                                }
                            } else {
                                $sql_archivos_view_clone->where('mo_file.type_id', 3)->limit(1);
                            }
                        } else {
                            if ($request->get('file_types') != '') {
                                $array_file_types = $request->get('file_types');
                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                    $sql_archivos_view_clone->whereIn('mo_file.type_id', $array_file_types);
                                }
                            } else {
                                // No se modifica la consulta
                            }
                        }
                        $datos_archivos = $sql_archivos_view_clone->get();
                        //

                    } else {
                        $array_salida_habitacion['order'] = $habitacion->order;

                        $array_salida_habitacion['lang'][] = [
                            $idioma->abbreviation => [
                                'name' => $habitacion->name,
                                'description' => $habitacion->description,
                                'services_included' => $habitacion->services_included,
                                'services_not_included' => $habitacion->services_not_included,
                                'short_description' => $habitacion->short_description,
                                'location' => $habitacion->location,
                                'views' => $habitacion->views,
                                'size' => $habitacion->size,
                                'capacity' => $habitacion->capacity,
                                'url_360' => $habitacion->url_360,
                                'friendly_url' => $habitacion->friendly_url,
                                'title_seo' => $habitacion->title_seo,
                                'description_seo' => $habitacion->description_seo,
                                'keywords_seo' => $habitacion->keywords_seo,
                                'legal' => $habitacion->legal,

                                'breadcrumb' => $habitacion->breadcrumb,
                                'rel' => $habitacion->rel,
                                'index' => $habitacion->index,
                                'og_title' => $habitacion->og_title,
                                'og_description' => $habitacion->og_description,
                                'og_image' => $og_image_hotel,
                                'twitter_title' => $habitacion->twitter_title,
                                'twitter_description' => $habitacion->twitter_description,
                                'twitter_image' => $twitter_image_hotel,
                                'canonical_url' => $habitacion->canonical_url,
                                'script_head' => $habitacion->script_head,
                                'script_body' => $habitacion->script_body,
                                'script_footer' => $habitacion->script_footer,
                            ]];

                        $sql_archivos_clone = clone $sql_archivos;
                        $sql_archivos_clone->where('mo_hotel_room_file.room_id', $habitacion->id);
                        if ($id == null) {
                            if ($request->get('file_types') != '') {
                                $array_file_types = $request->get('file_types');
                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                    $sql_archivos_clone->whereIn('mo_file.type_id', $array_file_types);
                                }
                            } else {
                                $sql_archivos_clone->where('mo_file.type_id', 3)->limit(1);
                            }
                        } else {
                            if ($request->get('file_types') != '') {
                                $array_file_types = $request->get('file_types');
                                if (isset($array_file_types) && isset($array_file_types) != '') {
                                    $sql_archivos_clone->whereIn('mo_file_type.id', $array_file_types);
                                }
                            } else {
                                // No se modifica la consulta
                            }
                        }
                        $datos_archivos = $sql_archivos_clone->get();
                        //

                    }

                    //Casas asociadas a la habitación
                    $houses = DB::connection('tenant')->table('mo_hotel_room_house')->where('room_id', $habitacion->id)
                        ->select('house_id as id')
                        ->join('mo_house', 'mo_hotel_room_house.house_id', 'mo_house.id')
                        ->whereNull('mo_house.deleted_at')
                        ->whereNull('mo_hotel_room_house.deleted_at')
                        ->get();
                    $habitacion->house = array();
                    foreach ($houses as $house) {
                        foreach ($idiomas as $idi) {
                            $traduccion = DB::connection('tenant')->table('mo_house_translation')
                                ->select('name','short_description','description')
                                ->where('house_id', '=', $house->id)
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $house->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                        $habitacion->house[] = $house;
                    }




                    $array_salida_habitacion['house'] = $habitacion->house;



                    // Files - Si son todas las habitaciones
                    foreach ($datos_archivos as $archivo) {
                        $array_traduccion = array();
                        $array_traduccion[][$idioma->abbreviation] = [
                            'id' => $archivo->file_id,
                            'language_id' => $archivo->language_id,
                            'name' => $archivo->name,
                            'alternative_text' => $archivo->alternative_text,
                        ];
                        $array_salida_habitacion['file'][] = ['id' => $archivo->file_id,
                            'url_file' => $storage . $archivo->url_file,
                            'order' => $archivo->order,
                            'file_type_id' => $archivo->file_type,
                            'device_id' => $archivo->device_id,
                            'key_file' => $archivo->key_file,
                            'lang' => $array_traduccion];
                    }

                    // Asignación de disponibilidad y tarifas

                    if ($request->get('info_only') == '0' || $request->get('info_only') == null) {

                        foreach ($datos_client_types as $client_type) {
                            $array_client_type = array();

                            $array_client_type['id'] = $client_type->id;
                            $array_client_type['code'] = $client_type->code;
                            $array_client_type['from'] = $client_type->from;
                            $array_client_type['to'] = $client_type->to;

                            $array_client_type['lang'] = array();

                            $array_client_type['lang'][][$idioma->abbreviation] = [
                                'language_id' => $client_type->language_id,
                                'name' => $client_type->name
                            ];


                            $array_client_type['avail'] = array();
                            if (isset($datos_externos) && isset($datos_externos->RoomStays->RoomStay->RoomRates)) {
                                $array_rate_plan = array();
                                foreach ($datos_hotel_rate_plan as $rate_plan) {
                                    foreach ($datos_externos->RoomStays->RoomStay->RoomRates->RoomRate as $tarifa) {
                                        if ($tarifa->RoomTypeCode == $habitacion->code && $tarifa->RatePlanCode == $rate_plan->rate_plan_code && $datos_hotel_currency->iso_code == $tarifa->Rates->Rate->Total->CurrencyCode) {
                                            $array_prices['method'] = $tarifa->RatePlanCode;

                                            $sql_rate_plan_clone = clone $sql_rate_plan;

                                            $datos_rate_plan = $sql_rate_plan_clone->where('mo_hotel_rate_plan.rate_plan_code', $tarifa->RatePlanCode)
                                                ->first();

                                            $array_prices['lang'] = array();

                                            $array_language = array();
                                            if($datos_rate_plan) {
                                                $array_language = [
                                                    'id' => $datos_rate_plan->id,
                                                    'name' => $datos_rate_plan->name,
                                                    'description' => $datos_rate_plan->description,
                                                    'cancellation_policies' => $datos_rate_plan->cancellation_policies,
                                                    'guarantee' => $datos_rate_plan->guarantee
                                                ];
                                            }


                                            $array_prices['lang'][][$idioma->abbreviation] = $array_language;

                                            if ($tarifa->Rates->Rate->Total->CurrencyCode == $datos_hotel_currency->iso_code) {
                                                if ($tarifa->Rates->Rate->Total->CurrencyCode == $datos_moneda->iso_code) {
                                                    $array_prices['price'] = $tarifa->Rates->Rate->Total->AmountAfterTax;
                                                    $array_prices['currency_code'] = $tarifa->Rates->Rate->Total->CurrencyCode;
                                                } else {
                                                    $array_prices['price'] = $this->conversor($tarifa->Rates->Rate->Total->AmountAfterTax, $datos_hotel_currency->id, $datos_moneda->id, $settings_decimales);
                                                    $array_prices['currency_code'] = $datos_moneda->iso_code;
                                                }
                                            }

                                            $array_client_type['avail'][] = [
                                                'avail' => $tarifa->NumberOfUnits,
                                                'price' => [
                                                    $array_prices
                                                ]
                                            ];
                                        }
                                    }
                                }

                            }

                            $array_salida_habitacion['client_type'][] = $array_client_type;
                        }
                        // Fin Asignación de disponibilidad y tarifas
                    }
                    $array['data'][0]['room'][] = $array_salida_habitacion;
                }
                if ($id == null) {
                    $array["total_results"] = $datos_count_habitaciones;
                }

                $this->createLog($request->get('hotel_id'), $request, $idioma->id, null, null, $datos_count_habitaciones, null, $datos_count_habitaciones, $datos_count_habitaciones > 0 ? $id : null, true);
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal server error';
            $array['error_catch'] = $e->getMessage() . ' ' . $e->getLine();
            reportService($e, 'Buscador');
        }


        return response()->json($array, $array['error']);
    }

    public function showCalendarHotel(Request $request){

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $idiomas = Language::where('front',1)->get();

            $validator = Validator::make($request->all(), [
                'date_start' => 'date|required|date_format:"Y-m"',
                'date_end' => 'date|required|after_or_equal:date_start|date_format:"Y-m"',
                'product' => 'required|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->first();
            }

            if ($request->get('currency') != '') {

                $currency = Currency::where('iso_code', Str::upper($request->get('currency')))->first();
                if (!$currency) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['currency' => ['The currency is not exists']]);
                } else {
                    $id_moneda_convertir = $currency->id;
                    $datos_moneda = $currency;
                }
            } else {
                $moneda_settings = Settings::where('name', 'moneda_defecto_id')->first();
                $id_moneda_convertir = $moneda_settings->value;
                $datos_moneda = Currency::find($id_moneda_convertir);
            }

            //Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_productos = DB::connection('tenant')->table('mo_product')
                    ->select(
                        'mo_product.id',
                        'mo_product.code',
                        'mo_product.order',
                        'mo_product.external_code',
                        'mo_product.external_connection_url',
                        'mo_product.external_service_user',
                        'mo_product.external_service_password',
                        'mo_product_translation.language_id',
                        'mo_product_translation.name',
                        'mo_product_type.id as product_type_id'
                    )
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_translation', 'mo_product.id', 'mo_product_translation.product_id')
                    ->whereNull('mo_product_translation.deleted_at')
                    ->where('mo_product_translation.language_id', '=', $idioma->id)
                    ->join('mo_product_type', 'mo_product.type_id', 'mo_product_type.id')
                    ->whereNull('mo_product_type.deleted_at')
                    ->join('mo_product_location', 'mo_product.id', 'mo_product_location.product_id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->join('mo_location', 'mo_product_location.location_id', 'mo_location.id')
                    ->whereNull('mo_location.deleted_at');

                $datos_hotel_language = DB::connection('tenant')->table('mo_hotel_language')
                    ->select(
                        'mo_language.abbreviation'
                    )
                    ->whereNull('mo_hotel_language.deleted_at')
                    ->where('mo_hotel_language.product_id', $request->get('product'))
                    ->where(function ($query) use ($idioma) {
                        $query->where('mo_hotel_language.language_id', $idioma->id)
                            ->orWhere('mo_hotel_language.default', 1);
                    })
                    ->join('mo_language', 'mo_language.id', 'mo_hotel_language.language_id')
                    ->whereNull('mo_language.deleted_at')
                    ->orderBy('mo_hotel_language.default', 'asc')
                    ->first();

                $datos_hotel_currency = DB::connection('tenant')->table('mo_currency')
                    ->select(
                        'mo_currency.id',
                        'mo_currency.iso_code'
                    )
                    ->whereNull('mo_currency.deleted_at')
                    ->join('mo_hotel_currency', 'mo_hotel_currency.currency_id', 'mo_currency.id')
                    ->whereNull('mo_hotel_currency.deleted_at')
                    ->where('mo_hotel_currency.product_id', $request->get('product'))
                    ->where(function ($query) use ($datos_moneda) {
                        $query->where('mo_hotel_currency.currency_id', $datos_moneda->id)
                            ->orWhere('mo_hotel_currency.default', 1);
                    })
                    ->orderBy('mo_hotel_currency.default', 'asc')
                    ->first();

                $sql_locaciones = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id', 'mo_location.name', 'mo_location.depends_on')
                    ->whereNull('mo_location.deleted_at')
                    ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->groupBy('mo_location.id');

                $fecha_inicio = $request->get('date_start');
                $fecha_fin = $request->get('date_end');

                $sql_productos->groupBy('mo_product.id');
                $array['data'] = array();

                $sql_resultados_productos_clone = clone $sql_productos;

                $producto = $sql_resultados_productos_clone->where('mo_product.id', $request->get('product'))->first();

                if($producto) {

                    $array_salida_producto = array();

                    $array_salida_producto['id'] = $producto->id;

                    $sql_locaciones_clone = clone $sql_locaciones;
                    $datos_locaciones = $sql_locaciones_clone->where('mo_product_location.product_id', $producto->id)->get();

                    $array_salida_producto['lang'][][$idioma->abbreviation] = [
                        'language_id' => $producto->language_id,
                        'name' => $producto->name,
                    ];

                    // Tipo de producto
                    $array_tipo_producto = array();
                    $array_tipo_producto['id'] = $producto->product_type_id;
                    $array_salida_producto['type'][] = $array_tipo_producto;
                    // FIN Tipo de producto

                    $array_traduccion = array();

                    $array_salida_producto['location'] = array();
                    foreach ($datos_locaciones as $locacion) {

                        $array_location = [
                            'id' => $locacion->id,
                            'name' => $locacion->name,
                        ];

                        $array_disponibilidades = array();
                        $fecha = '';
                        $fecha = Carbon::createFromFormat('Y-m', $fecha_inicio)->startOfMonth();
                        $fecha_end = Carbon::createFromFormat('Y-m', $fecha_fin)->endOfMonth();

                        $rooms = array();
                        $rooms['hotel_id'] = $request->get('product');
                        $rooms['number_room'] = 1;
                        $rooms['adult'] = 2;
                        $rooms['child'] = 0;
                        $rooms['date_start'] = $fecha->format('Y-m-d');
                        $rooms['date_end'] = $fecha_end->format('Y-m-d');

                        $rooms['external_code'] = $producto->external_code;
                        $rooms['external_connection_url'] = $producto->external_connection_url;
                        $rooms['external_service_user'] = $producto->external_service_user;
                        $rooms['external_service_password'] = $producto->external_service_password;

                        $datos_externos = getExternalAvailability($rooms, $datos_hotel_language->abbreviation, $datos_hotel_currency->iso_code);

                        if($datos_externos) {
                            $count = 0;
                            if(gettype($datos_externos->HotelStays->HotelStay) == "array") {
                                foreach ($datos_externos->HotelStays->HotelStay as $avail) {
                                    $fecha_avail_inicio = Carbon::parse($avail->Availability->Start);
                                    $fecha_avail_fin = Carbon::parse($avail->Availability->End);

                                    while($fecha_avail_inicio <= $fecha_avail_fin) {
                                        $array_disponibilidades[] = [
                                            'date' => $fecha_avail_inicio->format('Y-m-d'),
                                            'closed' => $avail->Availability->Status == 'Open' ? 0 : 1,
                                        ];
                                        $fecha_avail_inicio->addDay();
                                    }
                                }
                            } else {

                                if(isset($avail->Availability)){
                                    $fecha_avail_inicio = Carbon::parse($datos_externos->HotelStays->HotelStay->Availability->Start);
                                    $fecha_avail_fin = Carbon::parse($datos_externos->HotelStays->HotelStay->Availability->End);
                                } else {
                                    $fecha_avail_inicio = Carbon::parse($datos_externos->HotelStays->HotelStay->Availability->Start);
                                    $fecha_avail_fin = Carbon::parse($datos_externos->HotelStays->HotelStay->Availability->End);
                                }

                                while($fecha_avail_inicio <= $fecha_avail_fin) {
                                    $array_disponibilidades[] = [
                                        'date' => $fecha_avail_inicio->format('Y-m-d'),
                                        'closed' => $datos_externos->HotelStays->HotelStay->Availability->Status == 'Open' ? 0 : 1,
                                    ];
                                    $fecha_avail_inicio->addDay();
                                }
                            }

                        }
                        //Fin disponibilidades del producto

                        $array_location['availabilities'] = $array_disponibilidades;


                        $array_salida_producto['location'][] = $array_location;
                    }

                    $array['data'][0]['product'][] = $array_salida_producto;
                }

            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');
        }

        return response()->json($array, $array['error']);
    }
    /**
     * Funcion que lista todas las casas
     *
     * Para la consulta de casas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showHouse(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
                'house_id' => 'integer|min:0',
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->first();
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $fecha_entrada = Carbon::now()->format('Y-m-d');
                $fecha_salida = Carbon::now()->format('Y-m-d');

                $subchannel = Subchannel::where('id', $request->get('subchannel_id'))->first();

                //Se preparan las consultas
                $casas = DB::connection('tenant')->table('mo_house')
                    ->select('mo_house.id as id', 'mo_house.latitude', 'mo_house.longitude','mo_house_translation.name','mo_house_translation.description','mo_house_translation.short_description')
                    ->join('mo_house_translation', 'mo_house.id', '=', 'mo_house_translation.house_id')
                    ->where('mo_house_translation.deleted_at', '=', null)
                    ->where('mo_house.deleted_at', '=', null)
                    ->leftJoin('mo_hotel_room_house', function ($join) {
                        $join->on('mo_house.id', '=', 'mo_hotel_room_house.house_id')->where('mo_hotel_room_house.deleted_at', null);
                    })
                    ->leftjoin('mo_hotel_room', function ($join) {
                        $join->on('mo_hotel_room_house.room_id', '=', 'mo_hotel_room.id')->where('mo_hotel_room.deleted_at', null);
                    });


                $sub = DB::connection('tenant')->table('mo_house')
                    ->select('mo_house.id')
                    ->join('mo_house_translation', 'mo_house.id', '=', 'mo_house_translation.house_id')
                    ->where('mo_house_translation.deleted_at', '=', null)
                    ->where('mo_house.deleted_at', '=', null)
                    ->leftJoin('mo_hotel_room_house', function ($join) {
                        $join->on('mo_house.id', '=', 'mo_hotel_room_house.house_id')->where('mo_hotel_room_house.deleted_at', null);
                    })
                    ->leftjoin('mo_hotel_room', function ($join) {
                        $join->on('mo_hotel_room_house.room_id', '=', 'mo_hotel_room.id')->where('mo_hotel_room.deleted_at', null);
                    });

                $sql_archivos = DB::connection('tenant')->table('mo_hotel_room_file')
                    ->select('mo_file.id as file_id',
                        'mo_file.file_name',
                        'mo_file.file_size',
                        'mo_file_translation.id',
                        'mo_file_translation.name',
                        'mo_file_translation.alternative_text',
                        'mo_file_translation.language_id',
                        'mo_file.file_dimensions',
                        'mo_file.mimetype',
                        'mo_file.type_id',
                        'mo_file.order',
                        'mo_file.url_file',
                        'mo_file.device_id',
                        'mo_file.key_file',
                        'mo_file_type.id as file_type')
                    ->whereNull('mo_hotel_room_file.deleted_at')
                    ->join('mo_file', 'mo_file.id', '=', 'mo_hotel_room_file.file_id')
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->whereNull('mo_file_translation.deleted_at')
                    ->where('mo_file_translation.language_id', $idioma->id)
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->whereNull('mo_file_type.deleted_at')
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    })
                    ->orderBy('mo_file.order', 'desc');

                $sql_archivos_view = DB::connection('tenant')->table('mo_view_room_file')
                    ->select('mo_file.id as file_id',
                        'mo_file.file_name',
                        'mo_file.file_size',
                        'mo_file.file_dimensions',
                        'mo_file.mimetype',
                        'mo_file.type_id',
                        'mo_file.order',
                        'mo_file.url_file',
                        'mo_file.device_id',
                        'mo_file.key_file',
                        'mo_file_translation.id',
                        'mo_file_translation.language_id',
                        'mo_file_translation.name',
                        'mo_file_translation.alternative_text',
                        'mo_file_type.id as file_type')
                    ->whereNull('mo_view_room_file.deleted_at')
//                    ->join('mo_view_room', 'mo_view_room.id', 'mo_view_room_file.view_room_id')
//                    ->whereNull('mo_view_room.deleted_at')
                    ->join('mo_file', 'mo_file.id', 'mo_view_room_file.file_id')
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->whereNull('mo_file_translation.deleted_at')
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->whereNull('mo_file_type.deleted_at')
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    })
                    ->where('mo_file_translation.language_id', $idioma->id)
                    ->orderBy('mo_file.order', 'desc');

                // Recogida de datos enviados por el usuario y establecimiento de filtros
                if ($request->get('lang') != '') {
                    $casas->where('language_id', '=', $idioma->id);
                    $sub->where('language_id', '=', $idioma->id);
                }

                if ($request->get('name') != '') {

                    $casas->Where(function ($query) use ($request) {
                        $query->where('mo_house_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_house_translation.description', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_house_translation.short_description', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_house_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_house_translation.description', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_house_translation.short_description', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('room_id') != '') {
                    $casas->where('mo_hotel_room_house.room_id', '=', $request->get('room_id'))->where('mo_hotel_room.id', $request->get('room_id'));
                    $sub->where('mo_hotel_room_house.room_id', '=', $request->get('room_id'))->where('mo_hotel_room.id', $request->get('room_id'));
                }


                // Order
                $orden = 'mo_house_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_house_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_house_translation.description';
                            break;
                        case 'short_description':
                            $orden = 'mo_house_translation.short_description';
                            break;
                        case 'order':
                            $orden = 'mo_house.order';
                            break;
                        case 'id':
                            $orden = 'mo_house.id';
                            break;
                        default:
                            $orden = 'mo_house_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way


                $casas->groupBy('mo_house.id')->orderBy($orden, $sentido);


                // Paginación segun filtros y ejecución consulta

                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $casas = $casas->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $casas = $casas->get();

                }
                // FIN Paginación


                $sub->groupBy('mo_house_translation.house_id');
                $casas_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $casas_count->count();


                //ruta de storage de settings para concatenar a ruta de archivo de casas y habitaciones
                $storage = Settings::where('name', 'storage_files')->first()->value;

                $array['data'] = array();
                foreach ($casas as $casa) {

                    // Preparación de archivos de la casa
                    $casa->file = array();

                    $files_house = DB::connection('tenant')->table('mo_file')
                        ->select('mo_file.id', 'mo_file.file_name', 'mo_file.file_size', 'mo_file.file_dimensions', 'mo_file.mimetype', 'mo_file.type_id', 'mo_file.order', 'mo_file.url_file',
                            'mo_file.device_id', 'mo_file.key_file')
                        ->join('mo_house_file', 'mo_file.id', '=', 'mo_house_file.file_id')
                        ->where('mo_house_file.house_id', '=', $casa->id)
                        ->where('mo_house_file.deleted_at', '=', null)
                        ->where('mo_file.deleted_at', '=', null)
                        ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                        ->where('mo_file_translation.deleted_at', null)
                        ->where('mo_file_translation.language_id', $idioma->id)
                        ->groupBy('mo_file.id')->get();

                    foreach ($files_house as $file) {
                        //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                        $tipo = FileType::find($file->type_id);

                        $array_file = ['id' => $file->id, 'file_name' => $file->file_name, 'file_size' => convertExtension($file->file_size),
                            'file_dimensions' => $file->file_dimensions, 'mimetype' => $file->mimetype, 'order' => $file->order, 'url_file' => $storage . $file->url_file,
                            'device_id' => $file->device_id, 'key_file' => $file->key_file,
                            'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];

                        //si el tipo no existe o está borrado no busca sus traducciones
                        if ($tipo) {
                            $traduccion_tipo_file = FileTypeTranslation::where('type_id', $file->type_id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idioma->id)
                                ->groupBy('mo_file_type_translation.type_id')
                                ->first();

                            $array_file['type']['lang'][][$idioma->abbreviation] = $traduccion_tipo_file;
                        }

                        $traducciones_file = FileTranslation::where('file_id', $file->id)
                            ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                            ->where('language_id', '=', $idioma->id)
                            ->whereNull('deleted_at')
                            ->groupBy('mo_file_translation.file_id')
                            ->first();

                        $array_file['lang'][][$idioma->abbreviation] = $traducciones_file;

                        $casa->file[] = $array_file;
                    }



                    // Preparación de habitaciones de la casa

                    $casa->room = array();

                    $sql_rooms = DB::connection('tenant')->table('mo_hotel_room')
                        ->select(
                            'mo_hotel_room.*',
                            'mo_hotel_room_translation.name',
                            'mo_hotel_room_translation.description',
                            'mo_hotel_room_translation.services_included',
                            'mo_hotel_room_translation.services_not_included',
                            'mo_hotel_room_translation.short_description',
                            'mo_hotel_room_translation.location',
                            'mo_hotel_room_translation.views',
                            'mo_hotel_room_translation.size',
                            'mo_hotel_room_translation.capacity',
                            'mo_hotel_room_translation.url_360',
                            'mo_hotel_room_translation.friendly_url',
                            'mo_hotel_room_translation.title_seo',
                            'mo_hotel_room_translation.description_seo',
                            'mo_hotel_room_translation.keywords_seo',

                            'mo_hotel_room_translation.breadcrumb',
                            'mo_hotel_room_translation.rel',
                            'mo_hotel_room_translation.index',
                            'mo_hotel_room_translation.og_title',
                            'mo_hotel_room_translation.og_description',
                            'mo_hotel_room_translation.og_image',
                            'mo_hotel_room_translation.twitter_title',
                            'mo_hotel_room_translation.twitter_description',
                            'mo_hotel_room_translation.twitter_image',
                            'mo_hotel_room_translation.canonical_url',
                            'mo_hotel_room_translation.script_head',
                            'mo_hotel_room_translation.script_body',
                            'mo_hotel_room_translation.script_footer',

                            'mo_hotel_room_translation.legal',
                            'mo_view_room.id as view_id',
                            'mo_view_room.room_id as view_room_id',
                            'mo_view_room.order as view_order',
                            'mo_view_room_translation.name as view_name',
                            'mo_view_room_translation.description as view_description',
                            'mo_view_room_translation.services_included as view_services_included',
                            'mo_view_room_translation.services_not_included as view_services_not_included',
                            'mo_view_room_translation.short_description as view_short_description',
                            'mo_view_room_translation.location as view_location',
                            'mo_view_room_translation.views as view_views',
                            'mo_view_room_translation.size as view_size',
                            'mo_view_room_translation.capacity as view_capacity',
                            'mo_view_room_translation.url_360 as view_url_360',
                            'mo_view_room_translation.friendly_url as view_friendly_url',
                            'mo_view_room_translation.title_seo as view_title_seo',
                            'mo_view_room_translation.description_seo as view_description_seo',
                            'mo_view_room_translation.keywords_seo as view_keywords_seo',

                            'mo_view_room_translation.breadcrumb as view_breadcrumb',
                            'mo_view_room_translation.rel as view_rel',
                            'mo_view_room_translation.index as view_index',
                            'mo_view_room_translation.og_title as view_og_title',
                            'mo_view_room_translation.og_description as view_og_description',
                            'mo_view_room_translation.og_image as view_og_image',
                            'mo_view_room_translation.twitter_title as view_twitter_title',
                            'mo_view_room_translation.twitter_description as view_twitter_description',
                            'mo_view_room_translation.twitter_image as view_twitter_image',
                            'mo_view_room_translation.canonical_url as view_canonical_url',
                            'mo_view_room_translation.script_head as view_script_head',
                            'mo_view_room_translation.script_body as view_script_body',
                            'mo_view_room_translation.script_footer as view_script_footer',

                            'mo_view_room_translation.legal as view_legal',
                            'mo_contract_model.id as contract_mode_id',

                            DB::connection('tenant')->raw('IF(mo_view_room.id IS NULL, mo_hotel_room_translation.friendly_url, mo_view_room_translation.friendly_url) AS aux_friendly_url')
                        )
                        ->join('mo_hotel_room_house', 'mo_hotel_room_house.room_id', 'mo_hotel_room.id')
                        ->where('mo_hotel_room_house.house_id', $casa->id)
                        ->whereNull('mo_hotel_room.deleted_at')
                        ->whereNull('mo_hotel_room_house.deleted_at')
                        ->join('mo_hotel_room_translation', 'mo_hotel_room.id', '=', 'mo_hotel_room_translation.room_id')
                        ->whereNull('mo_hotel_room_translation.deleted_at')
                        ->where('mo_hotel_room_translation.language_id', '=', $idioma->id)
                        ->join('mo_product', 'mo_hotel_room.hotel_id', 'mo_product.id')
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_price_product', 'mo_product.id', '=', 'mo_price_product.product_id')
                        ->whereNull('mo_price_product.deleted_at')
                        ->whereNull('mo_price_product.service_id')
                        ->join('mo_price', 'mo_price_product.price_id', '=', 'mo_price.id')
                        ->whereNull('mo_price.deleted_at')
                        ->where('mo_price.date_start', '<=', $fecha_entrada)
                        ->where('mo_price.date_end', '>=', $fecha_salida)
                        ->join('mo_contract_model', 'mo_price.contract_model_id', '=', 'mo_contract_model.id')
                        ->whereNull('mo_contract_model.deleted_at')
                        ->join('mo_subchannel', 'mo_contract_model.id', '=', 'mo_subchannel.contract_model_id')
                        ->whereNull('mo_subchannel.deleted_at')
                        ->where('mo_subchannel.id', '=', $subchannel->id)
                        ->groupBy('mo_hotel_room.id');

                    if ($subchannel->view_blocked == 1) {
                        $sql_rooms->join('mo_view_product', 'mo_view_product.product_id', '=', 'mo_product.id')
                            ->where('mo_view_product.subchannel_id', '=', $subchannel->id)
                            ->whereNull('mo_view_product.deleted_at')
                            ->where('mo_view_product.published', '=', 1)
                            ->where('mo_view_room.published', '=', 1)
                            ->leftJoin('mo_view_room', function ($join) use ($subchannel) {
                                $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')
                                    ->whereNull('mo_view_room.deleted_at')
                                    ->where('mo_view_room.subchannel_id', '=', $subchannel->id);
                            })
                            ->leftJoin('mo_view_room_translation', function ($join) use ($idioma) {
                                $join->on('mo_view_room_translation.view_room_id', '=', 'mo_view_room.id')
                                    ->whereNull('mo_view_room_translation.deleted_at')
                                    ->where('mo_view_room_translation.language_id', '=', $idioma->id);
                            });
                    } else {
                        $sql_rooms->leftJoin('mo_view_product', function ($join) use ($subchannel) {
                            $join->on('mo_view_product.product_id', '=', 'mo_product.id')
                                ->where('mo_view_product.subchannel_id', '=', $subchannel->id)
                                ->whereNull('mo_view_product.deleted_at');
                            })
                            ->Where(function ($query) {
                                $query->where('mo_view_product.published', '=', '1');

                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_product.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '0');
                                });
                            })->Where(function ($query) {
                                $query->where('mo_view_room.published', '=', '1');

                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_room.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '0');
                                });
                            })
                            ->leftJoin('mo_view_room', function ($join) use ($subchannel) {
                                $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')
                                    ->whereNull('mo_view_room.deleted_at')
                                    ->where('mo_view_room.subchannel_id', '=', $subchannel->id);
                            })
                            ->leftJoin('mo_view_room_translation', function ($join) use ($idioma) {
                                $join->on('mo_view_room_translation.view_room_id', '=', 'mo_view_room.id')
                                    ->whereNull('mo_view_room_translation.deleted_at')
                                    ->where('mo_view_room_translation.language_id', '=', $idioma->id);
                            });
                    }

                    $hotel_rooms = $sql_rooms->get();

                    foreach ($hotel_rooms as $hotel_room) {

                        // Preparación de archivos de la habitación
                        $array_salida_habitacion = array();

                        $array_salida_habitacion['id'] = $hotel_room->id;
                        $array_salida_habitacion['hotel_id'] = $hotel_room->hotel_id;
                        $array_salida_habitacion['category_id'] = $hotel_room->category_id;
                        $array_salida_habitacion['code'] = $hotel_room->code;
                        $array_salida_habitacion['uom_id'] = $hotel_room->uom_id;
                        $array_salida_habitacion['address'] = $hotel_room->address;
                        $array_salida_habitacion['longitude'] = $hotel_room->longitude;
                        $array_salida_habitacion['latitude'] = $hotel_room->latitude;
                        $array_salida_habitacion['pax'] = $hotel_room->pax;

                        if ($hotel_room->view_id != null) {
                            $sql_archivos_view_clone = clone $sql_archivos_view;
                            $sql_archivos_view_clone->where('mo_view_room_file.view_room_id', '=', $hotel_room->view_id);

                            $files_hotel_room = $sql_archivos_view_clone->get();
                        } else {
                            $sql_archivos_clone = clone $sql_archivos;
                            $sql_archivos_clone->where('mo_hotel_room_file.room_id', $hotel_room->id);

                            $files_hotel_room = $sql_archivos_clone->get();
                        }


                        $array_salida_habitacion['file'] = array();

                        foreach ($files_hotel_room as $file) {

                            //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                            $tipo = FileType::find($file->type_id);

                            $array_file_room = ['id' => $file->id, 'file_name' => $file->file_name, 'file_size' => convertExtension($file->file_size),
                                'file_dimensions' => $file->file_dimensions, 'mimetype' => $file->mimetype, 'order' => $file->order, 'url_file' => $storage . $file->url_file,
                                'device_id' => $file->device_id, 'key_file' => $file->key_file,
                                'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];

                            //si el tipo no existe o está borrado no busca sus traducciones
                            if ($tipo) {
                                $traduccion_tipo_file = FileTypeTranslation::where('type_id', $file->type_id)
                                    ->select('id', 'language_id', 'name')
                                    ->where('language_id', '=', $idioma->id)
                                    ->whereNull('deleted_at')
                                    ->groupBy('mo_file_type_translation.type_id')
                                    ->first();

                                $array_file_room['type']['lang'][][$idioma->abbreviation] = $traduccion_tipo_file;
                            }

                            $traduccion_file = FileTranslation::where('file_id', $file->id)
                                ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                                ->where('language_id', '=', $idioma->id)
                                ->whereNull('deleted_at')
                                ->groupBy('mo_file_translation.file_id')
                                ->first();

                            $array_file_room['lang'][][$idioma->abbreviation] = $traduccion_file;

                            $array_salida_habitacion['file'][] = $array_file_room;
                        }

                        if($hotel_room->view_id != null) {
                            $array_salida_habitacion['order'] = $hotel_room->view_order;

                            $array_salida_habitacion['lang'][] = [
                                $idioma->abbreviation => [
                                    'name' => $hotel_room->view_name,
                                    'description' => $hotel_room->view_description,
                                    'services_included' => $hotel_room->view_services_included,
                                    'services_not_included' => $hotel_room->view_services_not_included,
                                    'short_description' => $hotel_room->view_short_description,
                                    'location' => $hotel_room->view_location,
                                    'views' => $hotel_room->view_views,
                                    'size' => $hotel_room->view_size,
                                    'capacity' => $hotel_room->view_capacity,
                                    'url_360' => $hotel_room->view_url_360,
                                    'friendly_url' => $hotel_room->view_friendly_url,
                                    'title_seo' => $hotel_room->view_title_seo,
                                    'description_seo' => $hotel_room->view_description_seo,
                                    'keywords_seo' => $hotel_room->view_keywords_seo,
                                    'legal' => $hotel_room->view_legal,

                                    'breadcrumb' => $hotel_room->view_breadcrumb,
                                    'rel' => $hotel_room->view_rel
                                ]];
                        } else {
                            $array_salida_habitacion['order'] = $hotel_room->order;

                            $array_salida_habitacion['lang'][] = [
                                $idioma->abbreviation => [
                                    'name' => $hotel_room->name,
                                    'description' => $hotel_room->description,
                                    'services_included' => $hotel_room->services_included,
                                    'services_not_included' => $hotel_room->services_not_included,
                                    'short_description' => $hotel_room->short_description,
                                    'location' => $hotel_room->location,
                                    'views' => $hotel_room->views,
                                    'size' => $hotel_room->size,
                                    'capacity' => $hotel_room->capacity,
                                    'url_360' => $hotel_room->url_360,
                                    'friendly_url' => $hotel_room->friendly_url,
                                    'title_seo' => $hotel_room->title_seo,
                                    'description_seo' => $hotel_room->description_seo,
                                    'keywords_seo' => $hotel_room->keywords_seo,
                                    'legal' => $hotel_room->legal,

                                    'breadcrumb' => $hotel_room->breadcrumb,
                                    'rel' => $hotel_room->rel,
                                    'index' => $hotel_room->index
                                ]];
                        }


//                        $traduccion = DB::connection('tenant')->table('mo_hotel_room_translation')
//                            ->select('name', 'short_description', 'description', 'views', 'size', 'capacity')
//                            ->where('room_id', '=', $hotel_room->id)
//                            ->where('language_id', '=', $idioma->id)
//                            ->first();
//                        $hotel_room->lang[][$idioma->abbreviation] = $traduccion;

                        $casa->room[] = $array_salida_habitacion;
                    }

                    $array['data'][0]['house'][] = [
                        'id' => $casa->id,
                        'latitude' => $casa->latitude,
                        'longitude' => $casa->longitude,
                        'lang' => array([
                            $idioma->abbreviation => [
                                'name' => $casa->name,
                                'description' => $casa->description,
                                'short_description' => $casa->short_description,
                            ]
                        ]),
                        'room' => $casa->room,
                        'file' => $casa->file,
                    ];

                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que devuelve las locaciones de transportación
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTransportationLocation(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;

            // Validación
            $mensaje_validador = collect();


            $validator = Validator::make($request->all(), [
                'page' => 'integer',
                'limit' => 'integer',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $sql_transportation_location = DB::connection('tenant')->table('mo_transportation_location')
                    ->select('mo_transportation_location.id', 'mo_transportation_location.name', 'mo_transportation_location.api_code')
                    ->where('mo_transportation_location.deleted_at', '=', null);
                $sub = DB::connection('tenant')->table('mo_transportation_location')
                    ->select('mo_transportation_location.id', 'mo_transportation_location.name', 'mo_transportation_location.api_code')
                    ->where('mo_transportation_location.deleted_at', '=', null);


                // Order
                $orden = 'mo_transportation_location.id';
                $orden_sin_parametros = null;
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_transportation_location.id';
                            break;
                        case 'name':
                            $orden = 'mo_transportation_location.name';
                            break;
                        case 'iso3':
                            $orden = 'mo_transportation_location.api_code';
                            break;
                        default:
                            $orden = 'mo_transportation_location.id';
                            break;
                    }
                } else {
                    // Caso - Sin filtros de ordenamiento
                    $orden_sin_parametros = 'mo_transportation_location.order';
                }

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $transportation_location = $sql_transportation_location->groupBy('mo_transportation_location.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $transportation_location = $transportation_location->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $transportation_location = $transportation_location->get();

                }

                //Fin de paginación


                $transportation_location_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $transportation_location_count->count();


                $array['data'][]['transportation_location'] = $transportation_location;

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que devuelve los pickups de un producto especificado para una localización dada
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPickups(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'date' => 'required|after_or_equal:today|date_format:"Y-m-d"',
                'product_code' => 'required',
                'transportation_location_code' => 'required',
                'subchannel_id' => 'required|exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
            ]);


            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_transportation_location = DB::connection('tenant')->table('mo_transportation_location_detail')
                    ->select(
                        'mo_transportation_location_detail.code',
                        'mo_transportation_location_detail.name',
                        'mo_transportation_location_detail.session'
                    )
                    ->whereNull('mo_transportation_location_detail.deleted_at')
                    ->join('mo_transportation_location','mo_transportation_location.id','mo_transportation_location_detail.transportation_location_id')
                    ->whereNull('mo_transportation_location.deleted_at')
                    ->where('mo_transportation_location.api_code', $request->get('transportation_location_code'))
                    ->join('mo_product','mo_product.id','mo_transportation_location_detail.product_id')
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_subchannel','mo_subchannel.id','mo_transportation_location_detail.subchannel_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_product.code', $request->get('product_code'))
                    ->where('mo_transportation_location_detail.subchannel_id', $request->get('subchannel_id'))
                    ->where(function ($query) use ($request){
                        $query->whereNull('mo_transportation_location_detail.date')
                            ->orWhere('mo_transportation_location_detail.date',$request->get('date'));
                    });

                if($request->get('session') != '')
                {
                    $sql_transportation_location->where('mo_transportation_location_detail.session', $request->get('session'));
                }
                
                $array_pickup = array();

                $datos_transportation = $sql_transportation_location->get();

                foreach($datos_transportation as $transportation) {
                    $array_aux = array();

                    $array_aux['id'] = $transportation->code;
                    $array_aux['name'] = $transportation->name . ' - ' . $transportation->session;
                    $array_aux['longitude'] = null;
                    $array_aux['latitude'] = null;

                    $array_pickup[] = $array_aux;
                }

                $array['data'][]['pickup'] = $array_pickup;
                
            }

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal server error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Buscador');
        }

        return response()->json($array, $array['error']);
    }

}
