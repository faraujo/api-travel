<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Language;
use App\Models\tenant\PaymentMethodTranslation;
use App\Models\tenant\Settings;
use App\Models\tenant\UserToken;
use App\Models\tenant\CurrencyExchange;
use App\Models\tenant\Insurance;
use App\Models\tenant\ReservationInsurance;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Http\Request;

class ReservationInsuranceController extends Controller
{

    /**
     * Función que asigna un seguro a la reserva
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignInsurance(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'emulated_subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'insurance_id' => 'required|integer|min:0|exists:tenant.mo_insurance,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $user_id = '';
            $client_session = '';
            $subchannel_id = '';
            $emulated_subchannel_id = '';

            // Si la petición es pública a través de un canal de venta
            if (strpos($request->path(), 'insurance/search') !== false) {

                // Si el usuario está logueado
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

                } elseif ($request->get('client_session') != '') { // Si no está logueado
                    $client_session = $request->get('client_session');

                } else { // Si no está logueado y no aporta identificación temporal
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token or session is required']]);
                }

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS

                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'id' => 'exists:tenant.mo_reservation,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                    //Fin validación

                } else {

                    // Preparación de datos
                    $reserva_client_id = null;
                    $reserva_client_session = null;
                    $reserva_agent_id = null;
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'insurance/search') !== false) {
                        if ($user_id != '') {
                            $reserva_client_id = $user_id;
                        } else {
                            $reserva_client_session = Str::random('200');

                        }
                    } else { // Si la petición procede del POS
                        $reserva_agent_id = $user_id;
                    }
                    // FIN preparación de datos

                    $sql_reservation = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id',
                            'mo_reservation.client_id',
                            'mo_reservation.client_session',
                            'mo_reservation.subchannel_id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                        ->whereNull('mo_reservation_detail.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'));

                    $datos_reservation = $sql_reservation->first();

                    // Validación por permisos
                    $error_aux = 0;
                    if($datos_reservation){
                        // Si la petición es pública a través de un canal de venta
                        if (strpos($request->path(), 'insurance/search') !== false) {
                            if ($user_id != '') {
                                if ($datos_reservation->client_id != $user_id) {
                                    $error_aux = 1;
                                }
                            } elseif ($client_session != '') {
                                if ($datos_reservation->client_session != $client_session) {
                                    $error_aux = 1;
                                }
                            }
                        } else { // Si la petición procede del POS
                            if (!$datos_permisos) {
                                if ($datos_reservation->subchannel_id != $subchannel_id) {
                                    $error_aux = 1;
                                }
                            }
                        }
                        // FIN Validación por permisos
                    } else {
                        $error_aux = 1;
                    }
                    // Validación por estado

                    $sql_estado_carro = DB::connection('tenant')->table('mo_reservation_detail')
                        ->whereNull('mo_reservation_detail.deleted_at')
                        ->where('mo_reservation_detail.operation_status_id', '!=', 1)
                        ->where('mo_reservation_detail.operation_status_id', '!=', 2)
                        ->where('mo_reservation_detail.operation_status_id', '!=', 7)
                        ->where('mo_reservation_detail.reservation_id', $request->get('id'));
                    $datos_estado_carro = $sql_estado_carro->first();
                    if ($datos_estado_carro) {
                        $error_aux = 1;
                    }
                    // FIN Validación por estado

                    if ($error_aux == 1) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The reservation can not be processed']]);
                    }

                    if ($error == 1) {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = $mensaje_validador;
                    } else {

                        insurance($request, $emulated_subchannel_id, $reserva_agent_id);
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Seguro');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que desasigna un seguro a la reserva
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unassignInsurance(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'emulated_subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'insurance_id' => 'required|integer|min:0|exists:tenant.mo_insurance,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $user_id = '';
            $client_session = '';
            $subchannel_id = '';
            $emulated_subchannel_id = '';

            // Si la petición es pública a través de un canal de venta
            if (strpos($request->path(), 'insurance/search') !== false) {

                // Si el usuario está logueado
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

                } elseif ($request->get('client_session') != '') { // Si no está logueado
                    $client_session = $request->get('client_session');

                } else { // Si no está logueado y no aporta identificación temporal
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token or session is required']]);
                }

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS

                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'id' => 'exists:tenant.mo_reservation,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                    //Fin validación

                } else {

                    // Preparación de datos
                    $reserva_client_id = null;
                    $reserva_client_session = null;
                    $reserva_agent_id = null;
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'insurance/search') !== false) {
                        if ($user_id != '') {
                            $reserva_client_id = $user_id;
                        } else {
                            $reserva_client_session = Str::random('200');

                        }
                    } else { // Si la petición procede del POS
                        $reserva_agent_id = $user_id;
                    }
                    // FIN preparación de datos

                    $sql_reservation = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id',
                            'mo_reservation.client_id',
                            'mo_reservation.client_session',
                            'mo_reservation.subchannel_id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                        ->whereNull('mo_reservation_detail.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'));

                    $datos_reservation = $sql_reservation->first();

                    $error_aux = 0;
                    // Validación por permisos

                    if($datos_reservation) {
                        // Si la petición es pública a través de un canal de venta
                        if (strpos($request->path(), 'insurance/search') !== false) {
                            if ($user_id != '') {
                                if ($datos_reservation->client_id != $user_id) {
                                    $error_aux = 1;
                                }
                            } elseif ($client_session != '') {
                                if ($datos_reservation->client_session != $client_session) {
                                    $error_aux = 1;
                                }
                            }
                        } else { // Si la petición procede del POS
                            if (!$datos_permisos) {
                                if ($datos_reservation->subchannel_id != $subchannel_id) {
                                    $error_aux = 1;
                                }
                            }
                        }
                        // FIN Validación por permisos
                    } else {
                        $error_aux = 1;
                    }


                    // Validación por estado
                    $sql_estado_carro = DB::connection('tenant')->table('mo_reservation_detail')
                        ->whereNull('mo_reservation_detail.deleted_at')
                        ->where('mo_reservation_detail.operation_status_id', '!=', 1)
                        ->where('mo_reservation_detail.operation_status_id', '!=', 2)
                        ->where('mo_reservation_detail.operation_status_id', '!=', 7)
                        ->where('mo_reservation_detail.reservation_id', $request->get('id'));
                    $datos_estado_carro = $sql_estado_carro->first();
                    if ($datos_estado_carro) {
                        $error_aux = 1;
                    }
                    // FIN Validación por estado

                    if ($error_aux == 1) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The reservation can not be processed']]);
                    }

                    if ($error == 1) {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = $mensaje_validador;
                    } else {

                        //Borrado del seguro en la reserva especificada
                        $reservation_insurance = ReservationInsurance::where('reservation_id', $request->get('id'))
                            ->where('insurance_id', $request->get('insurance_id'))->first();
                        if ($reservation_insurance) {
                            $reservation_insurance->delete();
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Seguro');
        }

        return response()->json($array, $array['error']);
    }

}