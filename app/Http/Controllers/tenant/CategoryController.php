<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\CategoryFile;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use App\Models\tenant\ProductCategory;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Category;
use App\Models\tenant\CategoryTranslation;
use App\Models\tenant\Language;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * Función para la creación de una categoría y sus traducciones
     *
     * Para la creación de categorías se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'order' => 'integer|min:0',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;
                    
                $rule_codes = [];

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();
            
                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_category = $rules->where('code', 'product_category');

                            if($rules_category) {
                                foreach($rules_category as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'product_category') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'product_category';
                    } else if ($cantidad > 0) {
                        $total_categories = Category::get();

                        if(count($total_categories) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'product_category';
                        }
                    }

                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    // Crear la categoría.
                    $categoria = Category::create([
                        'order' => $request->get('order') != '' ? $request->get('order') : 0,
                    ]);
                    // FIN crear la categoría

                    // Crear las traducciones para la categoría
                    foreach ($array_traducciones as $idioma) {

                        //Preparación de friendly_url
                        $friendly = null;
                        if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                            $friendly = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                        } else {
                            $friendly = Str::slug($request->get('name')[$idioma->abbreviation]);
                        }
                        //FIN preparación de friendly_url

                        CategoryTranslation::create(
                            [
                                'category_id' => $categoria->id,
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'friendly_url' => $friendly,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            ]
                        );
                        if ($request->get('files') != '') {
                            foreach ($request->get('files') as $file) {
                                CategoryFile::create([
                                    'category_id' => $categoria->id,
                                    'file_id' => $file,
                                ]);
                            }
                        }
                    }
                    // FIN crear las traducciones de la categoría
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Categorías');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de una categoría.
     *
     * Para la eliminación de categorías se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_category,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $category) {
                        $category = Category::where('id', $category)->first();
                        $category->productCategoryCat()->delete();
                        $category->categoryTranslation()->delete();
                        $category ->delete();
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Categorías');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una categoría.
     *
     * Para la modificación de categorías se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
                'order' => 'integer|min:0',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $categoria = Category::find($request->get('id'));
                if (!$categoria) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar categoría
                    $categoria->order = $request->get('order') != '' ? $request->get('order') : 0;

                    $categoria->save();
                    //FIN actualizar la categoría

                    // Actualizar traducciones de la categoría
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        // Preparación de datos
                        $friendly_url = null;

                        if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                            $friendly_url = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                        } else {
                            $friendly_url = Str::slug($request->get('name')[$idioma->abbreviation]);
                        }
                        //

                        $traduccion = $categoria->categoryTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {

                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'friendly_url' => $friendly_url,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            ]);
                        } else {

                            CategoryTranslation::create([
                                'category_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'friendly_url' => $friendly_url,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            ]);
                        }
                    }
                    $categoria->categoryTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de la categoría

                    $categoria->categoryFile()->delete();
                    //Crear relación con archivo
                    if ($request->get('files') != '') {
                        foreach ($request->get('files') as $file) {
                            CategoryFile::create([
                                'category_id' => $categoria->id,
                                'file_id' => $file,
                            ]);
                        }
                    }
                    //FIN crear relación con archivo

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Categorías');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Funcion que lista todas las categorías
     *
     * Para la consulta de categorías se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;


        try {


            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
                'product_id' => 'integer|min:0'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Se preparan las consultas
                $categorias = DB::connection('tenant')->table('mo_category')
                    ->select('mo_category.id', 'mo_category.order')
                    ->join('mo_category_translation', 'mo_category.id', '=', 'mo_category_translation.category_id')
                    ->where('mo_category_translation.deleted_at', '=', null)
                    ->where('mo_category.deleted_at', '=', null)
                    ->leftJoin('mo_product_category', function ($join) {
                        $join->on('mo_category.id', '=', 'mo_product_category.category_id')->where('mo_product_category.deleted_at', null);
                    })
                    ->leftjoin('mo_product', function ($join) {
                        $join->on('mo_product_category.product_id', '=', 'mo_product.id')->where('mo_product.deleted_at', null);
                    });


                $sub = DB::connection('tenant')->table('mo_category')
                    ->select('mo_category.id')
                    ->join('mo_category_translation', 'mo_category.id', '=', 'mo_category_translation.category_id')
                    ->where('mo_category_translation.deleted_at', '=', null)
                    ->where('mo_category.deleted_at', '=', null)
                    ->leftJoin('mo_product_category', function ($join) {
                        $join->on('mo_category.id', '=', 'mo_product_category.category_id')->where('mo_product_category.deleted_at', null);
                    })
                    ->leftjoin('mo_product', function ($join) {
                        $join->on('mo_product_category.product_id', '=', 'mo_product.id')->where('mo_product.deleted_at', null);
                    });



                // Recogida de datos enviados por el usuario y establecimiento de filtros
                if ($request->get('lang') != '') {
                    $categorias->where('language_id', '=', $idioma->id);
                    $sub->where('language_id', '=', $idioma->id);
                }

                if ($request->get('name') != '') {

                    $categorias->Where(function ($query) use ($request) {
                        $query->where('mo_category_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_category_translation.description', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_category_translation.short_description', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_category_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_category_translation.description', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_category_translation.short_description', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('product_id') != '') {
                    $categorias->where('mo_product_category.product_id', '=', $request->get('product_id'))->where('mo_product.id', $request->get('product_id'));
                    $sub->where('mo_product_category.product_id', '=', $request->get('product_id'))->where('mo_product.id', $request->get('product_id'));
                }

                if ($request->get('friendly_url') != '') {
                    $categorias->where('mo_category_translation.friendly_url', '=',$request->get('friendly_url'));
                    $sub->where('mo_category_translation.friendly_url', '=', $request->get('friendly_url'));
                }


                // Order
                $orden = 'mo_category_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_category_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_category_translation.description';
                            break;
                        case 'short_description':
                            $orden = 'mo_category_translation.short_description';
                            break;
                        case 'order':
                            $orden = 'mo_category.order';
                            break;
                        case 'id':
                            $orden = 'mo_category.id';
                            break;
                        default:
                            $orden = 'mo_category_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way


                $categorias->groupBy('mo_category.id')->orderBy($orden, $sentido);


                // Paginación segun filtros y ejecución consulta

                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $categorias = $categorias->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $categorias = $categorias ->get();

                }
                // FIN Paginación


                $sub->groupBy('mo_category_translation.category_id');
                $categorias_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $categorias_count->count();


                //ruta de storage de settings para concatenar a ruta de archivo de categoria
                $storage = Settings::where('name', 'storage_files')->first()->value;


                $array['data'] = array();
                foreach ($categorias as $categoria) {

                    foreach ($idiomas as $idi) {
                        $traduccion = CategoryTranslation::where('category_id', '=', $categoria->id)
                            ->select('id', 'language_id', 'name', 'description', 'short_description', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $categoria->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $files = DB::connection('tenant')->table('mo_category_file')
                        ->select('mo_file.id', 'mo_file.file_name','mo_file.file_size','mo_file.file_dimensions','mo_file.mimetype','mo_file.type_id', 'mo_file.order', 'mo_file.url_file')
                        ->where('mo_category_file.category_id', '=', $categoria->id)
                        ->where('mo_category_file.deleted_at', '=', null)
                        ->join('mo_file', 'mo_file.id', '=', 'mo_category_file.file_id')
                        ->where('mo_file.deleted_at', '=', null)
                        ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                        ->where('mo_file_translation.deleted_at', null)
                        ->orderBy('mo_file.order','desc')
                        ->groupBy('mo_file.id')->get();

                    $categoria->files = array();
                    $array_file = array();
                    foreach ($files as $file) {
                        //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                        $tipo = FileType::find($file->type_id);


                        $array_file = ['id' => $file->id, 'file_name' => $file->file_name,'file_size' => convertExtension($file->file_size),
                            'file_dimensions' => $file->file_dimensions,'mimetype' => $file->mimetype,'order' => $file->order, 'url_file' => $storage . $file->url_file,
                            'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];


                        foreach ($idiomas as $idi) {

                            //si el tipo no existe o está borrado no busca sus traducciones
                            if ($tipo) {
                                $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                    ->select('id', 'language_id', 'name')
                                    ->where('language_id', '=', $idi->id)
                                    ->groupBy('mo_file_type_translation.type_id')
                                    ->get();

                                foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                    $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                                }
                            }

                            $traducciones_files = FileTranslation::where('file_id', $file->id)
                                ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_translation.file_id')
                                ->get();

                            foreach ($traducciones_files as $traduccion_file) {
                                $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                            }
                        }
                        $categoria->files[] = $array_file;
                    }

                    $array['data'][0]['category'][] = $categoria;
                }

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Categorías');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función que muestra una categoría buscada por su id
     *
     * Para la consulta de una categoría se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();


            $categorias = DB::connection('tenant')->table('mo_category')
                ->select('mo_category.id', 'mo_category.order')
                ->join('mo_category_translation', 'mo_category.id', '=', 'mo_category_translation.category_id')
                ->where('mo_category_translation.deleted_at', '=', null)
                ->where('mo_category.deleted_at', '=', null);

            $categorias->where('mo_category.id', '=', $id);

            $categorias->groupBy('mo_category.id');
            $categorias = $categorias->get();

            //ruta de storage de settings para concatenar a ruta de archivo de categoria
            $storage = Settings::where('name', 'storage_files')->first()->value;

            $array['data'] = array();

            foreach ($categorias as $cat) {

                $files = DB::connection('tenant')->table('mo_category_file')
                    ->select('mo_file.id', 'mo_file.file_name','mo_file.file_size','mo_file.file_dimensions','mo_file.mimetype','mo_file.type_id', 'mo_file.order', 'mo_file.url_file','mo_file.device_id','mo_file.key_file')
                    ->where('mo_category_file.category_id', '=', $cat->id)
                    ->where('mo_category_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_category_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->groupBy('mo_file.id')->get();

                $cat->files = array();
                $array_file = array();
                foreach ($files as $file) {
                    //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                    $tipo = FileType::find($file->type_id);

                    $array_file = ['id' => $file->id, 'file_name' => $file->file_name,'file_size' => convertExtension($file->file_size),
                        'file_dimensions' => $file->file_dimensions,'mimetype' => $file->mimetype,'order' => $file->order, 'url_file' => $storage . $file->url_file,
                        'device_id' => $file->device_id,'key_file' => $file->key_file, 'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];
                    foreach ($idiomas as $idi) {

                        //si el tipo no existe o está borrado no busca sus traducciones
                        if ($tipo) {
                            $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_type_translation.type_id')
                                ->get();

                            foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                            }
                        }

                        $traducciones_files = FileTranslation::where('file_id', $file->id)
                            ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                            ->where('language_id', '=', $idi->id)
                            ->groupBy('mo_file_translation.file_id')
                            ->get();

                        foreach ($traducciones_files as $traduccion_file) {
                            $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                        }
                    }
                    $cat->files[] = $array_file;
                }

                foreach ($idiomas as $idi) {
                    $traduccion = CategoryTranslation::where('category_id', '=', $cat->id)
                        ->select('id', 'language_id', 'name', 'description', 'short_description', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo')
                        ->where('language_id', '=', $idi->id)
                        ->get();
                    foreach ($traduccion as $trad) {
                        $cat->lang[][$idi->abbreviation] = $trad;
                    }
                }

                $array['data'][0]['category'][] = $cat;
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Categorías');
        }

        return response()->json($array, $array['error']);

    }


    /**
     * Función que asigna categorias a productos
     *
     * Para asignar categorias a un producto se realiza un petición POST.
     * Borra todas las relaciones existentes del producto con las categorias y crea las nuevas relaciones segun las categorias que se le especifica.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'product_id' => 'required|numeric',
                'categories' => 'array',
                'categories.*' => 'required|numeric|exists:tenant.mo_category,id,deleted_at,NULL|distinct',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin de validación

            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'product_id' => 'exists:tenant.mo_product,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    //Busca y borra todas las categorias asociadas al producto
                    ProductCategory::where('product_id', '=', $request->get('product_id'))->delete();

                    //Si recibe parámetro categories recorre el array y crea la relación con el producto
                    if ($request->get('categories')) {
                        foreach ($request->get('categories') as $category) {
                            ProductCategory::create([
                                'product_id' => $request->get('product_id'),
                                'category_id' => $category,
                            ]);
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Categorías');
        }

        return response()->json($array, $array['error']);
    }

}
