<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Settings;
use App\Models\tenant\Company;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CompanyController extends Controller
{


    /**
     * Función para mostrar las compañias existentes.
     *
     * Para la consulta de compañías se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',

            ]);
            
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $companies = DB::connection('tenant')->table('mo_company')
                    ->select('mo_company.id', 'mo_company.name')
                    ->where('mo_company.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_company')
                    ->select('mo_company.id')
                    ->where('mo_company.deleted_at', '=', null);

                if ($request->get('name') != '') {
                    $companies->where('mo_company.name', 'Like', '%' . $request->get('name') . '%');
                    $sub->where('mo_company.name', 'Like', '%' . $request->get('name') . '%');
                }

                // Order
                $orden = 'name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'id';
                            break;
                        default:
                            $orden = 'name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $companies = $companies->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_companies = $companies->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_companies = $companies->get();

                }

                //Fin de paginación

                //Total de resultados

                $companies_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $companies_count->count();

                //Generación de salida

                $array['data'] = array();

                foreach ($datos_companies as $company) {

                    $array['data'][0]['company'][] = ['id' => $company->id, 'name' => $company->name];

                }

                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Compañias');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la creación de compañias
     *
     * Para la creación de compañias se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {
            
            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                $rule_codes = [];

                if($heimdall_middleware == '1') {
                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_company = $rules->where('code', 'product_company');

                            if($rules_company) {
                                foreach($rules_company as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'product_company') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if ($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'product_company';
                    } else if ($cantidad > 0) {
                        $total_companies = Company::get();

                        if(count($total_companies) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'product_company';
                        }
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    // Crear compañía
                    $supplier = Company::create([
                        'name' => $request->get('name') != '' ? $request->get('name') : null,
                    ]);
                    // FIN crear compañía
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Compañías');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de compañías.
     *
     * Para la eliminación de compañías se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_company,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $company_id) {
                        $company = Company::where('id', $company_id)->first();
                        // borrado de compañía
                        $company->delete();
                        //fin compañía

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Compañías');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una compañía.
     *
     * Para la modificación de una compañía se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $company = Company::find($request->get('id'));
                if (!$company) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar compañía
                    $company->name = $request->get('name') != '' ? $request->get('name') : null;

                    $company->save();
                    //FIN actualizar compañía

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Compañías');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra una compañía buscada por su id
     *
     * Para la consulta de una compañía se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $datos_company = DB::connection('tenant')->table('mo_company')
            ->select(
                'mo_company.id',
                'mo_company.name'
            )
            ->where('mo_company.deleted_at', '=', null)
            ->where('mo_company.id', '=', $id);

            $datos_company = $datos_company->get();

            $array['data'] = array();
            foreach ($datos_company as $company) {
                
                $array['data'][0]['company'][] = $company;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Compañías');
        }

        return response()->json($array, $array['error']);

    }

}