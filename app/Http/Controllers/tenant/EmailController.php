<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Logg;
use App\Mail\Email;
use App\Models\tenant\ReservationEmailTemplate;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Models\tenant\Template;
use App\Models\tenant\File;
use Illuminate\Support\Facades\Storage;
use App\Models\SaasSettings;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use function PHPSTORM_META\elementType;
use Illuminate\Support\Str;

class EmailController extends Controller
{


    /**
     * Método para el envío de emails
     *
     * Son requeridos dirección de email donde se envía, título de la página html que tendrá el email, cuerpo del mensaje, nombre de la plantilla que se envía "información" o "error" y mailbox
     * que contiene el nombre del buzón desde donde se envía el email, dirección de email desde donde se envía, nombre de quien envía el email, y su configuración de host, username, password, encryption, driver y port.
     * No requerido es el parámetro archivos adjuntos que contiene la url donde está el archivo adjuntado.
     * Para el envío de emails se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        $array['error'] = 200;
        try {

            $host = '';
            $username = '';
            $password = '';
            $encryption = '';
            $driver = '';
            $port = '';
            $from_email = '';
            $from_name = '';

            DB::connection('tenant')->beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'emails' => 'required|array',
                'emails.*.email' => 'required|email',
                'emails.*.subject_email' => 'required',
                'title' => 'required',
                'attachments' => 'array',
                'template' => 'exists:tenant.mo_email_template,name,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('template') != '' && !\File::exists(resource_path('views/emails/tenant/' . $request->get('template') . '.blade.php'))) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['template' => ['Template not exists']]);
            }

            if ($request->get('mailbox') != '') {
                $host = Settings::where('name', '=', 'email_host_' . $request->get('mailbox'))->first();
                $username = Settings::where('name', '=', 'email_username_' . $request->get('mailbox'))->first();
                $password = Settings::where('name', '=', 'email_password_' . $request->get('mailbox'))->first();
                $encryption = Settings::where('name', '=', 'email_encryption_' . $request->get('mailbox'))->first();
                $driver = Settings::where('name', '=', 'email_driver_' . $request->get('mailbox'))->first();
                $port = Settings::where('name', '=', 'email_port_' . $request->get('mailbox'))->first();
                $from_email = Settings::where('name', 'email_from_' . $request->get('mailbox'))->first();
                $from_name = Settings::where('name', 'email_fromname_' . $request->get('mailbox'))->first();
            } else {
                $host = Settings::where('name', '=', 'email_host_default')->first();
                $username = Settings::where('name', '=', 'email_username_default')->first();
                $password = Settings::where('name', '=', 'email_password_default')->first();
                $encryption = Settings::where('name', '=', 'email_encryption_default')->first();
                $driver = Settings::where('name', '=', 'email_driver_default')->first();
                $port = Settings::where('name', '=', 'email_port_default')->first();
                $from_email = Settings::where('name', 'email_from_default')->first();
                $from_name = Settings::where('name', 'email_fromname_default')->first();
            }

            if (($host == '') || ($username == '') || ($password == '') || ($encryption == '') || ($driver == '') || ($port == '') || ($from_email == '') || ($from_name == '')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['mailbox' => ['Mailbox not available']]);
            } else {
                $host = $host->value;
                $username = $username->value;
                $password = $password->value;
                $encryption = $encryption->value;
                $driver = $driver->value;
                $port = $port->value;
                $from_email = $from_email->value;
                $from_name = $from_name->value;
            }

            //Fin validacion

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                config([
                    'mail.driver' => $driver,
                    'mail.host' => $host,
                    'mail.port' => $port,
                    'mail.username' => $username,
                    'mail.password' => $password,
                    'mail.encryption' => $encryption,
                ]);

                $template = '';
                if ($request->get('template') != '') {
                    $template = $request->get('template');
                } else {
                    $template = Template::where('name', '=', Settings::where('name', '=', 'email_template_default')->first()->value)->first()->file_name;
                }

                //Logo
                $client_logo_id = Settings::where('name','=','logo_image_id')->first()->value;
                if($client_logo_id && $client_logo_id != ''){
                    $client_logo = File::where('mo_file.id', '=', $client_logo_id)->first();
                    if($client_logo && $client_logo->url_file){
                        $client_logo_url = Storage::url($client_logo->url_file);
                    }else{
                        $client_logo_url = '';
                    }
                }else{
                    $client_logo_url = '';
                }

                //Redes sociales
                $sql_social_network = DB::connection('tenant')->table('mo_web_social_network')
                ->select(
                    'mo_web_social_network.social_network',
                    'mo_web_social_network.external_url'
                )->whereNull('mo_web_social_network.deleted_at');
                
                $social_network = array();

                $datos_social_network = $sql_social_network->get();

                foreach($datos_social_network as $socialnetwork) {

                    $array_social_network = array();

                    $array_social_network['social_network'] = $socialnetwork->social_network;
                    $array_social_network['external_url'] = $socialnetwork->external_url ? $socialnetwork->external_url : '';

                    $social_network[] = $array_social_network;
                }
                //FIN redes sociales

                //Legales

                $idiomas = Language::where('front',1)->get();
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->first();
    
                $website = \Hyn\Tenancy\Facades\TenancyFacade::website();
                $front_url = '';
                if($website){
                    $website = DB::table('saas_website')
                            ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                            'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                            'saas_website.default_language_id', 'saas_website.default_currency_id')
                            ->whereNull('saas_website.deleted_at')
                            ->where('saas_website.website_id', $website->id)
                            ->first();
                    $website_domain = $website->domain;
                    $protocol = SaasSettings::where('name','=','protocol')->first()->value;
                    $base_path_system = SaasSettings::where('name','=','base_path_system')->first()->value;
                    $front_url = $protocol.$website_domain.'.'.$base_path_system.'/'.Str::lower($idioma->abbreviation);

                }else{
                    $front_url = 'urlfooterpruebastestmtr';
                }

                // Recogida de url de archivos del email
                $file_storage_url = Settings::where('name', 'storage_files')->first()->value . 'mtr/public/email/common/';
                foreach ($request->get('emails') as $email) {

                    $array_datos = [
                        'from_email' => $from_email,
                        'from_name' => $from_name,
                        'subject_email' => $email['subject_email'],
                        'template' => 'emails.tenant.' . $template,
                        'title' => $request->get('title'),
                        'body' => $request->get('body'),
                        'storage_files' => $file_storage_url,
                        'adjuntos' => $request->get('attachments'),
                        'client_logo' => $client_logo_url,
                        'social_media' => $social_network,
                        'front_url' => $front_url,
                    ];

                    Mail::to($email['email'])
                        ->bcc(Settings::where('name', 'email_copia_sistema')->first()->value)
                        ->queue(new Email($array_datos));

                }




                //log
                if (isset($array_datos['adjuntos'])) {
                    $adjuntos = '';
                    for ($i = 0; $i < count($array_datos['adjuntos']); $i++) {
                        $adjuntos .= $array_datos['adjuntos'][$i] . '   ';
                    }
                }
                //guarda un registro por cada dirección a la que envía
                foreach ($request->get('emails') as $email) {
                    Logg::create([
                        'date' => date('Y-m-d H:i:s'),
                        'from' => $from_email,
                        'to' => $email['email'],
                        'cc' => null,
                        'bcc' => null,
                        'subject' => $email['subject_email'],
                        'body' => $request->get('body'),
                        'headers' => null,
                        'attachments' => (isset($adjuntos)) ? $adjuntos : null,
                        'description_error' => null,
                        'received_email' => 1,
                    ]);
                }
                //fin log
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage() . $e->getLine();
            //log
            if (isset($array_datos['adjuntos'])) {
                $adjuntos = '';
                for ($i = 0; $i < count($array_datos['adjuntos']); $i++) {
                    $adjuntos .= $array_datos['adjuntos'][$i] . '   ';
                }
            }
            //guarda un registro por cada dirección a la que envía
            foreach ($request->get('emails') as $email) {
                Logg::create([
                    'date' => date('Y-m-d H:i:s'),
                    'from' => $from_email,
                    'to' => $email['email'],
                    'cc' => null,
                    'bcc' => null,
                    'subject' => $email['subject_email'],
                    'body' => $request->get('body'),
                    'headers' => null,
                    'attachments' => (isset($adjuntos)) ? $adjuntos : null,
                    'description_error' => utf8_encode($e->getMessage() . ' ' . $e->getLine()),
                    'received_email' => 0,
                ]);
            }
            //fin log
            
            reportService($e, 'Emails');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función que lista las plantillas de email disponibles
     * Para el listado de plantillas se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTemplate()
    {
        $array['error'] = 200;
        try {

            DB::connection('tenant')->beginTransaction();

            $templates = Template::all(['id', 'name', 'description', 'file_name']);
            $totales = count($templates);

            $array['data'] = array();

            $array['data'][0]['email'][0]['template'] = $templates;
            $array['total_results'] = $totales;

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            
            reportService($e, 'Emails');
        }

        return response()->json($array, $array['error']);
    }

}