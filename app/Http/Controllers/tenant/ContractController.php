<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\ContractModel;
use App\Models\tenant\ContractModelPenalization;
use App\Models\tenant\ContractModelProduct;
use App\Exceptions\Handler;
use App\Models\tenant\ProductTranslation;
use App\Models\tenant\ProductTypeTranslation;
use App\Models\tenant\ServiceTranslation;
use App\Models\tenant\Settings;
use Illuminate\Support\Str;
use Validator;
use App\Models\tenant\Language;
use App\Models\tenant\ContractTranslation;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ContractController extends Controller
{

    /**
     * Función que lista todos los contratos
     *
     * Para la consulta de contratos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showContracts(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $contracts = DB::connection('tenant')->table('mo_contract_model')
                    ->select('mo_contract_model.id')
                    ->where('mo_contract_model.deleted_at', '=', null)
                    ->join('mo_contract_model_translation', 'mo_contract_model.id', 'mo_contract_model_translation.contract_model_id')
                    ->where('mo_contract_model_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_contract_model')
                    ->select('mo_contract_model.id')
                    ->where('mo_contract_model.deleted_at', '=', null)
                    ->join('mo_contract_model_translation', 'mo_contract_model.id', 'mo_contract_model_translation.contract_model_id')
                    ->where('mo_contract_model_translation.deleted_at', '=', null);
                //Fin precompiladas


                //Filtros de busqueda
                if ($request->get('lang') != '') {
                    $contracts->where('mo_contract_model_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_contract_model_translation.language_id', '=', $idioma->id);
                }
                //Fin de filtros

                // Order
                $orden = 'mo_contract_model.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_contract_model.id';
                            break;
                        case 'name':
                            $orden = 'mo_contract_model_translation.name';
                            break;
                        default:
                            $orden = 'mo_contract_model.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $contracts = $contracts->groupBy('mo_contract_model.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $contracts = $contracts->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $contracts = $contracts->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_contract_model_translation.contract_model_id');
                $contracts_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $contracts_count->count();


                $array['data'] = array();


                //Se recorren los contratos
                foreach ($contracts as $contract) {

                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de los contratos
                        $traduccion = ContractTranslation::where('contract_model_id', '=', $contract->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name']);
                        foreach ($traduccion as $trad) {
                            $contract->lang[][$idi->abbreviation] = $trad;
                        }

                    }

                    $array['data'][0]['contract'][] = $contract;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contratos');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar los campos de configuración de un contracto.
     *
     * Para la modificación de los campos de configuración de un contracto se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function updateSettingsContracts(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'time_lock_avail' => 'integer|min:0',
                'products' => 'array',
                'products.*.id' => 'required|distinct|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
                'products.*.time_expiration' => 'integer|min:0|required_without_all:products.*.time_limit_enjoyment,products.*.apply_cancel_penalization,products.*.individual_sale',
                'products.*.time_limit_enjoyment' => 'integer|min:0|required_without_all:products.*.time_expiration,products.*.apply_cancel_penalization,products.*.individual_sale',
                'products.*.apply_cancel_penalization' => 'integer|min:0',
                'products.*.penalizations.*.days' => 'integer|min:0|required_if:products.*.apply_cancel_penalization,1',
                'products.*.penalizations.*.penalization_amount' => 'numeric|min:0|required_if:products.*.apply_cancel_penalization,1',
                'products.*.individual_sale' => 'boolean|required_without_all:products.*.time_limit_enjoyment,products.*.apply_cancel_penalization,products.*.time_expiration',
                'products.*.required_service' => 'boolean',
                'products.*.services' => 'array',
                'products.*.services.*.id' => 'required|integer|min:0|exists:tenant.mo_service,id,deleted_at,NULL',
                'products.*.services.*.time_expiration' => 'integer|min:0|required_without_all:products.*.services.*.time_limit_enjoyment,products.*.services.*.apply_cancel_penalization,products.*.services.*.individual_sale',
                'products.*.services.*.time_limit_enjoyment' => 'integer|min:0|required_without_all:products.*.services.*.time_expiration,products.*.services.*.apply_cancel_penalization,products.*.services.*.individual_sale',
                'products.*.services.*.apply_cancel_penalization' => 'integer|min:0',
                'products.*.services.*.penalizations.*.days' => 'integer|min:0|required_if:products.*.services.*.apply_cancel_penalization,1',
                'products.*.services.*.penalizations.*.penalization_amount' => 'numeric|min:0|required_if:products.*.services.*.apply_cancel_penalization,1',
                'products.*.services.*.individual_sale' => 'boolean|required_without_all:products.*.services.*.time_limit_enjoyment,products.*.services.*.apply_cancel_penalization,products.*.services.*.time_expiration',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $flag_error_changed = false;
            $request_products = $request->get('products');
            if(isset($request_products) && is_array($request_products) && $error == 0){
                foreach ($request_products as $key=>$product){

                    if (isset($product['apply_cancel_penalization']) && $product['apply_cancel_penalization'] == 1 && (!isset($product['penalizations'])) ) {
                        if($flag_error_changed == false){
                            $error = 1;
                            $flag_error_changed = true;
                        }
                        $mensaje_validador = $mensaje_validador->merge(['products.'.$key.'.apply_cancel_penalization' => ['If apply_cancel_penalization is 1, the penalization fields must be filled']]);

                    }
                }
            }

            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $contrato = ContractModel::find($request->get('id'));
                if (!$contrato) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar contrato
                    $contrato->time_lock_avail = $request->get('time_lock_avail') != '' ? $request->get('time_lock_avail') : null;
                    $contrato->save();
                    //FIN actualizar contrato

                    //borra relaciones con productos
                    $contrato->contractModelProduct()->delete();

                    //asociación de contrato a producto y sus configuraciones
                    if ($request->get('products') != '') {
                        foreach ($request->get('products') as $product){
                            //dd($product);
                            $contract_model_product = ContractModelProduct::create([
                                'contract_model_id' => $contrato->id,
                                'product_id' => $product['id'],
                                'time_expiration' => (isset($product['time_expiration'])) != '' ? $product['time_expiration'] : null,
                                'time_limit_enjoyment' => (isset($product['time_limit_enjoyment'])) != '' ? $product['time_limit_enjoyment'] : null,
                                'apply_cancel_penalization' => (isset($product['apply_cancel_penalization'])) != '' ? $product['apply_cancel_penalization'] : 0,
                                'individual_sale' => (isset($product['individual_sale'])) != '' ? $product['individual_sale'] : 0,
                                'required_service' => (isset($product['required_service'])) != '' ? $product['required_service'] : 0,
                            ]);

                            if(isset($product['apply_cancel_penalization']) && $product['apply_cancel_penalization'] == 1 && isset($product['penalizations']) ){
                                foreach ($product['penalizations'] as $penalization) {
                                    ContractModelPenalization::create([
                                        'contract_model_product_id' => $contract_model_product->id,
                                        'days' => $penalization['days'],
                                        'penalization_amount' => $penalization['penalization_amount'],
                                    ]);
                                }
                            }

                            if(isset($product['services']) && $product['services'] !=''){
                                foreach($product['services'] as $service){
                                    $contract_model_product_servicio = ContractModelProduct::create([
                                        'contract_model_id' => $contrato->id,
                                        'product_id' => $product['id'],
                                        'service_id' => $service['id'],
                                        'time_expiration' => (isset($service['time_expiration'])) != '' ? $service['time_expiration'] : null,
                                        'time_limit_enjoyment' => (isset($service['time_limit_enjoyment'])) != '' ? $service['time_limit_enjoyment'] : null,
                                        'apply_cancel_penalization' => (isset($service['apply_cancel_penalization'])) != '' ? $service['apply_cancel_penalization'] : 0,
                                        'individual_sale' => (isset($service['individual_sale'])) != '' ? $service['individual_sale'] : 0,
                                    ]);
                                    if(isset($service['apply_cancel_penalization']) && $service['apply_cancel_penalization'] == 1 && isset($service['penalizations'])){
                                        foreach($service['penalizations'] as $penalization) {
                                            ContractModelPenalization::create([
                                                'contract_model_product_id' => $contract_model_product_servicio->id,
                                                'days' => $penalization['days'],
                                                'penalization_amount' => $penalization['penalization_amount'],
                                            ]);
                                        }
                                    }

                                }
                            }

                        }
                    }
                    //fin asociación de contrato a producto y sus configuraciones
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contratos');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra un contrato buscado por su id, los diferentes productos asociados a ese contrato y los datos de configuración para cada uno de ellos
     *
     * Recibe un parámetro id en el path y muestra los datos del contrato solicitado, muestra todos los productos tengan o no tarifa y sus servicios asociados.
     * Para la consulta de un contrato se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showIdSettingsContracts($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $array['data'] = array();

            // Precompiladas
            $sql_productos = DB::connection('tenant')->table('mo_product')
                ->select('mo_product.id','mo_contract_model_product.time_expiration','mo_contract_model_product.time_limit_enjoyment',
                    'mo_contract_model_product.apply_cancel_penalization','mo_contract_model_product.individual_sale', 'mo_contract_model_product.required_service')
                ->whereNull('mo_product.deleted_at')
                ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                ->whereNull('mo_product_translation.deleted_at')
                ->leftJoin('mo_contract_model_product', function ($join) use ($id) {
                    $join->on('mo_product.id', '=', 'mo_contract_model_product.product_id')
                        ->where('mo_contract_model_product.contract_model_id',$id)
                        ->whereNull('mo_contract_model_product.deleted_at');
                })
                ->groupBy('mo_product.id');

            $sql_serviciosAsociados = DB::connection('tenant')->table('mo_service')
                ->select('mo_service.id', 'mo_service_translation.name')
                ->whereNull('mo_service.deleted_at')
                ->join('mo_service_translation', 'mo_service.id', '=', 'mo_service_translation.service_id')
                ->whereNull('mo_service_translation.deleted_at')
                ->join('mo_product_service', 'mo_product_service.service_id', 'mo_service.id')
                ->whereNull('mo_product_service.deleted_at')
                ->groupBy('mo_service_translation.service_id');

            $sql_types = DB::connection('tenant')->table('mo_product_type')
                ->select('mo_product_type.id', 'mo_product_type_translation.name')
                ->whereNull('mo_product_type.deleted_at')
                ->join('mo_product_type_translation', 'mo_product_type_translation.type_id', '=', 'mo_product_type.id')
                ->whereNull('mo_product_type_translation.deleted_at')
                ->groupBy('mo_product_type_translation.type_id');

            $sql_settings_service = DB::connection('tenant')->table('mo_contract_model_product')
                ->select('mo_contract_model_product.time_expiration','mo_contract_model_product.time_limit_enjoyment','mo_contract_model_product.apply_cancel_penalization','mo_contract_model_product.individual_sale')
                ->join('mo_contract_model','mo_contract_model.id','mo_contract_model_product.contract_model_id')
                ->whereNull('mo_contract_model.deleted_at')
                ->whereNull('mo_contract_model_product.deleted_at')
                ->where('mo_contract_model.id',$id);

            $sql_settings_service_penalization = DB::connection('tenant')->table('mo_contract_model_penalization')
                ->select('mo_contract_model_product.product_id', 'mo_contract_model_product.service_id', 'mo_contract_model_penalization.contract_model_product_id', 'mo_contract_model_penalization.days','mo_contract_model_penalization.penalization_amount')
                ->join('mo_contract_model_product','mo_contract_model_product.id','mo_contract_model_penalization.contract_model_product_id')
                ->whereNull('mo_contract_model_product.deleted_at')
                ->whereNull('mo_contract_model_penalization.deleted_at')
                ->where('mo_contract_model_product.contract_model_id',$id);



            $datos_idiomas = Language::where('front',1)->get();


            // FIN Precompiladas

            $contrato = ContractModel::where('id', $id)->first(['id', 'time_lock_avail']);
            if ($contrato) {
                $array_contrato = [
                    'id' => $contrato->id,
                    'time_lock_avail' => $contrato->time_lock_avail,
                ];

                //Se guardan los datos de la tarifa en el array
                $array['data'][0]['contract_model'][] = $array_contrato;

                // Se obtienen los tipos de producto
                $tipos = $sql_types->get();

                foreach ($tipos as $tipo) {

                    //Se obtiene todos los productos
                    $productos_clone = clone $sql_productos;
                    $datos_productos_clone = $productos_clone
                        ->where('mo_product.type_id', $tipo->id)
                        ->get();

                    $tipo->lang = array();

                    $array_type = array();
                    $array_type['id'] = $tipo->id;
                    foreach ($datos_idiomas as $idioma) {
                        // Se obtienen las traducciones del tipo de producto
                        $traduccion = ProductTypeTranslation::where('mo_product_type_translation.language_id', '=', $idioma->id)
                            ->where('mo_product_type_translation.type_id', '=', $tipo->id)
                            ->first();
                        if ($traduccion) {
                            $array_type["lang"][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                        }
                    }

                    $array_productos = array();
                    $array_producto = array();
                    foreach ($datos_productos_clone as $producto) {
                        $penalizations_product_clone = clone $sql_settings_service_penalization;
                        $array_producto['id'] = $producto->id;
                        $array_producto['lang'] = array();
                        // Se obtienen las traducciones del producto
                        foreach ($datos_idiomas as $idioma) {
                            $traduccion = ProductTranslation::where('mo_product_translation.language_id', '=', $idioma->id)
                                ->where('mo_product_translation.product_id', '=', $producto->id)
                                ->first();
                            if ($traduccion) {
                                $array_producto['lang'][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                            }
                        }

                        $array_settings = array();
                        //parametros de configuracion del contrato para ese producto
                        $array_settings = ['time_expiration' => $producto->time_expiration,'time_limit_enjoyment' => $producto->time_limit_enjoyment,
                            'apply_cancel_penalization' => $producto->apply_cancel_penalization,'individual_sale' => $producto->individual_sale, 'required_service' => $producto->required_service];

                        $array_settings['penalizations'] = $penalizations_product_clone
                            ->select('days','penalization_amount')
                            ->where('product_id','=', $producto->id)
                            ->whereNull('service_id')
                            ->get();


                        $array_producto['settings'] = [$array_settings];

                        // Se obtienen los servicios asociados a cada producto
                        $sql_serviciosAsociados_clone = clone $sql_serviciosAsociados;

                        $datos_serviciosAsociados_clone = $sql_serviciosAsociados_clone
                            ->where('mo_product_service.product_id', $producto->id)
                            ->get();

                        $array_producto['service'] = array();

                        //Se recorren los servicios asociados al producto
                        foreach ($datos_serviciosAsociados_clone as $servicio) {

                            $servicios_array = array();
                            $servicios_array['id'] = $servicio->id;

                            $servicios_array["lang"] = array();

                            $penalizations_service_clone = clone $sql_settings_service_penalization;

                            // Se obtienen las traducciones del servicio
                            foreach ($datos_idiomas as $idioma) {
                                $traduccion = ServiceTranslation::where('mo_service_translation.language_id', '=', $idioma->id)
                                    ->where('mo_service_translation.service_id', '=', $servicio->id)
                                    ->first();
                                if ($traduccion) {
                                    $servicios_array["lang"][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                                }
                            }

                            $sql_settings_service_clone = clone $sql_settings_service;

                            $datos_settings_servicios = $sql_settings_service_clone->where('mo_contract_model_product.product_id', $producto->id)
                                ->where('mo_contract_model_product.service_id',$servicio->id)->first();

                            $array_settings_servicios = array();

                            if($datos_settings_servicios){
                                $array_settings_servicios = ['time_expiration' => $datos_settings_servicios->time_expiration,'time_limit_enjoyment' => $datos_settings_servicios->time_limit_enjoyment,
                                    'apply_cancel_penalization' => $datos_settings_servicios->apply_cancel_penalization,'individual_sale' => $datos_settings_servicios->individual_sale];


                                $array_settings_servicios['penalizations'] = $penalizations_service_clone
                                    ->select('days','penalization_amount')
                                    ->where('product_id','=', $producto->id)
                                    ->where('service_id','=', $servicio->id)
                                    ->get();


                            }else{
                                $array_settings_servicios = ['time_expiration' => null,'time_limit_enjoyment' => null,
                                    'apply_cancel_penalization' => null,'individual_sale' => null];
                            }
                            $array_settings_servicios['penalizations'] = $penalizations_service_clone
                                ->select('days','penalization_amount')
                                ->where('product_id','=', $producto->id)
                                ->where('service_id','=', $servicio->id)
                                ->get();

                            $servicios_array['settings'] = array();

                            $servicios_array['settings'] = [$array_settings_servicios];

                            $array_producto['service'][] = $servicios_array;
                        }


                        $array_productos[] = $array_producto;
                    }

                    $array_type['product'] = $array_productos;
                    $array['data'][1]['type'][] = $array_type;
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contratos');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para la creación de un contrato y sus traducciones
     *
     * Para la creación de contratos se realiza una petición POST. Si la operación no produce errores se devuelve,
     * en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $error = 0;

            // Validación
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'time_lock_avail' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
          
            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //FIN de validación
            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                $rule_codes = [];

                if($heimdall_middleware == '1') {
                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_contract = $rules->where('code', 'contract');

                            if($rules_contract) {
                                foreach($rules_contract as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'contract') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'contract';
                    } else if ($cantidad > 0) {
                        $total_contracts = ContractModel::get();

                        if(count($total_contracts) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'contract';
                        }
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    //Crea el contrato
                    $contract = ContractModel::create([
                        'time_lock_avail' => $request->get('time_lock_avail'),
                    ]);

                    //Crea las traducciones del contrato
                    foreach ($array_traducciones as $idioma) {
                        ContractTranslation::create([
                            'contract_model_id' => $contract->id,
                            'language_id' => $idioma->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                        ]);
                    }
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contratos');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar un contrato.
     *
     * Para la modificación de contratos se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'time_lock_avail' => 'integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation])) {
                    $array_traducciones[] = $idioma;
                }

            }
            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $contract = ContractModel::where('id', '=', $request->get('id'))->first();
                if (!$contract) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    //Actualiza datos contrato
                    $contract->time_lock_avail = $request->get('time_lock_avail') != '' ? $request->get('time_lock_avail') : null;  

                    $contract->save();

                    // Actualizar traducciones del contrato
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        $traduccion = $contract->contractModelTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {
                            //Si existe traducción lo actualiza
                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                            ]);

                        } else {
                            //Si no existe traducción la crea
                            ContractTranslation::create([
                                'contract_model_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],

                            ]);
                        }
                    }
                    $contract->contractModelTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de contrato
                }
            }
            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contratos');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra un contrato buscado por su id
     *
     * Para la consulta de un contrato se realizará una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $sql_contract = DB::connection('tenant')->table('mo_contract_model')
                ->select('mo_contract_model.id', 'mo_contract_model.time_lock_avail')
                ->where('mo_contract_model.deleted_at', '=', null)
                ->join('mo_contract_model_translation', 'mo_contract_model_translation.contract_model_id', '=', 'mo_contract_model.id')
                ->where('mo_contract_model_translation.deleted_at', '=', null);

            $sql_contract->where('mo_contract_model.id', '=', $id);

            $sql_contract = $sql_contract->groupBy('mo_contract_model.id');
            $contract = $sql_contract->get();

            $array['data'] = array();
            foreach ($contract as $contr) {

                foreach ($idiomas as $idi) {
                    $traduccion = ContractTranslation::where('contract_model_id', '=', $contr->id)
                        ->select('id', 'language_id', 'name')
                        ->where('language_id', '=', $idi->id)
                        ->get();
                    foreach ($traduccion as $trad) {
                        $contr->lang[][$idi->abbreviation] = $trad;
                    }
                }

                $array['data'][0]['contract'][] = $contr;

            }


            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contratos');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de un contrato.
     *
     * Para la eliminación de contratos se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_contract_model,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $contract_id) {
                        $contract = ContractModel::where('id', $contract_id)->first();
                        $contract->contractModelTranslation()->delete();
                        $contract->delete();
                    }
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Contratos');
        }
        return response()->json($array, $array['error']);
    }

}