<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use App\Models\tenant\ProductTag;
use App\Models\tenant\Tag;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\TagTranslation;
use App\Models\tenant\Language;
use Illuminate\Http\Request;

class TagController extends Controller
{


    /**
     * Función para la creación de una etiqueta y sus traducciones
     *
     * Para la creación de etiquetas se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'order' => 'integer|min:0'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_tag = $rules->where('code', 'tag');

                            if($rules_tag) {
                                foreach($rules_tag as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'tag') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'tag';
                    } else if ($cantidad > 0) {
                        $total_tags = Tag::get();

                        if(count($total_tags) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'tag';
                        }
                    }

                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    // Crear la etiqueta.
                    $etiqueta = Tag::create([
                        'order' => $request->get('order') != '' ? $request->get('order') : 0,
                    ]);
                    // FIN crear la etiqueta

                    // Crear las traducciones para la etiqueta
                    foreach ($array_traducciones as $idioma) {

                        //Preparación de friendly_url
                        $friendly = null;
                        if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                            $friendly = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                        } else {
                            $friendly = Str::slug($request->get('name')[$idioma->abbreviation]);
                        }
                        //FIN preparación de friendly_url

                        TagTranslation::create(
                            [
                                'tag_id' => $etiqueta->id,
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'friendly_url' => $friendly,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            ]
                        );
                    }
                    // FIN crear las traducciones de la etiqueta
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Etiquetas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de una etiqueta.
     *
     * Para la eliminación de tags se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_tag,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $tag_id) {
                        $tag = Tag::where('id', $tag_id)->first();
                        // borrado de etiqueta, traducciones y relaciones en tabla intermedia con productos y etiquetas
                        $tag->productTag()->delete();
                        $tag->tagTranslation()->delete();
                        $tag->delete();
                        //fin borrado

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Etiquetas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una etiqueta.
     *
     * Para la modificación de etiquetas se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
                'order' => 'integer|min:0'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $etiqueta = Tag::find($request->get('id'));
                if (!$etiqueta) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar etiqueta
                    $etiqueta->order = $request->get('order') != '' ? $request->get('order') : 0;

                    $etiqueta->save();
                    //FIN actualizar la etiqueta

                    // Actualizar traducciones de la etiqueta
                    $array_borrar = [];
                    foreach ($array_traducciones as $idioma) {

                        // Preparación de datos
                        $friendly_url = null;

                        if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                            $friendly_url = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                        } else {
                            $friendly_url = Str::slug($request->get('name')[$idioma->abbreviation]);
                        }
                        //

                        $traduccion = $etiqueta->tagTranslation()->where('language_id', $idioma->id)->first();

                        $array_borrar[] = $idioma->id;

                        if ($traduccion) {

                            $traduccion->update([
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'friendly_url' => $friendly_url,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            ]);
                        } else {

                            TagTranslation::create([
                                'tag_id' => $request->get('id'),
                                'language_id' => $idioma->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'friendly_url' => $friendly_url,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            ]);
                        }
                    }
                    $etiqueta->tagTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                    // FIN actualizar traducciones de etiqueta

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Etiquetas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todas las etiquetas
     *
     * Para la consulta de tags se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
                'product_id' => 'integer|min:0'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Se preparan las consultas
                $tags = DB::connection('tenant')->table('mo_tag')
                    ->select('mo_tag.id', 'mo_tag.order')
                    ->join('mo_tag_translation', 'mo_tag.id', '=', 'mo_tag_translation.tag_id')
                    ->where('mo_tag_translation.deleted_at', '=', null)
                    ->where('mo_tag.deleted_at', '=', null)
                    ->leftJoin('mo_product_tag', function ($join) {
                        $join->on('mo_tag.id', '=', 'mo_product_tag.tag_id')->where('mo_product_tag.deleted_at', null);
                    })
                    ->leftjoin('mo_product', function ($join) {
                        $join->on('mo_product_tag.product_id', '=', 'mo_product.id')->where('mo_product.deleted_at', null);
                    });


                $sub = DB::connection('tenant')->table('mo_tag')
                    ->select('mo_tag.id')
                    ->join('mo_tag_translation', 'mo_tag.id', '=', 'mo_tag_translation.tag_id')
                    ->where('mo_tag_translation.deleted_at', '=', null)
                    ->where('mo_tag.deleted_at', '=', null)
                    ->leftJoin('mo_product_tag', function ($join) {
                        $join->on('mo_tag.id', '=', 'mo_product_tag.tag_id')->where('mo_product_tag.deleted_at', null);
                    })
                    ->leftjoin('mo_product', function ($join) {
                        $join->on('mo_product_tag.product_id', '=', 'mo_product.id')->where('mo_product.deleted_at', null);
                    });

                // Recogida de datos enviados por el usuario y establecimiento de filtros
                if ($request->get('lang') != '') {
                    $tags->where('language_id', '=', $idioma->id);
                    $sub->where('language_id', '=', $idioma->id);
                }

                if ($request->get('name') != '') {

                    $tags->Where(function ($query) use ($request) {
                        $query->where('mo_tag_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_tag_translation.description', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_tag_translation.short_description', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_tag_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_tag_translation.description', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_tag_translation.short_description', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('product_id') != '') {
                    $tags->where('mo_product_tag.product_id', '=', $request->get('product_id'))->where('mo_product.id', $request->get('product_id'));
                    $sub->where('mo_product_tag.product_id', '=', $request->get('product_id'))->where('mo_product.id', $request->get('product_id'));
                }

                // Order
                $orden = 'mo_tag_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_tag_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_tag_translation.description';
                            break;
                        case 'short_description':
                            $orden = 'mo_tag_translation.short_description';
                            break;
                        case 'order':
                            $orden = 'mo_tag.order';
                            break;
                        case 'id':
                            $orden = 'mo_tag.id';
                            break;
                        default:
                            $orden = 'mo_tag_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $tags->groupBy('mo_tag.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $tags = $tags->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $tags = $tags->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_tag_translation.tag_id');
                $tags_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $tags_count->count();



                $array['data'] = array();
                foreach ($tags as $tag) {

                    foreach ($idiomas as $idi) {
                        $traduccion = TagTranslation::where('tag_id', '=', $tag->id)
                            ->select('id', 'language_id', 'name', 'description', 'short_description', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $tag->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['tag'][] = $tag;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Etiquetas');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra una etiqueta buscada por su id
     *
     * Para la consulta de un tag se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // FIN Validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $tags = DB::connection('tenant')->table('mo_tag')
                    ->select('mo_tag.id', 'mo_tag.order')
                    ->join('mo_tag_translation', 'mo_tag.id', '=', 'mo_tag_translation.tag_id')
                    ->where('mo_tag_translation.deleted_at', '=', null)
                    ->where('mo_tag.deleted_at', '=', null);

                if ($request->get('lang') != '') {
                    $tags->where('language_id', '=', $idioma->id);
                }

                $tags->where('mo_tag.id', '=', $id);

                $tags->groupBy('mo_tag.id');
                $tags = $tags->get();

                $array['data'] = array();
                foreach ($tags as $tag) {
                    if ($request->get('lang') != '') {
                        $traducciones = TagTranslation::where('language_id', '=', $idioma->id)->where('tag_id', '=', $tag->id)
                            ->select('id', 'language_id', 'name', 'description', 'short_description', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $tag->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = TagTranslation::where('tag_id', '=', $tag->id)
                                ->select('id', 'language_id', 'name', 'description', 'short_description', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $tag->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['tag'][] = $tag;
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Etiquetas');
        }

        return response()->json($array, $array['error']);

    }

    /**
     * Función que asigna etiquetas a productos
     *
     * Para asignar etiquetas a un producto se realiza un petición POST.
     * Borra todas las relaciones existentes del producto con las etiquetas y crea las nuevas relaciones según las etiquetas que se le especifica.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(Request $request)
    {
        $array['error'] = 200;
        try {

            DB::connection('tenant')->beginTransaction();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'product_id' => 'required|numeric',
                'tags' => 'array',
                'tags.*' => 'required|numeric|exists:tenant.mo_tag,id,deleted_at,NULL|distinct',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin de validación

            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'product_id' => 'exists:tenant.mo_product,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    //Busca y borra todas las etiquetas asociadas al producto
                    ProductTag::where('product_id', '=', $request->get('product_id'))->delete();

                    //Si recibe parámetro tags recorre el array y crea la relación con el producto
                    if ($request->get('tags')) {
                        foreach ($request->get('tags') as $tag) {
                            ProductTag::create([
                                'product_id' => $request->get('product_id'),
                                'tag_id' => $tag,
                            ]);
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Etiquetas');
        }

        return response()->json($array, $array['error']);
    }

}