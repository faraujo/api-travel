<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Device;
use App\Models\tenant\IdentificationTranslation;
use App\Models\tenant\IdentificationUser;
use App\Models\tenant\Language;
use App\Models\tenant\Promotion;
use App\Models\tenant\PromotionAccumulate;
use App\Models\tenant\PromotionClientType;
use App\Models\tenant\PromotionCountry;
use App\Models\tenant\PromotionCurrency;
use App\Models\tenant\PromotionDevice;
use App\Models\tenant\PromotionEnjoy;
use App\Models\tenant\PromotionEnjoyDay;
use App\Models\tenant\PromotionLanguage;
use App\Models\tenant\PromotionPaymentMethod;
use App\Models\tenant\PromotionPickup;
use App\Models\tenant\PromotionProduct;
use App\Models\tenant\PromotionProductClientType;
use App\Models\tenant\PromotionProductType;
use App\Models\tenant\PromotionProductTypeClientType;
use App\Models\tenant\PromotionService;
use App\Models\tenant\PromotionServiceClientType;
use App\Models\tenant\PromotionState;
use App\Models\tenant\PromotionSubchannel;
use App\Models\tenant\PromotionTranslation;
use App\Models\tenant\PromotionTypeTranslation;
use App\Models\tenant\ReservationDetail;
use App\Models\tenant\ReservationDetailBenefitCard;
use App\Models\tenant\ReservationDetailClientType;
use App\Models\tenant\ReservationDetailDayMore;
use App\Models\tenant\ReservationDetailEvent;
use App\Models\tenant\ReservationDetailPackage;
use App\Models\tenant\ReservationDetailPhoto;
use App\Models\tenant\ReservationDetailFood;
use App\Models\tenant\ReservationDetailRestaurant;
use App\Models\tenant\ReservationDetailTransportation;
use App\Models\tenant\ReservationDetailActivity;
use App\Models\tenant\ReservationDetailHotel;
use App\Models\tenant\ReservationDetailPark;
use App\Models\tenant\ReservationDetailTour;
use App\Models\tenant\ReservationPromotion;
use App\Models\tenant\Settings;
use App\Models\tenant\UserToken;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Http\Request;

class ReservationsPromotionController extends Controller
{

    /**
     * Meétodo para mostrar promociones
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPromotions(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $idiomas = Language::where('front', 1)->get();

            $settings_language = Settings::where('name','idioma_defecto_id')->first()->value;

            $settings_device = Settings::where('name','dispositivo_defecto_id')->first()->value;

            $devices = Device::all();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
                'id' => 'required|integer|min:0',
                'payment_method_id' => 'integer|min:0|exists:tenant.mo_payment_method,id,deleted_at,NULL',
//                'lang' => 'required',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma = $idiomas->where('id', $settings_language)->where('front', 1)->first();
            }

            if ($request->get('device') != '') {
                $device = $devices->where('code', Str::upper($request->get('device')))->where('active', 1)->first();
                if (!$device) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['device' => ['The device is not exists']]);
                }
            } else {
                $device = $devices->where('id', $settings_device)->where('active', 1)->first();
            }

            $user_id = '';
            $client_session = '';

            if (strpos($request->path(), 'search') !== false) {

                // Si el usuario está logueado
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

                } elseif ($request->get('client_session') != '') { // Si no está logueado
                    $client_session = $request->get('client_session');

                } else { // Si no está logueado y no aporta identificación temporal
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token or session is required']]);
                }

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS

                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }


            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'id' => 'exists:tenant.mo_reservation,id,deleted_at,NULL'
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                    //Fin validación

                } else {
                    // Preparación de datos
                    $reserva_client_id = null;
                    $reserva_client_session = null;
                    $reserva_agent_id = null;
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'search') !== false) {
                        if ($user_id != '') {
                            $reserva_client_id = $user_id;
                        } else {
                            $reserva_client_session = Str::random('200');

                        }
                    } else { // Si la petición procede del POS
                        $reserva_agent_id = $user_id;
                    }

                    $sql_reservation = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id',
                            'mo_reservation.client_id',
                            'mo_reservation.client_session',
                            'mo_reservation.subchannel_id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'));


                    $datos_reservation = $sql_reservation->first();

                    $error_aux = 0;
                    // Validación por permisos
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'search') !== false) {
                        if ($user_id != '') {
                            if ($datos_reservation->client_id != $user_id) {
                                $error_aux = 1;
                            }
                        } elseif ($client_session != '') {
                            if ($datos_reservation->client_session != $client_session) {
                                $error_aux = 1;
                            }
                        }
                    } else { // Si la petición procede del POS
                        if (!$datos_permisos) {
                            if ($datos_reservation->subchannel_id != $subchannel_id) {
                                $error_aux = 1;
                            }
                        }
                    }
                    // FIN Validación por permisos


                    $sql_estado_carro = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'))
                        ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                        ->whereNull('mo_reservation.deleted_at')
                        ->where(function ($query) {
                            $query->where('mo_reservation_detail.operation_status_id', 1)
                                ->orWhere('mo_reservation_detail.operation_status_id', 2)
                                ->orWhere('mo_reservation_detail.operation_status_id', 3)
                                ->orWhere('mo_reservation_detail.operation_status_id', 7);
                        })->groupBy('mo_reservation_detail.reservation_id');


                    $datos_estado_carro = $sql_estado_carro->first();

                    if (!$datos_estado_carro) {
                        $error_aux = 1;
                    }

                    if ($error_aux == 1) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['You can not apply promotions to the reservation.']]);
                    }
                    // FIN Validación por cada reserva que se aplicar promociones

                    if ($error == 1) {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = $mensaje_validador;
                    } else {

                        $fecha = Carbon::now();

                        $fecha_actual = $fecha->format('Y-m-d H:i:s');

                        $week_day = $fecha->format('N');

                        $sql_promotions = DB::connection('tenant')->table('mo_promotion')
                            ->select(
                                'mo_promotion.id',
                                'mo_promotion.order',
                                'mo_promotion.visibility',
                                'mo_promotion.accumulate',
                                'mo_promotion.min_quantity',
                                'mo_promotion.max_quantity',
                                'mo_promotion.promotion_amount',
                                'mo_promotion.currency_id',
                                'mo_promotion.promotion_type_id',
                                'mo_promotion.campaign_id',
                                'mo_promotion.department_id',
                                'mo_promotion.cost_center_id',
                                'mo_promotion.apply_payment_method_filter',
                                'mo_promotion.apply_language_filter',
                                'mo_promotion.apply_currency_filter',
                                'mo_promotion.apply_pickup_filter',
                                'mo_promotion.apply_product_filter',
                                'mo_promotion.apply_product_type_filter',
                                'mo_promotion.apply_service_filter',
                                'mo_promotion.apply_subchannel_filter',
                                'mo_promotion.apply_client_type_filter',
                                'mo_promotion.apply_country_filter',
                                'mo_promotion.apply_state_filter',
                                'mo_promotion.apply_device_filter',
                                'mo_promotion.lockers_validation',
                                'mo_promotion.supervisor_validation',
                                'mo_promotion.allow_date_change',
                                'mo_promotion.allow_upgrade',
                                'mo_promotion.allow_product_change',
                                'mo_promotion.pay_difference',
                                'mo_promotion.anticipation_start',
                                'mo_promotion.anticipation_end',
                                'mo_promotion.coupon_code',
                                'mo_promotion.coupon_type',
                                'mo_promotion.coupon_sheet_start',
                                'mo_promotion.coupon_sheet_end',
                                'mo_promotion.coupon_total_uses',
                                'mo_promotion.benefit_card',
                                'mo_promotion.benefit_card_total_uses',
                                'mo_promotion.benefit_card_id',
                                'mo_promotion.user_id',
                                'mo_promotion_translation.name',
                                'mo_promotion_translation.description',
                                'mo_promotion_translation.short_description',
                                'mo_promotion_translation.friendly_url',
                                'mo_promotion_translation.external_url',
                                'mo_promotion_translation.title_seo',
                                'mo_promotion_translation.description_seo',
                                'mo_promotion_translation.keywords_seo',
                                'mo_promotion_translation.observations',
                                'mo_promotion_translation.legal',
                                'mo_promotion_translation.notes',
                                'mo_promotion_translation.gift',
                                'mo_promotion_type_translation.name as type_name',
                                'mo_campaign_translation.name as campaign_name'
                            )
                            ->whereNull('mo_promotion.deleted_at')
                            ->join('mo_promotion_type', 'mo_promotion_type.id', 'mo_promotion.promotion_type_id')
                            ->whereNull('mo_promotion_type.deleted_at')
                            ->join('mo_promotion_type_translation', 'mo_promotion_type_translation.promotion_type_id', 'mo_promotion_type.id')
                            ->whereNull('mo_promotion_type_translation.deleted_at')
                            ->join('mo_promotion_translation', 'mo_promotion_translation.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_translation.deleted_at')
//                            ->where('mo_promotion_type_translation.language_id', $idioma->id)
                            ->join('mo_campaign', 'mo_campaign.id', 'mo_promotion.campaign_id')
                            ->whereNull('mo_campaign.deleted_at')
                            ->where('mo_campaign.active', 1)
                            ->where('mo_campaign.date_start', '<=', $fecha->format('Y-m-d'))
                            ->where('mo_campaign.date_end', '>=', $fecha->format('Y-m-d'))
                            ->join('mo_campaign_translation', 'mo_campaign_translation.campaign_id', 'mo_campaign.id')
                            ->whereNull('mo_campaign_translation.deleted_at')
                            ->leftJoin('mo_campaign_subchannel', function ($join) {
                                $join->on('mo_campaign_subchannel.campaign_id', 'mo_campaign.id')
                                    ->whereNull('mo_campaign_subchannel.deleted_at');
                            })
                            ->where(function ($query) use ($emulated_subchannel_id) {
                                $query->where('mo_campaign.apply_filter_subchannel', 0)
                                    ->orWhere('mo_campaign_subchannel.subchannel_id', $emulated_subchannel_id);
                            })
                            ->join('mo_promotion_range_sale_day', 'mo_promotion_range_sale_day.promotion_id', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_sale_day.deleted_at')
                            ->where('mo_promotion_range_sale_day.sale_date_start', '<=', $fecha_actual)
                            ->where('mo_promotion_range_sale_day.sale_date_end', '>=', $fecha_actual)
                            ->join('mo_promotion_sale_day', 'mo_promotion_sale_day.promotion_range_sale_day_id', 'mo_promotion_range_sale_day.id')
                            ->whereNull('mo_promotion_sale_day.deleted_at')
                            ->where('mo_promotion_sale_day.day_id', $week_day)
                            ->leftJoin('mo_promotion_subchannel', function ($join) {
                                $join->on('mo_promotion_subchannel.promotion_id', '=', 'mo_promotion.id')
                                    ->whereNull('mo_promotion_subchannel.deleted_at');
                            })
                            ->where(function ($query) use ($emulated_subchannel_id) {
                                $query->where('mo_promotion.apply_subchannel_filter', 0)
                                    ->orWhere('mo_promotion_subchannel.subchannel_id', $emulated_subchannel_id);
                            })
                            ->orderBy('mo_promotion.order', 'desc')
                            ->groupBy('mo_promotion.id');

                        $sql_price = DB::connection('tenant')->table('mo_price')
                            ->select(
                                'mo_price.id'
                            )
                            ->whereNull('mo_price.deleted_at')
                            ->where('mo_price.promotions', 1)
                            ->join('mo_reservation_detail', 'mo_reservation_detail.price_id', 'mo_price.id')
                            ->whereNull('mo_reservation_detail.deleted_at')
                            ->join('mo_reservation', 'mo_reservation.id', 'mo_reservation_detail.reservation_id')
                            ->whereNull('mo_reservation.deleted_at')
                            ->where('mo_reservation.id', $request->get('id'));

                        $datos_price = $sql_price->first();

                        $array_promotions = array();

                        if ($datos_price) {

                            if ($request->get('coupon_code') != '') {

                                $coupon = explode('-', $request->get('coupon_code'));

                                $sql_promotions->where('mo_promotion.coupon_type', 1);

                                if (isset($coupon[0])) {
                                    $sql_promotions->where('mo_promotion.coupon_code', $coupon[0]);
                                } else {
                                    $sql_promotions->where('mo_promotion.coupon_code', $request->get('coupon_code'));
                                }

                                if (isset($coupon[1])) {
                                    $sql_promotions->where('mo_promotion.coupon_sheet_start', '<=', $coupon[1])
                                        ->where('mo_promotion.coupon_sheet_end', '>=', $coupon[1]);
                                }

                            }

                            if ($request->get('benefit_card_id') != '') {
                                $sql_promotions->where('mo_promotion.benefit_card', 1)
                                    ->where('mo_promotion.benefit_card_id', $request->get('benefit_card_id'));
                            }

                            $datos_promotions = $sql_promotions->get();

                            $array_promotions = promotionsApplicable($datos_promotions, $request, $idioma, $device);
                        }
                        //FIN promociones vigentes

                        $array['data'] = $array_promotions;
                    }
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Reservas - Promociones');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Método para quitar promociones
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removePromotion(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
                'id' => 'required|integer|min:0',
                'promotion_id' => 'required|integer|min:0|exists:tenant.mo_promotion,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $user_id = '';
            $client_session = '';

            if (strpos($request->path(), 'search') !== false) {

                // Si el usuario está logueado
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

                } elseif ($request->get('client_session') != '') { // Si no está logueado
                    $client_session = $request->get('client_session');

                } else { // Si no está logueado y no aporta identificación temporal
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token or session is required']]);
                }

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS

                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'id' => 'exists:tenant.mo_reservation,id,deleted_at,NULL'
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                    //Fin validación

                } else {
                    // Preparación de datos
                    $reserva_client_id = null;
                    $reserva_client_session = null;
                    $reserva_agent_id = null;
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'search') !== false) {
                        if ($user_id != '') {
                            $reserva_client_id = $user_id;
                        } else {
                            $reserva_client_session = Str::random('200');

                        }
                    } else { // Si la petición procede del POS
                        $reserva_agent_id = $user_id;
                    }

                    $sql_reservation = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id',
                            'mo_reservation.client_id',
                            'mo_reservation.client_session',
                            'mo_reservation.subchannel_id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'));


                    $datos_reservation = $sql_reservation->first();

                    $error_aux = 0;
                    // Validación por permisos
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'search') !== false) {
                        if ($user_id != '') {
                            if ($datos_reservation->client_id != $user_id) {
                                $error_aux = 1;
                            }
                        } elseif ($client_session != '') {
                            if ($datos_reservation->client_session != $client_session) {
                                $error_aux = 1;
                            }
                        }
                    } else { // Si la petición procede del POS
                        if (!$datos_permisos) {
                            if ($datos_reservation->subchannel_id != $subchannel_id) {
                                $error_aux = 1;
                            }
                        }
                    }
                    // FIN Validación por permisos


                    $sql_estado_carro = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'))
                        ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                        ->whereNull('mo_reservation.deleted_at')
                        ->where(function ($query) {
                            $query->where('mo_reservation_detail.operation_status_id', 1)
                                ->orWhere('mo_reservation_detail.operation_status_id', 2)
                                ->orWhere('mo_reservation_detail.operation_status_id', 3)
                                ->orWhere('mo_reservation_detail.operation_status_id', 7);
                        })->groupBy('mo_reservation_detail.reservation_id');


                    $datos_estado_carro = $sql_estado_carro->first();

                    if (!$datos_estado_carro) {
                        $error_aux = 1;
                    }

                    if ($error_aux == 1) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['You can not apply promotions to the reservation.']]);
                    }
                    // FIN Validación por cada reserva que se aplicar promociones

                    if ($error == 1) {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = $mensaje_validador;
                    } else {
                        $validator = Validator::make($request->all(), [
                            'id' => 'exists:tenant.mo_reservation,id,deleted_at,NULL',
                            'promotion_id' => 'exists:tenant.mo_promotion,id,deleted_at,NULL'
                        ]);

                        if ($validator->fails()) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge($validator->errors());
                        }

                        if ($error == 1) {
                            $array['error'] = 404;
                            $array['error_description'] = 'Data not found';
                            $array['error_inputs'][0] = $mensaje_validador;
                            //Fin validación

                        } else {
                            ReservationPromotion::where('promotion_id', $request->get('promotion_id'))
                                ->where('reservation_id', $request->get('id'))
                                ->delete();
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Reservas - Promociones');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Método para aplicar todas las promociones posibles a una reserva
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function autoaplicatePromotion(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $idiomas = Language::where('front', 1)->get();

            $devices = Device::all();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|exists:tenant.mo_subchannel,id,deleted_at,NULL|integer|min:0',
                'id' => 'required|integer|min:0',
                'benefit_card_id' => 'integer|min:0|exists:tenant.mo_identification,id,deleted_at,NULL',
                'lang' => 'required',
                'reset_promotions' => 'boolean',
                'promotion_id' => 'integer|min:0|exists:tenant.mo_promotion,id,deleted_at,NULL'
            ]);


            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($request->get('device') != '') {
                $device = $devices->where('code', Str::upper($request->get('device')))->first();
                if (!$device) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['device' => ['The device is not exists']]);
                }
            }

            $user_id = '';
            $client_session = '';

            if (strpos($request->path(), 'search') !== false) {

                // Si el usuario está logueado
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

                } elseif ($request->get('client_session') != '') { // Si no está logueado
                    $client_session = $request->get('client_session');

                } else { // Si no está logueado y no aporta identificación temporal
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token or session is required']]);
                }

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS

                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'id' => 'exists:tenant.mo_reservation,id,deleted_at,NULL'
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                    //Fin validación

                } else {
                    // Preparación de datos
                    $reserva_client_id = null;
                    $reserva_client_session = null;
                    $reserva_agent_id = null;
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'search') !== false) {
                        if ($user_id != '') {
                            $reserva_client_id = $user_id;
                        } else {
                            $reserva_client_session = Str::random('200');

                        }
                    } else { // Si la petición procede del POS
                        $reserva_agent_id = $user_id;
                    }

                    $sql_reservation = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id',
                            'mo_reservation.client_id',
                            'mo_reservation.client_session',
                            'mo_reservation.subchannel_id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'));


                    $datos_reservation = $sql_reservation->first();

                    $error_aux = 0;
                    // Validación por permisos
                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'search') !== false) {
                        if ($user_id != '') {
                            if ($datos_reservation->client_id != $user_id) {
                                $error_aux = 1;
                            }
                        } elseif ($client_session != '') {
                            if ($datos_reservation->client_session != $client_session) {
                                $error_aux = 1;
                            }
                        }
                    } else { // Si la petición procede del POS
                        if (!$datos_permisos) {
                            if ($datos_reservation->subchannel_id != $subchannel_id) {
                                $error_aux = 1;
                            }
                        }
                    }
                    // FIN Validación por permisos


                    $sql_estado_carro = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->where('mo_reservation.id', $request->get('id'))
                        ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                        ->whereNull('mo_reservation.deleted_at')
                        ->where(function ($query) {
                            $query->where('mo_reservation_detail.operation_status_id', 1)
                                ->orWhere('mo_reservation_detail.operation_status_id', 2)
                                ->orWhere('mo_reservation_detail.operation_status_id', 3)
                                ->orWhere('mo_reservation_detail.operation_status_id', 7);
                        })->groupBy('mo_reservation_detail.reservation_id');


                    $datos_estado_carro = $sql_estado_carro->first();

                    if (!$datos_estado_carro) {
                        $error_aux = 1;
                    }

                    if ($error_aux == 1) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['You can not apply promotions to the reservation.']]);
                    }
                    // FIN Validación por cada reserva que se aplicar promociones

                    if ($error == 1) {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = $mensaje_validador;
                    } else {

                        $promotion_coupon = ReservationPromotion::whereNotNull('coupon_code')->where('reservation_id', $request->get('id'))->get();

                        $promotion_benefit_card = ReservationPromotion::whereNotNull('benefit_card_id')->where('reservation_id', $request->get('id'))->get();

                        if($request->get('coupon_code') != '' || $request->get('benefit_card_id') != '') {
                            ReservationPromotion::where('reservation_id', $request->get('id'))->delete();
                        }

                        if($request->get('benefit_card_id') != ''){

                            $request_benefit = new Request();

                            $request_benefit->request->add(['id' => $request->get('id')]);

                            $request_benefit->request->add(['benefit_card_id' => $request->get('benefit_card_id')]);
                            $request_benefit->request->add(['benefit_card_number' => $request->get('benefit_card_number')]);
                            $request_benefit->request->add(['device' => $request->get('device')]);

                            $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_benefit, $emulated_subchannel_id, $idioma);

                            foreach($promotion_benefit_card as $benefit) {

                                $request_benefit = new Request();

                                $request_benefit->request->add(['id' => $request->get('id')]);

                                $request_benefit->request->add(['benefit_card_id' => $benefit->benefit_card_id]);
                                $request_benefit->request->add(['benefit_card_number' => $benefit->benefit_card_number]);
                                $request_benefit->request->add(['device' => $request->get('device')]);

                                $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_benefit, $emulated_subchannel_id, $idioma);
                            }

                            foreach($promotion_coupon as $coupon) {
                                $request_coupon = new Request();

                                $request_coupon->request->add(['id' => $request->get('id')]);

                                $request_coupon->request->add(['coupon_code' => $coupon->coupon_code]);
                                $request_coupon->request->add(['device' => $request->get('device')]);

                                $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_coupon, $emulated_subchannel_id, $idioma);
                            }
                        }

                        if($request->get('coupon_code') != ''){

                            $request_coupon = new Request();

                            $request_coupon->request->add(['id' => $request->get('id')]);

                            $request_coupon->request->add(['coupon_code' => $request->get('coupon_code')]);
                            $request_coupon->request->add(['device' => $request->get('device')]);

                            $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_coupon, $emulated_subchannel_id, $idioma);

                            foreach($promotion_benefit_card as $benefit) {

                                $request_benefit = new Request();

                                $request_benefit->request->add(['id' => $request->get('id')]);

                                $request_benefit->request->add(['benefit_card_id' => $benefit->benefit_card_id]);
                                $request_benefit->request->add(['benefit_card_number' => $benefit->benefit_card_number]);
                                $request_benefit->request->add(['device' => $request->get('device')]);

                                $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_benefit, $emulated_subchannel_id, $idioma);
                            }

                            foreach($promotion_coupon as $coupon) {
                                $request_coupon = new Request();

                                $request_coupon->request->add(['id' => $request->get('id')]);

                                $request_coupon->request->add(['coupon_code' => $coupon->coupon_code]);
                                $request_coupon->request->add(['device' => $request->get('device')]);

                                $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_coupon, $emulated_subchannel_id, $idioma);
                            }
                        }

                        $request_new = new Request();

                        $request_new->request->add(['id' => $request->get('id')]);
                        $request_new->request->add(['device' => $request->get('device')]);

                        $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request_new, $emulated_subchannel_id, $idioma);


                        if($request->get('promotion_id') != '') {

                            $respuesta_aplicatePromotionHelper = aplicatePromotionHelper($request, $emulated_subchannel_id, $idioma);
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();
        } catch
        (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Reservas - Promociones');
        }

        return response()->json($array, $array['error']);
    }


}