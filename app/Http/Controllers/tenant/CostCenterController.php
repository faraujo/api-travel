<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Settings;
use validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CostCenterController extends Controller
{
    /**
     * Función que lista todos los centros de costo
     *
     * Para la consulta de centros de costo se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCostCenters(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $centro_costes = DB::connection('tenant')->table('mo_cost_center')
                    ->select('mo_cost_center.id','mo_cost_center.department_id', 'mo_cost_center.name', 'mo_cost_center.code','mo_cost_center.accounting_cost_center','mo_cost_center.accounting_code' )
                    ->where('mo_cost_center.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_cost_center')
                    ->select('mo_cost_center.id', 'mo_cost_center.name', 'mo_cost_center.code')
                    ->where('mo_cost_center.deleted_at', '=', null);

                //Fin precompiladas
                

                // Order
                $orden = 'mo_cost_center.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_cost_center.id';
                            break;
                        case 'name':
                            $orden = 'mo_cost_center.name';
                            break;
                        default:
                            $orden = 'mo_cost_center.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $centro_costes = $centro_costes->groupBy('mo_cost_center.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $centro_costes = $centro_costes->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $centro_costes = $centro_costes->get();

                }

                //Fin de paginación

                $cost_centers_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $cost_centers_count->count();


                $array['data'] = array();


                //Se recorren las campañas
                foreach ($centro_costes as $centro_coste) {

                    $array['data'][0]['cost_center'][] = $centro_coste;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Centros de coste');
        }

        return response()->json($array, $array['error']);
    }

}