<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use App\Models\tenant\TransportationLocation;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransportationLocationController extends Controller
{
   
    /**
     * Función que lista todas las localizaciones del trasporte
     *
     * Para la consulta de localizaciones se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Se preparan las consultas
                $sql_transportation_location = DB::connection('tenant')->table('mo_transportation_location')
                ->select('mo_transportation_location.id', 'mo_transportation_location.name', 'mo_transportation_location.api_code')
                ->where('mo_transportation_location.deleted_at', '=', null);
                
                $sub = DB::connection('tenant')->table('mo_transportation_location')
                ->select('mo_transportation_location.id', 'mo_transportation_location.name', 'mo_transportation_location.api_code')
                ->where('mo_transportation_location.deleted_at', '=', null);
                
                // Order
                $orden = 'mo_transportation_location.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_transportation_location.name';
                            break;
                        case 'api_code':
                            $orden = 'mo_transportation_location.api_code';
                            break;
                        case 'id':
                            $orden = 'mo_transportation_location.id';
                            break;
                        default:
                            $orden = 'mo_transportation_location.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_transportation_location->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_transportation_location = $sql_transportation_location->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_transportation_location = $sql_transportation_location->get();

                }

                //Fin de paginación
                $transportation_location_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $transportation_location_count->count();

                $array['data'] = array();
                foreach ($datos_transportation_location as $transportation) {
                    $array['data'][0]['transportationlocation'][] = $transportation;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones Transporte');
        }

        return response()->json($array, $array['error']);
    }
 
    /**
     * Función para la creación de localizaciones para el servicio transporte
     *
     * Para la creación de localizaciones de transporte se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'api_code' => 'required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                // Crear la localización de transporte
                $transportation_location = TransportationLocation::create([
                    'name' => $request->get('name') != '' ? $request->get('name') : null,
                    'api_code' => $request->get('api_code') != '' ? $request->get('api_code') : null,
                ]);
                // FIN crear localización de transporte

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones Transporte');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de una localización de transporte.
     *
     * Para la eliminación de localización de transporte se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_transportation_location,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $transportation_location_id) {
                        $transportation_location = TransportationLocation::where('id', $transportation_location_id)->first();
                        // borrado de localización de transporte
                        $transportation_location->delete();
                        //fin borrado

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones Transporte');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una localización de transporte.
     *
     * Para la modificación de localización de transporte se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'name' => 'required',
                'api_code' => 'required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $transportation_location = TransportationLocation::find($request->get('id'));
                if (!$transportation_location) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar localización de transporte
                    $transportation_location->name = $request->get('name') != '' ? $request->get('name') : null;
                    $transportation_location->api_code = $request->get('api_code') != '' ? $request->get('api_code') : null;

                    $transportation_location->save();
                    //FIN actualizar la localización de transporte

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones Transporte');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra una localización de transporte buscada por su id
     *
     * Para la consulta de una localización de transporte se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $datos_transportation = DB::connection('tenant')->table('mo_transportation_location')
            ->select(
                'mo_transportation_location.id',
                'mo_transportation_location.name',
                'mo_transportation_location.api_code'
            )
            ->where('mo_transportation_location.deleted_at', '=', null)
            ->where('mo_transportation_location.id', '=', $id);

            $datos_transportation = $datos_transportation->get();

            $array['data'] = array();
            foreach ($datos_transportation as $transportation) {
                
                $array['data'][0]['transportationlocation'][] = $transportation;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones Transporte');
        }

        return response()->json($array, $array['error']);

    }
   
 
}