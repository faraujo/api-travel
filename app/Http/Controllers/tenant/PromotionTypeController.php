<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\PromotionTypeTranslation;
use validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;


class PromotionTypeController extends Controller
{


    /**
     * Muestra los tipos de promoción disponibles
     *
     * Para la consulta de los tipos de promoción se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTypes(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            //fin validación
            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                $tipos = DB::connection('tenant')->table('mo_promotion_type')
                    ->select('mo_promotion_type.id')
                    ->join('mo_promotion_type_translation', 'mo_promotion_type.id', 'mo_promotion_type_translation.promotion_type_id')
                    ->where('mo_promotion_type_translation.deleted_at', '=', null)
                    ->where('mo_promotion_type.deleted_at', '=', null)
                    ->join('mo_language', 'mo_promotion_type_translation.language_id', 'mo_language.id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_promotion_type.id')->get();

                $sub = DB::connection('tenant')->table('mo_promotion_type')
                    ->select('mo_promotion_type.id')
                    ->join('mo_promotion_type_translation', 'mo_promotion_type.id', 'mo_promotion_type_translation.promotion_type_id')
                    ->where('mo_promotion_type_translation.deleted_at', '=', null)
                    ->where('mo_promotion_type.deleted_at', '=', null)
                    ->join('mo_language', 'mo_promotion_type_translation.language_id', 'mo_language.id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_promotion_type.id')->get();


                foreach ($tipos as $tipo) {
                    if ($request->get('lang') != '') {
                        $traducciones = PromotionTypeTranslation::where('language_id', '=', $idioma->id)->where('promotion_type_id', '=', $tipo->id)
                            ->select('name')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $tipo->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = PromotionTypeTranslation::where('promotion_type_id', '=', $tipo->id)
                                ->select('name','language_id')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $tipo->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['type'][] = $tipo;
                }
                $totales = $sub->count();
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de promociones');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Devuelve el tipo de promoción solicitado
     *
     * Para la consulta de un tipo de promoción se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function showTypeId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            //fin validación
            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $array['data'][0]['type'] = array();

                $type = DB::connection('tenant')->table('mo_promotion_type')->where('id', $id)->first();

                if ($type != '') {
                    $array['data'][0]['type'][]['id'] = $type->id;
                    if ($request->get('lang') != '') {
                        $traducciones = PromotionTypeTranslation::where('language_id', '=', $idioma->id)->where('promotion_type_id', '=', $id)
                            ->select('name','language_id')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $array['data'][0]['type'][0]['lang'][] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = PromotionTypeTranslation::where('promotion_type_id', '=', $id)
                                ->select('name','language_id')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $array['data'][0]['type'][0]['lang'][][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de promociones');
        }

        return response()->json($array, $array['error']);
    }

}