<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\DayTranslation;
use App\Exceptions\Handler;
use App\Models\tenant\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DayController extends Controller
{

    public function show(Request $request)
    {

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $sql_dias = DB::connection('tenant')->table('mo_day')
                    ->select(['mo_day.id'])
                    ->whereNull('mo_day.deleted_at')
                    ->join('mo_day_translation', 'mo_day.id', 'mo_day_translation.day_id')
                    ->whereNull('mo_day_translation.deleted_at')
                    ->groupBy('mo_day.id')
                    ->orderBy('mo_day.id');

                $sub = DB::connection('tenant')->table('mo_day')
                    ->select(['mo_day.id'])
                    ->whereNull('mo_day.deleted_at')
                    ->join('mo_day_translation', 'mo_day.id', 'mo_day_translation.day_id')
                    ->whereNull('mo_day_translation.deleted_at')
                    ->groupBy('mo_day.id')
                    ->orderBy('mo_day.id');


                if ($request->get('lang') != '') {
                    $sql_dias->where('mo_day_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_day_translation.language_id', '=', $idioma->id);
                }


                $dias_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $dias_count->count();


                $datos_dias = $sql_dias->get();
                foreach ($datos_dias as $dia) {
                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de la campaña
                        $traduccion = DayTranslation::where('day_id', '=', $dia->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name', 'abbreviation']);
                        foreach ($traduccion as $trad) {
                            $dia->lang[][$idi->abbreviation] = $trad;
                        }

                    }
                    $array['data'][0]['day'][] = $dia;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Días');
        }
        return response()->json($array, $array['error']);
    }

}