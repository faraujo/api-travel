<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\HotelRoomTranslation;
use App\Models\tenant\Subchannel;
use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use App\Models\tenant\ViewRoom;
use App\Models\tenant\ViewRoomFile;
use App\Models\tenant\ViewRoomTranslation;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use Illuminate\Http\Request;

class ViewRoomController extends Controller
{

    /**
     * Función para creación de visualización por canal de habitaciones
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'order' => 'integer|min:0',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                'room_id' => 'required|integer|min:0|exists:tenant.mo_hotel_room,id,deleted_at,NULL',
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'published' => 'required|boolean',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('location')[$idioma->abbreviation]) ||
                    isset($request->get('views')[$idioma->abbreviation]) || isset($request->get('size')[$idioma->abbreviation]) ||
                    isset($request->get('capacity')[$idioma->abbreviation]) || isset($request->get('url_360')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation]) ||
                    isset($request->get('title_seo')[$idioma->abbreviation]) || isset($request->get('description_seo')[$idioma->abbreviation]) ||
                    isset($request->get('keywords_seo')[$idioma->abbreviation]) || isset($request->get('legal')[$idioma->abbreviation]) ||
                    isset($request->get('services_included')[$idioma->abbreviation]) || isset($request->get('services_not_included')[$idioma->abbreviation]) ||

                    isset($request->get('breadcrumb')[$idioma->abbreviation]) || isset($request->get('rel')[$idioma->abbreviation]) ||
                    isset($request->get('og_title')[$idioma->abbreviation]) || isset($request->get('og_description')[$idioma->abbreviation]) ||
                    isset($request->get('og_image')[$idioma->abbreviation]) || isset($request->get('twitter_title')[$idioma->abbreviation]) ||
                    isset($request->get('twitter_description')[$idioma->abbreviation]) || isset($request->get('twitter_image')[$idioma->abbreviation]) ||
                    isset($request->get('script_head')[$idioma->abbreviation]) || isset($request->get('canonical_url')[$idioma->abbreviation]) ||
                    isset($request->get('script_body')[$idioma->abbreviation]) || isset($request->get('script_footer')[$idioma->abbreviation]) ||
                    isset($request->get('index')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',

                        'index.' . $idioma->abbreviation => 'boolean',
                        'og_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                        'twitter_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $view_room = ViewRoom::where('room_id', $request->get('room_id'))
                    ->where('subchannel_id', $request->get('subchannel_id'))->first();
                //si hay un registro con ese room_id y ese subchannel actualiza y si no lo crea
                if ($view_room) {
                    $view_room->update([
                        'order' => ($request->get('order') != '') ? $request->get('order') : 0,
                        'published' => $request->get('published'),
                    ]);
                } else {
                    // Crea la habitación
                    $view_room = ViewRoom::create([
                        'room_id' => $request->get('room_id'),
                        'subchannel_id' => $request->get('subchannel_id'),
                        'order' => ($request->get('order') != '') ? $request->get('order') : 0,
                        'published' => $request->get('published'),

                    ]);
                    // FIN crear la habitación
                }

                // Actualizar traducciones de la habitación
                $array_borrar = [];
                foreach ($array_traducciones as $idioma) {
                    // Preparación de datos
                    $friendly_url = null;

                    if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                        $friendly_url = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                    } else {
                        $friendly_url = Str::slug($request->get('name')[$idioma->abbreviation]);
                    }

                    $traduccion = $view_room->viewRoomTranslation()->where('language_id', $idioma->id)->first();

                    $array_borrar[] = $idioma->id;

                    if ($traduccion) {

                        $traduccion->update([
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                            'short_description' => $request->get('short_description')[$idioma->abbreviation],
                            'services_included' => (isset($request->get('services_included')[$idioma->abbreviation]) && $request->get('services_included')[$idioma->abbreviation] != '') ? $request->get('services_included')[$idioma->abbreviation] : null,
                            'services_not_included' => (isset($request->get('services_not_included')[$idioma->abbreviation]) && $request->get('services_not_included')[$idioma->abbreviation] != '') ? $request->get('services_not_included')[$idioma->abbreviation] : null,
                            'location' => (isset($request->get('location')[$idioma->abbreviation]) && $request->get('location')[$idioma->abbreviation] != '') ? $request->get('location')[$idioma->abbreviation] : null,
                            'views' => (isset($request->get('views')[$idioma->abbreviation]) && $request->get('views')[$idioma->abbreviation] != '') ? $request->get('views')[$idioma->abbreviation] : null,
                            'size' => (isset($request->get('size')[$idioma->abbreviation]) && $request->get('size')[$idioma->abbreviation] != '') ? $request->get('size')[$idioma->abbreviation] : null,
                            'capacity' => (isset($request->get('capacity')[$idioma->abbreviation]) && $request->get('capacity')[$idioma->abbreviation] != '') ? $request->get('capacity')[$idioma->abbreviation] : null,
                            'url_360' => (isset($request->get('url_360')[$idioma->abbreviation]) && $request->get('url_360')[$idioma->abbreviation] != '') ? $request->get('url_360')[$idioma->abbreviation] : null,
                            'friendly_url' => $friendly_url,
                            'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                            'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                            'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,

                            'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                            'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                            'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                            'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                            'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                            'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                            'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                            'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                            'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                            'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                            'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                            'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                            'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                            ]);
                    } else {

                        ViewRoomTranslation::create([
                            'view_room_id' => $view_room->id,
                            'language_id' => $idioma->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                            'services_included' => (isset($request->get('services_included')[$idioma->abbreviation]) && $request->get('services_included')[$idioma->abbreviation] != '') ? $request->get('services_included')[$idioma->abbreviation] : null,
                            'services_not_included' => (isset($request->get('services_not_included')[$idioma->abbreviation]) && $request->get('services_not_included')[$idioma->abbreviation] != '') ? $request->get('services_not_included')[$idioma->abbreviation] : null,
                            'location' => (isset($request->get('location')[$idioma->abbreviation]) && $request->get('location')[$idioma->abbreviation] != '') ? $request->get('location')[$idioma->abbreviation] : null,
                            'views' => (isset($request->get('views')[$idioma->abbreviation]) && $request->get('views')[$idioma->abbreviation] != '') ? $request->get('views')[$idioma->abbreviation] : null,
                            'size' => (isset($request->get('size')[$idioma->abbreviation]) && $request->get('size')[$idioma->abbreviation] != '') ? $request->get('size')[$idioma->abbreviation] : null,
                            'capacity' => (isset($request->get('capacity')[$idioma->abbreviation]) && $request->get('capacity')[$idioma->abbreviation] != '') ? $request->get('capacity')[$idioma->abbreviation] : null,
                            'url_360' => (isset($request->get('url_360')[$idioma->abbreviation]) && $request->get('url_360')[$idioma->abbreviation] != '') ? $request->get('url_360')[$idioma->abbreviation] : null,
                            'short_description' => $request->get('short_description')[$idioma->abbreviation],
                            'friendly_url' => $friendly_url,
                            'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                            'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                            'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,

                            'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                            'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                            'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                            'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                            'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                            'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                            'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                            'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                            'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                            'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                            'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                            'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                            'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                        ]);
                    }
                }

                $view_room->viewRoomTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                // FIN actualizar traducciones de habitación

                // Crear relación con files
                $view_room->viewRoomFile()->delete();

                if ($request->get('files') != '') {
                    foreach ($request->get('files') as $file) {
                        ViewRoomFile::create([
                            'view_room_id' => $view_room->id,
                            'file_id' => $file,
                        ]);
                    }
                }
                //FIN crear relación con archivo
            }
            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de habitaciones');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función de eliminación de visualización por canal de habitaciones
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
                'subchannel_id' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_hotel_room,id,deleted_at,NULL',
                    'subchannel_id' => 'exists:tenant.mo_subchannel,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $id) {
                        $viewRoom = ViewRoom::where('room_id', $id)->where('subchannel_id', $request->get('subchannel_id'))->first();
                        //por si mandan un par ya borrado
                        if ($viewRoom) {
                            $viewRoom->viewRoomTranslation()->delete();
                            $viewRoom->delete();
                        }
                    }
                }
            }
            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de habitaciones');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar la visualización por canal de las habitaciones
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
                'hotel_id' => 'integer|min:0',
                'subchannel_id' => 'required|integer|min:0',
                'category_id' => 'integer|min:0',
                'published' => 'boolean',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                $subcanal = $request->get('subchannel_id');
                $fecha_actual = Carbon::now()->format('Y-m-d');

                //Preparación de consultas
                $sql_rooms = DB::connection('tenant')->table('mo_hotel_room')
                    ->select('mo_hotel_room.*','mo_view_room.id as view_id', 'mo_view_room.room_id as view_room_id',
                        'mo_view_room.order as view_order', 'mo_subchannel.view_blocked', 'mo_subchannel.id as subchannel_id',
                        'mo_view_room.published as view_published')
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->join('mo_hotel_room_translation', 'mo_hotel_room.id', '=', 'mo_hotel_room_translation.room_id')
                    ->whereNull('mo_hotel_room_translation.deleted_at')
                    ->join('mo_product','mo_product.id','mo_hotel_room.hotel_id')
                    ->whereNull('mo_product.deleted_at')
                    ->where('mo_product.type_id','=',2)
                    ->join('mo_price_product','mo_price_product.product_id','mo_product.id')
                    ->whereNull('mo_price_product.service_id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->join('mo_price','mo_price.id','mo_price_product.price_id')
                    ->whereNull('mo_price.deleted_at')
                    //Comprueba la existencia de una tarifa en la fecha actual
                    ->where('mo_price.date_start', '<=', $fecha_actual)
                    ->where('mo_price.date_end', '>=', $fecha_actual)
                    ->join('mo_contract_model','mo_price.contract_model_id','mo_contract_model.id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel','mo_subchannel.contract_model_id','mo_price.contract_model_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_subchannel.id', '=', $subcanal)
                    ->leftJoin('mo_view_room', function ($join) use ($subcanal) {
                        $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')->where('mo_view_room.subchannel_id', '=', $subcanal)->whereNull('mo_view_room.deleted_at');
                    })
                    ->leftJoin('mo_view_room_translation', function ($join) {
                        $join->on('mo_view_room.id', '=', 'mo_view_room_translation.view_room_id')->whereNull('mo_view_room_translation.deleted_at');
                    });


                $sub = DB::connection('tenant')->table('mo_hotel_room')
                    ->select('mo_hotel_room.id')
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->join('mo_hotel_room_translation', 'mo_hotel_room.id', '=', 'mo_hotel_room_translation.room_id')
                    ->whereNull('mo_hotel_room_translation.deleted_at')
                    ->join('mo_product','mo_product.id','mo_hotel_room.hotel_id')
                    ->whereNull('mo_product.deleted_at')
                    ->where('mo_product.type_id','=',2)
                    ->join('mo_price_product','mo_price_product.product_id','mo_product.id')
                    ->whereNull('mo_price_product.service_id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->join('mo_price','mo_price.id','mo_price_product.price_id')
                    ->whereNull('mo_price.deleted_at')
                    //Comprueba la existencia de una tarifa en la fecha actual
                    ->where('mo_price.date_start', '<=', $fecha_actual)
                    ->where('mo_price.date_end', '>=', $fecha_actual)
                    ->join('mo_contract_model','mo_price.contract_model_id','mo_contract_model.id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel','mo_subchannel.contract_model_id','mo_price.contract_model_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_subchannel.id', '=', $subcanal)
                    ->leftJoin('mo_view_room', function ($join) use ($subcanal) {
                        $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')->where('mo_view_room.subchannel_id', '=', $subcanal)->whereNull('mo_view_room.deleted_at');
                    })
                    ->leftJoin('mo_view_room_translation', function ($join) {
                        $join->on('mo_view_room.id', '=', 'mo_view_room_translation.view_room_id')->whereNull('mo_view_room_translation.deleted_at');
                    });

                // Recogida de datos enviados por el usuario y establecimiento de filtros

                if ($request->get('lang') != '') {
                    $sql_rooms->Where(function ($query) use ($idioma) {
                        $query->where('mo_hotel_room_translation.language_id', '=', $idioma->id)
                            ->orWhere('mo_view_room_translation.language_id', '=', $idioma->id);
                    });
                    $sub->Where(function ($query) use ($idioma) {
                        $query->where('mo_hotel_room_translation.language_id', '=', $idioma->id)
                            ->orWhere('mo_view_room_translation.language_id', '=', $idioma->id);
                    });
                }

                if ($request->get('code') != '') {
                    $sql_rooms->where('mo_hotel_room.code', 'Like', '%' . $request->get('code') . '%');
                    $sub->where('mo_hotel_room.code', 'Like', '%' . $request->get('code') . '%');
                }

                if ($request->get('name') != '') {
                    $sql_rooms->Where(function ($query) use ($request) {
                        $query->where('mo_hotel_room_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_view_room_translation.name', 'Like', '%' . $request->get('name') . '%');
                    });

                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_hotel_room_translation.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_view_room_translation.name', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('hotel_id')){
                    $sql_rooms->where('mo_hotel_room.hotel_id',$request->get('hotel_id'));
                    $sub->where('mo_hotel_room.hotel_id',$request->get('hotel_id'));
                }

                if ($request->get('category_id')){
                    $sql_rooms->where('mo_hotel_room.category_id',$request->get('category_id'));
                    $sub->where('mo_hotel_room.category_id',$request->get('category_id'));
                }


                if ($request->get('published') != '') {
                    if ($request->get('published') == '1') {
                        $sql_rooms->Where(function ($query) {
                            $query->where('mo_view_room.published', '=', '1');

                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_room.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '0');
                            });
                        });
                        $sub->Where(function ($query) {
                            $query->where('mo_view_room.published', '=', '1');
                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_room.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '0');
                            });
                        });
                    } elseif ($request->get('published') == '0') {
                        $sql_rooms->Where(function ($query) {
                            $query->where('mo_view_room.published', '=', '0');
                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_room.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '1');
                            });
                        });
                        $sub->Where(function ($query) {
                            $query->where('mo_view_room.published', '=', '0');
                            $query->orWhere(function ($query) {
                                $query->WhereNull('mo_view_room.published')
                                    ->Where('mo_subchannel.view_blocked', '=', '1');
                            });
                        });
                    }
                }

                // Order
                $orden = 'mo_hotel_room_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_hotel_room_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_hotel_room_translation.description';
                            break;
                        case 'short_description':
                            $orden = 'mo_hotel_room_translation.short_description';
                            break;
                        case 'id':
                            $orden = 'mo_hotel_room.id';
                            break;
                        case 'order':
                            $orden = 'mo_view_room.order';
                            break;
                        default:
                            $orden = 'mo_hotel_room_translation.name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }

                // FIN Order_way


                $sql_rooms = $sql_rooms->groupBy('mo_hotel_room.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_rooms = $sql_rooms->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_rooms = $sql_rooms->get();

                }

                //Fin de paginación

                $sub->groupBy('mo_hotel_room.id');
                $rooms_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $rooms_count->count();


                $array['data'] = array();
                $rooms_aux = array();

                foreach ($datos_rooms as $room) {

                    $rooms_aux['lang'] = array();

                    //Habitación (por canal)

                    if ($room->view_id != null) {
                        $rooms_aux = [
                            'id' => $room->id,
                            'hotel_id' => $room->hotel_id,
                            'subchannel_id' => $room->subchannel_id,
                            'published' => $room->view_published,
                            // Campos especiales
                            'order' => $room->view_order,
                            // FIN Campos especiales
                            'category_id' => $room->category_id,
                            'code' => $room->code,
                            'uom_id' => $room->uom_id,
                            'longitude' => $room->longitude,
                            'latitude' => $room->latitude,
                            'address' => $room->address,
                            'pax' => $room->pax,
                        ];
                        foreach ($idiomas as $idi) {
                            $traduccion = ViewRoomTranslation::where('view_room_id', '=', $room->view_id)
                                ->select('id', 'language_id', 'name', 'description', 'services_included', 'services_not_included', 'short_description', 'location', 'views', 'size', 'capacity', 'url_360','friendly_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                    'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $rooms_aux['lang'][][$idi->abbreviation] = $trad;
                            }
                        }
                        //Habitación (general)
                    } else {
                        $rooms_aux = [
                            'id' => $room->id,
                            'hotel_id' => $room->hotel_id,
                            'subchannel_id' => $room->subchannel_id,
                            'published' => ($room->view_blocked == 1) ? 0 : 1,
                            // Campos especiales
                            'order' => $room->order,
                            // FIN Campos especiales
                            'category_id' => $room->category_id,
                            'code' => $room->code,
                            'uom_id' => $room->uom_id,
                            'longitude' => $room->longitude,
                            'latitude' => $room->latitude,
                            'address' => $room->address,
                            'pax' => $room->pax,
                        ];


                        foreach ($idiomas as $idi) {
                            $traduccion = HotelRoomTranslation::where('room_id', '=', $room->id)
                                ->select('id', 'language_id', 'name', 'description', 'services_included', 'services_not_included', 'short_description', 'location', 'views', 'size', 'capacity', 'url_360', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                    'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $rooms_aux['lang'][][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    //Generación de array de salida
                    $array['data'][0]['viewroom'][] = $rooms_aux;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de habitaciones');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar la visualización por subcanal de una habitación
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'global' => 'required|boolean',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $subcanal = $request->get('subchannel_id');
                $fecha_actual = Carbon::now()->format('Y-m-d');

                //Preparación de las consultas
                $datos_rooms = DB::connection('tenant')->table('mo_hotel_room')
                    ->select('mo_hotel_room.*',
                        'mo_view_room.id as view_id', 'mo_view_room.room_id as view_room_id', 'mo_view_room.subchannel_id as view_subchannel_id',
                        'mo_view_room.order as view_order', 'mo_subchannel.view_blocked',
                        'mo_view_room.published as view_published')
                    ->where('mo_hotel_room.id', '=', $id)
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->join('mo_hotel_room_translation', 'mo_hotel_room.id', '=', 'mo_hotel_room_translation.room_id')
                    ->whereNull('mo_hotel_room_translation.deleted_at')

                    ->join('mo_product','mo_product.id','mo_hotel_room.hotel_id')
                    ->whereNull('mo_product.deleted_at')
                    ->where('mo_product.type_id','=',2)
                    ->join('mo_price_product','mo_price_product.product_id','mo_product.id')
                    ->whereNull('mo_price_product.service_id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->join('mo_price','mo_price.id','mo_price_product.price_id')
                    ->whereNull('mo_price.deleted_at')
                    ->where('mo_price.date_start', '<=', $fecha_actual)
                    ->where('mo_price.date_end', '>=', $fecha_actual)
                    ->join('mo_contract_model','mo_price.contract_model_id','mo_contract_model.id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel','mo_subchannel.contract_model_id','mo_price.contract_model_id')
                    ->whereNull('mo_subchannel.deleted_at')
                    ->where('mo_subchannel.id', '=', $subcanal)

                    ->leftJoin('mo_view_room', function ($join) use ($subcanal) {
                        $join->on('mo_view_room.room_id', '=', 'mo_hotel_room.id')->where('mo_view_room.subchannel_id', '=', $subcanal)->whereNull('mo_view_room.deleted_at');
                    })
                    ->leftJoin('mo_view_room_translation', function ($join) {
                        $join->on('mo_view_room.id', '=', 'mo_view_room_translation.view_room_id')->whereNull('mo_view_room_translation.deleted_at');
                    })->groupBy('mo_hotel_room.id')->get();


                //ruta de storage de settings para concatenar a ruta de archivo de habitación
                $storage = Settings::where('name', 'storage_files')->first()->value;

                $files_view = DB::connection('tenant')->table('mo_view_room_file')
                    ->select('mo_file.id', 'mo_file.file_name', 'mo_file.file_size', 'mo_file.file_dimensions', 'mo_file.mimetype', 'mo_file.type_id', 'mo_file.order', 'mo_file.url_file',
                        'mo_file.device_id','mo_file.key_file')
                    ->where('mo_view_room_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_view_room_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->where('mo_file_type.deleted_at', null)
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    });

                $files_no_view = DB::connection('tenant')->table('mo_hotel_room_file')
                    ->select('mo_file.id', 'mo_file.file_name', 'mo_file.file_size', 'mo_file.file_dimensions', 'mo_file.mimetype', 'mo_file.type_id', 'mo_file.order', 'mo_file.url_file',
                        'mo_file.device_id','mo_file.key_file')
                    ->where('mo_hotel_room_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_hotel_room_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->where('mo_file_type.deleted_at', null)
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    });

                $array['data'] = array();

                foreach ($datos_rooms as $room) {

                    $room_aux = array();
                    $room_aux['lang'] = array();

                    //Habitación (por canal)
                    if ($room->view_id != null && $request->get('global') != 1) {
                        $room_aux = [
                            'id' => $room->id,
                            'hotel_id' => $room->hotel_id,
                            'category_id' => $room->category_id,
                            'subchannel_id' => $subcanal,
                            'published' => $room->view_published,
                            // Campos especiales
                            'order' => $room->view_order,
                            // FIN Campos especiales
                            'code' => $room->code,
                            'uom_id' => $room->uom_id,
                            'longitude' => $room->longitude,
                            'latitude' => $room->latitude,
                            'address' => $room->address,
                            'pax' => $room->pax,
                        ];

                        foreach ($idiomas as $idi) {
                            $traduccion = ViewRoomTranslation::where('view_room_id', '=', $room->view_id)
                                ->select('id', 'language_id', 'name', 'description', 'services_included', 'services_not_included', 'short_description', 'location', 'views', 'size', 'capacity', 'url_360', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                    'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                if ($trad->og_image && $trad->og_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->og_image)->first()->url_file;
                                    $trad->og_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->og_image_route = null;
                                }
                                if ($trad->twitter_image && $trad->twitter_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->twitter_image)->first()->url_file;
                                    $trad->twitter_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->twitter_image_route = null;
                                }
                                $room_aux['lang'][][$idi->abbreviation] = $trad;
                            }
                        }

                        $files = $files_view->where('mo_view_room_file.view_room_id', '=', $room->view_id)
                            ->groupBy('mo_file.id')->get();

                        //Habitación (general)
                    } else {

                        $room_aux = [
                            'id' => $room->id,
                            'hotel_id' => $room->hotel_id,
                            'category_id' => $room->category_id,
                            'subchannel_id' => $subcanal,
                            'published' => ($room->view_blocked == 1) ? 0 : 1,
                            // Campos especiales
                            'order' => $room->order,
                            // FIN Campos especiales
                            'code' => $room->code,
                            'uom_id' => $room->uom_id,
                            'longitude' => $room->longitude,
                            'latitude' => $room->latitude,
                            'address' => $room->address,
                            'pax' => $room->pax,
                        ];

                        foreach ($idiomas as $idi) {
                            $traduccion = HotelRoomTranslation::where('room_id', '=', $room->id)
                                ->select('id', 'language_id', 'name', 'description', 'services_included', 'services_not_included', 'short_description', 'location', 'views', 'size', 'capacity', 'url_360', 'friendly_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                    'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                if ($trad->og_image && $trad->og_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->og_image)->first()->url_file;
                                    $trad->og_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->og_image_route = null;
                                }
                                if ($trad->twitter_image && $trad->twitter_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->twitter_image)->first()->url_file;
                                    $trad->twitter_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->twitter_image_route = null;
                                }
                                $room_aux['lang'][][$idi->abbreviation] = $trad;
                            }
                        }

                        $files = $files_no_view->where('mo_hotel_room_file.room_id', '=', $room->id)
                            ->groupBy('mo_file.id')->get();
                    }

                    //archivos asociados a la habitación
                    $room_aux['files'] = array();
                    $array_file = array();
                    foreach ($files as $file) {
                        $array_file = ['id' => $file->id, 'file_name' => $file->file_name, 'file_size' => $file->file_size,
                            'file_dimensions' => $file->file_dimensions, 'mimetype' => $file->mimetype, 'order' => $file->order, 'url_file' => $storage . $file->url_file,
                            'device_id' => $file->device_id,'key_file' => $file->key_file,'type' => ($file->type_id != null) ? ['id' => $file->type_id] : null];
                        foreach ($idiomas as $idi) {

                            $traducciones_files = FileTranslation::where('file_id', $file->id)
                                ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_translation.file_id')
                                ->get();

                            foreach ($traducciones_files as $traduccion_file) {
                                $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                            }

                            if ($file->type_id != null) {
                                $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                    ->select('id', 'language_id', 'name')
                                    ->where('language_id', '=', $idi->id)
                                    ->groupBy('mo_file_type_translation.type_id')
                                    ->get();

                                foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                    $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                                }
                            }

                        }
                        $room_aux['files'][] = $array_file;

                    }
                    //Generación de array de salida
                    $array['data'][0]['viewroom'][] = $room_aux;
                }
            }
            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de habitaciones');
        }
        return response()->json($array, $array['error']);
    }
}