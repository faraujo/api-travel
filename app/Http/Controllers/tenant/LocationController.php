<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Models\tenant\Location;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class LocationController extends Controller
{

    /**
     * Función que lista todas las localizaciones
     *
     * Para la consulta de localizaciones se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::all();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
                'depends_on' => 'integer|min:0'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                //Precompiladas
                $locations = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id', 'mo_location.name', 'mo_location.depends_on as depends_on_id')
                    ->where('mo_location.deleted_at', '=', null);

               
                $sub = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id')
                    ->where('mo_location.deleted_at', '=', null);
                
                if ($request->get('name') != '') {
                    $locations->where('mo_location.name', 'Like', '%' . $request->get('name') . '%');
                    $sub->where('mo_location.name', 'Like', '%' . $request->get('name') . '%');
                }

                if ($request->get('depends_on') != '') {
                    $locations->where('mo_location.depends_on', '=', $request->get('depends_on'));
                    $sub->where('mo_location.depends_on', '=', $request->get('depends_on'));
                }
                //Fin precompiladas

                // Order
                $orden = 'mo_location.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_location.id';
                            break;
                        case 'name':
                            $orden = 'mo_location.name';
                            break;
                        default:
                            $orden = 'mo_location.id';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $locations = $locations->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $locations = $locations->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $locations = $locations->get();

                }

                //Fin de paginación

                //Total de resultados

                $locations_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $locations_count->count();

                //Generación de salida

                $array['data'] = array();

                foreach ($locations as $location) {
                    $location->depends_on = null;

                    foreach ($idiomas as $idioma) {

                        $datos_depends_on_traduccion = DB::connection('tenant')->table('mo_product')
                            ->select('mo_product_translation.language_id', 'mo_product_translation.name')
                            ->whereNull('mo_product.deleted_at')
                            ->where('mo_product.id', '=', $location->depends_on_id)
                            ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                            ->whereNull('mo_product_translation.deleted_at')
                            ->where('mo_product_translation.language_id', '=', $idioma->id)
                            ->get();
                        foreach ($datos_depends_on_traduccion as $depends_on_traduccion) {
                            $location->depends_on['id'] = $location->depends_on_id;
                            $location->depends_on['lang'][][$idioma->abbreviation] = $depends_on_traduccion;
                        }
                    }

                    $location_aux = [
                        'id' => $location->id,
                        'name' => $location->name,
                        'depends_on' => $location->depends_on,
                    ];

                    $array['data'][0]['location'][] = $location_aux;

                }

                $array['total_results'] = $totales;
                //fin resumen

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra una localización buscada por su id
     *
     * Para la consulta de una localización se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id,Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::all();

          

                //Precompiladas
                $locations = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id', 'mo_location.name', 'mo_location.depends_on as depends_on_id')
                    ->where('mo_location.deleted_at', '=', null)
                    ->where('id','=',$id);

                $sub = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id')
                    ->where('mo_location.deleted_at', '=', null)
                    ->where('id','=',$id);
                //Fin precompiladas

                $locations = $locations->get();

                //Total de resultados

                $locations_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $locations_count->count();

                //Generación de salida

                $array['data'] = array();

                foreach ($locations as $location) {
                    $location->depends_on = null;

                    foreach ($idiomas as $idioma) {

                        $datos_depends_on_traduccion = DB::connection('tenant')->table('mo_product')
                            ->select('mo_product_translation.language_id', 'mo_product_translation.name')
                            ->whereNull('mo_product.deleted_at')
                            ->where('mo_product.id', '=', $location->depends_on_id)
                            ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                            ->whereNull('mo_product_translation.deleted_at')
                            ->where('mo_product_translation.language_id', '=', $idioma->id)
                            ->get();
                        foreach ($datos_depends_on_traduccion as $depends_on_traduccion) {
                            $location->depends_on['id'] = $location->depends_on_id;
                            $location->depends_on['lang'][][$idioma->abbreviation] = $depends_on_traduccion;
                        }
                    }

                    $location_aux = [
                        'id' => $location->id,
                        'name' => $location->name,
                        'depends_on' => $location->depends_on,
                    ];

                    $array['data'][0]['location'][] = $location_aux;

                }

                $array['total_results'] = $totales;
                //fin resumen

            

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para la creación de Localizaciones
     *
     * Para la creación de localizaciones se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'depends_on' => 'exists:tenant.mo_product,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                $rule_codes = [];

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_location = $rules->where('code', 'product_location');

                            if($rules_location) {
                                foreach($rules_location as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'product_location') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if ($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'product_location';
                    } else if ($cantidad > 0) {
                        $total_locations = Location::get();

                        if(count($total_locations) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'product_location';
                        }
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';
                    
                } else {

                    // Crear localizacion
                    $supplier = Location::create([
                        'name' => $request->get('name') != '' ? $request->get('name') : null,
                        'depends_on' => $request->get('depends_on') != '' ? $request->get('depends_on') : null,
                    ]);
                    // FIN crear compañía
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la eliminación de localizaciones.
     *
     * Para la eliminación de localizaciones se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_location,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $location_id) {
                        $location = Location::where('id', $location_id)->first();
                        // borrado de localización
                        $location->delete();
                        //fin localización

                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar una localización.
     *
     * Para la modificación de una localización se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'name' => 'required',
                'depends_on' => 'exists:tenant.mo_product,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $location = Location::find($request->get('id'));
                if (!$location) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    // Actualizar localización
                    $location->name = $request->get('name') != '' ? $request->get('name') : null;
                    $location->depends_on = $request->get('depends_on') != '' ? $request->get('depends_on') : null;

                    $location->save();
                    //FIN actualizar localización

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Localizaciones');
        }
        return response()->json($array, $array['error']);
    }
}