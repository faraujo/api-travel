<?php

namespace App\Http\Controllers\tenant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\tenant\Settings;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use Illuminate\Support\Str;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\WebSlider;
use App\Models\tenant\WebSliderFile;

class WebSliderController extends Controller
{
    //
    public function show(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();
            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_slider = DB::connection('tenant')->table('mo_web_slider')
                    ->select(
                        'mo_web_slider.id',
                        'mo_web_slider.name'
                    )
                    ->whereNull('mo_web_slider.deleted_at')
                    ->leftjoin('mo_web_slider_file',function ($join) {
                        $join->on('mo_web_slider_file.slider_id','mo_web_slider.id')
                            ->whereNull('mo_web_slider_file.deleted_at');
                    });

                $sub = DB::connection('tenant')->table('mo_web_slider')
                    ->select(
                        'mo_web_slider.id'
                    )
                    ->whereNull('mo_web_slider.deleted_at')
                    ->leftjoin('mo_web_slider_file',function ($join) {
                        $join->on('mo_web_slider_file.slider_id','mo_web_slider.id')
                            ->whereNull('mo_web_slider_file.deleted_at');
                    });

                if($request->get('name') != '') {
                    $sql_slider->where('mo_web_slider.name', 'like', '%'.$request->get('name').'%');
                    $sub->where('mo_web_slider.name', 'like', '%'.$request->get('name').'%');
                }

                $orden = 'mo_web_slider.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_web_slider.id';
                            break;
                        case 'name':
                            $orden = 'mo_web_slider.name';
                            break;
                        default:
                            $orden = 'mo_web_slider.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_slider = $sql_slider->groupBy('mo_web_slider.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_slider = $sql_slider->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_slider = $sql_slider->get();

                }
                //Fin de paginación

                $sub->groupBy('mo_web_slider.id');

                $slider_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $slider_count->count();

                $array['data'] = array();
                foreach ($datos_slider as $slider) {
                    $array_slider = array();

                    $array_slider['id'] = $slider->id;
                    $array_slider['name'] = $slider->name;

                    $array['data'][0]['slider'][] = $array_slider;

                }
                
                $array['total_results'] = $totales;
                
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Slider');

        }

        return response()->json($array, $array['error']);
    }

    public function showId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            $idiomas = Language::where('front', 1)->get();

            $sql_slider = DB::connection('tenant')->table('mo_web_slider')
                ->select(
                    'mo_web_slider.id',
                    'mo_web_slider.name'
                )
                ->whereNull('mo_web_slider.deleted_at')
                ->leftjoin('mo_web_slider_file',function ($join) {
                    $join->on('mo_web_slider_file.slider_id','mo_web_slider.id')
                        ->whereNull('mo_web_slider_file.deleted_at');
                })
                ->where('mo_web_slider.id', $id)
                ->groupBy('mo_web_slider.id');

            $sql_files_slider = DB::connection('tenant')->table('mo_web_slider_file')
                ->select(
                    'mo_file.id',
                    'mo_file.file_name',
                    'mo_file.file_size',
                    'mo_file.file_dimensions',
                    'mo_file.mimetype',
                    'mo_file.order',
                    'mo_file.url_file',
                    'mo_file.device_id',
                    'mo_file.key_file',
                    'mo_file.type_id'
                )
                ->whereNull('mo_web_slider_file.deleted_at')
                ->join('mo_file','mo_file.id','mo_web_slider_file.file_id')
                ->whereNull('mo_file.deleted_at')
                ->join('mo_file_translation','mo_file_translation.file_id','mo_file.id')
                ->whereNull('mo_file_translation.deleted_at')
                ->where('mo_web_slider_file.slider_id', $id)
                ->groupBy('mo_file.id');
            $datos_slider = $sql_slider->get();

            //ruta de storage de settings para concatenar a ruta de archivo de producto
            $storage = Settings::where('name', 'storage_files')->first()->value;

            $array['data'] = array();

            foreach ($datos_slider as $slider) {

                $array_slider = array();

                $array_slider['id'] = $slider->id;
                $array_slider['name'] = $slider->name;

                $array_files = array();

                $datos_file = $sql_files_slider->get();

                foreach($datos_file as $file) {
                    $tipo = FileType::find($file->type_id);

                    $array_file = [
                        'id' => $file->id, 
                        'file_name' => $file->file_name, 
                        'file_size' => convertExtension($file->file_size),
                        'file_dimensions' => $file->file_dimensions, 
                        'mimetype' => $file->mimetype, 
                        'order' => $file->order, 
                        'url_file' => $storage . $file->url_file,
                        'device_id' => $file->device_id,
                        'key_file' => $file->key_file,
                        'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];
                    foreach ($idiomas as $idi) {

                        //si el tipo no existe o está borrado no busca sus traducciones
                        if ($tipo) {
                            $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_type_translation.type_id')
                                ->get();

                            foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                            }
                        }

                        $traducciones_files = FileTranslation::where('file_id', $file->id)
                            ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                            ->where('language_id', '=', $idi->id)
                            ->groupBy('mo_file_translation.file_id')
                            ->get();

                        foreach ($traducciones_files as $traduccion_file) {
                            $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                        }
                    }
                    $array_slider['files'][] = $array_file;
                }

                $array['data'][0]['slider'][] = $array_slider;
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage() . ' ' . $e->getLine();
            reportService($e, 'Slider');

        }

        return response()->json($array, $array['error']);

    }

    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // FIN validación
            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $slider = WebSlider::find($request->get('id'));
                if (!$slider) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                        $slider->sliderFiles()->delete();
                        //Crear relación con archivo
                        if ($request->get('files') != '') {
                            foreach ($request->get('files') as $file) {
                                WebSliderFile::create([
                                    'slider_id' => $slider->id,
                                    'file_id' => $file,
                                ]);
                            }
                        }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Slider');
        }
        return response()->json($array, $array['error']);
    }

}
