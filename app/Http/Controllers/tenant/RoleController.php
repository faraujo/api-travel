<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\RolePermission;
use App\Models\tenant\Role;
use App\Models\tenant\UserRole;
use App\Models\tenant\Settings;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RoleController extends Controller
{


    /**
     * Creación de roles
     *
     * Para la creación de roles se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'name' => 'required|unique:tenant.mo_role,name,NULL,id,deleted_at,NULL',
                'permissions' => 'array',
                'permissions.general' => 'array',
                'permissions.general.*' => 'integer|min:0|exists:tenant.mo_permission,id,deleted_at,NULL|distinct',
                'permissions.globalaccess' => 'array',
                'permissions.globalaccess.*' => 'integer|min:0|exists:tenant.mo_permission,id,deleted_at,NULL|distinct',
                'permissions.subchannel' => 'array',
                'permissions.subchannel.*.id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
            ]);


            //validación para separar permisos duplicados para diferentes subcanales
            if ($request->get('permissions') != '' && isset($request->get('permissions')['subchannel'])) {
                $contador = 0;
                foreach ($request->get('permissions')['subchannel'] as $subchannel) {
                    $subchannel['id'] = array();
                    if (isset($subchannel['permissions'])) {
                        foreach ($subchannel['permissions'] as $permission) {
                            if (in_array($permission, $subchannel['id'])) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['The permissions.subchannel.'.$contador => 'The permissions.subchannel.'.$contador.' field has a duplicate value.']);
                            } else {
                                array_push($subchannel['id'], $permission);
                            }
                        }
                    }
                    $contador++;
                }
            }

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;
                    

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_role = $rules->where('code', 'user_role');

                            if($rules_role) {
                                foreach($rules_role as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'user_role') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'user_role';
                    } else if ($cantidad > 0) {
                        $total_roles = Role::get();

                        if(count($total_roles) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'user_role';
                        }
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {
                    $role = Role::create([
                        'name' => $request->get('name'),
                        'description' => $request->get('description') != '' ? $request->get('description') : null,
                    ]);

                    if ($request->get('permissions')) {
                        if (isset($request->get('permissions')['general'])) {
                            foreach ($request->get('permissions')['general'] as $permission) {
                                RolePermission::create([
                                    'role_id' => $role->id,
                                    'permission_id' => $permission,
                                ]);
                            }
                        }

                        if (isset($request->get('permissions')['globalaccess'])) {
                            foreach ($request->get('permissions')['globalaccess'] as $permission) {
                                RolePermission::create([
                                    'role_id' => $role->id,
                                    'permission_id' => $permission,
                                ]);
                            }
                        }

                        if (isset($request->get('permissions')['subchannel'])) {
                            foreach ($request->get('permissions')['subchannel'] as $subchannel) {
                                if (isset($subchannel['permissions'])) {
                                    foreach ($subchannel['permissions'] as $permission) {
                                        RolePermission::create([
                                            'role_id' => $role->id,
                                            'permission_id' => $permission,
                                            'subchannel_id' => $subchannel['id'],
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Roles');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Eliminación de roles
     *
     * Para la eliminación de roles se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Rol administrador no modificable
            if($error == 0 && in_array(1, $request->get('ids'))){
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['admin' => ['Admin role is not modifiable']]);
            }
            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = \Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_role,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $role_id) {
                        $role = Role::where('id', $role_id)->first();
                        $role->permissionRole()->delete();
                        $role->roleUser()->delete();
                        $role->delete();

                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Roles');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Consulta de roles
     *
     * Para la consulta de roles se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable
     * “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'user_id' => 'integer|min:0',
                'permission_id' => 'integer|min:0',
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $sql_roles = DB::connection('tenant')->table('mo_role')
                    ->select('mo_role.id', 'mo_role.name', 'mo_role.description')
                    ->leftJoin('mo_role_permission', function ($join) {
                        $join->on('mo_role_permission.role_id', '=', 'mo_role.id')
                            ->where('mo_role_permission.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_permission', function ($join) {
                        $join->on('mo_permission.id', '=', 'mo_role_permission.permission_id')
                            ->where('mo_permission.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user_role', function ($join) {
                        $join->on('mo_user_role.role_id', 'mo_role.id')
                            ->where('mo_user_role.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user', function ($join) {
                        $join->on('mo_user.id', 'mo_user_role.user_id')
                            ->where('mo_user.deleted_at', '=', null);
                    })
                    ->where('mo_role.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_role')
                    ->select('mo_role.id')
                    ->leftJoin('mo_role_permission', function ($join) {
                        $join->on('mo_role_permission.role_id', '=', 'mo_role.id')
                            ->where('mo_role_permission.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_permission', function ($join) {
                        $join->on('mo_permission.id', '=', 'mo_role_permission.permission_id')
                            ->where('mo_permission.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user_role', function ($join) {
                        $join->on('mo_user_role.role_id', 'mo_role.id')
                            ->where('mo_user_role.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user', function ($join) {
                        $join->on('mo_user_role.user_id', 'mo_user.id')
                            ->where('mo_user.deleted_at', '=', null);
                    })
                    ->where('mo_role.deleted_at', '=', null);

                $sql_permissions = DB::connection('tenant')->table('mo_role_permission')
                    ->select('mo_permission.id')
                    ->join('mo_permission', 'mo_permission.id', 'mo_role_permission.permission_id')
                    ->where('mo_role_permission.deleted_at', '=', null)
                    ->where('mo_permission.deleted_at', '=', null);

                if ($request->get('name') != '') {
                    $sql_roles->where(function ($query) use ($request) {
                        $query->where('mo_role.name', 'LIKE', '%' . $request->get('name') . '%')
                            ->orWhere('mo_role.description', 'LIKE', '%' . $request->get('name') . '%');
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_role.name', 'LIKE', '%' . $request->get('name') . '%')
                            ->orWhere('mo_role.description', 'LIKE', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('user_id') != '') {
                    $sql_roles->where('mo_user.id', '=', $request->get('user_id'));
                    $sub->where('mo_user.id', '=', $request->get('user_id'));
                }

                if ($request->get('permission_id') != '') {
                    $sql_roles->where('mo_permission.id', '=', $request->get('permission_id'));
                    $sub->where('mo_permission.id', '=', $request->get('permission_id'));
                }

                // Order
                $orden = 'mo_role.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_role.name';
                            break;
                        case 'description':
                            $orden = 'mo_role.description';
                            break;
                        case 'id':
                            $orden = 'mo_role.id';
                            break;
                        default:
                            $orden = 'mo_role.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_roles = $sql_roles->groupBy('mo_role.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $sql_roles = $sql_roles->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $sql_roles = $sql_roles->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_role.id');
                $roles_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $roles_count->count();


                $array['data'] = array();

                //recorrido de los roles obtenidos
                foreach ($sql_roles as $rol) {

                    $array['data'][0]['role'][] = $rol;
                }

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Roles');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Asigna roles a usuario
     *
     * Para asignar roles a un usuario se realiza un petición POST.
     * Borra todas las relaciones existentes del usuario con los roles y crea las nuevas relaciones segun los roles que se le especifica.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'user_id' => 'required|integer|min:0',
                'roles' => 'array',
                'roles.*' => 'required|integer|exists:tenant.mo_role,id,deleted_at,NULL|distinct|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Comprobación de administradores resultantes previo a modificación (si los roles a asignar no contienen el propio rol administrador)
            if($error == 0 && $request->get('roles') && is_array($request->get('roles')) && !in_array(1, $request->get('roles'))){
                $admin_number = DB::connection('tenant')->table('mo_user_role')
                ->select('user_id')
                ->join('mo_user', 'mo_user.id', '=', 'mo_user_role.user_id')
                ->whereNull('mo_user_role.deleted_at')
                ->whereNull('mo_user.deleted_at')
                ->where('role_id', 1)->whereNotIn('user_id', [$request->get('user_id')])
                ->count();
                    
                if ($admin_number == 0) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['admin' => ['At least one admin user is required']]);
                }
            }

            // Fin de validación

            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $validator = \Validator::make($request->all(), [
                    'user_id' => 'exists:tenant.mo_user,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    //Busca y borra todas las relaciones de roles con usuarios
                    UserRole::where('user_id', '=', $request->get('user_id'))->delete();

                    //Si recibe parámetro roles recorre el array y crea la relación con el usuario
                    if ($request->get('roles')) {
                        foreach ($request->get('roles') as $role) {
                            UserRole::create([
                                'user_id' => $request->get('user_id'),
                                'role_id' => $role,
                            ]);
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Roles');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Modificación de roles
     *
     * Para la modificación de roles se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'name' => 'required',
                'permissions' => 'array',
                'permissions.general' => 'array',
                'permissions.general.*' => 'integer|min:0|exists:tenant.mo_permission,id,deleted_at,NULL|distinct',
                'permissions.globalaccess' => 'array',
                'permissions.globalaccess.*' => 'integer|min:0|exists:tenant.mo_permission,id,deleted_at,NULL|distinct',
                'permissions.subchannel' => 'array',
                'permissions.subchannel.*.id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
            ]);

            //Rol administrador no modificable
            if($request->get('id') && $request->get('id') == 1){
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['admin' => ['Admin role is not modifiable']]);
            }

            //validación para separar permisos duplicados para diferentes subcanales
            if ($request->get('permissions') != '' && isset($request->get('permissions')['subchannel'])) {
                $contador = 0;
                foreach ($request->get('permissions')['subchannel'] as $subchannel) {
                    $subchannel['id'] = array();
                    if (isset($subchannel['permissions'])) {
                        foreach ($subchannel['permissions'] as $permission) {
                            if (in_array($permission, $subchannel['id'])) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['The permissions.subchannel.'.$contador => 'The permissions.subchannel.'.$contador.' field has a duplicate value.']);
                            } else {
                                array_push($subchannel['id'], $permission);
                            }
                        }
                    }
                    $contador++;
                }
            }

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            } else {
                // Fin validación
                $rol = Role::find($request->get('id'));
                if (!$rol) {
                    $error = 2;
                } else {
                    if ($rol->name != $request->get('name')) {
                        $validator = \Validator::make($request->all(), [
                            'name' => 'unique:tenant.mo_role,name,' . $request->get('id') . ',id,deleted_at,NULL',
                        ]);

                        if ($validator->fails()) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge($validator->errors());
                        }
                    }
                }
            }

            if ($error == 0) {

                $rol->update([
                    'name' => $request->get('name'),
                    'description' => $request->get('description') != '' ? $request->get('description') : null,
                ]);

                //Busca y borra todos los permisos asociadas al rol
                $rol->permissionRole()->delete();

                //Guarda asociación de permisos generales (admin 0 y subchannel 0) al rol
                if ($request->get('permissions')) {
                    if (isset($request->get('permissions')['general'])) {
                        foreach ($request->get('permissions')['general'] as $permission) {
                            RolePermission::create([
                                'role_id' => $rol->id,
                                'permission_id' => $permission,
                            ]);
                        }

                    }
                    //Guarda asociación de permisos globales (admin 1) al rol
                    if (isset($request->get('permissions')['globalaccess'])) {
                        foreach ($request->get('permissions')['globalaccess'] as $permission) {
                            RolePermission::create([
                                'role_id' => $rol->id,
                                'permission_id' => $permission,
                            ]);
                        }

                    }
                    //Guarda asociación de permisos por canal (subchannel 1) al rol
                    if (isset($request->get('permissions')['subchannel'])) {
                        foreach ($request->get('permissions')['subchannel'] as $subchannel) {
                            if (isset($subchannel['permissions'])) {
                                foreach ($subchannel['permissions'] as $permission) {
                                    RolePermission::create([
                                        'role_id' => $rol->id,
                                        'permission_id' => $permission,
                                        'subchannel_id' => $subchannel['id'],
                                    ]);
                                }
                            }
                        }
                    }
                }

            } elseif ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } elseif ($error == 2) {
                $array['error'] = 404;
                $array['error_description'] = 'Data not found';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Roles');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Consulta de un rol
     *
     * Para la consulta de un rol se realizará una petición GET. Si la operación no produce errores se devuelve, en la
     * variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $sql_role = DB::connection('tenant')->table('mo_role')
                ->select('mo_role.id', 'mo_role.name', 'mo_role.description')
                ->where('mo_role.deleted_at', '=', null)
                ->where('mo_role.id', '=', $id);

            $sql_permisos = DB::connection('tenant')->table('mo_role_permission')
                ->select('mo_permission.id')
                ->where('mo_role_permission.deleted_at', '=', null)
                ->join('mo_permission', 'mo_permission.id', 'mo_role_permission.permission_id')
                ->where('mo_permission.deleted_at', '=', null)
                ->where('mo_role_permission.role_id', '=', $id);

            $sql_permisos_general = DB::connection('tenant')->table('mo_role_permission')
                ->select('mo_permission.id')
                ->where('mo_role_permission.deleted_at', '=', null)
                ->join('mo_permission', 'mo_permission.id', 'mo_role_permission.permission_id')
                ->where('mo_permission.deleted_at', '=', null)
                ->where('mo_permission.admin', '=', 0)
                ->where('mo_permission.subchannel', '=', 0)
                ->where('mo_role_permission.role_id', '=', $id);

            $sql_permisos_global = DB::connection('tenant')->table('mo_role_permission')
                ->select('mo_permission.id')
                ->where('mo_role_permission.deleted_at', '=', null)
                ->join('mo_permission', 'mo_permission.id', 'mo_role_permission.permission_id')
                ->where('mo_permission.deleted_at', '=', null)
                ->where('mo_permission.admin', '=', 1)
                ->where('mo_role_permission.role_id', '=', $id);

            $sql_permisos_subcanal = DB::connection('tenant')->table('mo_role_permission')
                ->select('mo_permission.id')
                ->where('mo_role_permission.deleted_at', '=', null)
                ->join('mo_permission', 'mo_permission.id', 'mo_role_permission.permission_id')
                ->where('mo_permission.deleted_at', '=', null)
                ->where('mo_permission.subchannel', '=', 1)
                ->where('mo_role_permission.role_id', '=', $id);

            $datos_subcanales = DB::connection('tenant')->table('mo_subchannel')
                ->select('mo_subchannel.id')
                ->whereNull('mo_subchannel.deleted_at')
                ->join('mo_role_permission', 'mo_role_permission.subchannel_id', 'mo_subchannel.id')
                ->whereNull('mo_role_permission.deleted_at')
                ->join('mo_permission', 'mo_permission.id', 'mo_role_permission.permission_id')
                ->whereNull('mo_permission.deleted_at')
                ->where('mo_permission.subchannel', '=', 1)
                ->where('mo_role_permission.role_id', '=', $id)
                ->groupBy('mo_subchannel.id')
                ->get();

            $datos_role = $sql_role->first();

            $array['data'] = array();
            if ($datos_role != '') {
                $array_permisos_general = $sql_permisos_general->get();
                $array_permisos_global = $sql_permisos_global->get();

                $datos_role->permissions = array();

                $array_permissions = array();
                $array_permissions[]['general'][]['permissions'] = $array_permisos_general;
                $array_permissions[]['globalaccess'][]['permissions'] = $array_permisos_global;

                $array_subcanales = array();

                foreach ($datos_subcanales as $subcanal) {
                    $array_subcanal = array();
                    $array_subcanal['id'] = $subcanal->id;

                    $sql_permisos_subcanal_clone = clone $sql_permisos_subcanal;
                    $datos_permisos_subcanal = $sql_permisos_subcanal_clone->where('mo_role_permission.subchannel_id', '=', $subcanal->id)->get();

                    $array_subcanal['permissions'] = $datos_permisos_subcanal;

                    $array_subcanales[] = $array_subcanal;
                }

                $array_permissions[]['subchannel'] = $array_subcanales;

                $datos_role->permissions = $array_permissions;
                $array['data'][0]['role'][] = $datos_role;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Roles');
        }

        return response()->json($array, $array['error']);
    }
}