<?php

namespace App\Http\Controllers\tenant\web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Currency;
use App\Models\tenant\Settings;

class SettingsController extends Controller
{

    public function precharge(Request $request) {

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $monedas = Currency::where('front', 1)->get();

            $error = 0;

            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->where('front', 1)->first();

            }

            if ($request->get('currency') != '') {
                $moneda = $monedas->where('iso_code', $request->get('currency'))->where('front', 1)->first();
                if (!$moneda) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['currency' => ['The currency is not exists']]);
                }
            } else {
                $moneda_settings = Settings::where('name', 'moneda_defecto_id')->first();
                $moneda = $monedas->where('id', $moneda_settings->value)->where('front', 1)->first();

            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $array['data'][0] = settings($idioma, $request->get('subchannel_id'), $moneda, $request);              

            }


            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();

            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();

            reportService($e, 'WebMenu');
        }

        return response()->json($array, $array['error']);
    }
    
}