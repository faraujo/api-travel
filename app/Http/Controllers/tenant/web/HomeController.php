<?php

namespace App\Http\Controllers\tenant\web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Currency;
use App\Models\tenant\Settings;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{

    public function precharge(Request $request) {

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $monedas = Currency::where('front', 1)->get();

            $error = 0;

            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            } else {
                $idioma_settings = Settings::where('name', 'idioma_defecto_id')->first();
                $idioma = $idiomas->where('id', $idioma_settings->value)->where('front', 1)->first();

            }

            if ($request->get('currency') != '') {
                $moneda = $monedas->where('iso_code', $request->get('currency'))->where('front', 1)->first();
                if (!$moneda) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['currency' => ['The currency is not exists']]);
                }
            } else {
                $moneda_settings = Settings::where('name', 'moneda_defecto_id')->first();
                $moneda = $monedas->where('id', $moneda_settings->value)->where('front', 1)->first();

            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $array['data'][0] = settings($idioma, $request->get('subchannel_id'), $moneda, $request);

                $sql_slider = DB::connection('tenant')->table('mo_web_slider')
                    ->select(
                        'mo_web_slider.id',
                        'mo_web_slider.name'
                    )
                    ->whereNull('mo_web_slider.deleted_at')
                    ->join('mo_web_slider_file','mo_web_slider_file.slider_id','mo_web_slider.id')
                    ->whereNull('mo_web_slider_file.deleted_at')
                    ->join('mo_file','mo_file.id','mo_web_slider_file.file_id')
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_file_translation','mo_file_translation.file_id','mo_file.id')
                    ->whereNull('mo_file_translation.deleted_at')
                    ->where('mo_file_translation.language_id', $idioma->id)
                    ->groupBy('mo_web_slider.id');

                $sql_file_slider = DB::connection('tenant')->table('mo_file')
                        ->select(
                            'mo_file.id',
                            'mo_file.url_file',
                            'mo_file.device_id',
                            'mo_file.type_id',
                            'mo_file.key_file',
                            'mo_file.order',
                            'mo_file_translation.name',
                            'mo_file_translation.description',
                            'mo_file_translation.alternative_text'
                        )
                        ->whereNull('mo_file.deleted_at')
                        ->join('mo_web_slider_file','mo_web_slider_file.file_id','mo_file.id')
                        ->whereNull('mo_web_slider_file.deleted_at')
                        ->join('mo_file_translation','mo_file_translation.file_id','mo_file.id')
                        ->whereNull('mo_file_translation.deleted_at')
                        ->where('mo_file_translation.language_id', $idioma->id)
                        ->orderBy('mo_file.order', 'desc');

                $sql_client_types = DB::connection('tenant')->table('mo_client_type')
                    ->select(
                        'mo_client_type.id',
                        'mo_client_type_translation.name'
                    )
                    ->whereNull('mo_client_type.deleted_at')
                    ->join('mo_client_type_translation','mo_client_type_translation.client_type_id','mo_client_type.id')
                    ->whereNull('mo_client_type.deleted_at')
                    ->where('mo_client_type_translation.language_id', $idioma->id);

                $sql_payment_method = DB::connection('tenant')->table('mo_payment_method')
                    ->select(
                        'mo_payment_method.id',
                        'mo_payment_method.type',
                        'mo_payment_method_translation.name'
                    )
                    ->whereNull('mo_payment_method.deleted_at')
                    ->join('mo_payment_method_translation','mo_payment_method_translation.payment_method_id','mo_payment_method.id')
                    ->whereNull('mo_payment_method_translation.deleted_at')
                    ->where('mo_payment_method_translation.language_id',$idioma->id);
                $datos_slider = $sql_slider->get();

                $array['data'][0]['slider'] = array();

                foreach($datos_slider as $slider) {

                    $array_slider = array();

                    $array_slider['id'] = $slider->id;
                    $array_slider['name'] = $slider->name;

                    $array_slider['file'] = array();

                    $sql_file_slider_clone = clone $sql_file_slider;

                    $datos_file_slider = $sql_file_slider_clone->where('mo_web_slider_file.slider_id', $slider->id)
                        ->get();

                    foreach($datos_file_slider as $file) {
                        $array_file = array();

                        $array_file['id'] = $file->id;
                        $array_file['url_file'] = Storage::url($file->url_file);
                        $array_file['type_id'] = $file->type_id;
                        $array_file['device_id'] = $file->device_id;
                        $array_file['key_file'] = $file->key_file;
                        $array_file['order'] = $file->order;
                        $array_file['name'] = $file->name;
                        $array_file['description'] = $file->description;
                        $array_file['alternative_text'] = $file->alternative_text;

                        $array_slider['file'][] = $array_file;
                    }


                    $array['data'][0]['slider'][$slider->name] = $array_slider;
                }

                $array['data'][0]['client_type'] = array();

                $datos_client_types = $sql_client_types->get();

                foreach($datos_client_types as $client_type) {
                    $array_client_type = array();

                    $array_client_type['id'] = $client_type->id;
                    $array_client_type['name'] = $client_type->name;

                    $array['data'][0]['client_type'][] = $array_client_type;

                }

                /*$datos_payment_method = $sql_payment_method->get();

                $array['data'][0]['payment_method'] = array();

                foreach($datos_payment_method as $payment_method){
                    $array_payment_method = array();

                    $array_payment_method['id'] = $payment_method->id;
                    $array_payment_method['name'] = $payment_method->name;

                    $array['data'][0]['payment_method'][] = $array_payment_method;
                }*/

            }


            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();

            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();

            reportService($e, 'WebMenu');
        }

        return response()->json($array, $array['error']);
    }
    
}