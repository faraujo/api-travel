<?php

namespace App\Http\Controllers\tenant\web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Currency;
use App\Models\tenant\Settings;
use Illuminate\Support\Facades\Storage;

class LanguageController extends Controller
{

    public function show(Request $request) {

        $array['error'] = 200;

        try {
            
            DB::connection('tenant')->beginTransaction();



            $language_default = DB::connection('tenant')->table('mo_settings')->where('name', 'idioma_defecto_id')->first();

            $languages = languages();

            $array_languages = array();

            foreach($languages as $language) {

                $array_language = array();

                $array_language['id'] = $language['id'];
                $array_language['abbreviation'] = $language['abbreviation'];
                $array_language['name_lang'] = $language['name_lang'];

                if($language['id'] == $language_default->value) {
                    $array_language['language_default'] = 1;
                } else {
                    $array_language['language_default'] = 0;
                }

                $array_languages[] = $array_language;
            }



            $array['data'][0]['language'][0] = $array_languages;

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();

            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();

            reportService($e, 'WebMenu');
        }

        return response()->json($array, $array['error']);
    }
    
}