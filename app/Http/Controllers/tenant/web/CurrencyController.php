<?php

namespace App\Http\Controllers\tenant\web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\tenant\Language;
use App\Models\tenant\Currency;
use App\Models\tenant\Settings;
use Illuminate\Support\Facades\Storage;

class CurrencyController extends Controller
{

    public function show(Request $request) {

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $currency_default = DB::connection('tenant')->table('mo_settings')->where('name', 'moneda_defecto_id')->first();

            $currencies = currencies();

            $array_currencies = array();

            foreach($currencies as $currency) {

                $array_currency = array();

                $array_currency['id'] = $currency['id'];
                $array_currency['iso_code'] = $currency['iso_code'];
                $array_currency['name'] = $currency['name'];

                if($currency['id'] == $currency_default->value) {
                    $array_currency['currency_default'] = 1;
                } else {
                    $array_currency['currency_default'] = 0;
                }

                $array_currencies[] = $array_currency;
            }



            $array['data'][0]['currency'][0] = $array_currencies;

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();

            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();

            reportService($e, 'WebMenu');
        }

        return response()->json($array, $array['error']);
    }
    
}