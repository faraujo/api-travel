<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Company;
use App\Exceptions\Handler;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\Location;
use App\Models\tenant\ProductFile;
use App\Models\tenant\ProductLocation;
use App\Models\tenant\ProductService;
use App\Models\tenant\ProductTag;
use App\Models\tenant\Settings;
use App\Models\tenant\Type;
use App\Models\tenant\ProductCategory;
use App\Models\tenant\ProductPackage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Product;
use App\Models\tenant\ProductTranslation;
use App\Models\tenant\Language;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Función para la creación de un producto y sus traducciones
     *
     * Para la creación de productos se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     * Si el producto se crea con type_id 1 es un producto de tipo paquete con lo que se podrán enviar como
     * parámetros los productos que compondrán el paquete en el array product_package[].
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'type_id' => 'required|numeric',
                'code' => 'required',
                'company_id' => 'required|numeric',
                'order' => 'integer|min:0',
                'billable' => 'boolean',
                'height_from' => 'numeric',
                'height_to' => 'numeric',
                'weight_from' => 'numeric',
                'weight_to' => 'numeric',
                'longitude' => 'numeric',
                'latitude' => 'numeric',
                'pax' => 'required|boolean',
                'product_package' => 'array',
                'product_package.*.id' => 'required|integer|min:0|distinct',
                'product_package.*.location_id' => 'required|integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL',
                'services' => 'array',
                'services.*' => 'required|distinct|integer|min:0|exists:tenant.mo_service,id,deleted_at,NULL',
                'categories' => 'array',
                'categories.*' => 'required|distinct|integer|min:0|exists:tenant.mo_category,id,deleted_at,NULL',
                'tags' => 'array',
                'tags.*' => 'required|distinct|integer|min:0|exists:tenant.mo_tag,id,deleted_at,NULL',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                'locations' => 'array|required',
                'locations.*' => 'required|distinct|integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL',
                'uom_id' => 'required|integer|min:0|exists:tenant.mo_uom,id,deleted_at,NULL',
                'salable' => 'boolean',
                'iva_id' => 'required|integer|min:0|exists:tenant.mo_iva,id,deleted_at,NULL',

            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $tipo = Type::find($request->get('type_id'));
            if (!$tipo && $request->get('type_id') != '') {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['type_id' => ['0' => 'The type id is not exists.']]);
            }

            $compania = Company::find($request->get('company_id'));
            if (!$compania && $request->get('company_id') != '') {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['company_id' => ['0' => 'The company id is not exists.']]);
            }

            $product = Product::whereNull('mo_product.deleted_at')->where('mo_product.code', $request->get('code'))->first();

            if (!is_null($product)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['code' => ['0' => 'The code already exists.']]);
            }

            if ($request->get('type_id') == 1 && $request->get('product_package')) {
                foreach ($request->get('product_package') as $producto_paquete) {
                    if (isset($producto_paquete['id'])) {
                        $product_paquete = Product::find($producto_paquete['id']);
                        if ($product_paquete) {
                            if ($product_paquete->type_id == 1) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['product_package' => ['0' => 'The product type of the package can not be equal to package']]);
                            }
                            if ($product_paquete->type_id == 2) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['product_package' => ['0' => 'The product type of the package can not be equal to hotel']]);
                            }
                        }
                        if (!$product_paquete) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['product_package' => ['0' => 'The product is not exist in the products table']]);
                        }
                    }
                }
            }

            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('features')[$idioma->abbreviation]) ||
                    isset($request->get('recommendations')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation]) || isset($request->get('discover_url')[$idioma->abbreviation]) ||
                    isset($request->get('legal')[$idioma->abbreviation]) ||

                    isset($request->get('breadcrumb')[$idioma->abbreviation]) || isset($request->get('rel')[$idioma->abbreviation]) ||
                    isset($request->get('og_title')[$idioma->abbreviation]) || isset($request->get('og_description')[$idioma->abbreviation]) ||
                    isset($request->get('og_image')[$idioma->abbreviation]) || isset($request->get('twitter_title')[$idioma->abbreviation]) ||
                    isset($request->get('twitter_description')[$idioma->abbreviation]) || isset($request->get('twitter_image')[$idioma->abbreviation]) ||
                    isset($request->get('script_head')[$idioma->abbreviation]) || isset($request->get('canonical_url')[$idioma->abbreviation]) ||
                    isset($request->get('script_body')[$idioma->abbreviation]) || isset($request->get('script_footer')[$idioma->abbreviation]) ||
                    isset($request->get('index')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',

                        'index.' . $idioma->abbreviation => 'boolean',
                        'og_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                        'twitter_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                $rule_codes = [];

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();
            
                    $website = DB::table('saas_website')
                        ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                        'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                        'saas_website.default_language_id', 'saas_website.default_currency_id')
                        ->whereNull('saas_website.deleted_at')
                        ->where('saas_website.website_id', $website->id)
                        ->first();
            
                    $plans = websitePlanRules($website);

                    $cantidad = 0;
                    $servicios = 0;
                    $archivos = 0;
                    $product_rule_code = '';

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);

                            if($request->get('type_id') != 2 && $request->get('type_id') != 8){
                                $rules_product = $rules->where('code', 'products');

                                $product_rule_code = 'products';
                            }
    
                            if($request->get('type_id') == 2) {
                                $rules_product = $rules->where('code', 'product_hotel');

                                $product_rule_code = 'product_hotel';
                            }
    
                            if($request->get('type_id') == 8){
                                $rules_product = $rules->where('code', 'product_benefitcard');

                                $product_rule_code = 'product_benefitcard';
                            }
    
                            if ($rules_product) {
                                foreach($rules_product as $rule) {
                                    if($rule->unlimited == 1) {
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value){
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }

                            if($request->get('services') != ''){
                                $rules_service = $rules->where('code', 'product_service');
                                if($rules_service) {
                                    foreach($rules_service as $rule_service) {
                                        if($rule_service->unlimited == 1) {
                                            $servicios = -1;
                                        } else {
                                            if($servicios >= 0 && $servicios < $rule_service->value){
                                                $servicios = $rule_service->value;
                                            }
                                        }
                                    }
                                }
                            }

                            if($request->get('files') != '') {
                                $rules_files = $rules->where('code', 'product_file');
                                if($rules_files) {
                                    foreach($rules_files as $rule_file) {
                                        if($rule_file->unlimited == 1) {
                                            $archivos = -1;
                                        } else {
                                            if($archivos >= 0 && $archivos < $rule_file->value){
                                                $archivos = $rule_file->value;
                                            }
                                        }
                                    }
                                }
                            }
                        } else {

                            if($request->get('type_id') != 2 && $request->get('type_id') != 8 && $plan->rule_code == 'products'){
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
    
                            if($request->get('type_id') == 2 && $plan->rule_code == 'product_hotel') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
    
                            if($request->get('type_id') == 8 && $plan->rule_code == 'product_benefitcard' ){
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }

                            if($request->get('services') != '' && $plan->rule_code == 'product_service'){
                                if($plan->unlimited == 1){
                                    $servicios = -1;
                                }else{
                                    if($servicios >= 0 && $servicios < $plan->value){
                                        $servicios = $plan->value;
                                    }
                                }
                            }

                            if($request->get('files') != '' && $plan->rule_code == 'product_file'){
                                if($plan->unlimited == 1){
                                    $archivos = -1;
                                }else{
                                    if($archivos >= 0 && $archivos < $plan->value){
                                        $archivos = $plan->value;
                                    }
                                }
                            }
                        }
                    }
                    
                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = $product_rule_code;
                    } else if ($cantidad > 0) {
                        if($request->get('type_id') == 2 || $request->get('type_id') == 8) {
                            $total_productos = Product::where('type_id', $request->get('type_id'))
                            ->get();
                        } else {
                            $total_productos = Product::where('type_id', '!=', 2)
                            ->where('type_id', '!=', 8)
                            ->get();
                        
                        }

                        if(count($total_productos) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = $product_rule_code;
                        }
                    }

                    if($request->get('services') != '' && count($request->get('services')) > 0) {
                        if($servicios == 0) {
                            $error = 1;
                            $rule_codes[] = 'product_service';
                        } else if ($servicios > 0 && count($request->get('services')) > $servicios){
                            $error = 1;
                            $rule_codes[] = 'product_service';
                        }
                    }

                    if($request->get('files') != '' && count($request->get('files')) > 0){
                        if($archivos == 0){
                            $error = 1;
                            $rule_codes[] = 'product_file';
                        } else if ($archivos > 0  && count($request->get('files')) > $archivos) {
                            $error = 1;
                            $rule_codes[] = 'product_file';
                        }
                    }  
                }
                
                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {
                    // Crear el producto
                    $producto = Product::create([
                        'type_id' => $request->get('type_id'),
                        'code' => $request->get('code'),
                        'company_id' => $request->get('company_id'),
                        'uom_id' => $request->get('uom_id'),
                        'order' => ($request->get('order') != '') ? $request->get('order') : 0,
                        'billable' => ($request->get('billable') != '') ? $request->get('billable') : 1,
                        'height_from' => ($request->get('height_from') != '') ? $request->get('height_from') : null,
                        'height_to' => ($request->get('height_to') != '') ? $request->get('height_to') : null,
                        'weight_from' => ($request->get('weight_from') != '') ? $request->get('weight_from') : null,
                        'weight_to' => ($request->get('weight_to') != '') ? $request->get('weight_to') : null,
                        'longitude' => ($request->get('longitude') != '') ? $request->get('longitude') : null,
                        'latitude' => ($request->get('latitude') != '') ? $request->get('latitude') : null,
                        'address' => ($request->get('address') != '') ? $request->get('address') : null,
                        'pax' => $request->get('pax'),
                        'cost_center' => ($request->get('cost_center') != '') ? $request->get('cost_center') : null,
                        'accounting_account' => ($request->get('accounting_account') != '') ? $request->get('accounting_account') : null,
                        'salable' => ($request->get('salable') !== '' && !is_null($request->get('salable'))) ? $request->get('salable') : 1,
                        'iva_id' => $request->get('iva_id'),
                    ]);
                    // FIN crear el producto

                    // Crear las traducciones del producto
                    foreach ($array_traducciones as $idioma) {

                        //Preparación
                        $friendly = null;
                        if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                            $friendly = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                        } else {
                            if ($request->get('name')[$idioma->abbreviation] != '') {
                                $friendly = Str::slug($request->get('name')[$idioma->abbreviation]);
                            }
                        }
                        //

                        ProductTranslation::create(
                            [
                                'product_id' => $producto->id,
                                'name' => $request->get('name')[$idioma->abbreviation],
                                'language_id' => $idioma->id,
                                'description' => $request->get('description')[$idioma->abbreviation],
                                'features' => (isset($request->get('features')[$idioma->abbreviation]) && $request->get('features')[$idioma->abbreviation] != '') ? $request->get('features')[$idioma->abbreviation] : null,
                                'recommendations' => (isset($request->get('recommendations')[$idioma->abbreviation]) && $request->get('recommendations')[$idioma->abbreviation] != '') ? $request->get('recommendations')[$idioma->abbreviation] : null,
                                'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                'friendly_url' => $friendly,
                                'discover_url' => (isset($request->get('discover_url')[$idioma->abbreviation]) && $request->get('discover_url')[$idioma->abbreviation] != '') ? $request->get('discover_url')[$idioma->abbreviation] : null,
                                'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                                'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,

                                'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                                'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                                'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                                'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                                'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                                'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                                'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                                'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                                'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                                'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                                'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                                'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                                'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                            ]
                        );
                        if ($request->get('files') != '') {
                            foreach ($request->get('files') as $file) {
                                ProductFile::create([
                                    'product_id' => $producto->id,
                                    'file_id' => $file,
                                ]);
                            }
                        }


                    }
                    // FIN crear las traducciones del producto

                    // Crear la relación paquete-producto
                    if ($request->get('type_id') == 1 && $request->get('product_package') != '') {

                        foreach ($request->get('product_package') as $prod) {
                            ProductPackage::create([
                                'product_id' => $prod['id'],
                                'package_id' => $producto->id,
                                'location_id' => $prod['location_id'],
                            ]);
                        }

                    }
                    // FIN crear la relación paquete-producto

                    //Crear relaciones con servicios
                    if ($request->get('services') != '') {
                        foreach ($request->get('services') as $service) {
                            ProductService::create([
                                'product_id' => $producto->id,
                                'service_id' => $service,
                            ]);
                        }
                    }
                    // FIN crear relación con servicios

                    //Crear relaciones con categories
                    if ($request->get('categories') != '') {
                        foreach ($request->get('categories') as $category) {
                            ProductCategory::create([
                                'product_id' => $producto->id,
                                'category_id' => $category,
                            ]);
                        }
                    }
                    // FIN crear relación con categories

                    //Crear relaciones con tags
                    if ($request->get('tags') != '') {
                        foreach ($request->get('tags') as $tag) {
                            ProductTag::create([
                                'product_id' => $producto->id,
                                'tag_id' => $tag,
                            ]);
                        }
                    }
                    // FIN crear relación con tags

                    //Crear relación con location
                    foreach ($request->get('locations') as $location) {
                        ProductLocation::create([
                            'product_id' => $producto->id,
                            'location_id' => $location,
                        ]);
                    }
                    //fin crear relación con location
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Productos');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para la eliminación de uno o más productos.
     *
     * Para la eliminación de productos se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_product,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {

                    foreach ($request->get('ids') as $product_id) {

                        $producto = Product::where('id', $product_id)->first();
                        $producto->productPackagePack()->delete();
                        $producto->productPackagePro()->delete();
                        $producto->productTranslation()->delete();
                        $producto->delete();
                    }

                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Productos');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para modificar un producto.
     *
     * Para la modificación de productos se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'id' => 'required|numeric',
                'type_id' => 'required|numeric',
                'code' => 'required',
                'company_id' => 'required|numeric',
                'order' => 'integer|min:0',
                'billable' => 'boolean',
                'height_from' => 'numeric',
                'height_to' => 'numeric',
                'weight_from' => 'numeric',
                'weight_to' => 'numeric',
                'longitude' => 'numeric',
                'latitude' => 'numeric',
                'pax' => 'required|boolean',
                'product_package' => 'array',
                'product_package.*.id' => 'required|integer|min:0|distinct',
                'product_package.*.location_id' => 'required|integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL',
                'services' => 'array',
                'services.*' => 'required|distinct|integer|min:0|exists:tenant.mo_service,id,deleted_at,NULL',
                'categories' => 'array',
                'categories.*' => 'required|distinct|integer|min:0|exists:tenant.mo_category,id,deleted_at,NULL',
                'tags' => 'array',
                'tags.*' => 'required|distinct|integer|min:0|exists:tenant.mo_tag,id,deleted_at,NULL',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                'locations' => 'array|required',
                'locations.*' => 'required|distinct|integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL',
                'uom_id' => 'required|integer|min:0|exists:tenant.mo_uom,id,deleted_at,NULL',
                'salable' => 'boolean',
                'iva_id' => 'required|integer|min:0|exists:tenant.mo_iva,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $tipo = Type::find($request->get('type_id'));
            if (!$tipo && $request->get('type_id') != '') {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['type_id' => [0 => 'The type id is not exists.']]);
            }

            $compania = Company::find($request->get('company_id'));
            if (!$compania && $request->get('company_id') != '') {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['company_id' => ['0' => 'The company id is not exists.']]);
            }

            $product_check = Product::where('code', $request->get('code'))->where('id', '!=', $request->get('id'))->first();
            if ($product_check) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['code' => ['0' => 'The code already exists.']]);
            }

            if ($request->get('type_id') == 1 && $request->get('product_package')) {
                foreach ($request->get('product_package') as $producto_paquete) {
                    if (isset($producto_paquete['id'])) {
                        $product_paquete = Product::find($producto_paquete['id']);
                        if ($product_paquete && $product_paquete->type_id == 1) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['product_package' => ['0' => 'The product type of the package can not be equal to package']]);
                        }

                        if ($product_paquete->type_id == 2) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['product_package' => ['0' => 'The product type of the package can not be equal to hotel']]);
                        }
                        if (!$product_paquete) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['product_package' => ['0' => 'The product is not exist in the products table']]);
                        }
                    }
                }
            }

            if ($request->get('type_id') == 2) {
                $datos_paquete = DB::connection('tenant')->table('mo_product_package')
                    ->where('mo_product_package.deleted_at')
                    ->where('mo_product_package.product_id', $request->get('id'))
                    ->first();

                if ($datos_paquete) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['product_package' => ['0' => 'The product is part of a package']]);
                }
            }


            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('features')[$idioma->abbreviation]) ||
                    isset($request->get('recommendations')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation]) || isset($request->get('discover_url')[$idioma->abbreviation]) ||
                    isset($request->get('legal')[$idioma->abbreviation]) ||

                    isset($request->get('breadcrumb')[$idioma->abbreviation]) || isset($request->get('rel')[$idioma->abbreviation]) ||
                    isset($request->get('og_title')[$idioma->abbreviation]) || isset($request->get('og_description')[$idioma->abbreviation]) ||
                    isset($request->get('og_image')[$idioma->abbreviation]) || isset($request->get('twitter_title')[$idioma->abbreviation]) ||
                    isset($request->get('twitter_description')[$idioma->abbreviation]) || isset($request->get('twitter_image')[$idioma->abbreviation]) ||
                    isset($request->get('script_head')[$idioma->abbreviation]) || isset($request->get('canonical_url')[$idioma->abbreviation]) ||
                    isset($request->get('script_body')[$idioma->abbreviation]) || isset($request->get('script_footer')[$idioma->abbreviation]) ||
                    isset($request->get('index')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',

                        'index.' . $idioma->abbreviation => 'boolean',
                        'og_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                        'twitter_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }
            // FIN validación


            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $producto = Product::find($request->get('id'));
                if (!$producto) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {

                    
                    $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                    $rule_codes = [];

                    if($heimdall_middleware == '1') {

                        $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();
                
                        $website = DB::table('saas_website')
                                        ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                        'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                        'saas_website.default_language_id', 'saas_website.default_currency_id')
                                        ->whereNull('saas_website.deleted_at')
                                        ->where('saas_website.website_id', $website->id)
                                        ->first();
                
                        $plans = websitePlanRules($website);

                        $cantidad = 0;
                        $servicios = 0;
                        $archivos = 0;
                        $product_rule_code = '';

                        foreach($plans as $plan) {
                            if($plan->plan_id != null){
                                $rules = websiteRules($plan);

                                if($request->get('type_id') != 2 && $request->get('type_id') != 8){
                                    $rules_product = $rules->where('code', 'products');

                                    $product_rule_code = 'products';
                                }
        
                                if($request->get('type_id') == 2) {
                                    $rules_product = $rules->where('code', 'product_hotel');

                                    $product_rule_code = 'product_hotel';
                                }
        
                                if($request->get('type_id') == 8){
                                    $rules_product = $rules->where('code', 'product_benefitcard');

                                    $product_rule_code = 'product_benefitcard';
                                }
        
                                if ($rules_product) {
                                    foreach($rules_product as $rule) {
                                        if($rule->unlimited == 1) {
                                            $cantidad = -1;
                                        } else {
                                            if($cantidad >= 0 && $cantidad < $rule->value){
                                                $cantidad = $rule->value;
                                            }
                                        }
                                    }
                                }

                                if($request->get('services') != ''){
                                    $rules_service = $rules->where('code', 'product_service');
                                    if($rules_service) {
                                        foreach($rules_service as $rule_service) {
                                            if($rule_service->unlimited == 1) {
                                                $servicios = -1;
                                            } else {
                                                if($servicios >= 0 && $servicios < $rule_service->value){
                                                    $servicios = $rule_service->value;
                                                }
                                            }
                                        }
                                    }
                                }

                                if($request->get('files') != '') {
                                    $rules_files = $rules->where('code', 'product_file');
                                    if($rules_files) {
                                        foreach($rules_files as $rule_file) {
                                            if($rule_file->unlimited == 1) {
                                                $archivos = -1;
                                            } else {
                                                if($archivos >= 0 && $archivos < $rule_file->value){
                                                    $archivos = $rule_file->value;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {

                                if($request->get('type_id') != 2 && $request->get('type_id') != 8 && $plan->rule_code == 'products'){
                                    if($plan->unlimited == 1){
                                        $cantidad = -1;
                                    }else{
                                        if($cantidad >= 0 && $cantidad < $plan->value){
                                            $cantidad = $plan->value;
                                        }
                                    }
                                }
        
                                if($request->get('type_id') == 2 && $plan->rule_code == 'product_hotel') {
                                    if($plan->unlimited == 1){
                                        $cantidad = -1;
                                    }else{
                                        if($cantidad >= 0 && $cantidad < $plan->value){
                                            $cantidad = $plan->value;
                                        }
                                    }
                                }
        
                                if($request->get('type_id') == 8 && $plan->rule_code == 'product_benefitcard' ){
                                    if($plan->unlimited == 1){
                                        $cantidad = -1;
                                    }else{
                                        if($cantidad >= 0 && $cantidad < $plan->value){
                                            $cantidad = $plan->value;
                                        }
                                    }
                                }

                                if($request->get('services') != '' && $plan->rule_code == 'product_service'){
                                    if($plan->unlimited == 1){
                                        $servicios = -1;
                                    }else{
                                        if($servicios >= 0 && $servicios < $plan->value){
                                            $servicios = $plan->value;
                                        }
                                    }
                                }

                                if($request->get('files') != '' && $plan->rule_code == 'product_file'){
                                    if($plan->unlimited == 1){
                                        $archivos = -1;
                                    }else{
                                        if($archivos >= 0 && $archivos < $plan->value){
                                            $archivos = $plan->value;
                                        }
                                    }
                                }
                            }
                        }

                        if($cantidad == 0) {
                            $error = 1;
                            $rule_codes[] = $product_rule_code;
                        } else if ($cantidad > 0) {
                            if($request->get('type_id') == 2 || $request->get('type_id') == 8) {
                                $total_productos = Product::where('type_id', $request->get('type_id'))
                                ->where('id', '!=', $request->get('id'))
                                ->get();
                            } else {
                                $total_productos = Product::where('id', '!=', $request->get('id'))
                                ->where('type_id', '!=', 2)
                                ->where('type_id', '!=', 8)
                                ->get();
                            }
                            

                            if(count($total_productos) >= $cantidad) {
                                $error = 1;
                                $rule_codes[] = $product_rule_code;
                            }
                        }

                        if($request->get('services') != '' && count($request->get('services')) > 0) {
                            if($servicios == 0) {
                                $error = 1;
                                $rule_codes[] = 'product_service';
                            } else if ($servicios > 0 && count($request->get('services')) > $servicios){
                                $error = 1;
                                $rule_codes[] = 'product_service';
                            }
                        }

                        if($request->get('files') != '' && count($request->get('files')) > 0){
                            if($archivos == 0){
                                $error = 1;
                                $rule_codes[] = 'product_file';
                            } else if ($archivos > 0  && count($request->get('files')) > $archivos) {
                                $error = 1;
                                $rule_codes[] = 'product_file';
                            }
                        }
                    }

                    if($error == 1) {
                        $array['error'] = 419;
                        $array['error_description'] = 'You do not have a valid plan';
                        $array['error_rule_code'] = $rule_codes;

                        //Heimdall - datos para redirección
                        $array['data'] = array();
                        $array['data'][0]['website_id'] = $website->id;

                        $protocol = DB::table('saas_settings')
                        ->where('saas_settings.name', 'protocol')->first()->value;
                        $base_path_saas = DB::table('saas_settings')
                        ->where('saas_settings.name', 'base_path_saas')->first()->value;
                        $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                    } else {
                        $producto->productService()->delete();
                        $producto->productCategory()->delete();
                        $producto->productTag()->delete();
                        $producto->productLocation()->delete();
                        $producto->productPackagePack()->delete();
                        // Actualizar producto
                        $producto->type_id = $request->get('type_id');
                        $producto->code = $request->get('code');
                        $producto->company_id = $request->get('company_id');
                        $producto->uom_id = $request->get('uom_id');
                        $producto->order = $request->get('order') != '' ? $request->get('order') : 0;
                        $producto->billable = $request->get('billable') != '' ? $request->get('billable') : 1;
                        $producto->height_from = $request->get('height_from') != '' ? $request->get('height_from') : null;
                        $producto->height_to = $request->get('height_to') != '' ? $request->get('height_to') : null;
                        $producto->weight_from = $request->get('weight_from') != '' ? $request->get('weight_from') : null;
                        $producto->weight_to = $request->get('weight_to') != '' ? $request->get('weight_to') : null;
                        $producto->longitude = $request->get('longitude') != '' ? $request->get('longitude') : null;
                        $producto->latitude = $request->get('latitude') != '' ? $request->get('latitude') : null;
                        $producto->address = $request->get('address') != '' ? $request->get('address') : null;
                        $producto->pax = $request->get('pax');
                        $producto->cost_center = $request->get('cost_center') != '' ? $request->get('cost_center') : null;
                        $producto->accounting_account = $request->get('accounting_account') != '' ? $request->get('accounting_account') : null;
                        $producto->salable = ($request->get('salable') !== '' && !is_null($request->get('salable'))) ? $request->get('salable') : 1;
                        $producto->iva_id = $request->get('iva_id');

                        $producto->save();

                        // Si es paquete crear relaciones
                        if ($request->get('type_id') == 1 && $request->get('product_package') != '') {
                            $producto->productPackagePro()->delete();
                            foreach ($request->get('product_package') as $pro) {
                                ProductPackage::create([
                                    'package_id' => $producto->id,
                                    'product_id' => $pro['id'],
                                    'location_id' => $pro['location_id'],
                                ]);
                            }
                        }
                        //FIN actualizar producto

                        // Actualizar traducciones del producto
                        $array_borrar = [];
                        foreach ($array_traducciones as $idioma) {

                            // Preparación de datos
                            $friendly_url = null;

                            if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                                $friendly_url = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                            } else {
                                $friendly_url = Str::slug($request->get('name')[$idioma->abbreviation]);
                            }
                            //

                            $traduccion = $producto->productTranslation()->where('language_id', $idioma->id)->first();

                            $array_borrar[] = $idioma->id;

                            if ($traduccion) {

                                $traduccion->update([
                                    'name' => $request->get('name')[$idioma->abbreviation],
                                    'description' => $request->get('description')[$idioma->abbreviation],
                                    'features' => (isset($request->get('features')[$idioma->abbreviation]) && $request->get('features')[$idioma->abbreviation] != '') ? $request->get('features')[$idioma->abbreviation] : null,
                                    'recommendations' => (isset($request->get('recommendations')[$idioma->abbreviation]) && $request->get('recommendations')[$idioma->abbreviation] != '') ? $request->get('recommendations')[$idioma->abbreviation] : null,
                                    'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                    'friendly_url' => $friendly_url,
                                    'discover_url' => (isset($request->get('discover_url')[$idioma->abbreviation]) && $request->get('discover_url')[$idioma->abbreviation] != '') ? $request->get('discover_url')[$idioma->abbreviation] : null,
                                    'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                    'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                    'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                                    'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,

                                    'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                                    'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                                    'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                                    'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                                    'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                                    'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                                    'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                                    'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                                    'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                                    'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                                    'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                                    'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                                    'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                                ]);
                            } else {

                                ProductTranslation::create([
                                    'product_id' => $request->get('id'),
                                    'language_id' => $idioma->id,
                                    'name' => $request->get('name')[$idioma->abbreviation],
                                    'description' => $request->get('description')[$idioma->abbreviation],
                                    'features' => (isset($request->get('features')[$idioma->abbreviation]) && $request->get('features')[$idioma->abbreviation] != '') ? $request->get('features')[$idioma->abbreviation] : null,
                                    'recommendations' => (isset($request->get('recommendations')[$idioma->abbreviation]) && $request->get('recommendations')[$idioma->abbreviation] != '') ? $request->get('recommendations')[$idioma->abbreviation] : null,
                                    'short_description' => $request->get('short_description')[$idioma->abbreviation],
                                    'friendly_url' => $friendly_url,
                                    'discover_url' => (isset($request->get('discover_url')[$idioma->abbreviation]) && $request->get('discover_url')[$idioma->abbreviation] != '') ? $request->get('discover_url')[$idioma->abbreviation] : null,
                                    'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                                    'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                                    'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                                    'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,

                                    'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                                    'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                                    'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                                    'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                                    'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                                    'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                                    'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                                    'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                                    'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                                    'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                                    'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                                    'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                                    'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                                ]);
                            }
                        }
                        $producto->productTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                        // FIN actualizar traducciones de producto

                        //Crear relaciones con servicios
                        if ($request->get('services') != '') {
                            foreach ($request->get('services') as $service) {
                                ProductService::create([
                                    'product_id' => $producto->id,
                                    'service_id' => $service,
                                ]);
                            }
                        }
                        // FIN crear relación con servicios


                        //Crear de relaciones con categories
                        if ($request->get('categories') != '') {
                            foreach ($request->get('categories') as $category) {
                                ProductCategory::create([
                                    'product_id' => $producto->id,
                                    'category_id' => $category,
                                ]);
                            }
                        }
                        // FIN crear relación con categories

                        //Crear de relaciones con tags
                        if ($request->get('tags') != '') {
                            foreach ($request->get('tags') as $tag) {
                                ProductTag::create([
                                    'product_id' => $producto->id,
                                    'tag_id' => $tag,
                                ]);
                            }
                        }
                        // FIN crear relación con tags

                        $producto->productFile()->delete();
                        //Crear relación con archivo
                        if ($request->get('files') != '') {
                            foreach ($request->get('files') as $file) {
                                ProductFile::create([
                                    'product_id' => $producto->id,
                                    'file_id' => $file,
                                ]);
                            }
                        }
                        //FIN crear relación con archivo

                        //Crear relación con location
                        foreach ($request->get('locations') as $location) {
                            ProductLocation::create([
                                'product_id' => $producto->id,
                                'location_id' => $location,
                            ]);
                        }
                        //fin crear relación con location
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Productos');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función que lista todos los productos
     *
     * Para la consulta de productos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            $idiomas = Language::where('front', 1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'type_id' => 'numeric',
                'page' => 'numeric',
                'limit' => 'numeric',
                'category_id' => 'numeric',
                'location_id' => 'integer'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Preparamos las consultas
                $productos = DB::connection('tenant')->table('mo_product')
                    ->select(
                        'mo_product_category.id',
                        'mo_product.id',
                        'mo_product.type_id',
                        'mo_product.code',
                        'mo_product.company_id',
                        'mo_product.uom_id',
                        'mo_product.order',
                        'mo_product.billable',
                        'mo_product.height_from',
                        'mo_product.height_to',
                        'mo_product.weight_from',
                        'mo_product.weight_to',
                        'mo_product.longitude',
                        'mo_product.latitude',
                        'mo_product.address',
                        'mo_product.pax',
                        'mo_product.cost_center',
                        'mo_product.accounting_account',
                        'mo_product.salable',
                        'mo_product.iva_id'
                    )
                    ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                    ->join('mo_product_location', 'mo_product.id', '=', 'mo_product_location.product_id')
                    ->join('mo_location', 'mo_product_location.location_id', '=', 'mo_location.id')
                    ->where('mo_product_translation.deleted_at', '=', null)
                    ->where('mo_product.deleted_at', '=', null)
                    ->where('mo_product_location.deleted_at', '=', null)
                    ->where('mo_location.deleted_at', '=', null)
                    ->leftJoin('mo_product_category', function ($join) {
                        $join->on('mo_product.id', '=', 'mo_product_category.product_id')->where('mo_product_category.deleted_at', null);
                    })
                    ->leftjoin('mo_category', function ($join) {
                        $join->on('mo_product_category.category_id', '=', 'mo_category.id')->where('mo_category.deleted_at', null);
                    });

                $sub = DB::connection('tenant')->table('mo_product')
                    ->select('mo_product.id')
                    ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                    ->join('mo_product_location', 'mo_product.id', '=', 'mo_product_location.product_id')
                    ->join('mo_location', 'mo_product_location.location_id', '=', 'mo_location.id')
                    ->where('mo_product_translation.deleted_at', '=', null)
                    ->where('mo_product.deleted_at', '=', null)
                    ->where('mo_product_location.deleted_at', '=', null)
                    ->where('mo_location.deleted_at', '=', null)
                    ->leftJoin('mo_product_category', function ($join) {
                        $join->on('mo_product.id', '=', 'mo_product_category.product_id')->where('mo_product_category.deleted_at', null);
                    })
                    ->leftJoin('mo_category', function ($join) {
                        $join->on('mo_product_category.category_id', '=', 'mo_category.id')->where('mo_category.deleted_at', null);
                    });

                // Recogida de datos enviados por el usuario y establecimiento de filtros
                if ($request->get('lang') != '') {
                    $productos->where('language_id', '=', $idioma->id);
                    $sub->where('language_id', '=', $idioma->id);
                }

                if ($request->get('type_id') != '') {
                    $productos->where('type_id', '=', $request->get('type_id'));
                    $sub->where('type_id', '=', $request->get('type_id'));
                }

                if ($request->get('code') != '') {
                    $productos->where('code', 'Like', '%' . $request->get('code') . '%');
                    $sub->where('code', 'Like', '%' . $request->get('code') . '%');
                }

                if ($request->get('name') != '') {
                    $productos->where('mo_product_translation.name', 'Like', '%' . $request->get('name') . '%');
                    $sub->where('mo_product_translation.name', 'Like', '%' . $request->get('name') . '%');
                }

                if ($request->get('category_id') != '') {
                    $productos->where('mo_product_category.category_id', '=', $request->get('category_id'))->where('mo_category.id', $request->get('category_id'));
                    $sub->where('mo_product_category.category_id', '=', $request->get('category_id'))->where('mo_category.id', $request->get('category_id'));
                }


                if ($request->get('location_id') != '') {

                    $productos->Where(function ($query) use ($request) {
                        $query->where('mo_location.id', '=', $request->get('location_id'));

                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_location.id', '=', $request->get('location_id'));
                    });
                }

                // Order
                $orden = 'mo_product_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_product_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_product_translation.description';
                            break;
                        case 'short_description':
                            $orden = 'mo_product_translation.short_description';
                            break;
                        case 'type_id':
                            $orden = 'mo_product.type_id';
                            break;
                        case 'id':
                            $orden = 'mo_product.id';
                            break;
                        case 'order':
                            $orden = 'mo_product.order';
                            break;
                        default:
                            $orden = 'mo_product_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $productos = $productos->groupBy('mo_product.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $productos = $productos->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $productos = $productos->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_product_translation.product_id');
                $productos_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $productos_count->count();


                // Preparada locaciones

                $sql_location = DB::connection('tenant')->table('mo_location')
                    ->select('mo_location.id', 'mo_location.name')
                    ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                    ->whereNull('mo_location.deleted_at')
                    ->whereNull('mo_product_location.deleted_at');


                $array['data'] = array();
                foreach ($productos as $producto) {
                    $sql_location_clone = clone $sql_location;

                    $datos_location = $sql_location_clone->where('mo_product_location.product_id', $producto->id)->get();

                    $producto->location = $datos_location;

                    foreach ($idiomas as $idi) {
                        $traduccion = ProductTranslation::where('product_id', '=', $producto->id)
                            ->select('id', 'language_id', 'name', 'description', 'features', 'recommendations', 'short_description', 'friendly_url', 'discover_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $producto->lang[][$idi->abbreviation] = $trad;
                        }
                    }
                    $array['data'][0]['product'][] = $producto;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Productos');

        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función que muestra un producto buscado por su id
     *
     * Para la consulta de un producto se realizará una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function showId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            $idiomas = Language::where('front', 1)->get();

            $productos = DB::connection('tenant')->table('mo_product')
                ->select(
                    'mo_product.id',
                    'mo_product.type_id',
                    'mo_product.code',
                    'mo_product.company_id',
                    'mo_product.uom_id',
                    'mo_product.order',
                    'mo_product.billable',
                    'mo_product.height_from',
                    'mo_product.height_to',
                    'mo_product.weight_from',
                    'mo_product.weight_to',
                    'mo_product.longitude',
                    'mo_product.latitude',
                    'mo_product.address',
                    'mo_product.pax',
                    'mo_product.cost_center',
                    'mo_product.accounting_account',
                    'mo_product.salable',
                    'mo_product.iva_id'
                )
                ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                ->join('mo_product_location', 'mo_product.id', '=', 'mo_product_location.product_id')
                ->join('mo_location', 'mo_product_location.location_id', '=', 'mo_location.id')
                ->where('mo_product_translation.deleted_at', '=', null)
                ->where('mo_product_location.deleted_at', '=', null)
                ->where('mo_location.deleted_at', '=', null)
                ->where('mo_product.deleted_at', '=', null);

            $productos->where('mo_product.id', '=', $id);
            $productos = $productos->groupBy('mo_product.id');
            $productos = $productos->get();

            //ruta de storage de settings para concatenar a ruta de archivo de producto
            $storage = Settings::where('name', 'storage_files')->first()->value;

            $array['data'] = array();

            foreach ($productos as $producto) {

                //locations asociadas al producto
                $locations = DB::connection('tenant')->table('mo_product_location')->where('product_id', $producto->id)
                    ->select('mo_location.id', 'mo_location.name')
                    ->join('mo_location', 'mo_product_location.location_id', 'mo_location.id')
                    ->whereNull('mo_location.deleted_at')
                    ->whereNull('mo_product_location.deleted_at')
                    ->get();

                foreach ($locations as $location) {
                    $producto->locations[] = $location;
                }


                $files = DB::connection('tenant')->table('mo_product_file')
                    ->select('mo_file.id', 'mo_file.file_name', 'mo_file.file_size', 'mo_file.file_dimensions', 'mo_file.mimetype', 'mo_file.type_id', 'mo_file.order', 'mo_file.url_file',
                        'mo_file.device_id','mo_file.key_file')
                    ->where('mo_product_file.product_id', '=', $producto->id)
                    ->where('mo_product_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_product_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->groupBy('mo_file.id')->get();


                $producto->files = array();
                $array_file = array();
                foreach ($files as $file) {

                    //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                    $tipo = FileType::find($file->type_id);

                    $array_file = ['id' => $file->id, 'file_name' => $file->file_name, 'file_size' => convertExtension($file->file_size),
                        'file_dimensions' => $file->file_dimensions, 'mimetype' => $file->mimetype, 'order' => $file->order, 'url_file' => $storage . $file->url_file,
                        'device_id' => $file->device_id,'key_file' => $file->key_file,
                        'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];
                    foreach ($idiomas as $idi) {

                        //si el tipo no existe o está borrado no busca sus traducciones
                        if ($tipo) {
                            $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_type_translation.type_id')
                                ->get();

                            foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                            }
                        }

                        $traducciones_files = FileTranslation::where('file_id', $file->id)
                            ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                            ->where('language_id', '=', $idi->id)
                            ->groupBy('mo_file_translation.file_id')
                            ->get();

                        foreach ($traducciones_files as $traduccion_file) {
                            $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                        }
                    }
                    $producto->files[] = $array_file;
                }
                //Traducciones del producto
                foreach ($idiomas as $idi) {

                    $traduccion = ProductTranslation::where('product_id', '=', $producto->id)
                        ->select('id', 'language_id', 'name', 'description', 'features', 'recommendations', 'short_description', 'friendly_url', 'discover_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                            'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                        ->where('language_id', '=', $idi->id)
                        ->get();
                    foreach ($traduccion as $trad) {
                        if ($trad->og_image && $trad->og_image != ''){
                            $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->og_image)->first()->url_file;
                            $trad->og_image_route = $storage . $sql_file_seo;
                        }else{
                            $trad->og_image_route = null;
                        }
                        if ($trad->twitter_image && $trad->twitter_image != ''){
                            $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->twitter_image)->first()->url_file;
                            $trad->twitter_image_route = $storage . $sql_file_seo;
                        }else{
                            $trad->twitter_image_route = null;
                        }
                        $producto->lang[][$idi->abbreviation] = $trad;
                    }
                }


                //Paquetes
                $paquete = array();
                if ($producto->type_id == 1) {
                    $package = ProductPackage::where('package_id', '=', $producto->id)->get();
                    foreach ($package as $pack) {
                        $paquete[] = ['product_id' => $pack->product_id, 'location_id' => $pack->location_id];
                    }
                }
                $producto->package = $paquete;

                //servicios asociados al producto
                $services = DB::connection('tenant')->table('mo_product_service')->where('product_id', $producto->id)
                    ->select('service_id as id')
                    ->join('mo_service', 'mo_product_service.service_id', 'mo_service.id')
                    ->whereNull('mo_service.deleted_at')
                    ->whereNull('mo_product_service.deleted_at')
                    ->get();
                $producto->services = array();
                foreach ($services as $service) {
                    $producto->services[] = $service;
                }


                //categorías asociadas al producto
                $categories = DB::connection('tenant')->table('mo_product_category')->where('product_id', $producto->id)
                    ->select('category_id as id')
                    ->join('mo_category', 'mo_product_category.category_id', 'mo_category.id')
                    ->whereNull('mo_category.deleted_at')
                    ->whereNull('mo_product_category.deleted_at')
                    ->get();
                $producto->categories = array();
                foreach ($categories as $cat) {
                    $producto->categories[] = $cat;
                }

                //etiquetas asociadas al producto
                $tags = DB::connection('tenant')->table('mo_product_tag')->where('product_id', $producto->id)
                    ->select('tag_id as id')
                    ->join('mo_tag', 'mo_product_tag.tag_id', 'mo_tag.id')
                    ->whereNull('mo_tag.deleted_at')
                    ->whereNull('mo_product_tag.deleted_at')
                    ->get();
                $producto->tags = array();
                foreach ($tags as $tag) {
                    $producto->tags[] = $tag;
                }

                $array['data'][0]['product'][] = $producto;
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Productos');

        }

        return response()->json($array, $array['error']);

    }

}