<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;
use Validator;
//use \Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use App\Models\tenant\Type;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use App\Exceptions\Handler;
use App\Models\tenant\TypeTranslation;
use Illuminate\Support\Facades\DB;


class TypeController extends Controller
{

    //use DatabaseTransactions;

    /**
     * Función que muestra los tipos de producto
     *
     * Para la consulta de los tipos de producto se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTypes(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $types = DB::connection('tenant')->table('mo_product_type')
                    ->select('mo_product_type.id')
                    ->where('mo_product_type.deleted_at', '=', null)
                    ->join('mo_product_type_translation', 'mo_product_type.id', 'mo_product_type_translation.type_id')
                    ->where('mo_product_type_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_product_type')
                    ->select('mo_product_type.id')
                    ->where('mo_product_type.deleted_at', '=', null)
                    ->join('mo_product_type_translation', 'mo_product_type.id', 'mo_product_type_translation.type_id')
                    ->where('mo_product_type_translation.deleted_at', '=', null);
                //Fin precompiladas

                //Filtros de busqueda
                if ($request->get('lang') != '') {
                    $types->where('mo_product_type_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_product_type_translation.language_id', '=', $idioma->id);
                }
                //Fin de filtros


                // Order
                $orden = 'mo_product_type.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_product_type.id';
                            break;
                        case 'name':
                            $orden = 'mo_product_type_translation.name';
                            break;
                        default:
                            $orden = 'mo_product_type.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $types = $types->groupBy('mo_product_type.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $types = $types->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $types = $types->get();

                }



                // Totales resultado
                $sub->groupBy('mo_product_type_translation.type_id');
                $types_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $types_count->count();


                //generacion de salida

                $array['data'] = array();


                //Se recorren los tipos
                foreach ($types as $type) {

                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de los tipos
                        $traduccion = TypeTranslation::where('type_id', '=', $type->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name']);
                        foreach ($traduccion as $trad) {
                            $type->lang[][$idi->abbreviation] = $trad;
                        }

                    }

                    $array['data'][0]['type'][] = $type;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de productos');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Devuelve el tipo de producto solicitado
     *
     * Para la consulta de los tipos de producto se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTypeId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            //Consulta BBDD
            $type = DB::connection('tenant')->table('mo_product_type')
                ->select('mo_product_type.id')
                ->join('mo_product_type_translation', 'mo_product_type.id', '=', 'mo_product_type_translation.type_id')
                ->where('mo_product_type_translation.deleted_at', '=', null)
                ->where('mo_product_type.deleted_at', '=', null)
                ->where('mo_product_type.id', '=', $id);


            $type = $type->groupBy('mo_product_type.id');
            $type = $type->get();
            //Fin consulta BBDD

            $array['data'] = array();
            foreach ($type as $type_aux) {
                //Traducciones del tipo de producto.
                foreach ($idiomas as $idi) {
                    $traducciones = TypeTranslation::where('language_id', '=', $idi->id)->where('type_id', '=', $type_aux->id)
                        ->select('id', 'language_id', 'type_id', 'name')
                        ->get();
                    foreach ($traducciones as $trad) {
                        $type_aux->lang[][$idi->abbreviation] = $trad;
                    }
                }
                $array['data'][0]['type'][] = $type_aux;
            }

            //Fin traducciones

            DB::connection('tenant')->commit();


        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tipos de productos');
        }
        return response()->json($array, $array['error']);
    }

}