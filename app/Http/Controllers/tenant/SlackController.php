<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Notifications\SlackNotification;
use App\Notifications\SlackNotificationError;
use App\Notifications\SlackNotificationInfo;
use App\Models\tenant\Settings;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class SlackController extends Controller
{
    use Notifiable;


    /**
     * Método para el envío de notificaciones por Slack
     *
     * Son requeridos el from y content que indican desde donde se envía la notificación y el contenido principal de este mensaje.
     * No requerido es el parámetro space, que define el nombre del espacio de Slack contemplado para las notificaciones. Si no se envía, se utilizará por defecto el espacio “default”, del que se obtendrán el webhook o url 'slack_webhook_default'
     * y su canal asociado 'slack_channel_default'. También se podrán especificar canales distintos al asociado al webhook del espacio.
     * Si se quiere dar una información más detallada en la notificación, podrá enviarse un adjunto teniendo como parámetros configurables 'title', 'content' y una 'url'.
     * El tipo de notificación también será un parámetro opcional ('info', 'error' o 'default'), tomando el valor 'default' si no se indica ningún tipo concreto en la petición.
     * Para el envío de notificaciones por Slack se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function send(Request $request)
    {
        $array['error'] = 200;
        try {

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'type' => 'in:info,error,default',
                'from' => 'required',
                'content' => 'required',
                'attachment' => 'array',
                'channels' => 'array',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('space') != '') {
                $space = Settings::where('name', '=', 'slack_webhook_' . $request->get('space'))->first();
            } else {
                $space = Settings::where('name', '=', 'slack_webhook_default')->first();
            }

            if (($space == '')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['space' => ['Space not available']]);
            } else {
                $space = $space->value;
            }

            //Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $datos = [
                    'from' => $request->get('from'),
                    'content' => $request->get('content'),
                    'attachment' => [
                        'title' => isset($request->get('attachment')['title']) ? $request->get('attachment')['title'] : null,
                        'url' => isset($request->get('attachment')['url']) ? $request->get('attachment')['url'] : null,
                        'content' => isset($request->get('attachment')['content']) ? $request->get('attachment')['content'] : null,
                    ]
                ];

                if ($request->get('channels')) {

                    $channels = $request->get('channels');

                } else {

                    if ($request->get('space') != '') {
                        $channels[] = Settings::where('name', '=', 'slack_channel_' . $request->get('space'))->first()->value;

                    } else {
                        $channels[] = Settings::where('name', '=', 'slack_channel_default')->first()->value;
                    }
                }

                foreach ($channels as $channel) {

                    $datos['to'] = $channel;

                    if ($request->get('type') == 'error') {

                        Notification::route('slack', $space)->notify(new SlackNotificationError($datos));

                    } elseif ($request->get('type') == 'info') {

                        Notification::route('slack', $space)->notify(new SlackNotificationInfo($datos));

                    } else {

                        Notification::route('slack', $space)->notify(new SlackNotification($datos));

                    }
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_description'] = $e->getMessage();
        }

        return response()->json($array, $array['error']);
    }

}