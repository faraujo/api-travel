<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Language;
use App\Models\tenant\PaymentMethod;
use App\Models\tenant\PaymentMethodTranslation;
use App\Models\tenant\Settings;
use Carbon\Carbon;
use GuzzleHttp\Client;
//use Hamcrest\Core\Set;
use Illuminate\Support\Facades\DB;
use App\Exceptions\Handler;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Http\Request;
use App\Models\tenant\UserToken;
use App\Models\tenant\CurrencyExchange;
use App\Models\tenant\Currency;
use App\Models\tenant\Reservation;
use App\Models\tenant\ReservationPayment;
use App\Models\tenant\ReservationDetailTransportation;
use App\Models\tenant\ReservationDetailFood;
use App\Models\tenant\ReservationDetailPhoto;
use App\Models\tenant\ReservationDetailDayMore;
use App\Models\tenant\ReservationDetailEvent;
use App\Models\tenant\ReservationDetailPackage;
use App\Models\tenant\ReservationDetailHotel;
use App\Models\tenant\ReservationDetailActivity;
use App\Models\tenant\ReservationDetailTour;
use App\Models\tenant\ReservationDetailPark;
use App\Models\tenant\ReservationDetailRestaurant;
use App\Models\tenant\ReservationDetailBenefitCard;
use App\Models\tenant\ReservationAntifraudLevel;
use App\Models\tenant\Subchannel;
use App\Models\tenant\ReservationLog;
use App\Models\tenant\ReservationDetail;
use App\Models\tenant\ReservationInsurance;
use App\Models\tenant\ReservationDetailHotelRoom;
use SimpleXMLElement;
use SoapVar;
use App\Models\SaasSettings;
use Srmklive\PayPal\Services\ExpressCheckout;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ReservationsPaymentMethodController extends Controller
{

    /**
     * Función que lista todos los métodos de pago
     *
     * Para la consulta de métodos de pago se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPaymentMethod(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $error = 0;

            // Validación
            $mensaje_validador = collect();


            $validator = Validator::make($request->all(), [
                'page' => 'integer',
                'limit' => 'integer',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $sql_payment = DB::connection('tenant')->table('mo_payment_method')
                    ->select('mo_payment_method.id')
                    ->where('mo_payment_method.deleted_at', '=', null)
                    ->join('mo_payment_method_translation', 'mo_payment_method_translation.payment_method_id', 'mo_payment_method.id')
                    ->where('mo_payment_method_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_payment_method')
                    ->select('mo_payment_method.id')
                    ->where('mo_payment_method.deleted_at', '=', null)
                    ->join('mo_payment_method_translation', 'mo_payment_method_translation.payment_method_id', 'mo_payment_method.id')
                    ->where('mo_payment_method_translation.deleted_at', '=', null);

                if ($request->get('lang') != '') {
                    $sql_payment->where('mo_payment_method_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_payment_method_translation.language_id', '=', $idioma->id);
                }

                // Order
                $orden = 'mo_payment_method_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_payment_method_translation.name';
                            break;
                        case 'id':
                            $orden = 'mo_payment_method.id';
                            break;
                        default:
                            $orden = 'mo_payment_method_translation.name';
                            break;
                    }
                }

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $payment = $sql_payment->groupBy('mo_payment_method.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $payment = $payment->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $payment = $payment->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_payment_method_translation.payment_method_id');
                $payment_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $payment_count->count();


                $array['data'] = array();
                foreach ($payment as $pay) {

                    foreach ($idiomas as $idi) {
                        $traduccion = PaymentMethodTranslation::where('payment_method_id', '=', $pay->id)
                            ->select('id', 'language_id', 'name')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $pay->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['payment_method'][] = $pay;
                }
                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Pagos');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todos los bancos disponibles para el pago
     *
     * Para la consulta de bancos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentMethodsMop(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'payment_method_types' => 'array',
                'payment_method_types.*' => 'exists:tenant.mo_payment_method,type,deleted_at,NULL'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;

            } else {

                //Consultas preparadas
                $payment_methods = DB::connection('tenant')->table('mo_payment_method')
                    ->select(
                        'mo_payment_method.id',
                        'mo_payment_method.order',
                        'mo_payment_method.api_code',
                        'mo_payment_method.type',
                        'mo_payment_method.url_image'
                    )
                    ->whereNull('mo_payment_method.deleted_at')
                    ->join('mo_payment_method_translation', 'mo_payment_method_translation.payment_method_id', 'mo_payment_method.id')
                    ->whereNull('mo_payment_method_translation.deleted_at');

                if($request->get('payment_method_types')) {
                    $payment_methods->whereIn('mo_payment_method.type', $request->get('payment_method_types'));
                }

                if($request->get('front') != '') {
                    $payment_methods->where('mo_payment_method.front', '1');
                }

                if($request->get('back') != '') {
                    $payment_methods->where('mo_payment_method.back', '1');
                }
                // FIN consultas preparadas

                $idiomas = Language::where('front', 1)->get();
                
                $array['data'] = array();

                $array['data'][0]['payment_method'] = [];

                $datos_payment_methods = $payment_methods->groupBy('mo_payment_method.id')->get();


                foreach ($datos_payment_methods as $payment_method) {

                    $array_tarjeta = array();

                    $array_tarjeta['id'] = $payment_method->id;
                    $array_tarjeta['api_code'] = $payment_method->api_code;
                    $array_tarjeta['order'] = $payment_method->order;
                    $array_tarjeta['type'] = $payment_method->type;
                    $array_tarjeta['url_file'] = asset('/images/' . $payment_method->url_image);

                    $array_tarjeta['lang'] = array();

                    foreach ($idiomas as $idioma) {
                        $traduccion = PaymentMethodTranslation::where('payment_method_id', $payment_method->id)->where('language_id', $idioma->id)->first();

                        if($traduccion) {
                            $array_tarjeta['lang'][][$idioma->abbreviation] = [
                                'id' => $traduccion->id,
                                'name' => $traduccion->name,
                            ];
                        }
                    }
                    if($payment_method->type == 'PayPal') {

                        $user = Settings::where('name','paypal_username')->first()->value;
                        $pass = Settings::where('name','paypal_password')->first()->value;
                        $secret = Settings::where('name', 'paypal_secret')->first()->value;

                        if(($user != null && $user != '') && ($pass != null && $pass != '') && ($secret != null && $secret != '')) {
                            $array['data'][0]['payment_method'][] = $array_tarjeta;
                        }
                    } else {
                        $array['data'][0]['payment_method'][] = $array_tarjeta;
                    }
                    

                }
                $array['data'][0]['bank'][0]['idBank'] = '1';
                $array['data'][0]['bank'][0]['bankCode'] = 'N/A';
                $array['data'][0]['bank'][0]['bankName'] = 'OTROS';

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Pagos');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para el pago de una reserva.
     *
     * Para el pago de reservas se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payment(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $credit_card_number = null;
            $credit_card_cvv = null;
            $credit_expiry_date = null;
            $credit_card_type = null;
            $payment_method_id = null;
            $respuesta_procesar = 0;

            //Desencriptación de variables enviadas desde los fronts
            $request->get('id') != '' ? $request->request->add(['id' => desencriptar($request->get('id'))]) : '';
            $request->get('emulated_subchannel_id') != '' ? $request->request->add(['emulated_subchannel_id' => desencriptar($request->get('emulated_subchannel_id'))]) : '';
            $request->get('amount') != '' ? $request->request->add(['amount' => desencriptar($request->get('amount'))]) : '';
            $request->get('currency_id') != '' ? $request->request->add(['currency_id' => desencriptar($request->get('currency_id'))]) : '';
            $request->get('card_number') != '' ? $request->request->add(['card_number' => desencriptar($request->get('card_number'))]) : '';
            $request->get('payment_method_id') != '' ? $request->request->add(['payment_method_id' => desencriptar($request->get('payment_method_id'))]) : '';
            $request->get('bank_id') != '' ? $request->request->add(['bank_id' => desencriptar($request->get('bank_id'))]) : '';
            $request->get('bank_name') != '' ? $request->request->add(['bank_name' => desencriptar($request->get('bank_name'))]) : '';
            $request->get('expiry_date') != '' ? $request->request->add(['expiry_date' => desencriptar($request->get('expiry_date'))]) : '';
            $request->get('cvv') != '' ? $request->request->add(['cvv' => desencriptar($request->get('cvv'))]) : '';
            $request->get('name') != '' ? $request->request->add(['name' => desencriptar($request->get('name'))]) : '';
            $request->get('surname') != '' ? $request->request->add(['surname' => desencriptar($request->get('surname'))]) : '';
            $request->get('card_payment_method') != '' ? $request->request->add(['card_payment_method' => desencriptar($request->get('card_payment_method'))]) : '';
            $request->get('months_no_interest') != '' ? $request->request->add(['months_no_interest' => desencriptar($request->get('months_no_interest'))]) : '';
            $request->get('client_session') != '' ? $request->request->add(['client_session' => desencriptar($request->get('client_session'))]) : '';
            $request->get('return_url') != '' ? $request->request->add(['return_url' => desencriptar($request->get('return_url'))]) : '';
            //Fin desencriptación de variables enviadas desde los fronts

            $validator = Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'emulated_subchannel_id' => 'integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'amount' => 'required|numeric|min:0',
                'currency_id' => 'required|integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',
                'card_number' => 'regex:/^[0-9]+$/',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if($error == 0) {
                $total = reservationTotalToPay($request->get('id'));
                $total_pagado = reservationTotalPaid($request->get('id'));

                if (round($total_pagado,5) < round($total,5)) {
                    $validator = Validator::make($request->all(), [
                        'payment_method_id' => 'required|integer|min:0|exists:tenant.mo_payment_method,id,deleted_at,NULL',
                    ]);

                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }

                    $metodo_pago = PaymentMethod::where('id', $request->get('payment_method_id'))->first();

                    if ($metodo_pago && $metodo_pago->type == 'Tarjeta') {

                        $validator = Validator::make($request->all(), [
                            'bank_id' => 'required|integer|min:0',
                            'card_number' => 'required',
                            'expiry_date' => 'required|date_format:"m/y"',
                            'cvv' => 'required',
                            'name' => 'required',
                            'surname' => 'required',
                        ]);


                        if ($validator->fails()) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge($validator->errors());
                        }
                    }

                    $payment_method_id = $request->get('payment_method_id');
                } else {
                    $metodo_pago = PaymentMethod::where('type', 'Cortesia')->first();

                    $payment_method_id = $metodo_pago->id;
                }
            }

            if($error == 0){
                $credit_card_number = $request->get('card_number');
                $credit_card_cvv = $request->get('cvv');
                $credit_expiry_date = $request->get('expiry_date');
                $credit_card_type = $request->get('payment_method_id');
            }


            //Comprueba si existe algun producto de tipo hotel en la reserva
            $sql_reservation_hotel = DB::connection('tenant')->table('mo_product')
                ->whereNull('mo_product.deleted_at')
                ->join('mo_reservation_detail', 'mo_reservation_detail.product_id', 'mo_product.id')
                ->whereNull('mo_reservation_detail.deleted_at')
                ->where('mo_reservation_detail.reservation_id', $request->get('id'))
                ->join('mo_reservation', 'mo_reservation.id', 'mo_reservation_detail.reservation_id')
                ->whereNull('mo_reservation.deleted_at')
                ->where('mo_product.type_id', 2);

            $sql_reservation_hotel_clone = clone $sql_reservation_hotel;

            $datos_reservation_hotel_validador = $sql_reservation_hotel_clone
                ->whereNull('mo_reservation.credit_card_number')
                ->whereNull('mo_reservation.credit_card_name')
                ->whereNull('mo_reservation.credit_card_surname')
                ->first();


            $datos_reservation_hotel = $sql_reservation_hotel->first();


            if ($datos_reservation_hotel_validador) {
                // $validator = Validator::make($request->all(), [
                //     'card_number' => 'required',
                //     'expiry_date' => 'required|date_format:"m/y"',
                //     'cvv' => 'required',
                //     'name' => 'required',
                //     'surname' => 'required',
                //     'card_payment_method' => 'required|integer|min:0|exists:tenant.mo_payment_method,id,deleted_at,NULL,type,"Tarjeta"',
                // ]);

                // if ($validator->fails()) {
                //     $error = 1;
                //     $mensaje_validador = $mensaje_validador->merge($validator->errors());
                // } else {
                //     $credit_card_number = $request->get('card_number');
                //     $credit_card_cvv = $request->get('cvv');
                //     $credit_expiry_date = $request->get('expiry_date');
                //     $credit_card_type = $request->get('card_payment_method');
                // }
            }


            $user_id = '';
            $client_session = '';
            $subchannel_id = '';
            $emulated_subchannel_id = '';

            // Si la petición es pública a través de un canal de venta
            if (strpos($request->path(), 'payment/search') !== false) {

                // Si el usuario está logueado
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

                } elseif ($request->get('client_session') != '') { // Si no está logueado
                    $client_session = $request->get('client_session');

                } else { // Si no está logueado y no aporta identificación temporal
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token or session is required']]);
                }

                $subchannel_id = $request->get('subchannel_id');
                $emulated_subchannel_id = $request->get('subchannel_id');

            } else { // Si la petición procede del POS


                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;

                        $sql_permisos = DB::connection('tenant')->table('mo_permission')
                            ->select('mo_permission.id')
                            ->where('mo_permission.subchannel', '=', 1)
                            ->whereNull('mo_permission.deleted_at')
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->where('mo_role_permission.subchannel_id', $request->get('subchannel_id'))
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user_id)
                            ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                            ->whereNull('mo_module.deleted_at')
                            ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                            ->whereNull('mo_module_subchannel.deleted_at')
                            ->where('mo_module_subchannel.subchannel_id', '=', $request->get('subchannel_id'))
                            ->where('mo_permission.id', 133);

                        $datos_permisos = $sql_permisos->first();

                        if ($request->get('emulated_subchannel_id') != '' && $request->get('subchannel_id') != $request->get('emulated_subchannel_id')) {

                            if ($datos_permisos) {
                                $subchannel_id = $request->get('subchannel_id');
                                $emulated_subchannel_id = $request->get('emulated_subchannel_id');
                            } else {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The subchannel is not valid']]);
                            }

                        } else {
                            $subchannel_id = $request->get('subchannel_id');
                            $emulated_subchannel_id = $request->get('subchannel_id');
                        }

                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $validator = Validator::make($request->all(), [
                    'id' => 'exists:tenant.mo_reservation,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                $sql_reservation = DB::connection('tenant')->table('mo_reservation')
                    ->select(
                        'mo_reservation.id',
                        'mo_reservation.subchannel_id',
                        'mo_reservation.client_id',
                        'mo_reservation.client_name',
                        'mo_reservation.client_surname',
                        'mo_reservation.client_email',
                        'mo_reservation.client_country_id',
                        'mo_reservation.client_telephone1',
                        'mo_reservation.client_session',
                        'mo_reservation.code',
                        'mo_reservation.language_id'
                    )
                    ->whereNull('mo_reservation.deleted_at')
                    ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                    ->whereNull('mo_reservation_detail.deleted_at')
                    ->where('mo_reservation.id', $request->get('id'));

                $datos_reservation = $sql_reservation->first();

                if (!$datos_reservation) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The reservation can not be paid']]);
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                    //Fin validación

                } else {

                    // Preparación de datos
                    $reserva_client_id = null;
                    $reserva_client_session = null;
                    $reserva_agent_id = null;

                    // Si la petición es pública a través de un canal de venta
                    if (strpos($request->path(), 'payment/search') !== false) {
                        if ($user_id != '') {
                            $reserva_client_id = $user_id;
                        } else {
                            $reserva_client_session = Str::random('200');

                        }
                    } else { // Si la petición procede del POS
                        $reserva_agent_id = $user_id;
                    }
                    // FIN preparación de datos

                    if($datos_reservation) {
                        // Preparación de datos *2
                        $reserva_client_id = $datos_reservation->client_id;

                        // Validación por permisos
                        $error_aux = 0;
                        // Si la petición es pública a través de un canal de venta
                        if (strpos($request->path(), 'payment/search') !== false) {
                            
                            if ($user_id != '') {
                                if ($datos_reservation->client_id != $user_id) {
                                    $error_aux = 1;
                                }
                            } elseif ($client_session != '') {
                                if ($datos_reservation->client_session != $client_session) {
                                    $error_aux = 1;
                                }
                            }
                        } else { // Si la petición procede del POS
                            if (!$datos_permisos) {
                                if ($datos_reservation->subchannel_id != $subchannel_id) {
                                    $error_aux = 1;
                                }
                            }
                        }
                    } else {
                        $error_aux = 1;
                    }

                    // FIN Validación por permisos

                    // Validación por estado
                    $sql_estado_carro = DB::connection('tenant')->table('mo_reservation_detail')
                        ->whereNull('mo_reservation_detail.deleted_at')
                        ->where(function ($query) {
                            $query->where('mo_reservation_detail.operation_status_id', 2)
                                ->orWhere('mo_reservation_detail.operation_status_id', 7);
                        })
                        ->where('mo_reservation_detail.reservation_id', $request->get('id'));
                    $datos_estado_carro = $sql_estado_carro->first();
                    if (!$datos_estado_carro) {
                        $error_aux = 1;
                    }
                    // FIN Validación por estado

                    if ($error_aux == 1) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The reservation can not be processed']]);
                    }

                    if ($error == 1) {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = $mensaje_validador;
                    } else {

                        // Preparación de datos
                        $datos_currency = Currency::where('id', $request->get('currency_id'))->first();
                        $fecha_hora = Carbon::now()->format('Y-m-d H:i:s');
                        $datos_currency_exchange = CurrencyExchange::where('currency_id', $request->get('currency_id'))
                            ->where('date_time_start', '<=', $fecha_hora)
                            ->orderBy('date_time_start', 'desc')
                            ->first();
                        // FIN Preparación de datos

                        // Según la forma de pago necesitará más o menos datos y distintas validaciones
                        $response = null;
                        $response_code = null;
                        //$response_company_name = null;
                        //$response_company_code = null;
                        $code = null;
                        //$compropago_place = null;
                        //$response_code_mop = null;
                        //$response_mop = null;
                        $status_ok = 0;
                        $processed = 0;
                        $authorized = 1;
                        $card = null;
                        $bank = null;
                        $bank_name = null;
                        $name = null;
                        $surname = null;
                        $autohorized_code = null;
                        $comments = null;
                        $ingenico_url = null;


                        if ($request->get('card_number') != '') {
                            $inicio = Str::substr($request->get('card_number'), 0, 6);

                            $final = Str::substr($request->get('card_number'), -4);

                            $tarjeta = str_pad($inicio, (strlen($inicio) + strlen($request->get('card_number')) - 10), "*", STR_PAD_RIGHT) . $final;
                            $card = $tarjeta;
                            $name = $request->get('name');
                            $surname = $request->get('surname');

                            $pago = ReservationPayment::create([
                                'reservation_id' => $request->get('id'),
                                'payment_method_id' => $payment_method_id,
                                'subchannel_id' => $emulated_subchannel_id,
                                'client_id' => $reserva_client_id,
                                'agent_id' => $reserva_agent_id,
                                'client_session' => $reserva_client_session,
                                'currency_id' => $request->get('currency_id'),
                                //'amount' => $payment_method_id != 10 ? $request->get('amount') : 0,
                                'amount' => $request->get('amount'),
                                'currency_exchange_id' => $datos_currency_exchange->id,
                                'date_time' => Carbon::now(),
                                'status_ok' => $status_ok,
                                'response' => $response,
                                'response_code' => $response_code,
                                //'response_company_code' => $response_company_code,
                                //'response_company_name' => $response_company_name,
                                //'response_mop' => $response_mop,
                                //'response_code_mop' => $response_code_mop,
                                'code' => $code,
                                //'compropago_place' => $compropago_place,
                                'credit_card' => $card,
                                'name' => $name,
                                'surname' => $surname,
                                'bank' => $bank,
                                'bank_name' => $bank_name,
                                'months_no_interest' => $request->get('months_no_interest'),
                            ]);
                        }


                        if ($metodo_pago->type == 'Tarjeta') {

                            $bank = $request->get('bank_id');
                            $bank_name = $request->get('bank_name');
                            $status_ok = 1;

                            $pago = ReservationPayment::create([
                                'reservation_id' => $request->get('id'),
                                'payment_method_id' => $payment_method_id,
                                'subchannel_id' => $emulated_subchannel_id,
                                'client_id' => $reserva_client_id,
                                'agent_id' => $reserva_agent_id,
                                'client_session' => $reserva_client_session,
                                'currency_id' => $request->get('currency_id'),
                                //'amount' => $payment_method_id != 10 ? $request->get('amount') : 0,
                                'amount' => $request->get('amount'),
                                'currency_exchange_id' => $datos_currency_exchange->id,
                                'date_time' => Carbon::now(),
                                'status_ok' => $status_ok,
                                'response' => $response,
                                'response_code' => $response_code,
                                //'response_company_code' => $response_company_code,
                                //'response_company_name' => $response_company_name,
                                //'response_mop' => $response_mop,
                                //'response_code_mop' => $response_code_mop,
                                'code' => $code,
                                //'compropago_place' => $compropago_place,
                                'credit_card' => $card,
                                'name' => $name,
                                'surname' => $surname,
                                'bank' => $bank,
                                'bank_name' => $bank_name,
                                'months_no_interest' => $request->get('months_no_interest'),
                            ]);

                        } elseif ($metodo_pago->type == 'PayPal') {
                            

                            $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                            $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                            $base_path_system = SaasSettings::where('name', 'base_path_system')
                                ->first()
                                ->value;

                            $protocolo = DB::table('saas_settings')
                                ->where('name', 'protocol')
                                ->first()
                                ->value;

                            $idioma = Language::where('id',$datos_reservation->language_id)->first();

                            $ok_url = $protocolo.$website->domain.'.'.$base_path_system.'/'.Str::lower($idioma->abbreviation).'/' . $request->get('return_url') . '?client_session='.$datos_reservation->client_session;
                            $ko_url = $protocolo.$website->domain.'.'.$base_path_system.'/'.Str::lower($idioma->abbreviation).'/'. $request->get('return_url') .'?error=1&client_session='.$datos_reservation->client_session;

                            $pago = ReservationPayment::create([
                                'reservation_id' => $request->get('id'),
                                'payment_method_id' => $payment_method_id,
                                'subchannel_id' => $emulated_subchannel_id,
                                'client_id' => $reserva_client_id,
                                'agent_id' => $reserva_agent_id,
                                'client_session' => $reserva_client_session,
                                'currency_id' => $request->get('currency_id'),
                                'amount' => $request->get('amount'),
                                'currency_exchange_id' => $datos_currency_exchange->id,
                                'date_time' => Carbon::now()
                            ]);

                            $subcanal = Subchannel::where('id', $request->get('subchannel_id'))->first();

                            $invoice_id = $website->domain.'-'.$subcanal->prefix.'-'.$pago->id;

                            $currency = Currency::where('id', $request->get('currency_id'))->first()->iso_code;

                            $cart = $this->getCart($ko_url, $ok_url, $invoice_id, $request->get('amount'));

                            $paypal_username = Settings::where('name','paypal_username')->first()->value;
                            $paypal_password = Settings::where('name','paypal_password')->first()->value;
                            $paypal_secret = Settings::where('name','paypal_secret')->first()->value;
                            $datos_entorno_paypal = Settings::where('name','paypal_environment')->first()->value;
                   
                            if($datos_entorno_paypal == 'live') {
                                config([
                                    'paypal.live.username' => $paypal_username,
                                    'paypal.live.password' => $paypal_password,
                                    'paypal.live.secret' => $paypal_secret,
                                    'paypal.currency' => $currency,
                                    'paypal.mode' => 'live',
                                ]);
                            } else {
                                config([
                                    'paypal.sandbox.username' => $paypal_username,
                                    'paypal.sandbox.password' => $paypal_password,
                                    'paypal.sandbox.secret' => $paypal_secret,
                                    'paypal.currency' => $currency,
                                    'paypal.mode' => 'sandbox',
                                ]);
                            }

                            $provider = new ExpressCheckout();

                            $request_paypal = $cart;
                            $response_paypal = $provider->setExpressCheckout($cart, false);
                            // if there is no link redirect back with error message
                            if (!$response_paypal['paypal_link']) {
                                $array['error'] = 400;
                                $array['error_description'] = 'Something went wrong with PayPal';
                                $array['error_inputs'][0] = ['paypal' => ['Paypal connection error']];
                            } else {
                                $array['data'][0]['paypal_link'] = $response_paypal['paypal_link'];
                            }

                            $pago->update([
                                'request_paypal' => json_encode($request_paypal),
                                'response_paypal' => json_encode($response_paypal),
                            ]);

                            $status_ok = 0;
                            $code = date('YmdHis').Str::random('5');

                            $pago->update([
                                'code' => $code,
                                'status_ok' => $status_ok
                            ]);

                        } elseif ($metodo_pago->type == 'Cortesia') {
                            $status_ok = 1;
                            $pago = ReservationPayment::create([
                                'reservation_id' => $request->get('id'),
                                'payment_method_id' => $payment_method_id,
                                'subchannel_id' => $emulated_subchannel_id,
                                'client_id' => $reserva_client_id,
                                'agent_id' => $reserva_agent_id,
                                'client_session' => $reserva_client_session,
                                'currency_id' => $request->get('currency_id'),
                                'amount' => $request->get('amount'),
                                'status_ok' => $status_ok,
                                'currency_exchange_id' => $datos_currency_exchange->id,
                                'date_time' => Carbon::now()
                            ]);

                           

                        }
                        // FIN método de pago

                        if ($error == 1) {
                            $mensaje_validador = $mensaje_validador->merge(['External' => ['External request fail']]);
                            $array['error'] = 400;
                            $array['error_description'] = 'The fields are not the required format';
                            $array['error_inputs'][] = $mensaje_validador;
                        } else {
                            $reservation = Reservation::where('id', $request->get('id'))->first();

                            $reservation->credit_card = is_null($card) ? $reservation->credit_card : $card;
                            $reservation->credit_card_number = is_null($credit_card_number) ? $reservation->credit_card_number : encriptar($credit_card_number);
                            $reservation->credit_card_cvv = is_null($credit_card_cvv) ? $reservation->credit_card_cvv : encriptar($credit_card_cvv);
                            $reservation->credit_expiry_date = is_null($credit_expiry_date) ? $reservation->credit_expiry_date : encriptar($credit_expiry_date);
                            $reservation->credit_card_name = is_null($name) ? $reservation->credit_card_name : $name;
                            $reservation->credit_card_surname = is_null($surname) ? $reservation->credit_card_surname : $surname;
                            $reservation->credit_card_type = is_null($credit_card_type) ? $reservation->credit_card_type : $credit_card_type;

                            $reservation->save();

                            if ($status_ok == 1) {

                                // Procesar reserva
                                $respuesta_procesar = $this->procesarReserva($request->get('id'));
                                // FIN Procesar reserva

                                // Si la reserva se pagó completamente se actualizan los estados, fechas de procesamiento y se notifica a los sitemas de terceros
                                if ($respuesta_procesar == 1) {

                                    $processed = 1;

                                    //Generar código anti fraude
                                    // $respuesta_fraude = $this->antiFraud($request->get('id'));

                                    // Notificación hotel
                                    $respuesta = $this->notificarHotel($request->get('id'));

                                    // Si la reserva a Synxis falla, el detalle del hotel queda sin procesar
                                    if (isset($respuesta['error']) && $respuesta['error'] == 1) {
                                        $processed = 0;
                                    }

                                    //Envio de email de confirmación
                                    $envio_sendconfirmation = 1;
                                    $data_subchannel = Subchannel::where('id', $emulated_subchannel_id)->first();
                                    if($data_subchannel->sendconfirmedemail == 0){
                                        $envio_sendconfirmation = 0;
                                    }

                                    $idioma = Language::where('id',$reservation->language_id)->first();

                                    $array_parametros = [
                                        'id' => $request->get('id'),
                                        'send_to_client' => 1,
                                        'lang' => $idioma->abbreviation
                                    ];

                                    $ruta_base = str_replace($request->path(), '', $request->url());
                                    $ruta_final = $ruta_base.'api/v1/reservation/sendconfirmedemail/search';

                                    //Fin de envio de email de confirmación

                                    ReservationLog::create([
                                        'date' => Carbon::now()->format('Y-m-d H:i:s'),
                                        'client_id' => $reserva_client_id,
                                        'agent_id' => $reserva_agent_id,
                                        'action_id' => 2,
                                        'reservation_id' => $request->get('id'),
                                        'reservation_detail_id' => null,
                                        'subchannel_id' => $emulated_subchannel_id,
                                    ]);
                                }
                            }

                            $array['data'][0]['status'] = [
                                'status_ok' => $status_ok,
                                'processed' => $processed,
                                'authorized' => $authorized,
                            ];
                            $array['data'][0]['id'] = $code;
                        }
                    }
                }
            }

            DB::connection('tenant')->commit();

            if($respuesta_procesar == 1){
                if($envio_sendconfirmation == 1) {

                    $client = new Client();
                    $res = $client->request('POST', $ruta_final, ['headers' => ['Authorization' => $request->header('Authorization')], 'json' => $array_parametros, 'http_errors' => false]);
                    $respuesta = response()->json(\GuzzleHttp\json_decode($res->getBody()->getContents(), true), $res->getStatusCode());
                }
            }

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Pagos');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para procesar la reserva
     *
     * Si los importes de los pagos acumulados superan el importe total de la reserva, se marca con estado procesada
     *
     * @param Integer $id
     * @return Integer
     */
    private function procesarReserva($id)
    {

        // Si los importes pagados cubren el importe de la reserva se procesa dicha reserva

        $total = reservationTotalToPay($id);

        $total_pagado = reservationTotalPaid($id);

        // Respuestas
        // 0 = el importe de los pagos no cubre el total de la reserva
        // 1 = el importe de los pagos cubre la reserva
        // FIN Respuestas
        $respuesta = 0;

        if (round($total_pagado,5) >= round($total,5)) {

            $respuesta = 1;

            $fecha_hora_procesamiento = Carbon::now()->format('Y-m-d H:i:s');

            // Actualización estado de los elementos de reserva
            ReservationDetail::where('reservation_id', $id)
                ->where(function ($query) {
                    $query->where('operation_status_id', 2)
                        ->orWhere('operation_status_id', 7);
                })
                ->update([
                    'operation_status_id' => 3,
                    'payment_status_id' => 2,
                    'date_time_processed' => $fecha_hora_procesamiento,
                ]);

            // Actualización estado del seguro de la reserva (si tiene)
            ReservationInsurance::where('reservation_id', $id)
                ->where(function ($query) {
                    $query->where('operation_status_id', 2)
                        ->orWhere('operation_status_id', 7);
                })
                ->update([
                    'operation_status_id' => 3,
                    'payment_status_id' => 2,
                    'date_time_processed' => $fecha_hora_procesamiento,
                ]);
        } else {
            $respuesta = 0;
        }

        return $respuesta;
    }

    /**
     * Función que establece el nivel de antifraude para una reserva
     *
     * @param $id
     * @return int
     */
    private function antiFraud($id)
    {

        $sql_reservation = DB::connection('tenant')->table('mo_reservation')
            ->select(
                'mo_reservation.id',
                'mo_reservation_detail.id as reservation_detail_id',
                'mo_reservation_detail.currency_id',
                'mo_reservation_detail.service_id',
                'mo_product.type_id'
            )
            ->where('mo_reservation.id', $id)
            ->whereNull('mo_reservation.deleted_at')
            ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
            ->whereNull('mo_reservation_detail.deleted_at')
            ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
            ->whereNull('mo_product.deleted_at')
            ->join('mo_reservation_payment', 'mo_reservation_payment.reservation_id', 'mo_reservation.id')
            ->where(function ($query) {
                $query->where('mo_reservation_detail.operation_status_id', 3)
                    ->orWhere('mo_reservation_detail.operation_status_id', 4);
            })
            ->whereNull('mo_reservation_payment.deleted_at');

        $datos_reservation_detail = DB::connection('tenant')->table('mo_reservation_detail_client_type')
            ->select(
                'mo_reservation_detail_client_type.sale_price'
            )
            ->whereNull('mo_reservation_detail_client_type.deleted_at')
            ->join('mo_reservation_detail', 'mo_reservation_detail.id', 'mo_reservation_detail_client_type.reservation_detail_id')
            ->whereNull('mo_reservation_detail.deleted_at')
            ->where('mo_reservation_detail.reservation_id', $id)
            ->where(function ($query) {
                $query->where('mo_reservation_detail.operation_status_id', 3)
                    ->orWhere('mo_reservation_detail.operation_status_id', 4);
            })
            ->sum('mo_reservation_detail_client_type.sale_price');

        $total_pagar = $datos_reservation_detail;

        $datos_reservation_insurance = DB::connection('tenant')->table('mo_reservation_insurance')
            ->select(
                'mo_reservation_insurance.total_price'
            )
            ->whereNull('mo_reservation_insurance.deleted_at')
            ->where('mo_reservation_insurance.reservation_id', $id)
            ->first();

        $sql_pagos_tarjeta = DB::connection('tenant')->table('mo_reservation_payment')
            ->whereNull('mo_reservation_payment.deleted_at');


        $sql_reservation_anti_fraud = DB::connection('tenant')->table('mo_reservation_anti_fraud_rule')
            ->whereNull('mo_reservation_anti_fraud_rule.deleted_at')
            ->join('mo_reservation_anti_fraud_level', 'mo_reservation_anti_fraud_level.id', 'mo_reservation_anti_fraud_rule.level_id')
            ->whereNull('mo_reservation_anti_fraud_level.deleted_at');

        $sql_level_anti_fraud = DB::connection('tenant')->table('mo_reservation_anti_fraud_level')
            ->whereNull('mo_reservation_anti_fraud_level.deleted_at')
            ->orderBy('mo_reservation_anti_fraud_level.order', 'desc');

        $datos_level_anti_fraud = $sql_level_anti_fraud->get();

        $datos_reservation = $sql_reservation->get();

        if ($datos_reservation_insurance) {
            $total_pagar += $datos_reservation_insurance->total_price;
        }

        $anti_fraud_level = null;

        $anticipation_days = null;

        foreach ($datos_reservation as $dato) {
            $datos = null;
            $date = null;
            if ($dato->service_id != '') {
                if ($dato->service_id == 1) {
                    $datos = ReservationDetailTransportation::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);
                }

                if ($dato->service_id == 2) {
                    $datos = ReservationDetailFood::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);
                }

                if ($dato->service_id == 3) {
                    $datos = ReservationDetailPhoto::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);
                }


                if ($dato->service_id == 4) {
                    $datos = ReservationDetailDayMore::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);
                }

            } else {

                if ($dato->type_id == 1) {
                    $datos = ReservationDetailPackage::where('reservation_detail_id', $dato->reservation_detail_id)->orderBy('date', 'asc')->first();

                    $date = Carbon::parse($datos->date);
                }

                if ($dato->type_id == 2) {
                    $datos = ReservationDetailHotel::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date_start);
                }

                if ($dato->type_id == 3) {
                    $datos = ReservationDetailActivity::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);
                }

                if ($dato->type_id == 4) {
                    $datos = ReservationDetailEvent::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);
                }

                if ($dato->type_id == 5) {
                    $datos = ReservationDetailTour::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);
                }

                if ($dato->type_id == 6) {
                    $datos = ReservationDetailPark::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);

                }

                if ($dato->type_id == 7) {
                    $datos = ReservationDetailRestaurant::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);

                }

                if ($dato->type_id == 8) {
                    $datos = ReservationDetailBenefitCard::where('reservation_detail_id', $dato->reservation_detail_id)->first();

                    $date = Carbon::parse($datos->date);

                }
            }

            $sql_pagos_tarjeta_clone = clone $sql_pagos_tarjeta;

            $datos_tarjeta = $sql_pagos_tarjeta_clone->where('mo_reservation_payment.reservation_id', $dato->id)
                ->groupBy('mo_reservation_payment.credit_card')
                ->get();

            $diferencia = $date->startOfDay()->diff(Carbon::now()->startOfDay())->days;

            if (is_null($anticipation_days) || $diferencia < $anticipation_days) {
                $anticipation_days = $diferencia;
            }


            $level = 0;

            $continuar = 1;
            foreach ($datos_level_anti_fraud as $fraud_level) {

                $sql_reservation_anti_fraud_clone = clone $sql_reservation_anti_fraud;

                $datos_anti_fraud = $sql_reservation_anti_fraud_clone->where('mo_reservation_anti_fraud_rule.level_id', $fraud_level->id)->get();

                if (count($datos_anti_fraud) < 1) {
                    $anti_fraud_level = $fraud_level->id;
                    $continuar = 1;
                } else {
                    if ($continuar == 1) {
                        $continuar = 0;
                        foreach ($datos_anti_fraud as $rule) {


                            //Dias de anticipación
                            if (!is_null($rule->anticipation_days) && is_null($rule->limit_hour)) {
                                if ($date->startOfDay()->diff(Carbon::now()->startOfDay())->days < $rule->anticipation_days) {
                                    $continuar = 1;
                                } else {
                                    $anti_fraud_level = $rule->level_id;
                                }
                            }

                            //Cantidad de coste
                            if (!is_null($rule->amount) && !is_null($rule->currency_id)) {

                                $total_convertido = conversor($total_pagar, $dato->currency_id, $rule->currency_id);

                                if ($total_convertido < $rule->amount) {
                                    $continuar = 1;
                                } else {
                                    $level = $rule->level_id;
                                }
                            }

                            //Cantidad de tipos de producto
                            if (!is_null($rule->amount) && !is_null($rule->product_type_id)) {

                                $sql_reservation_product_type = clone $sql_reservation;

                                $datos_product_type = $sql_reservation_product_type->where('mo_product.type_id', $rule->product_type_id)
                                    ->whereNull('mo_reservation_detail.service_id')
                                    ->groupBy('mo_reservation_detail.id')
                                    ->count();

                                if ($datos_product_type < $rule->amount) {
                                    $continuar = 1;
                                } else {
                                    $level = $rule->level_id;
                                }
                            }


                            //Compras por usuario
                            if (!is_null($rule->amount) && !is_null($rule->same_user) && !is_null($rule->user_week)) {
                                $datos_reservation = $sql_reservation->where('mo_reservation_payment.date_time', '>=', Carbon::now()->subWeeks($rule->user_week)->startOfDay())->get()->count();

                                if ($datos_reservation < $rule->amount) {
                                    $continuar = 1;
                                } else {
                                    $level = $rule->level_id;
                                }
                            }

                            if (!is_null($rule->diferent_buyer)) {


                                $sql_pagos_tarjeta_clone = clone $sql_pagos_tarjeta;

                                $datos_pagos_tarjeta = $sql_pagos_tarjeta_clone->where('mo_reservation_payment.reservation_id', $id)->get();

                                $visitante = 0;

                                foreach ($datos_pagos_tarjeta as $pago) {
                                    $sql_reservation_buyer = clone $sql_reservation;

                                    $datos_reservation_buyer = $sql_reservation_buyer
                                        ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                        ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                        ->where(function ($query) use ($pago) {
                                            $query->where(function ($query) use ($pago) {
                                                $query->where('mo_reservation.client_name', $pago->name)
                                                    ->where('mo_reservation.client_surname', $pago->surname);
                                            })->where(function ($query) use ($pago) {
                                                $query->where('mo_reservation_detail_client_type.client_name', $pago->name)
                                                    ->where('mo_reservation_detail_client_type.client_surname', $pago->surname);
                                            });
                                        })->first();


                                    if ($datos_reservation_buyer) {
                                        $visitante = 1;
                                    }
                                }

                                if ($visitante == 0) {
                                    $continuar = 1;
                                } else {
                                    $level = $rule->level_id;
                                }
                            }

                            //Pax
                            if (!is_null($rule->amount) && is_null($rule->product_type_id) && is_null($rule->currency_id) && is_null($rule->same_user) && is_null($rule->same_card) && is_null($rule->card_week) && is_null($rule->user_week)) {

                                $sql_reservation_pax = clone $sql_reservation;
                                $datos_reservation_pax = $sql_reservation_pax
                                    ->join('mo_reservation_detail_client_type', 'mo_reservation_detail_client_type.reservation_detail_id', 'mo_reservation_detail.id')
                                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                                    ->groupBy('mo_reservation_detail_client_type.id')
                                    ->get()
                                    ->count();

                                if ($datos_reservation_pax < $rule->amount) {
                                    $continuar = 1;

                                } else {
                                    $level = $rule->level_id;
                                }
                            }

                            //Fecha limite
                            if (!is_null($rule->limit_hour)) {
                                if (Carbon::now() > Carbon::createFromTimeString($rule->limit_hour) && Carbon::now()->addDays($rule->anticipation_days)->startOfDay() == $date) {
                                    $level = $rule->level_id;
                                } else {
                                    $continuar = 1;
                                }
                            }

                            //Tarjetas
                            if (!is_null($rule->same_card) && is_null($rule->card_day) && is_null($rule->card_week)) {

                                foreach ($datos_tarjeta as $tarjeta) {
                                    $sql_pagos_tarjeta_count_clone = clone $sql_pagos_tarjeta;

                                    $datos_tarjeta_count = $sql_pagos_tarjeta_count_clone->where('mo_reservation_payment.credit_card', $tarjeta->credit_card)->get()->count();
                                    if ($datos_tarjeta_count < $rule->amount) {
                                        $continuar = 1;
                                    } else {
                                        $level = $rule->level_id;
                                    }
                                }
                            }

                            if (!is_null($rule->same_card) && !is_null($rule->card_day)) {

                                foreach ($datos_tarjeta as $tarjeta) {
                                    $sql_pagos_tarjeta_count_clone = clone $sql_pagos_tarjeta;

                                    $datos_tarjeta_count = $sql_pagos_tarjeta_count_clone->where('mo_reservation_payment.credit_card', $tarjeta->credit_card)
                                        ->where('mo_reservation_payment.date_time', '>=', Carbon::now()->subDays($rule->card_day)->startOfDay())->get()->count();

                                    if ($datos_tarjeta_count < $rule->amount) {
                                        $continuar = 1;
                                    } else {
                                        $level = $rule->level_id;
                                    }
                                }
                            }

                            if (!is_null($rule->same_card) && !is_null($rule->card_week)) {

                                foreach ($datos_tarjeta as $tarjeta) {
                                    $sql_pagos_tarjeta_count_clone = clone $sql_pagos_tarjeta;

                                    $datos_tarjeta_count = $sql_pagos_tarjeta_count_clone->where('mo_reservation_payment.credit_card', $tarjeta->credit_card)
                                        ->where('mo_reservation_payment.date_time', '>=', Carbon::now()->subWeeks($rule->card_week)->startOfDay())->get()->count();

                                    if ($datos_tarjeta_count >= $rule->amount) {
                                        $continuar = 1;
                                    } else {
                                        $level = $rule->level_id;
                                    }
                                }
                            }

                            if ($level > $anti_fraud_level) {
                                $anti_fraud_level = $level;
                                $continuar = 1;
                            }

                        }
                    }
                }
            }
        }

        $level = ReservationAntifraudLevel::where('id', $anti_fraud_level)->first();

        Reservation::where('id', $id)->update([
            'anti_fraud_level' => $anti_fraud_level,
            'anti_fraud_code' => $level->code . $anticipation_days
        ]);


        return 1;
    }

    public function paypalProcessPayment(Request $request) {

        $array['error'] = 200;

        try {

            DB::beginTransaction();



            //Validacion
            $error = 0;
            $status_ok = 0;
            $processed = 0;
            $respuesta_procesar = 0;

            $mensaje_validador = collect();
            $validator = \Validator::make($request->all(), [
                'token' => 'required'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $user_id = '';
            $client_session = '';

            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = UserToken::where('token', '=', $token[1])->where('expired_at', '>=', Carbon::now())->first();

                if ($user_token) {
                    $user_id = $user_token->user_id;
                } else {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                }

            } elseif ($request->get('client_session') != '') { // Si no está logueado
                $client_session = $request->get('client_session');

            } else { // Si no está logueado y no aporta identificación temporal
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token or session is required']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                $website = DB::table('saas_website')
                        ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                        'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                        'saas_website.default_language_id', 'saas_website.default_currency_id')
                        ->whereNull('saas_website.deleted_at')
                        ->where('saas_website.website_id', $website->id)
                        ->first();

                $base_path_system = SaasSettings::where('name', 'base_path_system')
                    ->first()
                    ->value;

                $datos_entorno_paypal = Settings::where('name','paypal_environment')->first()->value;

                
                $protocolo = DB::table('saas_settings')
                                ->where('name', 'protocol')
                                ->first()
                                ->value;

                $paypal_username = Settings::where('name','paypal_username')->first()->value;
                $paypal_password = Settings::where('name','paypal_password')->first()->value;
                $paypal_secret = Settings::where('name','paypal_secret')->first()->value;

                $currency = DB::connection('tenant')->table('mo_currency')->select([
                    'mo_currency.iso_code'
                ])->whereNull('mo_currency.deleted_at')
                ->join('mo_reservation_detail','mo_reservation_detail.currency_id','mo_currency.id')
                ->whereNull('mo_reservation_detail.deleted_at')
                ->join('mo_reservation', 'mo_reservation.id','mo_reservation_detail.reservation_id')
                ->whereNull('mo_reservation.deleted_at')
                ->where('mo_currency.id', $request->get('currency_id'))
                ->first()->iso_code;

                if($datos_entorno_paypal == 'live') {
                    config([
                        'paypal.live.username' => $paypal_username,
                        'paypal.live.password' => $paypal_password,
                        'paypal.live.secret' => $paypal_secret,
                        'paypal.currency' => $currency,
                        'paypal.mode' => 'live',
                    ]);
                } else {
                    config([
                        'paypal.sandbox.username' => $paypal_username,
                        'paypal.sandbox.password' => $paypal_password,
                        'paypal.sandbox.secret' => $paypal_secret,
                        'paypal.currency' => $currency,
                        'paypal.mode' => 'sandbox',
                    ]);
                }

                $provider = new ExpressCheckout();

                $response = $provider->getExpressCheckoutDetails($request->get('token'));
                
                if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                    $array['error'] = 400;
                    $array['error_description'] = 'Error processing PayPal payment';
                    $array['error_inputs'][0] = ['paypal' => ['Paypal connection error']];
                } else {

                    $invoice_id = explode('-', $response['INVNUM'])[2];

                    $payment = ReservationPayment::where('id', '=', $invoice_id)->whereNull('response')->first();

                    $sql_carro_user = DB::connection('tenant')->table('mo_reservation')
                        ->select(
                            'mo_reservation.id',
                            'mo_reservation_detail.currency_id'
                        )
                        ->whereNull('mo_reservation.deleted_at')
                        ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
                        ->whereNull('mo_reservation_detail.deleted_at')
                        ->orderBy('mo_reservation.id', 'desc');

                    if ($user_id != '') {
                        $sql_carro_user->where('mo_reservation.client_id', $user_id);
                        if ($request->get('reservation_id') != '') {
                            $sql_carro_user->where('mo_reservation.id', $request->get('reservation_id'));
                        }
                    } elseif ($client_session != '') {
                        $sql_carro_user->where('mo_reservation.client_session', $client_session);
                    } else {
                        $sql_carro_user->where('mo_reservation.id', 0);
                    }

                    $datos_carro_user = $sql_carro_user->first();

                    if(!$payment) { 
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The reservation can not be processed']]);
                    } else {
                        if($datos_carro_user) {
                            if($datos_carro_user->id != $payment->reservation_id) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The reservation can not be processed']]);
                            }
                        } else {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The reservation can not be processed']]);
                        }
                    }

                    if($error == 1) {

                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][0] = $mensaje_validador;
                    }else {
                        $reservation = Reservation::where('id', $payment->reservation_id)->first();

                        $subcanal = Subchannel::where('id', $request->get('subchannel_id'))->first();

                        $ok_url = $protocolo.$website->domain.'.'.$base_path_system.'/processpaypal';
                        $ko_url = $protocolo.$website->domain.'.'.$base_path_system.'/processpaypal?error=1';

                        $invoice_id = $website->domain.'-'.$subcanal->prefix.'-'.$payment->id;

                        $cart = $this->getCart($ko_url, $ok_url, $invoice_id, $payment->amount);

                        if($datos_entorno_paypal == 'live') {
                            config([
                                'paypal.live.username' => $paypal_username,
                                'paypal.live.password' => $paypal_password,
                                'paypal.live.secret' => $paypal_secret,
                                'paypal.currency' => $currency,
                                'paypal.mode' => 'live',
                            ]);
                        } else {
                            config([
                                'paypal.sandbox.username' => $paypal_username,
                                'paypal.sandbox.password' => $paypal_password,
                                'paypal.sandbox.secret' => $paypal_secret,
                                'paypal.currency' => $currency,
                                'paypal.mode' => 'sandbox',
                            ]);
                        }

                        $response = $provider->doExpressCheckoutPayment($cart, $request->get('token'), $request->get('PayerID'));
                        
                        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                            $status_ok = 1;
                        }

                        $payment->response_code = json_encode($request->get('token'));
                        $payment->response = json_encode($response);

                        if($status_ok == '1') {

                            $payment->status_ok = 1;

                            $payment->save();

                            if($reservation) {
                                $respuesta_procesar = $this->procesarReserva($reservation->id);

                                if ($respuesta_procesar == 1) {
                                    $processed = 1;
                                    //Generar código anti fraude
                                    // $respuesta_fraude = $this->antiFraud($reservation->id);

                                    // Notificación hotel
                                    $respuesta = $this->notificarHotel($reservation->id);

                                    // Si la reserva a Synxis falla, el detalle del hotel queda sin procesar
                                    if (isset($respuesta['error']) && $respuesta['error'] == 1) {
                                        $processed = 0;
                                    }

                                    $idioma = Language::where('id',$reservation->language_id)->first();

                                    $array_parametros = [
                                        'id' => $reservation->id,
                                        'send_to_client' => 1,
                                        'lang' => $idioma->abbreviation
                                    ];

                                    //Envio de email de confirmación
                                    $envio_sendconfirmation = 1;
                                    $data_subchannel = Subchannel::where('id', $request->get('subchannel_id'))->first();
                                    if($data_subchannel->sendconfirmedemail == 0){
                                        $envio_sendconfirmation = 0;
                                    }
                                }
                            }
                        }
                        $array['data'][0]['status'] = [
                            'status_ok' => $status_ok,
                            'processed' => $processed
                        ];
                    }  
                }
            }

            DB::commit();

            if($respuesta_procesar == 1){
                if($envio_sendconfirmation == 1) {

                    $ruta_base = str_replace($request->path(), '', $request->url());
                    $ruta_final = $ruta_base.'api/v1/reservation/sendconfirmedemail/search';

                    $client = new Client();
                    $res = $client->request('POST', $ruta_final, ['headers' => ['Authorization' => $request->header('Authorization')], 'json' => $array_parametros, 'http_errors' => false]);
                    $respuesta = response()->json(\GuzzleHttp\json_decode($res->getBody()->getContents(), true), $res->getStatusCode());
                }
            }

        } catch(\Exception $e) {
            DB::rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage() . ' ' . $e->getLine();
            reportSaasService($e, 'SaasPayment');
        }
        return response()->json($array, $array['error']);
    }

    public function getCart($ko_url, $ok_url, $invoice_id, $amount){
        $cart = array();

        $invoice_name = Settings::where('name','nombre_cliente')->first()->value;

        $cart = [];

        $cart['items'] = [
            [
                'name'  => $invoice_name,
                'price' => round($amount, 2),
                'qty'   => 1,
                'desc' => $invoice_name
            ]
        ];


        $cart['invoice_id'] = $invoice_id;
        $cart['invoice_description'] = $invoice_name;
        $cart['return_url'] = $ok_url;
        $cart['cancel_url'] = $ko_url;

        $cart['total'] = round($amount, 2);

        return $cart;
    }

    private function notificarHotel($id)
    {

        $array_respuesta['error'] = 0;

        $sql_reservation = DB::connection('tenant')->table('mo_reservation')
            ->select(
                'mo_reservation.id',
                'mo_reservation.subchannel_id',
                'mo_reservation.client_id',
                'mo_reservation.client_name',
                'mo_reservation.client_surname',
                'mo_reservation.client_email',
                'mo_reservation.client_country_id',
                'mo_reservation.client_telephone1',
                'mo_reservation_detail.id as reservation_detail_id',
                'mo_reservation.code',
                'mo_reservation.credit_card_number',
                'mo_reservation.credit_card_cvv',
                'mo_reservation.credit_expiry_date',
                'mo_reservation.credit_card_name',
                'mo_reservation.credit_card_surname',
                'mo_reservation.credit_card_type',
                'mo_product.id as product_id',
                'mo_product.external_connection',
                'mo_product.external_connection_url',
                'mo_product.external_code',
                'mo_product.external_service_user',
                'mo_product.external_service_password',
                'mo_reservation_detail_hotel.date_start',
                'mo_reservation_detail_hotel.date_end'
            )
            ->whereNull('mo_reservation.deleted_at')
            ->join('mo_reservation_detail', 'mo_reservation_detail.reservation_id', 'mo_reservation.id')
            ->whereNull('mo_reservation_detail.deleted_at')
            ->where('mo_reservation.id', $id)
            ->join('mo_product', 'mo_product.id', 'mo_reservation_detail.product_id')
            ->whereNull('mo_product.deleted_at')
            ->join('mo_reservation_detail_hotel', 'mo_reservation_detail_hotel.reservation_detail_id', 'mo_reservation_detail.id')
            ->whereNull('mo_reservation_detail_hotel.deleted_at')
            ->where('mo_product.external_connection', 1)
            ->where('mo_product.type_id', 2)
            ->where('mo_reservation_detail.operation_status_id', 3);


        $datos_reservation = $sql_reservation->get();

        $respuesta = null;


        if (count($datos_reservation) > 0) {


            $datos_payment = PaymentMethod::where('id', $datos_reservation[0]->credit_card_type)->first();

            $array_pago = [
                'card_number' => desencriptar($datos_reservation[0]->credit_card_number),
                'cvv' => desencriptar($datos_reservation[0]->credit_card_cvv),
                'expiry_date' => desencriptar($datos_reservation[0]->credit_expiry_date),
                'name' => $datos_reservation[0]->credit_card_name,
                'surname' => $datos_reservation[0]->credit_card_surname,
                'api_code' => $datos_payment->api_code,
            ];

            foreach ($datos_reservation as $datos) {
                $error = 0;
                $cliente = iniciarClienteSoap($datos->external_connection_url, $datos->external_service_user, $datos->external_service_password);


                $datos_reservations_client_types = DB::connection('tenant')->table('mo_reservation_detail_client_type')
                    ->select(
                        'mo_reservation_detail_client_type.reservation_detail_id',
                        'mo_reservation_detail_client_type.client_type_id',
                        'mo_reservation_detail_client_type.age',
                        'mo_client_type.hotel_id',
                        DB::connection('tenant')->raw('count(mo_reservation_detail_client_type.id) as total_client_type')
                    )
                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                    ->join('mo_client_type', 'mo_client_type.id', 'mo_reservation_detail_client_type.client_type_id')
                    ->whereNull('mo_reservation_detail_client_type.deleted_at')
                    ->where('mo_reservation_detail_client_type.reservation_detail_id', $datos->reservation_detail_id)
                    ->groupBy('mo_reservation_detail_client_type.id')
                    ->get();

                $sql_reservations_rooms = DB::connection('tenant')->table('mo_reservation_detail_hotel_room')
                    ->select(
                        'mo_reservation_detail_hotel_room.id',
                        'mo_reservation_detail_hotel_room.rate_plan_code',
                        'mo_reservation_detail_hotel_room.confirmation_code',
                        'mo_hotel_room.code'
                    )
                    ->whereNull('mo_reservation_detail_hotel_room.deleted_at')
                    ->join('mo_reservation_detail_hotel', 'mo_reservation_detail_hotel.id', 'mo_reservation_detail_hotel_room.reservation_hotel_id')
                    ->whereNull('mo_reservation_detail_hotel.deleted_at')
                    ->join('mo_hotel_room', 'mo_hotel_room.id', 'mo_reservation_detail_hotel_room.room_id')
                    ->whereNull('mo_hotel_room.deleted_at')
                    ->where('mo_reservation_detail_hotel.reservation_detail_id', $datos->reservation_detail_id);

                $sql_reservations_rooms_clone = clone $sql_reservations_rooms;


                $datos_reservations_rooms = $sql_reservations_rooms_clone
                    ->whereNull('mo_reservation_detail_hotel_room.confirmation_code')
                    ->get();

                $array_rooms = array();

                foreach ($datos_reservations_rooms as $rooms) {

                    $array_rooms[] = $rooms->id;
                    $peticion = new SimpleXMLElement(' <OTA_HotelResRQ PrimaryLangID="en" EchoToken="12345" ResStatus="Commit" xmlns="http://www.opentravel.org/OTA/2003/05"></OTA_HotelResRQ>');
//
                    generarPOS($peticion);
//
                    $peticion->addChild('HotelReservations');
                    $peticion->HotelReservations->addChild('HotelReservation');
                    $peticion->HotelReservations->HotelReservation->addAttribute('RoomStayReservation', 'true');
                    $peticion->HotelReservations->HotelReservation->addChild('RoomStays');
                    $peticion->HotelReservations->HotelReservation->RoomStays->addChild('RoomStay');
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->addChild('RoomTypes');

                    $habitacion = null;

                    $habitacion = $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomTypes->addChild('RoomType');

                    $habitacion->addAttribute('RoomTypeCode', $rooms->code);

                    $habitacion->addAttribute('NumberOfUnits', "1");


                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->addChild('RatePlans');
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->RatePlans->addChild('RatePlan');
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->RatePlans->RatePlan->addAttribute('RatePlanCode', $rooms->rate_plan_code);

                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->addChild('RoomRates');
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomRates->addChild('RoomRate');
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomRates->RoomRate->addAttribute('EffectiveDate', $datos->date_start);
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->RoomRates->RoomRate->addAttribute('ExpireDate', $datos->date_end);

                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->addChild('GuestCounts');

                    $rooms_client_types = array();

                    $rooms_client_types['client_types'] = array();
                    foreach($datos_reservations_client_types as $client_type) {
                        if(!is_null($client_type->age)) {
                            $guest = array();
                            $guest['hotel_id'] = $client_type->hotel_id;
                            $guest['age'] = $client_type->age;
                            $guest['quantity'] = 1;
                            $rooms_client_types['client_types'][] = $guest;
                        } else {
                            $agrupamiento = array_search($client_type->hotel_id, array_column($rooms_client_types['client_types'],'hotel_id'));

                            if($agrupamiento === false) {
                                $guest = array();
                                $guest['hotel_id'] = $client_type->hotel_id;
                                $guest['quantity'] = 1;
                                $rooms_client_types['client_types'][] = $guest;
                            } else {
                                $rooms_client_types['client_types'][$agrupamiento]['quantity'] = $rooms_client_types['client_types'][$agrupamiento]['quantity'] + 1;
                            }
                        }
                    }

                    foreach ($rooms_client_types['client_types'] as $client_type) {

                        $tipo_cliente = null;

                        $tipo_cliente = $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->GuestCounts->addChild('GuestCount');

                        $tipo_cliente->addAttribute('AgeQualifyingCode', $client_type['hotel_id']);

                        $tipo_cliente->addAttribute('Count', $client_type['quantity']);

                        if(isset($client_type['age'])) {
                            $tipo_cliente->addAttribute('age', $client_type['age']);
                        }

                    }


                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->addChild('TimeSpan');
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->TimeSpan->addAttribute('Start', $datos->date_start);
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->TimeSpan->addAttribute('End', $datos->date_end);

                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->addChild('BasicPropertyInfo');
                    $peticion->HotelReservations->HotelReservation->RoomStays->RoomStay->BasicPropertyInfo->addAttribute('HotelCode', $datos->external_code);

                    $peticion->HotelReservations->HotelReservation->addChild('ResGuests');
                    $peticion->HotelReservations->HotelReservation->ResGuests->addChild('ResGuest');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->addChild('Profiles');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->addChild('ProfileInfo');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->addChild('Profile');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->addChild('Customer');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->addChild('PersonName');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->addChild('NamePrefix');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->NamePrefix = 'MR';

                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->addChild('GivenName');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->GivenName = $datos->client_name;
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->addChild('MiddleName');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->MiddleName = '';
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->addChild('Surname');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->PersonName->Surname = $datos->client_surname;

                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->addChild('Telephone');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->Telephone->addAttribute('PhoneTechType', 1);
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->Telephone->addAttribute('PhoneNumber', $datos->client_telephone1);

                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->addChild('Email');
                    $peticion->HotelReservations->HotelReservation->ResGuests->ResGuest->Profiles->ProfileInfo->Profile->Customer->Email = $datos->client_email;

                    $peticion->HotelReservations->HotelReservation->addChild('ResGlobalInfo');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->addChild('Guarantee');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->addChild('GuaranteesAccepted');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->addChild('GuaranteeAccepted');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->GuaranteeAccepted->addChild('PaymentCard');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->GuaranteeAccepted->PaymentCard->addAttribute('CardCode', $array_pago['api_code']);
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->GuaranteeAccepted->PaymentCard->addAttribute('CardNumber', $array_pago["card_number"]);
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->GuaranteeAccepted->PaymentCard->addAttribute('SeriesCode', $array_pago["cvv"]);
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->GuaranteeAccepted->PaymentCard->addAttribute('ExpireDate', $array_pago["expiry_date"]);

                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->GuaranteeAccepted->PaymentCard->addChild('CardHolderName');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->Guarantee->GuaranteesAccepted->GuaranteeAccepted->PaymentCard->CardHolderName = $array_pago['name'] . ' ' . $array_pago['surname'];


                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->addChild('WrittenConfInst');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->WrittenConfInst->addChild('WrittenConfInst');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->WrittenConfInst->WrittenConfInst->addChild('SupplementalData');
                    $peticion->HotelReservations->HotelReservation->ResGlobalInfo->WrittenConfInst->WrittenConfInst->addChild('SupplementalData');


                    $xml = $peticion->asXML();
                    $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml);
                    $param = new SoapVar($xml, XSD_ANYXML);

                    $respuesta = $cliente->CreateReservations($param);

                    $response_to_save = [
                        'external_request' => "'" . $xml . "'",
                        'external_response' => json_encode($respuesta),
                    ];

                    if (isset($respuesta->HotelReservations) && isset($respuesta->HotelReservations->HotelReservation) && isset($respuesta->HotelReservations->HotelReservation->ResGlobalInfo)) {

                        $ids = $respuesta->HotelReservations->HotelReservation->ResGlobalInfo->HotelReservationIDs->HotelReservationID;
                        $api_reserva_id = null;

                        foreach ($ids as $id) {
                            if ($id->ResID_Type == 14) {
                                $api_reserva_id = $id->ResID_Value;
                            }
                        }

                        $response_to_save['confirmation_code'] = $api_reserva_id;

                    } else {
                        $error = 1;
                    }

                    ReservationDetailHotelRoom::where('id', $rooms->id)->update($response_to_save);
                }

                if ($error == 1) {
                    $array_respuesta['error'] = 1;


                    $sql_reservations_rooms_cancel = clone $sql_reservations_rooms;


                    $datos_reservations_rooms = $sql_reservations_rooms_cancel
                        ->whereIn('mo_reservation_detail_hotel_room.id', $array_rooms)
                        ->get();


                    foreach ($datos_reservations_rooms as $room) {
                        $peticion = new SimpleXMLElement(' <OTA_CancelRQ xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" EchoToken="[token]"></OTA_CancelRQ>');
//
                        generarPOS($peticion);

                        $peticion->addChild('UniqueID');
                        $peticion->UniqueID->addAttribute('Type', 14);
                        $peticion->UniqueID->addAttribute('ID', $room->confirmation_code);
                        $peticion->UniqueID->addAttribute('ID_Context', 'CrsConfirmNumber');

                        $peticion->addChild('Verification');
                        $peticion->Verification->addChild('TPA_Extensions');
                        $peticion->Verification->TPA_Extensions->addChild('BasicPropertyInfo');
                        $peticion->Verification->TPA_Extensions->BasicPropertyInfo->addAttribute('HotelCode', $datos->external_code);

                        $xml = $peticion->asXML();
                        $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml);
                        $param = new SoapVar($xml, XSD_ANYXML);

                        $respuesta = $cliente->CancelReservations($param);

                    }

                } else {
                    $array_respuesta['error'] = 0;
                }
            }
        }

        return $array_respuesta;
    }
}