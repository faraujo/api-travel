<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\ProductPackage;
use App\Models\tenant\ProductTranslation;
use App\Models\tenant\Settings;
use App\Models\tenant\Subchannel;
use App\Models\tenant\ViewProduct;
use App\Models\tenant\ViewProductFile;
use App\Models\tenant\ViewProductTranslation;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use Illuminate\Http\Request;

class ViewProductController extends Controller
{


    /**
     * Función para la creación de un producto y sus traducciones
     *
     * En este método se realizará la actualización de un registro de la tabla mo_view_product si existe en el mismo registro el campo product_id y subchannel_id que se envían en la petición.
     * En caso de que no exista se creará.
     * Para la modificación / creación de productos por subcanal se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $array['error'] = 200;

        try {


            DB::connection('tenant')->beginTransaction();


            $idiomas = Language::where('front', 1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();


            $validator = Validator::make($request->all(), [

                'order' => 'integer|min:0',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                'product_id' => 'required|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'published' => 'required|boolean',

            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }


            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('features')[$idioma->abbreviation]) ||
                    isset($request->get('recommendations')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation]) || isset($request->get('discover_url')[$idioma->abbreviation]) ||
                    isset($request->get('legal')[$idioma->abbreviation]) ||

                    isset($request->get('breadcrumb')[$idioma->abbreviation]) || isset($request->get('rel')[$idioma->abbreviation]) ||
                    isset($request->get('og_title')[$idioma->abbreviation]) || isset($request->get('og_description')[$idioma->abbreviation]) ||
                    isset($request->get('og_image')[$idioma->abbreviation]) || isset($request->get('twitter_title')[$idioma->abbreviation]) ||
                    isset($request->get('twitter_description')[$idioma->abbreviation]) || isset($request->get('twitter_image')[$idioma->abbreviation]) ||
                    isset($request->get('script_head')[$idioma->abbreviation]) || isset($request->get('canonical_url')[$idioma->abbreviation]) ||
                    isset($request->get('script_body')[$idioma->abbreviation]) || isset($request->get('script_footer')[$idioma->abbreviation]) ||
                    isset($request->get('index')[$idioma->abbreviation])) {
                    $validator = Validator::make($request->all(), [
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'name.' . $idioma->abbreviation => 'required',

                        'index.' . $idioma->abbreviation => 'boolean',
                        'og_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                        'twitter_image.' . $idioma->abbreviation => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());

                    }
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $view_producto = ViewProduct::where('product_id', $request->get('product_id'))
                    ->where('subchannel_id', $request->get('subchannel_id'))->first();
                //si hay un registro con ese product_id y ese subchannel actualiza y si no lo crea
                if ($view_producto) {
                    $view_producto->update([
                        'published' => $request->get('published'),
                        'order' => ($request->get('order') != '') ? $request->get('order') : 0,
                    ]);
                } else {
                    // Crea el producto
                    $view_producto = ViewProduct::create([

                        'product_id' => $request->get('product_id'),
                        'subchannel_id' => $request->get('subchannel_id'),
                        'published' => $request->get('published'),
                        'order' => ($request->get('order') != '') ? $request->get('order') : 0,

                    ]);
                    // FIN crear el producto
                }

                // Actualizar traducciones del producto
                $array_borrar = [];
                foreach ($array_traducciones as $idioma) {

                    // Preparación de datos
                    $friendly_url = null;

                    if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                        $friendly_url = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                    } else {
                        $friendly_url = Str::slug($request->get('name')[$idioma->abbreviation]);
                    }
                    //

                    $traduccion = $view_producto->viewProductTranslation()->where('language_id', $idioma->id)->first();

                    $array_borrar[] = $idioma->id;

                    if ($traduccion) {

                        $traduccion->update([
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                            'features' => (isset($request->get('features')[$idioma->abbreviation]) && $request->get('features')[$idioma->abbreviation] != '') ? $request->get('features')[$idioma->abbreviation] : null,
                            'recommendations' => (isset($request->get('recommendations')[$idioma->abbreviation]) && $request->get('recommendations')[$idioma->abbreviation] != '') ? $request->get('recommendations')[$idioma->abbreviation] : null,
                            'short_description' => $request->get('short_description')[$idioma->abbreviation],
                            'friendly_url' => $friendly_url,
                            'discover_url' => (isset($request->get('discover_url')[$idioma->abbreviation]) && $request->get('discover_url')[$idioma->abbreviation] != '') ? $request->get('discover_url')[$idioma->abbreviation] : null,
                            'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                            'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                            'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,

                            'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                            'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                            'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                            'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                            'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                            'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                            'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                            'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                            'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                            'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                            'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                            'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                            'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                        ]);
                    } else {

                        ViewProductTranslation::create([
                            'view_product_id' => $view_producto->id,
                            'language_id' => $idioma->id,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                            'features' => (isset($request->get('features')[$idioma->abbreviation]) && $request->get('features')[$idioma->abbreviation] != '') ? $request->get('features')[$idioma->abbreviation] : null,
                            'recommendations' => (isset($request->get('recommendations')[$idioma->abbreviation]) && $request->get('recommendations')[$idioma->abbreviation] != '') ? $request->get('recommendations')[$idioma->abbreviation] : null,
                            'short_description' => $request->get('short_description')[$idioma->abbreviation],
                            'friendly_url' => $friendly_url,
                            'discover_url' => (isset($request->get('discover_url')[$idioma->abbreviation]) && $request->get('discover_url')[$idioma->abbreviation] != '') ? $request->get('discover_url')[$idioma->abbreviation] : null,
                            'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                            'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                            'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,

                            'index' => (isset($request->get('index')[$idioma->abbreviation]) && $request->get('index')[$idioma->abbreviation] != '') ? $request->get('index')[$idioma->abbreviation] : 0,
                            'breadcrumb' => (isset($request->get('breadcrumb')[$idioma->abbreviation]) && $request->get('breadcrumb')[$idioma->abbreviation] != '') ? $request->get('breadcrumb')[$idioma->abbreviation] : null,
                            'rel' => (isset($request->get('rel')[$idioma->abbreviation]) && $request->get('rel')[$idioma->abbreviation] != '') ? $request->get('rel')[$idioma->abbreviation] : 'follow',
                            'og_title' => (isset($request->get('og_title')[$idioma->abbreviation]) && $request->get('og_title')[$idioma->abbreviation] != '') ? $request->get('og_title')[$idioma->abbreviation] : null,
                            'og_description' => (isset($request->get('og_description')[$idioma->abbreviation]) && $request->get('og_description')[$idioma->abbreviation] != '') ? $request->get('og_description')[$idioma->abbreviation] : null,
                            'og_image' => (isset($request->get('og_image')[$idioma->abbreviation]) && $request->get('og_image')[$idioma->abbreviation] != '') ? $request->get('og_image')[$idioma->abbreviation] : null,
                            'twitter_title' => (isset($request->get('twitter_title')[$idioma->abbreviation]) && $request->get('twitter_title')[$idioma->abbreviation] != '') ? $request->get('twitter_title')[$idioma->abbreviation] : null,
                            'twitter_description' => (isset($request->get('twitter_description')[$idioma->abbreviation]) && $request->get('twitter_description')[$idioma->abbreviation] != '') ? $request->get('twitter_description')[$idioma->abbreviation] : null,
                            'twitter_image' => (isset($request->get('twitter_image')[$idioma->abbreviation]) && $request->get('twitter_image')[$idioma->abbreviation] != '') ? $request->get('twitter_image')[$idioma->abbreviation] : null,
                            'canonical_url' => (isset($request->get('canonical_url')[$idioma->abbreviation]) && $request->get('canonical_url')[$idioma->abbreviation] != '') ? $request->get('canonical_url')[$idioma->abbreviation] : null,
                            'script_head' => (isset($request->get('script_head')[$idioma->abbreviation]) && $request->get('script_head')[$idioma->abbreviation] != '') ? $request->get('script_head')[$idioma->abbreviation] : null,
                            'script_body' => (isset($request->get('script_body')[$idioma->abbreviation]) && $request->get('script_body')[$idioma->abbreviation] != '') ? $request->get('script_body')[$idioma->abbreviation] : null,
                            'script_footer' => (isset($request->get('script_footer')[$idioma->abbreviation]) && $request->get('script_footer')[$idioma->abbreviation] != '') ? $request->get('script_footer')[$idioma->abbreviation] : null,
                        ]);
                    }
                }
                $view_producto->viewProductTranslation()->whereNotIn('language_id', $array_borrar)->delete();
                // FIN actualizar traducciones de producto


                // Crear relación con files

                $view_producto->viewProductFile()->delete();
                //Crear relación con archivo
                if ($request->get('files') != '') {
                    foreach ($request->get('files') as $file) {
                        ViewProductFile::create([
                            'view_product_id' => $view_producto->id,
                            'file_id' => $file,
                        ]);
                    }
                }
                //FIN crear relación con archivo


            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de productos');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para la eliminación de uno o más productos.
     *
     * Para la eliminación de productos se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
                'subchannel_id' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_product,id,deleted_at,NULL',
                    'subchannel_id' => 'exists:tenant.mo_subchannel,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $id) {
                        $viewProducto = ViewProduct::where('product_id', $id)->where('subchannel_id', $request->get('subchannel_id'))->first();
                        //por si mandan un par ya borrado
                        if ($viewProducto) {
                            $viewProducto->viewproductTranslation()->delete();
                            $viewProducto->delete();
                        }
                    }

                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de productos');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función que lista todos los productos para un subcanal determinado
     *
     * Para la consulta de productos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            $idiomas = Language::where('front', 1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'type_id' => 'integer|min:0',
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
                'category_id' => 'integer|min:0',
                'location_id' => 'integer|min:0',
                'subchannel_id' => 'required|integer|min:0',
                'published' => 'boolean',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $rule_codes = [];

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_view_product = $rules->where('code', 'view_product');

                            if($rules_view_product) {
                                foreach($rules_view_product as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'view_product') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'view_product';
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                } else {

                    $subcanal = $request->get('subchannel_id');
                    $fecha_actual = Carbon::now()->format('Y-m-d');

                    //Preparación de consultas
                    $productos = DB::connection('tenant')->table('mo_product')
                        ->select('mo_product_category.id', 'mo_product.*',
                            'mo_view_product.id as view_id', 'mo_view_product.product_id as view_product_id', 'mo_view_product.subchannel_id as view_subchannel_id',
                            'mo_view_product.published as view_published', 'mo_view_product.order as view_order',
                            'mo_subchannel.view_blocked')
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->join('mo_price_product', 'mo_price_product.product_id', '=', 'mo_product.id')
                        ->whereNull('mo_price_product.service_id')
                        ->whereNull('mo_price_product.deleted_at')
                        ->join('mo_price', 'mo_price.id', '=', 'mo_price_product.price_id')
                        //Comprueba la existencia de una tarifa en la fecha actual
                        ->where('mo_price.date_start', '<=', $fecha_actual)
                        ->where('mo_price.date_end', '>=', $fecha_actual)
                        ->whereNull('mo_price.deleted_at')
                        ->join('mo_contract_model', 'mo_contract_model.id', '=', 'mo_price.contract_model_id')
                        ->whereNull('mo_contract_model.deleted_at')
                        ->join('mo_subchannel', 'mo_subchannel.contract_model_id', '=', 'mo_contract_model.id')
                        ->where('mo_subchannel.id', '=', $subcanal)
                        ->whereNull('mo_subchannel.deleted_at')
                        ->join('mo_product_location', 'mo_product.id', '=', 'mo_product_location.product_id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->join('mo_location', 'mo_product_location.location_id', '=', 'mo_location.id')
                        ->whereNull('mo_location.deleted_at')
                        ->leftJoin('mo_product_category', function ($join) {
                            $join->on('mo_product.id', '=', 'mo_product_category.product_id')->whereNull('mo_product_category.deleted_at');
                        })
                        ->leftjoin('mo_category', function ($join) {
                            $join->on('mo_product_category.category_id', '=', 'mo_category.id')->whereNull('mo_category.deleted_at');
                        })
                        ->leftJoin('mo_view_product', function ($join) use ($subcanal) {
                            $join->on('mo_view_product.product_id', '=', 'mo_product.id')->where('mo_view_product.subchannel_id', '=', $subcanal)->whereNull('mo_view_product.deleted_at');
                        })
                        ->leftJoin('mo_view_product_translation', function ($join) {
                            $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')->whereNull('mo_view_product_translation.deleted_at');
                        });

                    $sub = DB::connection('tenant')->table('mo_product')
                        ->select('mo_product.id')
                        ->whereNull('mo_product.deleted_at')
                        ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->join('mo_price_product', 'mo_price_product.product_id', '=', 'mo_product.id')
                        ->whereNull('mo_price_product.service_id')
                        ->whereNull('mo_price_product.deleted_at')
                        ->join('mo_price', 'mo_price.id', '=', 'mo_price_product.price_id')
                        //Comprueba la existencia de una tarifa en la fecha actual
                        ->where('mo_price.date_start', '<=', $fecha_actual)
                        ->where('mo_price.date_end', '>=', $fecha_actual)
                        ->whereNull('mo_price.deleted_at')
                        ->join('mo_contract_model', 'mo_contract_model.id', '=', 'mo_price.contract_model_id')
                        ->whereNull('mo_contract_model.deleted_at')
                        ->join('mo_subchannel', 'mo_subchannel.contract_model_id', '=', 'mo_contract_model.id')
                        ->where('mo_subchannel.id', '=', $subcanal)
                        ->whereNull('mo_subchannel.deleted_at')
                        ->join('mo_product_location', 'mo_product.id', '=', 'mo_product_location.product_id')
                        ->whereNull('mo_product_location.deleted_at')
                        ->join('mo_location', 'mo_product_location.location_id', '=', 'mo_location.id')
                        ->whereNull('mo_location.deleted_at')
                        ->leftJoin('mo_product_category', function ($join) {
                            $join->on('mo_product.id', '=', 'mo_product_category.product_id')->whereNull('mo_product_category.deleted_at');
                        })
                        ->leftjoin('mo_category', function ($join) {
                            $join->on('mo_product_category.category_id', '=', 'mo_category.id')->whereNull('mo_category.deleted_at');
                        })
                        ->leftJoin('mo_view_product', function ($join) use ($subcanal) {
                            $join->on('mo_view_product.product_id', '=', 'mo_product.id')->where('mo_view_product.subchannel_id', '=', $subcanal)->whereNull('mo_view_product.deleted_at');
                        })
                        ->leftJoin('mo_view_product_translation', function ($join) {
                            $join->on('mo_view_product.id', '=', 'mo_view_product_translation.view_product_id')->whereNull('mo_view_product_translation.deleted_at');
                        });

                    // Recogida de datos enviados por el usuario y establecimiento de filtros

                    if ($request->get('lang') != '') {
                        $productos->Where(function ($query) use ($idioma) {
                            $query->where('mo_product_translation.language_id', '=', $idioma->id)
                                ->orWhere('mo_view_product_translation.language_id', '=', $idioma->id);
                        });
                        $sub->Where(function ($query) use ($idioma) {
                            $query->where('mo_product_translation.language_id', '=', $idioma->id)
                                ->orWhere('mo_view_product_translation.language_id', '=', $idioma->id);
                        });
                    }

                    if ($request->get('type_id') != '') {
                        $productos->where('type_id', '=', $request->get('type_id'));
                        $sub->where('type_id', '=', $request->get('type_id'));
                    }

                    if ($request->get('code') != '') {
                        $productos->where('code', 'Like', '%' . $request->get('code') . '%');
                        $sub->where('code', 'Like', '%' . $request->get('code') . '%');
                    }

                    if ($request->get('name') != '') {

                        $productos->Where(function ($query) use ($request) {
                            $query->where('mo_product_translation.name', 'Like', '%' . $request->get('name') . '%')
                                ->orWhere('mo_view_product_translation.name', 'Like', '%' . $request->get('name') . '%');
                        });

                        $sub->Where(function ($query) use ($request) {
                            $query->where('mo_product_translation.name', 'Like', '%' . $request->get('name') . '%')
                                ->orWhere('mo_view_product_translation.name', 'Like', '%' . $request->get('name') . '%');
                        });
                    }

                    if ($request->get('category_id') != '') {
                        $productos->where('mo_product_category.category_id', '=', $request->get('category_id'))->where('mo_category.id', $request->get('category_id'));
                        $sub->where('mo_product_category.category_id', '=', $request->get('category_id'))->where('mo_category.id', $request->get('category_id'));
                    }

                    if ($request->get('location_id') != '') {
                        $productos->where('mo_product_location.location_id', '=', $request->get('location_id'));
                        $sub->where('mo_product_location.location_id', '=', $request->get('location_id'));
                    }

                    if ($request->get('published') != '') {
                        if ($request->get('published') == '1') {
                            $productos->Where(function ($query) {
                                $query->where('mo_view_product.published', '=', '1');

                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_product.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '0');
                                });
                            });
                            $sub->Where(function ($query) {
                                $query->where('mo_view_product.published', '=', '1');
                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_product.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '0');
                                });
                            });
                        } elseif ($request->get('published') == '0') {
                            $productos->Where(function ($query) {
                                $query->where('mo_view_product.published', '=', '0');
                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_product.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '1');
                                });
                            });
                            $sub->Where(function ($query) {
                                $query->where('mo_view_product.published', '=', '0');
                                $query->orWhere(function ($query) {
                                    $query->WhereNull('mo_view_product.published')
                                        ->Where('mo_subchannel.view_blocked', '=', '1');
                                });
                            });
                        }
                    }

                    // Order
                    $orden = 'mo_product_translation.name';
                    $request_order = $request->get('order');
                    if ($request_order != '') {
                        switch ($request->get('order')) {
                            case 'name':
                                $orden = 'mo_product_translation.name';
                                break;
                            case 'description':
                                $orden = 'mo_product_translation.description';
                                break;
                            case 'short_description':
                                $orden = 'mo_product_translation.short_description';
                                break;
                            case 'type_id':
                                $orden = 'mo_product.type_id';
                                break;
                            case 'id':
                                $orden = 'mo_product.id';
                                break;
                            case 'order':
                                $orden = 'mo_product.order';
                                break;
                            default:
                                $orden = 'mo_product_translation.name';
                                break;
                        }
                    }
                    // FIN Order

                    // Order_way
                    $sentido = 'asc';
                    $request_order_way = $request->get('order_way');
                    if ($request_order_way != '') {
                        switch ($request->get('order_way')) {
                            case 'asc':
                                $sentido = 'asc';
                                break;
                            case 'desc':
                                $sentido = 'desc';
                                break;
                            default:
                                $sentido = 'asc';
                                break;
                        }
                    }
                    // FIN Order_way

                    $productos = $productos->groupBy('mo_product.id')->orderBy($orden, $sentido);

                    // Paginación según filtros y ejecución de la consulta
                    if ($request->get('limit') != '0') {

                        $settings = Settings::where('name', '=', 'limit_registers')->first();
                        $limite = $settings->value;

                        if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                            $limite = $request->get('limit');
                        }

                        $inicio = 0;
                        if ($request->get('page') != '') {
                            $inicio = $request->get('page');
                        }

                        $productos = $productos->forPage($inicio, $limite)->get();

                        //si filtro limit = 0 se obtienen todos los resultados
                    } else {

                        $productos = $productos->get();

                    }

                    //Fin de paginación


                    $sub->groupBy('mo_product.id');
                    $productos_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                        ->mergeBindings($sub);

                    $totales = $productos_count->count();

                    // Preparada locaciones

                    $sql_location = DB::connection('tenant')->table('mo_location')
                        ->select('mo_location.id', 'mo_location.name')
                        ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                        ->whereNull('mo_location.deleted_at')
                        ->whereNull('mo_product_location.deleted_at');

                    $array['data'] = array();

                    foreach ($productos as $producto) {

                        $sql_location_clone = clone $sql_location;

                        $datos_location = $sql_location_clone->where('mo_product_location.product_id', $producto->id)->get();

                        $producto->location = $datos_location;


                        $product_aux = array();

                        $product_aux['lang'] = array();

                        //Producto (por canal)
                        if ($producto->view_id != null) {
                            $product_aux = [
                                'id' => $producto->id,
                                // Campos especiales
                                'subchannel_id' => $producto->view_subchannel_id,
                                'published' => $producto->view_published,
                                'order' => $producto->view_order,
                                // FIN Campos especiales
                                'code' => $producto->code,
                                'type_id' => $producto->type_id,
                                'company_id' => $producto->company_id,
                                'uom_id' => $producto->uom_id,
                                'billable' => $producto->billable,
                                'height_from' => $producto->height_from,
                                'height_to' => $producto->height_to,
                                'weight_from' => $producto->weight_from,
                                'weight_to' => $producto->weight_to,
                                'longitude' => $producto->longitude,
                                'latitude' => $producto->latitude,
                                'address' => $producto->address,
                                'pax' => $producto->pax,
                                'cost_center' => $producto->cost_center,
                                'accounting_account' => $producto->accounting_account,
                            ];
                            foreach ($idiomas as $idi) {
                                $traduccion = ViewProductTranslation::where('view_product_id', '=', $producto->view_id)
                                    ->select('id', 'language_id', 'name', 'description', 'features', 'recommendations', 'short_description', 'friendly_url', 'discover_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                        'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                    ->where('language_id', '=', $idi->id)
                                    ->get();
                                foreach ($traduccion as $trad) {
                                    $product_aux['lang'][][$idi->abbreviation] = $trad;
                                }
                            }

                            //Producto (general)
                        } else {
                            $product_aux = [
                                'id' => $producto->id,
                                // Campos especiales
                                'subchannel_id' => $subcanal,
                                'published' => ($producto->view_blocked == 1) ? 0 : 1,
                                'order' => $producto->order,
                                // FIN Campos especiales
                                'code' => $producto->code,
                                'type_id' => $producto->type_id,
                                'company_id' => $producto->company_id,
                                'uom_id' => $producto->uom_id,
                                'billable' => $producto->billable,
                                'height_from' => $producto->height_from,
                                'height_to' => $producto->height_to,
                                'weight_from' => $producto->weight_from,
                                'weight_to' => $producto->weight_to,
                                'longitude' => $producto->longitude,
                                'latitude' => $producto->latitude,
                                'address' => $producto->address,
                                'pax' => $producto->pax,
                                'cost_center' => $producto->cost_center,
                                'accounting_account' => $producto->accounting_account,
                            ];
                            foreach ($idiomas as $idi) {
                                $traduccion = ProductTranslation::where('product_id', '=', $producto->id)
                                    ->select('id', 'language_id', 'name', 'description', 'features', 'recommendations', 'short_description', 'friendly_url', 'discover_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                        'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                    ->where('language_id', '=', $idi->id)
                                    ->get();
                                foreach ($traduccion as $trad) {
                                    $product_aux['lang'][][$idi->abbreviation] = $trad;
                                }
                            }
                        }

                        //Generación de array de salida
                        $array['data'][0]['viewproduct'][] = $product_aux;
                    }
                    $array['total_results'] = $totales;
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de productos');

        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función que muestra un producto buscado por su id para un subcanal determinado
     *
     * Para la consulta de un producto se realizará una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function showId($id, Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            $idiomas = Language::where('front', 1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'required|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'global' => 'required|boolean',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                $subcanal = $request->get('subchannel_id');
                $fecha_actual = Carbon::now()->format('Y-m-d');

                //Preparación de las consultas
                $productos = DB::connection('tenant')->table('mo_product')
                    ->select('mo_product_category.id', 'mo_product.*',
                        'mo_view_product.id as view_id', 'mo_view_product.product_id as view_product_id', 'mo_view_product.subchannel_id as view_subchannel_id',
                        'mo_view_product.published as view_published', 'mo_view_product.order as view_order')
                    ->where('mo_product.id', '=', $id)
                    ->whereNull('mo_product.deleted_at')
                    ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                    ->whereNull('mo_product_translation.deleted_at')
                    ->join('mo_price_product', 'mo_price_product.product_id', '=', 'mo_product.id')
                    ->whereNull('mo_price_product.service_id')
                    ->whereNull('mo_price_product.deleted_at')
                    ->join('mo_price', 'mo_price.id', '=', 'mo_price_product.price_id')
                    ->whereNull('mo_price.deleted_at')
                    //Comprueba la existencia de una tarifa en la fecha actual
                    ->where('mo_price.date_start', '<=', $fecha_actual)
                    ->where('mo_price.date_end', '>=', $fecha_actual)
                    ->join('mo_contract_model', 'mo_contract_model.id', '=', 'mo_price.contract_model_id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_subchannel', 'mo_subchannel.contract_model_id', '=', 'mo_price.contract_model_id')
                    ->where('mo_subchannel.id', '=', $subcanal)
                    ->whereNull('mo_subchannel.deleted_at')
                    ->join('mo_product_location', 'mo_product.id', '=', 'mo_product_location.product_id')
                    ->whereNull('mo_product_location.deleted_at')
                    ->join('mo_location', 'mo_product_location.location_id', '=', 'mo_location.id')
                    ->whereNull('mo_location.deleted_at')
                    ->leftJoin('mo_product_category', function ($join) {
                        $join->on('mo_product.id', '=', 'mo_product_category.product_id')->whereNull('mo_product_category.deleted_at');
                    })
                    ->leftjoin('mo_category', function ($join) {
                        $join->on('mo_product_category.category_id', '=', 'mo_category.id')->whereNull('mo_category.deleted_at');
                    })
                    ->leftJoin('mo_view_product', function ($join) use ($subcanal) {
                        $join->on('mo_view_product.product_id', '=', 'mo_product.id')->where('mo_view_product.subchannel_id', '=', $subcanal)->whereNull('mo_view_product.deleted_at');
                    })
                    ->groupBy('mo_product.id')->get();

                //ruta de storage de settings para concatenar a ruta de archivo de producto
                $storage = Settings::where('name', 'storage_files')->first()->value;

                $files_view = DB::connection('tenant')->table('mo_view_product_file')
                    ->select('mo_file.id', 'mo_file.file_name', 'mo_file.file_size', 'mo_file.file_dimensions', 'mo_file.mimetype', 'mo_file.type_id', 'mo_file.order', 'mo_file.url_file','mo_file.device_id','mo_file.key_file')
                    ->where('mo_view_product_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_view_product_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->where('mo_file_type.deleted_at', null)
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    });

                $files_no_view = DB::connection('tenant')->table('mo_product_file')
                    ->select('mo_file.id', 'mo_file.file_name', 'mo_file.file_size', 'mo_file.file_dimensions', 'mo_file.mimetype', 'mo_file.type_id', 'mo_file.order', 'mo_file.url_file','mo_file.device_id','mo_file.key_file')
                    ->where('mo_product_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_product_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->leftJoin('mo_file_type', function ($join) {
                        $join->on('mo_file.type_id', '=', 'mo_file_type.id')
                            ->where('mo_file_type.deleted_at', null)
                            ->join('mo_file_type_translation', 'mo_file_type_translation.type_id', '=', 'mo_file_type.id');
                    });


                $array['data'] = array();
                $product_aux = array();

                foreach ($productos as $producto) {

                    $product_aux['lang'] = array();

                    //Producto (por canal)
                    if ($producto->view_id != null && $request->get('global') != 1) {
                        $product_aux = [
                            'id' => $producto->view_product_id,
                            // Campos especiales
                            'subchannel_id' => $producto->view_subchannel_id,
                            'published' => $producto->view_published,
                            'order' => $producto->view_order,
                            // FIN Campos especiales
                            'code' => $producto->code,
                            'type_id' => $producto->type_id,
                            'company_id' => $producto->company_id,
                            'uom_id' => $producto->uom_id,
                            'billable' => $producto->billable,
                            'height_from' => $producto->height_from,
                            'height_to' => $producto->height_to,
                            'weight_from' => $producto->weight_from,
                            'weight_to' => $producto->weight_to,
                            'longitude' => $producto->longitude,
                            'latitude' => $producto->latitude,
                            'address' => $producto->address,
                            'pax' => $producto->pax,
                            'cost_center' => $producto->cost_center,
                            'accounting_account' => $producto->accounting_account,
                            'salable' => $producto->salable,
                            'iva_id' => $producto->iva_id,
                        ];
                        foreach ($idiomas as $idi) {
                            $traduccion = ViewProductTranslation::where('view_product_id', '=', $producto->view_id)
                                ->select('id', 'language_id', 'name', 'description', 'features', 'recommendations', 'short_description', 'friendly_url', 'discover_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                    'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                if ($trad->og_image && $trad->og_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->og_image)->first()->url_file;
                                    $trad->og_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->og_image_route = null;
                                }
                                if ($trad->twitter_image && $trad->twitter_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->twitter_image)->first()->url_file;
                                    $trad->twitter_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->twitter_image_route = null;
                                }
                                $product_aux['lang'][][$idi->abbreviation] = $trad;
                            }
                        }

                        $files_clone = clone $files_view;
                        $files = $files_clone->where('mo_view_product_file.view_product_id', '=', $producto->view_id)
                            ->groupBy('mo_file.id')->get();

                        //Producto (general)
                    } else {
                        //Campo de subcanal que indica el valor por defecto de 'published'
                        $datos_subcanal = Subchannel::where('id', '=', $subcanal)
                            ->select('view_blocked')->first();

                        $product_aux = [
                            'id' => $producto->id,
                            // Campos especiales
                            'subchannel_id' => $subcanal,
                            'published' => ($datos_subcanal->view_blocked == 1) ? 0 : 1,
                            'order' => $producto->order,
                            // FIN Campos especiales
                            'code' => $producto->code,
                            'type_id' => $producto->type_id,
                            'company_id' => $producto->company_id,
                            'uom_id' => $producto->uom_id,
                            'billable' => $producto->billable,
                            'height_from' => $producto->height_from,
                            'height_to' => $producto->height_to,
                            'weight_from' => $producto->weight_from,
                            'weight_to' => $producto->weight_to,
                            'longitude' => $producto->longitude,
                            'latitude' => $producto->latitude,
                            'address' => $producto->address,
                            'pax' => $producto->pax,
                            'cost_center' => $producto->cost_center,
                            'accounting_account' => $producto->accounting_account,
                            'salable' => $producto->salable,
                            'iva_id' => $producto->iva_id,
                        ];
                        foreach ($idiomas as $idi) {
                            $traduccion = ProductTranslation::where('product_id', '=', $producto->id)
                                ->select('id', 'language_id', 'name', 'description', 'features', 'recommendations', 'short_description', 'friendly_url', 'discover_url', 'title_seo', 'description_seo', 'keywords_seo', 'legal',
                                    'index','breadcrumb','rel','og_title','og_description','og_image','twitter_title','twitter_description','twitter_image','canonical_url','script_head','script_body','script_footer')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                if ($trad->og_image && $trad->og_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->og_image)->first()->url_file;
                                    $trad->og_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->og_image_route = null;
                                }
                                if ($trad->twitter_image && $trad->twitter_image != ''){
                                    $sql_file_seo = DB::connection('tenant')->table('mo_file')->where('id',$trad->twitter_image)->first()->url_file;
                                    $trad->twitter_image_route = $storage . $sql_file_seo;
                                }else{
                                    $trad->twitter_image_route = null;
                                }
                                $product_aux['lang'][][$idi->abbreviation] = $trad;
                            }

                        }

                        $files_clone = clone $files_no_view;
                        $files = $files_clone->where('mo_product_file.product_id', '=', $producto->id)
                            ->groupBy('mo_file.id')->get();

                    }

                    //archivos asociados al producto
                    $product_aux['files'] = array();
                    $array_file = array();
                    foreach ($files as $file) {

                        $array_file = ['id' => $file->id, 'file_name' => $file->file_name, 'file_size' => $file->file_size,
                            'file_dimensions' => $file->file_dimensions, 'mimetype' => $file->mimetype, 'order' => $file->order, 'url_file' => $storage . $file->url_file,
                            'device_id' => $file->device_id,'key_file' => $file->key_file,'type' => ($file->type_id != null) ? ['id' => $file->type_id] : null];
                        foreach ($idiomas as $idi) {

                            $traducciones_files = FileTranslation::where('file_id', $file->id)
                                ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_translation.file_id')
                                ->get();

                            foreach ($traducciones_files as $traduccion_file) {
                                $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                            }

                            if ($file->type_id != null) {
                                $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                    ->select('id', 'language_id', 'name')
                                    ->where('language_id', '=', $idi->id)
                                    ->groupBy('mo_file_type_translation.type_id')
                                    ->get();

                                foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                    $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                                }
                            }

                        }
                        $product_aux['files'][] = $array_file;
                    }

                    //locations asociadas al producto
                    $locations = DB::connection('tenant')->table('mo_location')
                        ->select('mo_location.id', 'mo_location.name')
                        ->whereNull('mo_location.deleted_at')
                        ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                        ->where('mo_product_location.product_id', $producto->id)
                        ->whereNull('mo_product_location.deleted_at')
                        ->get();

                    foreach ($locations as $location) {
                        $product_aux['locations'][] = $location;
                    }

                    //Paquetes
                    $paquete = array();
                    if ($product_aux['type_id'] == 1) {
                        $package = ProductPackage::where('package_id', '=', $producto->id)->get();
                        foreach ($package as $pack) {
                            $paquete[] = ['product_id' => $pack->product_id, 'location_id' => $pack->location_id];
                        }
                    }
                    $product_aux['package'] = $paquete;

                    //servicios asociados al producto
                    $services = DB::connection('tenant')->table('mo_service')
                        ->select('mo_service.id')
                        ->whereNull('mo_service.deleted_at')
                        ->join('mo_product_service', 'mo_product_service.service_id', 'mo_service.id')
                        ->where('mo_product_service.product_id', $producto->id)
                        ->whereNull('mo_product_service.deleted_at')
                        ->get();
                    $product_aux['services'] = array();
                    foreach ($services as $service) {
                        $product_aux['services'][] = $service;
                    }

                    //categorías asociadas al producto
                    $categories = DB::connection('tenant')->table('mo_category')
                        ->select('mo_category.id')
                        ->whereNull('mo_category.deleted_at')
                        ->join('mo_product_category', 'mo_product_category.category_id', 'mo_category.id')
                        ->where('mo_product_category.product_id', $producto->id)
                        ->whereNull('mo_product_category.deleted_at')
                        ->get();
                    $product_aux['categories'] = array();
                    foreach ($categories as $cat) {
                        $product_aux['categories'][] = $cat;
                    }

                    //etiquetas asociadas al producto
                    $tags = DB::connection('tenant')->table('mo_tag')
                        ->select('mo_tag.id')
                        ->whereNull('mo_tag.deleted_at')
                        ->join('mo_product_tag', 'mo_product_tag.tag_id', 'mo_tag.id')
                        ->where('mo_product_tag.product_id', $producto->id)
                        ->whereNull('mo_product_tag.deleted_at')
                        ->get();
                    $product_aux['tags'] = array();
                    foreach ($tags as $tag) {
                        $product_aux['tags'][] = $tag;
                    }

                    //Generación de array de salida
                    $array['data'][0]['viewproduct'][] = $product_aux;
                }

                DB::connection('tenant')->commit();
            }
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Visualización de productos');
        }

        return response()->json($array, $array['error']);
    }

}