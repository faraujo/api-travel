<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\ClientTypeTranslation;
use App\Models\tenant\Company;
use App\Models\tenant\ContractModel;
use App\Models\tenant\ContractTranslation;
use App\Models\tenant\Currency;
use App\Exceptions\Handler;
use App\Models\tenant\Language;
use App\Models\tenant\Price;
use App\Models\tenant\PriceProduct;
use App\Models\tenant\PriceProductIntercompany;
use App\Models\tenant\ProductTranslation;
use App\Models\tenant\ProductTypeTranslation;
use App\Models\tenant\ServiceTranslation;
use App\Models\tenant\Settings;
use App\Models\tenant\TypeTranslation;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use function Sodium\add;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PriceController extends Controller
{

    /**
     * Función para la eliminación de una tarifa.
     *
     * Para la eliminación de una tarifa de un producto o producto y servicio se realiza una petición DELETE. Si la operación no produce error se devuelve la variable “error” con valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_price,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $tarifa_id) {
                        $tarifa = Price::where('id', $tarifa_id)->first();
                        $tarifa->priceProduct()->delete();
                        $tarifa->delete();
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tarifas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la creación o modificación de una tarifa
     *
     * Para la creación o actualizacion de una tarifa se realiza una petición POST.
     * Los parámetros requeridos son el date_start, date_end, contract_model_id, user_id, el array de services o products, al menos uno de ellos.
     * En el array es obligatorio el id del producto o servicio y tipo de cliente, net_price, uno de estos dos sale_price o markup y el array de locaciones donde se deberá especificar al menos una locación.
     * Ademas para la creacion o actualizacion de servicios sera necesario indicar el product_id asociado y en caso de que el servicio sea transportacion será requerido pickup.
     * En el caso de actualizar es requerido también el id y se comprueba que no exista ninguna tarifa solapada con las fechas indicadas.
     * Si la operación no produce error se devuelve la variable “error” con valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();
            //Validación
            $error = 0;
            $mensaje_validador = collect();


            $validator = Validator::make($request->all(), [
                'currency_id' => 'required|numeric|exists:tenant.mo_currency,id,deleted_at,NULL',
                'date_start' => 'date|required|date_format:"Y-m-d"',
                'date_end' => 'date|required|date_format:"Y-m-d"|after_or_equal:date_start',
                'contract_model_id' => 'required|numeric|exists:tenant.mo_contract_model,id,deleted_at,NULL',
                'promotions' => 'boolean',
                'user_id' => 'required|exists:tenant.mo_user,id,deleted_at,NULL',
                'products' => 'array',
                'products.*.id' => 'integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL|required',
                'products.*.locations' => 'array|required',
                'products.*.locations.*.id' => 'integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL|required',

                'products.*.locations.*.client_types' => 'array|required',
                'products.*.locations.*.client_types.*.id' => 'integer|min:0|exists:tenant.mo_client_type,id,deleted_at,NULL|required',
                'products.*.locations.*.client_types.*.net_price' => 'numeric|min:0|required_without_all:products.*.locations.*.client_types.*.sessions,products.*.locations.*.client_types.*.services',
                'products.*.locations.*.client_types.*.sale_price' => 'numeric|min:0|required_without_all:products.*.locations.*.client_types.*.markup,products.*.locations.*.client_types.*.sessions,products.*.locations.*.client_types.*.services',
                'products.*.locations.*.client_types.*.markup' => 'numeric|min:0|required_without_all:products.*.locations.*.client_types.*.sale_price,products.*.locations.*.client_types.*.sessions,products.*.locations.*.client_types.*.services',
                'products.*.locations.*.client_types.*.price_before' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.tip_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.tip_percent' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.commission_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.commission_percent' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.intercompany' => 'array',
                'products.*.locations.*.client_types.*.intercompany.*.id' => 'required|integer|min:0|exists:tenant.mo_company,id,deleted_at,NULL',
                'products.*.locations.*.client_types.*.intercompany.*.amount' => 'numeric',
                'products.*.locations.*.client_types.*.intercompany.*.percent' => 'numeric',
                'products.*.locations.*.client_types.*.intercompany.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',

                'products.*.locations.*.client_types.*.sessions' => 'array|required_without_all:products.*.locations.*.client_types.*.sale_price,products.*.locations.*.client_types.*.markup,products.*.locations.*.client_types.*.net_price,products.*.locations.*.client_types.*.services',
                'products.*.locations.*.client_types.*.sessions.*.session' => 'date_format:H:i|required',
                'products.*.locations.*.client_types.*.sessions.*.net_price' => 'numeric|min:0|required',
                'products.*.locations.*.client_types.*.sessions.*.sale_price' => 'numeric|min:0|required_without:products.*.locations.*.client_types.*.sessions.*.markup',
                'products.*.locations.*.client_types.*.sessions.*.markup' => 'numeric|min:0|required_without:products.*.locations.*.client_types.*.sessions.*.sale_price',
                'products.*.locations.*.client_types.*.sessions.*.price_before' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.sessions.*.intercompany' => 'array',
                'products.*.locations.*.client_types.*.sessions.*.intercompany.*.id' => 'required|integer|min:0|exists:tenant.mo_company,id,deleted_at,NULL',
                'products.*.locations.*.client_types.*.sessions.*.intercompany.*.amount' => 'numeric',
                'products.*.locations.*.client_types.*.sessions.*.intercompany.*.percent' => 'numeric',
                'products.*.locations.*.client_types.*.sessions.*.intercompany.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',

                'products.*.locations.*.client_types.*.services' => 'array|required_without_all:products.*.locations.*.client_types.*.sessions,products.*.locations.*.client_types.*.net_price',
                'products.*.locations.*.client_types.*.services.*.id' => 'integer|min:0|exists:tenant.mo_service,id,deleted_at,NULL|required',
                'products.*.locations.*.client_types.*.services.*.sale_price' => 'numeric|min:0|required_without_all:products.*.locations.*.client_types.*.services.*.pickups,products.*.locations.*.client_types.*.services.*.markup,products.*.locations.*.client_types.*.services.*.sessions',
                'products.*.locations.*.client_types.*.services.*.net_price' => 'numeric|min:0|required_without_all:products.*.locations.*.client_types.*.services.*.pickups,products.*.locations.*.client_types.*.services.*.sessions',
                'products.*.locations.*.client_types.*.services.*.markup' => 'numeric|min:0|required_without_all:products.*.locations.*.client_types.*.services.*.pickups,products.*.locations.*.client_types.*.services.*.sale_price,products.*.locations.*.client_types.*.services.*.sessions',
                'products.*.locations.*.client_types.*.services.*.price_before' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.services.*.tip_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.tip_percent' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.commission_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.commission_percent' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.services.*.intercompany' => 'array',
                'products.*.locations.*.client_types.*.services.*.intercompany.*.id' => 'required|integer|min:0|exists:tenant.mo_company,id,deleted_at,NULL',
                'products.*.locations.*.client_types.*.services.*.intercompany.*.amount' => 'numeric',
                'products.*.locations.*.client_types.*.services.*.intercompany.*.percent' => 'numeric',
                'products.*.locations.*.client_types.*.services.*.intercompany.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',

                'products.*.locations.*.client_types.*.services.*.pickups' => 'required_if:products.*.locations.*.client_types.*.services.*.id,1|array',
                'products.*.locations.*.client_types.*.services.*.pickups.*.id' => 'required|integer|min:0|exists:tenant.mo_transportation_location,id,deleted_at,NULL',
                'products.*.locations.*.client_types.*.services.*.pickups.*.sale_price' => 'numeric|min:0|required_without:products.*.locations.*.client_types.*.services.*.pickups.*.markup',
                'products.*.locations.*.client_types.*.services.*.pickups.*.net_price' => 'numeric|min:0|required',
                'products.*.locations.*.client_types.*.services.*.pickups.*.markup' => 'numeric|min:0|required_without:products.*.locations.*.client_types.*.services.*.pickups.*.sale_price',
                'products.*.locations.*.client_types.*.services.*.pickups.*.price_before' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.services.*.pickups.*.tip_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.pickups.*.tip_percent' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.pickups.*.commission_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.pickups.*.commission_percent' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.services.*.pickups.*.intercompany' => 'array',
                'products.*.locations.*.client_types.*.services.*.pickups.*.intercompany.*.id' => 'required|integer|min:0|exists:tenant.mo_company,id,deleted_at,NULL',
                'products.*.locations.*.client_types.*.services.*.pickups.*.intercompany.*.amount' => 'numeric',
                'products.*.locations.*.client_types.*.services.*.pickups.*.intercompany.*.percent' => 'numeric',
                'products.*.locations.*.client_types.*.services.*.pickups.*.intercompany.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',

                'products.*.locations.*.client_types.*.services.*.sessions' => 'array',
                'products.*.locations.*.client_types.*.services.*.sessions.*.session' => 'date_format:H:i|required',
                'products.*.locations.*.client_types.*.services.*.sessions.*.sale_price' => 'numeric|min:0|required_without:products.*.locations.*.client_types.*.services.*.sessions.*.markup',
                'products.*.locations.*.client_types.*.services.*.sessions.*.net_price' => 'numeric|min:0|required',
                'products.*.locations.*.client_types.*.services.*.sessions.*.markup' => 'numeric|min:0|required_without:products.*.locations.*.client_types.*.services.*.sessions.*.sale_price',
                'products.*.locations.*.client_types.*.services.*.sessions.*.price_before' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.services.*.sessions.*.tip_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.sessions.*.tip_percent' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.sessions.*.commission_amount' => 'numeric|min:0',
                'products.*.locations.*.client_types.*.services.*.sessions.*.commission_percent' => 'numeric|min:0',

                'products.*.locations.*.client_types.*.services.*.sessions.*.intercompany' => 'array',
                'products.*.locations.*.client_types.*.services.*.sessions.*.intercompany.*.id' => 'required|integer|min:0|exists:tenant.mo_company,id,deleted_at,NULL',
                'products.*.locations.*.client_types.*.services.*.sessions.*.intercompany.*.amount' => 'numeric',
                'products.*.locations.*.client_types.*.services.*.sessions.*.intercompany.*.percent' => 'numeric',
                'products.*.locations.*.client_types.*.services.*.sessions.*.intercompany.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->isMethod('put')) {
                $validator = Validator::make($request->all(), [
                    'id' => 'integer|min:0|required',
                    'products' => 'required',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }
            }

            if ($request->get('products') != '') {
                $array_productos = array();
                foreach ($request->get('products') as $key => $product) {

                    if (isset($product['id'])) {

                        //Comprueba si se ha introduccido un producto repetido
                        if (in_array($product['id'], $array_productos)) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['products.' . $key =>
                                ['The products.' . $key . '.id field has a duplicate value.']]);
                        }
                        $array_productos[] = $product['id'];
                    }
                    $array_locations = array();
                    if (isset($product['locations'])) {
                        $localizacion = 0;
                        foreach ($product['locations'] as $location) {

                            //Comprueba si se ha introduccido una locacion repetida para el producto
                            if (isset($location['id'])) {
                                if (in_array($location['id'], $array_locations)) {
                                    $error = 1;
                                    $mensaje_validador = $mensaje_validador->merge(['products.' . $key . '.locations.' . $localizacion . '.id' =>
                                        ['The products.' . $key . '.locations.' . $localizacion . '.id field has a duplicate value.']]);
                                }
                                $array_locations[] = $location['id'];
                            }

                            if (isset($location['client_types']) && !isset($location['net_price']) && !isset($location['session'])) {
                                $tipo_cliente = 0;
                                $array_client_types = array();
                                foreach ($location['client_types'] as $client_type) {

                                    //Comprueba si se ha introducido un tipo de cliente repetido para la locacion
                                    if (isset($client_type['id'])) {
                                        if (in_array($client_type['id'], $array_client_types)) {
                                            $error = 1;
                                            $mensaje_validador = $mensaje_validador->merge(['products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.id' =>
                                                ['The products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.id field has a duplicate value.']]);
                                        }
                                        $array_client_types[] = $client_type['id'];
                                    }

                                    if (isset($client_type['services'])) {
                                        $servicio = 0;
                                        $array_servicios = array();
                                        foreach ($client_type['services'] as $service) {

                                            //Comprueba si se ha introducido un servicio repetido para el tipo de cliente
                                            if (isset($service['id'])) {
                                                if (in_array($service['id'], $array_servicios)) {
                                                    $error = 1;
                                                    $mensaje_validador = $mensaje_validador->merge(['products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.id' =>
                                                        ['The products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.id field has a duplicate value.']]);
                                                }
                                                $array_servicios[] = $service['id'];
                                            }

                                            if (isset($service['id']) && $service['id'] != 1) {
                                                if (!isset($service['net_price']) && !isset($service['sessions']) && isset($service['id']) && $service['id'] != 1) {
                                                    $error = 1;
                                                    $mensaje_validador = $mensaje_validador->merge(['products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.sessions' =>
                                                        ['The products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.sessions field is required when products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.net_price is not present.']]);
                                                } elseif (!isset($service['net_price']) && !isset($service['pickups']) && isset($service['id']) && $service['id'] == 1) {
                                                    $error = 1;
                                                    $mensaje_validador = $mensaje_validador->merge(['products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.pickups' =>
                                                        ['The products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.pickups field is required when products.' . $key . '.locations.' . $localizacion . '.client_types.' . $tipo_cliente . '.services.' . $servicio . '.net_price is not present.']]);
                                                }
                                            }
                                            $servicio++;
                                        }
                                    }
                                    $tipo_cliente++;
                                }
                            }
                            $localizacion++;
                        }
                    }
                }
            }


            //Fin validacion

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                if ($request->isMethod('put')) {
                    $validator = Validator::make($request->all(), [
                        'id' => 'exists:tenant.mo_price,id,deleted_at,NULL',
                    ]);
                }
                if (!$validator->fails()) {
                    //Comprueba la existencia de una tarifa en las fechas indicadas
                    $prices = DB::connection('tenant')->table('mo_price')
                        ->where('mo_price.deleted_at', '=', null)
                        ->where('mo_price.contract_model_id', '=', $request->get('contract_model_id'))
                        ->where(function ($query) use ($request) {
                            $fecha_inicio = Carbon::createFromFormat('Y-m-d', $request->get('date_start'));
                            $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'));

                            $query->orWhere(function ($orquery) use ($fecha_fin, $fecha_inicio) {
                                $orquery->where('mo_price.date_start', '>=', $fecha_inicio->format('Y-m-d'))
                                    ->where('mo_price.date_start', '<=', $fecha_fin->format('Y-m-d'));
                            });
                            $query->orWhere(function ($orquery) use ($fecha_fin, $fecha_inicio) {
                                $orquery->where('mo_price.date_end', '>=', $fecha_inicio->format('Y-m-d'))
                                    ->where('mo_price.date_end', '<=', $fecha_fin->format('Y-m-d'));
                            });
                            $query->orWhere(function ($orquery) use ($fecha_fin, $fecha_inicio) {
                                $orquery->where('mo_price.date_start', '<=', $fecha_inicio->format('Y-m-d'))
                                    ->where('mo_price.date_end', '>=', $fecha_fin->format('Y-m-d'));
                            });
                        });

                    //Si recibe id se actualizara la tarifa si existe
                    if ($request->isMethod('put')) {
                        $prices->where('mo_price.id', '!=', $request->get('id'));
                    }

                    $tarifas = $prices->first();

                    if ($tarifas) {
                        //Si existe tarifa envio de mensaje de error
                        $array['error'] = 409;
                        $array['error_description'] = 'There is a price with the specified data';
                    } else {
                        $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                            ->where('mo_settings.name', 'heimdall_middleware')
                            ->first()
                            ->value;

                        $rule_codes = [];

                        if($heimdall_middleware == '1') {
                            $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                            $website = DB::table('saas_website')
                                            ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                            'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                            'saas_website.default_language_id', 'saas_website.default_currency_id')
                                            ->whereNull('saas_website.deleted_at')
                                            ->where('saas_website.website_id', $website->id)
                                            ->first();

                            $plans = websitePlanRules($website);

                            $cantidad = 0;

                            foreach($plans as $plan) {
                                if($plan->plan_id != null){
                                    $rules = websiteRules($plan);
                                    
                                    $rules_price = $rules->where('code', 'price');

                                    if($rules_price) {
                                        foreach($rules_price as $rule) {
                                            if($rule->unlimited == 1){
                                                $cantidad = -1;
                                            } else {
                                                if($cantidad >= 0 && $cantidad < $rule->value) {
                                                    $cantidad = $rule->value;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if($plan->rule_code == 'price') {
                                        if($plan->unlimited == 1){
                                            $cantidad = -1;
                                        }else{
                                            if($cantidad >= 0 && $cantidad < $plan->value){
                                                $cantidad = $plan->value;
                                            }
                                        }
                                    }
                                }
                            }

                            if($cantidad == 0) {
                                $error = 1;
                                $rule_codes[] = 'price';
                            } else if ($cantidad > 0) {
                                $total_prices = Price::get();

                                if(count($total_prices) >= $cantidad) {
                                    $error = 1;
                                    $rule_codes[] = 'price';
                                }
                            }
                        }

                        if($error == 1) {
                            $array['error'] = 419;
                            $array['error_description'] = 'You do not have a valid plan';
                            $array['error_rule_code'] = $rule_codes;

                            //Heimdall - datos para redirección
                            $array['data'] = array();
                            $array['data'][0]['website_id'] = $website->id;

                            $protocol = DB::table('saas_settings')
                            ->where('saas_settings.name', 'protocol')->first()->value;
                            $base_path_saas = DB::table('saas_settings')
                            ->where('saas_settings.name', 'base_path_saas')->first()->value;
                            $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';

                        } else {
                            if ($request->isMethod('PUT')) {
                                //Update tarifa
                                $tarifa = Price::find($request->get('id'));
                                $tarifa->priceProduct()->delete();

                                $fecha_inicio = Carbon::createFromFormat('Y-m-d', $request->get('date_start'));
                                $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'));

                                $tarifa->update([
                                    'contract_model_id' => $request->get('contract_model_id'),
                                    'currency_id' => $request->get('currency_id'),
                                    'promotions' => $request->get('promotions') != '' ? $request->get('promotions') : 0,
                                    'date_start' => $fecha_inicio->format('Y-m-d'),
                                    'date_end' => $fecha_fin->format('Y-m-d'),
                                    'user_id' => $request->get('user_id'),
                                ]);

                            } else {

                                // Create tarifa
                                $fecha_inicio = Carbon::createFromFormat('Y-m-d', $request->get('date_start'));
                                $fecha_fin = Carbon::createFromFormat('Y-m-d', $request->get('date_end'));

                                $tarifa = Price::create([
                                    'contract_model_id' => $request->get('contract_model_id'),
                                    'currency_id' => $request->get('currency_id'),
                                    'promotions' => $request->get('promotions') != '' ? $request->get('promotions') : 0,
                                    'date_start' => $fecha_inicio->format('Y-m-d'),
                                    'date_end' => $fecha_fin->format('Y-m-d'),
                                    'user_id' => $request->get('user_id'),
                                ]);

                                $array['data'][]['price'][]['id'] = $tarifa->id;
                            }

                            if ($request->get('products')) {
                                //Recorre el array de productos si se envia y crea los registros
                                foreach ($request->get('products') as $registro) {
                                    foreach ($registro['locations'] as $localizacion) {
                                        foreach ($localizacion['client_types'] as $tipocliente) {
                                            if (isset($tipocliente['net_price'])) {
                                                //Prepara datos a insertar en price_product
                                                if (isset($tipocliente['sale_price'])) {
                                                    $sale_price = $tipocliente['sale_price'];
                                                } elseif ($tipocliente['markup'] != 0) {
                                                    $sale_price = $tipocliente['net_price'] * ($tipocliente['markup'] / 100) + $tipocliente['net_price'];
                                                } else {
                                                    $sale_price = $tipocliente['net_price'];
                                                }

                                                if (isset($tipocliente['markup'])) {
                                                    $markup = $tipocliente['markup'];
                                                } elseif ($tipocliente['net_price'] != 0) {
                                                    $markup = ($tipocliente['sale_price'] * 100) / $tipocliente['net_price'] - 100;
                                                } else {
                                                    $markup = 0;
                                                }
                                                //Fin preparacion de datos
                                                $price_product = PriceProduct::create([
                                                    'price_id' => $tarifa->id,
                                                    'product_id' => $registro['id'],
                                                    'net_price' => $tipocliente['net_price'],
                                                    'sale_price' => $sale_price,
                                                    'markup' => $markup,
                                                    'price_before' => isset($tipocliente['price_before']) ? $tipocliente['price_before'] : null,
                                                    'client_type_id' => isset($tipocliente['id']) ? $tipocliente['id'] : null,
                                                    'location_id' => $localizacion['id'],
                                                    'tip_amount' => isset($tipocliente['tip_amount']) ? $tipocliente['tip_amount'] : null,
                                                    'tip_percent' => isset($tipocliente['tip_percent']) ? $tipocliente['tip_percent'] : null,
                                                    'commission_amount' => isset($tipocliente['commission_amount']) ? $tipocliente['commission_amount'] : null,
                                                    'commission_percent' => isset($tipocliente['commission_percent']) ? $tipocliente['commission_percent'] : null,
                                                ]);

                                                if(isset($tipocliente['intercompany'])){
                                                    foreach ($tipocliente['intercompany'] as $intercompany){
                                                        PriceProductIntercompany::create([
                                                            'price_product_id' => $price_product->id,
                                                            'currency_id' => isset($intercompany['currency_id']) ? $intercompany['currency_id'] : null,
                                                            'company_id' => isset($intercompany['id']) ? $intercompany['id'] : null,
                                                            'amount' => isset($intercompany['amount']) ? $intercompany['amount'] : null,
                                                            'percent' => isset($intercompany['percent']) ? $intercompany['percent'] : null,
                                                        ]);
                                                    }
                                                }
                                            }

                                            //Guarda la tarifa por sesion del producto
                                            if (isset($tipocliente['sessions'])) {
                                                foreach ($tipocliente['sessions'] as $sessions) {
                                                    //Prepara datos a insertar en price_product
                                                    if (isset($sessions['sale_price'])) {
                                                        $sale_price = $sessions['sale_price'];
                                                    } elseif ($sessions['markup'] != 0) {
                                                        $sale_price = $sessions['net_price'] * ($sessions['markup'] / 100) + $sessions['net_price'];
                                                    } else {
                                                        $sale_price = $sessions['net_price'];
                                                    }

                                                    if (isset($sessions['markup'])) {
                                                        $markup = $sessions['markup'];
                                                    } elseif ($sessions['net_price'] != 0) {
                                                        $markup = ($sessions['sale_price'] * 100) / $sessions['net_price'] - 100;
                                                    } else {
                                                        $markup = 0;
                                                    }
                                                    //Fin preparacion de datos

                                                    $price_product = PriceProduct::create([
                                                        'session' => $sessions['session'],
                                                        'price_id' => $tarifa->id,
                                                        'product_id' => $registro['id'],
                                                        'net_price' => $sessions['net_price'],
                                                        'sale_price' => $sale_price,
                                                        'markup' => $markup,
                                                        'price_before' => isset($sessions['price_before']) ? $sessions['price_before'] : null,
                                                        'client_type_id' => isset($tipocliente['id']) ? $tipocliente['id'] : null,
                                                        'location_id' => $localizacion['id'],
                                                        'tip_amount' => isset($sessions['tip_amount']) ? $sessions['tip_amount'] : null,
                                                        'tip_percent' => isset($sessions['tip_percent']) ? $sessions['tip_percent'] : null,
                                                        'commission_amount' => isset($sessions['commission_amount']) ? $sessions['commission_amount'] : null,
                                                        'commission_percent' => isset($sessions['commission_percent']) ? $sessions['commission_percent'] : null,
                                                    ]);

                                                    if(isset($sessions['intercompany'])){
                                                        foreach ($sessions['intercompany'] as $intercompany){
                                                            PriceProductIntercompany::create([
                                                                'price_product_id' => $price_product->id,
                                                                'currency_id' => isset($intercompany['currency_id']) ? $intercompany['currency_id'] : null,
                                                                'company_id' => isset($intercompany['id']) ? $intercompany['id'] : null,
                                                                'amount' => isset($intercompany['amount']) ? $intercompany['amount'] : null,
                                                                'percent' => isset($intercompany['percent']) ? $intercompany['percent'] : null,
                                                            ]);
                                                        }
                                                    }
                                                }
                                            }

                                            //Guarda la tarifa general para los servicios
                                            if (isset($tipocliente['services'])) {
                                                foreach ($tipocliente['services'] as $service) {
                                                    if (isset($service['net_price']) && $service['id'] != 1) {
                                                        //Prepara datos a insertar en price_product
                                                        if (isset($service['sale_price'])) {
                                                            $sale_price = $service['sale_price'];
                                                        } elseif ($service['markup'] != 0) {
                                                            $sale_price = $service['net_price'] * ($service['markup'] / 100) + $service['net_price'];
                                                        } else {
                                                            $sale_price = $service['net_price'];
                                                        }

                                                        if (isset($service['markup'])) {
                                                            $markup = $service['markup'];
                                                        } elseif ($service['net_price'] != 0) {
                                                            $markup = ($service['sale_price'] * 100) / $service['net_price'] - 100;
                                                        } else {
                                                            $markup = 0;
                                                        }
                                                        //Fin preparacion de datos

                                                        $price_product_service = PriceProduct::create([
                                                            'price_id' => $tarifa->id,
                                                            'product_id' => $registro['id'],
                                                            'service_id' => $service['id'],
                                                            'pickup_id' => $service['id'] == 1 ? $service['id'] : null,
                                                            'net_price' => $service['net_price'],
                                                            'sale_price' => $sale_price,
                                                            'markup' => $markup,
                                                            'price_before' => isset($service['price_before']) ? $service['price_before'] : null,
                                                            'client_type_id' => isset($tipocliente['id']) ? $tipocliente['id'] : null,
                                                            'location_id' => $localizacion['id'],
                                                            'tip_amount' => isset($service['tip_amount']) ? $service['tip_amount'] : null,
                                                            'tip_percent' => isset($service['tip_percent']) ? $service['tip_percent'] : null,
                                                            'commission_amount' => isset($service['commission_amount']) ? $service['commission_amount'] : null,
                                                            'commission_percent' => isset($service['commission_percent']) ? $service['commission_percent'] : null,
                                                        ]);


                                                        if(isset($service['intercompany'])){
                                                            foreach ($service['intercompany'] as $intercompany){
                                                                PriceProductIntercompany::create([
                                                                    'price_product_id' => $price_product_service->id,
                                                                    'currency_id' => isset($intercompany['currency_id']) ? $intercompany['currency_id'] : null,
                                                                    'company_id' => isset($intercompany['id']) ? $intercompany['id'] : null,
                                                                    'amount' => isset($intercompany['amount']) ? $intercompany['amount'] : null,
                                                                    'percent' => isset($intercompany['percent']) ? $intercompany['percent'] : null,
                                                                ]);
                                                            }
                                                        }
                                                    }
                                                    if ($service['id'] != 1) {
                                                        //Si el servicio no es trasportación comprueba si se envian sesiones y las guarda
                                                        if (isset($service['sessions'])) {
                                                            foreach ($service['sessions'] as $sessions) {
                                                                //Prepara datos a insertar en price_product
                                                                if (isset($sessions['sale_price'])) {
                                                                    $sale_price = $sessions['sale_price'];
                                                                } elseif ($sessions['markup'] != 0) {
                                                                    $sale_price = $sessions['net_price'] * ($sessions['markup'] / 100) + $sessions['net_price'];
                                                                } else {
                                                                    $sale_price = $sessions['net_price'];
                                                                }

                                                                if (isset($sessions['markup'])) {
                                                                    $markup = $sessions['markup'];
                                                                } elseif ($sessions['net_price'] != 0) {
                                                                    $markup = ($sessions['sale_price'] * 100) / $sessions['net_price'] - 100;
                                                                } else {
                                                                    $markup = 0;
                                                                }

                                                                //Fin preparacion de datos
                                                                $price_product_service_session = PriceProduct::create([
                                                                    'session' => $sessions['session'],
                                                                    'price_id' => $tarifa->id,
                                                                    'product_id' => $registro['id'],
                                                                    'service_id' => $service['id'],
                                                                    'net_price' => $sessions['net_price'],
                                                                    'sale_price' => $sale_price,
                                                                    'markup' => $markup,
                                                                    'price_before' => isset($sessions['price_before']) ? $sessions['price_before'] : null,
                                                                    'client_type_id' => isset($tipocliente['id']) ? $tipocliente['id'] : null,
                                                                    'location_id' => $localizacion['id'],
                                                                    'tip_amount' => isset($sessions['tip_amount']) ? $sessions['tip_amount'] : null,
                                                                    'tip_percent' => isset($sessions['tip_percent']) ? $sessions['tip_percent'] : null,
                                                                    'commission_amount' => isset($sessions['commission_amount']) ? $sessions['commission_amount'] : null,
                                                                    'commission_percent' => isset($sessions['commission_percent']) ? $sessions['commission_percent'] : null,
                                                                ]);

                                                                if(isset($sessions['intercompany'])){
                                                                    foreach ($sessions['intercompany'] as $intercompany){
                                                                        PriceProductIntercompany::create([
                                                                            'price_product_id' => $price_product_service_session->id,
                                                                            'currency_id' => isset($intercompany['currency_id']) ? $intercompany['currency_id'] : null,
                                                                            'company_id' => isset($intercompany['id']) ? $intercompany['id'] : null,
                                                                            'amount' => isset($intercompany['amount']) ? $intercompany['amount'] : null,
                                                                            'percent' => isset($intercompany['percent']) ? $intercompany['percent'] : null,
                                                                        ]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        foreach ($service['pickups'] as $pickup) {
                                                            //Prepara datos a insertar en price_product
                                                            if (isset($pickup['sale_price'])) {
                                                                $sale_price = $pickup['sale_price'];
                                                            } elseif ($pickup['markup'] != 0) {
                                                                $sale_price = $pickup['net_price'] * ($pickup['markup'] / 100) + $pickup['net_price'];
                                                            } else {
                                                                $sale_price = $pickup['net_price'];
                                                            }

                                                            if (isset($pickup['markup'])) {
                                                                $markup = $pickup['markup'];
                                                            } elseif ($pickup['net_price'] != 0) {
                                                                $markup = ($pickup['sale_price'] * 100) / $pickup['net_price'] - 100;
                                                            } else {
                                                                $markup = 0;
                                                            }

                                                            //Fin preparacion de datos
                                                            $price_product_service_pickup = PriceProduct::create([
                                                                'price_id' => $tarifa->id,
                                                                'product_id' => $registro['id'],
                                                                'service_id' => $service['id'],
                                                                'pickup_id' => $pickup['id'],
                                                                'net_price' => $pickup['net_price'],
                                                                'sale_price' => $sale_price,
                                                                'markup' => $markup,
                                                                'price_before' => isset($pickup['price_before']) ? $pickup['price_before'] : null,
                                                                'client_type_id' => isset($tipocliente['id']) ? $tipocliente['id'] : null,
                                                                'location_id' => $localizacion['id'],
                                                                'tip_amount' => isset($pickup['tip_amount']) ? $pickup['tip_amount'] : null,
                                                                'tip_percent' => isset($pickup['tip_percent']) ? $pickup['tip_percent'] : null,
                                                                'commission_amount' => isset($pickup['commission_amount']) ? $pickup['commission_amount'] : null,
                                                                'commission_percent' => isset($pickup['commission_percent']) ? $pickup['commission_percent'] : null,
                                                            ]);

                                                            if(isset($pickup['intercompany'])){
                                                                foreach ($pickup['intercompany'] as $intercompany){
                                                                    PriceProductIntercompany::create([
                                                                        'price_product_id' => $price_product_service_pickup->id,
                                                                        'currency_id' => isset($intercompany['currency_id']) ? $intercompany['currency_id'] : null,
                                                                        'company_id' => isset($intercompany['id']) ? $intercompany['id'] : null,
                                                                        'amount' => isset($intercompany['amount']) ? $intercompany['amount'] : null,
                                                                        'percent' => isset($intercompany['percent']) ? $intercompany['percent'] : null,
                                                                    ]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][] = $validator->errors();
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tarifas');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra una tarifa buscada por su id y los precios de todos los productos
     *
     * Recibe un parámetro id en el path y muestra los datos de la tarifa solicitada, muestra todos los productos tengan o no tarifa, sus servicios asociados. El producto o servicio que no tenga tarifa mostrará los campos tarifarios como null.
     * Para la consulta de tarifa se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $array['data'] = array();

            // Precompiladas
            $productos = DB::connection('tenant')->table('mo_product')
                ->select(
                    'mo_product.id',
                    'mo_product.company_id'
                )
                ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                ->where('mo_product.deleted_at', '=', null)
                ->where('mo_product_translation.deleted_at', '=', null)
                ->groupBy('mo_product.id');

            $sql_client_type = DB::connection('tenant')->table('mo_client_type')
                ->select('mo_client_type.id', 'mo_client_type.code', 'mo_client_type.from', 'mo_client_type.to', 'mo_client_type_translation.name')
                ->where('mo_client_type.deleted_at', '=', null)
                ->join('mo_client_type_translation', 'mo_client_type_translation.client_type_id', 'mo_client_type.id')
                ->where('mo_client_type_translation.deleted_at', '=', null)
                ->groupBy('mo_client_type.id');

            $serviciosAsociados = DB::connection('tenant')->table('mo_service')
                ->select('mo_service.id', 'mo_service_translation.name')
                ->join('mo_service_translation', 'mo_service.id', '=', 'mo_service_translation.service_id')
                ->join('mo_product_service', 'mo_product_service.service_id', 'mo_service.id')
                ->whereNull('mo_product_service.deleted_at')
                ->where('mo_service.deleted_at', '=', null)
                ->where('mo_service_translation.deleted_at', '=', null)
                ->groupBy('mo_service_translation.service_id');

            $sql_tarifa_client_type = DB::connection('tenant')->table('mo_price_product')
                ->select(
                    'mo_price_product.id',
                    'mo_price_product.id',
                    'mo_price_product.price_id',
                    'mo_price_product.net_price',
                    'mo_price_product.sale_price',
                    'mo_price_product.markup',
                    'mo_price_product.price_before',
                    'mo_price_product.tip_amount',
                    'mo_price_product.tip_percent',
                    'mo_price_product.commission_amount',
                    'mo_price_product.commission_percent'
                    )
                ->where('mo_price_product.deleted_at', '=', null);

            $datos_transporteLocation = DB::connection('tenant')->table('mo_transportation_location')
                ->select('id', 'name')
                ->where('deleted_at', '=', null)
                ->get();

            $sql_types = DB::connection('tenant')->table('mo_product_type')
                ->select('mo_product_type.id', 'mo_product_type_translation.name')
                ->where('mo_product_type.deleted_at', '=', null)
                ->join('mo_product_type_translation', 'mo_product_type_translation.type_id', '=', 'mo_product_type.id')
                ->where('mo_product_type_translation.deleted_at', '=', null)
                ->groupBy('mo_product_type_translation.type_id');

            $sql_localization = DB::connection('tenant')->table('mo_location')
                ->select('mo_location.id', 'mo_location.name')
                ->where('mo_location.deleted_at', null)
                ->join('mo_product_location', 'mo_product_location.location_id', 'mo_location.id')
                ->where('mo_product_location.deleted_at', null);

            $sql_availability = DB::connection('tenant')->table('mo_availability')
                ->select('mo_availability.session')
                ->whereNull('mo_availability.deleted_at')
                ->whereNull('mo_availability.service_id')
                ->orderBy('mo_availability.date')
                ->orderBy('mo_availability.session', 'asc')
                ->groupBy('mo_availability.session');

            $sql_availability_servicios = DB::connection('tenant')->table('mo_availability')
                ->select('mo_availability.session')
                ->whereNull('mo_availability.deleted_at')
                ->orderBy('mo_availability.date')
                ->orderBy('mo_availability.session', 'asc')
                ->groupBy('mo_availability.session');

            $datos_idiomas = Language::where('front',1)->get();

            $datos_product_intercompany = Settings::where('name','product_intercompany')->first()->value;

            // FIN Precompiladas

            $tarifa = Price::where('id', $id)->first(['id', 'date_start', 'date_end', 'contract_model_id', 'currency_id', 'promotions', 'user_id']);
            if ($tarifa) {

                $array_tarifa = array();
                $array_tarifa = [
                    'id' => $tarifa->id,
                    'date_start' => $tarifa->date_start,
                    'date_end' => $tarifa->date_end,
                    'promotions' => $tarifa->promotions,
                    'currency_id' => $tarifa->currency_id,
                    'contract_model_id' => $tarifa->contract_model_id,
                    'user_id' => $tarifa->user_id];


                //Se guardan los datos de la tarifa en el array
                $array['data'][0]['price'][] = $array_tarifa;

                // Se obtienen los tipos de producto
                $tipos = $sql_types->get();

                $datos_client_type_clone = $sql_client_type->get();

                foreach ($tipos as $tipo) {

                    // Se obtiene todos los productos

                    $productos_clone = clone $productos;
                    $datos_productos_clone = $productos_clone
                        ->where('mo_product.type_id', $tipo->id)
                        ->get();

                    $tipo->lang = array();

                    $array_type = array();
                    $array_type['id'] = $tipo->id;
                    foreach ($datos_idiomas as $idioma) {
                        $traduccion = ProductTypeTranslation::where('mo_product_type_translation.language_id', '=', $idioma->id)
                            ->where('mo_product_type_translation.type_id', '=', $tipo->id)
                            ->first();
                        if ($traduccion) {
                            $array_type["lang"][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                        }
                    }

                    foreach ($datos_productos_clone as $producto) {

                        $datos_companys = Company::where('id','!=',$producto->company_id)->get();

                        $producto->lang = array();
                        // Se obtienen las traducciones del tipo de producto
                        foreach ($datos_idiomas as $idioma) {
                            $traduccion = ProductTranslation::where('mo_product_translation.language_id', '=', $idioma->id)
                                ->where('mo_product_translation.product_id', '=', $producto->id)
                                ->first();
                            if ($traduccion) {
                                $producto->lang[][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                            }


                        }

                        // Se obtienen los servicios asociados a cada producto, también por localización
                        $serviciosAsociados_clone = clone $serviciosAsociados;

                        $datos_serviciosAsociados_clone = $serviciosAsociados_clone
                            ->where('mo_product_service.product_id', $producto->id)
                            ->get();


                        // Se obtienen las localizaciones del producto
                        $sql_localization_clone = clone $sql_localization;

                        $datos_localizacion_producto = $sql_localization_clone->where('mo_product_location.product_id', $producto->id)->get();

                        $producto->location = array();


                        foreach ($datos_localizacion_producto as $localizacion) {

                            $localizacion_array = array();

                            $localizacion_array['id'] = $localizacion->id;
                            $localizacion_array['name'] = $localizacion->name;

                            $localizacion_array['client_type'] = array();
                            foreach ($datos_client_type_clone as $tipo_client) {

                                $array_tipo_cliente = array();
                                $array_tipo_cliente['id'] = $tipo_client->id;
                                $array_tipo_cliente['code'] = $tipo_client->code;
                                $array_tipo_cliente['from'] = $tipo_client->from;
                                $array_tipo_cliente['to'] = $tipo_client->to;

                                $array_tipo_cliente['lang'] = array();

                                foreach ($datos_idiomas as $idioma) {
                                    $traduccion = ClientTypeTranslation::where('mo_client_type_translation.language_id', '=', $idioma->id)
                                        ->where('mo_client_type_translation.client_type_id', '=', $tipo_client->id)
                                        ->first();
                                    if ($traduccion) {
                                        $array_tipo_cliente['lang'][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                                    }

                                }


                                $array_availability = array();

                                //Se obtienen la tarifa general asociada al producto
                                $sql_tarifa_client_type_general_clone = clone $sql_tarifa_client_type;
                                $datos_tarifa_client_type_general_clone = $sql_tarifa_client_type_general_clone->where('mo_price_product.client_type_id', $tipo_client->id)
                                    ->where('mo_price_product.product_id', $producto->id)
                                    ->where('mo_price_product.price_id', $id)
                                    ->where('mo_price_product.service_id', null)
                                    ->where('mo_price_product.location_id', $localizacion->id)
                                    ->whereNull('mo_price_product.session')
                                    ->get();


                                //Se incluye la tarifa general para el producto en la localización recorrida y para el tipo de cliente recorrido

                                $array_disponibilidad = [
                                    "session" => null,
                                    "net_price" => isset($datos_tarifa_client_type_general_clone[0]->net_price) ? $datos_tarifa_client_type_general_clone[0]->net_price : null,
                                    "sale_price" => isset($datos_tarifa_client_type_general_clone[0]->sale_price) ? $datos_tarifa_client_type_general_clone[0]->sale_price : null,
                                    "markup" => isset($datos_tarifa_client_type_general_clone[0]->markup) ? $datos_tarifa_client_type_general_clone[0]->markup : null,
                                    "price_before" => isset($datos_tarifa_client_type_general_clone[0]->price_before) ? $datos_tarifa_client_type_general_clone[0]->price_before : null,
                                    "tip_amount" => isset($datos_tarifa_client_type_general_clone[0]->tip_amount) ? $datos_tarifa_client_type_general_clone[0]->tip_amount : null,
                                    "tip_percent" => isset($datos_tarifa_client_type_general_clone[0]->tip_percent) ? $datos_tarifa_client_type_general_clone[0]->tip_percent : null,
                                    "commission_amount" => isset($datos_tarifa_client_type_general_clone[0]->commission_amount) ? $datos_tarifa_client_type_general_clone[0]->commission_amount : null,
                                    "commission_percent" => isset($datos_tarifa_client_type_general_clone[0]->commission_percent) ? $datos_tarifa_client_type_general_clone[0]->commission_percent : null,
                                ];

                                if($datos_product_intercompany == '1') {

                                    foreach ($datos_companys as $company) {

                                        $array_intercompany = [
                                            'id' => $company->id,
                                            'name' => $company->name,
                                        ];

                                        if (isset($datos_tarifa_client_type_general_clone[0]->id)) {
                                            if ($producto->id == 1) {
                                            }
                                            $datos_price_product_intercompany = PriceProductIntercompany::where('price_product_id', $datos_tarifa_client_type_general_clone[0]->id)
                                                ->where('company_id', $company->id)->first();

                                            $array_intercompany['company_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->company_id : null;
                                            $array_intercompany['amount'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->amount : null;
                                            $array_intercompany['percent'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->percent : null;
                                            $array_intercompany['currency_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->currency_id : null;
                                        } else {
                                            $array_intercompany['company_id'] = null;
                                            $array_intercompany['amount'] = null;
                                            $array_intercompany['percent'] = null;
                                            $array_intercompany['currency_id'] = null;
                                        }


                                        $array_disponibilidad['intercompany'][] = $array_intercompany;
                                    }
                                }


                                $array_availability[] = $array_disponibilidad;


                                //Se obtienen las disponibilidades del producto para las fechas de la tarifa
                                $sql_availability_clone = clone $sql_availability;
                                $datos_availability = $sql_availability_clone
                                    ->where('mo_availability.location_id', '=', $localizacion->id)
                                    ->where('mo_availability.product_id', '=', $producto->id)
                                    ->where(function ($query) use ($tarifa) {
                                        $query->where('mo_availability.date', '>=', $tarifa->date_start)
                                            ->where('mo_availability.date', '<=', $tarifa->date_end);

                                    })
                                    ->get();

                                //Se recorren las disponibilidades del producto para las fechas de la tarifa o disponibilidad general del producto
                                foreach ($datos_availability as $availability) {

                                    //Se obtiene las tarifas para las disponibilidades del producto
                                    $sql_tarifa_client_type_clone = clone $sql_tarifa_client_type;
                                    $datos_tarifa_client_type = $sql_tarifa_client_type_clone->where('mo_price_product.client_type_id', $tipo_client->id)
                                        ->where('mo_price_product.product_id', $producto->id)
                                        ->where('mo_price_product.price_id', $id)
                                        ->where('mo_price_product.service_id', null)
                                        ->where('mo_price_product.location_id', $localizacion->id)
                                        ->where('mo_price_product.session', $availability->session)
                                        ->get();

                                    //Se incluye la tarifa para la disponibilidad del producto para la session existente
                                    $availability->net_price = isset($datos_tarifa_client_type[0]->net_price) ? $datos_tarifa_client_type[0]->net_price : null;
                                    $availability->sale_price = isset($datos_tarifa_client_type[0]->sale_price) ? $datos_tarifa_client_type[0]->sale_price : null;
                                    $availability->markup = isset($datos_tarifa_client_type[0]->markup) ? $datos_tarifa_client_type[0]->markup : null;
                                    $availability->price_before = isset($datos_tarifa_client_type[0]->price_before) ? $datos_tarifa_client_type[0]->price_before : null;
                                    $availability->tip_amount = isset($datos_tarifa_client_type[0]->tip_amount) ? $datos_tarifa_client_type[0]->tip_amount : null;
                                    $availability->tip_percent = isset($datos_tarifa_client_type[0]->tip_percent) ? $datos_tarifa_client_type[0]->tip_percent : null;
                                    $availability->commission_amount = isset($datos_tarifa_client_type[0]->commission_amount) ? $datos_tarifa_client_type[0]->commission_amount : null;
                                    $availability->commission_percent = isset($datos_tarifa_client_type[0]->commission_percent) ? $datos_tarifa_client_type[0]->commission_percent : null;

                                    if($datos_product_intercompany == '1') {
                                        $availability->intercompany = array();

                                        foreach ($datos_companys as $company) {

                                            $array_intercompany = [
                                                'id' => $company->id,
                                                'name' => $company->name,
                                            ];

                                            if (isset($datos_tarifa_client_type[0]->id)) {
                                                $datos_price_product_intercompany = PriceProductIntercompany::where('price_product_id', $datos_tarifa_client_type[0]->id)
                                                    ->where('company_id', $company->id)->first();

                                                $array_intercompany['company_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->company_id : null;
                                                $array_intercompany['amount'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->amount : null;
                                                $array_intercompany['percent'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->percent : null;
                                                $array_intercompany['currency_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->currency_id : null;
                                            } else {
                                                $array_intercompany['company_id'] = null;
                                                $array_intercompany['amount'] = null;
                                                $array_intercompany['percent'] = null;
                                                $array_intercompany['currency_id'] = null;
                                            }


                                            $availability->intercompany[] = $array_intercompany;
                                        }
                                    }

                                    $array_availability[] = $availability;
                                }


                                $array_tipo_cliente['price'] = $array_availability;
                                $array_tipo_cliente['service'] = array();

                                //Se recorren los servicios asociados al producto
                                foreach ($datos_serviciosAsociados_clone as $servicio) {

                                    $servicios_array = array();
                                    $servicios_array['id'] = $servicio->id;

                                    $servicios_array["lang"] = array();

                                    // Se obtienen las traducciones del servicio
                                    foreach ($datos_idiomas as $idioma) {
                                        $traduccion = ServiceTranslation::where('mo_service_translation.language_id', '=', $idioma->id)
                                            ->where('mo_service_translation.service_id', '=', $servicio->id)
                                            ->first();
                                        if ($traduccion) {
                                            $servicios_array["lang"][][$idioma->abbreviation] = ['id' => $traduccion->id, 'language_id' => $traduccion->language_id, 'name' => $traduccion->name];
                                        }

                                    }


                                    //Si el servicio es transportacion (id == 1)
                                    if ($servicio->id == 1) {
                                        //Se recorren los pickups
                                        foreach ($datos_transporteLocation as $pickup) {
                                            //Se perpara consulta para la tarifa con el pickup del servicio transportación
                                            $sql_serviciosAsociadosTarifa_clone = clone $sql_tarifa_client_type;
                                            $datos_serviciosAsociadosTarifa = $sql_serviciosAsociadosTarifa_clone->where('mo_price_product.service_id', $servicio->id)
                                                ->where('mo_price_product.product_id', $producto->id)
                                                ->where('mo_price_product.price_id', $id)
                                                ->where('mo_price_product.pickup_id', $pickup->id)
                                                ->where('mo_price_product.location_id', $localizacion->id)
                                                ->where('mo_price_product.service_id', $servicio->id)
                                                ->whereNull('mo_price_product.session')
                                                ->where('mo_price_product.client_type_id', $tipo_client->id)->get();

                                            $pickup_array = array();
                                            $pickup_array['id'] = $pickup->id;
                                            $pickup_array['name'] = $pickup->name;

                                            //Se obtiene la tarifa general para el servicio transportación

                                            $array_pickups = [
                                                'net_price' => isset($datos_serviciosAsociadosTarifa[0]->net_price) ? $datos_serviciosAsociadosTarifa[0]->net_price : null,
                                                "sale_price" => isset($datos_serviciosAsociadosTarifa[0]->sale_price) ? $datos_serviciosAsociadosTarifa[0]->sale_price : null,
                                                'markup' => isset($datos_serviciosAsociadosTarifa[0]->markup) ? $datos_serviciosAsociadosTarifa[0]->markup : null,
                                                "price_before" => isset($datos_serviciosAsociadosTarifa[0]->price_before) ? $datos_serviciosAsociadosTarifa[0]->price_before : null,
                                                "tip_amount" => isset($datos_serviciosAsociadosTarifa[0]->tip_amount) ? $datos_serviciosAsociadosTarifa[0]->tip_amount : null,
                                                "tip_percent" => isset($datos_serviciosAsociadosTarifa[0]->tip_percent) ? $datos_serviciosAsociadosTarifa[0]->tip_percent : null,
                                                "commission_amount" => isset($datos_serviciosAsociadosTarifa[0]->commission_amount) ? $datos_serviciosAsociadosTarifa[0]->commission_amount : null,
                                                "commission_percent" => isset($datos_serviciosAsociadosTarifa[0]->commission_percent) ? $datos_serviciosAsociadosTarifa[0]->commission_percent : null,
                                            ];

                                            if($datos_product_intercompany == '1') {

                                                foreach ($datos_companys as $company) {

                                                    $array_intercompany = [
                                                        'id' => $company->id,
                                                        'name' => $company->name,
                                                    ];

                                                    if (isset($datos_serviciosAsociadosTarifa[0]->id)) {
                                                        $datos_price_product_intercompany = PriceProductIntercompany::where('price_product_id', $datos_serviciosAsociadosTarifa[0]->id)
                                                            ->where('company_id', $company->id)->first();

                                                        $array_intercompany['company_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->company_id : null;
                                                        $array_intercompany['amount'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->amount : null;
                                                        $array_intercompany['percent'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->percent : null;
                                                        $array_intercompany['currency_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->currency_id : null;
                                                    } else {
                                                        $array_intercompany['company_id'] = null;
                                                        $array_intercompany['amount'] = null;
                                                        $array_intercompany['percent'] = null;
                                                        $array_intercompany['currency_id'] = null;
                                                    }


                                                    $array_pickups['intercompany'][] = $array_intercompany;
                                                }
                                            }

                                            $pickup_array['price'] = $array_pickups;

                                            $servicios_array['pickup'][] = $pickup_array;
                                        }
                                        $array_tipo_cliente['service'][] = $servicios_array;
                                    } else {

                                        //Si el servicio no es transportación
                                        $array_availability = array();

                                        //Se obtiene la tarifa general para el servicio recorrido que no sea transportación
                                        $sql_tarifa_client_type_general_clone = clone $sql_tarifa_client_type;
                                        $datos_tarifa_client_type_general_clone = $sql_tarifa_client_type_general_clone->where('mo_price_product.client_type_id', $tipo_client->id)
                                            ->where('mo_price_product.product_id', $producto->id)
                                            ->where('mo_price_product.price_id', $id)
                                            ->where('mo_price_product.service_id', $servicio->id)
                                            ->where('mo_price_product.location_id', $localizacion->id)
                                            ->whereNull('mo_price_product.session')
                                            ->get();

                                        //Se añade la tarifa general del servicio
                                        $array_disponibilidad = [
                                            "session" => null,
                                            "net_price" => isset($datos_tarifa_client_type_general_clone[0]->net_price) ? $datos_tarifa_client_type_general_clone[0]->net_price : null,
                                            "sale_price" => isset($datos_tarifa_client_type_general_clone[0]->sale_price) ? $datos_tarifa_client_type_general_clone[0]->sale_price : null,
                                            "markup" => isset($datos_tarifa_client_type_general_clone[0]->markup) ? $datos_tarifa_client_type_general_clone[0]->markup : null,
                                            "price_before" => isset($datos_tarifa_client_type_general_clone[0]->price_before) ? $datos_tarifa_client_type_general_clone[0]->price_before : null,
                                            "tip_amount" => isset($datos_tarifa_client_type_general_clone[0]->tip_amount) ? $datos_tarifa_client_type_general_clone[0]->tip_amount : null,
                                            "tip_percent" => isset($datos_tarifa_client_type_general_clone[0]->tip_percent) ? $datos_tarifa_client_type_general_clone[0]->tip_percent : null,
                                            "commission_amount" => isset($datos_tarifa_client_type_general_clone[0]->commission_amount) ? $datos_tarifa_client_type_general_clone[0]->commission_amount : null,
                                            "commission_percent" => isset($datos_tarifa_client_type_general_clone[0]->commission_percent) ? $datos_tarifa_client_type_general_clone[0]->commission_percent : null,
                                        ];

                                        if($datos_product_intercompany == '1') {
                                            foreach ($datos_companys as $company) {

                                                $array_intercompany = [
                                                    'id' => $company->id,
                                                    'name' => $company->name,
                                                ];

                                                if (isset($datos_tarifa_client_type_general_clone[0]->id)) {
                                                    $datos_price_product_intercompany = PriceProductIntercompany::where('price_product_id', $datos_tarifa_client_type_general_clone[0]->id)
                                                        ->where('company_id', $company->id)->first();

                                                    $array_intercompany['company_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->company_id : null;
                                                    $array_intercompany['amount'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->amount : null;
                                                    $array_intercompany['percent'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->percent : null;
                                                    $array_intercompany['currency_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->currency_id : null;
                                                } else {
                                                    $array_intercompany['company_id'] = null;
                                                    $array_intercompany['amount'] = null;
                                                    $array_intercompany['percent'] = null;
                                                    $array_intercompany['currency_id'] = null;
                                                }


                                                $array_disponibilidad['intercompany'][] = $array_intercompany;
                                            }
                                        }

                                        $array_availability[] = $array_disponibilidad;

                                        //Se obtienen las disponibilidades del servicio para las fechas de la tarifa
                                        $sql_availability_clone = clone $sql_availability_servicios;
                                        $datos_availability = $sql_availability_clone
                                            ->where('mo_availability.location_id', '=', $localizacion->id)
                                            ->where('mo_availability.product_id', '=', $producto->id)
                                            ->where(function ($query) use ($tarifa) {
                                                $query->where('mo_availability.date', '>=', $tarifa->date_start)
                                                    ->where('mo_availability.date', '<=', $tarifa->date_end);

                                            })
                                            ->where('mo_availability.service_id', '=', $servicio->id)
                                            ->get();

                                        //Se recorren las disponibilidades del servicio
                                        foreach ($datos_availability as $availability) {
                                            //Se obtiene la tarifa para la disponibilidad del servicio
                                            $sql_tarifa_client_type_clone = clone $sql_tarifa_client_type;
                                            $datos_tarifa_client_type = $sql_tarifa_client_type_clone->where('mo_price_product.client_type_id', $tipo_client->id)
                                                ->where('mo_price_product.product_id', $producto->id)
                                                ->where('mo_price_product.price_id', $id)
                                                ->where('mo_price_product.service_id', $servicio->id)
                                                ->where('mo_price_product.location_id', $localizacion->id)
                                                ->where('mo_price_product.session', $availability->session)
                                                ->get();

                                            //Se añaden las tarifas del servicio para la disponibilidad recorrida
                                            $availability->net_price = isset($datos_tarifa_client_type[0]->net_price) ? $datos_tarifa_client_type[0]->net_price : null;
                                            $availability->sale_price = isset($datos_tarifa_client_type[0]->sale_price) ? $datos_tarifa_client_type[0]->sale_price : null;
                                            $availability->markup = isset($datos_tarifa_client_type[0]->markup) ? $datos_tarifa_client_type[0]->markup : null;
                                            $availability->price_before = isset($datos_tarifa_client_type[0]->price_before) ? $datos_tarifa_client_type[0]->price_before : null;
                                            $availability->tip_amount = isset($datos_tarifa_client_type[0]->tip_amount) ? $datos_tarifa_client_type[0]->tip_amount : null;
                                            $availability->tip_percent = isset($datos_tarifa_client_type[0]->tip_percent) ? $datos_tarifa_client_type[0]->tip_percent : null;
                                            $availability->commission_amount = isset($datos_tarifa_client_type[0]->commission_amount) ? $datos_tarifa_client_type[0]->commission_amount : null;
                                            $availability->commission_percent = isset($datos_tarifa_client_type[0]->commission_percent) ? $datos_tarifa_client_type[0]->commission_percent : null;

                                            if($datos_product_intercompany == '1') {
                                                $availability->intercompany = array();

                                                foreach ($datos_companys as $company) {

                                                    $array_intercompany = [
                                                        'id' => $company->id,
                                                        'name' => $company->name,
                                                    ];

                                                    if (isset($datos_tarifa_client_type[0]->id)) {
                                                        $datos_price_product_intercompany = PriceProductIntercompany::where('price_product_id', $datos_tarifa_client_type[0]->id)
                                                            ->where('company_id', $company->id)->first();

                                                        $array_intercompany['company_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->company_id : null;
                                                        $array_intercompany['amount'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->amount : null;
                                                        $array_intercompany['percent'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->percent : null;
                                                        $array_intercompany['currency_id'] = $datos_price_product_intercompany ? $datos_price_product_intercompany->currency_id : null;
                                                    } else {
                                                        $array_intercompany['company_id'] = null;
                                                        $array_intercompany['amount'] = null;
                                                        $array_intercompany['percent'] = null;
                                                        $array_intercompany['currency_id'] = null;
                                                    }


                                                    $availability->intercompany[] = $array_intercompany;
                                                }
                                            }

                                            $array_availability[] = $availability;
                                        }
                                        $servicios_array['price'] = $array_availability;

                                        $array_tipo_cliente['service'][] = $servicios_array;
                                    }
                                }
                                $localizacion_array['client_type'][] = $array_tipo_cliente;
                            }
                            $producto->location[] = $localizacion_array;
                        }
                    }


                    $array_type['product'] = $datos_productos_clone;
                    $array['data'][1]['type'][] = $array_type;
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tarifas');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función que muestra las tarifas asociadas a un modelo de contrato
     *
     * Muestra las tarifas asociadas a un modelo de contrato concreto. Se requiere un parámetro contract_model_id para consultar sus tarifas asociadas, y adicionalmente se le puede enviar un parámetro date como filtro para una fecha determinada.
     * Para la consulta de tarifas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'contract_model_id' => 'integer|min:0',
                'date' => 'date|date_format:"Y-m-d"',
                'previous_price' => 'boolean',
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $array['data'] = array();
                $sql_tarifa = DB::connection('tenant')->table('mo_price')
                    ->select('mo_price.id', 'mo_price.contract_model_id', 'mo_price.currency_id', 'mo_price.promotions', 'mo_price.date_start', 'mo_price.date_end', 'mo_price.user_id')
                    ->whereNull('mo_price.deleted_at')
                    ->join('mo_contract_model', 'mo_contract_model.id', '=', 'mo_price.contract_model_id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_contract_model_translation', 'mo_contract_model_translation.contract_model_id', 'mo_contract_model.id')
                    ->whereNull('mo_contract_model_translation.deleted_at');

                $sub = DB::connection('tenant')->table('mo_price')
                    ->select('mo_price.id')
                    ->whereNull('mo_price.deleted_at')
                    ->join('mo_contract_model', 'mo_contract_model.id', '=', 'mo_price.contract_model_id')
                    ->whereNull('mo_contract_model.deleted_at')
                    ->join('mo_contract_model_translation', 'mo_contract_model_translation.contract_model_id', 'mo_contract_model.id')
                    ->whereNull('mo_contract_model_translation.deleted_at');

                if ($request->get('contract_model_id') != '') {
                    $sql_tarifa->where('mo_price.contract_model_id', $request->get('contract_model_id'));
                    $sub->where('mo_price.contract_model_id', $request->get('contract_model_id'));
                }

                // Por defecto solo se mostrarán las tarifas vigentes
                if ($request->get('previous_price') != 1) {
                    $fecha_actual = Carbon::now()->format('Y-m-d');
                    $sql_tarifa->where('mo_price.date_end', '>=', $fecha_actual);
                    $sub->where('mo_price.date_end', '>=', $fecha_actual);
                }

                if ($request->get('date') != '') {
                    $fecha = Carbon::createFromFormat('Y-m-d', $request->get('date'));
                    $sql_tarifa->where('mo_price.date_start', '<=', $fecha->format('Y-m-d'))
                        ->where('mo_price.date_end', '>=', $fecha->format('Y-m-d'));
                    $sub->where('mo_price.date_start', '<=', $fecha->format('Y-m-d'))
                        ->where('mo_price.date_end', '>=', $fecha->format('Y-m-d'));
                }

                $orden = 'mo_price.date_start';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_price.id';
                            break;
                        case 'name':
                            $orden = 'mo_contract_model_translation.name';
                            break;
                        case 'date_start':
                            $orden = 'mo_price.date_start';
                            break;
                        case 'date_end':
                            $orden = 'mo_price.date_end';
                            break;
                        default:
                            $orden = 'mo_price.id';
                            break;
                    }
                }

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_tarifa->groupBy('mo_price.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_tarifa = $sql_tarifa->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {
                    $datos_tarifa = $sql_tarifa->get();
                }

                //Fin de paginación

                $sub->groupBy('mo_price.id');
                $tarifa_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $tarifa_count->count();

                $idiomas = Language::where('front',1)->get();
                $array_price = array();
                foreach ($datos_tarifa as $tarifa) {

                    $currency = Currency::where('id', '=', $tarifa->currency_id)->first(['id', 'presentation_simbol']);
                    $array_currency = array();
                    if ($currency) {
                        $array_currency = [
                            'id' => $currency->id,
                            'iso_code' => $currency->presentation_simbol,
                        ];
                    }
                    $array_tarifa = [
                        'id' => $tarifa->id,
                        'currency' => $array_currency,
                        'promotions' => $tarifa->promotions,
                        'date_start' => $tarifa->date_start,
                        'date_end' => $tarifa->date_end,
                        'user_id' => $tarifa->user_id,
                    ];
                    $array_tarifa["contract_model"]["id"] = $tarifa->contract_model_id;
                    foreach ($idiomas as $idioma) {
                        $traduccion = ContractTranslation::where('contract_model_id', $tarifa->contract_model_id)->where('language_id', $idioma->id)->first(['id', 'language_id', 'name']);
                        if ($traduccion) {
                            $array_tarifa["contract_model"]["lang"][][$idioma->abbreviation] = $traduccion;
                        }
                    }
                    $array_price[] = $array_tarifa;

                }
                if (count($array_price) > 0) {
                    $array['data'][]['price'] = $array_price;
                }

                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Tarifas');
        }
        return response()->json($array, $array['error']);

    }
}