<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class GeneralController extends Controller
{

    /**
     * Función para devolución mensaje de ruta raíz
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['error' => 0, 'data' => 'Version 1.0.0']);
    }

    /**
     * Función para validación de token a través del middleware de Autorization
     *
     * @return \Illuminate\Http\Response
     */
    public function valid_token()
    {
        return response()->json(['error' => 200]);
    }

    /**
     * Función para peticiones comunes en login de CP
     *
     * Para la consulta de categorías se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cpcommon(Request $request)
    {
        $array['error'] = 200;

        try {

            //Petición monedas
            $controller = app(\App\Http\Controllers\tenant\CurrencyController::class);
            $peticion = new Request();
            $peticion->setMethod('GET');
            $peticion->request->add($request->all());
            $res_currencies = $controller->show($peticion);

            //Petición idiomas
            $controller = app(\App\Http\Controllers\tenant\LanguageController::class);
            $peticion = new Request();
            $peticion->setMethod('GET');
            $peticion->request->add($request->all());
            $res_languages = $controller->show($peticion);

            //Petición subcanales
            $controller = app(\App\Http\Controllers\tenant\SubChannelController::class);
            $peticion = new Request();
            $peticion->setMethod('GET');
            $peticion->request->add($request->all());
            $res_subchannels = $controller->show($peticion);

            //Petición settings
            $controller = app(\App\Http\Controllers\tenant\SettingsController::class);
            $peticion = new Request();
            $peticion->setMethod('GET');
            $peticion->request->add($request->all());
            $res_settings = $controller->show($peticion);

            //Petición locations
            $controller = app(\App\Http\Controllers\tenant\LocationController::class);
            $peticion = new Request();
            $peticion->setMethod('GET');
            $peticion->request->add($request->all());
            $res_locations = $controller->show($peticion);
            

            if($res_currencies->getStatusCode() == 200 && $res_languages->getStatusCode() == 200 
                && $res_subchannels->getStatusCode() == 200 && $res_settings->getStatusCode() == 200 
                && $res_locations->getStatusCode() == 200){
                
                $array['data'] = array();
                $array['data'][0]['currencies'] =  $res_currencies->original;
                $array['data'][0]['languages'] =  $res_languages->original;
                $array['data'][0]['subchannels'] =  $res_subchannels->original;
                $array['data'][0]['settings'] =  $res_settings->original;
                $array['data'][0]['locations'] =  $res_locations->original;
                
                // PLAN
                $array['data'][0]['current_plan'] = null;
                $website  = \Hyn\Tenancy\Facades\TenancyFacade::website();
                
                dd($website);

                $website = DB::table('saas_website')
                ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                'saas_website.default_language_id', 'saas_website.default_currency_id')
                ->whereNull('saas_website.deleted_at')
                ->where('saas_website.website_id', $website->id)
                ->first();

                
                
                $plans = websitePlanRules($website);
                foreach($plans as $plan) {
                    if($plan->plan_id != null){
                        $array['data'][0]['current_plan'] = $plan; 
                    }
                }
                //Fin plan

                // RUTAS (Precio y redireccion para actualizar plan)
                $protocol = DB::table('saas_settings')
                ->where('saas_settings.name', 'protocol')->first()->value;
                $base_path_saas = DB::table('saas_settings')
                        ->where('saas_settings.name', 'base_path_saas')->first()->value;
                $array['data'][0]['redirect_prices_route'] = $protocol . $base_path_saas . '/es/precios';
                $array['data'][0]['redirect_plan_route'] = $protocol . $base_path_saas . '/login?website_id=' . $website->id;
                $array['data'][0]['redirect_contact_route'] = $protocol . $base_path_saas . '/es/contacto';

            } else{

                $array['error'] = 500;
                $array['error_description'] = 'Internal system error';

            }

        } catch (\Exception $e) {
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'CP - commonrequests');
        }

        return response()->json($array, $array['error']);
    }

}
