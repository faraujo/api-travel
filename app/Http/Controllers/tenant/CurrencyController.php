<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Currency;
use App\Models\tenant\CurrencyExchange;
use App\Models\tenant\UserToken;
use App\Exceptions\Handler;
use App\Models\tenant\Exchange;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Models\tenant\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class CurrencyController extends Controller
{


    /**
     * Función para mostrar las monedas.
     *
     * Para la consulta de monedas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'limit' => 'integer|min:0',
                'page' => 'integer|min:0',
                'front' => 'boolean',
                'exchange' => 'boolean'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $monedas = DB::connection('tenant')->table('mo_currency')
                    ->select('mo_currency.id', 'mo_currency.name', 'mo_currency.simbol', 'mo_currency.iso_number', 'mo_currency.iso_code', 'mo_currency.presentation_simbol', 'mo_currency.position', 'mo_currency.decimal_separator', 'mo_currency.thousand_separator', 'mo_currency.front')
                    ->where('mo_currency.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_currency')
                    ->select('mo_currency.id')
                    ->where('mo_currency.deleted_at', '=', null);

                if ($request->get('front') != '') {
                    $monedas = $monedas->where('front', '=', $request->get('front'));
                    $sub = $sub->where('front', '=', $request->get('front'));
                }

                $id_moneda_principal = Settings::where('name', '=', 'moneda_defecto_id')->first();

                $moneda_principal = Currency::where('id', '=', $id_moneda_principal->value)->first(['id', 'name', 'simbol', 'iso_number', 'iso_code', 'presentation_simbol',
                    'position', 'decimal_separator', 'thousand_separator', 'front']);


                // Order
                $orden = 'name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'id';
                            break;
                        case 'name':
                            $orden = 'name';
                            break;
                        default:
                            $orden = 'name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $monedas = $monedas->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $monedas = $monedas->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $monedas = $monedas->get();

                }

                //Fin de paginación


                $monedas_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $monedas_count->count();

                $array['data'] = array();

                if($request->get('exchange') == 1) {
                        foreach ($monedas as $moneda) {
                            $cambio = CurrencyExchange::where('currency_id',$moneda->id)->whereNull('deleted_at')->orderBy('date_time_start','desc')->limit(1)->get();
                            $moneda->exchange = $cambio;
                        }
                }

                if ($totales > 0) {
                    $array['data'][0]['currency'] = $monedas;
                    $array['data'][0]['main_currency'][] = $moneda_principal;
                }

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Monedas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar los cambios de valores de las monedas.
     *
     * Para la consulta de cambios de valores de las monedas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showExchange(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'currency_id' => 'integer|min:0|required_with:date_time',
                'user_id' => 'integer|min:0',
                'date_time' => 'date|date_format:"Y-m-d H:i:s"',
                'limit' => 'integer|min:0',
                'page' => 'integer|min:0',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $exchanges = DB::connection('tenant')->table('mo_currency_exchange')
                    ->select('mo_currency.id', 'mo_currency.name', 'mo_currency.simbol', 'mo_currency.iso_number', 'mo_currency.presentation_simbol', 'mo_currency.position', 'mo_currency_exchange.id as exchange_id', 'mo_currency_exchange.user_id', 'mo_currency_exchange.date_time_start', 'mo_currency_exchange.exchange')
                    ->whereNull('mo_currency_exchange.deleted_at')
                    ->join('mo_currency', 'mo_currency_exchange.currency_id', 'mo_currency.id')
                    ->whereNull('mo_currency.deleted_at');
                   

                $sub = DB::connection('tenant')->table('mo_currency_exchange')
                    ->select('mo_currency_exchange.id')
                    ->whereNull('mo_currency_exchange.deleted_at')
                    ->join('mo_currency', 'mo_currency_exchange.currency_id', 'mo_currency.id')
                    ->whereNull('mo_currency.deleted_at');
                 

                //filtros
                if ($request->get('currency_id') != '') {
                    $exchanges->where('mo_currency_exchange.currency_id', '=', $request->get('currency_id'));
                    $sub->where('mo_currency_exchange.currency_id', '=', $request->get('currency_id'));
                }

                if ($request->get('user_id') != '') {
                    $exchanges->where('mo_currency_exchange.user_id', '=', $request->get('user_id'));
                    $sub->where('mo_currency_exchange.user_id', '=', $request->get('user_id'));
                }

                if ($request->get('date_time') != '') {
                    $exchanges->where('date_time_start', '<=', $request->get('date_time'));
                    $sub->where('date_time_start', '<=', $request->get('date_time'));
                }
                //fin filtros

                // Order
                $orden = 'currency_id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'id';
                            break;
                        case 'currency_id':
                            $orden = 'currency_id';
                            break;
                        case 'date_time_start':
                            $orden = 'date_time_start';
                            break;
                        case 'exchange':
                            $orden = 'exchange';
                            break;
                        default:
                            $orden = 'currency_id';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $exchanges = $exchanges->orderBy($orden, $sentido);


                $exchanges_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $exchanges_count->count();

                //si se envía filtro fecha va a devolver el registro que coincida con el campo enviado o el registro con la fecha inmediatamente anterior, sólo uno
                if ($request->get('date_time') != '') {
                    $exchanges = $exchanges->orderBy('date_time_start', 'desc')->limit(1)->get();
                    if ($exchanges) {
                        $totales = 1;
                    }
                } else {

                    // Paginación según filtros y ejecución de la consulta
                    if ($request->get('limit') != '0') {

                        $settings = Settings::where('name', '=', 'limit_registers')->first();
                        $limite = $settings->value;

                        if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                            $limite = $request->get('limit');
                        }

                        $inicio = 0;
                        if ($request->get('page') != '') {
                            $inicio = $request->get('page');
                        }

                        $exchanges = $exchanges->forPage($inicio, $limite)->get();

                        //si filtro limit = 0 se obtienen todos los resultados
                    } else {

                        $exchanges = $exchanges->get();

                    }

                    //Fin de paginación
                }

                $array['data'] = array();

                foreach ($exchanges as $exchange) {
                    $datos_exchange = [
                        'id' => $exchange->id,
                        'name' => $exchange->name,
                        'simbol' => $exchange->simbol,
                        'iso_number' => $exchange->iso_number,
                        'presentation_simbol' => $exchange->presentation_simbol,
                        'position' => $exchange->position,
                        'exchange' => array(
                            'id' => $exchange->exchange_id,
                            'user_id' => $exchange->user_id,
                            'date_time_start' => $exchange->date_time_start,
                            'exchange' => $exchange->exchange,
                        )
                    ];

                    $array['data'][0]['currency'][] = $datos_exchange;
                }


                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Monedas');
        }

        return response()->json($array, $array['error']);

    }


    /**
     * Función para la creación de una conversión de valor de moneda
     *
     * Para la creación de registros de conversión de moneda se realiza una petición POST. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createExchange(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();

            $validator = Validator::make($request->all(), [
                'currency_id' => 'required|integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',
                'exchange' => 'numeric|min:0|required',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $user_id = '';
            if ($request->header('Authorization')) {
                $token = explode(' ', $request->header('Authorization'));
                $user_token = UserToken::where('token', '=', $token[1])
                    ->where('expired_at', '>=', Carbon::now())
                    ->first();

                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }

            } else { // Si no está logueado
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
            }
            //Fin de validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $date_time_start = Carbon::now()->format('Y-m-d H:i:s');
                $exchange = Exchange::where('currency_id', $request->get('currency_id'))->where('date_time_start', $date_time_start)->first();

                //si hay un registro con esa moneda en la misma fecha y hora lo elimina
                if ($exchange) {
                    $exchange->delete();
                }

                Exchange::create([
                    'currency_id' => $request->get('currency_id'),
                    'date_time_start' => $date_time_start,
                    'exchange' => $request->get('exchange'),
                    'user_id' => $user_id,
                ]);

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Monedas');
        }
        return response()->json($array, $array['error']);
    }

}