<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\Settings;
use validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DeviceController extends Controller
{
    /**
     * Función que lista todas los dispositivos
     *
     * Para la consulta de dispositivos se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDevices(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'integer|min:0',
                'limit' => 'integer|min:0',
                'active' => 'boolean',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $dispositivos = DB::connection('tenant')->table('mo_device')
                    ->select('mo_device.id', 'mo_device.name', 'mo_device.code','mo_device.active')
                    ->where('mo_device.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_device')
                    ->select('mo_device.id', 'mo_device.name', 'mo_device.code','mo_device.active')
                    ->where('mo_device.deleted_at', '=', null);

                //Fin precompiladas

                //Filtros de busqueda


                if($request->get('active') != ''){
                    $dispositivos->where('mo_device.active', $request->get('active'));
                    $sub->where('mo_device.active', $request->get('active'));
                }


                //Fin de filtros

                // Order
                $orden = 'mo_device.id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'mo_device.id';
                            break;
                        case 'name':
                            $orden = 'mo_device.name';
                            break;
                        default:
                            $orden = 'mo_device.id';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $dispositivos = $dispositivos->groupBy('mo_device.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $dispositivos = $dispositivos->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $dispositivos = $dispositivos->get();

                }

                //Fin de paginación

                $devices_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $devices_count->count();


                $array['data'] = array();


                //Se recorren las campañas
                foreach ($dispositivos as $dispositivo) {

                    $array['data'][0]['device'][] = $dispositivo;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Dispositivos');
        }

        return response()->json($array, $array['error']);
    }

}