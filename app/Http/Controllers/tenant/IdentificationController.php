<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Identification;
use App\Models\tenant\IdentificationTranslation;
use App\Models\tenant\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use App\Exceptions\Handler;

class IdentificationController extends Identification
{

    /**
     * Muestra los identificadores
     *
     * Para la consulta de identificadores se realiza una petición GET. Si la operación no produce errores se devuelve,
     * en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showIdentifications(Request $request)
    {
        $array['error'] = 200;
        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $identifications = DB::connection('tenant')->table('mo_identification')
                    ->select('mo_identification.id')
                    ->where('mo_identification.deleted_at', '=', null)
                    ->join('mo_identification_translation', 'mo_identification_translation.identification_id', '=', 'mo_identification.id')
                    ->where('mo_identification_translation.deleted_at', '=', null)
                    ->join('mo_language', 'mo_language.id', '=', 'mo_identification_translation.language_id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_identification_translation.identification_id');


                $sub = DB::connection('tenant')->table('mo_identification')
                    ->select('mo_identification.id')
                    ->where('mo_identification.deleted_at', '=', null)
                    ->join('mo_identification_translation', 'mo_identification_translation.identification_id', '=', 'mo_identification.id')
                    ->where('mo_identification_translation.deleted_at', '=', null)
                    ->join('mo_language', 'mo_language.id', '=', 'mo_identification_translation.language_id')
                    ->where('mo_language.deleted_at', '=', null)->groupBy('mo_identification_translation.identification_id');

                if ($request->get('lang') != '') {
                    $identifications->where('mo_identification_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_identification_translation.language_id', '=', $idioma->id);
                }

                $identifications_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $identifications_count->count();

                $identifications = $identifications->get();

                $array['data'] = array();
                foreach ($identifications as $identification) {

                    foreach ($idiomas as $idi) {
                        $traduccion = IdentificationTranslation::where('identification_id', '=', $identification->id)
                            ->select('language_id', 'name', 'description')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $identification->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['identification'][] = $identification;
                }

                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Identificaciones');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Muestra un idenficador buscado por su id
     *
     * Para la consulta de identificadores se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function showIdentificationId($id, Request $request)
    {
        $array['error'] = 200;


        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $error = 0;

            $mensaje_validador = collect();

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }


            if ($error == 0) {
                $idiomas = Language::where('front',1)->get();

                $identification = DB::connection('tenant')->table('mo_identification')
                    ->select('mo_identification.id')
                    ->join('mo_identification_translation', 'mo_identification.id', 'mo_identification_translation.identification_id')
                    ->where('mo_identification_translation.deleted_at', '=', null)
                    ->where('mo_identification.deleted_at', '=', null)
                    ->where('mo_identification.id', '=', $id);

                if ($request->get('lang') != '') {
                    $identification->where('mo_identification_translation.language_id', '=', $idioma->id);
                }

                $identifications = $identification->groupBy('mo_identification_translation.identification_id')->get();

                $array['data'] = array();

                foreach ($identifications as $identification) {
                    if ($request->get('lang') != '') {
                        $traducciones = IdentificationTranslation::where('language_id', '=', $idioma->id)->where('identification_id', '=', $identification->id)
                            ->select('id', 'language_id', 'name', 'description')
                            ->get();
                        foreach ($traducciones as $traduccion) {
                            $identification->lang[] = [Str::upper($request->get('lang')) => $traduccion];
                        }
                    } else {
                        foreach ($idiomas as $idi) {
                            $traduccion = IdentificationTranslation::where('identification_id', '=', $identification->id)
                                ->select('id', 'language_id', 'name', 'description')
                                ->where('language_id', '=', $idi->id)
                                ->get();
                            foreach ($traduccion as $trad) {
                                $identification->lang[][$idi->abbreviation] = $trad;
                            }
                        }
                    }
                    $array['data'][0]['identification'][] = $identification;
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][]['lang'][] = 'The lang is not exists';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Identificaciones');
        }

        return response()->json($array, $array['error']);
    }

}