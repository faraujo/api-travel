<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\CommentStatus;
use App\Models\tenant\CommentStatusTranslation;
use App\Exceptions\Handler;
use App\Models\tenant\Subchannel;
use App\Models\tenant\ProductTranslation;
use App\Models\tenant\Settings;
use App\Models\tenant\Language;
use App\Models\tenant\Product;
use App\Models\tenant\Comment;
use App\Models\tenant\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\UserToken;
use Illuminate\Support\Facades\Storage;


class CommentController extends Controller
{

    /**
     * Función para la creación de un comentario sobre un producto.
     *
     * Para la creación de un comment se realiza una petición POST.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;

            // Validación
            $mensaje_validador = collect();
            $user_id = null;

            $validator = Validator::make($request->all(), [
                'rating' => 'numeric|required|between:0,5',
                'user_id' => 'numeric',
                'product_id' => 'numeric',
                'subchannel_id' => 'numeric|required',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Validadores exclusivos de "/comment".
            if(strpos($request -> path(), 'search') !== false){
                
                if ($request->header('Authorization')) {
                    $token = explode(' ', $request->header('Authorization'));
                    $user_token = UserToken::where('token', '=', $token[1])
                        ->where('expired_at', '>=', Carbon::now())
                        ->first();
    
                    if ($user_token) {
                        $user_id = $user_token->user_id;
                    } else {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is not valid']]);
                    }
    
                } else { // Si no está logueado
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['Authorization' => ['The authorization token is required']]);
                }
            } else {

                if ($request->get('user_id') != '') {
                    $user = User::find($request->get('user_id'));
                    if (!$user) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['user_id' => ['0' => 'The user is not exists']]);
                    } else {
                        $user_id = $user->id;
                    }
                }
            }

            

            if ($request->get('lang') != '') {
                $idioma = Language::where('abbreviation', '=', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['0' => 'The lang is not exists']]);
                }
            }

            if ($request->get('product_id') != '') {
                $producto = Product::find($request->get('product_id'));
                if (!$producto) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['product_id' => ['0' => 'The product is not exists']]);
                }
            }


            if ($request->get('subchannel_id') != '') {
                $canal = Subchannel::find($request->get('subchannel_id'));
                if (!$canal) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['subchannel_id' => ['0' => 'The subchannel is not exists']]);
                }
            }
            // FIN Validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                Comment::create([
                    'text' => $request->get('text') != '' ? $request->get('text') : null,
                    'language_id' => $request->get('lang') != '' ? $idioma->id : null,
                    'user_id' => $user_id,
                    'subchannel_id' => $request->get('subchannel_id'),
                    'rating' => $request->get('rating'),
                    'product_id' => $request->get('product_id') != '' ? $request->get('product_id') : null,
                    'comment_status_id' => 1,
                ]);
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Comentarios');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para el borrado de comentarios.
     *
     * Para la eliminación de uno o varios comment se realiza una petición DELETE.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación


            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_comment,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $comment_id) {
                        $comment = Comment::where('id', $comment_id)->delete();

                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Comentarios');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la validación de comentarios.
     *
     * Para la validacion de un comment se realiza una petición PUT. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validated(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            // Validación de campos
            $validator = Validator::make($request->all(), [
                'id' => 'numeric|required',
                'comment_status_id' => 'integer|exists:tenant.mo_comment_status,id,deleted_at,NULL|required',
                'updated_by' => 'numeric|required',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $usuario = User::find($request->get('updated_by'));
            if (!$usuario) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['updated_by' => ['The user_id is not exists']]);
            }

            $estado = CommentStatus::find($request->get('comment_status_id'));
            if (!$estado) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['comment_status_id' => ['The comment_status_id is not exists']]);
            }
            //Fin de la validacion

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $comment = Comment::find($request->get('id'));
                if (!$comment) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
                    $now = new \DateTime();
                    $comment->update([
                        'updated_by' => $usuario->id,
                        'updated_when' => $now->format('Y-m-d H:i:s'),
                        'comment_status_id' => $estado->id]);
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Comentarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar los comentarios.
     *
     * Para la consulta de comments se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            // Validadores comunes a "/comment/search" y "/comment"
            $validator = Validator::make($request->all(), [
                'subchannel_id' => 'numeric',
                'page' => 'numeric',
                'limit' => 'numeric',
                'subchannel_all' => 'boolean|integer|min:0'
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // Validadores exclusivos de "/comment".
            if(strpos($request -> path(), 'search') === false){
                $validator = Validator::make($request->all(), [
                    'product_id' => 'numeric',
                    'user_id' => 'numeric',
                    'rating' => 'numeric|between:0,5',
                    'validated' => 'integer|exists:tenant.mo_comment_status,id,deleted_at,NULL',
                    'updated_by' => 'numeric',
                    'created_at' => 'date|date_format:"Y-m-d"',
                ]);
            }else{
                // Validadores exclusivos de "/comment/search".
                $validator = Validator::make($request->all(), [
                    'product_id' => 'required|numeric',
                ]);
            }

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = Language::where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }
            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $comments = DB::connection('tenant')->table('mo_comment')
                    ->select('mo_comment.id', 'mo_comment.product_id', 'mo_comment.language_id', 'mo_comment.subchannel_id', 'mo_comment.text', 'mo_comment.rating', 'mo_comment.updated_by', 'mo_comment.comment_status_id', 'mo_comment.updated_when', 'mo_comment.created_at', 'mo_user.id as user_id', 'mo_user.email', 'mo_user.name', 'mo_user.surname', 'mo_user.image_id','mo_user_updated.id as user_updated_id', 'mo_user_updated.email as user_updated_email', 'mo_user_updated.name as user_updated_name', 'mo_user_updated.surname as user_updated_surname')
                    ->leftJoin('mo_user', function ($join) {
                        $join->on('mo_comment.user_id', 'mo_user.id')
                            ->where('mo_user.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user as mo_user_updated', function ($join) {
                        $join->on('mo_comment.updated_by', 'mo_user_updated.id')
                            ->where('mo_user_updated.deleted_at', '=', null);
                    })
                    ->where('mo_comment.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_comment')
                    ->select('mo_comment.id')
                    ->leftJoin('mo_user', function ($join) {
                        $join->on('mo_comment.user_id', 'mo_user.id')
                            ->where('mo_user.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user as mo_user_updated', function ($join) {
                        $join->on('mo_comment.updated_by', 'mo_user_updated.id')
                            ->where('mo_user_updated.deleted_at', '=', null);
                    })
                    ->where('mo_comment.deleted_at', '=', null);

                $opiniones_clasificadas = DB::connection('tenant')->table('mo_comment')
                    ->select(DB::connection('tenant')->raw('count(*) as comments,mo_comment.rating'))
                    ->leftJoin('mo_user', function ($join) {
                        $join->on('mo_comment.user_id', 'mo_user.id')
                            ->where('mo_user.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user as mo_user_updated', function ($join) {
                        $join->on('mo_comment.updated_by', 'mo_user_updated.id')
                            ->where('mo_user_updated.deleted_at', '=', null);
                    })
                    ->where('mo_comment.deleted_at', '=', null);

                $sql_image_user = DB::connection('tenant')->table('mo_file')
                    ->select(
                        'mo_file.id',
                        'mo_file.url_file'
                    )
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_file_translation','mo_file_translation.file_id','mo_file.id')
                    ->whereNull('mo_file.deleted_at')
                    ->join('mo_user','mo_user.image_id','mo_file.id')
                    ->whereNull('mo_user.deleted_at')
                    ->groupBy('mo_file.id');

                $sql_image_user_translate = DB::connection('tenant')->table('mo_file_translation')
                    ->whereNull('mo_file_translation.deleted_at');

                if ($request->get('product_id') != '') {
                    $comments->where('mo_comment.product_id', $request->get('product_id'));
                    $sub->where('mo_comment.product_id', $request->get('product_id'));
                    $opiniones_clasificadas->where('mo_comment.product_id', $request->get('product_id'));
                }

                if ($request->get('created_at') != '') {
                    $created_at_start = Carbon::parse($request->get('created_at'))->format('Y-m-d H:i:s');
                    $created_at_end = Carbon::parse($created_at_start)->addDay(1);
                    $comments->where('mo_comment.created_at', '>=', $created_at_start);
                    $comments->where('mo_comment.created_at', '<', $created_at_end);
                    $sub->where('mo_comment.created_at', '>=', $created_at_start);
                    $sub->where('mo_comment.created_at', '<', $created_at_end);
                    $opiniones_clasificadas->where('mo_comment.created_at', '>=', $created_at_start);
                    $opiniones_clasificadas->where('mo_comment.created_at', '<', $created_at_end);
                }

                if ($request->get('user_id') != '') {
                    $comments->where('user_id', $request->get('user_id'));
                    $sub->where('user_id', $request->get('user_id'));
                    $opiniones_clasificadas->where('user_id', $request->get('user_id'));
                }

                if($request->get('subchannel_all') == '' || $request->get('subchannel_all') == '0') {
                    if ($request->get('subchannel_id') != '') {
                        $comments->where('mo_comment.subchannel_id', $request->get('subchannel_id'));
                        $sub->where('mo_comment.subchannel_id', $request->get('subchannel_id'));
                        $opiniones_clasificadas->where('mo_comment.subchannel_id', $request->get('subchannel_id'));
                    }
                }

                if ($request->get('validated') != '') {
                    $comments->where('mo_comment.comment_status_id', $request->get('validated'));
                    $sub->where('mo_comment.comment_status_id', $request->get('validated'));
                    $opiniones_clasificadas->where('mo_comment.comment_status_id', $request->get('validated'));
                }

                if ($request->get('updated_by') != '') {
                    $comments->where('mo_comment.updated_by', '=', $request->get('updated_by'));
                    $sub->where('mo_comment.updated_by', '=', $request->get('updated_by'));
                    $opiniones_clasificadas->where('mo_comment.updated_by', '=', $request->get('updated_by'));
                }
                if ($request->get('lang') != '') {
                    $comments->where('mo_comment.language_id', $idioma->id);
                    $sub->where('mo_comment.language_id', $idioma->id);
                    $opiniones_clasificadas->where('mo_comment.language_id', $idioma->id);
                }

                if ($request->get('rating') != '') {
                    $comments->where('mo_comment.rating', $request->get('rating'));
                    $sub->where('mo_comment.rating', $request->get('rating'));
                    $opiniones_clasificadas->where('mo_comment.rating', $request->get('rating'));
                }

                if ($request->get('text') != '') {
                    $comments->where("mo_comment.text", "like", "%" . $request->get('text') . "%");
                    $sub->where("mo_comment.text", "like", "%" . $request->get('text') . "%");
                    $opiniones_clasificadas->where("mo_comment.text", "like", "%" . $request->get('text') . "%");
                }

                if ($request->get('name') != '') {

                    $comments->Where(function ($query) use ($request) {
                        $query->where('mo_user.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_user.email', 'Like', '%' . $request->get('name') . '%');
                    });

                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_user.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_user.email', 'Like', '%' . $request->get('name') . '%');
                    });

                    $opiniones_clasificadas->Where(function ($query) use ($request) {
                        $query->where('mo_user.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_user.email', 'Like', '%' . $request->get('name') . '%');
                    });

                }

                // Filtro en caso de searchcomment, solo muestra opiniones validadas
                if(strpos($request -> path(), 'search') !== false){
                    $comments->where('mo_comment.comment_status_id', 2);
                    $sub->where('mo_comment.comment_status_id', 2);
                    $opiniones_clasificadas->where('mo_comment.comment_status_id', 2);
                }

                // Order
                $orden = 'product_id';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'product_id':
                            $orden = 'product_id';
                            break;
                        case 'email':
                            $orden = 'email';
                            break;
                        case 'created_at':
                            $orden = 'created_at';
                            break;
                        case 'language_id':
                            $orden = 'language_id';
                            break;
                        case 'user_id':
                            $orden = 'user_id';
                            break;
                        case 'subchannel_id':
                            $orden = 'subchannel_id';
                            break;
                        case 'rating':
                            $orden = 'rating';
                            break;
                        case 'id':
                            $orden = 'id';
                            break;
                        case 'updated_by':
                            $orden = 'updated_by';
                            break;
                        case 'updated_when':
                            $orden = 'updated_when';
                            break;
                        default:
                            $orden = 'product_id';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $comments->groupBy('mo_comment.id')->orderBy($orden,$sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $comments = $comments->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $comments = $comments->get();

                }

                //Fin de paginación

                $sub->groupBy('mo_comment.id');

                $comments_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $comments_count->count();

                $media_rating = $opiniones_clasificadas->avg('rating');
                $opiniones_clasificadas = $opiniones_clasificadas->groupBy('rating')->get();


                $array['data'] = array();

                foreach ($comments as $comment) {
                    $comment->user = null;
                    $comment->updated_by = null;
                    $comment->product = null;
                    $comment->comment_status = null;

                    foreach ($idiomas as $idioma) {

                        $datos_estado_traduccion = DB::connection('tenant')->table('mo_comment_status')
                            ->select('mo_comment_status_translation.language_id', 'mo_comment_status_translation.name')
                            ->whereNull('mo_comment_status.deleted_at')
                            ->where('mo_comment_status.id', '=', $comment->comment_status_id)
                            ->join('mo_comment_status_translation', 'mo_comment_status.id', '=', 'mo_comment_status_translation.comment_status_id')
                            ->whereNull('mo_comment_status_translation.deleted_at')
                            ->where('mo_comment_status_translation.language_id', '=', $idioma->id)
                            ->get();

                        foreach ($datos_estado_traduccion as $estado_traduccion) {
                            $comment->comment_status['id'] = $comment->comment_status_id;
                            $comment->comment_status['lang'][][$idioma->abbreviation] = $estado_traduccion;
                        }

                        $datos_producto_traduccion = DB::connection('tenant')->table('mo_product')
                            ->select('mo_product_translation.language_id', 'mo_product_translation.name')
                            ->whereNull('mo_product.deleted_at')
                            ->where('mo_product.id', '=', $comment->product_id)
                            ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                            ->whereNull('mo_product_translation.deleted_at')
                            ->where('mo_product_translation.language_id', '=', $idioma->id)
                            ->get();

                        foreach ($datos_producto_traduccion as $producto_traduccion) {
                            $comment->product['id'] = $comment->product_id;
                            $comment->product['lang'][][$idioma->abbreviation] = $producto_traduccion;
                        }

                    }

                    if ($comment->user_id != '') {
                        $sql_image_user_clone = clone $sql_image_user;

                        $datos_image_user = $sql_image_user_clone->where('mo_file.id',$comment->image_id)
                            ->first();

                        $comment->user['id'] = $comment->user_id;
                        $comment->user['email'] = $comment->email;
                        $comment->user['name'] = $comment->name;
                        $comment->user['surname'] = $comment->surname;
                        // $comment->user['image_id'] = $comment->image_id;

                        $file = array();

                        if($datos_image_user) {

                            $array_traduccion = array();

                            if($request->get('lang') != '') {
                                $idioma = Language::where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                                
                                $sql_image_user_translate_clone = clone $sql_image_user_translate;
                                $datos_image_user_translate = $sql_image_user_translate_clone
                                ->where('mo_file_translation.file_id', $datos_image_user->id)
                                ->where('mo_file_translation.language_id', $idioma->id)
                                ->first();

                                if($datos_image_user_translate) {
                                    $array_traduccion[][$idioma->abbreviation] = [
                                        'id' => $datos_image_user_translate->id,
                                        'language_id' => $datos_image_user_translate->language_id,
                                        'name' => $datos_image_user_translate->name,
                                        'alternative_text' => $datos_image_user_translate->alternative_text,
                                    ];
                                }
                            }else {
                                foreach ($idiomas as $language) {
                                    $sql_image_user_translate_clone = clone $sql_image_user_translate;

                                    $datos_image_user_translate = $sql_image_user_translate_clone
                                    ->where('mo_file_translation.file_id', $datos_image_user->id)
                                    ->where('mo_file_translation.language_id', $language->id)
                                    ->first();

                                    if($datos_image_user_translate) {
                                        $array_traduccion[][$language->abbreviation] = [
                                            'id' => $datos_image_user_translate->id,
                                            'language_id' => $datos_image_user_translate->language_id,
                                            'name' => $datos_image_user_translate->name,
                                            'alternative_text' => $datos_image_user_translate->alternative_text,
                                        ];
                                    }
                                }
                            }

                            

                            $file = [
                                'id' => $datos_image_user->id,
                                'url_file' => Storage::url($datos_image_user->url_file),
                                // 'file_type_id' => $datos_image_user->file_type,
                                'lang' => $array_traduccion
                            ];
                        }
                        $comment->user['file'] = $file;
                    }

                    if ($comment->user_updated_id != '') {
                        $comment->updated_by['id'] = $comment->user_updated_id;
                        $comment->updated_by['email'] = $comment->user_updated_email;
                        $comment->updated_by['name'] = $comment->user_updated_name;
                        $comment->updated_by['surname'] = $comment->user_updated_surname;
                        $comment->updated_by['updated_when'] = $comment->updated_when;
                    }

                    $rating = [
                        'id' => $comment->id,
                        'language_id' => $comment->language_id,
                        'subchannel_id' => $comment->subchannel_id,
                        'text' => $comment->text,
                        'rating' => $comment->rating,
                        'created_at' => $comment->created_at,
                        'user' => $comment->user,
                        'updated_by' => $comment->updated_by,
                        'product' => $comment->product,
                        'comment_status' => $comment->comment_status,
                    ];

                    $array['data'][0]['comment'][] = $rating;

                    //resumen: media rating, total de comentarios y total de comentarios con las diferentes puntuaciones

                }

                if ($totales > 0) {
                    $array['data'][1]['summary'][0]['media_rating'] = ($media_rating) ? $media_rating : 0;
                    $array['data'][1]['summary'][0]['total_comments'] = $totales;
                    $array['data'][1]['summary'][0]['total_ratings'] = $opiniones_clasificadas;
                }

                $array['total_results'] = $totales;
                //fin resumen
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Comentarios');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar un comentario.
     *
     * Para la consulta de un comment se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $comment_aux = Comment::find($id);

            $array['data'] = array();

            if ($comment_aux) {

                $comment = DB::connection('tenant')->table('mo_comment')
                    ->select('mo_comment.id', 'mo_comment.product_id', 'mo_comment.language_id', 'mo_comment.subchannel_id', 'mo_comment.text', 'mo_comment.rating', 'mo_comment.updated_by', 'mo_comment.comment_status_id', 'mo_comment.updated_when', 'mo_comment.created_at', 'mo_user.id as user_id', 'mo_user.email', 'mo_user.name', 'mo_user.surname', 'mo_user_updated.id as user_updated_id', 'mo_user_updated.email as user_updated_email', 'mo_user_updated.name as user_updated_name', 'mo_user_updated.surname as user_updated_surname')
                    ->leftJoin('mo_user', function ($join) {
                        $join->on('mo_comment.user_id', 'mo_user.id')
                            ->where('mo_user.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_user as mo_user_updated', function ($join) {
                        $join->on('mo_comment.updated_by', 'mo_user_updated.id')
                            ->where('mo_user_updated.deleted_at', '=', null);
                    })
                    ->where('mo_comment.id', '=', $id)
                    ->where('mo_comment.deleted_at', '=', null)->first();

                $comment->user = null;
                $comment->updated_by = null;
                $comment->product = null;
                $comment->comment_status = null;

                foreach ($idiomas as $idioma) {

                    $datos_estado_traduccion = DB::connection('tenant')->table('mo_comment_status')
                        ->select('mo_comment_status_translation.language_id', 'mo_comment_status_translation.name')
                        ->whereNull('mo_comment_status.deleted_at')
                        ->where('mo_comment_status.id', '=', $comment->comment_status_id)
                        ->join('mo_comment_status_translation', 'mo_comment_status.id', '=', 'mo_comment_status_translation.comment_status_id')
                        ->whereNull('mo_comment_status_translation.deleted_at')
                        ->where('mo_comment_status_translation.language_id', '=', $idioma->id)
                        ->get();

                    foreach ($datos_estado_traduccion as $estado_traduccion) {
                        $comment->comment_status['id'] = $comment->comment_status_id;
                        $comment->comment_status['lang'][][$idioma->abbreviation] = $estado_traduccion;
                    }

                    $datos_producto_traduccion = DB::connection('tenant')->table('mo_product')
                        ->select('mo_product_translation.language_id', 'mo_product_translation.name')
                        ->whereNull('mo_product.deleted_at')
                        ->where('mo_product.id', '=', $comment->product_id)
                        ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                        ->whereNull('mo_product_translation.deleted_at')
                        ->where('mo_product_translation.language_id', '=', $idioma->id)
                        ->get();

                    foreach ($datos_producto_traduccion as $producto_traduccion) {
                        $comment->product['id'] = $comment->product_id;
                        $comment->product['lang'][][$idioma->abbreviation] = $producto_traduccion;
                    }

                }

                if ($comment->user_id != '') {
                    $comment->user['id'] = $comment->user_id;
                    $comment->user['email'] = $comment->email;
                    $comment->user['name'] = $comment->name;
                    $comment->user['surname'] = $comment->surname;
                }

                if ($comment->user_updated_id != '') {
                    $comment->updated_by = array();
                    $comment->updated_by['id'] = $comment->user_updated_id;
                    $comment->updated_by['email'] = $comment->user_updated_email;
                    $comment->updated_by['name'] = $comment->user_updated_name;
                    $comment->updated_by['surname'] = $comment->user_updated_surname;
                    $comment->updated_by['updated_when'] = $comment->updated_when;
                }

                $rating = [
                    'id' => $comment->id,
                    'language_id' => $comment->language_id,
                    'subchannel_id' => $comment->subchannel_id,
                    'text' => $comment->text,
                    'rating' => $comment->rating,
                    'created_at' => $comment->created_at,
                    'user' => $comment->user,
                    'updated_by' => $comment->updated_by,
                    'product' => $comment->product,
                    'comment_status' => $comment->comment_status,
                ];

                $array['data'][0]['comment'][] = $rating;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Comentarios');
        }
        return response()->json($array, $array['error']);
    }


}