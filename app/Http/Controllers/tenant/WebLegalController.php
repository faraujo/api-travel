<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\WebLegal;
use App\Models\tenant\WebLegalTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Support\Str;

class WebLegalController extends Controller
{

    public function update(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $array_traducciones = array();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0',
                'type' => 'unique:tenant.mo_web_legal,type,'. $request->get('id') . ',id,deleted_at,NULL'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            

            foreach ($idiomas as $idioma) {
                if (isset($request->get('legal')[$idioma->abbreviation])) {
                    
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $legal = WebLegal::where('id', $request->get('id'))->first();

                if(!$legal) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
    
                    $legal->legalTranslation()->delete();

                    foreach ($array_traducciones as $idioma) {
                        WebLegalTranslation::create([
                            'legal_id' => $legal->id,
                            'language_id' => $idioma->id,
                            'legal' => $request->get('legal')[$idioma->abbreviation]
                        ]);
                    }
                }
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Legal');

        }

        return response()->json($array, $array['error']);
    }

    public function show(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();
            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_legal = DB::connection('tenant')->table('mo_web_legal')
                    ->select(
                        'mo_web_legal.id',
                        'mo_web_legal.type'
                    )
                    ->whereNull('mo_web_legal.deleted_at')
                    ->leftJoin('mo_web_legal_translation', function ($join){
                        $join->on('mo_web_legal_translation.legal_id','mo_web_legal.id')
                            ->whereNull('mo_web_legal_translation.deleted_at');
                    });

                $sub = DB::connection('tenant')->table('mo_web_legal')
                    ->select(
                        'mo_web_legal.id',
                        'mo_web_legal.type'
                    )
                    ->whereNull('mo_web_legal.deleted_at')
                    ->leftJoin('mo_web_legal_translation', function ($join){
                        $join->on('mo_web_legal_translation.legal_id','mo_web_legal.id')
                            ->whereNull('mo_web_legal_translation.deleted_at');
                    });

                if ($request->get('lang') != '') {
                    $sql_legal->where('mo_web_legal_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_web_legal_translation.language_id', '=', $idioma->id);
                }

                $orden = 'mo_web_legal.type';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'type':
                            $orden = 'mo_web_legal.type';
                            break;
                        case 'id':
                            $orden = 'mo_web_legal.id';
                            break;
                        case 'legal':
                            $orden = 'mo_web_legal_translation.legal';
                            break;
                        default:
                            $orden = 'mo_web_legal.type';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_legal = $sql_legal->groupBy('mo_web_legal.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_legal = $sql_legal->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $datos_legal = $sql_legal->get();

                }
                //Fin de paginación

                $sub->groupBy('mo_web_legal.id');

                $legal_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $legal_count->count();

                $array['data'] = array();
                foreach ($datos_legal as $legal) {
                    $array_legal = array();

                    $array_legal['id'] = $legal->id;
                    $array_legal['type'] = $legal->type;

                    foreach($idiomas as $idioma) {
                        $traducciones = WebLegalTranslation::where('legal_id', $legal->id)
                            ->select(
                                'id',
                                'language_id',
                                'legal'
                            )
                            ->where('language_id', $idioma->id)
                            ->get();

                        foreach($traducciones as $traduccion) {
                            $array_traduccion = array();

                            $array_traduccion['id'] = $traduccion->id;
                            $array_traduccion['language_id'] = $traduccion->language_id;
                            $array_traduccion['legal'] = $traduccion->legal;

                            $array_legal['lang'][][$idioma->abbreviation] = $array_traduccion;
                        }
                    }

                    $array['data'][0]['legal'][] = $array_legal;

                }
                
                $array['total_results'] = $totales;
                
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Legal');

        }

        return response()->json($array, $array['error']);
    }

    public function showId($id) {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $sql_legal = DB::connection('tenant')->table('mo_web_legal')
                ->select(
                    'mo_web_legal.id',
                    'mo_web_legal.type'
                )
                ->whereNull('mo_web_legal.deleted_at')
                ->leftJoin('mo_web_legal_translation', function ($join){
                    $join->on('mo_web_legal_translation.legal_id','mo_web_legal.id')
                        ->whereNull('mo_web_legal_translation.deleted_at');
                })
                ->where('mo_web_legal.id', $id);

            $datos_legal = $sql_legal->get();
            $array['data'] = array();
            foreach($datos_legal as $legal) {
                $array_legal = array();

                $array_legal['id'] = $legal->id;
                $array_legal['type'] = $legal->type;

                foreach ($idiomas as $idioma) {
                    $traducciones = WebLegalTranslation::where('legal_id', $legal->id)
                        ->select(
                            'id',
                            'language_id',
                            'legal'
                        )->where('language_id', $idioma->id)
                        ->get();
                    
                    foreach($traducciones as $traduccion) {
                        $array_traduccion = array();

                        $array_traduccion['id'] = $traduccion->id;
                        $array_traduccion['language_id'] = $traduccion->language_id;
                        $array_traduccion['legal'] = $traduccion->legal;

                        $array_legal['lang'][][$idioma->abbreviation] = $array_traduccion;
                    }

                }

                $array['data'][0]['legal'][] = $array_legal;


            }

            

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Legal');
        }

        return response()->json($array, $array['error']);
    }

}