<?php


namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\WebLiteralTranslation;
use App\Models\tenant\WebLiteral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Support\Str;

class WebLiteralController extends Controller
{

    public function show(Request $request) {
        $array['error'] = 200;
        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();
            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_literal = DB::connection('tenant')->table('mo_web_literal')
                    ->select(
                        'mo_web_literal.id',
                        'mo_web_literal.name'
                    )
                    ->whereNull('mo_web_literal.deleted_at')
                    ->leftJoin('mo_web_literal_translation', function ($join){
                        $join->on('mo_web_literal_translation.literal_id','mo_web_literal.id')
                            ->whereNull('mo_web_literal_translation.deleted_at');
                    });

                $sub = DB::connection('tenant')->table('mo_web_literal')
                    ->select(
                        'mo_web_literal.id'
                    )
                    ->whereNull('mo_web_literal.deleted_at')
                    ->leftJoin('mo_web_literal_translation', function ($join){
                        $join->on('mo_web_literal_translation.literal_id','mo_web_literal.id')
                            ->whereNull('mo_web_literal_translation.deleted_at');
                    });

                if ($request->get('lang') != '') {
                    $sql_literal->where('mo_web_literal_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_web_literal_translation.language_id', '=', $idioma->id);
                }

                if($request->get('name') != '') {
                    $sql_literal->where('mo_web_literal.name', 'like', '%'.$request->get('name').'%');
                    $sub->where('mo_web_literal.name', 'like', '%'.$request->get('name').'%');
                }

                $orden = 'mo_web_literal.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_web_literal.name';
                            break;
                        case 'id':
                            $orden = 'mo_web_literal.id';
                            break;
                        case 'translation':
                            $orden = 'mo_web_literal_translation.translation';
                            break;
                        default:
                            $orden = 'mo_web_literal.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_literal = $sql_literal->groupBy('mo_web_literal.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_literal = $sql_literal->forPage($inicio, $limite)->get();
                    //si filtro limit = 0 se obtienen todos los resultados
                } else {
                    $datos_literal = $sql_literal->get();
                }
                //Fin de paginación

                $sub->groupBy('mo_web_literal.id');

                $literal_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $literal_count->count();

                $array['data'] = array();
                foreach ($datos_literal as $literal) {
                    $array_literal = array();

                    $array_literal['id'] = $literal->id;
                    $array_literal['name'] = $literal->name;

                    foreach($idiomas as $idioma) {
                        $traducciones = WebLiteralTranslation::where('literal_id', $literal->id)
                            ->select(
                                'id',
                                'language_id',
                                'translation'
                            )
                            ->where('language_id', $idioma->id)
                            ->get();

                        foreach($traducciones as $traduccion) {
                            $array_traduccion = array();

                            $array_traduccion['id'] = $traduccion->id;
                            $array_traduccion['language_id'] = $traduccion->language_id;
                            $array_traduccion['translation'] = $traduccion->translation;

                            $array_literal['lang'][][$idioma->abbreviation] = $array_traduccion;
                        }
                    }
                    $array['data'][0]['literal'][] = $array_literal;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Literal');

        }

        return response()->json($array, $array['error']);
    }

    public function showId($id) {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $sql_literal = DB::connection('tenant')->table('mo_web_literal')
                ->select(
                    'mo_web_literal.id',
                    'mo_web_literal.name'
                )
                ->whereNull('mo_web_literal.deleted_at')
                ->leftJoin('mo_web_literal_translation', function ($join){
                    $join->on('mo_web_literal_translation.literal_id','mo_web_literal.id')
                        ->whereNull('mo_web_literal_translation.deleted_at');
                })
                ->where('mo_web_literal.id', $id);

            $datos_literal = $sql_literal->get();
            $array['data'] = array();
            foreach($datos_literal as $literal) {
                $array_literal = array();

                $array_literal['id'] = $literal->id;
                $array_literal['name'] = $literal->name;

                foreach ($idiomas as $idioma) {
                    $traducciones = WebLiteralTranslation::where('literal_id', $literal->id)
                        ->select(
                            'id',
                            'language_id',
                            'translation'
                        )->where('language_id', $idioma->id)
                        ->get();
                    
                    foreach($traducciones as $traduccion) {
                        $array_traduccion = array();

                        $array_traduccion['id'] = $traduccion->id;
                        $array_traduccion['language_id'] = $traduccion->language_id;
                        $array_traduccion['translation'] = $traduccion->translation;

                        $array_literal['lang'][][$idioma->abbreviation] = $array_traduccion;
                    }

                }

                $array['data'][0]['literal'][] = $array_literal;
            }

            

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Literal');
        }

        return response()->json($array, $array['error']);
    }

    public function update(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            $array_traducciones = array();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            

            foreach ($idiomas as $idioma) {
                if (isset($request->get('translation')[$idioma->abbreviation])) {
                    
                    $array_traducciones[] = $idioma;
                }

            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $literal = WebLiteral::where('id', $request->get('id'))->first();

                if(!$literal) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                } else {
    
                    $literal->literalTranslation()->delete();

                    foreach ($array_traducciones as $idioma) {
                        WebLiteralTranslation::create([
                            'literal_id' => $literal->id,
                            'language_id' => $idioma->id,
                            'translation' => $request->get('translation')[$idioma->abbreviation]
                        ]);
                    }
                }
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Legal');

        }

        return response()->json($array, $array['error']);
    }

}