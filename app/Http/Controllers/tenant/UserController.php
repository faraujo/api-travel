<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Country;
use App\Models\tenant\PasswordHistory;
use App\Models\tenant\PasswordToken;
use App\Models\tenant\Settings;
use App\Models\tenant\User;
use App\Models\tenant\UserRememberToken;
use App\Models\tenant\UserToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\Handler;
use App\Models\tenant\UserRole;
use App\Models\tenant\UserIdentification;
use App\Models\tenant\File;

use App\Models\SaasUser;
use App\Models\SaasUserToken;
use App\Models\SaasSettings;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    /**
     * Función para la creación/modificación de un usuario.
     *
     * Para la creación de un usuario se realiza una petición POST.
     * Para la modificación de un usuario se realiza una petición PUT.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'email2' => 'email',
                'password' => 'min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                'c_password' => 'required_with:password|same:password',
                'birthdate' => 'date|date_format:"Y-m-d"|before_or_equal:today',
                'language_id' => 'exists:tenant.mo_language,id,deleted_at,NULL|integer|min:0',
                'state_id' => 'integer|min:0|exists:tenant.mo_state,id,deleted_at,NULL',
                'country_id' => 'integer|min:0|required_with:state_id|exists:tenant.mo_country,id,deleted_at,NULL',
                'document_type_id' => 'required_with:number_document|exists:tenant.mo_document_type,id,deleted_at,NULL|integer|min:0',
                'number_document' => 'required_with:document_type_id',
                'sex_id' => 'exists:tenant.mo_sex,id,deleted_at,NULL|integer|min:0',
                'bloqued_login' => 'boolean',
                'worker' => 'boolean',
                'worker_type_id' => 'exists:tenant.mo_worker_type,id,deleted_at,NULL|integer|min:0',
                'identification' => 'array',
                'identification.*.identification_id' => 'integer|exists:tenant.mo_identification,id,deleted_at,NULL|min:0|required',
                'identification.*.number' => 'required',
                'roles' => 'array',
                'roles.*' => 'required|integer|exists:tenant.mo_role,id,deleted_at,NULL|min:0',
                'image_id' => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //controla que no introduzcan espacios en blanco en el password
            if ($request->get('password')) {

                if (preg_match('/\s/', $request->get('password')) || preg_match('/\s/', $request->get('c_password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('c_password'))) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
                }
            }
            //Método que valida que un estado pertenezca al país enviado

            if ($request->get('state_id') != '') {
                $state = $request->get('state_id');
                $country = $request->get('country_id');
                $sql_country = DB::connection('tenant')->Table('mo_state')
                    ->where('id', $state)
                    ->select('country_id');
                $datos_country = $sql_country->first();

                if ($datos_country) {
                    if ($datos_country->country_id != $country) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['state_id' => ['state_id not match with country_id']]);
                    }
                }


            }


            //Validación específica para creación de usuario

            if ($request->isMethod('post')) {

                $validator = \Validator::make($request->all(), [
                    'email' => 'unique:tenant.mo_user,email,NULL,id,deleted_at,NULL',
                    'password' => 'required',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                //Validación específica para actualización de usuario

            } else {
                $validator = \Validator::make($request->all(), [
                    'id' => 'integer|min:0|required',
                    'email' => 'unique:tenant.mo_user,email,' . $request->get('id') . ',id,deleted_at,NULL',
                ]);
                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                } else {

                    $usuario = User::find($request->get('id'));
                    if (!$usuario) {
                        $error = 2;
                    } else {

                        //Comprobación de administradores resultantes previo a modificación 
                        $check_admin = 0;
                        if($request->get('roles') && is_array($request->get('roles'))){
                            //Si los roles a asignar no contienen el propio rol administrador)
                            if(!in_array(1, $request->get('roles'))){
                                $check_admin = 1;
                            }
                        //Si no se asignan roles al usuario                                
                        }else{
                            $check_admin = 1;
                        }

                        if($check_admin == 1){
                            $admin_number = DB::connection('tenant')->table('mo_user_role')
                            ->select('user_id')
                            ->join('mo_user', 'mo_user.id', '=', 'mo_user_role.user_id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->whereNull('mo_user.deleted_at')
                            ->where('role_id', 1)->whereNotIn('user_id', [$request->get('id')])
                            ->count();
                            
                            if ($admin_number == 0) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['admin' => ['At least one admin user is required']]);
                            }
                        }
                       
                        if($error == 0){
                            $usada = false;

                            $guardadas = PasswordHistory::where('user_id', $request->get('id'))->orderBy('id', 'desc')->limit(4)->get();

                            foreach ($guardadas as $guardada) {
                                if (Hash::check($request->get('password'), $guardada->password)) {
                                    $usada = true;
                                }
                            }

                            if ($usada) {
                                $error = 1;
                                $mensaje_validador = $mensaje_validador->merge(['password' => ['The password entered must be different from the last four.']]);
                            }
                        }
                    }
                }
            }
            // FIN Validación

            $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                ->where('mo_settings.name', 'heimdall_middleware')
                ->first()
                ->value;

            $rule_codes = [];

            if($heimdall_middleware == '1') {

                $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                $plans = websitePlanRules($website);

                $cantidad = 0;
                $user_other = 0;
                $user_customer = 0;

                foreach($plans as $plan) {

                    if($plan->plan_id != null){
                        $rules = websiteRules($plan);
                        
                        $rules_admin = $rules->where('code', 'user_admin');

                        if($rules_admin) {
                            foreach($rules_admin as $rule) {
                                if($rule->unlimited == 1){
                                    $cantidad = -1;
                                } else {
                                    if($cantidad >= 0 && $cantidad < $rule->value) {
                                        $cantidad = $rule->value;
                                    }
                                }
                            }
                        }

                        $rules_other = $rules->where('code', 'user_other');

                        if($rules_other) {
                            foreach($rules_other as $rule) {
                                if($rule->unlimited == 1) {
                                    $user_other = -1;
                                } else {
                                    if($user_other >= 0 && $user_other < $rule->value) {
                                        $user_other = $rule->value;
                                    }
                                }
                            }
                        }
                    } else {
                        if($plan->rule_code == 'user_admin') {
                            if($plan->unlimited == 1){
                                $cantidad = -1;
                            }else{
                                if($cantidad >= 0 && $cantidad < $plan->value){
                                    $cantidad = $plan->value;
                                }
                            }
                        }

                        if($plan->rule_code == 'user_other') {
                            if($plan->unlimited == 1){
                                $user_other = -1;
                            }else{
                                if($user_other >= 0 && $user_other < $plan->value){
                                    $user_other = $plan->value;
                                }
                            }
                        }
                    }
                }

                    
                if($request->get('roles') != '' && is_array($request->get('roles')) && count($request->get('roles')) > 0) {
                    if(in_array('1' , $request->get('roles'))) {
                        if($cantidad == 0) {
                            $error = 3;
                            $rule_codes[] = 'user_admin';
                        } else if ($cantidad > 0) {
                            $total_roles = DB::connection('tenant')->table('mo_user')
                                ->whereNull('mo_user.deleted_at')
                                ->join('mo_user_role','mo_user_role.user_id','mo_user.id')
                                ->whereNull('mo_user_role.deleted_at')
                                ->where('mo_user_role.role_id',1);

                            if ($request->isMethod('put')) {
                                $total_roles = $total_roles->where('mo_user_role.user_id', '!=', $request->get('id'));
                            }
                            $total_roles = $total_roles->get();

                            if(count($total_roles) >= $cantidad) {
                                $error = 3;
                                $rule_codes[] = 'user_admin';
                            }
                        }
                        
                    } else {
                        
                        if($user_other == 0) {
                            $error = 3;
                            $rule_codes[] = 'user_other';
                        } else if ($user_other > 0) {
                            $total_users = DB::connection('tenant')->table('mo_user')
                                ->whereNull('mo_user.deleted_at')
                                ->join('mo_user_role','mo_user_role.user_id','mo_user.id')
                                ->whereNull('mo_user_role.deleted_at')
                                ->join('mo_role_permission','mo_role_permission.role_id','mo_user_role.role_id')
                                ->whereNull('mo_role_permission.deleted_at')
                                ->where('mo_user_role.role_id','!=',1)
                                ->where('mo_role_permission.permission_id',2);

                            if ($request->isMethod('put')) {
                                $total_users = $total_users->where('mo_user_role.user_id', '!=', $request->get('id'));
                            }

                            $total_users = $total_users->get();
        
                            if(count($total_users) >= $user_other) {
                                $error = 3;
                                $rule_codes[] = 'user_other';
                            }
                        }
                    }
                } 
            }

            if ($error == 0) {

                if($request->get('country_id') != '') {
                    $country = Country::where('id', $request->get('country_id'))->first();
                } else {
                    $country = Country::orderBy('order','desc')->first();
                }

                //Generación array de datos de usuario
                $user = [
                    'name' => ($request->get('name') != '') ? $request->get('name') : null,
                    'surname' => ($request->get('surname') != '') ? $request->get('surname') : null,
                    'document_type_id' => ($request->get('document_type_id') != '') ? $request->get('document_type_id') : null,
                    'number_document' => ($request->get('number_document') != '') ? $request->get('number_document') : null,
                    'sex_id' => ($request->get('sex_id') != '') ? $request->get('sex_id') : null,
                    'birthdate' => ($request->get('birthdate') != '') ? $request->get('birthdate') : null,
                    'email' => $request->get('email'),
                    'email2' => ($request->get('email2') != '') ? $request->get('email2') : null,
                    'bloqued_login' => ($request->get('bloqued_login') != '') ? $request->get('bloqued_login') : 0,
                    'language_id' => ($request->get('language_id') != '') ? $request->get('language_id') : null,
                    'telephone1' => ($request->get('telephone1') != '') ? $request->get('telephone1') : null,
                    'telephone2' => ($request->get('telephone2') != '') ? $request->get('telephone2') : null,
                    'telephone3' => ($request->get('telephone3') != '') ? $request->get('telephone3') : null,
                    'address' => ($request->get('address') != '') ? $request->get('address') : null,
                    'postal_code' => ($request->get('postal_code') != '') ? $request->get('postal_code') : null,
                    'city' => ($request->get('city') != '') ? $request->get('city') : null,
                    'state_id' => ($request->get('state_id') != '') ? $request->get('state_id') : null,
                    'country_id' => $country->id,
                    'business' => ($request->get('business') != '') ? $request->get('business') : null,
                    'telephone_business' => ($request->get('telephone_business') != '') ? $request->get('telephone_business') : null,
                    'worker' => ($request->get('worker') != '') ? $request->get('worker') : 0,
                    'worker_type_id' => ($request->get('worker_type_id') != '') ? $request->get('worker_type_id') : null,
                    'observations' => ($request->get('observations') != '') ? $request->get('observations') : null,
                    'image_id' => ($request->get('image_id') != '') ? $request->get('image_id') : null,
                ];


                if ($request->get('password')) {
                    $user['password'] = Hash::make($request->get('password'));
                }

                //Creación o actualización del registro de usuario en BBDD
                if ($request->isMethod('put')) {

                    $usuario->update($user);

                } else {

                    $usuario = User::create($user);

                }

                //Borrado de identificadores pertenecientes al usuario y reasignación
                UserIdentification::where('user_id', $usuario->id)->delete();

                if ($request->get('identification') != '') {
                    // Creación de identificadores
                    foreach ($request->get('identification') as $identification) {
                        UserIdentification::create([
                            'user_id' => $usuario->id,
                            'identification_id' => $identification['identification_id'],
                            'identification_number' => $identification['number'],
                        ]);
                    }
                }

                //Borrado y asignacion de roles
                UserRole::where('user_id', '=', $usuario->id)->delete();

                if ($request->get('roles') != '') {

                    foreach ($request->get('roles') as $role) {
                        UserRole::create([
                            'user_id' => $usuario->id,
                            'role_id' => $role,
                        ]);
                    }
                }
                //Almacenamiento de contraseña en el histórico
                if ($request->get('password')) {

                    PasswordHistory::create([
                        'user_id' => $usuario->id,
                        'password' => $usuario->password,
                    ]);
                }

                

            } elseif ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } elseif ($error == 2) {
                $array['error'] = 404;
                $array['error_description'] = 'Data not found';
            } elseif ($error == 3) {
                $array['error'] = 419;
                $array['error_description'] = 'You do not have a valid plan';
                $array['error_rule_code'] = $rule_codes;

                //Heimdall - datos para redirección
                $array['data'] = array();
                $array['data'][0]['website_id'] = $website->id;

                $protocol = DB::table('saas_settings')
                ->where('saas_settings.name', 'protocol')->first()->value;
                $base_path_saas = DB::table('saas_settings')
                ->where('saas_settings.name', 'base_path_saas')->first()->value;
                $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para mostrar los usuarios.
     *
     * Para la consulta de usuarios se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'limit' => 'integer|min:0',
                'page' => 'integer|min:0',
                'state_id' => 'integer|min:0|exists:tenant.mo_state,id,deleted_at,NULL',
                'country_id' => 'integer|min:0|required_with:state_id|exists:tenant.mo_country,id,deleted_at,NULL',
                'language_id' => 'integer|min:0',
                'sex_id' => 'integer|min:0',
                'worker_type_id' => 'integer|min:0',
                'role_id' => 'integer|min:0',
                'bloqued_login' => 'boolean',
                'worker' => 'boolean',
                'document_type_id' => 'integer|min:0',
                'date_start' => 'date|required_with:date_end|date_format:"Y-m-d"',
                'date_end' => 'date|required_with:date_start|after_or_equal:date_start|date_format:"Y-m-d"',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //Método que valida que un estado pertenezca al país enviado

            if ($request->get('state_id') != '') {
                $state = $request->get('state_id');
                $country = $request->get('country_id');
                $sql_country = DB::connection('tenant')->Table('mo_state')
                    ->where('id', $state)
                    ->select('country_id');
                $datos_country = $sql_country->first();

                if ($datos_country) {
                    if ($datos_country->country_id != $country) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['state_id' => ['state_id not match with country_id']]);
                    }
                }


            }

            // Fin validación de campos

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {

                $usuarios = DB::connection('tenant')->table('mo_user')
                    ->select('mo_user.id', 'mo_user.name', 'mo_user.surname', 'mo_user.document_type_id', 'mo_user.number_document', 'mo_user.sex_id', 'mo_user.birthdate', 'mo_user.email', 'mo_user.email2',
                        'mo_user.faults_login', 'mo_user.bloqued_login', 'mo_user.bloqued_to', 'mo_user.language_id', 'mo_user.telephone1', 'mo_user.telephone2', 'mo_user.telephone3', 'mo_user.address', 'mo_user.postal_code',
                        'mo_user.city', 'mo_user.state_id', 'mo_user.country_id', 'mo_user.business', 'mo_user.telephone_business', 'mo_user.worker', 'mo_user.worker_type_id',
                        'mo_user.observations', 'mo_user.image_id')
                    ->where('mo_user.deleted_at', '=', null)
                    ->leftJoin('mo_user_role', function ($join) {
                        $join->on('mo_user_role.user_id', '=', 'mo_user.id')
                            ->where('mo_user_role.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_role', function ($join) {
                        $join->on('mo_user_role.role_id', '=', 'mo_role.id')
                            ->where('mo_role.deleted_at', '=', null);
                    });

                $sub = DB::connection('tenant')->table('mo_user')
                    ->select('mo_user.id')
                    ->where('mo_user.deleted_at', '=', null)
                    ->leftJoin('mo_user_role', function ($join) {
                        $join->on('mo_user_role.user_id', '=', 'mo_user.id')
                            ->where('mo_user_role.deleted_at', '=', null);
                    })
                    ->leftJoin('mo_role', function ($join) {
                        $join->on('mo_user_role.role_id', '=', 'mo_role.id')
                            ->where('mo_role.deleted_at', '=', null);
                    });


                if ($request->get('country_id') != '') {
                    $usuarios->where('mo_user.country_id', $request->get('country_id'));
                    $sub->where('mo_user.country_id', $request->get('country_id'));
                }

                if ($request->get('state_id') != '') {
                    $usuarios->where('mo_user.state_id', $request->get('state_id'));
                    $sub->where('mo_user.state_id', $request->get('state_id'));
                }

                if ($request->get('language_id') != '') {
                    $usuarios->where('mo_user.language_id', $request->get('language_id'));
                    $sub->where('mo_user.language_id', $request->get('language_id'));
                }

                if ($request->get('sex_id') != '') {
                    $usuarios->where('mo_user.sex_id', $request->get('sex_id'));
                    $sub->where('mo_user.sex_id', $request->get('sex_id'));
                }

                if ($request->get('worker_type_id') != '') {
                    $usuarios->where('mo_user.worker_type_id', $request->get('worker_type_id'));
                    $sub->where('mo_user.worker_type_id', $request->get('worker_type_id'));
                }

                if ($request->get('date_start') != '') {
                    $usuarios->where('mo_user.birthdate', '>=', $request->get('date_start'))
                        ->where('mo_user.birthdate', '<=', $request->get('date_end'));
                    $sub->where('mo_user.birthdate', '>=', $request->get('date_start'))
                        ->where('mo_user.birthdate', '<=', $request->get('date_end'));
                }

                if ($request->get('role_id') != '') {
                    $usuarios->where('mo_role.id', $request->get('role_id'));
                    $sub->where('mo_role.id', $request->get('role_id'));
                }

                if ($request->get('name') != '') {
                    $usuarios->where(function ($query) use ($request) {
                        $query->where('mo_user.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_user.surname', 'Like', '%' . $request->get('name') . '%');
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_user.name', 'Like', '%' . $request->get('name') . '%')
                            ->orWhere('mo_user.surname', 'Like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('bloqued_login') != '') {
                    $usuarios->where('mo_user.bloqued_login', $request->get('bloqued_login'));
                    $sub->where('mo_user.bloqued_login', $request->get('bloqued_login'));
                }

                if ($request->get('worker') != '') {
                    $usuarios->where('mo_user.worker', $request->get('worker'));
                    $sub->where('mo_user.worker', $request->get('worker'));
                }

                if ($request->get('document_type_id') != '') {
                    $usuarios->where('mo_user.document_type_id', $request->get('document_type_id'));
                    $sub->where('mo_user.document_type_id', $request->get('document_type_id'));
                }

                if ($request->get('number_document') != '') {
                    $usuarios->where('mo_user.number_document', $request->get('number_document'));
                    $sub->where('mo_user.number_document', $request->get('number_document'));
                }

                if ($request->get('email') != '') {
                    $usuarios->where(function ($query) use ($request) {
                        $query->where('mo_user.email', 'Like', '%' . $request->get('email') . '%')->orWhere('mo_user.email2', 'Like', '%' . $request->get('email') . '%');
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_user.email', 'Like', '%' . $request->get('email') . '%')->orWhere('mo_user.email2', 'Like', '%' . $request->get('email') . '%');
                    });
                }

                if ($request->get('postal_code') != '') {
                    $usuarios->where('mo_user.postal_code', $request->get('postal_code'));
                    $sub->where('mo_user.postal_code', $request->get('postal_code'));
                }

                if ($request->get('business') != '') {
                    $usuarios->where('mo_user.business', 'Like', '%' . $request->get('business') . '%');
                    $sub->where('mo_user.business', 'Like', '%' . $request->get('business') . '%');
                }

                if ($request->get('address') != '') {
                    $usuarios->where(function ($query) use ($request) {
                        $query->where('mo_user.address', 'Like', '%' . $request->get('address') . '%')
                            ->orWhere('mo_user.city', 'Like', '%' . $request->get('address') . '%');
                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_user.address', 'Like', '%' . $request->get('address') . '%')
                            ->orWhere('mo_user.city', 'Like', '%' . $request->get('address') . '%');
                    });
                }

                if ($request->get('telephone') != '') {
                    $usuarios->where(function ($query) use ($request) {
                        $query->where('mo_user.telephone1', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_user.telephone2', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_user.telephone3', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_user.telephone_business', 'Like', '%' . $request->get('telephone') . '%');

                    });
                    $sub->where(function ($query) use ($request) {
                        $query->where('mo_user.telephone1', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_user.telephone2', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_user.telephone3', 'Like', '%' . $request->get('telephone') . '%')
                            ->orWhere('mo_user.telephone_business', 'Like', '%' . $request->get('telephone') . '%');

                    });
                }

                // Order
                $orden = 'name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'id':
                            $orden = 'id';
                            break;
                        case 'name':
                            $orden = 'name';
                            break;
                        case 'surname':
                            $orden = 'surname';
                            break;
                        case 'birthdate':
                            $orden = 'birthdate';
                            break;
                        case 'email':
                            $orden = 'email';
                            break;
                        case 'number_document':
                            $orden = 'number_document';
                            break;
                        case 'postal_code':
                            $orden = 'postal_code';
                            break;
                        case 'business':
                            $orden = 'business';
                            break;
                        case 'worker_type_id':
                            $orden = 'worker_type_id';
                            break;
                        default:
                            $orden = 'name';
                            break;
                    }
                }
                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $usuarios = $usuarios->groupBy('mo_user.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $usuarios = $usuarios->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $usuarios = $usuarios->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_user.id');
                $usuarios_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);
                $totales = $usuarios_count->count();

                $array['data'] = array();

                if ($totales > 0) {

                    $array['data'][0]['user'] = $usuarios;

                }

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Eliminación de un usuario
     *
     * Para la eliminación de usuarios se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //Comprobación de administradores resultantes previo al borrado
            if($error == 0){
                //$admin_number = UserRole::where('role_id', 1)->whereNotIn('user_id', $request->get('ids'))->count();
                $admin_number = DB::connection('tenant')->table('mo_user_role')
                ->select('user_id')
                ->join('mo_user', 'mo_user.id', '=', 'mo_user_role.user_id')
                ->whereNull('mo_user_role.deleted_at')
                ->whereNull('mo_user.deleted_at')
                ->where('role_id', 1)->whereNotIn('user_id', $request->get('ids'))
                ->count();
                    
                if ($admin_number == 0) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['admin' => ['At least one admin user is required']]);
                }
            }
            // Fin validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $validator = \Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_user,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $user) {
                        $usuario = User::where('id', $user)->first();
                        $usuario->userIdentification()->delete();
                        $usuario->delete();
                    }

                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función para la consulta de un usuario
     *
     * Para la consulta de un usuario se realiza una petición GET. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $datos_usuario = DB::connection('tenant')->table('mo_user')
                ->select('mo_user.id', 'mo_user.name', 'mo_user.surname', 'mo_user.document_type_id', 'mo_user.number_document', 'mo_user.sex_id', 'mo_user.birthdate', 'mo_user.email', 'mo_user.email2',
                    'mo_user.faults_login', 'mo_user.bloqued_login', 'mo_user.bloqued_to', 'mo_user.language_id', 'mo_user.telephone1', 'mo_user.telephone2', 'mo_user.telephone3', 'mo_user.address', 'mo_user.postal_code',
                    'mo_user.city', 'mo_user.state_id', 'mo_user.country_id', 'mo_user.business', 'mo_user.telephone_business', 'mo_user.worker', 'mo_user.worker_type_id',
                    'mo_user.observations', 'mo_user.image_id')
                ->where('mo_user.deleted_at', '=', null)
                ->where('mo_user.id', '=', $id)
                ->first();

            $sql_roles = DB::connection('tenant')->table('mo_user_role')
                ->select('mo_user_role.role_id')
                ->join('mo_role', 'mo_user_role.role_id', '=', 'mo_role.id')
                ->where('mo_role.deleted_at', '=', null)
                ->where('mo_user_role.deleted_at', '=', null)
                ->where('mo_user_role.user_id', '=', $id);

            $sql_identifications = DB::connection('tenant')->table('mo_user_identification')
                ->select('mo_user_identification.identification_id', 'mo_user_identification.identification_number as number')
                ->where('mo_user_identification.deleted_at', '=', null)
                ->join('mo_identification', 'mo_user_identification.identification_id', '=', 'mo_identification.id')
                ->where('mo_identification.deleted_at', '=', null);

            $datos_roles = $sql_roles->groupBy('mo_role.id')->get();

            $array['data'] = array();

            if ($datos_usuario) {

                $datos_usuario->roles = $datos_roles;

                $datos_identifications = $sql_identifications->where('mo_user_identification.user_id', '=', $datos_usuario->id)->get();

                $datos_usuario->identification = $datos_identifications;

                $array['data'][0]['user'][] = $datos_usuario;


            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función para la consulta de un perfil y sus permisos
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();
            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {
                    $token = UserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {
                        $user = User::where('id', $token->user_id)->first(['id', 'name', 'surname', 'document_type_id', 'number_document', 'sex_id', 'birthdate', 'email', 'email2', 'faults_login', 'bloqued_login', 'bloqued_to', 'language_id', 'telephone1', 'telephone2', 'telephone3', 'address', 'postal_code', 'city', 'state_id', 'country_id', 'business', 'telephone_business', 'worker', 'worker_type_id', 'observations', 'image_id']);
                        if ($user != '') {

                            $sql_roles = DB::connection('tenant')->table('mo_user_role')
                                ->select('mo_user_role.role_id')
                                ->join('mo_role', 'mo_user_role.role_id', '=', 'mo_role.id')
                                ->where('mo_role.deleted_at', '=', null)
                                ->where('mo_user_role.deleted_at', '=', null)
                                ->where('mo_user_role.user_id', '=', $user->id);

                            $sql_permission_roles = DB::connection('tenant')->table('mo_role')
                                ->select('mo_permission.id')
                                ->whereNull('mo_role.deleted_at')
                                ->join('mo_role_permission', 'mo_role_permission.role_id', 'mo_role.id')
                                ->whereNull('mo_role_permission.deleted_at')
                                ->join('mo_permission', 'mo_permission.id', 'mo_role_permission.permission_id')
                                ->whereNull('mo_permission.deleted_at')
                                ->join('mo_module', 'mo_module.id', '=', 'mo_permission.module_id')
                                ->whereNull('mo_module.deleted_at');

                            $sql_subchannels = DB::connection('tenant')->table('mo_role')
                                ->select('mo_subchannel.id')
                                ->whereNull('mo_role.deleted_at')
                                ->join('mo_role_permission', 'mo_role_permission.role_id', '=', 'mo_role.id')
                                ->whereNull('mo_role_permission.deleted_at')
                                ->join('mo_subchannel', 'mo_role_permission.subchannel_id', '=', 'mo_subchannel.id')
                                ->whereNull('mo_subchannel.deleted_at')
                                ->groupBy('mo_subchannel.id');


                            $datos_roles = $sql_roles->groupBy('mo_role.id')->get();
                            $array_roles = array();
                            foreach ($datos_roles as $rol) {

                                $rol->permission = array();

                                $sql_permission_role_clone = clone $sql_permission_roles;
                                $datos_permisos_general = $sql_permission_role_clone->where('mo_permission.admin', '=', 0)
                                    ->where('mo_permission.subchannel', '=', 0)->where('mo_role.id', '=', $rol->role_id)->get();
                                $rol->permission['general'][]['permission'] = $datos_permisos_general;

                                $sql_permission_role_global_clone = clone $sql_permission_roles;

                                $datos_permisos_global = $sql_permission_role_global_clone->where('mo_permission.admin', '=', 1)
                                    ->where('mo_role.id', '=', $rol->role_id)->get();

                                $rol->permission['globalaccess'][]['permission'] = $datos_permisos_global;

                                $sql_subchannels_clone = clone $sql_subchannels;
                                $datos_subchannels = $sql_subchannels_clone->where('mo_role.id', '=', $rol->role_id)->get();

                                foreach ($datos_subchannels as $subchannel) {
                                    $sql_permission_role_global_clone = clone $sql_permission_roles;

                                    $datos_permisos_canal = $sql_permission_role_global_clone
                                        ->join('mo_module_subchannel', 'mo_module_subchannel.module_id', '=', 'mo_module.id')
                                        ->whereNull('mo_module_subchannel.deleted_at')
                                        ->where('mo_module_subchannel.subchannel_id', '=', $subchannel->id)
                                        ->where('mo_permission.subchannel', '=', 1)
                                        ->where('mo_role_permission.subchannel_id', '=', $subchannel->id)
                                        ->where('mo_role.id', '=', $rol->role_id)->get();

                                    $subchannel->permission = $datos_permisos_canal;
                                }

                                $rol->permission['subchannel'] = $datos_subchannels;


                                $array_roles[] = $rol;
                            }

                            $user->roles = $array_roles;

                        }
                        $array['data'] = array();
                        $array['data'][0]['user'] = [$user];
                    } else {
                        $array['error'] = 404;
                        $array['error_description'] = 'Data not found';
                    }
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = ['token' => ['Authorization header is required.']];
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para la consulta del perfil de un usuario en subcanales
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function profileSearch(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();
            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {
                    $token = UserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {
                        $user = User::where('id', $token->user_id)->first(['id', 'name', 'surname', 'document_type_id', 'number_document', 'sex_id', 'birthdate', 'email', 'email2', 'faults_login', 'bloqued_login', 'bloqued_to', 'language_id', 'telephone1', 'telephone2', 'telephone3', 'address', 'postal_code', 'city', 'state_id', 'country_id', 'business', 'telephone_business', 'worker', 'worker_type_id', 'observations', 'image_id','social_register', 'facebook_id' , 'google_id']);

                        $array['data'] = array();

                        
                        $array['data'][0]['user'] = [$user];
                    } else {
                        $array['error'] = 404;
                        $array['error_description'] = 'Data not found';
                    }
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = ['token' => ['Authorization header is required.']];
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para eliminar el propio usuario
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProfileSearch(Request $request) {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'actual_password' => 'required',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if (!$request->header('Authorization')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['token' => ['Authorization header is required.']]);
            }

            $token_api = explode(' ', $request->header('Authorization'));
            $user_token = UserToken::where('token', '=', $token_api[1])->first();

            if(!$user_token) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['token' => ['Authorization header is invalid.']]);
            } else {
                $user = User::where('id',$user_token->user_id)->first();
                //Validación específica para creación de usuario

                if (!$user) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['token' => ['Authorization header is invalid.']]);
                } else {

                    $user_id = $user->id;

                    $usada = false;


                    if (Hash::check($request->get('actual_password'), $user->password)) {
                        $usada = true;
                    }

                    if (!$usada) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['actual_password' => ['The actual password is not correct.']]);
                    }
                }
            }


            // FIN Validación

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $token_api = explode(' ', $request->header('Authorization'));
                $token = UserToken::where('token', '=', $token_api[1])->first();
                if ($token) {
                    $user_id = UserToken::where('token', '=', $token_api[1])->first()->user_id;
                    if (!$user_id) {
                        $array['error'] = 404;
                        $array['error_description'] = 'User not found';
                    } else {
                        //logica

                        User::where('id',$user_id)-> delete();

                        UserToken::where('user_id', $user_id)->where('expired_at', '>=', Carbon::now()->format('Y-m-d'))
                            ->delete();

                        //fin logica

                    }
                } else {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }
        return response()->json($array, $array['error']);
    }

    public function updateProfileSearch(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'email',
//                'actual_password' => 'required',
                'password' => 'min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                'c_password' => 'required_with:password|same:password',
                'state_id' => 'integer|min:0|exists:tenant.mo_state,id,deleted_at,NULL',
                'country_id' => 'integer|min:0|required_with:state_id|exists:tenant.mo_country,id,deleted_at,NULL',
                'image_id' => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if (!$request->header('Authorization')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['token' => ['Authorization header is required.']]);
            }

            //comprueba que se ha enviado correctamente el password del usuario

            $token_api = explode(' ', $request->header('Authorization'));
            $user_id = UserToken::where('token', '=', $token_api[1])->first()->user_id;

            $user = User::where('id', $user_id)->first();

            $password_match = false;

//            $password_guardada = PasswordHistory::where('user_id', $user_id)->orderBy('id', 'desc')->first();

            if ($request->get('actual_password') != '') {
                if (Hash::check($request->get('actual_password'), $user->password)) {
                    $password_match = true;
                }

                if (!$password_match) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['actual_password' => ['The actual password is not correct.']]);
                }
            }

            //Método que valida que un estado pertenezca al país enviado

            if ($request->get('state_id') != '') {
                $state = $request->get('state_id');
                $country = $request->get('country_id');
                $sql_country = DB::connection('tenant')->Table('mo_state')
                    ->where('id', $state)
                    ->select('country_id');
                $datos_country = $sql_country->first();

                if ($datos_country) {
                    if ($datos_country->country_id != $country) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['state_id' => ['state_id not match with country_id']]);
                    }
                }


            }
            if ($request->get('password') != '') {

                $token_api = explode(' ', $request->header('Authorization'));
                $user_id = UserToken::where('token', '=', $token_api[1])->first()->user_id;

                $usada = false;

                $guardadas = PasswordHistory::where('user_id', $user_id)->orderBy('id', 'desc')->limit(4)->get();

                foreach ($guardadas as $guardada) {
                    if (Hash::check($request->get('password'), $guardada->password)) {
                        $usada = true;
                    }
                }

                if ($usada) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['password' => ['The password entered must be different from the last four.']]);
                }

            }


            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {
                $token_api = explode(' ', $request->header('Authorization'));
                $token = UserToken::where('token', '=', $token_api[1])->first();
                if ($token) {
                    $user_id = UserToken::where('token', '=', $token_api[1])->first()->user_id;
                    if (!$user_id) {
                        $array['error'] = 404;
                        $array['error_description'] = 'User not found';
                    } else {
                        /*logica*/

                        //Asignacion de campos

                        $user = [];

                        if ($request->get('name') != '') {
                            $user['name'] = $request->get('name');
                        }
                        if ($request->get('surname') != '') {
                            $user['surname'] = $request->get('surname');

                        }

                        if ($request->get('country_id') != '') {
                            $user['country_id'] = $request->get('country_id');

                        }
                        if ($request->get('state_id') != '') {
                            $user['state_id'] = $request->get('state_id');

                        }
                        if ($request->get('telephone1') != '') {
                            $user['telephone1'] = $request->get('telephone1');

                        }
                        if ($request->get('email') != '') {
                            $user['email'] = $request->get('email');

                        }
                        if ($request->get('password') != '') {
                            $user['password'] = Hash::make($request->get('password'));

                            PasswordHistory::create([
                                'user_id' => $user_id,
                                'password' => Hash::make($request->get('password')),
                            ]);

                        }
                        if ($request->get('c_password') != '') {
                            $user['c_password'] = Hash::make($request->get('password'));

                        }
                        if ($request->get('image_id') != '') {
                            $user['image_id'] = $request->get('image_id');

                        } else {
                            $user['image_id'] = null;
                        }
                        $usuario = User::find($user_id);
                        $usuario->update($user);


                        /*fin logica*/
                    }
                } else {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->Rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Login
     *
     * Para la realización de login se realizara una peticion POST. Son requeridos los parámetros: email y password. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” además del token generado en la variable “data”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();
            
            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
                'remember' => 'boolean',
            ]);
            
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            
            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $user = User::where('email', '=', $request->get('email'))->first();
                
                $remember = 0;
                if ($request->get('remember') != '') {
                    $remember = $request->get('remember');
                }

                if (!$user) {

                    $array = ['error' => 401, 'error_description' => 'Unauthorized'];

                } elseif ($user->bloqued_login == 1) {

                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued']];

                } elseif ($user->bloqued_to >= Carbon::now()) {

                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued temporarily']];

                } elseif (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
                    $error_permisos = 0;
                    
                    if ($request->get('environment') != '' && $request->get('environment') == 'control_panel') {

                        $roles_permisos = DB::connection('tenant')->table('mo_permission')
                            ->whereNull('mo_permission.deleted_at')
                            ->where('mo_permission.id', 2)
                            ->join('mo_role_permission', 'mo_role_permission.permission_id', '=', 'mo_permission.id')
                            ->whereNull('mo_role_permission.deleted_at')
                            ->join('mo_role', 'mo_role.id', 'mo_role_permission.role_id')
                            ->whereNull('mo_role.deleted_at')
                            ->join('mo_user_role', 'mo_user_role.role_id', '=', 'mo_role.id')
                            ->whereNull('mo_user_role.deleted_at')
                            ->where('mo_user_role.user_id', $user->id)
                            ->first();
                        
                        if ($roles_permisos == '') {
                            $error_permisos = 1;
                        }

                    }
                    
                    if ($error_permisos == 1) {

                        $array = ['error' => 401, 'error_description' => 'Unauthorized'];

                    } else {
                        $user = Auth::user();
                        
                        $array['data'] = array();
                        $token = $user->createToken('MotorOnline')->accessToken;
                        
                        $remember_token = null;

                        $time_expired = Settings::where('name', 'token_validate')->first()->value;
                        $expired_at = Carbon::now()->addMinutes($time_expired);
                        UserToken::create([
                            'user_id' => $user->id,
                            'token' => $token,
                            'expired_at' => $expired_at,
                        ]);
                        
                    
                        if ($remember == 1) {
                            $remember_token = $user->createToken('MotorOnline')->accessToken;

                            UserRememberToken::create([
                                'user_id' => $user->id,
                                'token' => $remember_token,
                                'ip' => $request->ip(),
                            ]);
                        }


                        $user->faults_login = 0;
                        $user->save();
                        $array['data'][] = ['user_id' => $user->id, 'token' => 'Bearer ' . $token, 'remember_token' => $remember_token];
                    }
                } else {

                    $array = ['error' => 401, 'error_description' => 'Unauthorized'];

                    //Actualización de parámetros de fallo de acceso y bloqueo del usuario

                    $errores_maximos = Settings::where('name', '=', 'maximo_accesos_failed')->first()->value;
                    $user->faults_login = $user->faults_login < $errores_maximos ? $user->faults_login + 1 : $user->faults_login;

                    if ($user->faults_login >= $errores_maximos) {
                        $tiempo_bloqueo = Settings::where('name', 'time_bloqued_password')->first()->value;
                        $user->bloqued_to = Carbon::now()->addMinutes($tiempo_bloqueo);
                        $user->faults_login = 0;
                    }
                    $user->save();
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Registro de usuario
     *
     * Para el registro de un usuario se realizara una peticion POST. Son requeridos los parámetros: Email, password, c_password además de number_document en caso de existir document_type_id. Si la operación no produce errores se devuelve, en la variable “error” el valor “200” además del token generado en la variable “data”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email|unique:tenant.mo_user,email,NULL,id,deleted_at,NULL',
                'email2' => 'email',
                'password' => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                'c_password' => 'required|same:password',
                'birthdate' => 'date|date_format:"Y-m-d"|before_or_equal:today',
                'language_id' => 'exists:tenant.mo_language,id,deleted_at,NULL|integer|min:0',
                'state_id' => 'integer|min:0|exists:tenant.mo_state,id,deleted_at,NULL',
                'country_id' => 'integer|min:0|required_with:state_id|exists:tenant.mo_country,id,deleted_at,NULL',
                'document_type_id' => 'required_with:number_document|exists:tenant.mo_document_type,id,deleted_at,NULL|integer|min:0',
                'number_document' => 'required_with:document_type_id',
                'sex_id' => 'exists:tenant.mo_sex,id,deleted_at,NULL|integer|min:0',
                'worker' => 'boolean',
                'worker_type_id' => 'exists:tenant.mo_worker_type,id,deleted_at,NULL|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            //controla que no introduzcan espacios en blanco en el password ni caracteres acentuados
            if (preg_match('/\s/', $request->get('password')) || preg_match('/\s/', $request->get('c_password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('c_password'))) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
            }
            //Método que valida que un estado pertenezca al país enviado

            if ($request->get('state_id') != '') {
                $state = $request->get('state_id');
                $country = $request->get('country_id');
                $sql_country = DB::connection('tenant')->Table('mo_state')
                    ->where('id', $state)
                    ->select('country_id');
                $datos_country = $sql_country->first();

                if ($datos_country) {
                    if ($datos_country->country_id != $country) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['state_id' => ['state_id not match with country_id']]);
                    }
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                if($request->get('country_id') != '') {
                    $country = Country::where('id', $request->get('country_id'))->first();
                } else {
                    $country = Country::orderBy('order','desc')->first();
                }

                $user = User::create([
                    'name' => ($request->get('name') != '') ? $request->get('name') : null,
                    'surname' => ($request->get('surname') != '') ? $request->get('surname') : null,
                    'document_type_id' => ($request->get('document_type_id') != '') ? $request->get('document_type_id') : null,
                    'number_document' => ($request->get('number_document') != '') ? $request->get('number_document') : null,
                    'sex_id' => ($request->get('sex_id') != '') ? $request->get('sex_id') : null,
                    'birthdate' => ($request->get('birthdate') != '') ? $request->get('birthdate') : null,
                    'email' => $request->get('email'),
                    'email2' => ($request->get('email2') != '') ? $request->get('email2') : null,
                    'password' => Hash::make($request->get('password')),
                    'language_id' => ($request->get('language_id') != '') ? $request->get('language_id') : null,
                    'telephone1' => ($request->get('telephone1') != '') ? $request->get('telephone1') : null,
                    'telephone2' => ($request->get('telephone2') != '') ? $request->get('telephone2') : null,
                    'telephone3' => ($request->get('telephone3') != '') ? $request->get('telephone3') : null,
                    'address' => ($request->get('address') != '') ? $request->get('address') : null,
                    'postal_code' => ($request->get('postal_code') != '') ? $request->get('postal_code') : null,
                    'city' => ($request->get('city') != '') ? $request->get('city') : null,
                    'state_id' => ($request->get('state_id') != '') ? $request->get('state_id') : null,
                    'country_id' => $country->id,
                    'nationality_id' => ($request->get('nationality_id') != '') ? $request->get('nationality_id') : null,
                    'business' => ($request->get('business') != '') ? $request->get('business') : null,
                    'telephone_business' => ($request->get('telephone_business') != '') ? $request->get('telephone_business') : null,
                    'worker' => ($request->get('worker') != '') ? $request->get('worker') : 0,
                    'worker_type_id' => ($request->get('worker_type_id') != '') ? $request->get('worker_type_id') : null,
                    'observations' => ($request->get('observations') != '') ? $request->get('observations') : null
                ]);

                $array['data'] = array();
                $token = $user->createToken('MotorOnline')->accessToken;
                $time_expired = Settings::where('name', 'token_validate')->first()->value;
                $expired_at = Carbon::now()->addMinutes($time_expired);
                UserToken::create([
                    'user_id' => $user->id,
                    'token' => $token,
                    'expired_at' => $expired_at,
                ]);

                $remember_token = $user->createToken('MotorOnline')->accessToken;

                UserRememberToken::create([
                    'user_id' => $user->id,
                    'token' => $remember_token,
                    'ip' => $request->ip(),
                ]);

                $array['data'][] = ['token' => 'Bearer ' . $token, 'remember_token' => $remember_token, 'id' => $user->id];

                PasswordHistory::create([
                    'user_id' => $user->id,
                    'password' => $user->password,
                ]);
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función solicitar password
     *
     * Para solicitar un nuevo password se realizará una peticion POST. Es requerido el parámetro 'email' y 'environment'. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function forgot(Request $request)
    {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();
            $validator = \Validator::make($request->all(), [
                'email' => 'required|email|exists:tenant.mo_user,email,deleted_at,NULL',
                'environment' => 'required|in:front,back,hotel',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }
            //fin validación


            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $user = User::where('email', '=', $request->get('email'))->first();

                if ($user->bloqued_login || $user->bloqued_to >= Carbon::now()) {
                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued']];
                } else {
                    //comprueba si pidió un cambio de contraseña y ya se le envió un email para restablecerla
                    $token_user = PasswordToken::where('user_id', '=', $user->id)->where('expired_at', '>=', Carbon::now())->first();

                    if ($token_user) {
                        $array['error'] = 409;
                        $array['error_description'] = 'Conflict';
                        $array['error_inputs'][0] = ['email' => [0 => 'You have already been sent an email to reset your password']];
                    } else {

                        //borra token que pudiera haber sin borrar porque no se haya llevado a cabo el cambio de contraseña y quedaran expirados pero no borrados
                        $token_no_borrados = PasswordToken::where('user_id', '=', $user->id)->get();
                        if (count($token_no_borrados) > 0) {
                            foreach ($token_no_borrados as $token) {
                                $token->delete();
                            }
                        }
                        //crea token y envía email para restablecer contraseña
                        $token_forgot = $user->createToken('MotorOnline')->accessToken;

                        $time_expired = Settings::where('name', 'recover_token_validate')->first()->value;
                        $expired_at = Carbon::now()->addMinutes($time_expired);

                        //trae link para cambio de password en función del entorno desde el que se solicita
                        // $ruta_api = Settings::where('name', '=', 'api_gateway_route')->first()->value;

                        $ruta_recover_entorno = Settings::where('name', '=', 'recover_route_' . $request->get('environment'))->first()->value;


                        $ruta_recover = $ruta_recover_entorno;
                        /*$params = ['title' => 'Recuperar password',
                            'body' => $ruta_recover . '?token=' . $token_forgot,
                            'template' => 'recover',
                            'emails' => [
                                '0' => [
                                    'email' => $request->get('email'),
                                    'subject_email' => 'Restore password',
                                ]
                            ]];

                        $ruta_final = $ruta_api . '/v1/email_without_token';
                        $client = new Client();
                        $client->request('POST', $ruta_final, ['form_params' => $params, 'http_errors' => false]);*/

                        $emails = [
                            '0' => [
                                'email' => $request->get('email'),
                                'subject_email' => 'Restore password',
                            ]
                        ];
                                               
                        $controller = app(\App\Http\Controllers\tenant\EmailController::class);

                        $peticion = new Request();

                        $peticion->setMethod('POST');

                        $peticion->request->add(['title' => 'Recuperar password']);

                        $peticion->request->add(['body' => $ruta_recover . '?token=' . $token_forgot]);

                        $peticion->request->add(['template' => 'recover']);

                        $peticion->request->add(['emails' => $emails]);
                        
                        $controller->send($peticion);

                        PasswordToken::create([
                            'user_id' => $user->id,
                            'token' => $token_forgot,
                            'expired_at' => $expired_at,
                        ]);

                    }
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }


        return response()->json($array, $array['error']);
    }


    /**
     * Función para cambiar password
     *
     * Para cambiar el password se realizará una peticion POST. Es requerido el nuevo password, la repetición del mismo y el token que se le ha generado al pedir el nuevo password. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function recover(Request $request)
    {

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $passToken = PasswordToken::where('token', '=', $request->get('token'))->where('expired_at', '>=', Carbon::now())->first();

            if ($passToken) {
                //Validacion
                $error = 0;
                $mensaje_validador = collect();

                $validator = \Validator::make($request->all(), [
                    'password' => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                    'c_password' => 'required|same:password',
                    'token' => 'required',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                //controla que no introduzcan espacios en blanco en el password ni caracteres acentuados
                if (preg_match('/\s/', $request->get('password')) || preg_match('/\s/', $request->get('c_password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('c_password'))) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
                }
                if ($error == 1) {
                    $array['error'] = 400;
                    $array['error_description'] = 'The fields are not the required format';
                    $array['error_inputs'][] = $mensaje_validador;
                } else {

                    $usada = false;

                    $guardadas = PasswordHistory::where('user_id', $passToken->user_id)->orderBy('id', 'desc')->limit(4)->get();

                    foreach ($guardadas as $guardada) {
                        if (Hash::check($request->get('password'), $guardada->password)) {
                            $usada = true;
                        }
                    }

                    if (!$usada) {
                        $user = User::find($passToken->user_id);

                        $user->update([
                            'password' => Hash::make($request->get('password')),
                        ]);

                        PasswordHistory::create([
                            'user_id' => $user->id,
                            'password' => $user->password,
                        ]);

                        $passToken->delete();
                        $userToken = UserToken::where('user_id', '=', $passToken->user_id)->get();
                        if ($userToken) {
                            foreach ($userToken as $userT) {
                                $userT->delete();
                            }
                        }
                    } else {
                        $array['error'] = 400;
                        $array['error_description'] = 'The fields are not the required format';
                        $array['error_inputs'][] = ['password' => ['The password entered must be different from the last four.']];
                    }
                }
            } else {
                $array['error'] = 401;
                $array['error_description'] = 'Unauthorized';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para hacer logout
     *
     * Para hacer logout se realizará una peticion POST. Es requerido el token para eliminarlo de base de datos. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public
    function logout(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();
            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {
                    $token = UserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {
                        $token->delete();
                    } else {
                        $array['error'] = 404;
                        $array['error_description'] = 'Data not found';
                    }

                    // Se borra el remember token de este modo para eliminar únicamente el recuérdame del equipo en el que se está operando
                    if ($request->get('remember_token') != '') {
                        UserRememberToken::where('token', '=', $request->get('remember_token'))->delete();
                    }
                }
            } else {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = ['token' => ['Authorization header is required.']];
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función para hacer refresh del token
     *
     * Para hacer refresh se realizará una peticion POST. Es requerido el remember token para generar un nuevo token para el usuario.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $array['error'] = 200;
        try {
            DB::connection('tenant')->beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'remember_token' => 'required'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $remember_token = $request->get('remember_token');

                $user = User::whereHas('userRemeberToken', function ($query) use ($remember_token) {
                    $query->where('token', $remember_token);
                })->where('bloqued_login', '!=', 1)->where(function ($query) {
                    $query->whereNull('bloqued_to')->orWhere('bloqued_to', '<', Carbon::now());
                })->first();

                if ($user) {
                    $token = $user->createToken('MotorOnline')->accessToken;
                    $remember_token = $user->createToken('MotorOnline')->accessToken;

                    UserRememberToken::where('token', '=', $request->get('remember_token'))->update([
                        'token' => $remember_token,
                        'ip' => $request->ip(),
                    ]);

                    UserToken::create([
                        'user_id' => $user->id,
                        'token' => $token,
                        'expired_at' => Carbon::now()->addMinutes(Settings::where('name', 'token_validate')->first()->value),
                    ]);
                    $array['data'][] = ['token' => 'Bearer ' . $token, 'remember_token' => $remember_token];
                } else {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                }

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    function loginsocial(Request $request)
    {
        $array['error'] = 200;
        try {

            DB::connection('tenant')->beginTransaction();

            //Validacion
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'social_id' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'method' => 'required'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            $method = $request->get('method');
            if ($method != 'facebook' && $method != 'google') {
                $error = 1;
                $mensaje_validador = 'Method not implemented';
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $user = User::where('email', '=', $request->get('email'))->first();

                //If no exist, create new user
                if (!$user) {

                    //Generate Password

                    //$password = null;
                    
                    $length_pass = 16;
                    $password = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!$#%', ceil($length_pass / strlen($x)))), 1, $length_pass);
                    


                    //Search Country
                    $country_id = null;
                    if ($request->get('country_code') != null && $request->get('country_code') != '') {

                        $country = Country::where('iso3', $request->get('country_code'))->first();
                        if (isset($country->id)) {
                            $country_id = $country->id;
                        }
                    }

                    //Default language
                    $language_id = null;
                    if ($request->get('lang_code') != null && $request->get('lang_code') != '') {
                        $lang = DB::connection('tenant')->table('mo_language')->where('abbreviation', $request->get('lang_code'))->whereNull('deleted_at')->first();
                        if (isset($lang->id)) {
                            $language_id = $lang->id;
                        }
                    }

                    if(is_null($country_id)) {
                        $country_id = Country::orderBy('order','desc')->first()->id;
                    }


                    $user = User::create([
                        'name' => ($request->get('first_name') != '') ? $request->get('first_name') : null,
                        'surname' => ($request->get('last_name') != '') ? $request->get('last_name') : null,
                        'document_type_id' => null,
                        'number_document' => null,
                        'sex_id' => null,
                        'birthdate' => ($request->get('birthdate') != '') ? $request->get('birthdate') : null,
                        'email' => $request->get('email'),
                        'email2' => ($request->get('email2') != '') ? $request->get('email2') : null,
                        'password' => Hash::make($password),
                        'language_id' => $language_id,
                        'worker' => 0,
                        'worker_type_id' => null,
                        'observations' => null,
                        'country_id' => $country_id,
                        'social_register' => 0
                    ]);

                    $user->save();

                    if ($method == 'facebook') {
                        $user->update([
                            'facebook_id' => $request->get('social_id'),
                        ]);
                        $user->save();
                    } elseif ($method == 'google') {
                        $user->update([
                            'google_id' => $request->get('social_id'),
                        ]);
                        $user->save();
                    }

                    $array['data'] = array();
                    $token = $user->createToken('MotorOnline')->accessToken;
                    $remember_token = $user->createToken('MotorOnline')->accessToken;
                    $time_expired = Settings::where('name', 'token_validate')->first()->value;
                    $expired_at = Carbon::now()->addMinutes($time_expired);

                    UserToken::create([
                        'user_id' => $user->id,
                        'token' => $token,
                        'expired_at' => $expired_at,
                    ]);

                    UserRememberToken::create([
                        'user_id' => $user->id,
                        'token' => $remember_token,
                        'ip' => $request->ip(),
                    ]);

                    PasswordHistory::create([
                        'user_id' => $user->id,
                        'password' => $user->password,
                    ]);

                    $remember_token = $user->createToken('MotorOnline')->accessToken;

                    UserRememberToken::create([
                        'user_id' => $user->id,
                        'token' => $remember_token,
                        'ip' => $request->ip(),
                    ]);

                    $array['data'][] = ['user_id' => $user->id, 'token' => 'Bearer ' . $token, 'remember_token' => $remember_token];


                } elseif ($user->bloqued_login == 1) {

                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued']];

                } elseif ($user->bloqued_to >= Carbon::now()) {

                    $array['error'] = 403;
                    $array['error_description'] = 'Forbidden';
                    $array['error_inputs'][0] = ['email' => [0 => 'User bloqued temporarily']];

                } elseif (Auth::loginUsingId($user->id)) {

                    $error_social = 0;

                    if ($method == 'facebook') {
//                        if (isset($user->facebook_id) && intval($user->facebook_id) > 0) {
//
//                            if (intval($user->facebook_id) != intval($request->get('social_id'))) {
//                                $error_social = 1;
//                            }
//
//                        } else {
                            $user->update([
                                'facebook_id' => $request->get('social_id'),
                            ]);
                            $user->save();
//                        }
                    } elseif ($method == 'google') {
//                        if (isset($user->google_id) && intval($user->google_id) > 0) {
//
//                            if (intval($user->google_id) != intval($request->get('social_id'))) {
//                                $error_social = 1;
//                            }
//
//                        } else {
                            $user->update([
                                'google_id' => $request->get('social_id'),
                            ]);
                            $user->save();
//                        }
                    } else {
                        $error_social = 1;
                    }


                    if ($error_social == 0) {

                        $array['data'] = array();
                        $token = $user->createToken('MotorOnline')->accessToken;

                        $remember_token = null;

                        $time_expired = Settings::where('name', 'token_validate')->first()->value;
                        $expired_at = Carbon::now()->addMinutes($time_expired);

                        UserToken::create([
                            'user_id' => $user->id,
                            'token' => $token,
                            'expired_at' => $expired_at,
                        ]);

                        $remember_token = $user->createToken('MotorOnline')->accessToken;

                        UserRememberToken::create([
                            'user_id' => $user->id,
                            'token' => $remember_token,
                            'ip' => $request->ip(),
                        ]);


                        $user->faults_login = 0;
                        $user->save();
                        $array['data'][] = ['user_id' => $user->id, 'token' => 'Bearer ' . $token, 'remember_token' => $remember_token];

                    } else {
                        $array['error'] = 403;
                        $array['error_description'] = 'Forbidden';
                        $array['error_inputs'][0] = ['social_id' => [0 => 'Facebook Token error.']];
                    }

                } else {
                    $array = ['error' => 401, 'error_description' => 'Unauthorized'];
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    /**
     * Función para cambiar password unicamente si el usuario tiene social_register a 0
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changepassword(Request $request)
    {

        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            if ($request->header('Authorization')) {
                $token_api = explode(' ', $request->header('Authorization'));
                if ($request->headers->has('Authorization')) {

                    $token = UserToken::where('token', '=', $token_api[1])->first();
                    if ($token) {

                        $error = 0;
                        $mensaje_validador = collect();

                        $validator = \Validator::make($request->all(), [
                            'password' => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
                            'c_password' => 'required|same:password'
                        ]);

                        if ($validator->fails()) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge($validator->errors());
                        }

                        //controla que no introduzcan espacios en blanco en el password ni caracteres acentuados
                        if (preg_match('/\s/', $request->get('password')) || preg_match('/\s/', $request->get('c_password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('password')) || preg_match('/^.*(?=.*[áéíóúÁÉÍÓÚ]).*$/', $request->get('c_password'))) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['password' => ['You can not enter spaces or accented characters in the password.']]);
                        }
                        if ($error == 1) {
                            $array['error'] = 400;
                            $array['error_description'] = 'The fields are not the required format';
                            $array['error_inputs'][] = $mensaje_validador;
                        } else {

                            $user = User::find($token->user_id);
                            
                            // El usuario se ha registrado con sociallogin y ademas tiene social_register a 0
                            if (($user->facebook_id || $user->google_id) && $user->social_register === 0) {

                                $user->update([
                                    'password' => Hash::make($request->get('password')),
                                    'social_register' => 1
                                ]);

                                PasswordHistory::create([
                                    'user_id' => $user->id,
                                    'password' => $user->password
                                ]);
                                
                            } else {
                                $array['error'] = 400;
                                $array['error_description'] = 'The fields are not the required format';
                                $array['error_inputs'][] = ['password' => ['The password entered must be different from the last four.']];
                            }
                        }
                    } else {
                        $array['error'] = 401;
                        $array['error_description'] = 'Unauthorized';
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    public function reservationLogin(Request $request){
        $array['error'] = 200;
        try{

            DB::connection('tenant')->beginTransaction();

            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'reservation_confirm' => 'boolean|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if($request->get('reservation_confirm') == 1){
                $user = User::where('email', $request->get('email'))
                    ->first();

                if(!$user) {
                    $validator = \Validator::make($request->all(), [
                        'name' => 'required',
                        'surname' => 'required',
                    ]);

                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                }
            } else {
                $validator = \Validator::make($request->all(), [
                    'arrival_date' => 'required|date|date_format:"Y-m-d"',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $error = 0;

                $user = User::where('email', $request->get('email'))
                    ->first();

                if($user) {
                    if ($user->bloqued_login == 1) {
                        $error = 1;
                        $array['error'] = 403;
                        $array['error_description'] = 'Forbidden';
                        $array['error_inputs'][0] = ['email' => [0 => 'User bloqued']];

                    } elseif ($user->bloqued_to >= Carbon::now()) {
                        $error = 1;
                        $array['error'] = 403;
                        $array['error_description'] = 'Forbidden';
                        $array['error_inputs'][0] = ['email' => [0 => 'User bloqued temporarily']];

                    }
                }

                if($error == 0) {
                    $settings = Settings::all();

                    $time_expired = $settings->where('name', 'token_validate')->first()->value;

                    $api_route = $settings->where('name', 'opera_endpoint')->first()->value;
                    $api_user = $settings->where('name', 'opera_user')->first()->value;
                    $api_password = $settings->where('name', 'opera_password')->first()->value;

                    $client = new Client();

                    $user_data = array();
                    $reservas = array();

                    if($request->get('reservation_confirm') == 1) {
                        $user = User::where('email', $request->get('email'))
                            ->first();

                        if(!$user) {
                            if($request->get('country') != '') {
                                $country = Country::where('iso2', $request->get('country'))->first();
                            } else {
                                $country = Country::orderBy('order','desc')->first();
                            }

                            $user = User::create([
                                'name' => $request->get('name'),
                                'surname' => $request->get('surname'),
                                'email' => $request->get('email'),
                                'telephone1' => $request->get('telephone'),
                                'country_id' => $country->id
                            ]);
                        }

                        $token = $user->createToken('MotorOnline')->accessToken;
                        $remember_token = $user->createToken('MotorOnline')->accessToken;

                        $expired_at = Carbon::now()->addMinutes($time_expired);

                        UserToken::create([
                            'user_id' => $user->id,
                            'token' => $token,
                            'expired_at' => $expired_at,
                        ]);

                        UserRememberToken::create([
                            'user_id' => $user->id,
                            'token' => $remember_token,
                            'ip' => $request->ip(),
                        ]);

                        $user_data = [
                            'user_id' => $user->id,
                            'token' => 'Bearer ' . $token,
                            'remember_token' => $remember_token
                        ];
                    } else {

                        $total_reservas = 0;

                        if($request->get('reservation_code') != '') {

                            $res = $client->request('GET', $api_route . '?$filter=' . "EXTERNAL_REFERENCE eq '" . $request->get('reservation_code') . "'", ['auth'    => [
                                $api_user,
                                $api_password
                            ], 'http_errors' => false]);

                            $res = $res->getBody()->getContents();

                            $res = str_replace('m:properties', 'properties',$res);
                            $res = str_replace('d:', 'd-', $res);
                            $res = str_replace('m:', 'm-', $res);
                            $respuesta = simplexml_load_string($res);

                            foreach ($respuesta->entry as $reserva) {

                                $arrival_date = (string)$reserva->content->properties->{'d-ARRIVAL'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL'})->format('Y-m-d') : '';
                                if ($arrival_date == $request->get('arrival_date')) {

                                    $total_reservas++;

                                    $new_reserva = array();

                                    $new_reserva['external_reference'] = (string) $reserva->content->properties->{'d-EXTERNAL_REFERENCE'};
                                    $new_reserva['nights'] = (string) $reserva->content->properties->{'d-NIGHTS'};
                                    $new_reserva['rooms'] = (string) $reserva->content->properties->{'d-NO_OF_ROOMS'};
                                    $new_reserva['room_category'] = (string) $reserva->content->properties->{'d-ROOM_CATEGORY_LABEL'};
                                    $new_reserva['room_class'] = (string) $reserva->content->properties->{'d-ROOM_CLASS'};
                                    $new_reserva['rate_code'] = (string) $reserva->content->properties->{'d-RATE_CODE'};
                                    $new_reserva['arrival'] = (string)$reserva->content->properties->{'d-ARRIVAL'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL'})->format('Y-m-d H:i:s') : '';
                                    $new_reserva['arrival_time'] = (string)$reserva->content->properties->{'d-ARRIVAL_TIME'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL_TIME'})->format('H:i:s') : '';
                                    $new_reserva['departure'] = (string)$reserva->content->properties->{'d-DEPARTURE'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-DEPARTURE'})->format('Y-m-d') : '';
                                    $new_reserva['reservation_status'] = (string) $reserva->content->properties->{'d-RESV_STATUS'};
                                    $new_reserva['adults'] = (int) $reserva->content->properties->{'d-ADULTS'};
                                    $new_reserva['childs'] = (int) $reserva->content->properties->{'d-CHILDREN'};

                                    $new_reserva['user_name'] = (string) $reserva->content->properties->{'d-GUEST_FIRST_NAME'};
                                    $new_reserva['surname'] = (string) $reserva->content->properties->{'d-GUEST_NAME'};
                                    $new_reserva['country'] = $reserva->content->properties->{'d-GUEST_COUNTRY'} != 'None' ? (string) $reserva->content->properties->{'d-GUEST_COUNTRY'} : null;
                                    $new_reserva['phone'] = $reserva->content->properties->{'d-GUEST_PHONE'} != 'None' ? (string) $reserva->content->properties->{'d-GUEST_PHONE'} : null;
                                    $new_reserva['room'] = (string) $reserva->content->properties->{'d-ROOM'};

                                    $reservas [] = $new_reserva;
                                }
                            }

                            if($total_reservas == 0) {

                                $res = $client->request('GET', $api_route . '?$filter=' . "CONFIRMATION_NO eq '" . $request->get('reservation_code') . "'", ['auth' => [
                                    $api_user,
                                    $api_password
                                ], 'http_errors' => false]);

                                $res = $res->getBody()->getContents();

                                $res = str_replace('m:properties', 'properties', $res);
                                $res = str_replace('d:', 'd-', $res);
                                $res = str_replace('m:', 'm-', $res);
                                $respuesta = simplexml_load_string($res);

                                foreach ($respuesta->entry as $reserva) {

                                    $agrupamiento = array_search((string)$reserva->content->properties->{'d-EXTERNAL_REFERENCE'}, array_column($reservas, 'external_reference'));

                                    if ($agrupamiento === false) {

                                        $arrival_date = (string)$reserva->content->properties->{'d-ARRIVAL'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL'})->format('Y-m-d') : '';
                                        if ($arrival_date == $request->get('arrival_date')) {

                                            $total_reservas++;

                                            $new_reserva = array();

                                            $new_reserva['external_reference'] = (string)$reserva->content->properties->{'d-EXTERNAL_REFERENCE'};
                                            $new_reserva['nights'] = (string)$reserva->content->properties->{'d-NIGHTS'};
                                            $new_reserva['rooms'] = (string)$reserva->content->properties->{'d-NO_OF_ROOMS'};
                                            $new_reserva['room_category'] = (string)$reserva->content->properties->{'d-ROOM_CATEGORY_LABEL'};
                                            $new_reserva['room_class'] = (string)$reserva->content->properties->{'d-ROOM_CLASS'};
                                            $new_reserva['rate_code'] = (string)$reserva->content->properties->{'d-RATE_CODE'};
                                            $new_reserva['arrival'] = (string)$reserva->content->properties->{'d-ARRIVAL'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL'})->format('Y-m-d H:i:s') : '';
                                            $new_reserva['arrival_time'] = (string)$reserva->content->properties->{'d-ARRIVAL_TIME'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL_TIME'})->format('H:i:s') : '';
                                            $new_reserva['departure'] = (string)$reserva->content->properties->{'d-DEPARTURE'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-DEPARTURE'})->format('Y-m-d') : '';
                                            $new_reserva['reservation_status'] = (string)$reserva->content->properties->{'d-RESV_STATUS'};
                                            $new_reserva['adults'] = (int)$reserva->content->properties->{'d-ADULTS'};
                                            $new_reserva['childs'] = (int)$reserva->content->properties->{'d-CHILDREN'} +
                                                (int)$reserva->content->properties->{'d-CHILDREN1'} +
                                                (int)$reserva->content->properties->{'d-CHILDREN2'} +
                                                (int)$reserva->content->properties->{'d-CHILDREN3'};

                                            $new_reserva['user_name'] = (string)$reserva->content->properties->{'d-GUEST_FIRST_NAME'};
                                            $new_reserva['surname'] = (string)$reserva->content->properties->{'d-GUEST_NAME'};
                                            $new_reserva['country'] = $reserva->content->properties->{'d-GUEST_COUNTRY'} != 'None' ? (string)$reserva->content->properties->{'d-GUEST_COUNTRY'} : null;
                                            $new_reserva['phone'] = $reserva->content->properties->{'d-GUEST_PHONE'} != 'None' ? (string)$reserva->content->properties->{'d-GUEST_PHONE'} : null;
                                            $new_reserva['room'] = (string)$reserva->content->properties->{'d-ROOM'};

                                            $reservas [] = $new_reserva;
                                        }
                                    }
                                }
                            }
                        }

                        if($total_reservas == 0) {
                            $res = $client->request('GET', $api_route . '?$filter=' . "GUEST_EMAIL eq '" . $request->get('email') . "'", ['auth'    => [
                                $api_user,
                                $api_password
                            ],'http_errors' => false]);

                            $res = $res->getBody()->getContents();

                            $res = str_replace('m:properties', 'properties',$res);
                            $res = str_replace('d:', 'd-', $res);
                            $res = str_replace('m:', 'm-', $res);
                            $respuesta = simplexml_load_string($res);

                            foreach ($respuesta->entry as $reserva) {

                                $arrival_date = (string)$reserva->content->properties->{'d-ARRIVAL'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL'})->format('Y-m-d') : '';
                                if ($arrival_date == $request->get('arrival_date')) {

                                    $new_reserva = array();

                                    $new_reserva['external_reference'] = (string)$reserva->content->properties->{'d-EXTERNAL_REFERENCE'};
                                    $new_reserva['confirmation_number'] = (string)$reserva->content->properties->{'d-CONFIRMATION_NO'};
                                    $new_reserva['nights'] = (string)$reserva->content->properties->{'d-NIGHTS'};
                                    $new_reserva['rooms'] = (string)$reserva->content->properties->{'d-NO_OF_ROOMS'};
                                    $new_reserva['room_category'] = (string)$reserva->content->properties->{'d-ROOM_CATEGORY_LABEL'};
                                    $new_reserva['room_class'] = (string)$reserva->content->properties->{'d-ROOM_CLASS'};
                                    $new_reserva['rate_code'] = (string)$reserva->content->properties->{'d-RATE_CODE'};
                                    $new_reserva['arrival'] = (string)$reserva->content->properties->{'d-ARRIVAL'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL'})->format('Y-m-d H:i:s') : '';
                                    $new_reserva['arrival_time'] = (string)$reserva->content->properties->{'d-ARRIVAL_TIME'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-ARRIVAL_TIME'})->format('H:i:s') : '';
                                    $new_reserva['departure'] = (string)$reserva->content->properties->{'d-DEPARTURE'} != 'None' ? Carbon::parse((string)$reserva->content->properties->{'d-DEPARTURE'})->format('Y-m-d') : '';
                                    $new_reserva['reservation_status'] = (string)$reserva->content->properties->{'d-RESV_STATUS'};
                                    $new_reserva['adults'] = (int)$reserva->content->properties->{'d-ADULTS'};
                                    $new_reserva['childs'] = (int)$reserva->content->properties->{'d-CHILDREN'} +
                                        (int)$reserva->content->properties->{'d-CHILDREN1'} +
                                        (int)$reserva->content->properties->{'d-CHILDREN2'} +
                                        (int)$reserva->content->properties->{'d-CHILDREN3'};

                                    $new_reserva['user_name'] = (string)$reserva->content->properties->{'d-GUEST_FIRST_NAME'};
                                    $new_reserva['surname'] = (string)$reserva->content->properties->{'d-GUEST_NAME'};
                                    $new_reserva['country'] = $reserva->content->properties->{'d-GUEST_COUNTRY'} != 'None' ? (string)$reserva->content->properties->{'d-GUEST_COUNTRY'} : null;
                                    $new_reserva['phone'] = $reserva->content->properties->{'d-GUEST_PHONE'} != 'None' ? (string)$reserva->content->properties->{'d-GUEST_PHONE'} : null;
                                    $new_reserva['room'] = (string)$reserva->content->properties->{'d-ROOM'};

                                    $reservas [] = $new_reserva;
                                }
                            }
                        }
                    }

                    $array['data'][] = [
                        'user' => $user_data,
                        'reservation' => $reservas
                    ];
                }
            }

            DB::connection('tenant')->commit();
        }catch (\Exception $e){
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

    public function saasLogin(Request $request) {

        $array['error'] = 200;

        try{

            DB::connection('tenant')->beginTransaction();
            
            $error = 0;
            $mensaje_validador = collect();

            if (!$request->header('Authorization')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['token' => ['Authorization header is required.']]);
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $token_api = explode(' ', $request->header('Authorization'));
                $token = UserToken::where('token', '=', $token_api[1])->first();

                if ($token) {
                    
                    $user = User::where('id', $token->user_id)->first();

                    if (!$user || !$user->system_user_id) {
                        $array['error'] = 404;
                        $array['error_description'] = 'User not found';
                    } else {
                        
                        $saas_user = SaasUser::where('id', $user->system_user_id)->first();

                        $token = $saas_user->createToken('MotorOnline')->accessToken;

                        SaasUserToken::create([
                            'user_id' => $saas_user->id,
                            'token' => $token,
                            'expired_at' => Carbon::now()->addMinutes(SaasSettings::where('name', 'token_validate')->first()->value),
                        ]);

                        $array['data'][] = ['token' => encriptar($token)];
                    }
                } else {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                }
            }

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Usuarios');
        }

        return response()->json($array, $array['error']);
    }

}

