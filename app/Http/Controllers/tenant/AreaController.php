<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Continent;
use App\Models\tenant\ContinentTranslation;
use App\Models\tenant\Country;
use App\Models\tenant\CountryTranslation;
use App\Models\tenant\State;
use App\Exceptions\Handler;

use App\Models\tenant\Language;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\tenant\Settings;


class AreaController extends Controller
{
    /**
     * Función que lista todos los continentes
     *
     * Para la consulta de continentes se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showContinent(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $error = 0;

            // Validación
            $mensaje_validador = collect();


            $validator = Validator::make($request->all(), [
                'page' => 'integer',
                'limit' => 'integer',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $sql_continent = DB::connection('tenant')->table('mo_continent')
                    ->select('mo_continent.id', 'mo_continent.iso2')
                    ->where('mo_continent.deleted_at', '=', null)
                    ->join('mo_continent_translation', 'mo_continent_translation.continent_id', 'mo_continent.id')
                    ->where('mo_continent_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_continent')
                    ->select('mo_continent.id')
                    ->where('mo_continent.deleted_at', '=', null)
                    ->join('mo_continent_translation', 'mo_continent_translation.continent_id', 'mo_continent.id')
                    ->where('mo_continent_translation.deleted_at', '=', null);

                if ($request->get('lang') != '') {
                    $sql_continent->where('mo_continent_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_continent_translation.language_id', '=', $idioma->id);
                }

                // Order
                $orden = 'mo_continent_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_continent_translation.name';
                            break;
                        case 'iso2':
                            $orden = 'mo_continent.iso2';
                            break;
                        case 'id':
                            $orden = 'mo_continent.id';
                            break;
                        default:
                            $orden = 'mo_continent_translation.name';
                            break;
                    }
                }

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $continent = $sql_continent->groupBy('mo_continent.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $continent = $continent->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $continent = $continent->get();

                }

                //Fin de paginación



                $sub->groupBy('mo_continent_translation.continent_id');
                $continent_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $continent_count->count();


                $array['data'] = array();
                foreach ($continent as $conti) {

                    foreach ($idiomas as $idi) {
                        $traduccion = ContinentTranslation::where('continent_id', '=', $conti->id)
                            ->select('id', 'language_id', 'name')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $conti->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['continent'][] = $conti;
                }
                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Áreas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todos los paises
     *
     * Para la consulta de paises se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCountry(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front',1)->get();

            $error = 0;

            // Validación
            $mensaje_validador = collect();


            $validator = Validator::make($request->all(), [
                'page' => 'integer',
                'limit' => 'integer',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang') != '') {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front',1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $sql_country = DB::connection('tenant')->table('mo_country')
                    ->select('mo_country.id','mo_country.continent_id', 'mo_country.iso3','mo_country.order')
                    ->where('mo_country.deleted_at', '=', null)
                    ->join('mo_country_translation', 'mo_country_translation.country_id', 'mo_country.id')
                    ->where('mo_country_translation.deleted_at', '=', null)
                    ->join('mo_continent', 'mo_country.continent_id', 'mo_continent.id')
                    ->where('mo_continent.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_country')
                    ->select('mo_country.id')
                    ->where('mo_country.deleted_at', '=', null)
                    ->join('mo_country_translation', 'mo_country_translation.country_id', 'mo_country.id')
                    ->where('mo_country_translation.deleted_at', '=', null)
                    ->join('mo_continent', 'mo_country.continent_id', 'mo_continent.id')
                    ->where('mo_continent.deleted_at', '=', null);

                if ($request->get('lang') != '') {
                    $sql_country->where('mo_country_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_country_translation.language_id', '=', $idioma->id);
                }

                if ($request->get('continent_id') != '') {
                    $sql_country->where('mo_country.continent_id', '=', $request->get('continent_id'));
                    $sub->where('mo_country.continent_id', '=', $request->get('continent_id'));
                }

                // Order
                $orden = 'mo_country_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_country_translation.name';
                            break;
                        case 'iso3':
                            $orden = 'mo_country.iso3';
                            break;
                        case 'order':
                            $orden = 'mo_country.order';
                            break;
                        case 'id':
                            $orden = 'mo_country.id';
                            break;
                        case 'continent_id':
                            $orden = 'mo_country.continent_id';
                            break;
                        default:
                            $orden = 'mo_country_translation.name';
                            break;
                    }
                }

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $country = $sql_country->groupBy('mo_country.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $country = $country->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $country = $country->get();

                }

                //Fin de paginación



                $sub->groupBy('mo_country_translation.country_id');
                $country_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $country_count->count();


                $array['data'] = array();
                foreach ($country as $coun) {

                    foreach ($idiomas as $idi) {
                        $traduccion = CountryTranslation::where('country_id', '=', $coun->id)
                            ->select('id', 'language_id', 'name')
                            ->where('language_id', '=', $idi->id)
                            ->get();
                        foreach ($traduccion as $trad) {
                            $coun->lang[][$idi->abbreviation] = $trad;
                        }
                    }

                    $array['data'][0]['country'][] = $coun;
                }
                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Áreas');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que lista todos los estados de los paises
     *
     * Para la consulta de estados se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showState(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $error = 0;

            // Validación
            $mensaje_validador = collect();


            $validator = Validator::make($request->all(), [
                'page' => 'integer',
                'limit' => 'integer',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }


            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {
                $sql_state = DB::connection('tenant')->table('mo_state')
                    ->select('mo_state.id','mo_state.country_id', 'mo_state.iso3','mo_state.name')
                    ->where('mo_state.deleted_at', '=', null)
                    ->join('mo_country', 'mo_state.country_id', 'mo_country.id')
                    ->where('mo_country.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_state')
                    ->select('mo_state.id')
                    ->where('mo_state.deleted_at', '=', null)
                    ->join('mo_country', 'mo_state.country_id', 'mo_country.id')
                    ->where('mo_country.deleted_at', '=', null);


                if ($request->get('country_id') != '') {
                    $sql_state->where('mo_state.country_id', '=', $request->get('country_id'));
                    $sub->where('mo_state.country_id', '=', $request->get('country_id'));
                }

                // Order
                $orden = 'mo_state.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_state.name';
                            break;
                        case 'iso3':
                            $orden = 'mo_state.iso3';
                            break;
                        case 'id':
                            $orden = 'mo_state.id';
                            break;
                        case 'country_id':
                            $orden = 'mo_state.country_id';
                            break;
                        default:
                            $orden = 'mo_state.name';
                            break;
                    }
                }

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $state = $sql_state->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $state = $state->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $state = $state->get();

                }

                //Fin de paginación

                $state_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);

                $totales = $state_count->count();


                $array['data'] = array();

                $array['data'][0]['state'] = $state;

                $array['total_results'] = $totales;

            }

            DB::connection('tenant')->commit();
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Áreas');
        }
        return response()->json($array, $array['error']);
    }
}