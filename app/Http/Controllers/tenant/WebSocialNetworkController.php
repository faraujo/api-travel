<?php


namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\WebSocialNetwork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;
use App\Models\tenant\Settings;
use Illuminate\Support\Str;

class WebSocialNetworkController extends Controller
{

    public function show(Request $request) {
        $array['error'] = 200;
        try{
            DB::connection('tenant')->beginTransaction();
            
            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'numeric',
                'limit' => 'numeric',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                $sql_social_network = DB::connection('tenant')->table('mo_web_social_network')
                    ->select(
                        'mo_web_social_network.id',
                        'mo_web_social_network.social_network',
                        'mo_web_social_network.external_url'
                    )
                    ->whereNull('mo_web_social_network.deleted_at');

                $sub = DB::connection('tenant')->table('mo_web_social_network')
                    ->select(
                        'mo_web_social_network.id'
                    )
                    ->whereNull('mo_web_social_network.deleted_at');

                $orden = 'mo_web_social_network.social_network';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'social_network':
                            $orden = 'mo_web_social_network.social_network';
                            break;
                        case 'id':
                            $orden = 'mo_web_social_network.id';
                            break;
                        case 'external_url':
                            $orden = 'mo_web_social_network.external_url';
                            break;
                        default:
                            $orden = 'mo_web_social_network.social_network';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $sql_social_network = $sql_social_network->groupBy('mo_web_social_network.id')->orderBy($orden, $sentido);

                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }


                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $datos_social_network = $sql_social_network->forPage($inicio, $limite)->get();
                    //si filtro limit = 0 se obtienen todos los resultados
                } else {
                    $datos_social_network = $sql_social_network->get();
                }
                //Fin de paginación

                $totales = $sub->count();

                $array['data'] = array();
                foreach ($datos_social_network as $social_network) {
                    $array_social_network = array();

                    $array_social_network['id'] = $social_network->id;
                    $array_social_network['social_network'] = $social_network->social_network;
                    $array_social_network['external_url'] = $social_network->external_url;

                    
                    $array['data'][0]['socialnetwork'][] = $array_social_network;
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Social network');

        }

        return response()->json($array, $array['error']);
    }

    public function showId($id) {
        $array['error'] = 200;

        try {
            DB::connection('tenant')->beginTransaction();

            $sql_social_network = DB::connection('tenant')->table('mo_web_social_network')
                    ->select(
                        'mo_web_social_network.id',
                        'mo_web_social_network.social_network',
                        'mo_web_social_network.external_url'
                    )
                    ->whereNull('mo_web_social_network.deleted_at')
                    ->where('mo_web_social_network.id', $id);

            $datos_social_network = $sql_social_network->get();
            $array['data'] = array();
            foreach($datos_social_network as $social_network) {
                $array_social_network = array();

                $array_social_network['id'] = $social_network->id;
                $array_social_network['social_network'] = $social_network->social_network;
                $array_social_network['external_url'] = $social_network->external_url;

                

                $array['data'][0]['socialnetwork'][] = $array_social_network;
            }

            

            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Social network');
        }

        return response()->json($array, $array['error']);
    }

    public function update(Request $request){
        $array['error'] = 200;

        try{
            DB::connection('tenant')->beginTransaction();

            $array_traducciones = array();

            $error = 0;

            // Validación
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                //'id' => 'required|integer|min:0'
                'socialnetworks' => 'array|required',
                'socialnetworks.*.id' => 'integer|min:0|exists:tenant.mo_web_social_network,id,deleted_at,NULL|required',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {

                foreach ($request->get('socialnetworks') as $socialnetwork) {
                    $social_network = WebSocialNetwork::where('id', $socialnetwork['id'])->first();

                    if($social_network) {
                        $social_network->external_url = isset($socialnetwork['external_url']) ? $socialnetwork['external_url'] : null;
                        $social_network->save(); 
                    } 
                }
            }
            DB::connection('tenant')->commit();

        }catch(\Exception $e) {
            DB::connection('tenant')->rollback();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Social network');

        }

        return response()->json($array, $array['error']);
    }

}