<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\tenant\Language;

class LanguageController extends Controller
{


    /**
     * Función que lista todos los idiomas
     *
     * Para la consulta de idiomas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {

        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            // Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = Validator::make($request->all(), [
                'front' => 'boolean',
                'back' => 'boolean',
            ]);
            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            // FIN Validación

            if ($error == 1) {
                //Si se produce error en validación se envía mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Preparación de las consultas
                $idiomas = Language::all();

                if ($request->get('front') != '') {
                    $idiomas = $idiomas->where('front', '=', $request->get('front'));
                }

                if ($request->get('back') != '') {
                    $idiomas = $idiomas->where('back', '=', $request->get('back'));
                }

                $array['data'] = array();
                foreach ($idiomas as $idioma) {
                    $array['data'][0]['language'][] = $idioma;
                }
                $array['total_results'] = count($idiomas);

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Idiomas');
        }

        return response()->json($array, $array['error']);
    }

        /**
     * Función que lista información de un idioma filtrado por su id
     *
     * Para la consulta de idiomas se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id,Request $request)
    {

        $array['error'] = 200;

        try {

          
                //Preparación de las consultas
                $idiomas = Language::all();

                $idiomas = $idiomas->where('id','=',$id);
              
                $array['data'] = array();
                foreach ($idiomas as $idioma) {
                    $array['data'][0]['language'][] = $idioma;
                }
                $array['total_results'] = count($idiomas);

    

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Idiomas');
        }

        return response()->json($array, $array['error']);
    }

}
