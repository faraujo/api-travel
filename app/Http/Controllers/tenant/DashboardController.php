<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Models\tenant\Currency;
use App\Exceptions\Handler;
use App\Models\tenant\Exchange;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Models\tenant\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class DashboardController extends Controller
{


    /**
     * Función para mostrar el dashboard.
     *
     * Para la consulta de dashboard se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();
            
            // CONTRATOS
            $contracts = DB::connection('tenant')->table('mo_contract_model')
                ->select('mo_contract_model.id')
                ->where('mo_contract_model.deleted_at', '=', null)
                ->join('mo_contract_model_translation', 'mo_contract_model.id', 'mo_contract_model_translation.contract_model_id')
                ->where('mo_contract_model_translation.deleted_at', '=', null)
                ->groupBy('mo_contract_model.id');

            $contracts_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$contracts->toSql()}) as sub"))
                ->mergeBindings($contracts);
            $array['data']['contracts'] = $contracts_count->count();

            // USUARIOS
            $users = DB::connection('tenant')->table('mo_user')
                ->select('mo_user.id')
                ->where('mo_user.deleted_at', '=', null);
            $array['data']['users'] = $users->count();

            // PRODUCTOS
            $productos = DB::connection('tenant')->table('mo_product')
                ->select('mo_product.id')
                ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                ->join('mo_product_location', 'mo_product.id', '=', 'mo_product_location.product_id')
                ->join('mo_location', 'mo_product_location.location_id', '=', 'mo_location.id')
                ->where('mo_product_translation.deleted_at', '=', null)
                ->where('mo_product.deleted_at', '=', null)
                ->where('mo_product_location.deleted_at', '=', null)
                ->where('mo_location.deleted_at', '=', null)
                ->groupBy('mo_product.id');

            $productos_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$productos->toSql()}) as sub"))
                ->mergeBindings($productos);
            $array['data']['products'] = $productos_count->count();

            // PROMOCIONES
            $promotions = DB::connection('tenant')->table('mo_promotion')
                ->select('mo_promotion.id')
                ->join('mo_promotion_translation', 'mo_promotion.id', '=', 'mo_promotion_translation.promotion_id')
                ->where('mo_promotion_translation.deleted_at', '=', null)
                ->where('mo_promotion.deleted_at', '=', null)
                ->groupBy('mo_promotion.id');

            $promotions_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$promotions->toSql()}) as sub"))
                ->mergeBindings($promotions);
            $array['data']['promotions'] = $promotions_count->count();

            // PRODUCTOS MAS VISITADOS (Limitado a 6 productos)
            $products_viewed = DB::connection('tenant')->table('mo_search_log')
                ->select('mo_search_log.product_id', 'mo_product_translation.name', DB::connection('tenant')->raw('count(mo_search_log.product_id) as count'))
                ->join('mo_product', 'mo_product.id','=','mo_search_log.product_id')
                ->join('mo_product_translation', 'mo_product.id', '=', 'mo_product_translation.product_id')
                ->where('mo_search_log.deleted_at', '=', null)
                ->where('mo_search_log.product_id', '!=', null)
                ->where('mo_search_log.created_at', '>=', Carbon::now()->startOfDay()->subMonths(2))
                ->where('mo_search_log.created_at', '<=', Carbon::now()->startOfDay())
                ->groupBy('mo_search_log.product_id');
            $array['data']['products_viewed'] = $products_viewed->get();

            // VOLUMEN DE BUSQUEDAS (Ultimos 90 dias)
            $products_search_volume = DB::connection('tenant')->table('mo_search_log')
                ->select('mo_search_log.product_id', DB::connection('tenant')->raw('MONTH(mo_search_log.created_at) as month'), DB::connection('tenant')->raw('count(mo_search_log.id) as count'))
                ->whereNull('mo_search_log.deleted_at')
                ->where('mo_search_log.created_at', '>=', Carbon::now()->startOfDay()->subMonths(2))
                ->where('mo_search_log.created_at', '<=', Carbon::now()->startOfDay())
                ->groupBy('month');
            $array['data']['search_volume'] = $products_search_volume->get();

            // PLAN
            $website  = \Hyn\Tenancy\Facades\TenancyFacade::website();

            $website = DB::table('saas_website')
                            ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                            'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                            'saas_website.default_language_id', 'saas_website.default_currency_id')
                            ->whereNull('saas_website.deleted_at')
                            ->where('saas_website.website_id', $website->id)
                            ->first();

            $plans = websitePlanRules($website);
            foreach($plans as $plan) {
                if($plan->plan_id != null){
                    $array['data']['current_plan'] = $plan; 
                }
            }

            // RUTAS (Precio y redireccion para actualizar plan)
            $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
            $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
            $array['data']['routes']['prices'] = $protocol . $base_path_saas . '/es/precios';
            $array['data']['routes']['redirect_plan'] = $protocol . $base_path_saas . '/login?website_id=' . $website->id;

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
        }
        return response()->json($array, $array['error']);
    }
}