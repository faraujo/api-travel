<?php

namespace App\Http\Controllers\tenant;
use App\Http\Controllers\Controller;

use App\Exceptions\Handler;
use App\Models\tenant\FileTranslation;
use App\Models\tenant\FileType;
use App\Models\tenant\FileTypeTranslation;
use App\Models\tenant\PromotionAccumulate;
use App\Models\tenant\PromotionClientType;
use App\Models\tenant\PromotionCoupon;
use App\Models\tenant\PromotionCouponTranslation;
use App\Models\tenant\PromotionCurrency;
use App\Models\tenant\PromotionDevice;
use App\Models\tenant\PromotionEnjoyDay;
use App\Models\tenant\PromotionFile;
use App\Models\tenant\PromotionLanguage;
use App\Models\tenant\PromotionPaymentMethod;
use App\Models\tenant\PromotionPickup;
use App\Models\tenant\PromotionProductClientType;
use App\Models\tenant\PromotionProductTypeClientType;
use App\Models\tenant\PromotionRangeEnjoyDay;
use App\Models\tenant\PromotionRangeSaleDay;
use App\Models\tenant\PromotionSaleDay;
use App\Models\tenant\PromotionServiceClientType;
use App\Models\tenant\PromotionState;
use App\Models\tenant\Settings;
use Carbon\Carbon;
use validator;
use Illuminate\Support\Str;
use App\Models\tenant\Promotion;
use App\Models\tenant\PromotionSubchannel;
use App\Models\tenant\PromotionCountry;
use App\Models\tenant\PromotionProduct;
use App\Models\tenant\PromotionProductType;
use App\Models\tenant\PromotionService;
use App\Models\tenant\PromotionTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\tenant\Language;


class PromotionController extends Controller
{


    /**
     * Función para la eliminación de una promoción.
     *
     * Para la eliminación de promociones se realiza una petición DELETE. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación

            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'ids' => 'array|required',
                'ids.*' => 'required|integer|min:0',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;
            } else {

                $validator = \Validator::make($request->all(), [
                    'ids.*' => 'exists:tenant.mo_promotion,id,deleted_at,NULL',
                ]);

                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                }

                if ($error == 1) {
                    $array['error'] = 404;
                    $array['error_description'] = 'Data not found';
                    $array['error_inputs'][0] = $mensaje_validador;
                } else {
                    foreach ($request->get('ids') as $promo_id) {
                        $promo = Promotion::where('id', $promo_id)->first();

                        $promo->promotionChannel()->delete();
                        $promo->promotionProduct()->delete();
                        $promo->promotionProductType()->delete();
                        $promo->promotionService()->delete();
                        $promo->promotionCountry()->delete();
                        $promo->promotionState()->delete();
                        $promo->promotionClientType()->delete();
                        $promo->promotionTranslation()->delete();
                        $promo->promotionPaymentMethod()->delete();
                        $promo->promotionCurrency()->delete();
                        $promo->promotionLanguage()->delete();
                        $promo->promotionPickup()->delete();
                        $promo->promotionRangeSaleDay()->delete();
                        $promo->promotionRangeEnjoyDay()->delete();
                        $promo->promotionDevice()->delete();
                        $promo->promotionAccumulate()->delete();
                        $promo->delete();
                    }
                }
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Promociones');
        }
        return response()->json($array, $array['error']);
    }

    /**
     * Función que crea o modifica una promoción
     *
     * Para la creación o modificación de promociones se realiza una petición POST para la creación o PUT para la modificación. Si la operación no produce errores se devuelve, en la variable “error” el valor “200”.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            //Validación
            $error = 0;
            $mensaje_validador = collect();
            $array_traducciones = array();
            $validator = \Validator::make($request->all(), [
                'user_id' => 'required|integer|min:0|exists:tenant.mo_user,id,deleted_at,NULL',
                'code' => 'required',
                'order' => 'integer|min:0',
                'visibility' => 'boolean',
                'accumulate' => 'boolean',
                'promotion_accumulate' => 'required_if:accumulate,1|array',
                'promotion_accumulate.*.id' => 'integer|required|min:0|exists:tenant.mo_promotion,id,deleted_at,NULL|distinct',
                'min_quantity' => 'integer|min:0',
                'max_quantity' => 'integer|min:0',
                'promotion_amount' => 'numeric|min:0',
                'promotion_type_id' => 'required|integer|min:0|exists:tenant.mo_promotion_type,id,deleted_at,NULL',
                'currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL|required_if:promotion_type_id,==,2,promotion_type_id,==,3',
                'campaign_id' => 'integer|required|min:0|exists:tenant.mo_campaign,id,deleted_at,NULL',
                'department_id' => 'required|integer|min:0|exists:tenant.mo_department,id,deleted_at,NULL',
                'cost_center_id' => 'required|integer|min:0|exists:tenant.mo_cost_center,id,deleted_at,NULL',
                'devices' => 'array',
                'devices.*.id' => 'integer|min:0|exists:tenant.mo_device,id,deleted_at,NULL|distinct',
                'apply_product_filter' => 'boolean|required',
                'apply_product_type_filter' => 'boolean|required',
                'apply_service_filter' => 'boolean|required',
                'apply_subchannel_filter' => 'boolean|required',
                'apply_country_filter' => 'boolean|required',
                'apply_state_filter' => 'boolean|required',
                'apply_payment_method_filter' => 'boolean|required',
                'apply_language_filter' => 'boolean|required',
                'apply_currency_filter' => 'boolean|required',
                'apply_pickup_filter' => 'boolean|required',
                'apply_client_type_filter' => 'boolean|required',
                'apply_device_filter' => 'boolean|required',
                'lockers_validation' => 'boolean',
                'supervisor_validation' => 'boolean',
                'allow_date_change' => 'boolean',
                'allow_upgrade' => 'boolean',
                'allow_product_change' => 'boolean',
                'pay_difference' => 'boolean',
                'payment_methods' => 'array',
                'payment_methods.*.id' => 'distinct|integer|min:0|exists:tenant.mo_payment_method,id,deleted_at,NULL',
                'currencies' => 'array',
                'currencies.*.id' => 'distinct|integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',
                'languages' => 'array',
                'languages.*.id' => 'distinct|integer|min:0|exists:tenant.mo_language,id,deleted_at,NULL',
                'anticipation_start' => 'integer|min:0|required_with:anticipation_end',
                'anticipation_end' => 'integer|min:0|required_with:anticipation_start',
                'products' => 'array',
                'products.*.id' => 'required_with:products.*.promotion_amount,products.*.min_quantity,products.*.max_quantity,products.*.series_one,products.*.series_two,products.*.promotion_type_id|integer|min:0|exists:tenant.mo_product,id,deleted_at,NULL',
                'products.*.location_id' => 'integer|min:0|exists:tenant.mo_location,id,deleted_at,NULL',
                'products.*.promotion_amount' => 'numeric|min:0',
                'products.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL|required_if:products.*.promotion_type_id,==,2,products.*.promotion_type_id,==,3',
                'products.*.min_quantity' => 'integer|min:0',
                'products.*.max_quantity' => 'integer|min:0',
                'products.*.series_one' => 'integer|min:0,required_with:products.*.series_two',
                'products.*.series_two' => 'integer|min:0,required_with:products.*.series_one',
                'products.*.promotion_type_id' => 'integer|min:0|exists:tenant.mo_promotion_type,id,deleted_at,NULL',
                'products.*.client_types' => 'array',
                'products.*.client_types.*.id' => 'integer|min:0|exists:tenant.mo_client_type,id,deleted_at,NULL',
                'products.*.apply_client_type_filter' => 'required_with:products.*.id|boolean',
                'product_types' => 'array',
                'product_types.*.id' => 'required_with:product_types.*.promotion_amount,product_types.*.min_quantity,product_types.*.max_quantity,product_types.*.series_one,product_types.*.series_two,product_types.*.promotion_type_id|integer|min:0|exists:tenant.mo_product_type,id,deleted_at,NULL',
                'product_types.*.promotion_amount' => 'numeric|min:0',
                'product_types.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL|required_if:product_types.*.promotion_type_id,==,2,product_types.*.promotion_type_id,==,3',
                'product_types.*.min_quantity' => 'integer|min:0',
                'product_types.*.max_quantity' => 'integer|min:0',
                'product_types.*.series_one' => 'integer|min:0,required_with:product_types.*.series_two',
                'product_types.*.series_two' => 'integer|min:0,required_with:product_types.*.series_one',
                'product_types.*.promotion_type_id' => 'integer|min:0|exists:tenant.mo_promotion_type,id,deleted_at,NULL',
                'product_types.*.client_types' => 'array',
                'product_types.*.client_types.*.id' => 'integer|min:0|exists:tenant.mo_client_type,id,deleted_at,NULL',
                'product_types.*.apply_client_type_filter' => 'required_with:product_types.*.id|boolean',
                'services' => 'array',
                'services.*.id' => 'required_with:services.*.promotion_amount,services.*.min_quantity,services.*.max_quantity,services.*.series_one,services.*.series_two,services.*.promotion_type_id|integer|min:0|exists:tenant.mo_service,id,deleted_at,NULL',
                'services.*.promotion_amount' => 'numeric|min:0',
                'services.*.currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL|required_if:services.*.promotion_type_id,==,2,services.*.promotion_type_id,==,3',
                'services.*.min_quantity' => 'integer|min:0',
                'services.*.max_quantity' => 'integer|min:0',
                'services.*.series_one' => 'integer|min:0,required_with:services.*.series_two',
                'services.*.series_two' => 'integer|min:0,required_with:services.*.series_one',
                'services.*.promotion_type_id' => 'integer|min:0|exists:tenant.mo_promotion_type,id,deleted_at,NULL',
                'services.*.client_types' => 'array',
                'services.*.client_types.*.id' => 'integer|min:0|exists:tenant.mo_client_type,id,deleted_at,NULL',
                'services.*.apply_client_type_filter' => 'required_with:services.*.id|boolean',
                'subchannels' => 'array',
                'subchannels.*.id' => 'distinct|integer|min:0|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'countries' => 'array',
                'countries.*.id' => 'distinct|integer|min:0|exists:tenant.mo_country,id,deleted_at,NULL',
                'states' => 'array',
                'states.*.id' => 'distinct|integer|min:0|exists:tenant.mo_state,id,deleted_at,NULL',
                'pickups' => 'array',
                'pickups.*.id' => 'distinct|integer|min:0|exists:tenant.mo_transportation_location,id,deleted_at,NULL',
                'range_sale_date' => 'array',
                'range_sale_date.*.sale_date_start' => 'date|required_with:range_sale_date.*.sale_date_end,range_sale_date.*.sale_days|date_format:"Y-m-d H:i:s"',
                'range_sale_date.*.sale_date_end' => 'date|required_with:range_sale_date.*.sale_date_start,range_sale_date.*.sale_days|after_or_equal:range_sale_date.*.sale_date_start|date_format:"Y-m-d H:i:s"',
                'range_sale_date.*.sale_days' => 'array|required_with:range_sale_date.*.sale_date_start,range_sale_date.*.sale_date_end',
                'range_sale_date.*.sale_days.*.id' => 'integer|min:0|exists:tenant.mo_day,id,deleted_at,NULL',
                'range_enjoy_date' => 'array',
                'range_enjoy_date.*.enjoy_date_start' => 'date|required_with:range_enjoy_date.*.enjoy_date_end,range_enjoy_date.*.enjoy_days|date_format:"Y-m-d H:i:s"',
                'range_enjoy_date.*.enjoy_date_end' => 'date|required_with:range_enjoy_date.*.enjoy_date_start,range_enjoy_date.*.enjoy_days|after_or_equal:range_enjoy_date.*.enjoy_date_start|date_format:"Y-m-d H:i:s"',
                'range_enjoy_date.*.enjoy_days' => 'array|required_with:range_enjoy_date.*.enjoy_date_start,range_enjoy_date.*.enjoy_date_end',
                'range_enjoy_date.*.enjoy_days.*.id' => 'integer|min:0|exists:tenant.mo_day,id,deleted_at,NULL',
                'coupon_type' => 'boolean',
                'coupon_sheet_start' => 'integer|min:0|required_with:coupon_sheet_end',
                'coupon_sheet_end' => 'integer|min:0|required_with:coupon_sheet_start',
                'coupon_total_uses' => 'integer|min:0',
                'benefit_card' => 'boolean',
                'benefit_card_id' => 'required_if:benefit_card,1|exists:tenant.mo_identification,id,deleted_at,NULL|integer|min:0',
                'benefit_card_total_uses' => 'integer|min:0',
                'files' => 'array',
                'files.*' => 'required|distinct|integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
                'client_types' => 'array',
                'client_types.*.id' => 'distinct|integer|min:0|exists:tenant.mo_client_type,id,deleted_at,NULL',
                'coupon' => 'array',
                'coupon.*' => 'array',
                'coupon.*.file_id' => 'array',
                'coupon.*.file_id.*' => 'integer|min:0|exists:tenant.mo_file,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('coupon') != '') {
                foreach ($request->get('coupon') as $key => $coupon) {
                    $array_traducciones_coupon = array();
                    foreach ($idiomas as $idioma) {
                        if (isset($coupon['legal'][$idioma->abbreviation])) {
                            $array_traducciones_coupon[] = $idioma;
                        }

                    }
                    if (count($array_traducciones_coupon) <= 0) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['coupon.' . $key . '.translation' => ['0' => 'you need at least one translation']]);
                    }
                }
            }

            if ($request->get('range_sale_date') != '') {
                foreach ($request->get('range_sale_date') as $key => $range) {
                    if (isset($range['sale_days'])) {
                        $array_days = array();
                        foreach ($range['sale_days'] as $key2 => $day) {
                            if (isset($day['id'])) {
                                if (in_array($day['id'], $array_days)) {
                                    $error = 1;
                                    $mensaje_validador = $mensaje_validador->merge(['range_sale_date.' . $key . '.sale_days.' . $key2 . '.id' =>
                                        ['The range_sale_date.' . $key . '.sale_days.' . $key2 . '.id field has a duplicate value.']]);
                                }

                                $array_days[] = $day['id'];
                            }

                        }
                    }
                }
            }

            if ($request->get('range_enjoy_date') != '') {
                foreach ($request->get('range_enjoy_date') as $key => $range) {
                    if (isset($range['enjoy_days'])) {
                        $array_days = array();
                        foreach ($range['enjoy_days'] as $key2 => $day) {
                            if (isset($day['id'])) {
                                if (in_array($day['id'], $array_days)) {
                                    $error = 1;
                                    $mensaje_validador = $mensaje_validador->merge(['range_enjoy_date.' . $key . '.enjoy_days.' . $key2 . '.id' =>
                                        ['The range_enjoy_date.' . $key . '.enjoy_days.' . $key2 . '.id field has a duplicate value.']]);
                                }
                                $array_days[] = $day['id'];
                            }
                        }
                    }
                }
            }

            if (($request->get('min_quantity') != '' && $request->get('max_quantity') != '') && ($request->get('min_quantity') > $request->get('max_quantity'))) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['max_quantity' => ['The max_quantity must be at least min_quantity']]);
            }

            if ($request->get('anticipation_start') > $request->get('anticipation_end')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['anticipation_start' => ['anticipation_start must be less than or equal to anticipation_end'], 'anticipation_end' => ['anticipation_end must be greater than or equal to anticipation_start']]);
            }

            if ($request->get('coupon_sheet_start') > $request->get('coupon_sheet_end')) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['coupon_sheet_start' => ['coupon_sheet_start must be less than or equal to coupon_sheet_end'], 'coupon_sheet_end' => ['coupon_sheet_end must be greater than or equal to coupon_sheet_start']]);
            }

            $array_products = $request->get('products');
            if ($request->get('apply_product_filter') == 1 && !isset($array_products)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['products' => ['If apply_product_filter is 1, the product field must arrive, null or informed']]);
            }

            if ($request->get('apply_product_filter') == "0" && isset($array_products)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['products' => ['If apply_product_filter is 0, can not send products']]);
            }

            $array_product_types = $request->get('product_types');
            if ($request->get('apply_product_type_filter') == 1 && !isset($array_product_types)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['product_types' => ['If apply_product_type_filter is 1, the product_types field must arrive, null or informed']]);
            }

            if ($request->get('apply_product_type_filter') == "0" && isset($array_product_types)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['product_types' => ['If apply_product_type_filter is 0, can not send product_types']]);
            }

            $array_services = $request->get('services');
            if ($request->get('apply_service_filter') == 1 && !isset($array_services)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['services' => ['If apply_service_filter is 1, the services field must arrive, null or informed']]);
            }

            if ($request->get('apply_service_filter') == "0" && isset($array_services)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['services' => ['If apply_service_filter is 0, can not send services']]);
            }

            $array_subchannels = $request->get('subchannels');
            if ($request->get('apply_subchannel_filter') == 1 && !isset($array_subchannels)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['subchannels' => ['If apply_subchannel_filter is 1, the subchannels field must arrive, null or informed']]);
            }

            if ($request->get('apply_subchannel_filter') == "0" && isset($array_subchannels)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['subchannels' => ['If apply_subchannel_filter is 0, can not send subchannels']]);
            }

            $array_countries = $request->get('countries');
            if ($request->get('apply_country_filter') == 1 && !isset($array_countries)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['countries' => ['If apply_country_filter is 1, the countries field must arrive, null or informed']]);
            }

            if ($request->get('apply_country_filter') == "0" && isset($array_countries)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['countries' => ['If apply_country_filter is 0, can not send countries']]);
            }

            $array_states = $request->get('states');
            if ($request->get('apply_state_filter') == 1 && !isset($array_states)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['states' => ['If apply_state_filter is 1, the states field must arrive, null or informed']]);
            }

            if ($request->get('apply_state_filter') == "0" && isset($array_states)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['states' => ['If apply_state_filter is 0, can not send states']]);
            }

            $array_languages = $request->get('languages');
            if ($request->get('apply_language_filter') == 1 && !isset($array_languages)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['languages' => ['If apply_language_filter is 1, the languages field must arrive, null or informed']]);
            }

            if ($request->get('apply_language_filter') == "0" && isset($array_languages)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['languages' => ['If apply_language_filter is 0, can not send languages']]);
            }

            $array_currencies = $request->get('currencies');
            if ($request->get('apply_currency_filter') == 1 && !isset($array_currencies)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['currencies' => ['If apply_currency_filter is 1, the currencies field must arrive, null or informed']]);
            }

            if ($request->get('apply_currency_filter') == "0" && isset($array_currencies)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['currencies' => ['If apply_currency_filter is 0, can not send currencies']]);
            }

            $array_payment = $request->get('payment_methods');
            if ($request->get('apply_payment_method_filter') == 1 && !isset($array_payment)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['payment_methods' => ['If apply_payment_method_filter is 1, the payment_methods field must arrive, null or informed']]);
            }

            if ($request->get('apply_payment_method_filter') == "0" && isset($array_payment)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['payment_methods' => ['If apply_payment_method_filter is 0, can not send payment_methods']]);
            }

            $array_pickups = $request->get('pickups');
            if ($request->get('apply_pickup_filter') == 1 && !isset($array_pickups)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['pickups' => ['If apply_pickup_filter is 1, the pickups field must arrive, null or informed']]);
            }

            if ($request->get('apply_pickup_filter') == "0" && isset($array_pickups)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['pickups' => ['If apply_pickup_filter is 0, can not send pickups']]);
            }

            $array_client_types = $request->get('client_types');
            if ($request->get('apply_client_type_filter') == 1 && !isset($array_client_types)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['client_types' => ['If apply_client_type_filter is 1, the client types field must arrive, null or informed']]);
            }

            if ($request->get('apply_client_type_filter') == "0" && isset($array_client_types)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['client_types' => ['If apply_client_type_filter is 0, can not send client types']]);
            }

            $array_devices = $request->get('devices');
            if ($request->get('apply_device_filter') == 1 && !isset($array_devices)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['devices' => ['If apply_device_filter is 1, the device field must arrive, null or informed']]);
            }

            if ($request->get('apply_device_filter') == "0" && isset($array_devices)) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['devices' => ['If apply_device_filter is 0, can not send devices']]);
            }


            $array_client_types = array();
            if ($request->get('products') != '' && gettype($request->get('products')) == 'array') {
                foreach ($request->get('products') as $index => $product) {
                    if ((isset($product['min_quantity']) && isset($product['max_quantity'])) && ($product['min_quantity'] > $product['max_quantity'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['products.' . $index . '.max_quantity' => ['The products.' . $index . '.max_quantity must be at least products.' . $key . '.min_quantity']]);
                    }

                    if ((isset($product['series_one']) && isset($product['series_two'])) && ($product['series_one'] <= $product['series_two'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['products.' . $index . '.series_one' => ['The products.' . $index . '.series_one must be at least products.' . $key . '.series_two']]);
                    }

                    if (isset($product['apply_client_type_filter']) && $product['apply_client_type_filter'] == "0" && isset($product['client_types'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['products.' . $index . '.client_types' => ['If products.' . $index . '.apply_client_type_filter is 0, can not send client types']]);
                    }

                    if (isset($product['apply_client_type_filter']) && $product['apply_client_type_filter'] == 1) {
                        if (!isset($product['client_types'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['products.' . $index . '.client_types' => ['If products.' . $index . '.apply_client_type_filter is 1, the products.' . $index . '.client types field must arrive, null or informed']]);
                        }
                        if (!isset($product['id'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['products.' . $index . '.id' => ['If products.' . $index . '.apply_client_type_filter is 1, the products.' . $index . '.id field must arrive informed']]);
                        }
                    }

                    $array_client_types = [];
                    if (isset($product['client_types']) && gettype($product['client_types']) == 'array') {
                        foreach ($product['client_types'] as $index_cli => $client) {
                            foreach ($array_client_types as $array_cli) {
                                if ($array_cli == $client['id']) {
                                    $error = 1;
                                    $mensaje_validador = $mensaje_validador->merge(['products.' . $index . '.client_types.' . $index_cli => ['products.' . $index . '.client_types.' . $index_cli . ' has a duplicate value.']]);
                                }
                            }
                            array_push($array_client_types, $client['id']);
                        }
                    }

                }
            }

            if ($request->get('services') != '' && gettype($request->get('services')) == 'array') {
                foreach ($request->get('services') as $index => $service) {
                    if ((isset($service['min_quantity']) && isset($service['max_quantity'])) && ($service['min_quantity'] > $service['max_quantity'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['services.' . $index . '.max_quantity' => ['The services.' . $index . '.max_quantity must be at least services.' . $key . '.min_quantity']]);
                    }

                    if ((isset($service['series_one']) && isset($service['series_two'])) && ($service['series_one'] <= $service['series_two'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['services.' . $index . '.series_one' => ['The services.' . $index . '.series_one must be at least services.' . $key . '.series_two']]);
                    }

                    if (isset($service['apply_client_type_filter']) && $service['apply_client_type_filter'] == "0" && isset($service['client_types'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['services.' . $index . '.client_types' => ['If services.' . $index . '.apply_client_type_filter is 0, can not send client types']]);
                    }

                    if (isset($service['apply_client_type_filter']) && $service['apply_client_type_filter'] == 1) {
                        if (!isset($service['client_types'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['services.' . $index . '.client_types' => ['If services.' . $index . '.apply_client_type_filter is 1, the services.' . $index . '.client types field must arrive, null or informed']]);
                        }
                        if (!isset($service['id'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['services.' . $index . '.id' => ['If services.' . $index . '.apply_client_type_filter is 1, the services.' . $index . '.id field must arrive informed']]);
                        }
                    }

                    $array_client_types = [];
                    if (isset($service['client_types']) && gettype($service['client_types']) == 'array') {
                        foreach ($service['client_types'] as $index_cli => $client) {
                            foreach ($array_client_types as $array_cli) {
                                if ($array_cli == $client['id']) {
                                    $error = 1;
                                    $mensaje_validador = $mensaje_validador->merge(['services.' . $index . '.client_types.' . $index_cli => ['services.' . $index . '.client_types.' . $index_cli . ' has a duplicate value.']]);
                                }
                            }

                            array_push($array_client_types, $client['id']);
                        }
                    }
                }
            }

            if ($request->get('product_types') != '' && gettype($request->get('product_types')) == 'array') {
                foreach ($request->get('product_types') as $index => $product_type) {
                    if ((isset($product_type['min_quantity']) && isset($product_type['max_quantity'])) && ($product_type['min_quantity'] > $product_type['max_quantity'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['product_types.' . $index . '.max_quantity' => ['The product_types.' . $index . '.max_quantity must be at least services.' . $key . '.min_quantity']]);
                    }
                    if ((isset($product_type['series_one']) && isset($product_type['series_two'])) && ($product_type['series_one'] <= $product_type['series_two'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['product_types.' . $index . '.series_one' => ['The product_types.' . $index . '.series_one must be at least services.' . $key . '.series_two']]);
                    }
                    if (isset($product_type['apply_client_type_filter']) && $product_type['apply_client_type_filter'] == "0" && isset($product_type['client_types'])) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge(['product_types.' . $index . '.client_types' => ['If product_types.' . $index . '.apply_client_type_filter is 0, can not send client types']]);
                    }

                    if (isset($product_type['apply_client_type_filter']) && $product_type['apply_client_type_filter'] == 1) {
                        if (!isset($product_type['client_types'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['product_types.' . $index . '.client_types' => ['If product_types.' . $index . '.apply_client_type_filter is 1, the product_types.' . $index . '.client types field must arrive, null or informed']]);
                        }
                        if (!isset($product_type['id'])) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['product_types.' . $index . '.id' => ['If product_types.' . $index . '.apply_client_type_filter is 1, the product_types.' . $index . '.id field must arrive informed']]);
                        }
                    }

                    $array_client_types = [];
                    if (isset($product_type['client_types']) && gettype($product_type['client_types']) == 'array') {
                        foreach ($product_type['client_types'] as $index_cli => $client) {
                            foreach ($array_client_types as $array_cli) {
                                if ($array_cli == $client['id']) {
                                    $error = 1;
                                    $mensaje_validador = $mensaje_validador->merge(['product_types.' . $index . '.client_types.' . $index_cli => ['product_types.' . $index . '.client_types.' . $index_cli . ' has a duplicate value.']]);
                                }
                            }
                            array_push($array_client_types, $client['id']);
                        }
                    }
                }
            }

            $sql_campaign = DB::connection('tenant')->table('mo_campaign')
                ->whereNull('mo_campaign.deleted_at')
                ->where('mo_campaign.id',$request->get('campaign_id'));

            $rangos_sale_introducidos = array();
            //introduce en un array nuevo todos los rangos introducidos
            if ($request->get('range_sale_date') != '' && gettype($request->get('range_sale_date')) == 'array') {
                foreach ($request->get('range_sale_date') as $key => $rango_nuevo) {
                    foreach ($rangos_sale_introducidos as $rango_introducido) {
                        if (isset($rango_nuevo['sale_date_start']) && isset($rango_nuevo['sale_date_end']) &&
                            $rango_nuevo['sale_date_start'] >= $rango_introducido['sale_date_start'] &&
                            $rango_nuevo['sale_date_start'] <= $rango_introducido['sale_date_end']) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['range_sale_date' => ['The range_sale_date.' . $key . '.sale_date_start are overlapping']]);
                        }
                        if (isset($rango_nuevo['sale_date_start']) && isset($rango_nuevo['sale_date_end']) &&
                            $rango_nuevo['sale_date_end'] >= $rango_introducido['sale_date_start'] &&
                            $rango_nuevo['sale_date_end'] <= $rango_introducido['sale_date_end']) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['range_sale_date' => ['The range_sale_date.' . $key . '.sale_date_start are overlapping']]);
                        }
                        if (isset($rango_nuevo['sale_date_start']) && isset($rango_nuevo['sale_date_end']) &&
                            $rango_nuevo['sale_date_start'] <= $rango_introducido['sale_date_start'] &&
                            $rango_nuevo['sale_date_end'] >= $rango_introducido['sale_date_end']) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['range_sale_date' => ['The range_sale_date.' . $key . '.sale_date_start are overlapping']]);
                        }


                    }
                    if(isset($rango_nuevo['sale_date_start']) && isset($rango_nuevo['sale_date_end'])){
                        $sql_campaign_clone = clone $sql_campaign;

                        $datos_campaign = $sql_campaign_clone->where('mo_campaign.date_start','<=',Carbon::parse($rango_nuevo['sale_date_start'])->format('Y-m-d'))
                            ->where('mo_campaign.date_end','>=',Carbon::parse($rango_nuevo['sale_date_end'])->format('Y-m-d'))->first();

                        if(!$datos_campaign){
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['range_sale_date' => ['The range_sale_date.' . $key . ' is outside the range of the campaign']]);
                        }
                    }
                    $rangos_sale_introducidos[] = $rango_nuevo;
                }
            }

            $rangos_enjoy_introducidos = array();
            //introduce en un array nuevo todos los rangos introducidos
            if ($request->get('range_enjoy_date') != '' && gettype($request->get('range_enjoy_date')) == 'array') {
                foreach ($request->get('range_enjoy_date') as $key => $rango_nuevo){
                    foreach ($rangos_enjoy_introducidos as $rango_introducido) {

                        if (isset($rango_nuevo['enjoy_date_start']) && isset($rango_nuevo['enjoy_date_end']) &&
                            $rango_nuevo['enjoy_date_start'] >= $rango_introducido['enjoy_date_start'] &&
                            $rango_nuevo['enjoy_date_start'] <= $rango_introducido['enjoy_date_end']) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['range_enjoy_date' => ['The range_enjoy_date.' . $key . '.enjoy_date_start are overlapping']]);

                        }
                        if (isset($rango_nuevo['enjoy_date_start']) && isset($rango_nuevo['enjoy_date_end']) &&
                            $rango_nuevo['enjoy_date_end'] >= $rango_introducido['enjoy_date_start'] &&
                            $rango_nuevo['enjoy_date_end'] <= $rango_introducido['enjoy_date_end']) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['range_enjoy_date' => ['The range_enjoy_date.' . $key . '.enjoy_date_start are overlapping']]);

                        }
                        if (isset($rango_nuevo['enjoy_date_start']) && isset($rango_nuevo['enjoy_date_end']) &&
                            $rango_nuevo['enjoy_date_start'] <= $rango_introducido['enjoy_date_start'] &&
                            $rango_nuevo['enjoy_date_end'] >= $rango_introducido['enjoy_date_end']) {
                            $error = 1;
                            $mensaje_validador = $mensaje_validador->merge(['range_enjoy_date' => ['The range_enjoy_date.' . $key . '.enjoy_date_start are overlapping']]);
                        }

                    }


//                    if(isset($rango_nuevo['enjoy_date_start']) && isset($rango_nuevo['enjoy_date_end'])){
//                        $sql_campaign_clone = clone $sql_campaign;
//
//                        $datos_campaign = $sql_campaign_clone->where('mo_campaign.date_start','<=',Carbon::parse($rango_nuevo['enjoy_date_start'])->format('Y-m-d'))
//                            ->where('mo_campaign.date_end','>=',Carbon::parse($rango_nuevo['enjoy_date_end'])->format('Y-m-d'))->first();
//
//                        if(!$datos_campaign){
//                            $error = 1;
//                            $mensaje_validador = $mensaje_validador->merge(['range_enjoy_date' => ['The range_enjoy_date.' . $key . ' is outside the range of the campaign']]);
//                        }
//                    }
                    $rangos_enjoy_introducidos[] = $rango_nuevo;
                }
            }

            $array_traducciones_coupon = array();

            foreach ($idiomas as $idioma) {
                if (isset($request->get('short_description')[$idioma->abbreviation]) || isset($request->get('title_seo')[$idioma->abbreviation]) ||
                    isset($request->get('description_seo')[$idioma->abbreviation]) || isset($request->get('description')[$idioma->abbreviation]) ||
                    isset($request->get('name')[$idioma->abbreviation]) || isset($request->get('gift')[$idioma->abbreviation]) ||
                    isset($request->get('friendly_url')[$idioma->abbreviation]) || isset($request->get('keywords_seo')[$idioma->abbreviation]) ||
                    isset($request->get('observations')[$idioma->abbreviation]) || isset($request->get('legal')[$idioma->abbreviation]) ||
                    isset($request->get('external_url')[$idioma->abbreviation]) || isset($request->get('notes')[$idioma->abbreviation])) {
                    $validator = \Validator::make($request->all(), [
                        'name.' . $idioma->abbreviation => 'required',
                        'description.' . $idioma->abbreviation => 'required',
                        'short_description.' . $idioma->abbreviation => 'required',
                        'title_seo.' . $idioma->abbreviation => 'required',
                        'description_seo.' . $idioma->abbreviation => 'required',
                    ]);
                    if ($validator->fails()) {
                        $error = 1;
                        $mensaje_validador = $mensaje_validador->merge($validator->errors());
                    }
                    $array_traducciones[] = $idioma;
                }
            }

            if (count($array_traducciones) <= 0) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge(['translation' => ['0' => 'you need at least one translation']]);
            }

            if ($request->isMethod('put')) {
                $validator = \Validator::make($request->all(), [
                    'id' => 'integer|min:0|required',
                ]);
                if ($validator->fails()) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge($validator->errors());
                } else {
                    $promocion = Promotion::find($request->get('id'));
                    if (!$promocion) {
                        $error = 2;
                    }
                }
            }
            //Fin de validación

            $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                ->where('mo_settings.name', 'heimdall_middleware')
                ->first()
                ->value;

            $rule_codes = [];

            if($heimdall_middleware == '1') {
                $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                $website = DB::table('saas_website')
                                ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                'saas_website.default_language_id', 'saas_website.default_currency_id')
                                ->whereNull('saas_website.deleted_at')
                                ->where('saas_website.website_id', $website->id)
                                ->first();

                $plans = websitePlanRules($website);

                $cantidad = 0;

                foreach($plans as $plan) {
                    if($plan->plan_id != null){
                        $rules = websiteRules($plan);
                        
                        $rules_promo = $rules->where('code', 'promo_promo');

                        if($rules_promo) {
                            foreach($rules_promo as $rule) {
                                if($rule->unlimited == 1){
                                    $cantidad = -1;
                                } else {
                                    if($cantidad >= 0 && $cantidad < $rule->value) {
                                        $cantidad = $rule->value;
                                    }
                                }
                            }
                        }
                    } else {
                        if($plan->rule_code == 'promo_promo') {
                            if($plan->unlimited == 1){
                                $cantidad = -1;
                            }else{
                                if($cantidad >= 0 && $cantidad < $plan->value){
                                    $cantidad = $plan->value;
                                }
                            }
                        }
                    }
                }

                if($cantidad == 0) {
                    $error = 3;
                    $rule_codes[] = 'promo_promo';
                } else if ($cantidad > 0) {
                    if($request->isMethod('post')){
                        $total_promotions = Promotion::get();
                    } else {
                        $total_promotions = Promotion::where('id','!=', $request->get('id'))->get();
                    }
                   

                    if(count($total_promotions) >= $cantidad) {
                        $error = 3;
                        $rule_codes[] = 'promo_promo';
                    }
                }
            }

            if ($error == 0) {

                if ($request->isMethod('put')) {

                    //borra asociaciones de promociones en tablas intermedias

                    $promocion->promotionChannel()->delete();
                    $promotion_product = $promocion->promotionProduct()->get();
                    foreach ($promotion_product as $promo_prod) {
                        PromotionProductClientType::where('promotion_product_id', $promo_prod->id)->delete();
                        $promo_prod->delete();

                    }

                    $promotion_product_type = $promocion->promotionProductType()->get();
                    foreach ($promotion_product_type as $promo_prod_type) {
                        PromotionProductTypeClientType::where('promotion_product_type_id', $promo_prod_type->id)->delete();
                        $promo_prod_type->delete();

                    }

                    $promotion_service = $promocion->promotionService()->get();
                    foreach ($promotion_service as $promo_service) {
                        PromotionServiceClientType::where('promotion_service_id', $promo_service->id)->delete();
                        $promo_service->delete();
                    }

                    $promocion->promotionCountry()->delete();
                    $promocion->promotionState()->delete();
                    $promocion->promotionPaymentMethod()->delete();
                    $promocion->promotionCurrency()->delete();
                    $promocion->promotionLanguage()->delete();
                    $promocion->promotionPickup()->delete();
                    $promocion->promotionClientType()->delete();
                    $promocion->promotionRangeSaleDay()->delete();
                    $promocion->promotionRangeEnjoyDay()->delete();
                    $promocion->promotionCoupon()->delete();
                    $promocion->promotionDevice()->delete();
                    $promocion->promotionAccumulate()->delete();

                    //Actualiza la promocion
                    $promocion->update([
                        'code' => $request->get('code'),
                        'order' => $request->get('order') != '' ? $request->get('order') : 0,
                        'visibility' => $request->get('visibility') != '' ? $request->get('visibility') : 0,
                        'accumulate' => $request->get('accumulate') != '' ? $request->get('accumulate') : 0,
                        'min_quantity' => $request->get('min_quantity') != '' ? $request->get('min_quantity') : null,
                        'max_quantity' => $request->get('max_quantity') != '' ? $request->get('max_quantity') : null,
                        'promotion_amount' => $request->get('promotion_amount') != '' ? $request->get('promotion_amount') : null,
                        'currency_id' => $request->get('currency_id') != '' ? $request->get('currency_id') : null,
                        'department_id' => $request->get('department_id') != '' ? $request->get('department_id') : null,
                        'cost_center_id' => $request->get('cost_center_id') != '' ? $request->get('cost_center_id') : null,
                        'promotion_type_id' => $request->get('promotion_type_id'),
                        'campaign_id' => $request->get('campaign_id'),
                        'apply_product_filter' => $request->get('apply_product_filter'),
                        'apply_product_type_filter' => $request->get('apply_product_type_filter'),
                        'apply_service_filter' => $request->get('apply_service_filter'),
                        'apply_subchannel_filter' => $request->get('apply_subchannel_filter'),
                        'apply_country_filter' => $request->get('apply_country_filter'),
                        'apply_state_filter' => $request->get('apply_state_filter'),
                        'apply_language_filter' => $request->get('apply_language_filter'),
                        'apply_currency_filter' => $request->get('apply_currency_filter'),
                        'apply_pickup_filter' => $request->get('apply_pickup_filter'),
                        'apply_payment_method_filter' => $request->get('apply_payment_method_filter'),
                        'apply_client_type_filter' => $request->get('apply_client_type_filter'),
                        'apply_device_filter' => $request->get('apply_device_filter'),
                        'lockers_validation' => $request->get('lockers_validation') != '' ? $request->get('lockers_validation') : 0,
                        'supervisor_validation' => $request->get('supervisor_validation') != '' ? $request->get('supervisor_validation') : 0,
                        'allow_date_change' => $request->get('allow_date_change') != '' ? $request->get('allow_date_change') : 0,
                        'allow_upgrade' => $request->get('allow_upgrade') != '' ? $request->get('allow_upgrade') : 0,
                        'allow_product_change' => $request->get('allow_product_change') != '' ? $request->get('allow_product_change') : 0,
                        'pay_difference' => $request->get('pay_difference') != '' ? $request->get('pay_difference') : 0,
                        'anticipation_start' => $request->get('anticipation_start') != '' ? $request->get('anticipation_start') : null,
                        'anticipation_end' => $request->get('anticipation_end') != '' ? $request->get('anticipation_end') : null,
                        'coupon_code' => $request->get('coupon_code') != '' ? $request->get('coupon_code') : null,
                        'coupon_type' => $request->get('coupon_type') != '' ? $request->get('coupon_type') : 0,
                        'coupon_sheet_start' => $request->get('coupon_sheet_start') != '' ? $request->get('coupon_sheet_start') : null,
                        'coupon_sheet_end' => $request->get('coupon_sheet_end') != '' ? $request->get('coupon_sheet_end') : null,
                        'coupon_total_uses' => $request->get('coupon_total_uses') != '' ? $request->get('coupon_total_uses') : null,
                        'benefit_card' => $request->get('benefit_card') != '' ? $request->get('benefit_card') : 0,
                        'benefit_card_total_uses' => $request->get('benefit_card_total_uses'),
                        'benefit_card_id' => $request->get('benefit_card_id') != '' ? $request->get('benefit_card_id') : null,
                        'user_id' => $request->get('user_id'),
                    ]);
                    //fin actualiza la promoción

                } else {

                    $promocion = Promotion::create([
                        'code' => $request->get('code'),
                        'order' => $request->get('order') != '' ? $request->get('order') : 0,
                        'visibility' => $request->get('visibility') != '' ? $request->get('visibility') : 0,
                        'accumulate' => $request->get('accumulate') != '' ? $request->get('accumulate') : 0,
                        'min_quantity' => $request->get('min_quantity') != '' ? $request->get('min_quantity') : null,
                        'max_quantity' => $request->get('max_quantity') != '' ? $request->get('max_quantity') : null,
                        'promotion_amount' => $request->get('promotion_amount') != '' ? $request->get('promotion_amount') : null,
                        'currency_id' => $request->get('currency_id') != '' ? $request->get('currency_id') : null,
                        'department_id' => $request->get('department_id') != '' ? $request->get('department_id') : null,
                        'cost_center_id' => $request->get('cost_center_id') != '' ? $request->get('cost_center_id') : null,
                        'promotion_type_id' => $request->get('promotion_type_id'),
                        'campaign_id' => $request->get('campaign_id'),
                        'apply_product_filter' => $request->get('apply_product_filter'),
                        'apply_product_type_filter' => $request->get('apply_product_type_filter'),
                        'apply_service_filter' => $request->get('apply_service_filter'),
                        'apply_subchannel_filter' => $request->get('apply_subchannel_filter'),
                        'apply_country_filter' => $request->get('apply_country_filter'),
                        'apply_state_filter' => $request->get('apply_state_filter'),
                        'apply_language_filter' => $request->get('apply_language_filter'),
                        'apply_currency_filter' => $request->get('apply_currency_filter'),
                        'apply_payment_method_filter' => $request->get('apply_payment_method_filter'),
                        'apply_pickup_filter' => $request->get('apply_pickup_filter'),
                        'apply_client_type_filter' => $request->get('apply_client_type_filter'),
                        'apply_device_filter' => $request->get('apply_device_filter'),
                        'lockers_validation' => $request->get('lockers_validation') != '' ? $request->get('lockers_validation') : 0,
                        'supervisor_validation' => $request->get('supervisor_validation') != '' ? $request->get('supervisor_validation') : 0,
                        'allow_date_change' => $request->get('allow_date_change') != '' ? $request->get('allow_date_change') : 0,
                        'allow_upgrade' => $request->get('allow_upgrade') != '' ? $request->get('allow_upgrade') : 0,
                        'allow_product_change' => $request->get('allow_product_change') != '' ? $request->get('allow_product_change') : 0,
                        'pay_difference' => $request->get('pay_difference') != '' ? $request->get('pay_difference') : 0,
                        'anticipation_start' => $request->get('anticipation_start') != '' ? $request->get('anticipation_start') : null,
                        'anticipation_end' => $request->get('anticipation_end') != '' ? $request->get('anticipation_end') : null,
                        'coupon_code' => $request->get('coupon_code') != '' ? $request->get('coupon_code') : null,
                        'coupon_type' => $request->get('coupon_type') != '' ? $request->get('coupon_type') : 0,
                        'coupon_sheet_start' => $request->get('coupon_sheet_start') != '' ? $request->get('coupon_sheet_start') : null,
                        'coupon_sheet_end' => $request->get('coupon_sheet_end') != '' ? $request->get('coupon_sheet_end') : null,
                        'coupon_total_uses' => $request->get('coupon_total_uses') != '' ? $request->get('coupon_total_uses') : null,
                        'benefit_card' => $request->get('benefit_card') != '' ? $request->get('benefit_card') : 0,
                        'benefit_card_total_uses' => $request->get('benefit_card_total_uses'),
                        'benefit_card_id' => $request->get('benefit_card_id') != '' ? $request->get('benefit_card_id') : null,
                        'user_id' => $request->get('user_id'),
                    ]);


                }


                //Actualiza o crea las traducciones si no existen
                $array_borrar = [];
                foreach ($array_traducciones as $idioma) {
                    // Preparación de datos
                    $friendly_url = null;

                    if (isset($request->get('friendly_url')[$idioma->abbreviation]) && $request->get('friendly_url')[$idioma->abbreviation] != '') {
                        $friendly_url = Str::slug($request->get('friendly_url')[$idioma->abbreviation]);
                    } else {
                        $friendly_url = Str::slug($request->get('name')[$idioma->abbreviation]);
                    }

                    $traduccion = $promocion->promotionTranslation()->where('language_id', $idioma->id)->first();

                    $array_borrar[] = $idioma->id;

                    if ($traduccion) {
                        $traduccion->update([
                            'language_id' => $idioma->id,
                            'gift' => (isset($request->get('gift')[$idioma->abbreviation])) ? $request->get('gift')[$idioma->abbreviation] : null,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                            'short_description' => $request->get('short_description')[$idioma->abbreviation],
                            'friendly_url' => $friendly_url,
                            'external_url' => (isset($request->get('external_url')[$idioma->abbreviation]) && $request->get('external_url')[$idioma->abbreviation] != '') ? $request->get('external_url')[$idioma->abbreviation] : null,
                            'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                            'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                            'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            'observations' => (isset($request->get('observations')[$idioma->abbreviation]) && $request->get('observations')[$idioma->abbreviation] != '') ? $request->get('observations')[$idioma->abbreviation] : null,
                            'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,
                            'notes' => (isset($request->get('notes')[$idioma->abbreviation]) && $request->get('notes')[$idioma->abbreviation] != '') ? $request->get('notes')[$idioma->abbreviation] : null,
                        ]);
                    } else {
                        PromotionTranslation::create([
                            'promotion_id' => $promocion->id,
                            'language_id' => $idioma->id,
                            'gift' => (isset($request->get('gift')[$idioma->abbreviation])) ? $request->get('gift')[$idioma->abbreviation] : null,
                            'name' => $request->get('name')[$idioma->abbreviation],
                            'description' => $request->get('description')[$idioma->abbreviation],
                            'short_description' => $request->get('short_description')[$idioma->abbreviation],
                            'friendly_url' => $friendly_url,
                            'external_url' => (isset($request->get('external_url')[$idioma->abbreviation]) && $request->get('external_url')[$idioma->abbreviation] != '') ? $request->get('external_url')[$idioma->abbreviation] : null,
                            'title_seo' => $request->get('title_seo')[$idioma->abbreviation],
                            'description_seo' => $request->get('description_seo')[$idioma->abbreviation],
                            'keywords_seo' => (isset($request->get('keywords_seo')[$idioma->abbreviation]) && $request->get('keywords_seo')[$idioma->abbreviation] != '') ? $request->get('keywords_seo')[$idioma->abbreviation] : null,
                            'observations' => (isset($request->get('observations')[$idioma->abbreviation]) && $request->get('observations')[$idioma->abbreviation] != '') ? $request->get('observations')[$idioma->abbreviation] : null,
                            'legal' => (isset($request->get('legal')[$idioma->abbreviation]) && $request->get('legal')[$idioma->abbreviation] != '') ? $request->get('legal')[$idioma->abbreviation] : null,
                            'notes' => (isset($request->get('notes')[$idioma->abbreviation]) && $request->get('notes')[$idioma->abbreviation] != '') ? $request->get('notes')[$idioma->abbreviation] : null,

                        ]);


                    }

                }

                $promocion->promotionTranslation()->whereNotIn('language_id', $array_borrar)->delete();


                if ($request->get('coupon') != '') {
                    foreach ($request->get('coupon') as $coupon) {
                        $promotion_coupon = PromotionCoupon::create([
                            'promotion_id' => $promocion->id
                        ]);
                        foreach ($idiomas as $idioma) {
                            if (isset($coupon['legal'][$idioma->abbreviation])) {
                                PromotionCouponTranslation::create([
                                    'coupon_id' => $promotion_coupon->id,
                                    'language_id' => $idioma->id,
                                    'file_id' => isset($coupon['file_id'][$idioma->abbreviation]) ? $coupon['file_id'][$idioma->abbreviation] : null,
                                    'legal' => isset($coupon['legal'][$idioma->abbreviation]) ? $coupon['legal'][$idioma->abbreviation] : null,
                                ]);
                            }
                        }
                    }


                }
                //asociación de promociones  a productos
                if ($request->get('products') != '') {
                    foreach ($request->get('products') as $product) {
                        if (isset($product['id']) && $product['id'] != '') {

                            $promotionProduct = PromotionProduct::create([
                                'promotion_id' => $promocion->id,
                                'product_id' => $product['id'],
                                'location_id' => (isset($product['location_id'])) ? $product['location_id'] : null,
                                'min_quantity' => (isset($product['min_quantity'])) ? $product['min_quantity'] : null,
                                'max_quantity' => (isset($product['max_quantity'])) ? $product['max_quantity'] : null,
                                'series_one' => (isset($product['series_one'])) ? $product['series_one'] : null,
                                'series_two' => (isset($product['series_two'])) ? $product['series_two'] : null,
                                'promotion_amount' => (isset($product['promotion_amount'])) ? $product['promotion_amount'] : null,
                                'currency_id' => (isset($product['currency_id'])) ? $product['currency_id'] : null,
                                'promotion_type_id' => (isset($product['promotion_type_id'])) ? $product['promotion_type_id'] : null,
                                'apply_client_type_filter' => (isset($product['apply_client_type_filter'])) ? $product['apply_client_type_filter'] : 0,
                            ]);

                            if (isset($product['client_types'])) {
                                foreach ($product['client_types'] as $client_type) {
                                    if (isset($client_type['id']) && $client_type['id'] != '') {
                                        PromotionProductClientType::create([
                                            'promotion_product_id' => $promotionProduct->id,
                                            'client_type_id' => $client_type['id'],
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
                //fin asociación de promociones  a productos

                //asociación de promociones  a tipos de productos
                if ($request->get('product_types') != '') {
                    foreach ($request->get('product_types') as $product_type) {
                        if (isset($product_type['id']) && $product_type['id'] != '') {
                            $promotionProductType = PromotionProductType::create([
                                'promotion_id' => $promocion->id,
                                'product_type_id' => $product_type['id'],
                                'min_quantity' => (isset($product_type['min_quantity'])) ? $product_type['min_quantity'] : null,
                                'max_quantity' => (isset($product_type['max_quantity'])) ? $product_type['max_quantity'] : null,
                                'series_one' => (isset($product_type['series_one'])) ? $product_type['series_one'] : null,
                                'series_two' => (isset($product_type['series_two'])) ? $product_type['series_two'] : null,
                                'promotion_amount' => (isset($product_type['promotion_amount'])) ? $product_type['promotion_amount'] : null,
                                'currency_id' => (isset($product_type['currency_id'])) ? $product_type['currency_id'] : null,
                                'promotion_type_id' => (isset($product_type['promotion_type_id'])) ? $product_type['promotion_type_id'] : null,
                                'apply_client_type_filter' => (isset($product_type['apply_client_type_filter'])) ? $product_type['apply_client_type_filter'] : 0,
                            ]);

                            if (isset($product_type['client_types'])) {
                                foreach ($product_type['client_types'] as $client_type) {
                                    if (isset($client_type['id']) && $client_type['id'] != '') {
                                        PromotionProductTypeClientType::create([
                                            'promotion_product_type_id' => $promotionProductType->id,
                                            'client_type_id' => $client_type['id'],
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
                //fin asociación de promociones  a tipos de productos

                //asociación de promociones  a servicios
                if ($request->get('services') != '') {
                    foreach ($request->get('services') as $service) {
                        //for ($i = 0; $i < count($request->get('services')); $i++) {
                        if (isset($service['id']) && $service['id'] != '') {
                            $promotionService = PromotionService::create([
                                'promotion_id' => $promocion->id,
                                'service_id' => $service['id'],
                                'min_quantity' => (isset($service['min_quantity'])) ? $service['min_quantity'] : null,
                                'max_quantity' => (isset($service['max_quantity'])) ? $service['max_quantity'] : null,
                                'series_one' => (isset($service['series_one'])) ? $service['series_one'] : null,
                                'series_two' => (isset($service['series_two'])) ? $service['series_two'] : null,
                                'promotion_amount' => (isset($service['promotion_amount'])) ? $service['promotion_amount'] : null,
                                'currency_id' => (isset($service['currency_id'])) ? $service['currency_id'] : null,
                                'promotion_type_id' => (isset($service['promotion_type_id'])) ? $service['promotion_type_id'] : null,
                                'apply_client_type_filter' => (isset($service['apply_client_type_filter'])) ? $service['apply_client_type_filter'] : 0,
                            ]);

                            if (isset($service['client_types'])) {
                                foreach ($service['client_types'] as $client_type) {
                                    if (isset($client_type['id']) && $client_type['id'] != '') {
                                        PromotionServiceClientType::create([
                                            'promotion_service_id' => $promotionService->id,
                                            'client_type_id' => $client_type['id'],
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
                //fin asociación de promociones  a servicios

                //asociación de promociones  a canales
                if ($request->get('subchannels') != '') {
                    foreach ($request->get('subchannels') as $subchannel) {
                        if (isset($subchannel['id']) && $subchannel['id'] != '') {
                            PromotionSubchannel::create([
                                'promotion_id' => $promocion->id,
                                'subchannel_id' => $subchannel['id'],
                            ]);
                        }
                    }
                }
                //fin asociación de promociones  a canales

                //asociación de promociones  a paises
                if ($request->get('countries') != '') {
                    foreach ($request->get('countries') as $country) {
                        if (isset($country['id']) && $country['id'] != '') {
                            PromotionCountry::create([
                                'promotion_id' => $promocion->id,
                                'country_id' => $country['id'],
                            ]);
                        }
                    }
                }
                //fin asociación de promociones  a paises

                //asociación de promociones  a estados
                if ($request->get('states') != '') {
                    foreach ($request->get('states') as $state) {
                        if (isset($state['id']) && $state['id'] != '') {
                            PromotionState::create([
                                'promotion_id' => $promocion->id,
                                'state_id' => $state['id'],
                            ]);
                        }
                    }
                }
                //fin asociación de promociones  a estados

                //asociación de promociones  a currencies
                if ($request->get('currencies') != '') {
                    foreach ($request->get('currencies') as $currency) {
                        if (isset($currency['id']) && $currency['id'] != '') {
                            PromotionCurrency::create([
                                'promotion_id' => $promocion->id,
                                'currency_id' => $currency['id'],
                            ]);
                        }
                    }
                }
                //fin asociación de promociones  a currencies

                //asociación de promociones  a languages
                if ($request->get('languages') != '') {
                    foreach ($request->get('languages') as $lenguage) {
                        if (isset($lenguage['id']) && $lenguage['id'] != '') {
                            PromotionLanguage::create([
                                'promotion_id' => $promocion->id,
                                'language_id' => $lenguage['id'],
                            ]);
                        }
                    }
                }
                //fin asociación de promociones  a languages

                //asociación de promociones  a payment method
                if ($request->get('payment_methods') != '') {
                    foreach ($request->get('payment_methods') as $payment) {
                        if (isset($payment['id']) && $payment['id'] != '') {
                            PromotionPaymentMethod::create([
                                'promotion_id' => $promocion->id,
                                'payment_method_id' => $payment['id'],
                            ]);
                        }
                    }
                }
                //fin asociación de promociones  a payment method

                //asociación de promociones  a localizaciones para transporte
                if ($request->get('pickups') != '') {
                    foreach ($request->get('pickups') as $pickup) {
                        if (isset($pickup['id']) && $pickup['id'] != '') {
                            PromotionPickup::create([
                                'promotion_id' => $promocion->id,
                                'pickup_id' => $pickup['id'],
                            ]);
                        }
                    }
                }
                //fin asociación de promociones  a localizaciones para transporte

                $promocion->promotionFile()->delete();
                //Crear relación con archivo
                if ($request->get('files') != '') {
                    foreach ($request->get('files') as $file) {
                        PromotionFile::create([
                            'promotion_id' => $promocion->id,
                            'file_id' => $file,
                        ]);
                    }
                }
                //FIN crear relación con archivo

                //Crea relación con tipos de cliente
                if ($request->get('client_types') != '') {
                    foreach ($request->get('client_types') as $client_type) {
                        if (isset($client_type['id']) && $client_type['id'] != '') {
                            PromotionClientType::create([
                                'promotion_id' => $promocion->id,
                                'client_type_id' => $client_type['id'],
                            ]);
                        }
                    }
                }
                //Fin crear relación con tipos de cliente

                //Crear relación con dispositivos
                if ($request->get('devices') != '') {
                    foreach ($request->get('devices') as $device) {
                        if (isset($device['id']) && $device['id'] != '') {
                            PromotionDevice::create([
                                'promotion_id' => $promocion->id,
                                'device_id' => $device['id'],
                            ]);
                        }
                    }
                }
                //Fin crear relación con dispositivos

                //asociación de promociones  a rangos de fecha de venta
                if ($request->get('range_sale_date') != '') {
                    foreach ($request->get('range_sale_date') as $range_sale) {
                        if (isset($range_sale['sale_date_start']) && $range_sale['sale_date_start'] != '' &&
                            isset($range_sale['sale_date_end']) && $range_sale['sale_date_end'] != '' &&
                            isset($range_sale['sale_days']) && $range_sale['sale_days'] != '') {
                            $registro = PromotionRangeSaleDay::create([
                                'promotion_id' => $promocion->id,
                                'sale_date_start' => $range_sale['sale_date_start'],
                                'sale_date_end' => $range_sale['sale_date_end'],
                            ]);
                            foreach ($range_sale['sale_days'] as $sale_day) {
                                PromotionSaleDay::create([
                                    'promotion_range_sale_day_id' => $registro->id,
                                    'day_id' => $sale_day['id'],
                                ]);
                            }
                        }
                    }
                }
                //fin asociación de promociones  a rangos de fecha de venta

                //asociación de promociones  a rangos de fecha de disfrute
                if ($request->get('range_enjoy_date') != '') {
                    foreach ($request->get('range_enjoy_date') as $range_enjoy) {
                        if (isset($range_enjoy['enjoy_date_start']) && $range_enjoy['enjoy_date_start'] != '' &&
                            isset($range_enjoy['enjoy_date_end']) && $range_enjoy['enjoy_date_end'] != '' &&
                            isset($range_enjoy['enjoy_days']) && $range_enjoy['enjoy_days'] != '') {
                            $registro = PromotionRangeEnjoyDay::create([
                                'promotion_id' => $promocion->id,
                                'enjoy_date_start' => $range_enjoy['enjoy_date_start'],
                                'enjoy_date_end' => $range_enjoy['enjoy_date_end'],
                            ]);

                            foreach ($range_enjoy['enjoy_days'] as $sale_day) {
                                PromotionEnjoyDay::create([
                                    'promotion_range_enjoy_day_id' => $registro->id,
                                    'day_id' => $sale_day['id'],
                                ]);
                            }
                        }
                    }
                }
                //fin asociación de promociones  a rangos de fecha de disfrute

                //Asociación de promociones acumulativas
                if($request->get('promotion_accumulate') != ''){
                    foreach ($request->get('promotion_accumulate') as $accumulate){
                        PromotionAccumulate::create([
                            'promotion_id' => $promocion->id,
                            'promotion_accumulate_id' => $accumulate['id'],
                        ]);
                    }
                }
                //FIN asociación de promociones acumulativas


            } elseif ($error == 1) {
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } elseif ($error == 2) {
                $array['error'] = 404;
                $array['error_description'] = 'Data not found';
            } elseif ($error == 3) {
                $array['error'] = 419;
                $array['error_description'] = 'You do not have a valid plan';
                $array['error_rule_code'] = $rule_codes;

                //Heimdall - datos para redirección
                $array['data'] = array();
                $array['data'][0]['website_id'] = $website->id;

                $protocol = DB::table('saas_settings')
                ->where('saas_settings.name', 'protocol')->first()->value;
                $base_path_saas = DB::table('saas_settings')
                ->where('saas_settings.name', 'base_path_saas')->first()->value;
                $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Promociones');
        }
        return response()->json($array, $array['error']);
    }


    /**
     * Función que lista todas las promociones
     *
     * Para la consulta de promociones se realiza una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();
            // Validación de campos
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'page' => 'integer',
                'limit' => 'integer',
                'visibility' => 'boolean',
                'accumulate' => 'boolean',
                'promotion_type_id' => 'integer|exists:tenant.mo_promotion_type,id,deleted_at,NULL',
                'campaign_id' => 'integer|exists:tenant.mo_campaign,id,deleted_at,NULL',
                'department_id' => 'integer|min:0|exists:tenant.mo_department,id,deleted_at,NULL',
                'cost_center_id' => 'integer|min:0|exists:tenant.mo_cost_center,id,deleted_at,NULL',
                'device_id' => 'integer|min:0|exists:tenant.mo_device,id,deleted_at,NULL',
                'apply_product_filter' => 'boolean',
                'apply_product_type_filter' => 'boolean',
                'apply_service_filter' => 'boolean',
                'apply_subchannel_filter' => 'boolean',
                'apply_country_filter' => 'boolean',
                'apply_state_filter' => 'boolean',
                'apply_language_filter' => 'boolean',
                'apply_currency_filter' => 'boolean',
                'apply_payment_method_filter' => 'boolean',
                'apply_pickup_filter' => 'boolean',
                'apply_client_type_filter' => 'boolean',
                'apply_device_filter' => 'boolean',
                'lockers_validation' => 'boolean',
                'supervisor_validation' => 'boolean',
                'allow_date_change' => 'boolean',
                'allow_upgrade' => 'boolean',
                'allow_product_change' => 'boolean',
                'pay_difference' => 'boolean',
                'anticipation_start' => 'integer|min:0',
                'anticipation_end' => 'integer|min:0',
                'coupon_type' => 'boolean',
                'coupon_sheet_start' => 'integer',
                'coupon_sheet_end' => 'integer',
                'coupon_total_uses' => 'integer',
                'product_id' => 'integer|exists:tenant.mo_product,id,deleted_at,NULL',
                'service_id' => 'integer|exists:tenant.mo_service,id,deleted_at,NULL',
                'country_id' => 'integer|exists:tenant.mo_country,id,deleted_at,NULL',
                'state_id' => 'integer|exists:tenant.mo_state,id,deleted_at,NULL',
                'client_type_id' => 'integer|exists:tenant.mo_client_type,id,deleted_at,NULL',
                'product_type_id' => 'integer|exists:tenant.mo_product_type,id,deleted_at,NULL',
                'subchannel_id' => 'integer|exists:tenant.mo_subchannel,id,deleted_at,NULL',
                'day_id' => 'integer|min:0|exists:tenant.mo_day,id,deleted_at,NULL',
                'payment_method_id' => 'integer|min:0|exists:tenant.mo_payment_method,id,deleted_at,NULL',
                'currency_id' => 'integer|min:0|exists:tenant.mo_currency,id,deleted_at,NULL',
                'language_id' => 'integer|exists:tenant.mo_language,id,deleted_at,NULL',
                'pickup_id' => 'integer|exists:tenant.mo_transportation_location,id,deleted_at,NULL',
                'benefit_card' => 'boolean',
                'benefit_card_id' => 'exists:tenant.mo_identification,id,deleted_at,NULL|integer|min:0',
                'benefit_card_total_uses' => 'integer|min:0',
                'range_sale_date_start' => 'required_with:range_sale_date_end',
                'range_sale_date_end' => 'required_with:range_sale_date_start',
                'enjoy_date_start' => 'required_with:enjoy_date_end',
                'enjoy_date_end' => 'required_with:enjoy_date_start',
                'valid' => 'boolean'
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }

            if ($request->get('lang')) {
                $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->where('front', 1)->first();
                if (!$idioma) {
                    $error = 1;
                    $mensaje_validador = $mensaje_validador->merge(['lang' => ['The lang is not exists']]);
                }
            }

            // FIN Validación de campos

            if ($error == 1) {
                //Si se produce error en validación enviamos mensaje de error
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][] = $mensaje_validador;

            } else {
                //Precompiladas
                $promociones = DB::connection('tenant')->table('mo_promotion')
                    ->select(
                        'mo_promotion.id',
                        'mo_promotion.code',
                        'mo_promotion.order',
                        'mo_promotion.visibility',
                        'mo_promotion.accumulate',
                        'mo_promotion.min_quantity',
                        'mo_promotion.max_quantity',
                        'mo_promotion.promotion_amount',
                        'mo_promotion.currency_id',
                        'mo_promotion.department_id',
                        'mo_promotion.cost_center_id',
                        'mo_promotion.promotion_type_id',
                        'mo_promotion.campaign_id',
                        'mo_promotion.apply_product_filter',
                        'mo_promotion.apply_product_type_filter',
                        'mo_promotion.apply_service_filter',
                        'mo_promotion.apply_subchannel_filter',
                        'mo_promotion.apply_country_filter',
                        'mo_promotion.apply_state_filter',
                        'mo_promotion.apply_payment_method_filter',
                        'mo_promotion.apply_language_filter',
                        'mo_promotion.apply_currency_filter',
                        'mo_promotion.apply_pickup_filter',
                        'mo_promotion.apply_client_type_filter',
                        'mo_promotion.apply_device_filter',
                        'mo_promotion.lockers_validation',
                        'mo_promotion.supervisor_validation',
                        'mo_promotion.allow_date_change',
                        'mo_promotion.allow_upgrade',
                        'mo_promotion.allow_product_change',
                        'mo_promotion.pay_difference',
                        'mo_promotion.anticipation_start',
                        'mo_promotion.anticipation_end',
                        'mo_promotion.coupon_code',
                        'mo_promotion.coupon_type',
                        'mo_promotion.coupon_sheet_start',
                        'mo_promotion.coupon_sheet_end',
                        'mo_promotion.coupon_total_uses',
                        'mo_promotion.benefit_card',
                        'mo_promotion.benefit_card_id',
                        'mo_promotion.benefit_card_total_uses',
                        'mo_promotion.user_id'
                    )
                    ->where('mo_promotion.deleted_at', '=', null)
                    ->join('mo_promotion_translation', 'mo_promotion.id', 'mo_promotion_translation.promotion_id')
                    ->join('mo_campaign','mo_promotion.campaign_id', 'mo_campaign.id')
                    ->join('mo_campaign_translation','mo_campaign.id', 'mo_campaign_translation.campaign_id')
                    ->leftJoin('mo_promotion_product', function ($join) {
                        $join->on('mo_promotion_product.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product.deleted_at');
                    })
                    ->leftJoin('mo_promotion_product_type', function ($join) {
                        $join->on('mo_promotion_product_type.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product_type.deleted_at');
                    })
                    ->leftJoin('mo_promotion_service', function ($join) {
                        $join->on('mo_promotion_service.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_service.deleted_at');
                    })
                    ->leftJoin('mo_promotion_subchannel', function ($join) {
                        $join->on('mo_promotion_subchannel.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_subchannel.deleted_at');
                    })
                    ->leftJoin('mo_promotion_country', function ($join) {
                        $join->on('mo_promotion_country.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_country.deleted_at');
                    })
                    ->leftJoin('mo_promotion_state', function ($join) {
                        $join->on('mo_promotion_state.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_state.deleted_at');
                    })
                    ->leftJoin('mo_promotion_client_type', function ($join) {
                        $join->on('mo_promotion_client_type.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_client_type.deleted_at');
                    })
                    ->leftJoin('mo_promotion_language', function ($join) {
                        $join->on('mo_promotion_language.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_language.deleted_at');
                    })
                    ->leftJoin('mo_promotion_payment_method', function ($join) {
                        $join->on('mo_promotion_payment_method.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_payment_method.deleted_at');
                    })
                    ->leftJoin('mo_promotion_currency', function ($join) {
                        $join->on('mo_promotion_currency.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_currency.deleted_at');
                    })
                    ->leftJoin('mo_promotion_pickup', function ($join) {
                        $join->on('mo_promotion_pickup.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_pickup.deleted_at');
                    })
                    ->leftJoin('mo_promotion_range_sale_day', function ($join) {
                        $join->on('mo_promotion_range_sale_day.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_sale_day.deleted_at');
                    })
                    ->leftJoin('mo_promotion_range_enjoy_day', function ($join) {
                        $join->on('mo_promotion_range_enjoy_day.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_enjoy_day.deleted_at');
                    })
                    ->leftJoin('mo_promotion_device', function ($join) {
                        $join->on('mo_promotion_device.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_device.deleted_at');
                    })
                    ->where('mo_promotion_translation.deleted_at', '=', null)
                    ->where('mo_campaign.deleted_at', '=', null)
                    ->where('mo_campaign_translation.deleted_at', '=', null);

                $sub = DB::connection('tenant')->table('mo_promotion')
                    ->select('mo_promotion.id')
                    ->where('mo_promotion.deleted_at', '=', null)
                    ->join('mo_promotion_translation', 'mo_promotion.id', 'mo_promotion_translation.promotion_id')
                    ->join('mo_campaign','mo_promotion.campaign_id', 'mo_campaign.id')
                    ->join('mo_campaign_translation','mo_campaign.id', 'mo_campaign_translation.campaign_id')                    ->leftJoin('mo_promotion_product', function ($join) {
                        $join->on('mo_promotion_product.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product.deleted_at');
                    })
                    ->leftJoin('mo_promotion_product_type', function ($join) {
                        $join->on('mo_promotion_product_type.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_product_type.deleted_at');
                    })
                    ->leftJoin('mo_promotion_service', function ($join) {
                        $join->on('mo_promotion_service.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_service.deleted_at');
                    })
                    ->leftJoin('mo_promotion_subchannel', function ($join) {
                        $join->on('mo_promotion_subchannel.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_subchannel.deleted_at');
                    })
                    ->leftJoin('mo_promotion_country', function ($join) {
                        $join->on('mo_promotion_country.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_country.deleted_at');
                    })
                    ->leftJoin('mo_promotion_state', function ($join) {
                        $join->on('mo_promotion_state.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_state.deleted_at');
                    })
                    ->leftJoin('mo_promotion_client_type', function ($join) {
                        $join->on('mo_promotion_client_type.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_client_type.deleted_at');
                    })
                    ->leftJoin('mo_promotion_language', function ($join) {
                        $join->on('mo_promotion_language.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_language.deleted_at');
                    })
                    ->leftJoin('mo_promotion_payment_method', function ($join) {
                        $join->on('mo_promotion_payment_method.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_payment_method.deleted_at');
                    })
                    ->leftJoin('mo_promotion_currency', function ($join) {
                        $join->on('mo_promotion_currency.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_currency.deleted_at');
                    })
                    ->leftJoin('mo_promotion_pickup', function ($join) {
                        $join->on('mo_promotion_pickup.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_pickup.deleted_at');
                    })
                    ->leftJoin('mo_promotion_range_sale_day', function ($join) {
                        $join->on('mo_promotion_range_sale_day.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_sale_day.deleted_at');
                    })
                    ->leftJoin('mo_promotion_range_enjoy_day', function ($join) {
                        $join->on('mo_promotion_range_enjoy_day.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_range_enjoy_day.deleted_at');
                    })
                    ->leftJoin('mo_promotion_device', function ($join) {
                        $join->on('mo_promotion_device.promotion_id', '=', 'mo_promotion.id')
                            ->whereNull('mo_promotion_device.deleted_at');
                    })
                    ->where('mo_promotion_translation.deleted_at', '=', null)
                    ->where('mo_campaign.deleted_at', '=', null)
                    ->where('mo_campaign_translation.deleted_at', '=', null);

                //Fin precompiladas

                //Filtros de busqueda

                if ($request->get('country_id') != '') {

                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_country_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_country_filter',1)
                                    ->where('mo_promotion_country.country_id',$request->get('country_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_country_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_country_filter',1)
                                    ->where('mo_promotion_country.country_id',$request->get('country_id'));
                            });
                    });
                }

                if ($request->get('state_id') != '') {

                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_state_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_state_filter',1)
                                    ->where('mo_promotion_state.state_id',$request->get('state_id'));
                            });
                    });

                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_state_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_state_filter',1)
                                    ->where('mo_promotion_state.state_id',$request->get('state_id'));
                            });
                    });
                }

                if ($request->get('client_type_id') != '') {
                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_client_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_client_type_filter',1)
                                    ->where('mo_promotion_client_type.client_type_id',$request->get('client_type_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_client_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_client_type_filter',1)
                                    ->where('mo_promotion_client_type.client_type_id',$request->get('client_type_id'));
                            });
                    });
                }

                if ($request->get('subchannel_id') != '') {

                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_subchannel_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_subchannel_filter',1)
                                    ->where('mo_promotion_subchannel.subchannel_id',$request->get('subchannel_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_subchannel_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_subchannel_filter',1)
                                    ->where('mo_promotion_subchannel.subchannel_id',$request->get('subchannel_id'));
                            });
                    });
                }

                if ($request->get('product_id') != '') {

                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_product_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_filter',1)
                                    ->where('mo_promotion_product.product_id',$request->get('product_id'));
                            });
                    });

                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_product_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_filter',1)
                                    ->where('mo_promotion_product.product_id',$request->get('product_id'));
                            });
                    });
                }

                if ($request->get('product_type_id') != '') {
                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_product_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_type_filter',1)
                                    ->where('mo_promotion_product_type.product_type_id',$request->get('product_type_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_product_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_type_filter',1)
                                    ->where('mo_promotion_product_type.product_type_id',$request->get('product_type_id'));
                            });
                    });
                }


                if ($request->get('service_id') != '') {
                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_product_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_type_filter',1)
                                    ->where('mo_promotion_service.service_id',$request->get('service_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_product_type_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_product_type_filter',1)
                                    ->where('mo_promotion_service.service_id',$request->get('service_id'));
                            });
                    });
                }

                if ($request->get('payment_method_id') != '') {
                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_payment_method_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_payment_method_filter',1)
                                    ->where('mo_promotion_payment_method.payment_method_id',$request->get('payment_method_id'));
                            });
                    });
                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_payment_method_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_payment_method_filter',1)
                                    ->where('mo_promotion_payment_method.payment_method_id',$request->get('payment_method_id'));
                            });
                    });
                }

                if ($request->get('currency_id') != '') {

                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_language_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_language_filter',1)
                                    ->where('mo_promotion_currency.currency_id',$request->get('currency_id'));
                            });
                    });

                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_language_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_language_filter',1)
                                    ->where('mo_promotion_currency.currency_id',$request->get('currency_id'));
                            });
                    });
                }

                if ($request->get('department_id') != '') {
                    $promociones->where('mo_promotion.department_id', '=', $request->get('department_id'));
                    $sub->where('mo_promotion.department_id', '=', $request->get('department_id'));
                }

                if ($request->get('cost_center_id') != '') {
                    $promociones->where('mo_promotion.cost_center_id', '=', $request->get('cost_center_id'));
                    $sub->where('mo_promotion.cost_center_id', '=', $request->get('cost_center_id'));
                }

                if ($request->get('device_id') != '') {
                    $promociones->when('mo_promotion.apply_device_filter' == 1, function ($query) use ($request){
                        $query->where('mo_promotion_device.device_id',$request->get('device_id'));
                    });
                    $sub->when('mo_promotion.apply_device_filter' == 1, function ($query) use ($request){
                        $query->where('mo_promotion_device.device_id',$request->get('device_id'));
                    });
                }

                if ($request->get('language_id') != '') {

                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_language_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_language_filter',1)
                                    ->where('mo_promotion_language.language_id',$request->get('language_id'));
                            });
                    });

                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_language_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_language_filter',1)
                                    ->where('mo_promotion_language.language_id',$request->get('language_id'));
                            });
                    });
                }

                if ($request->get('pickup_id') != '') {

                    $promociones->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_pickup_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_pickup_filter',1)
                                    ->where('mo_promotion_pickup.pickup_id',$request->get('pickup_id'));
                            });
                    });

                    $sub->where(function ($query) use ($request){
                        $query->where('mo_promotion.apply_pickup_filter', 0)
                            ->orWhere(function ($query) use ($request) {
                                $query->where('mo_promotion.apply_pickup_filter',1)
                                    ->where('mo_promotion_pickup.pickup_id',$request->get('pickup_id'));
                            });
                    });
                }

                if ($request->get('coupon_total_uses') != '') {
                    $promociones->where('mo_promotion.coupon_total_uses', '=', $request->get('coupon_total_uses'));
                    $sub->where('mo_promotion.coupon_total_uses', '=', $request->get('coupon_total_uses'));
                }

                if ($request->get('coupon_sheet_end') != '') {
                    $promociones->where('mo_promotion.coupon_sheet_end', '<=', $request->get('coupon_sheet_end'));
                    $sub->where('mo_promotion.coupon_sheet_end', '<=', $request->get('coupon_sheet_end'));
                }

                if ($request->get('coupon_sheet_start') != '') {
                    $promociones->where('mo_promotion.coupon_sheet_start', '>=', $request->get('coupon_sheet_start'));
                    $sub->where('mo_promotion.coupon_sheet_start', '>=', $request->get('coupon_sheet_start'));
                }

                if ($request->get('coupon_type') != '') {
                    $promociones->where('mo_promotion.coupon_type', '=', $request->get('coupon_type'));
                    $sub->where('mo_promotion.coupon_type', '=', $request->get('coupon_type'));
                }

                if ($request->get('anticipation_end') != '') {
                    $promociones->where('mo_promotion.anticipation_end', '<=', $request->get('anticipation_end'));
                    $sub->where('mo_promotion.anticipation_end', '<=', $request->get('anticipation_end'));
                }

                if ($request->get('anticipation_start') != '') {
                    $promociones->where('mo_promotion.anticipation_start', '>=', $request->get('anticipation_start'));
                    $sub->where('mo_promotion.anticipation_start', '>=', $request->get('anticipation_start'));
                }

                if ($request->get('apply_country_filter') != '') {
                    $promociones->where('mo_promotion.apply_country_filter', '=', $request->get('apply_country_filter'));
                    $sub->where('mo_promotion.apply_country_filter', '=', $request->get('apply_country_filter'));
                }

                if ($request->get('apply_state_filter') != '') {
                    $promociones->where('mo_promotion.apply_state_filter', '=', $request->get('apply_state_filter'));
                    $sub->where('mo_promotion.apply_state_filter', '=', $request->get('apply_state_filter'));
                }

                if ($request->get('apply_subchannel_filter') != '') {
                    $promociones->where('mo_promotion.apply_subchannel_filter', '=', $request->get('apply_subchannel_filter'));
                    $sub->where('mo_promotion.apply_subchannel_filter', '=', $request->get('apply_subchannel_filter'));
                }

                if ($request->get('apply_service_filter') != '') {
                    $promociones->where('mo_promotion.apply_service_filter', '=', $request->get('apply_service_filter'));
                    $sub->where('mo_promotion.apply_service_filter', '=', $request->get('apply_service_filter'));
                }

                if ($request->get('apply_product_type_filter') != '') {
                    $promociones->where('mo_promotion.apply_product_type_filter', '=', $request->get('apply_product_type_filter'));
                    $sub->where('mo_promotion.apply_product_type_filter', '=', $request->get('apply_product_type_filter'));
                }

                if ($request->get('apply_product_filter') != '') {
                    $promociones->where('mo_promotion.apply_product_filter', '=', $request->get('apply_product_filter'));
                    $sub->where('mo_promotion.apply_product_filter', '=', $request->get('apply_product_filter'));
                }

                if ($request->get('apply_payment_method_filter') != '') {
                    $promociones->where('mo_promotion.apply_payment_method_filter', '=', $request->get('apply_payment_method_filter'));
                    $sub->where('mo_promotion.apply_payment_method_filter', '=', $request->get('apply_payment_method_filter'));
                }

                if ($request->get('apply_language_filter') != '') {
                    $promociones->where('mo_promotion.apply_language_filter', '=', $request->get('apply_language_filter'));
                    $sub->where('mo_promotion.apply_language_filter', '=', $request->get('apply_language_filter'));
                }

                if ($request->get('apply_currency_filter') != '') {
                    $promociones->where('mo_promotion.apply_currency_filter', '=', $request->get('apply_currency_filter'));
                    $sub->where('mo_promotion.apply_currency_filter', '=', $request->get('apply_currency_filter'));
                }

                if ($request->get('apply_pickup_filter') != '') {
                    $promociones->where('mo_promotion.apply_pickup_filter', '=', $request->get('apply_pickup_filter'));
                    $sub->where('mo_promotion.apply_pickup_filter', '=', $request->get('apply_pickup_filter'));
                }

                if ($request->get('apply_client_type_filter') != '') {
                    $promociones->where('mo_promotion.apply_client_type_filter', '=', $request->get('apply_client_type_filter'));
                    $sub->where('mo_promotion.apply_client_type_filter', '=', $request->get('apply_client_type_filter'));
                }

                if ($request->get('apply_device_filter') != '') {
                    $promociones->where('mo_promotion.apply_device_filter', '=', $request->get('apply_device_filter'));
                    $sub->where('mo_promotion.apply_device_filter', '=', $request->get('apply_device_filter'));
                }

                if ($request->get('lockers_validation') != '') {
                    $promociones->where('mo_promotion.lockers_validation', '=', $request->get('lockers_validation'));
                    $sub->where('mo_promotion.lockers_validation', '=', $request->get('lockers_validation'));
                }


                if ($request->get('supervisor_validation') != '') {
                    $promociones->where('mo_promotion.supervisor_validation', '=', $request->get('supervisor_validation'));
                    $sub->where('mo_promotion.supervisor_validation', '=', $request->get('supervisor_validation'));
                }


                if ($request->get('allow_date_change') != '') {
                    $promociones->where('mo_promotion.allow_date_change', '=', $request->get('allow_date_change'));
                    $sub->where('mo_promotion.allow_date_change', '=', $request->get('allow_date_change'));
                }


                if ($request->get('allow_upgrade') != '') {
                    $promociones->where('mo_promotion.allow_upgrade', '=', $request->get('allow_upgrade'));
                    $sub->where('mo_promotion.allow_upgrade', '=', $request->get('allow_upgrade'));
                }


                if ($request->get('allow_product_change') != '') {
                    $promociones->where('mo_promotion.allow_product_change', '=', $request->get('allow_product_change'));
                    $sub->where('mo_promotion.allow_product_change', '=', $request->get('allow_product_change'));
                }


                if ($request->get('pay_difference') != '') {
                    $promociones->where('mo_promotion.pay_difference', '=', $request->get('pay_difference'));
                    $sub->where('mo_promotion.pay_difference', '=', $request->get('pay_difference'));
                }

                if ($request->get('promotion_type_id') != '') {
                    $promociones->where('mo_promotion.promotion_type_id', '=', $request->get('promotion_type_id'));
                    $sub->where('mo_promotion.promotion_type_id', '=', $request->get('promotion_type_id'));
                }

                if ($request->get('campaign_id') != '') {
                    $promociones->where('mo_promotion.campaign_id', '=', $request->get('campaign_id'));
                    $sub->where('mo_promotion.campaign_id', '=', $request->get('campaign_id'));
                }

                if ($request->get('campaign') != '') {
                    $promociones->Where(function ($query) use ($request) {
                        $query->where('mo_campaign_translation.name', 'Like', '%' . $request->get('campaign') . '%')
                            ->orWhere('mo_campaign_translation.description', 'Like', '%' . $request->get('campaign') . '%');
                    });
                    $sub->Where(function ($query) use ($request) {
                        $query->where('mo_campaign_translation.name', 'Like', '%' . $request->get('campaign') . '%')
                            ->orWhere('mo_campaign_translation.description', 'Like', '%' . $request->get('campaign') . '%');
                    });
                }

                if ($request->get('accumulate') != '') {
                    $promociones->where('mo_promotion.accumulate', '=', $request->get('accumulate'));
                    $sub->where('mo_promotion.accumulate', '=', $request->get('accumulate'));
                }

                if ($request->get('visibility') != '') {
                    $promociones->where('mo_promotion.visibility', '=', $request->get('visibility'));
                    $sub->where('mo_promotion.visibility', '=', $request->get('visibility'));
                }

                if ($request->get('lang') != '') {
                    $idioma = $idiomas->where('abbreviation', Str::upper($request->get('lang')))->first();
                    $promociones->where('mo_promotion_translation.language_id', '=', $idioma->id);
                    $sub->where('mo_promotion_translation.language_id', '=', $idioma->id);
                }


                if ($request->get('benefit_card') != '') {
                    $promociones->where('mo_promotion.benefit_card', '=', $request->get('benefit_card'));
                    $sub->where('mo_promotion.benefit_card', '=', $request->get('benefit_card'));
                }


                if ($request->get('benefit_card_id') != '') {
                    $promociones->where('mo_promotion.benefit_card_id', '=', $request->get('benefit_card_id'));
                    $sub->where('mo_promotion.benefit_card_id', '=', $request->get('benefit_card_id'));
                }


                if ($request->get('benefit_card_uses') != '') {
                    $promociones->where('mo_promotion.benefit_card_total_uses', '=', $request->get('benefit_card_uses'));
                    $sub->where('mo_promotion.benefit_card_total_uses', '=', $request->get('benefit_card_uses'));
                }

                if ($request->get('range_sale_date_start') != '' && $request->get('range_sale_date_end') != '') {
                    $promociones->where('mo_promotion_range_sale_day.sale_date_start', '>=', Carbon::parse($request->get('range_sale_date_start'))->startOfDay())
                        ->where('mo_promotion_range_sale_day.sale_date_end', '<=', Carbon::parse($request->get('range_sale_date_end'))->endOfDay());

                    $sub->where('mo_promotion_range_sale_day.sale_date_start', '>=', Carbon::parse($request->get('range_sale_date_start'))->startOfDay())
                        ->where('mo_promotion_range_sale_day.sale_date_end', '<=', Carbon::parse($request->get('range_sale_date_end'))->endOfDay());
                }

                if ($request->get('enjoy_date_start') != '' && $request->get('enjoy_date_end') != '') {
                    $promociones->where('mo_promotion_range_enjoy_day.enjoy_date_start', '>=', Carbon::parse($request->get('enjoy_date_start'))->startOfDay())
                        ->where('mo_promotion_range_enjoy_day.enjoy_date_end', '<=', Carbon::parse($request->get('enjoy_date_end'))->endOfDay());

                    $sub->where('mo_promotion_range_enjoy_day.enjoy_date_start', '>=', Carbon::parse($request->get('enjoy_date_start'))->startOfDay())
                        ->where('mo_promotion_range_enjoy_day.enjoy_date_end', '<=', Carbon::parse($request->get('enjoy_date_end'))->endOfDay());
                }

                if ($request->get('valid') == 1) {
                    $now = Carbon::now()->startOfDay()->format('Y-m-d H:i:s');
                    $promociones->where(function ($query) use ($now) {
                        $query->where('mo_promotion_range_enjoy_day.enjoy_date_end', '>=', $now)
                            ->orWhere('mo_promotion_range_sale_day.sale_date_end', '>=', $now);
                    });
                    $sub->where(function ($query) use ($now) {
                        $query->where('mo_promotion_range_enjoy_day.enjoy_date_end', '>=', $now)
                            ->orWhere('mo_promotion_range_sale_day.sale_date_end', '>=', $now);
                    });
                }

                //Fin de filtros

                // Order
                $orden = 'mo_promotion_translation.name';
                $request_order = $request->get('order');
                if ($request_order != '') {
                    switch ($request->get('order')) {
                        case 'name':
                            $orden = 'mo_promotion_translation.name';
                            break;
                        case 'description':
                            $orden = 'mo_promotion_translation.description';
                            break;
                        case 'short_description':
                            $orden = 'mo_promotion_translation.short_description';
                            break;
                        case 'friendly_url':
                            $orden = 'mo_promotion_translation.friendly_url';
                            break;
                        case 'title_seo':
                            $orden = 'mo_promotion_translation.title_seo';
                            break;
                        case 'description_seo':
                            $orden = 'mo_promotion_translation.description_seo';
                            break;
                        case 'keywords_seo':
                            $orden = 'mo_promotion_translation.keywords_seo';
                            break;
                        case 'observations':
                            $orden = 'mo_promotion_translation.observations';
                            break;
                        case 'legal':
                            $orden = 'mo_promotion_translation.legal';
                            break;
                        case 'gift':
                            $orden = 'mo_promotion_translation.gift';
                            break;
                        case 'id':
                            $orden = 'mo_promotion.id';
                            break;
                        case 'code':
                            $orden = 'mo_promotion.code';
                            break;
                        case 'visibility':
                            $orden = 'mo_promotion.visibility';
                            break;
                        case 'min_quantity':
                            $orden = 'mo_promotion.min_quantity';
                            break;
                        case 'max_quantity':
                            $orden = 'mo_promotion.max_quantity';
                            break;
                        case 'promotion_amount':
                            $orden = 'mo_promotion.promotion_amount';
                            break;
                        case 'promotion_type_id':
                            $orden = 'mo_promotion.promotion_type_id';
                            break;
                        case 'campaign_id':
                            $orden = 'mo_promotion.campaign_id';
                            break;
                        case 'department_id':
                            $orden = 'mo_promotion.department_id';
                            break;
                        case 'cost_center_id':
                            $orden = 'mo_promotion.cost_center_id';
                            break;
                        case 'apply_product_filter':
                            $orden = 'mo_promotion.apply_product_filter';
                            break;
                        case 'apply_product_type_filter':
                            $orden = 'mo_promotion.apply_product_type_filter';
                            break;
                        case 'apply_service_filter':
                            $orden = 'mo_promotion.apply_service_filter';
                            break;
                        case 'apply_subchannel_filter':
                            $orden = 'mo_promotion.apply_subchannel_filter';
                            break;
                        case 'apply_country_filter':
                            $orden = 'mo_promotion.apply_country_filter';
                            break;
                        case 'apply_state_filter':
                            $orden = 'mo_promotion.apply_state_filter';
                            break;
                        case 'apply_client_type_filter':
                            $orden = 'mo_promotion.apply_client_type_filter';
                            break;
                        case 'apply_payment_method_filter':
                            $orden = 'mo_promotion.apply_payment_method_filter';
                            break;
                        case 'apply_language_filter':
                            $orden = 'mo_promotion.apply_language_filter';
                            break;
                        case 'apply_currency_filter':
                            $orden = 'mo_promotion.apply_currency_filter';
                            break;
                        case 'apply_pickup_filter':
                            $orden = 'mo_promotion.apply_pickup_filter';
                            break;
                        case 'lockers_validation':
                            $orden = 'mo_promotion.lockers_validation';
                            break;
                        case 'supervisor_validation':
                            $orden = 'mo_promotion.supervisor_validation';
                            break;
                        case 'anticipation_start':
                            $orden = 'mo_promotion.anticipation_start';
                            break;
                        case 'anticipation_end':
                            $orden = 'mo_promotion.anticipation_end';
                            break;
                        case 'coupon_code':
                            $orden = 'mo_promotion.coupon_code';
                            break;
                        case 'coupon_type':
                            $orden = 'mo_promotion.coupon_type';
                            break;
                        case 'coupon_sheet_start':
                            $orden = 'mo_promotion.coupon_sheet_start';
                            break;
                        case 'coupon_sheet_end':
                            $orden = 'mo_promotion.coupon_sheet_end';
                            break;
                        case 'coupon_total_uses':
                            $orden = 'mo_promotion.coupon_total_uses';
                            break;
                        case 'accumulate':
                            $orden = 'mo_promotion.accumulate';
                            break;
                        case 'order':
                            $orden = 'mo_promotion.order';
                            break;
                        case 'benefit_card':
                            $orden = 'mo_promotion.benefit_card';
                            break;
                        case 'benefit_card_id':
                            $orden = 'mo_promotion.benefit_card_id';
                            break;
                        case 'benefit_card_uses':
                            $orden = 'mo_promotion.benefit_card_total_uses';
                            break;
                        default:
                            $orden = 'mo_promotion_translation.name';
                            break;
                    }
                }

                // FIN Order

                // Order_way
                $sentido = 'asc';
                $request_order_way = $request->get('order_way');
                if ($request_order_way != '') {
                    switch ($request->get('order_way')) {
                        case 'asc':
                            $sentido = 'asc';
                            break;
                        case 'desc':
                            $sentido = 'desc';
                            break;
                        default:
                            $sentido = 'asc';
                            break;
                    }
                }
                // FIN Order_way

                $promociones = $promociones->groupBy('mo_promotion.id')->orderBy($orden, $sentido);


                // Paginación según filtros y ejecución de la consulta
                if ($request->get('limit') != '0') {

                    $settings = Settings::where('name', '=', 'limit_registers')->first();
                    $limite = $settings->value;

                    if ($request->get('limit') != '' && $request->get('limit') <= $limite) {
                        $limite = $request->get('limit');
                    }

                    $inicio = 0;
                    if ($request->get('page') != '') {
                        $inicio = $request->get('page');
                    }

                    $promociones = $promociones->forPage($inicio, $limite)->get();

                    //si filtro limit = 0 se obtienen todos los resultados
                } else {

                    $promociones = $promociones->get();

                }

                //Fin de paginación


                $sub->groupBy('mo_promotion_translation.promotion_id');
                $promotions_count = DB::connection('tenant')->table(DB::connection('tenant')->raw("({$sub->toSql()}) as sub"))
                    ->mergeBindings($sub);


                $totales = $promotions_count->count();


                $array['data'] = array();

                //Se recorren las promociones
                foreach ($promociones as $promocion) {

                    $promocion->campaign = null;

                    $promocion->lang = [];
                    foreach ($idiomas as $idi) {
                        //Se obtienen las traducciones de la promocion
                        $traduccion = PromotionTranslation::where('promotion_id', '=', $promocion->id)
                            ->where('language_id', '=', $idi->id)
                            ->get(['id', 'language_id', 'name', 'gift', 'description', 'short_description', 'friendly_url', 'external_url', 'title_seo', 'description_seo', 'keywords_seo', 'observations', 'legal', 'notes']);
                        foreach ($traduccion as $trad) {
                            $promocion->lang[][$idi->abbreviation] = $trad;
                        }

                        $datos_campana_traduccion = DB::connection('tenant')->table('mo_campaign')
                            ->select('mo_campaign_translation.language_id', 'mo_campaign_translation.name', 'mo_campaign_translation.description')
                            ->whereNull('mo_campaign.deleted_at')
                            ->where('mo_campaign.id', '=', $promocion->campaign_id)
                            ->join('mo_campaign_translation', 'mo_campaign.id', '=', 'mo_campaign_translation.campaign_id')
                            ->whereNull('mo_campaign_translation.deleted_at')
                            ->where('mo_campaign_translation.language_id', '=', $idi->id)
                            ->get();

                        foreach ($datos_campana_traduccion as $campana_traduccion) {
                            $promocion->campaign['id'] = $promocion->campaign_id;
                            $promocion->campaign['lang'][][$idi->abbreviation] = $campana_traduccion;
                        }

                    }

                    $promocion->subchannels = [];
                    if ($promocion->apply_subchannel_filter == 1) {
                        //Se obtienen los canales de la promocion
                        $promocion->subchannels = PromotionSubchannel::where('mo_promotion_subchannel.promotion_id', '=', $promocion->id)->get(['subchannel_id']);
                    }

                    $promocion->countrys = [];
                    if ($promocion->apply_country_filter == 1) {
                        //Se obtienen los paises de la promocion
                        $promocion->countrys = PromotionCountry::where('mo_promotion_country.promotion_id', '=', $promocion->id)->get(['country_id']);
                    }

                    $promocion->states = [];
                    if ($promocion->apply_state_filter == 1) {
                        //Se obtienen los estados de la promocion
                        $promocion->states = PromotionState::where('mo_promotion_state.promotion_id', '=', $promocion->id)->get(['state_id']);
                    }

                    $promocion->client_types = [];
                    if ($promocion->apply_client_type_filter == 1) {
                        //Se obtienen los tipos de cliente de la promocion
                        $promocion->client_types = PromotionClientType::where('mo_promotion_client_type.promotion_id', '=', $promocion->id)->get(['client_type_id']);
                    }

                    $promocion->devices = [];
                    if ($promocion->apply_device_filter == 1) {
                        //Se obtienen los dispositivos de la promocion
                        $promocion->devices = PromotionDevice::where('mo_promotion_device.promotion_id', '=', $promocion->id)->get(['device_id']);
                    }

                    $promocion->products = [];
                    if ($promocion->apply_product_filter == 1) {
                        //Se obtienen los productos de la promocion
                        $promocion->products = PromotionProduct::where('mo_promotion_product.promotion_id', '=', $promocion->id)->get(['product_id']);
                    }

                    $promocion->product_types = [];
                    if ($promocion->apply_product_type_filter == 1) {
                        //Se obtienen los tipos de producto de la promocion
                        $promocion->product_types = PromotionProductType::where('mo_promotion_product_type.promotion_id', '=', $promocion->id)->get(['product_type_id']);
                    }

                    $promocion->services = [];
                    if ($promocion->apply_service_filter == 1) {
                        //Se obtienen los servicios de la promocion
                        $promocion->services = PromotionService::where('mo_promotion_service.promotion_id', '=', $promocion->id)->get(['service_id']);
                    }

                    $promocion->currencies = [];
                    if ($promocion->apply_currency_filter == 1) {
                        //Se obtienen las monedas de la promocion
                        $promocion->currencies = PromotionCurrency::where('mo_promotion_currency.promotion_id', '=', $promocion->id)->get(['currency_id']);
                    }

                    $promocion->languages = [];
                    if ($promocion->apply_language_filter == 1) {
                        //Se obtienen los idiomas de la promocion
                        $promocion->languages = PromotionLanguage::where('mo_promotion_language.promotion_id', '=', $promocion->id)->get(['language_id']);
                    }

                    $promocion->payment_methods = [];
                    if ($promocion->apply_payment_method_filter == 1) {
                        //Se obtienen los métodos de pago de la promocion
                        $promocion->payment_methods = PromotionPaymentMethod::where('mo_promotion_payment_method.promotion_id', '=', $promocion->id)->get(['payment_method_id']);
                    }

                    $promocion->pickups = [];
                    if ($promocion->apply_pickup_filter == 1) {
                        //Se obtienen los canales de la promocion
                        $promocion->pickups = PromotionPickup::where('mo_promotion_pickup.promotion_id', '=', $promocion->id)->get(['pickup_id']);
                    }

                    //rangos de promocion de tipo venta en fecha
                    $promocion->sale_ranges = [];
                    $datos_promocion_sale_ranges = PromotionRangeSaleDay::where('mo_promotion_range_sale_day.promotion_id', '=', $promocion->id)->get();

                    foreach ($datos_promocion_sale_ranges as $promocion_sale_range) {

                        $datos_promocion_sale_days = PromotionSaleDay::where('mo_promotion_sale_day.promotion_range_sale_day_id', '=', $promocion_sale_range->id)->get();
                        $days_aux = [];
                        foreach ($datos_promocion_sale_days as $promocion_sale_days) {
                            $days_aux[] = $promocion_sale_days->day_id;
                        }

                        $promocion->sale_ranges[] = ['sale_date_start' => $promocion_sale_range->sale_date_start, 'sale_date_end' => $promocion_sale_range->sale_date_end, 'sale_days' => $days_aux];
                    }

                    //rangos de promocion de tipo disfrute en fecha
                    $promocion->enjoy_ranges = [];
                    //$promocion->enjoy_ranges = PromotionRangeEnjoyDay::where('mo_promotion_range_enjoy_day.promotion_id', '=', $promocion->id)->get(['enjoy_date_start', 'enjoy_date_end']);
                    $datos_promocion_enjoy_ranges = PromotionRangeEnjoyDay::where('mo_promotion_range_enjoy_day.promotion_id', '=', $promocion->id)->get();

                    foreach ($datos_promocion_enjoy_ranges as $promocion_enjoy_range) {

                        $datos_promocion_enjoy_days = PromotionEnjoyDay::where('mo_promotion_enjoy_day.promotion_range_enjoy_day_id', '=', $promocion_enjoy_range->id)->get();
                        $days_aux = [];
                        foreach ($datos_promocion_enjoy_days as $promocion_enjoy_days) {
                            $days_aux[] = $promocion_enjoy_days->day_id;
                        }

                        $promocion->enjoy_ranges[] = ['enjoy_date_start' => $promocion_enjoy_range->enjoy_date_start, 'enjoy_date_end' => $promocion_enjoy_range->enjoy_date_end, 'enjoy_days' => $days_aux];
                    }


                    $array['data'][0]['promotion'][] = [
                        'id' => $promocion->id,
                        'code' => $promocion->code,
                        'order' => $promocion->order,
                        'visibility' => $promocion->visibility,
                        'accumulate' => $promocion->accumulate,
                        'min_quantity' => $promocion->min_quantity,
                        'max_quantity' => $promocion->max_quantity,
                        'promotion_amount' => $promocion->promotion_amount,
                        'currency_id' => $promocion->currency_id,
                        'department_id' => $promocion->department_id,
                        'cost_center_id' => $promocion->cost_center_id,
                        'promotion_type_id' => $promocion->promotion_type_id,
                        'apply_product_filter' => $promocion->apply_product_filter,
                        'apply_product_type_filter' => $promocion->apply_product_type_filter,
                        'apply_service_filter' => $promocion->apply_service_filter,
                        'apply_subchannel_filter' => $promocion->apply_subchannel_filter,
                        'apply_country_filter' => $promocion->apply_country_filter,
                        'apply_state_filter' => $promocion->apply_state_filter,
                        'apply_client_type_filter' => $promocion->apply_client_type_filter,
                        'apply_device_filter' => $promocion->apply_device_filter,
                        'apply_payment_method_filter' => $promocion->apply_payment_method_filter,
                        'apply_language_filter' => $promocion->apply_language_filter,
                        'apply_currency_filter' => $promocion->apply_currency_filter,
                        'lockers_validation' => $promocion->lockers_validation,
                        'supervisor_validation' => $promocion->supervisor_validation,
                        'apply_pickup_filter' => $promocion->apply_pickup_filter,
                        'allow_date_change' => $promocion->allow_date_change,
                        'allow_upgrade' => $promocion->allow_upgrade,
                        'allow_product_change' => $promocion->allow_product_change,
                        'pay_difference' => $promocion->pay_difference,
                        'anticipation_start' => $promocion->anticipation_start,
                        'anticipation_end' => $promocion->anticipation_end,
                        'benefit_card' => $promocion->benefit_card,
                        'benefit_card_id' => $promocion->benefit_card_id,
                        'benefit_card_total_uses' => $promocion->benefit_card_total_uses,
                        'coupon_code' => $promocion->coupon_code,
                        'coupon_type' => $promocion->coupon_type,
                        'coupon_sheet_start' => $promocion->coupon_sheet_start,
                        'coupon_sheet_end' => $promocion->coupon_sheet_end,
                        'coupon_total_uses' => $promocion->coupon_total_uses,
                        'campaign' => $promocion->campaign,
                        'lang' => $promocion->lang,
                        'subchannels' => $promocion->subchannels,
                        'countries' => $promocion->countrys,
                        'states' => $promocion->states,
                        'products' => $promocion->products,
                        'product_types' => $promocion->product_types,
                        'services' => $promocion->services,
                        'currencies' => $promocion->currencies,
                        'languages' => $promocion->languages,
                        'payment_methods' => $promocion->payment_methods,
                        'pickups' => $promocion->pickups,
                        'client_types' => $promocion->client_types,
                        'devices' => $promocion->devices,
                        'sale_ranges' => $promocion->sale_ranges,
                        'enjoy_ranges' => $promocion->enjoy_ranges];
                }
                $array['total_results'] = $totales;
            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Promociones');
        }

        return response()->json($array, $array['error']);
    }


    /**
     * Función que muestra una promoción buscada por su id
     *
     * Para la consulta de una promoción se realizará una petición GET.
     * Si la operación no produce errores se devuelve, en la variable “error” el valor “200” y dentro del objeto “data” se almacena la información solicitada.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showId($id)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            $idiomas = Language::where('front', 1)->get();

            //Consulta BBDD
            $promo = DB::connection('tenant')->table('mo_promotion')
                ->select(
                    'mo_promotion.id',
                    'mo_promotion.code',
                    'mo_promotion.order',
                    'mo_promotion.user_id',
                    'mo_promotion.visibility',
                    'mo_promotion.accumulate',
                    'mo_promotion.min_quantity',
                    'mo_promotion.max_quantity',
                    'mo_promotion.promotion_amount',
                    'mo_promotion.currency_id',
                    'mo_promotion.department_id',
                    'mo_promotion.cost_center_id',
                    'mo_promotion.promotion_type_id',
                    'mo_promotion.campaign_id',
                    'mo_promotion.apply_product_filter',
                    'mo_promotion.apply_product_type_filter',
                    'mo_promotion.apply_service_filter',
                    'mo_promotion.apply_subchannel_filter',
                    'mo_promotion.apply_country_filter',
                    'mo_promotion.apply_state_filter',
                    'mo_promotion.apply_client_type_filter',
                    'mo_promotion.apply_device_filter',
                    'mo_promotion.apply_currency_filter',
                    'mo_promotion.apply_language_filter',
                    'mo_promotion.apply_payment_method_filter',
                    'mo_promotion.apply_pickup_filter',
                    'mo_promotion.lockers_validation',
                    'mo_promotion.supervisor_validation',
                    'mo_promotion.allow_date_change',
                    'mo_promotion.allow_upgrade',
                    'mo_promotion.allow_product_change',
                    'mo_promotion.pay_difference',
                    'mo_promotion.anticipation_start',
                    'mo_promotion.anticipation_end',
                    'mo_promotion.coupon_code',
                    'mo_promotion.coupon_type',
                    'mo_promotion.coupon_sheet_start',
                    'mo_promotion.coupon_sheet_end',
                    'mo_promotion.coupon_total_uses',
                    'mo_promotion.benefit_card',
                    'mo_promotion.benefit_card_id',
                    'mo_promotion.benefit_card_total_uses',
                    'mo_promotion.user_id'
                )
                ->join('mo_promotion_translation', 'mo_promotion.id', '=', 'mo_promotion_translation.promotion_id')
                ->where('mo_promotion_translation.deleted_at', '=', null)
                ->where('mo_promotion.deleted_at', '=', null);

            $promo->where('mo_promotion.id', '=', $id);

            $promo = $promo->groupBy('mo_promotion.id');
            $promo = $promo->first();
            //Fin consulta BBDD

            //ruta de storage de settings para concatenar a ruta de archivo de promocion
            $storage = Settings::where('name', 'storage_files')->first()->value;

            $array['data'] = array();

            if ($promo) {

                $files = DB::connection('tenant')->table('mo_promotion_file')
                    ->select('mo_file.id', 'mo_file.file_name', 'mo_file.file_size', 'mo_file.file_dimensions', 'mo_file.mimetype', 'mo_file.type_id', 'mo_file.order', 'mo_file.url_file',
                        'mo_file.device_id','mo_file.key_file')
                    ->where('mo_promotion_file.promotion_id', '=', $promo->id)
                    ->where('mo_promotion_file.deleted_at', '=', null)
                    ->join('mo_file', 'mo_file.id', '=', 'mo_promotion_file.file_id')
                    ->where('mo_file.deleted_at', '=', null)
                    ->join('mo_file_translation', 'mo_file_translation.file_id', '=', 'mo_file.id')
                    ->where('mo_file_translation.deleted_at', null)
                    ->groupBy('mo_file.id')->get();

                $sql_coupones = DB::connection('tenant')->table('mo_promotion_coupon')
                    ->select([
                        'mo_promotion_coupon.id',
                    ])
                    ->where('mo_promotion_coupon.deleted_at')
                    ->join('mo_promotion_coupon_translation', 'mo_promotion_coupon_translation.coupon_id', 'mo_promotion_coupon.id')
                    ->whereNull('mo_promotion_coupon_translation.deleted_at')
                    ->join('mo_promotion', 'mo_promotion.id', 'mo_promotion_coupon.promotion_id')
                    ->whereNull('mo_promotion.deleted_at')
                    ->where('mo_promotion.id', $promo->id)
                    ->groupBy('mo_promotion_coupon.id');


                $promo->files = array();
                $array_file = array();
                foreach ($files as $file) {

                    //obtiene tipo para comprobar que existe y no está borrado, si no es así no muestra datos de tipo
                    $tipo = FileType::find($file->type_id);

                    $array_file = ['id' => $file->id, 'file_name' => $file->file_name, 'file_size' => convertExtension($file->file_size),
                        'file_dimensions' => $file->file_dimensions, 'mimetype' => $file->mimetype, 'order' => $file->order, 'url_file' => $storage . $file->url_file,
                        'device_id' => $file->device_id,'key_file' => $file->key_file,'type' => ($file->type_id != null && $tipo != null) ? ['id' => $file->type_id] : null];
                    foreach ($idiomas as $idi) {

                        //si el tipo no existe o está borrado no busca sus traducciones
                        if ($tipo) {
                            $traducciones_tipos_files = FileTypeTranslation::where('type_id', $file->type_id)
                                ->select('id', 'language_id', 'name')
                                ->where('language_id', '=', $idi->id)
                                ->groupBy('mo_file_type_translation.type_id')
                                ->get();

                            foreach ($traducciones_tipos_files as $traduccion_tipo_file) {
                                $array_file['type']['lang'][][$idi->abbreviation] = $traduccion_tipo_file;
                            }
                        }

                        $traducciones_files = FileTranslation::where('file_id', $file->id)
                            ->select('id', 'language_id', 'name', 'description', 'alternative_text')
                            ->where('language_id', '=', $idi->id)
                            ->groupBy('mo_file_translation.file_id')
                            ->get();

                        foreach ($traducciones_files as $traduccion_file) {
                            $array_file['lang'][][$idi->abbreviation] = $traduccion_file;
                        }
                    }
                    $promo->files[] = $array_file;
                }

                //Elementos a los que aplica la promoción, según filtros activos
                $promo->products = array();
                if ($promo->apply_product_filter) {
                    $datos_products = PromotionProduct::where('promotion_id', $promo->id)
                        ->select('mo_promotion_product.id', 'mo_promotion_product.product_id', 'mo_promotion_product.location_id', 'mo_promotion_product.min_quantity', 'mo_promotion_product.max_quantity', 'mo_promotion_product.series_one', 'mo_promotion_product.series_two', 'mo_promotion_product.promotion_amount', 'mo_promotion_product.currency_id', 'mo_promotion_product.promotion_type_id', 'mo_promotion_product.apply_client_type_filter')
                        ->join('mo_product', 'mo_promotion_product.product_id', 'mo_product.id')
                        ->whereNull('mo_product.deleted_at')
                        ->whereNull('mo_promotion_product.deleted_at')
                        ->get();

                    $array_datos_products = array();

                    foreach ($datos_products as $product) {
                        $array_datos_products['product_id'] = $product->product_id;
                        $array_datos_products['location_id'] = $product->location_id;
                        $array_datos_products['min_quantity'] = $product->min_quantity;
                        $array_datos_products['max_quantity'] = $product->max_quantity;
                        $array_datos_products['series_one'] = $product->series_one;
                        $array_datos_products['series_two'] = $product->series_two;
                        $array_datos_products['promotion_amount'] = $product->promotion_amount;
                        $array_datos_products['currency_id'] = $product->currency_id;
                        $array_datos_products['promotion_type_id'] = $product->promotion_type_id;
                        $array_datos_products['apply_client_type_filter'] = $product->apply_client_type_filter;
                        $array_datos_products['client_type'] = array();
                        if ($product->apply_client_type_filter) {
                            $datos_client_type = DB::connection('tenant')->table('mo_promotion_product_client_type')
                                ->select('client_type_id')
                                ->where('mo_promotion_product_client_type.promotion_product_id', $product->id)
                                ->where('mo_promotion_product_client_type.deleted_at', null)
                                ->get();

                            foreach ($datos_client_type as $client_type) {
                                $array_datos_products['client_type'][] = ['client_type_id' => $client_type->client_type_id];
                            }
                        }
                        $promo->products[] = $array_datos_products;
                    }
                }

                $promo->product_types = array();
                if ($promo->apply_product_type_filter) {
                    $datos_product_types = PromotionProductType::where('promotion_id', $promo->id)
                        ->select('mo_promotion_product_type.id', 'mo_promotion_product_type.product_type_id', 'mo_promotion_product_type.min_quantity', 'mo_promotion_product_type.max_quantity', 'mo_promotion_product_type.series_one', 'mo_promotion_product_type.series_two', 'mo_promotion_product_type.promotion_amount', 'mo_promotion_product_type.currency_id', 'mo_promotion_product_type.promotion_type_id', 'mo_promotion_product_type.apply_client_type_filter')
                        ->join('mo_product_type', 'mo_promotion_product_type.product_type_id', 'mo_product_type.id')
                        ->whereNull('mo_product_type.deleted_at')
                        ->whereNull('mo_promotion_product_type.deleted_at')
                        ->get();

                    $array_datos_product_types = array();

                    foreach ($datos_product_types as $product_type) {
                        $array_datos_product_types['product_type_id'] = $product_type->product_type_id;
                        $array_datos_product_types['min_quantity'] = $product_type->min_quantity;
                        $array_datos_product_types['max_quantity'] = $product_type->max_quantity;
                        $array_datos_product_types['series_one'] = $product_type->series_one;
                        $array_datos_product_types['series_two'] = $product_type->series_two;
                        $array_datos_product_types['promotion_amount'] = $product_type->promotion_amount;
                        $array_datos_product_types['currency_id'] = $product_type->currency_id;
                        $array_datos_product_types['promotion_type_id'] = $product_type->promotion_type_id;
                        $array_datos_product_types['apply_client_type_filter'] = $product_type->apply_client_type_filter;
                        $array_datos_product_types['client_type'] = array();

                        //Obtiene los tipos de cliente asociados a la promocion por product_type
                        if ($product_type->apply_client_type_filter) {
                            $datos_client_type = DB::connection('tenant')->table('mo_promotion_product_type_client_type')
                                ->select('client_type_id')
                                ->where('mo_promotion_product_type_client_type.promotion_product_type_id', $product_type->id)
                                ->where('mo_promotion_product_type_client_type.deleted_at', null)
                                ->get();


                            foreach ($datos_client_type as $client_type) {
                                $array_datos_product_types['client_type'][] = ['client_type_id' => $client_type->client_type_id];
                            }
                        }

                        $promo->product_types[] = $array_datos_product_types;
                        //
                    }
                }

                $promo->services = array();
                if ($promo->apply_service_filter) {
                    $datos_services = PromotionService::where('promotion_id', $promo->id)
                        ->select('mo_promotion_service.id', 'mo_promotion_service.service_id', 'mo_promotion_service.min_quantity', 'mo_promotion_service.max_quantity', 'mo_promotion_service.series_one', 'mo_promotion_service.series_two', 'mo_promotion_service.promotion_amount', 'mo_promotion_service.currency_id', 'mo_promotion_service.promotion_type_id', 'mo_promotion_service.apply_client_type_filter')
                        ->join('mo_service', 'mo_promotion_service.service_id', 'mo_service.id')
                        ->whereNull('mo_service.deleted_at')
                        ->whereNull('mo_promotion_service.deleted_at')
                        ->get();
                    $array_datos_services = array();

                    foreach ($datos_services as $service) {

                        $array_datos_services['service_id'] = $service->service_id;
                        $array_datos_services['min_quantity'] = $service->min_quantity;
                        $array_datos_services['max_quantity'] = $service->max_quantity;
                        $array_datos_services['series_one'] = $service->series_one;
                        $array_datos_services['series_two'] = $service->series_two;
                        $array_datos_services['promotion_amount'] = $service->promotion_amount;
                        $array_datos_services['currency_id'] = $service->currency_id;
                        $array_datos_services['promotion_type_id'] = $service->promotion_type_id;
                        $array_datos_services['apply_client_type_filter'] = $service->apply_client_type_filter;
                        $array_datos_services['client_type'] = array();

                        //Obtiene los tipos de cliente asociados a la promocion por product_type
                        if ($service->apply_client_type_filter) {
                            $datos_client_type = DB::connection('tenant')->table('mo_promotion_service_client_type')
                                ->select('client_type_id')
                                ->where('mo_promotion_service_client_type.promotion_service_id', $service->id)
                                ->where('mo_promotion_service_client_type.deleted_at', null)
                                ->get();


                            foreach ($datos_client_type as $client_type) {
                                $array_datos_services['client_type'][] = ['client_type_id' => $client_type->client_type_id];
                            }
                        }

                        $promo->services[] = $array_datos_services;
                    }
                }

                $promo->subchannels = array();
                if ($promo->apply_subchannel_filter) {
                    $datos_subchannels = PromotionSubchannel::where('promotion_id', $promo->id)
                        ->select('subchannel_id')
                        ->join('mo_subchannel', 'mo_promotion_subchannel.subchannel_id', 'mo_subchannel.id')
                        ->whereNull('mo_subchannel.deleted_at')
                        ->whereNull('mo_promotion_subchannel.deleted_at')
                        ->get();

                    foreach ($datos_subchannels as $channel) {
                        $promo->subchannels[] = $channel;
                    }
                }

                $promo->countries = array();
                if ($promo->apply_country_filter) {
                    $datos_countries = PromotionCountry::where('promotion_id', $promo->id)
                        ->select('country_id')
                        ->join('mo_country', 'mo_promotion_country.country_id', 'mo_country.id')
                        ->whereNull('mo_country.deleted_at')
                        ->whereNull('mo_promotion_country.deleted_at')
                        ->get();

                    foreach ($datos_countries as $country) {
                        $promo->countries[] = $country;
                    }
                }

                $promo->states = array();
                if ($promo->apply_state_filter) {
                    $datos_states = PromotionState::where('promotion_id', $promo->id)
                        ->select('state_id')
                        ->join('mo_state', 'mo_promotion_state.state_id', 'mo_state.id')
                        ->whereNull('mo_state.deleted_at')
                        ->whereNull('mo_promotion_state.deleted_at')
                        ->get();

                    foreach ($datos_states as $state) {
                        $promo->states[] = $state;
                    }
                }

                $promo->currencies = array();
                if ($promo->apply_currency_filter) {
                    $datos_currencies = PromotionCurrency::where('promotion_id', $promo->id)
                        ->select('currency_id')
                        ->join('mo_currency', 'mo_promotion_currency.currency_id', 'mo_currency.id')
                        ->whereNull('mo_currency.deleted_at')
                        ->whereNull('mo_promotion_currency.deleted_at')
                        ->get();

                    foreach ($datos_currencies as $currency) {
                        $promo->currencies[] = $currency;
                    }
                }

                $promo->languages = array();
                if ($promo->apply_language_filter) {
                    $datos_languages = PromotionLanguage::where('promotion_id', $promo->id)
                        ->select('language_id')
                        ->join('mo_language', 'mo_promotion_language.language_id', 'mo_language.id')
                        ->whereNull('mo_language.deleted_at')
                        ->whereNull('mo_promotion_language.deleted_at')
                        ->get();

                    foreach ($datos_languages as $language) {
                        $promo->languages[] = $language;
                    }
                }


                $promo->client_types = array();
                if ($promo->apply_client_type_filter) {
                    $datos_clients = PromotionClientType::where('promotion_id', $promo->id)
                        ->select('client_type_id')
                        ->join('mo_client_type', 'mo_promotion_client_type.client_type_id', 'mo_client_type.id')
                        ->whereNull('mo_client_type.deleted_at')
                        ->whereNull('mo_promotion_client_type.deleted_at')
                        ->get();

                    foreach ($datos_clients as $client) {
                        $promo->client_types[] = $client;
                    }
                }

                $promo->devices = array();
                if ($promo->apply_device_filter) {

                    $datos_devices = PromotionDevice::where('promotion_id', $promo->id)
                        ->select('device_id')
                        ->join('mo_device', 'mo_device.id', 'mo_promotion_device.device_id')
                        ->whereNull('mo_device.deleted_at')
                        ->whereNull('mo_promotion_device.deleted_at')
                        ->get();

                    foreach ($datos_devices as $device) {
                        $promo->devices[] = $device;
                    }
                }

                $promo->payment_methods = array();
                if ($promo->apply_payment_method_filter) {
                    $datos_payment_methods = PromotionPaymentMethod::where('promotion_id', $promo->id)
                        ->select('payment_method_id')
                        ->join('mo_payment_method', 'mo_promotion_payment_method.payment_method_id', 'mo_payment_method.id')
                        ->whereNull('mo_payment_method.deleted_at')
                        ->whereNull('mo_promotion_payment_method.deleted_at')
                        ->get();

                    foreach ($datos_payment_methods as $payment_method) {
                        $promo->payment_methods[] = $payment_method;
                    }
                }

                $promo->pickups = array();
                if ($promo->apply_pickup_filter) {
                    $datos_pickups = PromotionPickup::where('promotion_id', $promo->id)
                        ->select('pickup_id')
                        ->join('mo_transportation_location', 'mo_promotion_pickup.pickup_id', 'mo_transportation_location.id')
                        ->whereNull('mo_transportation_location.deleted_at')
                        ->whereNull('mo_promotion_pickup.deleted_at')
                        ->get();

                    foreach ($datos_pickups as $pickup) {
                        $promo->pickups[] = $pickup;
                    }
                }
                //Fin elementos a los que aplica la promoción

                //rangos de promocion de tipo venta en fecha
                $promo->sale_ranges = [];
                //$promo->sale_ranges = PromotionRangeSale::where('mo_promotion_range_sale.promotion_id', '=', $promo->id)->get(['sale_date_start', 'sale_date_end']);
                $datos_promocion_sale_ranges = PromotionRangeSaleDay::where('mo_promotion_range_sale_day.promotion_id', '=', $promo->id)->get();

                foreach ($datos_promocion_sale_ranges as $promocion_sale_range) {

                    $datos_promocion_sale_days = PromotionSaleDay::where('mo_promotion_sale_day.promotion_range_sale_day_id', '=', $promocion_sale_range->id)->get();
                    $days_aux = [];
                    foreach ($datos_promocion_sale_days as $promocion_sale_days) {
                        $days_aux[] = $promocion_sale_days->day_id;
                    }

                    $promo->sale_ranges[] = ['sale_date_start' => $promocion_sale_range->sale_date_start, 'sale_date_end' => $promocion_sale_range->sale_date_end, 'sale_days' => $days_aux];
                }

                //rangos de promocion de tipo disfrute en fecha
                $promo->enjoy_ranges = [];
                //$promo->enjoy_ranges = PromotionRangeEnjoy::where('mo_promotion_range_enjoy.promotion_id', '=', $promo->id)->get(['enjoy_date_start', 'enjoy_date_end']);
                $datos_promocion_enjoy_ranges = PromotionRangeEnjoyDay::where('mo_promotion_range_enjoy_day.promotion_id', '=', $promo->id)->get();

                foreach ($datos_promocion_enjoy_ranges as $promocion_enjoy_range) {

                    $datos_promocion_enjoy_days = PromotionEnjoyDay::where('mo_promotion_enjoy_day.promotion_range_enjoy_day_id', '=', $promocion_enjoy_range->id)->get();
                    $days_aux = [];
                    foreach ($datos_promocion_enjoy_days as $promocion_enjoy_days) {
                        $days_aux[] = $promocion_enjoy_days->day_id;
                    }

                    $promo->enjoy_ranges[] = ['enjoy_date_start' => $promocion_enjoy_range->enjoy_date_start, 'enjoy_date_end' => $promocion_enjoy_range->enjoy_date_end, 'enjoy_days' => $days_aux];
                }

                $promo->campaign = null;
                $promo->lang = [];
                //Traducciones de la promoción. Si hay filtro lang, una única traducción.
                foreach ($idiomas as $idi) {

                    $traduccion = PromotionTranslation::where('promotion_id', '=', $promo->id)
                        ->select('id', 'language_id', 'gift', 'name', 'description', 'short_description', 'friendly_url', 'external_url', 'title_seo', 'description_seo', 'keywords_seo', 'observations', 'legal', 'notes')
                        ->where('language_id', '=', $idi->id)
                        ->get();
                    foreach ($traduccion as $trad) {
                        $promo->lang[][$idi->abbreviation] = $trad;
                    }

                    $datos_campana_traduccion = DB::connection('tenant')->table('mo_campaign')
                        ->select('mo_campaign_translation.language_id', 'mo_campaign_translation.name', 'mo_campaign_translation.description')
                        ->whereNull('mo_campaign.deleted_at')
                        ->where('mo_campaign.id', '=', $promo->campaign_id)
                        ->join('mo_campaign_translation', 'mo_campaign.id', '=', 'mo_campaign_translation.campaign_id')
                        ->whereNull('mo_campaign_translation.deleted_at')
                        ->where('mo_campaign_translation.language_id', '=', $idi->id)
                        ->get();

                    foreach ($datos_campana_traduccion as $campana_traduccion) {
                        $promo->campaign['id'] = $promo->campaign_id;
                        $promo->campaign['lang'][][$idi->abbreviation] = $campana_traduccion;
                    }
                }
                //Fin traducciones


                //Promociones accumulativas
                $promo->accumulate_promotion = array();

                $accumulate_promotion =
                    DB::connection('tenant')->table('mo_promotion_accumulate')
                        ->join('mo_promotion','mo_promotion.id' , 'mo_promotion_accumulate.promotion_accumulate_id' )
                        ->where('mo_promotion_accumulate.promotion_id',$promo->id)
                        ->whereNull('mo_promotion.deleted_at')
                        ->whereNull('mo_promotion_accumulate.deleted_at')
                        ->get();

                foreach ($accumulate_promotion as $accumulate){
                    $promotion_aux = array();

                    $promotion_aux['id'] = $accumulate->promotion_accumulate_id;
                    $promotion_aux['lang'] = array();
                    foreach ($idiomas as $idi) {
                        $traduccion = PromotionTranslation::where('promotion_id', '=', $accumulate->promotion_accumulate_id)
                            ->select('id', 'language_id', 'name')
                            ->where('language_id', '=', $idi->id)
                            ->whereNull('deleted_at')
                            ->get();

                        foreach ($traduccion as $trad) {
                            $promotion_aux['lang'][][$idi->abbreviation] = $trad;
                        }
                    }

                    $promo->accumulate_promotion[] = $promotion_aux;
                }
                //FIN promociones accumulativas

                //Cupones
                $promo->coupon = array();

                $datos_coupones = $sql_coupones->get();

                foreach ($datos_coupones as $coupon) {
                    $array_coupon = array();

                    $array_coupon['id'] = $coupon->id;

                    $array_coupon['lang'] = array();

                    foreach ($idiomas as $idi) {
                        $array_traducciones = array();
                        $datos_coupon_translations = PromotionCouponTranslation::where('coupon_id', $coupon->id)->where('language_id', $idi->id)->get();
                        foreach ($datos_coupon_translations as $coupon_translation) {
                            $array_traducciones = [
                                'file_id' => $coupon_translation->file_id,
                                'legal' => $coupon_translation->legal,
                            ];


                        }
                        $array_coupon['lang'][][$idi->abbreviation] = $array_traducciones;
                    }
                    $promo->coupon[] = $array_coupon;
                }
                    //Fin cupones

                $array['data'][0]['promotion'][] = [
                    'id' => $promo->id,
                    'code' => $promo->code,
                    'order' => $promo->order,
                    'visibility' => $promo->visibility,
                    'accumulate' => $promo->accumulate,
                    'min_quantity' => $promo->min_quantity,
                    'max_quantity' => $promo->max_quantity,
                    'promotion_amount' => $promo->promotion_amount,
                    'currency_id' => $promo->currency_id,
                    'department_id' => $promo->department_id,
                    'cost_center_id' => $promo->cost_center_id,
                    'promotion_type_id' => $promo->promotion_type_id,
                    'apply_product_filter' => $promo->apply_product_filter,
                    'apply_product_type_filter' => $promo->apply_product_type_filter,
                    'apply_service_filter' => $promo->apply_service_filter,
                    'apply_subchannel_filter' => $promo->apply_subchannel_filter,
                    'apply_country_filter' => $promo->apply_country_filter,
                    'apply_state_filter' => $promo->apply_state_filter,
                    'apply_client_type_filter' => $promo->apply_client_type_filter,
                    'apply_device_filter' => $promo->apply_device_filter,
                    'lockers_validation' => $promo->lockers_validation,
                    'supervisor_validation' => $promo->supervisor_validation,
                    'apply_payment_method_filter' => $promo->apply_payment_method_filter,
                    'apply_language_filter' => $promo->apply_language_filter,
                    'apply_currency_filter' => $promo->apply_currency_filter,
                    'apply_pickup_filter' => $promo->apply_pickup_filter,
                    'allow_date_change' => $promo->allow_date_change,
                    'allow_upgrade' => $promo->allow_upgrade,
                    'allow_product_change' => $promo->allow_product_change,
                    'pay_difference' => $promo->pay_difference,
                    'anticipation_start' => $promo->anticipation_start,
                    'anticipation_end' => $promo->anticipation_end,
                    'benefit_card' => $promo->benefit_card,
                    'benefit_card_id' => $promo->benefit_card_id,
                    'benefit_card_total_uses' => $promo->benefit_card_total_uses,
                    'coupon_code' => $promo->coupon_code,
                    'coupon_type' => $promo->coupon_type,
                    'coupon_sheet_start' => $promo->coupon_sheet_start,
                    'coupon_sheet_end' => $promo->coupon_sheet_end,
                    'coupon_total_uses' => $promo->coupon_total_uses,
                    'files' => $promo->files,
                    'campaign' => $promo->campaign,
                    'lang' => $promo->lang,
                    'subchannels' => $promo->subchannels,
                    'countries' => $promo->countries,
                    'states' => $promo->states,
                    'products' => $promo->products,
                    'product_types' => $promo->product_types,
                    'services' => $promo->services,
                    'currencies' => $promo->currencies,
                    'languages' => $promo->languages,
                    'payment_methods' => $promo->payment_methods,
                    'pickups' => $promo->pickups,
                    'client_types' => $promo->client_types,
                    'sale_ranges' => $promo->sale_ranges,
                    'enjoy_ranges' => $promo->enjoy_ranges,
                    'coupon' => $promo->coupon,
                    'devices' => $promo->devices,
                    'accumulate_promotion' => $promo->accumulate_promotion,
                ];

            }

            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Promociones');
        }

        return response()->json($array, $array['error']);

    }

    public function duplicatePromotion(Request $request)
    {
        $array['error'] = 200;

        try {

            DB::connection('tenant')->beginTransaction();

            //Validación
            $error = 0;
            $mensaje_validador = collect();

            $validator = \Validator::make($request->all(), [
                'id' => 'required|integer|min:0|exists:tenant.mo_promotion,id,deleted_at,NULL',
            ]);

            if ($validator->fails()) {
                $error = 1;
                $mensaje_validador = $mensaje_validador->merge($validator->errors());
            }


            if ($error == 1) {
                //Si se produce error lanza mensaje con campos implicados
                $array['error'] = 400;
                $array['error_description'] = 'The fields are not the required format';
                $array['error_inputs'][0] = $mensaje_validador;
            } else {  //Si no se a producido ningun error continua

                $heimdall_middleware = DB::connection('tenant')->table('mo_settings')
                    ->where('mo_settings.name', 'heimdall_middleware')
                    ->first()
                    ->value;

                $rule_codes = [];

                if($heimdall_middleware == '1') {

                    $website   = \Hyn\Tenancy\Facades\TenancyFacade::website();

                    $website = DB::table('saas_website')
                                    ->select('saas_website.id', 'saas_website.user_id', 'saas_website.status_id', 
                                    'saas_website.name', 'saas_website.domain', 'saas_website.nif',
                                    'saas_website.default_language_id', 'saas_website.default_currency_id')
                                    ->whereNull('saas_website.deleted_at')
                                    ->where('saas_website.website_id', $website->id)
                                    ->first();

                    $plans = websitePlanRules($website);

                    $cantidad = 0;

                    foreach($plans as $plan) {
                        if($plan->plan_id != null){
                            $rules = websiteRules($plan);
                            
                            $rules_promo = $rules->where('code', 'promo_promo');

                            if($rules_promo) {
                                foreach($rules_promo as $rule) {
                                    if($rule->unlimited == 1){
                                        $cantidad = -1;
                                    } else {
                                        if($cantidad >= 0 && $cantidad < $rule->value) {
                                            $cantidad = $rule->value;
                                        }
                                    }
                                }
                            }
                        } else {
                            if($plan->rule_code == 'promo_promo') {
                                if($plan->unlimited == 1){
                                    $cantidad = -1;
                                }else{
                                    if($cantidad >= 0 && $cantidad < $plan->value){
                                        $cantidad = $plan->value;
                                    }
                                }
                            }
                        }
                    }

                    if($cantidad == 0) {
                        $error = 1;
                        $rule_codes[] = 'promo_promo';
                    } else if ($cantidad > 0) {
                        $total_promotions = Promotion::get();

                        if(count($total_promotions) >= $cantidad) {
                            $error = 1;
                            $rule_codes[] = 'promo_promo';
                        }
                    }
                }

                if($error == 1) {
                    $array['error'] = 419;
                    $array['error_description'] = 'You do not have a valid plan';
                    $array['error_rule_code'] = $rule_codes;

                    //Heimdall - datos para redirección
                    $array['data'] = array();
                    $array['data'][0]['website_id'] = $website->id;

                    $protocol = DB::table('saas_settings')
                    ->where('saas_settings.name', 'protocol')->first()->value;
                    $base_path_saas = DB::table('saas_settings')
                    ->where('saas_settings.name', 'base_path_saas')->first()->value;
                    $array['data'][0]['heimdall_route_front'] = $protocol . $base_path_saas . '/login';
                    
                } else {

                    $promocion = Promotion::where('id', $request->get('id'))->first();

                    $promocion_new = Promotion::create([
                        'code' => $promocion->code.'-copy',
                        'order' => $promocion->order,
                        'visibility' => $promocion->visibility,
                        'accumulate' => $promocion->accumulate,
                        'min_quantity' => $promocion->min_quantity,
                        'max_quantity' => $promocion->max_quantity,
                        'promotion_amount' => $promocion->promotion_amount,
                        'currency_id' => $promocion->currency_id,
                        'promotion_type_id' => $promocion->promotion_type_id,
                        'campaign_id' => $promocion->campaign_id,
                        'department_id' => $promocion->department_id,
                        'cost_center_id' => $promocion->cost_center_id,
                        'apply_payment_method_filter' => $promocion->apply_payment_method_filter,
                        'apply_language_filter' => $promocion->apply_language_filter,
                        'apply_currency_filter' => $promocion->apply_currency_filter,
                        'apply_pickup_filter' => $promocion->apply_pickup_filter,
                        'apply_product_filter' => $promocion->apply_product_filter,
                        'apply_product_type_filter' => $promocion->apply_product_type_filter,
                        'apply_service_filter' => $promocion->apply_service_filter,
                        'apply_subchannel_filter' => $promocion->apply_subchannel_filter,
                        'apply_client_type_filter' => $promocion->apply_client_type_filter,
                        'apply_country_filter' => $promocion->apply_country_filter,
                        'apply_state_filter' => $promocion->apply_state_filter,
                        'apply_device_filter' => $promocion->apply_device_filter,
                        'lockers_validation' => $promocion->lockers_validation,
                        'supervisor_validation' => $promocion->supervisor_validation,
                        'allow_date_change' => $promocion->allow_date_change,
                        'allow_upgrade' => $promocion->allow_upgrade,
                        'allow_product_change' => $promocion->allow_product_change,
                        'pay_difference' => $promocion->pay_difference,
                        'anticipation_start' => $promocion->anticipation_start,
                        'anticipation_end' => $promocion->anticipation_end,
                        'coupon_code' => $promocion->coupon_code,
                        'coupon_type' => $promocion->coupon_type,
                        'coupon_sheet_start' => $promocion->coupon_sheet_start,
                        'coupon_sheet_end' => $promocion->coupon_sheet_end,
                        'coupon_total_uses' => $promocion->coupon_total_uses,
                        'benefit_card' => $promocion->benefit_card,
                        'benefit_card_total_uses' => $promocion->benefit_card_total_uses,
                        'benefit_card_id' => $promocion->benefit_card_id,
                        'user_id' => $promocion->user_id
                    ]);

                    $promotion_translations = PromotionTranslation::where('promotion_id', $promocion->id)->get();

                    foreach ($promotion_translations as $translation) {
                        PromotionTranslation::create([
                            'promotion_id' => $promocion_new->id,
                            'language_id' => $translation->language_id,
                            'gift' => $translation->gift,
                            'name' => $translation->name.'-copy',
                            'description' => $translation->description,
                            'short_description' => $translation->short_description,
                            'friendly_url' => $translation->friendly_url,
                            'external_url' => $translation->external_url,
                            'title_seo' => $translation->title_seo,
                            'description_seo' => $translation->description_seo,
                            'keywords_seo' => $translation->keywords_seo,
                            'observations' => $translation->observations,
                            'legal' => $translation->legal,
                            'notes' => $translation->notes,
                        ]);
                    }

                    $promotion_accumulate = PromotionAccumulate::where('promotion_id', $promocion->id)->get();

                    foreach ($promotion_accumulate as $accumulate) {
                        PromotionAccumulate::create([
                            'promotion_id' => $promocion_new->id,
                            'promotion_accumulate_id' => $accumulate->promotion_accumulate_id,
                        ]);
                    }


                    $client_types = PromotionClientType::where('promotion_id', $promocion->id)->get();

                    foreach ($client_types as $client_type) {
                        PromotionClientType::create([
                            'promotion_id' => $promocion_new->id,
                            'client_type_id' => $client_type->client_type_id,
                        ]);
                    }

                    $countries = PromotionCountry::where('promotion_id', $promocion->id)->get();
                    foreach ($countries as $country) {
                        PromotionCountry::create([
                            'promotion_id' => $promocion_new->id,
                            'country_id' => $country->country_id,
                        ]);
                    }

                    $cupones = PromotionCoupon::where('promotion_id', $promocion->id)->get();
                    foreach ($cupones as $cupon) {
                        $cupon_new = PromotionCoupon::create([
                            'promotion_id' => $promocion_new->id,
                        ]);

                        $coupon_translate = PromotionCouponTranslation::where('coupon_id', $cupon->id)->get();

                        foreach ($coupon_translate as $coupon) {
                            PromotionCouponTranslation::create([
                                'coupon_id' => $cupon_new->id,
                                'language_id' => $coupon->language_id,
                                'file_id' => $coupon->file_id,
                                'legal' => $coupon->legal,
                            ]);
                        }
                    }

                    $currencies = PromotionCurrency::where('promotion_id', $promocion->id)->get();

                    foreach ($currencies as $currency) {
                        PromotionCurrency::create([
                            'promotion_id' => $promocion_new->id,
                            'currency_id' => $currency->currency_id,
                        ]);
                    }

                    $range_enjoy_days = PromotionRangeEnjoyDay::where('promotion_id', $promocion->id)->get();

                    foreach ($range_enjoy_days as $range) {
                        $range_new = PromotionRangeEnjoyDay::create([
                            'promotion_id' => $promocion_new->id,
                            'enjoy_date_start' => $range->enjoy_date_start,
                            'enjoy_date_end' => $range->enjoy_date_end
                        ]);

                        $enjoy_days = PromotionEnjoyDay::where('promotion_range_enjoy_day_id', $range->id)->get();

                        foreach ($enjoy_days as $day) {
                            PromotionEnjoyDay::create([
                                'promotion_range_enjoy_day_id' => $range_new->id,
                                'day_id' => $day->day_id
                            ]);
                        }
                    }

                    $files = PromotionFile::where('promotion_id', $promocion->id)->get();

                    foreach ($files as $file) {
                        PromotionFile::create([
                            'promotion_id' => $promocion_new->id,
                            'file_id' => $file->file_id
                        ]);
                    }

                    $languages = PromotionLanguage::where('promotion_id', $promocion->id)->get();

                    foreach ($languages as $language) {
                        PromotionLanguage::create([
                            'promotion_id' => $promocion_new->id,
                            'language_id' => $language->language_id,
                        ]);
                    }

                    $payment_methods = PromotionPaymentMethod::where('promotion_id', $promocion->id)->get();

                    foreach ($payment_methods as $payment_method) {
                        PromotionPaymentMethod::create([
                            'promotion_id' => $promocion_new->id,
                            'payment_method_id' => $payment_method->payment_method_id,
                        ]);
                    }

                    $pickups = PromotionPickup::where('promotion_id', $promocion->id)->get();
                    foreach ($pickups as $pickup) {
                        PromotionPickup::create([
                            'promotion_id' => $promocion_new->id,
                            'pickup_id' => $pickup->pickup_id,
                        ]);
                    }

                    $products = PromotionProduct::where('promotion_id', $promocion->id)->get();

                    foreach ($products as $product) {
                        $product_new = PromotionProduct::create([
                            'promotion_id' => $promocion_new->id,
                            'product_id' => $product->product_id,
                            'location_id' => $product->location_id,
                            'min_quantity' => $product->min_quantity,
                            'max_quantity' => $product->max_quantity,
                            'series_one' => $product->series_one,
                            'series_two' => $product->series_two,
                            'promotion_amount' => $product->promotion_amount,
                            'currency_id' => $product->currency_id,
                            'promotion_type_id' => $product->promotion_type_id,
                            'apply_client_type_filter' => $product->apply_client_type_filter,
                        ]);

                        $product_client_types = PromotionProductClientType::where('promotion_product_id', $product->id)->get();

                        foreach ($product_client_types as $product_client_type) {
                            PromotionProductClientType::create([
                                'promotion_product_id' => $product_new->id,
                                'client_type_id' => $product_client_type->client_type_id
                            ]);
                        }
                    }

                    $product_types = PromotionProductType::where('promotion_id', $promocion->id)->get();
                    foreach ($product_types as $product_type) {
                        $product_type_new = PromotionProductType::create([
                            'promotion_id' => $promocion_new->id,
                            'product_type_id' => $product_type->product_type_id,
                            'min_quantity' => $product_type->min_quantity,
                            'max_quantity' => $product_type->max_quantity,
                            'series_one' => $product_type->series_one,
                            'series_two' => $product_type->series_two,
                            'promotion_amount' => $product_type->promotion_amount,
                            'currency_id' => $product_type->currency_id,
                            'promotion_type_id' => $product_type->promotion_type_id,
                            'apply_client_type_filter' => $product_type->apply_client_type_filter,
                        ]);

                        $product_type_client_types = PromotionProductTypeClientType::where('promotion_product_type_id', $product_type->id)->get();

                        foreach ($product_type_client_types as $product_type_client_type) {
                            PromotionProductTypeClientType::create([
                                'promotion_product_type_id' => $product_type_new->id,
                                'client_type_id' => $product_type_client_type->client_type_id
                            ]);
                        }
                    }

                    $ranges_sale = PromotionRangeSaleDay::where('promotion_id', $promocion->id)->get();

                    foreach ($ranges_sale as $range_sale) {
                        $range_sale_new = PromotionRangeSaleDay::create([
                            'promotion_id' => $promocion_new->id,
                            'sale_date_start' => $range_sale->sale_date_start,
                            'sale_date_end' => $range_sale->sale_date_end,
                        ]);

                        $sale_days = PromotionSaleDay::where('promotion_range_sale_day_id', $range_sale->id)->get();

                        foreach ($sale_days as $sale_day) {
                            PromotionSaleDay::create([
                                'promotion_range_sale_day_id' => $range_sale_new->id,
                                'day_id' => $sale_day->day_id
                            ]);
                        }
                    }

                    $services = PromotionService::where('promotion_id', $promocion->id)->get();

                    foreach ($services as $service) {
                        $service_new = PromotionService::create([
                            'promotion_id' => $promocion_new->id,
                            'service_id' => $service->service_id,
                            'min_quantity' => $service->min_quantity,
                            'max_quantity' => $service->max_quantity,
                            'series_one' => $service->series_one,
                            'series_two' => $service->series_two,
                            'promotion_amount' => $service->promotion_amount,
                            'currency_id' => $service->currency_id,
                            'promotion_type_id' => $service->promotion_type_id,
                            'apply_client_type_filter' => $service->apply_client_type_filter,
                        ]);

                        $service_client_types = PromotionServiceClientType::where('promotion_service_id', $service->id)->get();

                        foreach ($service_client_types as $service_client_type) {
                            PromotionServiceClientType::create([
                                'promotion_service_id' => $service_new->id,
                                'client_type_id' => $service_client_type->client_type_id
                            ]);
                        }
                    }

                    $states = PromotionState::where('promotion_id', $promocion->id)->get();

                    foreach ($states as $state) {
                        PromotionState::create([
                            'promotion_id' => $promocion_new->id,
                            'state_id' => $state->state_id,
                        ]);
                    }

                    $subchannels = PromotionSubchannel::where('promotion_id', $promocion->id)->get();

                    foreach ($subchannels as $subchannel) {
                        PromotionSubchannel::create([
                            'promotion_id' => $promocion_new->id,
                            'subchannel_id' => $subchannel->subchannel_id
                        ]);
                    }

                    $promotion_device = PromotionDevice::where('promotion_id', $promocion->id)->get();

                    foreach ($promotion_device as $device){
                        PromotionDevice::create([
                            'promotion_id' => $promocion_new->id,
                            'device_id' => $device->device_id,
                        ]);
                    }

                    $array['data'][] = ['promotion' => [['id' => $promocion_new->id]]];
                }
            }


            DB::connection('tenant')->commit();

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            $array['error'] = 500;
            $array['error_description'] = 'Internal system error';
            $array['error_catch'] = $e->getMessage();
            reportService($e, 'Promociones');
        }

        return response()->json($array, $array['error']);
    }
}