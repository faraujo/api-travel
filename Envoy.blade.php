@servers(['web' => 'ubuntu@108.128.107.80'])

@setup
    $repository = 'git@gitlab.com:viavox-travel/mtr.git';
    $releases_dir = '/var/www/websites/viavox_experience/mtr/releases';
    $releases_dir_staging = '/var/www/websites/viavox_experience_staging/mtr/releases';
    $app_dir = '/var/www/websites/viavox_experience/mtr';
    $app_dir_staging = '/var/www/websites/viavox_experience_staging/mtr';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    $new_release_dir_staging = $releases_dir_staging .'/'. $release;
@endsetup


@story('deploy_staging')
    clone_repository_staging
    permissions_staging
    run_composer_staging
    update_symlinks_staging
    cache_staging
    run_migrations_staging
    run_migrations_tenant_staging
    run_passport_staging
    run_swagger_staging
    update_symlinks_current_staging
    {{-- notify_staging --}}
@endstory

@story('deploy_production')
    clone_repository
    permissions
    run_composer
    update_symlinks
    cache
    run_migrations
    run_migrations_tenant
    run_passport
    run_swagger
    update_symlinks_current
    {{-- notify --}}
@endstory


{{-- Production --}}
@task('clone_repository')
    echo 'Cloning repository ({{ $release }})'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 --branch=master {{ $repository }} {{ $new_release_dir }}
    echo 'END Cloning repository ({{ $release }})'
@endtask

@task('permissions')
    echo 'Permissions ({{ $release }})'
    cd {{ $new_release_dir }}
    chmod 777 -R bootstrap/cache
    chmod 777 -R storage
    echo 'END Permissions ({{ $release }})'
@endtask

@task('run_composer')
    echo 'Starting Composer ({{ $release }})'
    cd {{ $new_release_dir }}
    composer install --optimize-autoloader --no-dev
    echo 'END Starting Composer ({{ $release }})'
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
    echo 'END Linking .env file'

    cd {{ $new_release_dir }}
    php artisan storage:link
@endtask

@task('cache')
    echo 'Starting Cache ({{ $release }})'
    cd {{ $new_release_dir }}
    php artisan config:cache
    php artisan route:cache
    echo 'END Starting Cache ({{ $release }})'
@endtask

@task('run_migrations')
    echo 'Starting Migrations System ({{ $release }})'
    cd {{ $new_release_dir }}
    php artisan migrate --force
    echo 'END Starting Migrations System ({{ $release }})'
@endtask

@task('run_migrations_tenant')
    echo 'Starting Migrations Tenant ({{ $release }})'
    cd {{ $new_release_dir }}
    php artisan tenancy:migrate --force
    echo 'END Starting Migrations System ({{ $release }})'
@endtask

@task('run_passport')
    echo 'Install Passport ({{ $release }})'
    cd {{ $new_release_dir }}
    php artisan passport:install
    echo 'END Install Passport ({{ $release }})'
@endtask

@task('run_swagger')
    echo 'Generate Swagger ({{ $release }})'
    cd {{ $new_release_dir }}
    php artisan l5-swagger:generate
    echo 'END Generate Swagger ({{ $release }})'
@endtask

@task('update_symlinks_current')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
    echo 'END Linking current release'
@endtask

{{-- @task('notify')
    @slack('https://hooks.slack.com/services/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', '#viavox_travel_mtr_gitlab')
@endtask --}}




{{-- Staging --}}
@task('clone_repository_staging')
    echo 'Cloning repository ({{ $release }})'
    [ -d {{ $releases_dir_staging }} ] || mkdir {{ $releases_dir_staging }}
    git clone --depth 1 --branch=staging {{ $repository }} {{ $new_release_dir_staging }}
    echo 'END Cloning repository ({{ $release }})'
@endtask

@task('permissions_staging')
    echo 'Permissions ({{ $release }})'
    cd {{ $new_release_dir_staging }}
    chmod 777 -R bootstrap/cache
    chmod 777 -R storage
    echo 'END Permissions ({{ $release }})'
@endtask

@task('run_composer_staging')
    echo 'Starting Composer ({{ $release }})'
    cd {{ $new_release_dir_staging }}
    composer install --optimize-autoloader --no-dev
    echo 'END Starting Composer ({{ $release }})'
@endtask

@task('update_symlinks_staging')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir_staging }}/storage
    ln -nfs {{ $app_dir_staging }}/storage {{ $new_release_dir_staging }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir_staging }}/.env {{ $new_release_dir_staging }}/.env
    echo 'END Linking .env file'

    cd {{ $new_release_dir_staging }}
    php artisan storage:link
@endtask

@task('cache_staging')
    echo 'Starting Cache ({{ $release }})'
    cd {{ $new_release_dir_staging }}
    php artisan config:cache
    php artisan route:cache
    echo 'END Starting Cache ({{ $release }})'
@endtask

@task('run_migrations_staging')
    echo 'Starting Migrations System ({{ $release }})'
    cd {{ $new_release_dir_staging }}
    php artisan migrate
    echo 'END Starting Migrations System ({{ $release }})'
@endtask

@task('run_migrations_tenant_staging')
    echo 'Starting Migrations Tenant ({{ $release }})'
    cd {{ $new_release_dir_staging }}
    php artisan tenancy:migrate
    echo 'END Starting Migrations System ({{ $release }})'
@endtask

@task('run_passport_staging')
    echo 'Install Passport ({{ $release }})'
    cd {{ $new_release_dir_staging }}
    php artisan passport:install
    echo 'END Install Passport ({{ $release }})'
@endtask

@task('run_swagger_staging')
    echo 'Generate Swagger ({{ $release }})'
    cd {{ $new_release_dir_staging }}
    php artisan l5-swagger:generate
    echo 'END Generate Swagger ({{ $release }})'
@endtask

@task('update_symlinks_current_staging')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir_staging }} {{ $app_dir_staging }}/current
    echo 'END Linking current release'
@endtask

{{-- @task('notify_staging')
    @slack('https://hooks.slack.com/services/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', '#viavox_travel_mtr_gitlab')
@endtask --}}